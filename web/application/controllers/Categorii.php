<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Categorii extends Admin_Controller
{

    private $viewPath = 'admin/generale/categorii/';

    /**
     * @var Mess $mess
     */
    public $mess;

    /**
     *
     * @var M_categorii $m_categorii
     */
    public $m_categorii;

    function __construct()
    {
        parent::__construct();

        $this->load->model('m_categorii');
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Listare categorii
     */
    public function index()
    {
        // Get data.
        $model_categorii = $this->m_categorii;
        $data = $model_categorii->fetchList(array('deleted' => $model_categorii::DELETED_ANY));

        // Set data.
        $viewData = array(
            'items' => $data,

            // Template data
            'page_title' => 'Categorii articole',
            'breadcrumb' => array(
                array(
                    'name' => 'Categorii articole',
                ),
            ),
        );

        // Render view.
        $this->_render($this->viewPath . 'list', $this->defaultTemplate, $viewData);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Adauga categorie noua
     *
     */
    public function add()
    {
        $this->edit();
    }

    /**
     * Editeaza categorie
     * @param int|bool $id
     */
    function edit($id = null)
    {
        $this->data['edit']   = $edit = ($id) ? (int)$id : null;

        // Get data by id
        if ($id) {
            $data = $this->m_categorii->load($id);
        } else {
            $data = [
                'nume' => null,
                'parent_id' => 0,
                'alias' => null,
                'order_id' => 255,
                'is_published' => 0,
            ];
        }
        $this->data['data'] = $data;

        // ------------------------------

        // Parse post data
        if (post()) {

            if (empty($_POST['alias'])) {
                $_POST['alias'] = str_replace(" ", "_", strtolower($_POST['nume']));
            }

            $fv = $this->form_validation;

            // REGULI
            $rules_config = array(
                array(
                    'field'     => 'nume',
                    'label'     => 'Nume',
                    'rules'     => 'trim|required|min_length[3]|xss_clean|strip_tags'
                ),
                array(
                    'field'     => 'is_published',
                    'label'     => 'Publicat',
                    'rules'     => 'trim|xss_clean|is_natural|strip_tags'
                ),
                array(
                    'field'     => 'parinte_id',
                    'label'     => 'Parinte',
                    'rules'     => 'trim|xss_clean|is_natural|strip_tags'
                ),
                array(
                    'field'     => 'alias',
                    'label'     => 'Alias',
                    'rules'     => 'trim|required|xss_clean|min_length[3]|strip_tags'
                ),
                array(
                    'field'     => 'order_id',
                    'label'     => 'Order',
                    'rules'     => 'trim|required|xss_clean|is_natural|strip_tags'
                ),
            );

            $fv->set_rules($rules_config);

            if ($fv->run()) {

                // Verifica unicitatea numelui
                $res = $this->m_categorii->fetchBrothers(post('nume'), post('parent_id'),  $id);
                if ($res && count($res)) {
                    $this->mess->displayErrors(array('nume' => 'Categoria nu poate fi salvată. Exista o altă categorie cu aceeași denumire'));
                }

                try {
                    $this->m_categorii->save(post(), $id);
                } catch (Exception $e) {
                    $this->mess->displayErrors(array('nume' => 'Datele nu pot fi salvate în baza de date -> ' . $e->getMessage()));
                }


                if ($this->input->is_ajax_request()) {
                    $this->mess->displaySuccess('Datele au fost salvate cu succes');
                }
            } else {

                if ($this->input->is_ajax_request()) {
                    $allErrors = $fv->error_array();
                    echo json_encode(array('error' => $allErrors));
                    exit;
                }
            }
        }

        // ------------------------------

        // Template data
        $this->data['page_title'] = $edit ? 'Editează categorie' : 'Adaugă categorie';
        $this->data['breadcrumb'] = array(
            array(
                'name' => 'Listă categorii',
                'href' => '/categorii/',
            ),
            array(
                'name' => $edit ? 'Editează categorie' : 'Adaugă categorie',
            ),
        );
        $this->data['parentItems'] = $this->m_categorii->fetchParents();

        $this->_render($this->viewPath . 'edit', $this->defaultTemplate);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Sterge categorie
     * @param int $id
     */
    function delete($id)
    {
        $m = new Mess();

        $id = (int)$id;
        if (!$id) {
            $m->displayErrors("ID categorie invalid");
        }

        if (!post('submited')) {
            $m->displayErrors("Formular invalid");
        }

        $data = $this->m_categorii->load($id);
        if (!$data || !count($data)) {
            $m->displayErrors("Categoria nu a fost găsită în baza de date");
        }

        // Verifica daca categoria are categorii 'copii'.
        if ($this->m_categorii->fetchChildren($id)) {
            $m->displayErrors("Categoria nu poate fi ștearsă. Există alte categorii care o au ca părinte.");
        }

        // Verifica daca categoria este folosit deja.
        if ($data['is_used']) {
            $m->displayErrors("Categoria nu poate fi ștearsă. Există articole către care este asignată.");
        }

        // Delete data.
        try {
            $this->m_categorii->delete($id);
            $m->displaySuccess(true);
        } catch (Exception $e) {
            $m->displayErrors('Nu am putut șterge categoria din baza de date -> ' . $e->getMessage());
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Restaurare categorie in urma stergerii
     * @param int $id
     */
    function restore($id)
    {
        $m = new Mess();

        $id = (int)$id;
        if (!$id) {
            $m->displayErrors("ID categorie invalid");
        }

        if (!post('submited')) {
            $m->displayErrors("Formular invalid");
        }

        $data = $this->m_categorii->load($id);
        if (!$data || !count($data)) {
            $m->displayErrors("Categoria nu a fost găsită în baza de date");
        }

        // Verifica daca categoria este stearsa.
        if (!$data['is_deleted']) {
            $m->displayErrors("Categoria nu poate fi restaurată, pentru ca nu este ștearsă.");
        }

        // Delete data.
        try {
            $this->m_categorii->restore($id);
            $m->displaySuccess('Categoria a fost restaurată!');
        } catch (Exception $e) {
            $m->displayErrors('Nu am putut restaura categoria -> ' . $e->getMessage());
        }
    }
}
