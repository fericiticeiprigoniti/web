<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Utilizatori extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * afiseaza autorii inregistrati cu detaliile
     *
     */
    function index()
    {
        $this->administratori();
    }

    /**
     * afiseaza detaliile profilului / modifica date + parola
     *
     */
    function cont($userID = false)
    {
        $m        = new Mess();
        $userInfo = [];

        if ($userID) {
            $userID    = (int)$userID;
            if (!$userInfo  = $this->aauth->get_user($userID)) show_404();
        }

        // reguli
        $rules_config = [
            array(
                'field'     => 'nume',
                'label'     => 'Nume',
                'rules'     => 'trim|required|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'prenume',
                'label'     => 'Prenume',
                'rules'     => 'trim|required|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'email',
                'label'     => 'Email',
                'rules'     => 'trim|required|valid_email|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'pass',
                'label'     => 'Parola',
                'rules'     => 'trim|xss_clean|min_length[5]|max_length[20]|matches[pass_conf]' . (!$userID ? '|required' : '')
            ),
            array(
                'field'     => 'pass_conf',
                'label'     => 'Parola confirmare',
                'rules'     => 'trim|xss_clean|min_length[5]|max_length[20]' . (!$userID ? '|required' : '')
            ),
            array(
                'field'     => 'groups[]',
                'label'     => 'Grupuri',
                'rules'     => 'trim|xss_clean' . (!$userID ? '|required' : '')
            )
        ];

        $this->form_validation->set_rules($rules_config);

        $this->form_validation->set_message('matches', 'Parola noua nu corespunde cu parola confirmata!');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        if (post()) {
            if ($this->form_validation->run() == TRUE) {
                // Save data
                $psw = !empty(post('pass')) ? post('pass') : false;

                try {
                    if ($userID) {
                        // update user
                        $this->aauth->update_user($userID, post('email'), $psw);
                    } else {
                        // insert new user
                        if (!$userID = $this->aauth->create_user(post('email'), $psw)) {
                            // not success - fetch errors
                            if ($this->input->is_ajax_request()) {
                                $allErrors = $this->aauth->print_errors();
                                echo json_encode(['error' => $allErrors]);
                                exit;
                            }
                        }
                    }

                    // Continue - set another variables
                    $this->aauth->set_user_var('nume', post('nume'), $userID);
                    $this->aauth->set_user_var('prenume', post('prenume'), $userID);

                    // Set Groups/permisions
                    // clear all groups
                    $this->aauth->remove_member_from_all($userID);

                    // check if multiple Group are selected and adds member to them
                    if (post('groups')) {
                        foreach (post('groups') as $group) {
                            $this->aauth->add_member($userID, (int)$group);
                        }
                    }

                    $m->displaySuccess('Setările au fost salvate cu succes!');
                } catch (Exception $e) {
                    if ($this->input->is_ajax_request()) {
                        $allErrors = $this->aauth->print_errors();
                        echo json_encode(['error' => $allErrors]);
                        exit;
                    } else {

                        $m->displayErrors('Nu s-a putut salva utilizatorul ->' . $e);
                    }
                }
            } else {

                // send pNotify with form error
                // show errors bellow the fields
                if ($this->input->is_ajax_request()) {
                    $allErrors = $this->form_validation->error_array();
                    echo json_encode(['error' => $allErrors]);
                    exit;
                }
            }
        }

        // memorize user Variables & Groups
        if ($userID) {
            // get variables
            if ($variables = $this->aauth->get_user_vars($userID)) {
                foreach ($variables as $v) {
                    $userInfo->variables[$v->data_key] = $v->value;
                }
            }

            if ($result = $this->aauth->get_user_groups($userID)) {
                $us_groups = [];
                foreach ($result as $item) {
                    $us_groups[] = $item->id;
                }
            }

            $userInfo->groups       = $us_groups;
        }

        // Set data.
        $viewData = [
            'userInfo'  => $userInfo,
            'groups'    => $this->aauth->list_groups(),

            // Template data
            'page_title' => $userID ? 'Edit utilizator' : 'Adaugă utilizator',
            'breadcrumb' => [
                ['name' => 'Utilizatori']
            ]
        ];

        $this->_render('/admin/administrativ/utilizatori/edit', $this->defaultTemplate, $viewData);
    }

    /**
     * List with all users
     *
     */
    function administratori()
    {
        $data = AauthUserTable::getInstance()->fetchAll()['items'];

        // Get data.
        if (empty($data))
            show_error('Nu am putut prelua informatiile din baza de date');

        // Set data.
        $viewData = [
            'items' => $data,

            // Template data
            'page_title' => 'Utilizatori',
            'breadcrumb' => [
                ['name' => 'Listă admini']
            ]
        ];

        $this->_render('/admin/administrativ/utilizatori/list', $this->defaultTemplate, $viewData);
    }

    /**
     * AJAX function - Delete user
     *
     */
    function delete()
    {
        $response = ['success' => false];

        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }


        if (post()) {
            if ($this->aauth->delete_user((int)post('id'))) {
                $response['success'] = true;
            }
        }

        echo json_encode($response);
        die;
    }

    /**
     * AJAX function - Ban user
     *
     */
    function ban()
    {
        $response = ['success' => false];

        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        if (post()) {
            if ($this->aauth->ban_user((int)post('id'))) {
                $response['success'] = true;
            }
        }

        echo json_encode($response);
        die;
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/admin/administrativ.php */