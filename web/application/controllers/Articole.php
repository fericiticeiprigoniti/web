<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Articole extends Admin_Controller
{

    private $viewPath = 'admin/generale/articole/';

    function __construct()
    {
        parent::__construct();

        $this->load->library('pagination');
        $this->load->helper(array('form', 'url'));

        $this->load->model('m_personaje');
        $this->load->model('m_biblioteca_carti');
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * TODO filters
     *
     * Listare articole
     * @internal param $viewData
     */
    public function index()
    {
        // Filters.
        $getData = $_GET;
        $filters = new ArticolFilters($getData);
        $filters->setOrderBy([$filters::ORDER_BY_ID_DESC]);

        // Limit.
        $page    = isset($_GET['_page']) ? (int)$_GET['_page'] : 1;
        $perPage = isset($_GET['_per_page']) ? (int)$_GET['_per_page'] : 50;
        $offset  = ($page - 1) * $perPage;

        // Fetch data
        $data = ArticolTable::getInstance()->fetchAll($filters, $perPage, $offset);

        // Get data.
        if (!isset($data['items']) && !isset($data['count'])) {
            die('Nu am putut prelua informatiile din baza de date');
        }

        // -----------------------------------

        // Initialize pagination.
        $pConfig = $this->config->item('pagination');
        $pConfig['base_url'] = General::url(array('_page' => null));
        $pConfig['total_rows'] = $data['count'];
        $pConfig['cur_page'] = $page;
        $pConfig['per_page'] = $perPage;
        $this->pagination->initialize($pConfig);

        // -----------------------------------

        // Set data.
        $viewData = array(
            'filters'       => (object)$filters,
            'items'         => $data['items'],
            'pagination'    => $this->pagination->create_links(),
            'pag' => array(
                'start'         => $offset,
                'perPage'       => $perPage,
                'count'         => $data['count'],
                'itemsPerPage'  => array(10, 20, 50, 100, 200, 500),
            ),
            'list_taguri'           => TagTable::getInstance()->fetchForSelect(),

            // Template data
            'page_title' => 'Listă articole',
            'breadcrumb' => [
                [
                    'name' => 'Listă articole',
                ]
            ],
        );

        // active Menu
        array_push($this->activeMenu, 'list_articole');

        // Render view.
        $this->_render($this->viewPath . 'list', $this->defaultTemplate, $viewData);
    }

    public function add()
    {
        $this->edit();
    }

    /**
     * funtia de editare Articol
     *
     * @param mixed $id
     */
    public function edit($id = false)
    {
        ini_set('memory_limit', '2048M'); // or you could use 1G
        $m = new Mess();
        $id = $id ? (int)$id : null;

        // Get data by id
        if ($id) {
            if (!$objArticol = ArticolTable::getInstance()->load($id)) show_404();
        } else {
            $objArticol = new ArticolItem();
        }

        // selecteaza TAB activ + Submit Form
        $tabActiv = 'generale';
        switch (get('tab')) {
            default:
                $this->_tab_generale($objArticol);
                $tabActiv = 'generale';
                break;
        }

        // prepare user list for dropdown
        $useriObs = $this->aauth->list_users();
        foreach ($useriObs as $obj) {
            $list_useri[$obj->id] = $obj->username;
        }

        $filterCategorii = new ArticolCategoriiFilters();
        $filterCategorii->setIsPublished($filterCategorii::PUBLISHED_YES);
        $list_categorii = ArticolCategoriiTable::getInstance()->fetchAll($filterCategorii)['items'];

        $groups_categorii = [];
        /**
         * @var ArticolCategoriiItem $objCategorie
         */
        foreach ($list_categorii as $objCategorie) {
            if (is_null($objCategorie->getParentId())) {
                $groups_categorii[(int)$objCategorie->getId()]['nume'] = $objCategorie->getNume();
            } else {
                $groups_categorii[(int)$objCategorie->getParentId()]['items'][$objCategorie->getId()] = $objCategorie->getNume();
            }
        }

        // Fetch active tag list
        $filters = new TagFilters();
        $filters->setIsDeleted(false);
        $filters->setOrderBy([$filters::ORDER_BY_NAME_ASC]);
        $activeTagList = TagTable::getInstance()->fetchForSelect($filters);

        // Template data
        $viewData = array(
            'objArticol'       => $objArticol,
            'tab_activ'        => $tabActiv,
            'edit'             => ($id) ? (int)$id : FALSE,

            // selectoare
            'list_personaje'        => $this->m_personaje->fetchAll()['items'],
            'list_carti'            => CarteTable::getInstance()->fetchForSelect(),
            'list_publicatii'       => [], // TODO
            'groups_categorii'      => $groups_categorii,
            'list_taguri'           => $activeTagList,

            'list_useri'            => $list_useri,
            'list_autori'           => AutorTable::getInstance()->fetchForSelect(),
            'filters'               => new ArticolFilters(),

            // Template data
            'page_title' => $id ? 'Editează articol' : 'Adaugă articol',
            'breadcrumb' => array(
                array(
                    'name' => 'Lista articole',
                    'href' => '/articole/',
                ),
                array(
                    'name' => $id ? 'Editează articol' : 'Adaugă articol',
                )
            ),
        );

        // active Menu
        array_push($this->activeMenu, 'list_articole');

        $this->_render($this->viewPath . 'edit', $this->defaultTemplate, $viewData);
    }

    /**
     * TAB - informatii generale
     *
     * @param ArticolItem $objArticol
     * @throws Exception
     */
    public function _tab_generale($objArticol)
    {
        // Am dat de inteles ca este apelata aceasta functie numai cand este trimis un formular de tip POST.
        if (!post()) return;

        $fv = $this->form_validation;

        $this->load->helper('htmlpurifier');

        /**
         * TAB  Generale
         *
         */
        $this->form_validation->set_rules('titlu',               'Titlu articol',    'trim|required|strip_tags|xss_clean');
        $this->form_validation->set_rules('cat_id',              'Categorie',        'trim|required|strip_tags|xss_clean');
        $this->form_validation->set_rules('continut',            'Continut',         'required');
        // $this->form_validation->set_rules('continut',            'Continut',        'htmlpurifier[article]'); // sanitize
        $this->form_validation->set_rules('autor_id',            'Autor',          'trim|numeric|xss_clean');         // autor
        $this->form_validation->set_rules('personaj_id',         'Personaj',       'trim|numeric|xss_clean');         // personaj

        // surse
        $this->form_validation->set_rules('sursa_doc_carte_id',       'Carte',  'trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('sursa_doc_publicatie_id',  'Publicatie',  'trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('sursa_doc_start_page',     'Pag start',  'trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('sursa_doc_end_page',       'Pag end',  'trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('sursa_titlu',              'Titlu sursa',  'trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('sursa_link',               'Link sursa online', 'trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('sursa_culegator',          'Sursa culegator', 'trim|strip_tags|xss_clean');

        $this->form_validation->set_rules('cuvinteCheie[]',           'Cuvinte cheie', 'trim|xss_clean');

        $this->form_validation->set_rules('order_id',                 'Order ID',       'trim|xss_clean');
        $this->form_validation->set_rules('data_publicare',           'Data publicarii', 'trim|xss_clean');
        $this->form_validation->set_rules('is_published',             'Articol publicat', 'trim|xss_clean');
        $this->form_validation->set_rules('is_deleted',               'Articol ștearsă', 'trim|xss_clean');

        $articleContent = post('continut');
        //$articleContent = html_purify(post('continut', false), 'article'); // get all input unmodified by xss from CI 

        if ($fv->run()) {

            if ($this->input->is_ajax_request()) {

                // setters
                $objArticol->setTitlu(post('titlu'));
                $objArticol->setCatId(post('cat_id'));
                $objArticol->setAutorId(post('autor_id'));
                $objArticol->setPersonajId(post('personaj_id'));
                $objArticol->setContinut($articleContent);

                $objArticol->setSursaDocCarteId(post('sursa_doc_carte_id'));
                $objArticol->setSursaDocPublicatieId(post('sursa_doc_publicatie_id'));
                $objArticol->setSursaDocStartPage(post('sursa_doc_start_page'));
                $objArticol->setSursaDocEndPage(post('sursa_doc_end_page'));
                $objArticol->setSursaTitlu(post('sursa_titlu'));
                $objArticol->setSursaLink(post('sursa_link'));
                $objArticol->setSursaCulegator(post('sursa_culegator'));

                $objArticol->setIsPublished(post('is_published') == ArticolFilters::PUBLISHED_YES ? 1 : 0);
                $objArticol->setIsDeleted((int)post('is_deleted'));
                $objArticol->setOrderId(post('order_id'));

                $dataPublicare = null;
                if (!empty(post('data_publicare'))) {
                    if (!Calendar::checkDate(post('data_publicare'), '/')) {
                        $this->mess->displayErrors(['data_publicare' => 'Data de publicare este invalida']);
                    } else {
                        $dataPublicare = Calendar::strToDateTime(post('data_publicare'));
                    }
                }
                $objArticol->setDataPublicare($dataPublicare);

                $objArticol->setSeoMetadescription(post('seo_metadescription'));
                $objArticol->setSeoMetakeywords(post('seo_metakeywords'));

                $objArticol->setUserId($this->aauth->get_user_id());
                $objArticol->setArrTagKeys(post('cuvinteCheie'));

                if (ArticolTable::getInstance()->save($objArticol)) {
                    $this->mess->displaySuccess(
                        'Articolul a fost salvata cu succes!',
                        ['redirect_url' => '/articole/edit/' . $objArticol->getId()]
                    );
                }
            }
        } else {

            if ($this->input->is_ajax_request()) {
                $allErrors = $fv->error_array();
                echo json_encode(array('error' => $allErrors));
                exit;
            }
        }
    }
}
