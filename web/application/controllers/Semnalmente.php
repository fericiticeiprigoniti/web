<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Semnalmente extends Admin_Controller
{

    private $viewPath = 'admin/generale/semnalmente/';

    /**
     * @var $mess Mess
     */
    public $mess;

    /**
     *
     * @var $m_semnalmente_corporale M_semnalmente_corporale
     */
    public $m_semnalmente_corporale;

    function __construct()
    {
        parent::__construct();

        $this->load->library('pagination');
        $this->load->helper(array('form', 'url'));
        $this->load->model('m_semnalmente_corporale');
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Listare semnalmente corporale
     */
    public function index()
    {
        // Get data.
        $data = $this->m_semnalmente_corporale->fetchList();

        // Set data.
        $viewData = array(
            'items' => $data,

            // Template data
            'page_title' => 'Lista semnalmente corporale',
            'breadcrumb' => array(
                array(
                    'name' => 'Lista semnalmente corporale',
                ),
            ),
        );

        // Render view.
        $this->_render($this->viewPath . 'list', $this->defaultTemplate, $viewData);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Adauga semnalment corporal nou
     *
     */
    public function add()
    {
        $this->edit();
    }

    /**
     * Editeaza semnalment corporal
     * @param int|bool $id
     */
    function edit($id = null)
    {
        $this->data['edit'] = $id ? (int)$id : null;

        // Get data by id
        if ($id) {
            $data = $this->m_semnalmente_corporale->load($id);
        } else {
            $data = array(
                'nume' => null,
                'detalii' => null,
            );
        }
        $this->data['data'] = $data;

        // ------------------------------

        // Parse post data
        if (post()) {

            /** @var CI_Form_validation $fv */
            $fv = $this->form_validation;

            // REGULI
            $rules_config = array(
                array(
                    'field'     => 'nume',
                    'label'     => 'Nume',
                    'rules'     => 'trim|required|min_length[3]|xss_clean|strip_tags'
                ),
            );

            $fv->set_rules($rules_config);

            if ($fv->run()) {

                // Verifica unicitatea numelui
                $res = $this->m_semnalmente_corporale->fetchByName(post('nume'), $id);
                if ($res && count($res)) {
                    $this->mess->displayErrors(array('nume' => 'Semnalmentul nu poate fi salvat. Exista un alt semnalment corporal cu aceeasi denumire'));
                }

                try {
                    $this->m_semnalmente_corporale->save(post(), $id);
                } catch (Exception $e) {
                    $this->mess->displayErrors(array('nume' => 'Datele nu pot fi salvate in baza de date -> ' . $e->getMessage()));
                }


                if ($this->input->is_ajax_request()) {
                    $this->mess->displaySuccess('Datele au fost salvate cu succes');
                }
            } else {

                if ($this->input->is_ajax_request()) {
                    $allErrors = $fv->error_array();
                    echo json_encode(array('error' => $allErrors));
                    exit;
                }
            }
        }

        // ------------------------------

        // Template data
        $this->data['page_title'] = $id ? 'Editează semnalment corporal' : 'Adaugă semnalment corporal';
        $this->data['breadcrumb'] = array(
            array(
                'name' => 'Lista semnalmente corporale',
                'href' => '/semnalmente/',
            ),
            array(
                'name' => $id ? 'Editează semnalment corporal' : 'Adaugă semnalment corporal',
            ),
        );

        $this->_render($this->viewPath . 'edit', $this->defaultTemplate);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Sterge semnalment corporal
     * @param int $id
     */
    function delete($id)
    {
        $m = new Mess();

        $id = (int)$id;
        if (!$id) {
            $m->displayErrors("ID semnalment corporal invalid");
        }

        if (!post('submited')) {
            $m->displayErrors("Formular invalid");
        }

        $data = $this->m_semnalmente_corporale->load($id);
        if (!$data || !count($data)) {
            $m->displayErrors("Semnalmentul nu a fost gasit in baza de date");
        }

        // Verifica daca semnalmentul este folosit deja.
        if ($data['is_used']) {
            $m->displayErrors("Semnalmentul nu poate fi sters. Este folosit in relatia cu un personaj");
        }

        // Delete data.
        try {
            $this->m_semnalmente_corporale->delete($id);
            $m->displaySuccess(true);
        } catch (Exception $e) {
            $m->displayErrors('Nu am putut sterge semnalmentul din baza de date -> ' . $e->getMessage());
        }
    }

    // ---------------------------------------------------------------------------------------------



}
