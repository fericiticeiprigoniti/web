<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Locuri_patimire extends Admin_Controller
{

    private $viewPath = 'admin/generale/locuri_patimire/';

    /**
     * @var $mess Mess
     */
    public $mess;

    /**
     * @var $m_locuri_patimire M_locuri_patimire
     */
    public $m_locuri_patimire;

    /**
     * @var $m_locuri_patimire_ M_locuri_patimire_tip
     */
    public $m_locuri_patimire_tip;

    /**
     * @var M_localitati $m_localitati
     */
    public $m_localitati;

    /**
     * @var M_locuri_patimire_citate $m_locuri_patimire_citate
     */
    public $m_locuri_patimire_citate;

    /**
     * @var M_locuri_patimire_comandanti $m_locuri_patimire_comandanti
     */
    public $m_locuri_patimire_comandanti;

    /**
     * @var M_articole $m_articole
     */
    public $m_articole;

    /**
     * @var M_personaje $m_personaje
     */
    public $m_personaje;

    /**
     * @var M_grade_militare $m_grade_militare
     */
    public $m_grade_militare;

    function __construct()
    {
        parent::__construct();

        $this->load->library('pagination');

        $this->load->model('m_locuri_patimire');
        $this->load->model('m_locuri_patimire_tip');
        $this->load->model('m_localitati');
        $this->load->model('m_locuri_patimire_citate');
        $this->load->model('m_articole');
        $this->load->model('m_personaje');
        $this->load->model('m_grade_militare');
        $this->load->model('m_locuri_patimire_comandanti');
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Listare categorii
     */
    public function index()
    {
        // Filters.
        $filters = array(
            'status' => isset($_GET['status']) ? trim(strip_tags($_GET['status'])) : null,
        );

        // Check if there are filters set
        $hasAnyFilter = false;
        foreach (get() as $key => $value) {
            if (!is_null($value)) {
                $hasAnyFilter = true;
            }
        }

        // Order.
        $order = array(
            'by' => isset($_GET['_order_by']) ? trim(strip_tags($_GET['_order_by'])) : null,
            'dir' => isset($_GET['_order_dir']) ? trim(strip_tags($_GET['_order_dir'])) : null,
        );

        // Limit.
        $page = isset($_GET['_page']) ? (int)$_GET['_page'] : 1;
        $perPage = isset($_GET['_per_page']) ? (int)$_GET['_per_page'] : 15;
        $limit = array(
            'start' => ($page - 1) * $perPage,
            'per_page' => $perPage,
        );

        // Get data
        // set default
        if (is_null($order['by'])) $order['by'] = 'locatie_nume';
        if (is_null($order['dir'])) $order['dir'] = 'ASC';
        $data = $this->m_locuri_patimire->fetchAll($filters, $order, $limit);
        if (!isset($data['items']) && !isset($data['count'])) {
            die('Nu am putut prelua informatiile din baza de date');
        }

        // Initialize pagination.
        $pConfig = $this->config->item('pagination');
        $pConfig['base_url'] = General::url(array('_page' => null));
        $pConfig['total_rows'] = $data['count'];
        $pConfig['cur_page'] = $page;
        $pConfig['per_page'] = $perPage;
        $this->pagination->initialize($pConfig);

        // Set data.
        $viewData = array(
            'items' => $data['items'],
            'order' => (object)$order,
            'pagination' => $this->pagination->create_links(),
            'pag' => array(
                'start' => $limit['start'],
                'perPage' => $perPage,
                'count' => $data['count'],
                'itemsPerPage' => array(10, 15, 50, 100, 200, 500),
            ),
            'hasAnyFilter' => $hasAnyFilter,

            // Template data
            'page_title' => 'Locuri patimire',
            'breadcrumb' => array(
                array(
                    'name' => 'Locuri patimire',
                ),
            ),
        );

        // Render view.
        $this->_render($this->viewPath . 'list', $this->defaultTemplate, $viewData);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Adauga categorie noua
     *
     */
    public function add()
    {
        $this->edit();
    }

    /**
     * Editeaza categorie
     * @param int|bool $id
     */
    function edit($id = null)
    {
        $this->data['edit'] = $edit = $id ? (int)$id : null;

        // Get data by id
        if ($id) {
            $data = $this->m_locuri_patimire->load($id);
        } else {
            $data = [
                'tip_surghiun_id' => null,
                'locatie_nume' => null,
                'locatie_supranume' => null,
                'locatie_cod_unitate' => null,
                'destinatie' => null,
                'localitate_id' => null,
                'localitate_nume' => null,
                'locatie_coord_lat' => null,
                'locatie_coord_long' => null,
                'descriere_art_id' => null,
            ];
        }

        $this->data['data'] = $data;
        $this->data['tip_surghiun_list']        = $this->m_locuri_patimire_tip->fetchAll(true);
        $this->data['descriere_articol_list']   = $this->m_articole->fetchForSelect();
        $this->data['list_locuri_patimire']     = $this->m_locuri_patimire->fetchForSelect();
        $this->data['list_grade_militare']      = $this->m_grade_militare->fetchForSelect();
        $this->data['list_carti']               = CarteTable::getInstance()->fetchForSelect();
        $this->data['list_useri']               = $this->aauth->list_users();
        $this->data['descriere']                = !empty($data['descriere_art_id']) ? $this->m_locuri_patimire->getDescriere($data['descriere_art_id']) : null;

        $locuriPatimireModel = $this->m_locuri_patimire_comandanti;

        $filtersPersonaje = array("rol" => $locuriPatimireModel::ROL_COMANDANT_UNITATE_DETENTIE);
        $personaje = $this->m_personaje->fetchAll($filtersPersonaje);
        $list_comandanti = array();
        foreach ($personaje['items'] as $personaj) {
            $list_comandanti[$personaj['id']] = $personaj['nume_complet'];
        }

        $this->data['list_comandanti'] = $list_comandanti;


        // selecteaza TAB activ + Submit Form
        $activeTab = get('tab');
        switch ($activeTab) {
            case 'generale':
                $this->_tab_generale($id);
                break;
            case 'descriere':
                $this->_tab_descriere($id);
                break;
            case 'comandanti_penitenciare':
                $this->_tab_comandanti_penitenciare($id);
                break;
            case 'citate':
                $this->_tab_citate($id);
                break;
            default:
                $this->_tab_generale($id);
                break;
        }
        $this->data['tab_activ'] = empty($activeTab) ? 'generale' : $activeTab;


        // populeaza tabelele cu date din fiecare TAB
        if ($id) {
            // TAB - citate
            $info['citate']      = $this->m_locuri_patimire_citate->fetchAll($id);
            $info['locuri_patimire_comandanti']      = $this->m_locuri_patimire_comandanti->fetchAll($id);
            $this->data['info'] = $info;
        }

        // ------------------------------

        // Template data
        $this->data['page_title'] = $edit ? 'Editează loc patimire' : 'Adaugă loc patimire';
        $this->data['page_title'] = $edit ? 'Editează loc patimire' : 'Adaugă loc patimire';
        $this->data['breadcrumb'] = array(
            array(
                'name' => 'Lista locuri patimire',
                'href' => '/locuri_patimire/',
            ),
            array(
                'name' => $edit ? 'Editează loc patimire' : 'Adaugă loc patimire',
            ),
        );

        $this->_render($this->viewPath . 'edit', $this->defaultTemplate);
    }

    // ---------------------------------------------------------------------------------------------

    public function _tab_generale($id)
    {
        // Am dat de inteles ca este apelata aceasta functie numai cand este trimis un formular de tip POST.
        if (!post()) return false;

        // REGULI

        /** @var CI_Form_validation $fv */
        $fv = $this->form_validation;

        // REGULI
        $rules_config = array(
            array(
                'field'     => 'tip_surghiun_id',
                'label'     => 'Tip surghiun',
                'rules'     => 'trim|required|xss_clean|is_natural|strip_tags'
            ),
            array(
                'field'     => 'locatie_nume',
                'label'     => 'Nume locatie',
                'rules'     => 'trim|required|min_length[3]|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'locatie_supranume',
                'label'     => 'Supranume locatie',
                'rules'     => 'trim|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'locatie_cod_unitate',
                'label'     => 'Cod unitate locatie',
                'rules'     => 'trim|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'destinatie',
                'label'     => 'Destinatie',
                'rules'     => 'trim|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'localitate_id',
                'label'     => 'Nume localitate',
                'rules'     => 'trim|required|xss_clean|is_natural|strip_tags'
            ),
            array(
                'field'     => 'locatie_coord_lat',
                'label'     => 'Coordonate locatie (Lat)',
                'rules'     => 'trim|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'locatie_coord_long',
                'label'     => 'Coordonate locatie (Long)',
                'rules'     => 'trim|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'descriere_art_id',
                'label'     => 'Titlu articol descriere (istoric)',
                'rules'     => 'trim|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'is_deleted',
                'label'     => 'Sters',
                'rules'     => 'trim|xss_clean|strip_tags|in_list[0,1]'
            ),
        );

        $fv->set_rules($rules_config);

        if ($fv->run()) {

            try {
                $id = $this->m_locuri_patimire->save(post(), $id);
            } catch (Exception $e) {
                $this->mess->displayErrors(array('nume' => 'Datele nu pot fi salvate in baza de date -> ' . $e->getMessage()));
            }

            if ($this->input->is_ajax_request()) {
                $this->mess->displaySuccess(
                    'Datele au fost salvate cu succes',
                    ['redirect_url' => '/locuri_patimire/edit/' . $id]
                );
            }
        } else {

            if ($this->input->is_ajax_request()) {
                $allErrors = $fv->error_array();
                echo json_encode(array('error' => $allErrors));
                exit;
            }
        }

        return false;
    }

    // ---------------------------------------------------------------------------------------------


    /**
     * @param int $locPatimireId
     * @throws Exception
     */
    public function _tab_descriere($locPatimireId)
    {
        $locPatimire = $this->m_locuri_patimire->load($locPatimireId);
        $objDescriere = $this->m_locuri_patimire->getDescriere($locPatimire['descriere_art_id']);
        $objDescriere = !is_null($objDescriere) ? $objDescriere : new ArticolItem();

        if ($this->input->is_ajax_request()) {
            if (post()) {
                $fv = $this->form_validation;

                $fv->set_rules('titlu', 'Continut', 'trim|required');
                $fv->set_rules('continut', 'Continut', 'trim|required');
                $fv->set_rules('carte_id', 'Carte', 'trim|xss_clean|strip_tags');
                $fv->set_rules('sursa_link', 'Sursa link', 'trim|xss_clean|strip_tags');
                $fv->set_rules('sursa_titlu', 'Sursa titlu', 'trim|xss_clean|strip_tags');
                $fv->set_rules('pag_start', 'Pag start', 'trim|is_natural|xss_clean|strip_tags');
                $fv->set_rules('pag_end', 'Pag end', 'trim|is_natural|xss_clean|strip_tags');
                $fv->set_rules('user_id', 'User', 'trim|is_natural|xss_clean|strip_tags');

                $fv->set_rules('status_id', 'Status', 'trim|xss_clean|strip_tags');
                $fv->set_rules('data_publicare', 'Data publicare', 'trim|xss_clean|strip_tags');

                if ($fv->run() == TRUE) {

                    // TODO - change hardcoded values
                    $objDescriere->setCatId(100); // HARDCODED
                    $objDescriere->setTitlu(post('titlu'));
                    $objDescriere->setContinut(post('continut'));
                    $objDescriere->setSursaDocCarteId((int)post('carte_id'));
                    $objDescriere->setSursatitlu(post('sursa_titlu'));
                    $objDescriere->setSursalink(post('sursa_link'));
                    $objDescriere->setSursaDocStartPage((int)post('pag_start'));
                    $objDescriere->setSursaDocEndPage((int)post('pag_end'));
                    $objDescriere->setUserId((int)post('user_id'));
                    $objDescriere->setIsPublished((int)post('status_id'));

                    $dataPublicare = null;
                    if (!empty(post('data_publicare'))) {
                        if (!Calendar::checkDate(post('data_publicare'), '/')) {
                            $this->mess->displayErrors(['data_publicare' => 'Data de publicare este invalida']);
                        } else {
                            $dataPublicare = Calendar::strToDateTime(post('data_publicare'));
                        }
                    }
                    print_r($dataPublicare = Calendar::strToDateTime(post('data_publicare')));
                    $objDescriere->setDataPublicare($dataPublicare);

                    $descriere_id = ArticolTable::getInstance()->save($objDescriere);

                    // update Personaj descriere ID
                    if (is_int($descriere_id)) {
                        $locPatimire['descriere_art_id'] = $descriere_id;
                        $this->m_locuri_patimire->save($locPatimire, $locPatimire['id']);
                    }
                } else {

                    if ($this->input->is_ajax_request()) {
                        $allErrors = $fv->error_array();
                        echo json_encode(array('error' => $allErrors));
                        exit;
                    }
                }

                return;
            }
        }
    }

    // ---------------------------------------------------------------------------------------------

    public function _tab_comandanti_penitenciare($loc_patimire_id)
    {
        $m = new Mess();

        if (post()) {
            $fv = $this->form_validation;

            $new_rows_comandanti = array();

            foreach (post() as $key => $item) {

                // get Comandanti
                // ==================
                if (preg_match('/^new_comandant_comandant_id_([0-9]*)/', $key, $matches)) {
                    $fv->set_rules($key, 'Comandant', 'trim|required|xss_clean');
                    $new_rows_comandanti[$matches[1]]['comandant_id'] = $item;
                }

                if (preg_match('/^new_comandant_grad_militar_id_([0-9]*)/', $key, $matches)) {
                    $fv->set_rules($key, 'Grad militar', 'trim|xss_clean');
                    $new_rows_comandanti[$matches[1]]['grad_id'] = $item;
                }

                // parse date time
                foreach (array('zi', 'luna', 'an') as $time_interval) {
                    // get data start
                    if (preg_match('/^new_comandant_data_start_' . $time_interval . '_([0-9]*)/', $key, $matches)) {
                        $fv->set_rules($key, 'Data ' . $time_interval, 'trim|xss_clean|is_natural' . (($time_interval == 'an') ? '|callback__validate_date[new_comandant_data_start_,' . $matches[1] . ']' : ''));
                        $new_rows_comandanti[$matches[1]]['data_start_' . $time_interval] = $item;
                    }

                    // get data end
                    if (preg_match('/^new_comandant_data_end_' . $time_interval . '_([0-9]*)/', $key, $matches)) {
                        $fv->set_rules($key, 'Data ' . $time_interval, 'trim|xss_clean|is_natural' . (($time_interval == 'an') ? '|callback__validate_date[new_comandant_data_end_,' . $matches[1] . ']' : ''));
                        $new_rows_comandanti[$matches[1]]['data_end_' . $time_interval] = $item;
                    }
                }
            }

            $this->data['new_rows_comandanti']  = $new_rows_comandanti;

            if ($fv->run() == TRUE) {

                // insert Comandanti in DB
                if ($new_rows_comandanti) {

                    foreach ($new_rows_comandanti as $item) {

                        $fields = array(
                            'personaj_id' => (int)$item['comandant_id'],
                            'grad_id' => (int)$item['grad_id'],
                            'loc_patimire_id' => (int)$loc_patimire_id,
                            'data_start' => Calendar::validateDate(array($item['data_start_zi'], $item['data_start_luna'], $item['data_start_an'])),
                            'data_end' => Calendar::validateDate(array($item['data_end_zi'], $item['data_end_luna'], $item['data_end_an'])),
                        );

                        $this->m_locuri_patimire_comandanti->save($fields);
                    }
                }

                // clear arrays
                $this->data['new_rows_comandanti'] = array();

                if ($this->input->is_ajax_request()) {
                    $m->displaySuccess('Comandanții de penitenciar au fost salvati cu succes');
                }
            } else {
                if ($this->input->is_ajax_request()) {
                    if ($allErrors = $fv->error_array()) {
                        echo json_encode(array('error' => $allErrors));
                    } else {
                        echo json_encode(array('error' => 'Nicio modificare'));
                    }
                    exit;
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------------


    /**
     * TAB - CITATE - citate atribuite locului de patimire
     *
     * @param mixed $id
     */
    function _tab_citate($id)
    {
        if ($citatID = get('citatID')) {
            $this->data['info_citat'] = $this->m_locuri_patimire_citate->getInfo($citatID);
            $this->data['citatID'] = (int)$citatID;
        }

        if ($this->input->is_ajax_request()) {
            if (post()) {
                $fv = $this->form_validation;

                $fv->set_rules('content', 'Continut',    'trim|required');
                $fv->set_rules('carte_id', 'Carte',       'trim|xss_clean|strip_tags');
                $fv->set_rules('sursa',     'Sursa',     'trim|xss_clean|strip_tags');
                $fv->set_rules('pag_start', 'Pag start',  'trim|is_natural|xss_clean|strip_tags');
                $fv->set_rules('pag_end', 'Pag end',      'trim|is_natural|xss_clean|strip_tags');

                if ($fv->run() == TRUE) {
                    $fields = array(
                        'content'      => post('content'),
                        'carte_id'      => !empty(post('carte_id')) ? post('carte_id') : null,
                        'sursa'         => !empty(post('sursa')) ? post('sursa') : null,
                        'pag_start'     => !empty(post('pag_start')) ? post('pag_start') : null,
                        'pag_end'       => !empty(post('pag_end')) ? post('pag_end') : null
                    );

                    if ($citatID) {
                        // update citat
                        if ($result = $this->m_locuri_patimire_citate->update($citatID, $fields)) {
                            $this->mess->displaySuccess('Citatul a fost actualizat');
                        }
                    } else {
                        if ($result = $this->m_locuri_patimire_citate->add((int)$id, $fields)) {
                            $this->mess->displaySuccess('Citatul a fost adăugat');
                        }
                    }
                } else {
                    $allErrors = $fv->error_array();
                    echo json_encode(array('error' => $allErrors));
                    exit;
                }
            }
        }
    }


    // ---------------------------------------------------------------------------------------------

    function stergeCitat($locId, $citatId)
    {
        $m = new Mess();

        if (empty($locId) || empty($citatId)) {
            $m->addError("Loc patimire sau citatul invalid");
        }

        if ($m->hasErrors()) {
            $m->displayErrors();
        }

        if ($this->m_locuri_patimire_citate->delete($locId, $citatId)) {
            $m->displaySuccess(true);
        } else {
            $m->displayErrors('Actiunea nu s-a putut realiza');
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Sterge categorie
     * @param int $id
     */
    function delete($id)
    {
        $m = new Mess();
        $id = (int)$id;
        if (!$id)
            $m->displayErrors("ID loc patimire invalid");

        if (!post('submited'))
            $m->displayErrors("Formular invalid");

        $data = $this->m_locuri_patimire->load($id);
        if (!$data || !count($data))
            $m->displayErrors("Locul de patimire nu a fost gasit in baza de date");

        // Verifica daca exista articole de descriere
        /*if ($data['is_used']) {
            $m->displayErrors("Categoria nu poate fi stearsa. Exista articole catre care este asignata.");
        }*/

        // Delete data.
        try {
            $this->m_locuri_patimire->delete($id);
            $m->displaySuccess(true);
        } catch (Exception $e) {
            $m->displayErrors('Nu am putut sterge locul de patimire din baza de date -> ' . $e->getMessage());
        }
    }

    // ---------------------------------------------------------------------------------------------


    public function cautaLocalitate()
    {
        $searchTerm = isset($_GET['query']) ? trim(strip_tags($_GET['query'])) : null;
        if (!$searchTerm) {
            $this->displayAutocompleteResults($searchTerm, array());
        }

        // Initialize data.
        $results = array();

        // Fetch data.
        $data = $this->m_localitati->fetchForAutocomplete($searchTerm);
        if ($data && count($data)) {
            foreach ($data as $k => $v) {
                $results[] = array(
                    'value' => $v['nume_complet'],
                    'id' => $v['id'],
                    'data' => array('category' => 'Localitate'),
                );
            }
        }

        // Return results.
        $this->displayAutocompleteResults($searchTerm, $results);
    }

    // ---------------------------------------------------------------------------------------------

    public function cautaPersonaj()
    {
        $searchTerm = isset($_GET['query']) ? trim(strip_tags($_GET['query'])) : null;
        if (!$searchTerm) {
            $this->displayAutocompleteResults($searchTerm, array());
        }

        // Initialize data.
        $results = array();

        $locuriPatimireModel = $this->m_locuri_patimire_comandanti;

        // Fetch data.
        $filters = array("rol_id" => $locuriPatimireModel::ROL_COMANDANT_UNITATE_DETENTIE);
        $data = $this->m_personaje->fetchForAutocomplete($searchTerm, $filters);
        if ($data && count($data)) {
            foreach ($data as $k => $v) {
                $results[] = array(
                    'value' => $v['nume_complet'],
                    'id'    => $v['id'],
                    'data'  => array('category' => 'Nume Prenume'),
                );
            }
        }

        // Return results.
        $this->displayAutocompleteResults($searchTerm, $results);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * functie apelata prin AJAX pentru stergerea unui comandant
     *
     * @param mixed $comandantId
     */
    public function stergeComandant($comandantId)
    {
        $m = new Mess();

        if (empty($comandantId)) {
            $m->addError("Id comandant invalid");
        }

        if ($m->hasErrors()) {
            $m->displayErrors();
        }

        // Save data.
        if ($this->m_locuri_patimire_comandanti->delete($comandantId)) {
            $m->displaySuccess(true);
        } else {
            $m->displayErrors('Actiunea nu s-a putut realiza');
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * callback function - used in form validation
     * check the valability of a partial date
     *
     * @param mixed $str
     * @param mixed $param (pattern + row number)
     * @return bool
     */
    public function _validate_date($str, $param)
    {
        $param = explode(',', $param);

        $pattern = $param[0];
        $row_id  = $param[1];

        // if date is empty - skip it
        $zi     = post($pattern . '_zi_' . $row_id);
        $luna   = post($pattern . '_luna_' . $row_id);
        $an     = post($pattern . '_an_' . $row_id);
        if (empty($zi) && empty($luna) && empty($an)) return true;

        if (!Calendar::validateDate(array(post($pattern . '_zi_' . $row_id), post($pattern . '_luna_' . $row_id), post($pattern . '_an_' . $row_id)))) {
            $this->form_validation->set_message('_validate_date', 'Data este invalidă.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    // ---------------------------------------------------------------------------------------------



}
