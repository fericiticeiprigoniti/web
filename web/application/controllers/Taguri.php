<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Taguri extends Admin_Controller
{

    private $viewPath = 'admin/generale/taguri/';

    /**
     * @var $mess Mess
     */
    public $mess;

    function __construct()
    {
        parent::__construct();
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Listare Taguri
     */
    public function index()
    {
        // Filters.
        $getData = $_GET;
        $filters = new TagFilters($getData);

        // Set page, perPage and offset
        $page = get('_page') ? (int)get('_page') : 1;
        $perPage = get('_per_page') ? (int)get('_per_page') : 50;
        $offset = ($page - 1) * $perPage;

        // Fetch data
        $data = TagTable::getInstance()->fetchAll($filters, $perPage, $offset);

        // Get data.
        if (!isset($data['items']) && !isset($data['count'])) {
            show_error('Nu am putut prelua informatiile din baza de date');
        }

        // Initialize pagination.
        $pConfig = $this->config->item('pagination');
        $pConfig['base_url'] = General::url(array('_page' => null));
        $pConfig['total_rows'] = $data['count'];
        $pConfig['cur_page'] = $page;
        $pConfig['per_page'] = $perPage;
        $this->pagination->initialize($pConfig);

        // Set data.
        $viewData = array(
            'items' => $data['items'],
            'nom_cat_tags' => unserialize(CAT_TAGS),
            'pagination' => $this->pagination->create_links(),
            'pag' => array(
                'start'   => $offset,
                'perPage' => $perPage,
                'count'   => $data['count'],
                'itemsPerPage' => array(10, 20, 50, 100, 200, 500),
            ),

            // Template data
            'page_title' => 'Lista cuvinte cheie',
            'breadcrumb' => array(
                array(
                    'name' => 'Lista cuvinte cheie',
                ),
            ),
        );

        // Render view.
        $this->_render($this->viewPath . 'list', $this->defaultTemplate, $viewData);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Adauga taguri noi
     *
     * @throws Exception
     */
    public function add()
    {
        $this->edit();
    }

    /**
     * Editeaza tag
     * @param int|bool $id
     * @throws Exception
     */
    function edit($id = null)
    {
        $this->data['edit'] = $id ? (int)$id : null;

        $id = $id ? (int)$id : null;

        // Get data by id
        $objTag = $id ? TagTable::getInstance()->load($id) : new TagItem();

        // ------------------------------

        // Parse post data
        if (post()) {
            /** @var CI_Form_validation $fv */
            $fv = $this->form_validation;

            // REGULI
            $rules_config = array(
                array(
                    'field'     => 'nume',
                    'label'     => 'Nume',
                    'rules'     => 'trim|required|min_length[2]|xss_clean|strip_tags'
                ),
                array(
                    'field'     => 'personaj_id',
                    'label'     => 'Personaj',
                    'rules'     => 'trim|numeric|xss_clean|strip_tags'
                ),
                array(
                    'field'     => 'cat_id',
                    'label'     => 'Categorie',
                    'rules'     => 'trim|xss_clean|strip_tags'
                ),
                array(
                    'field'     => 'observatii',
                    'label'     => 'Observatii',
                    'rules'     => 'trim|xss_clean|strip_tags'
                ),
                array(
                    'field'     => 'is_favorit',
                    'label'     => 'Favorit',
                    'rules'     => 'trim|is_numeric|xss_clean|strip_tags'
                ),
            );

            $fv->set_rules($rules_config);

            if ($fv->run()) {
                // Verifica unicitatea numelui
                if (!$objTag->getId()) {
                    $filters = new TagFilters();
                    $filters->setNume(post('nume'));

                    $results = TagTable::getInstance()->fetchAll($filters);

                    if ($results['count']) {
                        $this->mess->displayErrors(array('nume' => 'Cuvantul cheie nu poate fi salvat. Este existent deja in baza de date'));
                    }
                }

                // setter
                $objTag->setNume(post('nume'));
                $objTag->setCatId(post('cat_id'));
                $objTag->setPersonajId(post('personaj_id'));
                $objTag->setObservatii(post('observatii'));
                $objTag->setIsFavorit(post('is_favorit'));

                try {
                    TagTable::getInstance()->save($objTag);
                } catch (Exception $e) {
                    $this->mess->displayErrors(array('nume' => 'Datele nu pot fi salvate in baza de date -> ' . $e->getMessage()));
                }

                if ($this->input->is_ajax_request()) {
                    $this->mess->displaySuccess('Datele au fost salvate cu succes');
                }
            } else {

                if ($this->input->is_ajax_request()) {
                    $allErrors = $fv->error_array();
                    $this->mess->displayErrors(array('error' => $allErrors));
                }
            }
        }

        // Set filters
        $filters = new PersonajFilters();
        $filters->setOrderBy([$filters::ORDER_BY_NAME_ASC]);
        $list_personaje = PersonajTable::getInstance()->fetchForSelect($filters);

        // ------------------------------


        // Template data
        $viewData = array(
            'objItem'      => $objTag,
            'nom_cat_tags' => unserialize(CAT_TAGS),
            'list_personaje' => $list_personaje,

            // Template data
            'page_title' => $id ? 'Editează tag' : 'Adaugă tag',
            'breadcrumb' => [
                [
                    'name' => 'Lista taguri',
                    'href' => '/taguri/',
                ],
                [
                    'name' => $id ? 'Editează tag' : 'Adaugă tag',
                ]
            ],
        );

        $this->_render($this->viewPath . 'edit', $this->defaultTemplate, $viewData);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Sterge cuvant cheie
     * @param int $id
     * @throws Exception
     */
    function delete($id)
    {
        $response = array(
            'success' => FALSE
        );

        $m = new Mess();

        $id = (int)$id;
        if (!$id) {
            $m->displayErrors("ID categorie invalid");
        }

        if (!post('submited')) {
            $m->displayErrors("Formular invalid");
        }

        $item = TagTable::getInstance()->load($id);
        if (!$item) {
            $m->displayErrors("Tagul nu a fost gasita in baza de date");
        }

        // TODO - de verificat daca tagul este folosit
        // 1. articole
        // 2. personaje
        // 3. poezii

        // Delete data.
        try {
            if (TagTable::getInstance()->delete($item))
                $response['success'] = TRUE;
        } catch (Exception $e) {
            $m->displayErrors('Nu am putut sterge tag-ul din baza de date -> ' . $e->getMessage());
        }

        echo json_encode($response);
        die();
    }

    // ---------------------------------------------------------------------------------------------

    # TODO
    /**
     * functe apelata prin AJAX pentru afisarea tagurilor in select2
     *
     */
    function getTags()
    {
        $q = ($this->input->get()) ? $this->input->get('q') : FALSE;

        $response = array(
            'success' => FALSE
        );

        if ($response['tags'] = $this->m_taguri->search($q)) {
            $response['success'] = TRUE;
        }

        echo json_encode($response);
        die();
    }
}
