<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Edituri extends Admin_Controller
{

    private $viewPath = 'admin/biblioteca/edituri/';

    /**
     * @var $mess Mess
     */
    public $mess;


    public function __construct()
    {
        parent::__construct();

        $this->load->helper(array('form', 'url'));

        $this->load->model('m_autoload');

        /**
         * Toate modelele care tin de Biblioteca si Nomenclatoare sunt incarcate in mod automat,
         * doar atunci cand sunt apelate/initializate.
         * Vezi fisierul config/hooks.php
         */
    }

    // ---------------------------------------------------------------------------------------------

    public function index()
    {
        // Fetch data
        $filters = new EdituraFilters();
        $filters->setIsDeleted(0);
        $data = EdituraTable::getInstance()->fetchAll($filters);

        // Get data.
        if (!isset($data['items']) && !isset($data['count'])) {
            show_error('Nu am putut prelua informatiile din baza de date');
        }

        // Set data.
        $viewData = array(
            'items' => $data['items'],

            // Template data
            'page_title' => 'Listă edituri',
            'breadcrumb' => array(
                array(
                    'name' => 'Edituri',
                ),
            ),
        );

        // Render view.
        $this->_render($this->viewPath . 'list', $this->defaultTemplate, $viewData);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Adauga editura noua
     */
    public function add()
    {
        $this->edit();
    }

    /**
     * Editeaza editura
     * @param int|bool $id
     */
    function edit($id = null)
    {
        $id = $id ? (int)$id : null;

        // Get data by id
        $editura = $id ? EdituraTable::getInstance()->load($id) : new EdituraItem();

        // Parse post data
        if (post()) {

            if (empty($_POST['alias'])) {
                $_POST['alias'] = str_replace(" ", "_", strtolower($_POST['nume']));
            }

            $fv = $this->form_validation;

            // REGULI
            $rules_config = [
                [
                    'field'     => 'nume',
                    'label'     => 'Nume',
                    'rules'     => 'trim|required|min_length[3]|xss_clean|strip_tags'
                ],
                [
                    'field'     => 'alias',
                    'label'     => 'Alias',
                    'rules'     => 'trim|xss_clean|strip_tags'
                ],
                [
                    'field'     => 'website',
                    'label'     => 'Website',
                    'rules'     => 'trim|xss_clean|strip_tags'
                ],
                [
                    'field'     => 'loc_id',
                    'label'     => 'Localitate',
                    'rules'     => 'trim|xss_clean|is_natural|strip_tags'
                ],
                [
                    'field'     => 'is_deleted',
                    'label'     => 'Sters',
                    'rules'     => 'trim|xss_clean|in_list[0,1]|strip_tags'
                ]
            ];

            $fv->set_rules($rules_config);

            if ($fv->run()) {
                // Verifica unicitatea numelui
                $filters = new EdituraFilters();
                $filters->setNumeLike(post('nume'));
                if ($id) $filters->setExcludedId($id);
                $res = EdituraTable::getInstance()->fetchAll($filters);
                if ($res && count($res['items'])) {
                    $this->mess->displayErrors(array('nume' => 'Exista deja o editura cu acelasi nume.'));
                }

                $editura->setNume(post('nume'));
                $editura->setAlias(post('alias'));
                $editura->setWebsite(post('website'));
                $editura->setLocalitateId((int)post('loc_id'));

                try {
                    EdituraTable::getInstance()->save($editura);
                } catch (Exception $e) {
                    $this->mess->displayErrors(array('nume' => 'Datele nu pot fi salvate în baza de date -> ' . $e->getMessage()));
                }

                if ($this->input->is_ajax_request())
                    $this->mess->displaySuccess('Datele au fost salvate cu succes');
            } else {

                if ($this->input->is_ajax_request()) {
                    $allErrors = $fv->error_array();
                    echo json_encode(array('error' => $allErrors));
                    exit;
                }
            }
        }

        // ------------------------------

        // Template data
        $viewData = array(
            'editura'   => $editura,

            // Template data
            'page_title' => $id ? 'Editează editura ID ' . $id : 'Adaugă editura',
            'breadcrumb' => array(
                array(
                    'name' => 'Listă edituri',
                    'href' => '/biblioteca/edituri/',
                )
            ),
        );

        $this->_render($this->viewPath . 'edit', $this->defaultTemplate, $viewData);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Sterge categorie
     * @param int $id
     */
    function delete($id)
    {
        $m = new Mess();

        $id = (int)$id;
        if (!$id) {
            $m->displayErrors("ID categorie invalid");
        }

        if (!post('submited')) {
            $m->displayErrors("Formular invalid");
        }

        $categorie = CategorieCarteTable::getInstance()->load($id);
        if (!$categorie) {
            $m->displayErrors("Categoria nu a fost gasita in baza de date");
        }

        // Delete data.
        try {
            CategorieCarteTable::getInstance()->delete($categorie);
        } catch (Exception $e) {
            $m->displayErrors('Nu am putut sterge categoria din baza de date -> ' . $e->getMessage());
        }

        $m->displaySuccess(true);
    }

    // ---------------------------------------------------------------------------------------------

    public function cautaLocalitate()
    {
        $searchTerm = isset($_GET['query']) ? trim(strip_tags($_GET['query'])) : null;
        if (!$searchTerm) {
            $this->displayAutocompleteResults($searchTerm, array());
        }

        // Initialize data.
        $results = array();

        // Fetch data.
        $data = LocalitateTable::getInstance()->fetchForAutocomplete($searchTerm);
        if ($data && count($data)) {
            /** @var LocalitateItem $localitate */
            foreach ($data as $localitate) {
                $results[] = array(
                    'value' => $localitate->getNumeComplet(),
                    'id' => $localitate->getId(),
                    'data' => array('category' => 'Nume complet'),
                );
            }
        }

        // Return results.
        $this->displayAutocompleteResults($searchTerm, $results);
    }
}
