<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Categorii_carti extends Admin_Controller
{

    private $viewPath = 'admin/biblioteca/categorii_carti/';

    /**
     * @var $mess Mess
     */
    public $mess;


    public function __construct()
    {
        parent::__construct();

        $this->load->helper(array('form', 'url'));

        $this->load->model('m_autoload');

        /**
         * Toate modelele care tin de Biblioteca si Nomenclatoare sunt incarcate in mod automat,
         * doar atunci cand sunt apelate/initializate.
         * Vezi fisierul config/hooks.php
         */
    }

    // ---------------------------------------------------------------------------------------------

    public function index()
    {
        // Fetch data
        $data = CategorieCarteTable::getInstance()->fetchAll();

        // Get data.
        if (!isset($data['items']) && !isset($data['count'])) {
            show_error('Nu am putut prelua informatiile din baza de date');
        }

        // Set data.
        $viewData = array(
            'items' => $data['items'],

            // Template data
            'page_title' => 'Listă categorii cărţi',
            'breadcrumb' => array(
                array(
                    'name' => 'Categorii cărţi',
                ),
            ),
        );

        // Render view.
        $this->_render($this->viewPath . 'list', $this->defaultTemplate, $viewData);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Adauga categorie noua
     */
    public function add()
    {
        $this->edit();
    }

    /**
     * Editeaza categorie
     * @param int|bool $id
     */
    function edit($id = null)
    {
        $id = $id ? (int)$id : null;

        // Get data by id
        $categorie = $id ? CategorieCarteTable::getInstance()->load($id) : new CategorieCarteItem();

        // Parse post data
        if (post()) {

            try {
                $categorie->setParentId(post('parentId'));
            } catch (Exception $e) {
                $this->mess->addError(array('parentId' => $e->getMessage()));
            }

            try {
                $categorie->setNume(post('nume'));
            } catch (Exception $e) {
                $this->mess->addError(array('nume' => $e->getMessage()));
            }

            try {
                $categorie->setOrderId(post('orderId'));
            } catch (Exception $e) {
                $this->mess->addError(array('orderId' => $e->getMessage()));
            }

            if ($this->mess->hasErrors()) {
                $this->mess->displayErrors();
            }

            try {
                CategorieCarteTable::getInstance()->save($categorie);
            } catch (Exception $e) {
                $this->mess->displayErrors($e->getMessage());
            }

            $this->mess->displaySuccess('Datele au fost salvate cu succes');
        }

        // ------------------------------

        // Template data
        $viewData = array(
            'parentItems'   => CategorieCarteTable::getInstance()->fetchParentsForSelect(),
            'categorie'     => $categorie,
            // Template data
            'page_title' => $id ? 'Editează categorie ID ' . $id : 'Adaugă categorie',
            'breadcrumb' => array(
                array(
                    'name' => 'Listă categorii cărţi',
                    'href' => '/biblioteca/categorii_carti/',
                )
            ),
        );

        $this->_render($this->viewPath . 'edit', $this->defaultTemplate, $viewData);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Sterge categorie
     * @param int $id
     */
    function delete($id)
    {
        $m = new Mess();

        $id = (int)$id;
        if (!$id) {
            $m->displayErrors("ID categorie invalid");
        }

        if (!post('submited')) {
            $m->displayErrors("Formular invalid");
        }

        $categorie = CategorieCarteTable::getInstance()->load($id);
        if (!$categorie) {
            $m->displayErrors("Categoria nu a fost gasita in baza de date");
        }

        // Delete data.
        try {
            CategorieCarteTable::getInstance()->delete($categorie);
        } catch (Exception $e) {
            $m->displayErrors('Nu am putut sterge categoria din baza de date -> ' . $e->getMessage());
        }

        $m->displaySuccess(true);
    }

    // ---------------------------------------------------------------------------------------------


}
