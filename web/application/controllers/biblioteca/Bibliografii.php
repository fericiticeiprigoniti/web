<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bibliografii extends Admin_Controller
{

    private $viewPath = 'admin/biblioteca/bibliografii/';

    /**
     * @var $mess Mess
     */
    public $mess;


    public function __construct()
    {
        parent::__construct();

        $this->load->helper(array('form', 'url'));
        $this->load->model('m_autoload');

        /**
         * Toate modelele care tin de Biblioteca si Nomenclatoare sunt incarcate in mod automat,
         * doar atunci cand sunt apelate/initializate.
         * Vezi fisierul config/hooks.php
         */
    }

    // ---------------------------------------------------------------------------------------------

    public function index()
    {
        // Fetch data
        $data = BibliografieTable::getInstance()->fetchAll();

        // Get data.
        if (!isset($data['items']) && !isset($data['count'])) {
            show_error('Nu am putut prelua informatiile din baza de date');
        }

        // Set data.
        $viewData = array(
            'items' => $data['items'],

            // Template data
            'page_title' => 'Listă bibliografii',
            'breadcrumb' => array(
                [
                    'name' => 'Bibliografii',
                ],
            ),
        );

        // Render view.
        $this->_render($this->viewPath . 'list', $this->defaultTemplate, $viewData);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Adauga categorie noua
     */
    public function add()
    {
        $this->edit();
    }

    /**
     * Editeaza categorie
     * @param int|bool $id
     */
    function edit($id = null)
    {
        $id = $id ? (int)$id : null;

        // Get data by id
        $objItem = $id ? BibliografieTable::getInstance()->load($id) : new BibliografieItem();

        $fv = $this->form_validation;
        $this->form_validation->set_rules('titlu',          'Titlu',      'trim|required|strip_tags|xss_clean');
        $this->form_validation->set_rules('descriere',      'Descriere',  'trim|required|strip_tags|xss_clean');
        $this->form_validation->set_rules('is_published',    'Publicat',  'trim|required|strip_tags|xss_clean|numeric|in_list[0,1]');
        $this->form_validation->set_rules('is_deleted',      'Sters',     'trim|strip_tags|xss_clean|numeric|in_list[0,1]');
        $this->form_validation->set_rules('book[]',         'Carti',      'trim|strip_tags|xss_clean|numeric');

        if ($fv->run() && post()) {
            if (!$this->input->is_ajax_request()) return;

            // setters
            $objItem->setTitlu(post('titlu'));
            $objItem->setDescriere(post('descriere'));
            $objItem->setIsPublished(post('is_published'));
            $objItem->setIsDeleted(post('is_deleted'));

            if (post('book')) {
                $objItem->setCarti(post('book'));
            }

            if (BibliografieTable::getInstance()->save($objItem)) {
                $this->mess->displaySuccess(
                    'Bibliografia a fost salvata cu succes!',
                    ['redirect_url' => '/biblioteva/bibliografie/edit/' . $objItem->getId()]
                );
            }
        } else {
            if ($this->input->is_ajax_request()) {
                $this->mess->addError($fv->error_array());
                $this->mess->displayErrors();
            }
        }

        // ------------------------------

        // Template data
        $viewData = array(
            'objItem'     => $objItem,
            'list_carti'  => CarteTable::getInstance()->fetchForSelect(),

            // Template data
            'page_title' => $id ? "Editează bibliografie ID {$id}" : 'Adaugă bibliografie',
            'breadcrumb' => array(
                [
                    'name' => 'Listă bibliografii',
                    'href' => '/biblioteca/bibliografii',
                ],
                [
                    'name' => $id ? "Editează bibliografie" : 'Adaugă bibliografie',
                    'href' => '/',
                ],
            ),
        );

        $this->_render($this->viewPath . 'edit', $this->defaultTemplate, $viewData);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Sterge
     * @param int $id
     */
    function delete($id)
    {
        $m = new Mess();

        $id = (int)$id;
        if (!$id) {
            $m->displayErrors("ID invalid");
        }

        if (!post('submited')) {
            $m->displayErrors("Formular invalid");
        }

        $bibliografie = BibliografieTable::getInstance()->load($id);
        if (!$bibliografie) {
            $m->displayErrors("Bibliografia nu a fost gasita in baza de date");
        }

        // Delete data.
        try {
            BibliografieTable::getInstance()->delete($bibliografie);
        } catch (Exception $e) {
            $m->displayErrors('Nu am putut sterge bibliografia din baza de date -> ' . $e->getMessage());
        }

        $m->displaySuccess(true);
    }

    function stergeCarte($bibliografieID, $carteID) {}
}
