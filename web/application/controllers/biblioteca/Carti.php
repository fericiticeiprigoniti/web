<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Carti extends Admin_Controller
{

    private $viewPath = 'admin/biblioteca/carti/';

    // ---------------------------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();

        $this->load->model('m_autoload');

        /**
         * Toate modelele care tin de Biblioteca si Nomenclatoare sunt incarcate in mod automat,
         * doar atunci cand sunt initializate, ca sa nu incarce
         * Vezi fisierul config/hooks.php
         */
    }

    // ---------------------------------------------------------------------------------------------

    public function index()
    {
        // Filters.
        $getData = $_GET;
        $filters = new CarteFilters($getData);

        // Limit.
        $page = isset($_GET['_page']) ? (int)$_GET['_page'] : 1;
        $perPage = isset($_GET['_per_page']) ? (int)$_GET['_per_page'] : 50;
        $offset = ($page - 1) * $perPage;

        // Fetch data
        $data = CarteTable::getInstance()->fetchAll($filters, $perPage, $offset);

        // Get data.
        if (!isset($data['items']) && !isset($data['count'])) {
            die('Nu am putut prelua informatiile din baza de date');
        }

        // -----------------------------------

        // Initialize pagination.
        $pConfig = $this->config->item('pagination');
        $pConfig['base_url'] = General::url(array('_page' => null));
        $pConfig['total_rows'] = $data['count'];
        $pConfig['cur_page'] = $page;
        $pConfig['per_page'] = $perPage;
        $this->pagination->initialize($pConfig);

        // -----------------------------------

        // Set data.
        $viewData = array(
            'filters' => (object)$filters,
            'items' => $data['items'],
            'pagination' => $this->pagination->create_links(),
            'pag' => array(
                'start' => $offset,
                'perPage' => $perPage,
                'count' => $data['count'],
                'itemsPerPage' => array(10, 20, 50, 100, 200, 500),
            ),
            'dimensiuniCarte' => DimensiuneTable::getInstance()->fetchForSelect(),
            'tipuriFormat' => CarteItem::fetchFormatTypes(),

            // Template data
            'page_title' => 'Listă cărţi',
            'breadcrumb' => array(
                array(
                    'name' => 'Listă cărţi',
                ),
            ),
        );

        // Render view.
        $this->_render($this->viewPath . 'list', $this->defaultTemplate, $viewData);
    }

    // ---------------------------------------------------------------------------------------------

    public function add()
    {
        $this->edit();
    }

    /**
     * @param int $id
     */
    public function edit($id = false)
    {
        $edit = ($id) ? (int)$id : null;

        if ($id) {
            if (!$objCarte = CarteTable::getInstance()->load($id)) show_404();
        } else {
            $objCarte = new CarteItem();
        }

        // selecteaza TAB activ + Submit Form
        $activeTab = get('tab');
        switch ($activeTab) {
            case 'detalii':
                $this->_tab_detalii($id);
                break;
            case 'cuvinte':
                $this->_tab_cuvinte($id);
                break;
            case 'ebook':
                $this->_tab_ebook($id);
                break;

            default:
                $this->_tab_generale($id);
                break;
        }


        // populeaza tabelele cu date din fiecare TAB
        if ($id) {
        }

        $personajFilters = new PersonajFilters(['statusId' => 1]); // afsieaza toate persoanjele active
        $rolFilters = new RolFilters(['parentId' => 14]); // afsieaza toate rolurile Editoriale

        // Set data.
        $viewData = array(
            'edit'      => $edit,
            'tab_activ' => empty($activeTab) ? 'generale' : $activeTab,
            'objItem'   => $objCarte,
            'personaje'    => PersonajTable::getInstance()->fetchForSelect($personajFilters),
            'roluri'    => RolTable::getInstance()->fetchForSelect($rolFilters),
            'dimensiuniCarte' => DimensiuneTable::getInstance()->fetchForSelect(),

            // Template data
            'page_title' => "Editare carte - ID {$id}",
            'breadcrumb' => [
                [
                    'name' => 'Listă cărţi',
                    'href' => '/biblioteca/carti',
                ],
                [
                    'name' => $edit ? "Editează carte - ID {$id}" : 'Adaugă carte',
                ],
            ],
        );

        // Render view.
        $this->_render($this->viewPath . 'edit', $this->defaultTemplate, $viewData);
    }

    // ---------------------------------------------------------------------------------------------

    public function cautaCarte()
    {
        $searchTerm = isset($_GET['query']) ? trim(strip_tags($_GET['query'])) : null;
        if (!$searchTerm) {
            $this->displayAutocompleteResults($searchTerm, array());
        }

        // Initialize data.
        $results = array();

        // Fetch data.
        $data = CarteTable::getInstance()->fetchForAutocomplete($searchTerm);
        if ($data && count($data)) {
            /** @var CarteItem $carte */
            foreach ($data as $carte) {
                $results[] = array(
                    'value' => $carte->getSearchResult(),
                    'id' => $carte->getId(),
                    'data' => array('category' => 'Id | Titlu | Subtitlu'),
                );
            }
        }

        // Return results.
        $this->displayAutocompleteResults($searchTerm, $results);
    }

    // ---------------------------------------------------------------------------------------------

    public function cautaParinte()
    {
        $searchTerm = isset($_GET['query']) ? trim(strip_tags($_GET['query'])) : null;
        if (!$searchTerm) {
            $this->displayAutocompleteResults($searchTerm, array());
        }

        // Initialize data.
        $results = array();

        // Fetch data.
        $filters = new CarteFilters();
        $filters->setSearchTerm($searchTerm);
        $filters->setParentId(0);
        $res = CarteTable::getInstance()->fetchAll($filters, 10);
        $data = isset($res['items']) ? $res['items'] : array();
        if ($data && count($data)) {
            /** @var CarteItem $carte */
            foreach ($data as $carte) {
                $results[] = array(
                    'value' => $carte->getSearchResult(),
                    'id' => $carte->getId(),
                    'data' => array('category' => 'Id | Titlu | Subtitlu'),
                );
            }
        }

        // Return results.
        $this->displayAutocompleteResults($searchTerm, $results);
    }

    // ---------------------------------------------------------------------------------------------

    public function cautaCategorie()
    {
        $searchTerm = isset($_GET['query']) ? trim(strip_tags($_GET['query'])) : null;
        if (!$searchTerm) {
            $this->displayAutocompleteResults($searchTerm, array());
        }

        // Initialize data.
        $results = array();

        // Fetch data.
        $data = CategorieCarteTable::getInstance()->fetchForAutocomplete($searchTerm);
        if ($data && count($data)) {
            /** @var CategorieCarteItem $categorieCarte */
            foreach ($data as $categorieCarte) {
                $results[] = array(
                    'value' => $categorieCarte->getSearchResult(),
                    'id' => $categorieCarte->getId(),
                    'data' => array('category' => 'Id | Nume'),
                );
            }
        }

        // Return results.
        $this->displayAutocompleteResults($searchTerm, $results);
    }

    // ---------------------------------------------------------------------------------------------

    public function cautaLocalitate()
    {
        $searchTerm = isset($_GET['query']) ? trim(strip_tags($_GET['query'])) : null;
        if (!$searchTerm) {
            $this->displayAutocompleteResults($searchTerm, array());
        }

        // Initialize data.
        $results = array();

        // Fetch data.
        $data = LocalitateTable::getInstance()->fetchForAutocomplete($searchTerm);
        if ($data && count($data)) {
            /** @var LocalitateItem $localitate */
            foreach ($data as $localitate) {
                $results[] = array(
                    'value' => $localitate->getNumeComplet(),
                    'id' => $localitate->getId(),
                    'data' => array('category' => 'Nume complet'),
                );
            }
        }

        // Return results.
        $this->displayAutocompleteResults($searchTerm, $results);
    }

    // ---------------------------------------------------------------------------------------------

    public function cautaEditura()
    {
        $searchTerm = isset($_GET['query']) ? trim(strip_tags($_GET['query'])) : null;
        if (!$searchTerm) {
            $this->displayAutocompleteResults($searchTerm, array());
        }

        // Initialize data.
        $results = array();

        // Fetch data.
        $data = EdituraTable::getInstance()->fetchForAutocomplete($searchTerm);
        if ($data && count($data)) {
            /** @var EdituraItem $editura */
            foreach ($data as $editura) {
                $results[] = array(
                    'value' => $editura->getSearchResult(),
                    'id' => $editura->getId(),
                    'data' => array('category' => 'Id | Nume | Alias'),
                );
            }
        }

        // Return results.
        $this->displayAutocompleteResults($searchTerm, $results);
    }

    // ---------------------------------------------------------------------------------------------

    public function cautaColectie()
    {
        $searchTerm = isset($_GET['query']) ? trim(strip_tags($_GET['query'])) : null;
        if (!$searchTerm) {
            $this->displayAutocompleteResults($searchTerm, array());
        }

        // Initialize data.
        $results = array();

        // Fetch data.
        $data = ColectieTable::getInstance()->fetchForAutocomplete($searchTerm);
        if ($data && count($data)) {
            /** @var ColectieItem $colectie */
            foreach ($data as $colectie) {
                $results[] = array(
                    'value' => $colectie->getSearchResult(),
                    'id' => $colectie->getId(),
                    'data' => array('category' => 'Id | Titlu'),
                );
            }
        }

        // Return results.
        $this->displayAutocompleteResults($searchTerm, $results);
    }

    // ---------------------------------------------------------------------------------------------

    public function cautaAutor()
    {
        $searchTerm = isset($_GET['query']) ? trim(strip_tags($_GET['query'])) : null;
        if (!$searchTerm) {
            $this->displayAutocompleteResults($searchTerm, array());
        }

        // Initialize data.
        $results = array();

        // Fetch data.
        $data = AutorTable::getInstance()->fetchForAutocomplete($searchTerm);
        if ($data && count($data)) {
            /** @var AutorItem $autor */
            foreach ($data as $autor) {
                $results[] = array(
                    'value' => $autor->getSearchResult(),
                    'id' => $autor->getId(),
                    'data' => array('category' => 'Id | Titlu'),
                );
            }
        }

        // Return results.
        $this->displayAutocompleteResults($searchTerm, $results);
    }

    // ---------------------------------------------------------------------------------------------

    private function _tab_generale($id)
    {
        // Am dat de inteles ca este apelata aceasta functie numai cand este trimis un formular de tip POST.
        if (!post()) return false;

        /**
         * @var $fv CI_Form_validation
         */
        $fv = $this->form_validation;

        // REGULI
        $rules_config = array(
            [
                'field'     => 'titlu',
                'label'     => 'Titlu',
                'rules'     => 'trim|required|xss_clean|strip_tags'
            ],
            [
                'field'     => 'subtitlu',
                'label'     => 'Subtitlu',
                'rules'     => 'trim|required|min_length[3]|xss_clean|strip_tags'
            ],
            [
                'field'     => 'cat_id',
                'label'     => 'Categorie',
                'rules'     => 'trim|xss_clean|strip_tags|is_natural'
            ],
            [
                'field'     => 'editura_id',
                'label'     => 'Editura 1',
                'rules'     => 'trim|xss_clean|strip_tags|is_natural'
            ],
            [
                'field'     => 'editura2_id',
                'label'     => 'Editura 2',
                'rules'     => 'trim|xss_clean|strip_tags|is_natural'
            ],

            [
                'field'     => 'oras',
                'label'     => 'Orasul',
                'rules'     => 'trim|strip_tags'
            ],
            [
                'field'     => 'editia',
                'label'     => 'Editia',
                'rules'     => 'trim|strip_tags'
            ],
            [
                'field'     => 'anul',
                'label'     => 'Anul',
                'rules'     => 'trim|strip_tags|is_natural'
            ],
            [
                'field'     => 'nr_pagini',
                'label'     => 'Nr pagini',
                'rules'     => 'trim|strip_tags|is_natural'
            ],
            [
                'field'     => 'nr_anexe',
                'label'     => 'Nr anexe',
                'rules'     => 'trim|strip_tags|is_natural'
            ],
            [
                'field'     => 'dimensiune_id',
                'label'     => 'Dimensiuni',
                'rules'     => 'trim|strip_tags|is_natural'
            ],

            [
                'field'     => 'observatii',
                'label'     => 'Observatii',
                'rules'     => 'trim|xss_clean|strip_tags'
            ],
            [
                'field'     => 'disponibil_in_format',
                'label'     => 'Disponibil in format',
                'rules'     => 'trim|xss_clean|is_natural|strip_tags'
            ],
            [
                'field'     => 'upload_coperta',
                'label'     => 'Coperta',
                'rules'     => 'trim|xss_clean'
            ],
            [
                'field'     => 'is_published',
                'label'     => 'Publicat',
                'rules'     => 'trim|xss_clean|is_natural|strip_tags'
            ],
            [
                'field'     => 'is_deleted',
                'label'     => 'Sters',
                'rules'     => 'trim|xss_clean|is_natural|strip_tags'
            ]

        );

        $fv->set_rules($rules_config);

        if ($fv->run()) {

            if ($this->input->is_ajax_request()) {
                $objCarte = $id ? CarteTable::getInstance()->load($id) : new CarteItem();

                $objCarte->setTitlu(post('titlu'));
                $objCarte->setSubtitlu(post('subtitlu'));
                $objCarte->setCategorieId(post('cat_id'));
                $objCarte->setEdituraId(post('editura_id'));
                $objCarte->setEditura2Id(post('editura2_id'));
                $objCarte->setOras(post('oras'));
                $objCarte->setEditia(post('editia'));
                $objCarte->setAnulPublicatiei(post('anul_publicatiei'));
                $objCarte->setNrPagini(post('nr_pagini'));
                $objCarte->setNrAnexe(post('nr_anexe'));
                $objCarte->setDimensiuneId(post('dimensiune_id'));
                $objCarte->setFormat(post('disponibil_in_format'));
                $objCarte->setCoperta(post('upload_coperta'));
                $objCarte->setObservatii(post('observatii'));

                $objCarte->setIsPublished(post('is_published'));
                $objCarte->setIsDeleted(post('is_deleted'));


                CarteTable::getInstance()->save($objCarte);

                // HANDLE Responsabili carte


                $this->mess->displaySuccess(
                    'Felicitari conasule!!!',
                    ['redirect_url' => '/biblioteca/carti/edit/' . $objCarte->getId()]
                );
            }
        } else {

            if ($this->input->is_ajax_request()) {
                $allErrors = $fv->error_array();
                echo json_encode(array('error' => $allErrors));
                exit;
            }
        }

        return false;
    }


    private function _tab_detalii($id) {}


    private function _tab_cuvinte($id) {}


    private function _tab_ebook($id) {}
}
