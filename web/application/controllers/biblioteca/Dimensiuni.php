<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dimensiuni extends Admin_Controller
{

    private $viewPath = 'admin/biblioteca/dimensiuni/';

    /**
     * @var $mess Mess
     */
    public $mess;


    public function __construct()
    {
        parent::__construct();

        $this->load->helper(array('form', 'url'));
        $this->load->model('m_autoload');

        /**
         * Toate modelele care tin de Biblioteca si Nomenclatoare sunt incarcate in mod automat,
         * doar atunci cand sunt apelate/initializate.
         * Vezi fisierul config/hooks.php
         */
    }

    // ---------------------------------------------------------------------------------------------

    public function index()
    {
        // Fetch data
        $data = DimensiuneTable::getInstance()->fetchAll();

        // Get data.
        if (!isset($data['items']) && !isset($data['count'])) {
            show_error('Nu am putut prelua informatiile din baza de date');
        }

        // Set data.
        $viewData = array(
            'items' => $data['items'],

            // Template data
            'page_title' => 'Listă dimensiuni cărţi',
            'breadcrumb' => array(
                [
                    'name' => 'Dimensiuni cărţi',
                ],
            ),
        );

        // Render view.
        $this->_render($this->viewPath . 'list', $this->defaultTemplate, $viewData);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Adauga categorie noua
     */
    public function add()
    {
        $this->edit();
    }

    /**
     * Editeaza categorie
     * @param int|bool $id
     */
    function edit($id = null)
    {
        $id = $id ? (int)$id : null;

        // Get data by id
        $objItem = $id ? DimensiuneTable::getInstance()->load($id) : new DimensiuneItem();

        // Parse post data
        if (post()) {

            try {
                $objItem->setLatime(post('latime'));
            } catch (Exception $e) {
                $this->mess->addError(array('parentId' => $e->getMessage()));
            }

            try {
                $objItem->setInaltime(post('inaltime'));
            } catch (Exception $e) {
                $this->mess->addError(array('nume' => $e->getMessage()));
            }

            if ($this->mess->hasErrors()) {
                $this->mess->displayErrors();
            }

            try {
                DimensiuneTable::getInstance()->save($objItem);
            } catch (Exception $e) {
                $this->mess->displayErrors($e->getMessage());
            }

            $this->mess->displaySuccess('Datele au fost salvate cu succes');
        }

        // ------------------------------

        // Template data
        $viewData = array(
            'dimensiune'     => $objItem,

            // Template data
            'page_title' => $id ? "Editează dimensiune ID {$id}" : 'Adaugă dimensiune',
            'breadcrumb' => array(
                array(
                    'name' => 'Listă dimensiuni cărţi',
                    'href' => '/biblioteca/dimensiuni',
                )
            ),
        );

        $this->_render($this->viewPath . 'edit', $this->defaultTemplate, $viewData);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Sterge categorie
     * @param int $id
     */
    function delete($id)
    {
        $m = new Mess();

        $id = (int)$id;
        if (!$id) {
            $m->displayErrors("ID dimensiuni invalid");
        }

        if (!post('submited')) {
            $m->displayErrors("Formular invalid");
        }

        $dimensiune = DimensiuneTable::getInstance()->load($id);
        if (!$dimensiune) {
            $m->displayErrors("Dimensiunea nu a fost gasita in baza de date");
        }

        // Delete data.
        try {
            DimensiuneTable::getInstance()->delete($dimensiune);
        } catch (Exception $e) {
            $m->displayErrors('Nu am putut sterge dimensiunea din baza de date -> ' . $e->getMessage());
        }

        $m->displaySuccess(true);
    }
}
