<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Origini_sociale extends Admin_Controller
{

    private $viewPath = 'admin/generale/origini_sociale/';

    /**
     * @var $mess Mess
     */
    public $mess;

    /**
     *
     * @var $m_origini_sociale M_origini_sociale
     */
    public $m_origini_sociale;

    function __construct()
    {
        parent::__construct();

        $this->load->library('pagination');
        $this->load->helper(array('form', 'url'));
        $this->load->model('m_origini_sociale');
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Listare origini sociale
     */
    public function index()
    {
        // Get data.
        $data = $this->m_origini_sociale->fetchList();

        // Set data.
        $viewData = array(
            'items' => $data,

            // Template data
            'page_title' => 'Lista origini sociale',
            'breadcrumb' => array(
                array(
                    'name' => 'Lista origini sociale',
                ),
            ),
        );

        // Render view.
        $this->_render($this->viewPath . 'list', $this->defaultTemplate, $viewData);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Adauga origine sociala noua
     *
     */
    public function add()
    {
        $this->edit();
    }

    /**
     * Editeaza origine sociala
     * @param int|bool $id
     */
    function edit($id = null)
    {
        $this->data['edit'] = $id ? (int)$id : null;

        // Get data by id
        if ($id) {
            $data = $this->m_origini_sociale->load($id);
        } else {
            $data = array(
                'nume' => null,
                'detalii' => null,
            );
        }
        $this->data['data'] = $data;

        // ------------------------------

        // Parse post data
        if (post()) {

            /** @var CI_Form_validation $fv */
            $fv = $this->form_validation;

            // REGULI
            $rules_config = array(
                array(
                    'field'     => 'nume',
                    'label'     => 'Nume',
                    'rules'     => 'trim|required|min_length[3]|xss_clean|strip_tags'
                ),
                array(
                    'field'     => 'detalii',
                    'label'     => 'Detalii',
                    'rules'     => 'trim|xss_clean|strip_tags'
                ),
            );

            $fv->set_rules($rules_config);

            if ($fv->run()) {

                // Verifica unicitatea numelui
                $res = $this->m_origini_sociale->fetchByName(post('nume'), $id);
                if ($res && count($res)) {
                    $this->mess->displayErrors(array('nume' => 'Originea sociala nu poate fi salvata. Exista o alta origine sociala cu aceeasi denumire'));
                }

                try {
                    $this->m_origini_sociale->save(post(), $id);
                } catch (Exception $e) {
                    $this->mess->displayErrors(array('nume' => 'Datele nu pot fi salvate in baza de date -> ' . $e->getMessage()));
                }


                if ($this->input->is_ajax_request()) {
                    $this->mess->displaySuccess('Datele au fost salvate cu succes');
                }
            } else {

                if ($this->input->is_ajax_request()) {
                    $allErrors = $fv->error_array();
                    echo json_encode(array('error' => $allErrors));
                    exit;
                }
            }
        }

        // ------------------------------

        // Template data
        $this->data['page_title'] = $id ? 'Editează origine sociala' : 'Adaugă origine sociala';
        $this->data['breadcrumb'] = array(
            array(
                'name' => 'Lista origini sociale',
                'href' => '/origini_sociale/',
            ),
            array(
                'name' => $id ? 'Editează origine sociala' : 'Adaugă origine sociala',
            ),
        );

        $this->_render($this->viewPath . 'edit', $this->defaultTemplate);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Sterge origine sociala
     * @param int $id
     */
    function delete($id)
    {
        $m = new Mess();

        $id = (int)$id;
        if (!$id) {
            $m->displayErrors("ID origine sociala invalid");
        }

        if (!post('submited')) {
            $m->displayErrors("Formular invalid");
        }

        $data = $this->m_origini_sociale->load($id);
        if (!$data || !count($data)) {
            $m->displayErrors("Originea sociala nu a fost gasit in baza de date");
        }

        // Verifica daca Originea sociala este folosit deja.
        if ($data['is_used']) {
            $m->displayErrors("Originea sociala nu poate fi stearsa. Este folosita in relatia cu un personaj");
        }

        // Delete data.
        try {
            $this->m_origini_sociale->delete($id);
            $m->displaySuccess(true);
        } catch (Exception $e) {
            $m->displayErrors('Nu am putut sterge originea sociala din baza de date -> ' . $e->getMessage());
        }
    }

    // ---------------------------------------------------------------------------------------------



}
