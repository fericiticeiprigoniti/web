<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Search_all extends Admin_Controller {

    
    function __construct(){
        parent::__construct();
        $this->load->model('m_autoload');
        $this->load->model('m_search_all');
        $this->load->model('m_personaje');
    }

    // ---------------------------------------------------------------------------------------------
    
    public function index()
    {
        $searchTerm = isset($_GET['query']) ? trim(strip_tags($_GET['query'])) : null;
        if (!$searchTerm) {
            $this->displayAutocompleteResults($searchTerm, array());
        }
    
        // Initialize data.
        $results = array();
        $elapsed = array();
    
        // Check for quick search items.
        if (substr($searchTerm, 0, 1) == '#') {
            // Fetch search types.
            $searchTypes = M_search_all::fetchTypes();
        
            // Remove #
            $searchTerm = substr($searchTerm, 1);
        
            // Split into spaces, extract search type and get final search term.
            $arr = explode(' ', $searchTerm);
            $searchType = trim($arr[0]);
            unset($arr[0]);
            $newSearchTerm = implode(' ', $arr);
        
            // Check for search type.
            if (in_array($searchType, array_keys($searchTypes))) {
                $data = M_search_all::searchForItem($searchType, $newSearchTerm);
                if (count($data)) {
                    $results += $data;
                }
            } else {    // Return the list of available search types.
                foreach($searchTypes as $key=>$value) {
                    $results[] = array(
                        'value' => '#' . $key // . ' - ' . $value
                    , 'data' => array('category' => 'Search Type'),
                    );
                }
            }
        
            $this->displayAutocompleteResults($searchTerm, $results);
        }
    
        // -----------------------------
    
        // Start timer.
        $timer = microtime(true);
    
        // Search personaj.
        $data = M_search_all::searchPersonaj($searchTerm);
        if (count($data)) {
            $results += $data;
        }
    
        // Timer.
        $t0 = $timer;
        $timer = microtime(true);
        $elapsed[M_search_all::TIP_PERSONAJ] = round($timer - $t0, 2);
    
        // -----------------------------
    
        // Search autor.
        $data = M_search_all::searchAutor($searchTerm);
        if (count($data)) {
            $results += $data;
        }
    
        // Timer.
        $t0 = $timer;
        $timer = microtime(true);
        $elapsed[M_search_all::TIP_AUTOR] = round($timer - $t0, 2);
    
        // -----------------------------
    
        // Search carte.
        $data = M_search_all::searchCarte($searchTerm);
        if (count($data)) {
            $results += $data;
        }
    
        // Timer.
        $t0 = $timer;
        $timer = microtime(true);
        $elapsed[M_search_all::TIP_CARTE] = round($timer - $t0, 2);
    
        // -----------------------------
    
        // Search categorieCarte.
        $data = M_search_all::searchCategorieCarte($searchTerm);
        if (count($data)) {
            $results += $data;
        }
    
        // Timer.
        $t0 = $timer;
        $timer = microtime(true);
        $elapsed[M_search_all::TIP_CATEGORIE_CARTE] = round($timer - $t0, 2);
    
        // -----------------------------
    
        // Search articol.
        $data = M_search_all::searchArticol($searchTerm);
        if (count($data)) {
            $results += $data;
        }
    
        // Timer.
        $t0 = $timer;
        $timer = microtime(true);
        $elapsed[M_search_all::TIP_ARTICOL] = round($timer - $t0, 2);
    
        // -----------------------------
    
        // Search editura.
        $data = M_search_all::searchEditura($searchTerm);
        if (count($data)) {
            $results += $data;
        }
    
        // Timer.
        $t0 = $timer;
        $timer = microtime(true);
        $elapsed[M_search_all::TIP_EDITURA] = round($timer - $t0, 2);
    
        // -----------------------------
    
        // Search colectie.
        $data = M_search_all::searchColectie($searchTerm);
        if (count($data)) {
            $results += $data;
        }
    
        // Timer.
        $t0 = $timer;
        $timer = microtime(true);
        $elapsed[M_search_all::TIP_COLECTIE] = round($timer - $t0, 2);
    
        // -----------------------------
    
        // Return results.
        $this->displayAutocompleteResults($searchTerm, $results, $elapsed);
    }

    // ---------------------------------------------------------------------------------------------


}