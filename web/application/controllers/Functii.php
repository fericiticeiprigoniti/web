<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Functii extends Admin_Controller
{

    private $viewPath = 'admin/generale/functii/';

    /**
     * @var $mess Mess
     */
    public $mess;

    /**
     *
     * @var $m_functii M_functii
     */
    public $m_functii;

    function __construct()
    {
        parent::__construct();

        $this->load->library('pagination');
        $this->load->helper(array('form', 'url'));
        $this->load->model('m_functii');
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Listare functii
     */
    public function index()
    {
        // Get data.
        $data = $this->m_functii->fetchList();

        // Set data.
        $viewData = array(
            'items' => $data,

            // Template data
            'page_title' => 'Lista functii',
            'breadcrumb' => array(
                array(
                    'name' => 'Lista functii',
                ),
            ),
        );

        // Render view.
        $this->_render($this->viewPath . 'list', $this->defaultTemplate, $viewData);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Adauga functie noua
     *
     */
    public function add()
    {
        $this->edit();
    }

    /**
     * Editeaza functie
     * @param int|bool $id
     */
    function edit($id = null)
    {
        $this->data['edit']   = $edit = ($id) ? (int)$id : null;

        // Get data by id
        if ($id) {
            $data = $this->m_functii->load($id);
        } else {
            $data = array(
                'nume' => null,
            );
        }
        $this->data['data'] = $data;

        // ------------------------------

        // Parse post data
        if (post()) {

            /** @var CI_Form_validation $fv */
            $fv = $this->form_validation;

            // REGULI
            $rules_config = array(
                array(
                    'field'     => 'nume',
                    'label'     => 'Nume',
                    'rules'     => 'trim|required|min_length[3]|xss_clean|strip_tags'
                ),
            );

            $fv->set_rules($rules_config);

            if ($fv->run()) {

                // Verifica unicitatea numelui
                $res = $this->m_functii->fetchByName(post('nume'), $id);
                if ($res && count($res)) {
                    $this->mess->displayErrors(array('nume' => 'Functia nu poate fi salvata. Exista o alta functie cu aceeasi denumire'));
                }

                try {
                    $this->m_functii->save(post(), $id);
                } catch (Exception $e) {
                    $this->mess->displayErrors(array('nume' => 'Datele nu pot fi salvate in baza de date -> ' . $e->getMessage()));
                }


                if ($this->input->is_ajax_request()) {
                    $this->mess->displaySuccess('Datele au fost salvate cu succes');
                }
            } else {

                if ($this->input->is_ajax_request()) {
                    $allErrors = $fv->error_array();
                    echo json_encode(array('error' => $allErrors));
                    exit;
                }
            }
        }

        // ------------------------------

        // Template data
        $this->data['page_title'] = $edit ? 'Editează funcţie' : 'Adaugă funcţie';
        $this->data['breadcrumb'] = array(
            array(
                'name' => 'Lista funcţii',
                'href' => '/functii/',
            ),
            array(
                'name' => $edit ? 'Editează funcţie' : 'Adaugă funcţie',
            ),
        );

        $this->_render($this->viewPath . 'edit', $this->defaultTemplate);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Sterge functie
     * @param int $id
     */
    function delete($id)
    {
        $m = new Mess();

        $id = (int)$id;
        if (!$id) {
            $m->displayErrors("ID functie invalid");
        }

        if (!post('submited')) {
            $m->displayErrors("Formular invalid");
        }

        $data = $this->m_functii->load($id);
        if (!$data || !count($data)) {
            $m->displayErrors("Functia nu a fost gasita in baza de date");
        }

        // Verifica daca functia este folosita deja.
        if ($data['is_used']) {
            $m->displayErrors("Functia nu poate fi stearsa. Este folosita in relatia cu un personaj");
        }

        // Delete data.
        try {
            $this->m_functii->delete($id);
            $m->displaySuccess(true);
        } catch (Exception $e) {
            $m->displayErrors('Nu am putut sterge functia din baza de date -> ' . $e->getMessage());
        }
    }

    // ---------------------------------------------------------------------------------------------


}
