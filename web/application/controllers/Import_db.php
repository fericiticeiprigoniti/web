<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Import_db extends Admin_Controller {


    /**
    * @var $mess Mess
    */
    public $mess;

    /**
    *
    * @var $m_functii M_functii
    */
    public $m_functii;

    function __construct(){
        parent::__construct();

        $this->load->model('m_personaje');
        $this->load->model('m_articole');
    }

    function index()
    {
        d('start import from OLD database');

        // import poetii

        // import poezii + taguri
        //$this->syncTags();

        $this->syncPoezii();

    }

    function syncTags()
    {

        if( $old_tags = $this->db->get('pi_tags')->result_array() )
        {
            $k = 0;
            foreach( $old_tags as $tag)
            {
                $k++;

               $sql = "INSERT INTO fcp_taguri (id, nume, alias, observatii, user_id, is_favorit, data_insert, hits, personaj_id, is_deleted)
                        VALUES (?, ?, ?, ?, ?, ?, ?, ?, null , ?)
                        ON DUPLICATE KEY UPDATE id = ?";

               $this->db->query($sql, [
                    $tag['tagID'],
                    $tag['tagName'],
                    $tag['tagAlias'],
                    $tag['tagObservation'],
                    2,  // user Dan Tudorache
                    $tag['tagFavorite'],
                    $tag['tagDate'],
                    $tag['tagViews'],
                    $tag['tagIsDeleted'],
                    $tag['tagID']]);
            }

            echo "--------------- \n S-au importat ".$k. " taguri";
        }



    }

    function syncPoezii()
    {

        // 1 step
        $this->syncPersonaje();

        // 2 step
        if( $old = $this->db->get('pi_poezii')->result_array() )
        {
            $k = 0;
            foreach( $old as $item)
            {
                $k++;

                $sql = "INSERT INTO fcp_poezii (
                                                id
                                                ,titlu
                                                ,titlu_varianta
                                                ,personaj_id
                                                ,continut
                                                ,comentariu
                                                ,comentariu_user_id
                                                ,contextul_creatiei
                                                ,user_id
                                                ,sursa_link
                                                ,sursa_titlu
                                                ,sursa_doc_carte_id
                                                ,sursa_doc_publicatie_id
                                                ,sursa_doc_start_page
                                                ,sursa_doc_end_page
                                                ,sursa_culegator
                                                ,ciclu_id
                                                ,subiect_id
                                                ,momentul_creatiei_id
                                                ,specie_id
                                                ,structura_strofa_id
                                                ,rima_id
                                                ,picior_metric_id
                                                ,nr_strofe
                                                ,data_insert
                                                ,data_publicare
                                                ,order_id
                                                ,hits
                                                ,is_deleted
                                                ,is_published
               )
                        VALUES (".rtrim(str_repeat('?,', 29),',').")
                        ON DUPLICATE KEY UPDATE id = ?";

               $this->db->query($sql, [
                    $item['pID'],
                    $item['pTitle'],
                    null,                          // titlu varianta
                    $item['pAuthorID'],
                    $item['pContent'],
                    $item['pComment'],
                    $item['pCommentAuthorID'],
                    2,                             // user Dan Tudorache
                    $item['pSursaLink'],     // backup
                    $item['pSursaTitle'],     //
                    $item['pSursaDocID'],     // carte id
                    null,                     // publicatie id
                    $item['pSursaDocStartPage'],
                    $item['pSursaDocEndPage'],
                    $item['pSursaCulegator'],
                    $item['pCicluID'],
                    $item['pSubjectID'],
                    $item['pMomentulCreatieiID'],
                    $item['pSpecieID'],
                    $item['pStructuraStrofaID'],
                    $item['pRimaID'],
                    $item['pPiciorMetricID'],
                    $item['pNrStrofe'],
                    $item['pDateInsert'],
                    $item['pDatePublished'],
                    $item['pOrder'],
                    $item['pHits'],
                    $item['pIsDeleted'],
                    0,                          // is published

                    $item['pID'] // where
               ]);

               die();
            }

            echo "--------------- \n S-au importat ".$k. " poezii";
        }



    }

    public function syncPersonaje()
    {
        if( $old = $this->db->get('jsn_marturisitori')->result_array() )
        {
            $k = 0;
            foreach( $old as $item)
            {
                $k++;

                $sql = "INSERT INTO fcp_poezii (
                                                id
                                                ,mCatID_backup
                                                ,prefix
                                                ,nume
                                                ,prenume
                                                ,alias
                                                ,sex
                                                ,data_nastere
                                                ,data_adormire
                                                ,avatar_poza_id
                                                ,biografie_poza_id
                                                ,biografie_articol_id
                                                ,observatii
                                                ,observatii_user_id
                                                ,observatii_interne
                                                ,is_favorit
                                                ,status_id
                                                ,is_deleted
               )
                        VALUES (" . rtrim(str_repeat('?,', 18),',') .")
                        ON DUPLICATE KEY UPDATE id = ?";







   // TODO


//mLocNastere
//mJudNastere
//mOcupatie
//mAniInchisoare
//mInchisori
//mLocInmormantare

//mPozaBiografie
//mPathFototeca
//mFavorite
//mObservatiiInterne
//pBiografie
//pBiografieByUserID
//pObservatiiPersonaj
//pObservatiiPersonajByUserID
//mIsMarturisitor
//mIsMartir
//mIsErou
//mIsPoet
//mIsVictima
//mIsTortionar
//mIsPrigonitor
//mConfesiuneID


               // insert
               $this->db->query($sql, [
                    $item['mID'],
                    $item['mCatID'],
                    $item['mPrefix'],
                    $item['mNume'],
                    $item['mPrenume'],
                    $item['mPrenume'],
                    null,                           // alias
                    $item['mSex'],
                    $item['mDataNastere'],
                    $item['mDataAdormire'],
                    $item['mPoza'],
                    null,                           // poza mare Biografie (posibil sa schimbam sa punem Galerie Foto ID)



                    null,                          // titlu varianta
                    $item['pAuthorID'],
                    $item['pContent'],
//                    $comArtID,                     // comentariu_art_id
                        $item['pComment'],         // backup
                        $item['pCommentAuthorID'], // backup
                    2,

                    $item['mID']                // where
               ]);


            }
        }

    }

}