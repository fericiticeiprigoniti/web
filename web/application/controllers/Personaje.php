<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Personaje extends Admin_Controller
{

    private string $viewPath = 'admin/generale/personaje/';

    /**
     * @var Mess $mess
     */
    public $mess;

    /**
     * @var M_personaje $m_personaje
     */
    public $m_personaje;

    /**
     * @var M_localitati $m_localitati
     */
    public $m_localitati;

    /**
     * @var M_confesiuni $m_confesiuni
     */
    public $m_confesiuni;

    /**
     * @var M_nationalitati $m_nationalitati
     */
    public $m_nationalitati;

    /**
     *
     * @var M_origini_sociale $m_origini_sociale
     */
    public $m_origini_sociale;

    /**
     *
     * @var M_roluri $m_roluri
     */
    public $m_roluri;

    /**
     *
     * @var M_ocupatii $m_ocupatii
     */
    public $m_ocupatii;

    /**
     *
     * @var M_semnalmente_corporale $m_semnalmente_corporale
     */
    public $m_semnalmente_corporale;

    /**
     * @var M_functii $m_functii
     */
    public $m_functii;

    /**
     * @var M_locuri_patimire $m_locuri_patimire
     */
    public $m_locuri_patimire;

    /**
     * @var M_personaje_ocupatii $m_personaje_ocupatii
     */
    public $m_personaje_ocupatii;

    /**
     * @var M_personaje_condamnari $m_personaje_condamnari
     */
    public $m_personaje_condamnari;

    /**
     * @var M_apartenente_politice $m_apartenente_politice
     */
    public $m_apartenente_politice;

    /**
     * @var M_instante_condamnare $m_instante_condamnare
     */
    public $m_instante_condamnare;

    /**
     * @var M_personaje_itinerarii $m_personaje_itinerarii
     */
    public $m_personaje_itinerarii;

    /**
     * @var M_biblioteca_carti $m_biblioteca_carti
     */
    public $m_biblioteca_carti;

    /**
     * @var M_personaje_carti $m_personaje_carti
     */
    public $m_personaje_carti;

    /**
     * @var M_citate $m_citate
     */
    public $m_citate;

    function __construct()
    {
        parent::__construct();

        $this->load->library('pagination');
        $this->load->helper(array('form', 'url'));

        $this->load->model('m_personaje');
        $this->load->model('m_localitati');
        $this->load->model('m_confesiuni');
        $this->load->model('m_nationalitati');
        $this->load->model('m_origini_sociale');
        $this->load->model('m_roluri');
        $this->load->model('m_ocupatii');
        $this->load->model('m_semnalmente_corporale');
        $this->load->model('m_functii');
        $this->load->model('m_personaje_ocupatii');
        $this->load->model('m_apartenente_politice');
        $this->load->model('m_instante_condamnare');
        $this->load->model('m_locuri_patimire');
        $this->load->model('m_personaje_condamnari');
        $this->load->model('m_personaje_itinerarii');
        $this->load->model('m_biblioteca_carti');
        $this->load->model('m_personaje_carti');
        $this->load->model('m_citate');
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Listare personaje
     * @internal param $viewData
     */
    public function index()
    {
        // Filters.
        $filters = array(
            'id'                    => isset($_GET['id']) ? trim(strip_tags($_GET['id'])) : null,
            'nume_id'               => isset($_GET['nume_id']) ? trim(strip_tags($_GET['nume_id'])) : null,
            'nationalitate'         => $_GET['nationalitate'] ?? null,
            'loc_nastere'           => isset($_GET['loc_nastere']) ? trim(strip_tags($_GET['loc_nastere'])) : null,
            'loc_surghiun'          => $_GET['loc_surghiun'] ?? null,
            'data_nastere_de_la'    => isset($_GET['data_nastere_de_la']) ? trim(strip_tags($_GET['data_nastere_de_la'])) : null,
            'data_nastere_pana_la'  => isset($_GET['data_nastere_pana_la']) ? trim(strip_tags($_GET['data_nastere_pana_la'])) : null,
            'data_adormire_de_la'   => isset($_GET['data_adormire_de_la']) ? trim(strip_tags($_GET['data_adormire_de_la'])) : null,
            'data_adormire_pana_la' => isset($_GET['data_adormire_pana_la']) ? trim(strip_tags($_GET['data_adormire_pana_la'])) : null,
            'rol'                   => $_GET['rol'] ?? null,
            'ocupatie_consacrata'   => $_GET['ocupatie_consacrata'] ?? null,
            'ocupatie_arestare'     => $_GET['ocupatie_arestare'] ?? null,
            'confesiune'            => $_GET['confesiune'] ?? null,
            'origine_sociala'       => $_GET['origine_sociala'] ?? null,
            'luna_adormire'         => isset($_GET['luna_adormire']) ? trim(strip_tags($_GET['luna_adormire'])) : null,
            'sex'                   => isset($_GET['sex']) ? trim(strip_tags($_GET['sex'])) : null,
            'status'                => isset($_GET['status']) ? trim(strip_tags($_GET['status'])) : null,
        );

        // Check if there are filters set
        $hasAnyFilter = false;
        foreach ($filters as $value) {
            if (!is_null($value)) {
                $hasAnyFilter = true;
            }
        }

        // Setare filtre de tip autocomplete
        if ($filters['loc_nastere']) {
            $res = $this->m_localitati->load($filters['loc_nastere']);
            $filters['loc_nastere_nume_complet'] = $res['nume_complet'] ?? '';
        } else {
            $filters['loc_nastere_nume_complet'] = '';
        }
        if ($filters['nume_id']) {
            $res = $this->m_personaje->load($filters['nume_id']);
            $filters['nume_complet'] = $res['nume_complet'] ?? '';
        } else {
            $filters['nume_complet'] = '';
        }

        // Order.
        $order = array(
            'by' => isset($_GET['_order_by']) ? trim(strip_tags($_GET['_order_by'])) : null,
            'dir' => isset($_GET['_order_dir']) ? trim(strip_tags($_GET['_order_dir'])) : null,
        );

        // Limit.
        $page = isset($_GET['_page']) ? (int)$_GET['_page'] : 1;
        $perPage = isset($_GET['_per_page']) ? (int)$_GET['_per_page'] : 10;
        $limit = array(
            'start' => ($page - 1) * $perPage,
            'per_page' => $perPage,
        );

        // Get data.

        // set default
        if (is_null($order['by'])) $order['by'] = 'nume_complet';
        if (is_null($order['dir'])) $order['dir'] = 'ASC';

        $data = $this->m_personaje->fetchAll($filters, $order, $limit);
        if (!isset($data['items']) && !isset($data['count'])) {
            die('Nu am putut prelua informatiile din baza de date');
        }

        // -----------------------------------

        // Initialize pagination.
        $pConfig = $this->config->item('pagination');
        $pConfig['base_url'] = General::url(array('_page' => null));
        $pConfig['total_rows'] = $data['count'];
        $pConfig['cur_page'] = $page;
        $pConfig['per_page'] = $perPage;
        $this->pagination->initialize($pConfig);

        // -----------------------------------

        // Preluam rolurile morale.
        $roluriMorale = $this->m_roluri->fetchForSelect(array('parent_id' => 13));

        // Set data.
        $viewData = array(
            'filters' => (object)$filters,
            'order' => (object)$order,
            'items' => $data['items'],
            'pagination' => $this->pagination->create_links(),
            'pag' => array(
                'start' => $limit['start'],
                'perPage' => $perPage,
                'count' => $data['count'],
                'itemsPerPage' => array(10, 20, 50, 100, 200, 500),
            ),
            'nationalitati' => $this->m_nationalitati->fetchAll(true),
            'roluri' => $roluriMorale,
            'ocupatii' => $this->m_ocupatii->fetchAll(true),
            'confesiuni' => $this->m_confesiuni->fetchAll(true),
            'originiSociale' => $this->m_origini_sociale->fetchAll(true),
            'locuriPatimire' => $this->m_locuri_patimire->fetchForSelect(),
            'lunileAnului' => Calendar::fetchLunileAnului(),

            'hasAnyFilter' => $hasAnyFilter,

            // Template data
            'page_title' => 'Lista personaje',
            'breadcrumb' => array(
                array(
                    'name' => 'Lista personaje',
                ),
            ),
        );

        // Render view.
        $this->_render($this->viewPath . 'list_personaje', $this->defaultTemplate, $viewData);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * adauga personaj nou
     *
     * @throws Exception
     */
    public function adauga()
    {
        $this->editeaza();
    }

    /**
     * @throws Exception
     */
    function editeaza($pers_id = false)
    {
        $this->data['edit']   = $edit = ($pers_id) ? (int)$pers_id : FALSE;
        if ($pers_id) {
            if (!$objPersonaj = PersonajTable::getInstance()->load($pers_id)) show_404();
        } else {
            $objPersonaj = new PersonajItem();
        }

        // selecteaza TAB activ + Submit Form
        $activeTab = get('tab');
        switch ($activeTab) {
            case 'biografie':
                $this->_tab_biografie($pers_id);
                break;
            case 'realizarisocioprofesionale':
                $this->_tab_realizarisocioprofesionale($pers_id);
                break;
            case 'semnalmente_corporale':
                $this->_tab_semnalmente_corporale($pers_id);
                break;
            case 'episoade_represive':
                $this->_tab_episoade_represive($pers_id);
                break;
            case 'itinerariu_detentie':
                // tabel fcp_personaje2locuripatimire
                $this->_tab_itinerariu_detentie($pers_id);
                break;
            case 'pedepse_disciplinare':
                $this->_tab_pedepse_disciplinare($pers_id);
                break;
            case 'carti':
                $this->_tab_carti($pers_id);
                break;
            case 'citate':
                $this->_tab_citate($pers_id);
                break;
            default:
                $this->_tab_generale($pers_id);
                break;
        }
        $this->data['tab_activ'] = empty($activeTab) ? 'generale' : $activeTab;

        // populeaza tabelele cu date din fiecare TAB
        if ($pers_id) {
            $info                = $this->m_personaje->getProperty(array('prs.id' => $pers_id));
            $info['semnalmente'] = $this->m_personaje->fetchSemnalmente($pers_id);
            $info['pers_realizarisocioprofesionale']  = $this->m_personaje->fetchRealizari($pers_id);
            $info['pers_pedepse']     = $this->m_personaje->fetchPedepse($pers_id);
            $info['pers_itinerarii']  = $this->m_personaje_itinerarii->fetchAll($pers_id);
            $info['pers_carti']       = $this->m_personaje_carti->fetchAll($pers_id);

            // TAB - citate
            $info['pers_citate']      = $this->m_citate->fetchAll($pers_id);

            $this->data['info'] = $info;
        }

        // return OBJECT
        $this->data['objPersonaj'] = $objPersonaj;

        // selectbox
        // $this->data['list_localitati']      = $this->m_localitati->fetchList();
        $this->data['list_confesiuni']      = $this->m_confesiuni->fetchAll();
        $this->data['list_nationalitati']   = $this->m_nationalitati->fetchAll();
        $this->data['list_origini_sociale'] = $this->m_origini_sociale->fetchAll();
        $this->data['list_useri']           = $this->aauth->list_users(); // deprecated
        $this->data['list_aauth_users']     = AauthUserTable::getInstance()->fetchAll()['items'];
        $this->data['list_roluri']          = $this->m_roluri->getRoluri(13, true);
        $this->data['list_semnalmente']     = $this->m_semnalmente_corporale->fetchAll();
        $this->data['list_ocupatii']        = $this->m_personaje_ocupatii->fetchAll(true);
        $this->data['list_apart_politice']  = $this->m_apartenente_politice->fetchAll(true);
        $this->data['list_stare_civila']    = unserialize(STARE_CIVILA);
        $this->data['list_represiune_tipuri'] = $this->m_personaje_condamnari->fetchPersonajeDetentieIntrareTip();

        // TAB _ episoade_represive
        $this->data['condamnareID']                     = (int)get('condamnareID');
        $this->data['list_condamnari']                  = $this->m_personaje_condamnari->fetchAll((int)$pers_id);
        $this->data['list_sentinte_detinuti']           = $this->m_personaje->fetchAllSentinteDetinuti(true);
        $this->data['list_pedepse_tipuri']              = unserialize(CONDAMNARI_PEDEPSE_TIPURI);
        $this->data['list_condamnari_iesire_tipuri']    = $this->m_personaje_condamnari->fetchTipIesireListForSelect();

        // TAB _ itinerariu_condamnari
        $this->data['list_locuri_patimire']     = $this->m_locuri_patimire->fetchForSelect();
        $this->data['pers_list_condamnari']     = $this->m_personaje_condamnari->fetchForSelect($pers_id); // toate episoardele represive ale personajului

        $this->data['list_carti']                  = CarteTable::getInstance()->fetchForSelect();

        // autocomplete page details
        $this->data['autocomplete_page'] = array(
            'title'             => false,
            'subtitle'          => $edit ? $objPersonaj->getNumeComplet() : 'Adaugă personaj'
        );

        // Template data
        // $this->data['page_title'] = $edit ? 'Editează personaj ' . $objPersonaj->getFullName() : 'Adaugă personaj';
        $this->data['breadcrumb'] = array(
            array(
                'name' => 'Lista personaje',
                'href' => '/personaje',
            ),
            array(
                'name' => $edit ? 'Editează personaj' : 'Adaugă personaj',
            ),
        );

        $this->_render('/admin/generale/personaje/edit_personaj', $this->defaultTemplate);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * TODO
     * formular adaugare/edit Lot Detinuti
     *
     */
    public function lot_detinuti()
    {
        // TODO


        #
        #
        #
        #
        #
        #
        #


        // Template data
        $this->data['page_title'] = 'Sentințe condamnare';
        $this->data['breadcrumb'] = array(
            array(
                'name' => 'Personaje',
                'href' => '/personaje',
            ),
            array(
                'name' => 'Sentințe condamnare',
                'href' => '/personaje/lot_detinuti',
            )
        );

        //$this->_render('/admin/generale/personaje/simple_list',$this->defaultTemplate);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @throws Exception
     */
    public function _tab_generale($pers_id)
    {
        // Am dat de inteles ca este apelata aceasta functie numai cand este trimis un formular de tip POST.
        if (!post()) return false;

        /**
         * @var $fv CI_Form_validation
         */
        $fv = $this->form_validation;

        // REGULI
        $rules_config = array(
            array(
                'field'     => 'prefix',
                'label'     => 'Prefix',
                'rules'     => 'trim|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'nume',
                'label'     => 'Nume',
                'rules'     => 'trim|required|min_length[3]|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'prenume',
                'label'     => 'Prenume',
                'rules'     => 'trim|required|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'nume_anterior',
                'label'     => 'Nume anterior',
                'rules'     => 'trim|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'prenume_monahism',
                'label'     => 'Prenume monahism',
                'rules'     => 'trim|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'nume_porecla',
                'label'     => 'Nume poreclă',
                'rules'     => 'trim|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'nume_pseudonim',
                'label'     => 'Nume pseudonim',
                'rules'     => 'trim|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'alias',
                'label'     => 'Alias',
                'rules'     => 'trim|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'prenume_tata',
                'label'     => 'Prenume tată',
                'rules'     => 'trim|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'prenume_mama',
                'label'     => 'Prenume mamă',
                'rules'     => 'trim|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'data_nastere',
                'label'     => 'Dată naștere',
                'rules'     => 'trim|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'nastere_loc_id',
                'label'     => 'Loc nastere',
                'rules'     => 'trim|xss_clean|is_natural|strip_tags'
            ),
            array(
                'field'     => 'data_adormire',
                'label'     => 'Dată adormire',
                'rules'     => 'trim|xss_clean|strip_tags'
            ),

            array(
                'field'     => 'deces_locul_mortii',
                'label'     => 'Locul morții',
                'rules'     => 'trim|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'deces_nume_cimitir',
                'label'     => 'Nume cimitir',
                'rules'     => 'trim|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'deces_loc_inmormantare_id',
                'label'     => 'Loc inmormantare',
                'rules'     => 'trim|xss_clean|is_natural|strip_tags'
            ),
            array(
                'field'     => 'deces_mormant_coord_lat',
                'label'     => 'Mormânt (coord. lat.)',
                'rules'     => 'trim|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'deces_mormant_coord_long',
                'label'     => 'Mormânt (coord. long.)',
                'rules'     => 'trim|xss_clean|strip_tags'
            ),

            array(
                'field'     => 'count_copii_b',
                'label'     => 'Nr. copii (băieți)',
                'rules'     => 'trim|xss_clean|is_natural|strip_tags'
            ),
            array(
                'field'     => 'count_copii_f',
                'label'     => 'Nr. copii (fete)',
                'rules'     => 'trim|xss_clean|is_natural|strip_tags'
            ),
            array(
                'field'     => 'durata_detentie',
                'label'     => 'Durata detentie',
                'rules'     => 'trim|xss_clean|strip_tags|is_natural'
            ),
            array(
                'field'     => 'durata_prizonierat',
                'label'     => 'Durata prizonierat',
                'rules'     => 'trim|xss_clean|strip_tags|is_natural'
            ),
            array(
                'field'     => 'durata_deportare',
                'label'     => 'Durata deportare',
                'rules'     => 'trim|xss_clean|strip_tags|is_natural'
            ),
            array(
                'field'     => 'durata_domiciliu_oblig',
                'label'     => 'Durata dom obligatoriu',
                'rules'     => 'trim|xss_clean|strip_tags|is_natural'
            ),

            array(
                'field'     => 'observatii',
                'label'     => 'Observatii',
                'rules'     => 'trim|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'observatie_autor',
                'label'     => 'Autor observatie',
                'rules'     => 'trim|xss_clean|is_natural|strip_tags'
            ),
            array(
                'field'     => 'status_id',
                'label'     => 'Status',
                'rules'     => 'trim|xss_clean|is_natural|strip_tags'
            ),
            array(
                'field'     => 'is_detinut_politic',
                'label'     => 'Detinut politic',
                'rules'     => 'trim|xss_clean|is_natural|strip_tags'
            ),
            array(
                'field'     => 'sex',
                'label'     => 'Sex',
                'rules'     => 'trim|xss_clean|is_natural|in_list[0,1]|strip_tags'
            ),
            array(
                'field'     => 'confesiune_id',
                'label'     => 'Confesiune',
                'rules'     => 'trim|xss_clean|is_natural|strip_tags'
            ),
            array(
                'field'     => 'nationalitate_id',
                'label'     => 'Nationalitate',
                'rules'     => 'trim|xss_clean|is_natural|strip_tags'
            ),
            array(
                'field'     => 'ocupatii_socioprofesionale',
                'label'     => 'Ocupatii socio profesionale',
                'rules'     => 'trim|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'orig_sociala_id',
                'label'     => 'Origine sociala',
                'rules'     => 'trim|xss_clean|is_natural|strip_tags'
            ),
            array(
                'field'     => 'observatii_interne',
                'label'     => 'Observatii interne',
                'rules'     => 'trim|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'roluri[]',
                'label'     => 'Roluri',
                'rules'     => 'trim|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'seo_metadescription',
                'label'     => 'Metadescription',
                'rules'     => 'trim|xss_clean|strip_tags'
            ),
            array(
                'field'     => 'seo_metakeywords',
                'label'     => 'Metakeywords',
                'rules'     => 'trim|xss_clean|strip_tags'
            ),
        );

        $fv->set_rules($rules_config);

        if ($fv->run()) {

            if ($this->input->is_ajax_request()) {
                $objPersonaj = $pers_id ? PersonajTable::getInstance()->load($pers_id) : new PersonajItem();

                $objPersonaj->setPrefix(post('prefix'));
                $objPersonaj->setNume(post('nume'));
                $objPersonaj->setPrenume(post('prenume'));
                $objPersonaj->setNumeAnterior(post('nume_anterior'));
                $objPersonaj->setPrenumeMonahism(post('prenume_monahism'));
                $objPersonaj->setNumePorecla(post('nume_porecla'));
                $objPersonaj->setNumePseudonim(post('nume_pseudonim'));
                $objPersonaj->setAlias(post('alias'));

                $objPersonaj->setDataNastere(post('data_nastere') ? Calendar::convertDate2Mysql(post('data_nastere')) : null); // user Calnedar
                $objPersonaj->setNastereLocId(post('nastere_loc_id'));
                $objPersonaj->setDataAdormire(post('data_adormire') ? Calendar::convertDate2Mysql(post('data_adormire')) : null); // user Calnedar
                $objPersonaj->setDecesLoculMortii(post('deces_locul_mortii'));
                $objPersonaj->setDecesNumeCimitir(post('deces_nume_cimitir'));
                $objPersonaj->setDecesLocInmormantareId(post('deces_loc_inmormantare_id'));
                $objPersonaj->setDecesMormantCoordLat(post('deces_mormant_coord_lat'));
                $objPersonaj->setDecesMormantCoordLong(post('deces_mormant_coord_long'));

                $objPersonaj->setCountCopiiB(post('count_copii_b'));
                $objPersonaj->setCountCopiiF(post('count_copii_f'));
                $objPersonaj->setPrenumeTata(post('prenume_tata'));
                $objPersonaj->setPrenumeMama(post('prenume_mama'));

                $objPersonaj->setDurataDetentie(post('durata_detentie'));
                $objPersonaj->setDurataPrizonierat(post('durata_prizonierat'));
                $objPersonaj->setDurataDeportare(post('durata_deportare'));
                $objPersonaj->setDurataDomiciliuOblig(post('durata_domiciliu_oblig'));

                $objPersonaj->setObservatii(post('observatii'));
                $objPersonaj->setObservatiiUserId(post('observatie_autor'));
                $objPersonaj->setStatusId(post('status_id'));

                $objPersonaj->setIsDetinutPolitic(post('is_detinut_politic'));
                $objPersonaj->setSex(post('sex'));
                $objPersonaj->setConfesiuneId(post('confesiune_id'));
                $objPersonaj->setNationalitateId(post('nationalitate_id'));
                $objPersonaj->setOcupatiiSocioprofesionale(post('ocupatii_socioprofesionale'));
                $objPersonaj->setOrigSocialaId(post('orig_sociala_id'));

                $objPersonaj->setObservatiiInterne(post('observatii_interne'));
                $objPersonaj->setSeoMetadescription(post('seo_metadescription'));
                $objPersonaj->setSeoMetakeywords(post('seo_metakeywords'));

                PersonajTable::getInstance()->save($objPersonaj);

                // HANDLE roluri

                // remove al roluri
                if ($arrRoluri = $objPersonaj->getRoluri()) {
                    foreach ($arrRoluri as $objPers2Rol) {
                        PersonajRolTable::getInstance()->delete($objPers2Rol);
                    }
                }

                if (post('roluri')) {
                    // get roluri for validation
                    $list_roluri = RolTable::getInstance()->fetchForSelect();

                    // save new rols
                    foreach (post('roluri') as $rolID) {
                        // check if rolID is valid
                        if (key_exists($rolID, $list_roluri)) {
                            $objPersonajRol = new PersonajRolItem();
                            $objPersonajRol->setPersonajId($objPersonaj->getId());
                            $objPersonajRol->setRolId((int)$rolID);

                            PersonajRolTable::getInstance()->save($objPersonajRol);
                        }
                    }
                }

                $this->mess->displaySuccess(
                    'Felicitari conasule!!!',
                    ['redirect_url' => '/personaje/editeaza/' . $objPersonaj->getId()]
                );
            }
        } else {

            if ($this->input->is_ajax_request()) {
                $allErrors = $fv->error_array();
                echo json_encode(array('error' => $allErrors));
                exit;
            }
        }

        return false;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param int $pers_id
     * @throws Exception
     */
    public function _tab_biografie($pers_id)
    {
        $pers_id = (int)$pers_id;
        if (!$pers_id) {
            return;
        }

        $objPersonaj = PersonajTable::getInstance()->load($pers_id);

        /**
         * @var ArticolItem $objBiografie
         */
        $objBiografie = $objPersonaj->getBiografie();
        $objBiografie = !is_null($objBiografie) ? $objBiografie : new ArticolItem();

        if ($this->input->is_ajax_request()) {
            if (post()) {
                $fv = $this->form_validation;

                $fv->set_rules('titlu', 'Continut', 'trim|required');
                $fv->set_rules('continut', 'Continut', 'trim|required');
                $fv->set_rules('carte_id', 'Carte', 'trim|xss_clean|strip_tags');
                $fv->set_rules('sursa_link', 'Sursa link', 'trim|xss_clean|strip_tags');
                $fv->set_rules('sursa_titlu', 'Sursa titlu', 'trim|xss_clean|strip_tags');
                $fv->set_rules('pag_start', 'Pag start', 'trim|is_natural|xss_clean|strip_tags');
                $fv->set_rules('pag_end', 'Pag end', 'trim|is_natural|xss_clean|strip_tags');
                $fv->set_rules('user_id', 'User', 'trim|is_natural|xss_clean|strip_tags');

                $fv->set_rules('status_id', 'Status', 'trim|xss_clean|strip_tags');
                $fv->set_rules('data_publicare', 'Data publicare', 'trim|xss_clean|strip_tags');

                if ($fv->run()) {

                    // TODO - change hardcoded values
                    $objBiografie->setCatId(100); // HARDCODED
                    $objBiografie->setTitlu(post('titlu'));
                    $objBiografie->setContinut(post('continut'));
                    //                    $objBiografie->setSursaDocId(1); // 1 carte - 2 revista
                    $objBiografie->setSursaDocCarteId((int)post('carte_id'));
                    $objBiografie->setSursatitlu(post('sursa_titlu'));
                    $objBiografie->setSursalink(post('sursa_link'));
                    $objBiografie->setSursaDocStartPage((int)post('pag_start'));
                    $objBiografie->setSursaDocEndPage((int)post('pag_end'));
                    $objBiografie->setUserId((int)post('user_id'));
                    $objBiografie->setIsDeleted((int)post('status_id'));

                    $dataPublicare = null;
                    if (!empty(post('data_publicare'))) {
                        if (!Calendar::checkDate(post('data_publicare'), '/')) {
                            $this->mess->displayErrors(['data_publicare' => 'Data de publicare este invalida']);
                        } else {
                            $dataPublicare = Calendar::strToDateTime(post('data_publicare'));
                        }
                    }
                    $objBiografie->setDataPublicare($dataPublicare);

                    ArticolTable::getInstance()->save($objBiografie);
                    $biografie_id = $objBiografie->getId();

                    // update Personaj biografie ID
                    $objPersonaj->setBiografieArticolId($biografie_id);
                    PersonajTable::getInstance()->save($objPersonaj);

                    $this->mess->displaySuccess('Felicitari conasule!!!');
                } else {
                    $allErrors = $fv->error_array();
                    echo json_encode(array('error' => $allErrors));
                    exit;
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * formular Semnalmente corporale
     *
     * @param mixed $pers_id
     */
    public function _tab_semnalmente_corporale($pers_id)
    {
        $m = new Mess();

        if (post()) {
            $fv = $this->form_validation;

            $rows = array();
            foreach (post() as $key => $item) {

                // get SEMNE
                if (preg_match('/^semn_([0-9]*)/', $key, $matches)) {
                    $fv->set_rules($key, 'Semn', 'trim|strip_tags|xss_clean');
                    $rows[$matches[1]] = $item;
                }
            }

            if ($fv->run() && count($rows)) {
                foreach ($rows as $semn_id => $value) {
                    if ($this->m_personaje->fetchSemnalment($pers_id, $semn_id)) {
                        // update
                        $this->m_personaje->updateSemnalment($semn_id, $value);
                    } else {
                        // insert
                        $fields = [
                            'persID'  => $pers_id,
                            'semn_id' => $semn_id,
                            'detalii' => $value
                        ];

                        $this->m_personaje->addSemnalment($fields);
                    }
                }

                if ($this->input->is_ajax_request()) {
                    $m->displaySuccess('Semnalmentele corporale au fost salvate cu succes');
                }
            } else {
                if ($this->input->is_ajax_request()) {
                    $allErrors = $fv->error_array();
                    echo json_encode(array('error' => $allErrors));
                    exit;
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------------

    public function _tab_realizarisocioprofesionale($pers_id)
    {
        $m = new Mess();

        if (post()) {
            $fv = $this->form_validation;
            $new_rows_realizari = array();

            foreach (post() as $key => $item) {

                // ==================
                if (preg_match('/^new_realizare_input_([0-9]*)/', $key, $matches)) {
                    $fv->set_rules($key, 'Realizare', 'trim|required|xss_clean');
                    $new_rows_realizari[$matches[1]]['realizare'] = $item;
                }
            }
            $this->data['new_rows_realizari']  = $new_rows_realizari;

            if ($fv->run()) {
                // insert Distinctii in DB
                if ($new_rows_realizari) {
                    foreach ($new_rows_realizari as $item) {
                        $this->m_personaje->addRealizare($pers_id, $item);
                    }
                }

                // clear arrays
                $this->data['new_rows_realizari'] = array();

                if ($this->input->is_ajax_request()) {
                    $m->displaySuccess('Realizari socio profesionale au fost salvate cu succes');
                }
            } else {
                if ($this->input->is_ajax_request()) {
                    if ($allErrors = $fv->error_array()) {
                        echo json_encode(array('error' => $allErrors));
                    } else {
                        echo json_encode(array('error' => 'Nicio modificare'));
                    }
                    exit;
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------------

    public function _tab_episoade_represive($pers_id)
    {
        $m = new Mess();

        $condamnareID = get('condamnareID');

        if (post()) {
            $fv = $this->form_validation;

            // REGULI
            $rules_config = [
                'arest_dom_loc_id' => [
                    'field'     => 'arest_dom_loc_id',
                    'label'     => 'Localitatea la arestare',
                    'rules'     => 'trim|xss_clean|is_natural_no_zero|strip_tags'
                ],
                'arest_dom_strada' => [
                    'field'     => 'arest_dom_strada',
                    'label'     => 'Strada',
                    'rules'     => 'trim|xss_clean|strip_tags'
                ],
                'arest_ocupatie_id' => [
                    'field'     => 'arest_ocupatie_id',
                    'label'     => 'Ocupatia la arestare',
                    'rules'     => 'trim|xss_clean|is_natural_no_zero|strip_tags'
                ],
                'arest_apart_politica_id' => [
                    'field'     => 'arest_apart_politica_id',
                    'label'     => 'Apartenenta politica la arestare',
                    'rules'     => 'trim|xss_clean|is_natural_no_zero|strip_tags'
                ],
                'pre_arest_apart_politica_id' => [
                    'field'     => 'pre_arest_apart_politica_id',
                    'label'     => 'Apartenenta politica inainte de arestare',
                    'rules'     => 'trim|xss_clean|is_natural_no_zero|strip_tags'
                ],
                'stare_civila_id' => [
                    'field'     => 'stare_civila_id',
                    'label'     => 'Stare civila la arestare',
                    'rules'     => 'trim|xss_clean|is_natural_no_zero|strip_tags'
                ],
                'represiune_tip_id' => [
                    'field'     => 'represiune_tip_id',
                    'label'     => 'Tip condamnare',
                    'rules'     => 'trim|xss_clean|required|is_natural_no_zero|strip_tags'
                ],
                'represiune_data_start' => [
                    'field'     => 'represiune_data_start',
                    'label'     => 'Represiune data inceput',
                    'rules'     => 'trim|xss_clean|strip_tags'
                ],
                'represiune_data_end' => [
                    'field'     => 'represiune_data_end',
                    'label'     => 'Represiune data sfarsit',
                    'rules'     => 'trim|xss_clean|strip_tags'
                ],

                'condamnare_instanta_id' => [
                    'field'     => 'condamnare_instanta_id',
                    'label'     => 'Instanta de condamnare',
                    'rules'     => 'trim|xss_clean|is_natural_no_zero|strip_tags'
                ],
                'cond_fapta' => [
                    'field'     => 'cond_fapta',
                    'label'     => 'Fapta',
                    'rules'     => 'trim|xss_clean|strip_tags'
                ],
                'cond_fapta_short' => [
                    'field'     => 'cond_fapta_short',
                    'label'     => 'Desc. pe scurt a faptei',
                    'rules'     => 'trim|xss_clean|strip_tags'
                ],
                'cond_fapta_lege' => [
                    'field'     => 'cond_fapta_lege',
                    'label'     => 'Incadrarea legala a faptei',
                    'rules'     => 'trim|xss_clean|strip_tags'
                ],

                'arest_data_arestare' => [
                    'field'     => 'arest_data_arestare',
                    'label'     => 'Data arestare',
                    'rules'     => 'trim|xss_clean|strip_tags'
                ],
                'arest_institutie_arestare' => [
                    'field'     => 'arest_institutie_arestare',
                    'label'     => 'Instituție arestare',
                    'rules'     => 'trim|xss_clean|strip_tags'
                ],
                'arest_nr_mandat' => [
                    'field'     => 'arest_nr_mandat',
                    'label'     => 'Nr mandat arestare',
                    'rules'     => 'trim|xss_clean|strip_tags'
                ],
                'arest_data_mandat' => [
                    'field'     => 'arest_data_mandat',
                    'label'     => 'Data mandat arestare',
                    'rules'     => 'trim|xss_clean|strip_tags'
                ],
                'arest_emitent_mandat' => [
                    'field'     => 'arest_emitent_mandat',
                    'label'     => 'Emitent mandat arestare',
                    'rules'     => 'trim|xss_clean|strip_tags'
                ],

                'sentinta_id' => [
                    'field'     => 'sentinta_id',
                    'label'     => 'Nr sentinta',
                    'rules'     => 'trim|xss_clean|is_natural_no_zero|strip_tags'
                ],
                'cond_pedeapsa_durata' => [
                    'field'     => 'cond_pedeapsa_durata',
                    'label'     => 'Durata pedepsei',
                    'rules'     => 'trim|xss_clean|strip_tags'
                ],
                'cond_pedeapsa_felul_id' => [
                    'field'     => 'cond_pedeapsa_felul_id',
                    'label'     => 'Felul pedepsei',
                    'rules'     => 'trim|xss_clean|is_natural_no_zero|strip_tags'
                ],
                'cond_pedeapsa_data_start' => [
                    'field'     => 'cond_pedeapsa_data_start',
                    'label'     => 'Data incepere pedeapsa',
                    'rules'     => 'trim|xss_clean|strip_tags'
                ],
                'cond_pedeapsa_data_end' => [
                    'field'     => 'cond_pedeapsa_data_end',
                    'label'     => 'Data sfarsit pedeapsa',
                    'rules'     => 'trim|xss_clean|strip_tags'
                ],
                'cond_iesire_data' => [
                    'field'     => 'cond_iesire_data',
                    'label'     => 'Data iesire',
                    'rules'     => 'trim|xss_clean|strip_tags'
                ],
                'cond_iesire_tip_id' => [
                    'field'     => 'cond_iesire_tip_id',
                    'label'     => 'Tip iesire',
                    'rules'     => 'trim|xss_clean|strip_tags'
                ],
                'cond_iesire_doc' => [
                    'field'     => 'cond_iesire_doc',
                    'label'     => 'Document iesire',
                    'rules'     => 'trim|xss_clean|strip_tags'
                ],
                'observatii' => [
                    'field'     => 'observatii',
                    'label'     => 'Observatii',
                    'rules'     => 'trim|xss_clean|strip_tags'
                ]
            ];

            $fv->set_rules($rules_config);

            if (!$fv->run() && $this->input->is_ajax_request()) {
                $m->displayErrors($fv->error_array());
            }

            if ($m->hasErrors()) {
                $m->displayErrors();
            }

            $fields = ['personaj_id' => (int)$pers_id];
            foreach (post() as $key => $item) {
                if (!isset($rules_config[$key])) {
                    continue;
                }

                $dateList = [
                    'cond_pedeapsa_data_start',
                    'cond_pedeapsa_data_end',
                    'cond_iesire_data',
                    'arest_data_arestare',
                    'arest_data_mandat',
                    'represiune_data_start',
                    'represiune_data_end'
                ];
                if (in_array($key, $dateList)) {
                    $fields[$key] = (!empty($item) && Calendar::checkPartialDate($item, '/')) ? Calendar::convertPartialDate2Mysql($item) : null;
                } else {
                    $fields[$key] = $item ?? null;
                }
            }

            if ($fv->run()) {
                // insert Condamnarea in DB
                if ($condamnareID) {
                    // edit
                    $this->m_personaje_condamnari->update($pers_id, $condamnareID, $fields);
                    if ($this->input->is_ajax_request()) {
                        $m->displaySuccess('Modificarile au fost salvate cu succes');
                    }
                } else {
                    // add
                    $this->m_personaje_condamnari->add($fields);
                    if ($this->input->is_ajax_request()) {
                        $m->displaySuccess('Condamnarea a fost salvata cu succes');
                    }
                }
            } else {
                if ($this->input->is_ajax_request()) {
                    $m->displayErrors($fv->error_array());
                }
            }
        }

        if ($condamnareID) {
            $this->data['info_condamnare'] = $this->m_personaje->fetchCondamnare($pers_id, $condamnareID);
            $this->data['condamnareID'] = (int)$condamnareID;
        }
    }

    // ---------------------------------------------------------------------------------------------

    public function _tab_itinerariu_detentie($pers_id)
    {
        if (post()) {
            $fv = $this->form_validation;
            $new_rows = array();
            foreach (post() as $key => $item) {
                if (preg_match('/^new_loc_patimire_id_([0-9]*)/', $key, $matches)) {
                    $fv->set_rules($key, 'Loc patimire', 'trim|xss_clean|is_natural|required');
                    $new_rows[$matches[1]]['loc_patimire_id'] = $item;
                }

                // parse date time
                $new_rows = $this->parseDateTimeIntervalAndExtractNewRows($key, $fv, $item, $new_rows);

                if (preg_match('/^new_condamnare_id_([0-9]*)/', $key, $matches)) {
                    $fv->set_rules($key, 'Condamnare', 'trim|xss_clean|required');
                    $new_rows[$matches[1]]['condamnare_id'] = $item;
                }

                if (preg_match('/^new_observatii_([0-9]*)/', $key, $matches)) {
                    $fv->set_rules($key, 'Observatii', 'trim|xss_clean');
                    $new_rows[$matches[1]]['observatii'] = $item;
                }
            }

            if ($fv->run()) {
                // prepare for Insert new Rows
                foreach ($new_rows as $key => $row) {
                    $fields = array(
                        'personaj_id'    => (int)$pers_id,
                        'loc_patimire_id' => (int)$row['loc_patimire_id'],
                        'data_start'     => Calendar::validateDate(array($row['data_start_zi'], $row['data_start_luna'], $row['data_start_an'])),
                        'data_end'       => Calendar::validateDate(array($row['data_end_zi'], $row['data_end_luna'], $row['data_end_an'])),
                        'condamnare_id'  => (int)$row['condamnare_id'],
                        'order_id'       => $key + 1,
                        'observatii'     => $row['observatii']
                    );

                    $this->m_personaje_itinerarii->add($fields);
                }

                // update the other data
                if ($this->input->is_ajax_request()) {
                    $this->mess->displaySuccess('Itinerariu detentie a fost adaugat');
                }

                // View - reset rows
                $new_rows = [];
            } else {
                if ($this->input->is_ajax_request()) {
                    $allErrors = $fv->error_array();
                    echo json_encode(array('error' => $allErrors ?: 'Nicio modificare'));
                    exit;
                }
            }

            // for View - to refill the fields
            $this->data['new_rows'] = $new_rows;
        }
    }

    // ---------------------------------------------------------------------------------------------

    public function _tab_pedepse_disciplinare($pers_id)
    {
        if (post()) {
            $fv = $this->form_validation;
            $new_rows = array();
            foreach (post() as $key => $item) {
                // parse date time
                $new_rows = $this->parseDateTimeIntervalAndExtractNewRows($key, $fv, $item, $new_rows);

                if (preg_match('/^new_nr_zile_([0-9]*)/', $key, $matches)) {
                    $fv->set_rules($key, 'Nr zile', 'trim|xss_clean|is_natural');
                    $new_rows[$matches[1]]['nr_zile'] = $item;
                }

                if (preg_match('/^new_pedeapsa_input_([0-9]*)/', $key, $matches)) {
                    $fv->set_rules($key, 'Pedeapsa_input', 'trim|xss_clean|required');
                    $new_rows[$matches[1]]['pedeapsa_input'] = $item;
                }
            }

            if ($fv->run()) {
                // prepare for Insert new Rows
                foreach ($new_rows as $key => $row) {
                    $fields = array(
                        'personaj_id'   => (int)$pers_id,
                        'data_start'    => Calendar::validateDate(array($row['data_start_zi'], $row['data_start_luna'], $row['data_start_an'])),
                        'data_end'      => Calendar::validateDate(array($row['data_end_zi'], $row['data_end_luna'], $row['data_end_an'])),
                        'nr_zile'       => (int)$row['nr_zile'],
                        'content'       => $row['pedeapsa_input'],
                        'order_id'      => $key + 1
                    );

                    $this->m_personaje->addPedeapsaDisciplinara($fields);
                }

                // update the other data
                if ($this->input->is_ajax_request()) {
                    $this->mess->displaySuccess('Pedepsele disciplinare au fost inregistrate');
                }

                // View - reset rows
                $new_rows = array();
            } else {
                if ($this->input->is_ajax_request()) {
                    if ($allErrors = $fv->error_array()) {
                        echo json_encode(array('error' => $allErrors));
                    } else {
                        echo json_encode(array('error' => 'Nicio modificare'));
                    }
                    exit;
                }
            }

            // for View - to refill the fields
            $this->data['new_rows'] = $new_rows;
        }
    }

    // ---------------------------------------------------------------------------------------------

    function _tab_carti($pers_id)
    {
        if (post()) {
            $new_rows = [];

            foreach (post('book') as $bookID) {
                $bookID  = (int)$bookID;
                $bookTip = post('booktip_' . $bookID);

                $new_rows[$bookID]['carte_id']    = $bookID;
                $new_rows[$bookID]['personaj_id'] = (int)$pers_id;
                $new_rows[$bookID]['carte_tip']   = !empty($bookTip) ? (int)$bookTip : null;
            }

            // prepare for Insert new Rows
            $result = false;
            foreach ($new_rows as $row) {
                $fields = array(
                    'carte_id'       => $row['carte_id'],
                    'personaj_id'    => $row['personaj_id'],
                    'carte_tip'      => $row['carte_tip']
                );

                $result = $this->m_personaje_carti->add((int)$pers_id, $fields);
            }

            // update the other data
            if ($this->input->is_ajax_request()) {
                if ($result) {
                    $this->mess->displaySuccess('Cartile au fost asignate personajului');
                } else {
                    $this->mess->displayErrors('Cartile nu au putut fi asignate personajului');
                }
            }
        }

        $this->data['list_personaje2carti_tip']    = unserialize(PERSONAJE2CARTI_TIP);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * TAB - CITATE - citate atribuite personajului
     *
     * @param mixed $pers_id
     */
    function _tab_citate($pers_id)
    {
        if ($citatID = get('citatID')) {
            $this->data['info_citat'] = $this->m_citate->getInfo($citatID);
            $this->data['citatID'] = (int)$citatID;
        }

        if ($this->input->is_ajax_request()) {
            if (post()) {
                $fv = $this->form_validation;

                $fv->set_rules('continut', 'Continut',    'trim|required');
                $fv->set_rules('carte_id', 'Carte',       'trim|xss_clean|strip_tags');
                $fv->set_rules('sursa',     'Sursa',     'trim|xss_clean|strip_tags');
                $fv->set_rules('pag_start', 'Pag start',  'trim|is_natural|xss_clean|strip_tags');
                $fv->set_rules('pag_end', 'Pag end',      'trim|is_natural|xss_clean|strip_tags');
                $fv->set_rules('observatii', 'Observatii', 'trim|xss_clean|strip_tags');

                if ($fv->run()) {
                    $fields = array(
                        'continut'      => post('continut'),
                        'carte_id'      => !empty(post('carte_id')) ? post('carte_id') : null,
                        'sursa'         => !empty(post('sursa')) ? post('sursa') : null,
                        'observatii'    => !empty(post('observatii')) ? post('observatii') : null,
                        'pag_start'     => !empty(post('pag_start')) ? post('pag_start') : null,
                        'pag_end'       => !empty(post('pag_end')) ? post('pag_end') : null
                    );

                    if ($citatID) {
                        // update citat
                        if ($this->m_citate->update($citatID, $fields)) {
                            $this->mess->displaySuccess('Citatul a fost actualizat');
                        }
                    } else {
                        if ($this->m_citate->add((int)$pers_id, $fields)) {
                            $this->mess->displaySuccess('Citatul a fost adăugat');
                        }
                    }
                } else {
                    $allErrors = $fv->error_array();
                    echo json_encode(array('error' => $allErrors));
                    exit;
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * functie apelata prin AJAX pentru stergerea unei functii
     *
     * @param mixed $persID
     * @param mixed $funcID
     */
    function stergeFunctie($persID, $funcID)
    {
        $m = new Mess();

        if (empty($persID) || empty($funcID)) {
            $m->addError("Persoana sau functie invalida");
        }

        if ($m->hasErrors()) {
            $m->displayErrors();
        }

        // Save data.
        if ($this->m_functii->deleteFunctiePersonaj($persID, $funcID)) {
            $m->displaySuccess(true);
        } else {
            $m->displayErrors('Actiunea nu s-a putut realiza');
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * functie apelata prin AJAX pentru stergerea unei realizari
     *
     * @param mixed $persID
     * @param mixed $id
     */
    public function stergRealizare($persID, $id)
    {
        $m = new Mess();

        if (empty($persID) || empty($id)) {
            $m->addError("Persoana sau realizare invalida");
        }

        if ($m->hasErrors()) {
            $m->displayErrors();
        }

        // Save data.
        if ($this->m_personaje->stergRealizare($persID, $id)) {
            $m->displaySuccess(true);
        } else {
            $m->displayErrors('Actiunea nu s-a putut realiza');
        }
    }

    // ---------------------------------------------------------------------------------------------

    function stergeCondamnare($persID, $condamnareID)
    {
        $m = new Mess();

        if (empty($persID) || empty($condamnareID)) {
            $m->addError("Persoana sau condamnareID invalida");
        }

        if ($m->hasErrors()) {
            $m->displayErrors();
        }

        // Save data.
        if ($this->m_personaje_condamnari->delete($persID, $condamnareID)) {
            $m->displaySuccess(true);
        } else {
            $m->displayErrors('Actiunea nu s-a putut realiza');
        }
    }
    // ---------------------------------------------------------------------------------------------

    /**
     * functie apelata prin AJAX pentru stergerea unui itinerareiu detentie
     *
     * @param mixed $persID
     * @param mixed $itemID - itinerariuID
     */
    public function stergeItinerariu($persID, $itemID)
    {
        $m = new Mess();

        if (empty($persID) || empty($itemID)) {
            $m->addError("Persoana sau itinerariu invalid");
        }

        if ($m->hasErrors()) {
            $m->displayErrors();
        }

        // Save data.
        if ($this->m_personaje->deleteItinerariu($persID, $itemID)) {
            $m->displaySuccess(true);
        } else {
            $m->displayErrors('Actiunea nu s-a putut realiza');
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * functie apelata prin AJAX pentru stergerea unei pedepse
     *
     * @param mixed $persID
     * @param mixed $pedeapsaID
     */
    function stergePedeapsa($persID, $pedeapsaID)
    {
        $m = new Mess();

        if (empty($persID) || empty($pedeapsaID)) {
            $m->addError("Persoana sau pedeapsă invalida");
        }

        if ($m->hasErrors()) {
            $m->displayErrors();
        }

        // Save data.
        if ($this->m_personaje->deletePedeapsaDisciplinaraPersonaj($persID, $pedeapsaID)) {
            $m->displaySuccess(true);
        } else {
            $m->displayErrors('Actiunea nu s-a putut realiza');
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * AJAX function - order
     *
     */
    public function orderPedepseDisciplinare()
    {
        $m = new Mess();

        $this->db->trans_start();
        foreach (post('order') as $order_id => $itemID) {
            $this->m_personaje->updatePedeapsaDisciplinara($itemID, array('order_id' => (int)$order_id));
        }
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $m->addError('MySql error. Încearca din nou!');
        } else {
            $m->displaySuccess('Ordinea a fost salvată cu succes');
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * AJAX function - order
     *
     */
    public function orderItinerariiDetentie()
    {
        $m = new Mess();

        $this->db->trans_start();
        foreach (post('order') as $order_id => $itemID) {
            $this->m_personaje_itinerarii->update($itemID, array('order_id' => (int)$order_id));
        }
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $m->displayErrors('MySql error. Încearca din nou!');
        } else {
            $m->displaySuccess('Ordinea a fost salvată cu succes');
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * functie apelata prin AJAX pentru stergerea unei Carti atribuite Personajului
     *
     * @param mixed $persID
     * @param mixed $p2cID - personaj2carteID
     */
    function stergePers2Carte($persID, $p2cID)
    {
        $m = new Mess();

        if (empty($persID) || empty($p2cID)) {
            $m->addError("Persoana sau carte invalida");
        }

        if ($m->hasErrors()) {
            $m->displayErrors();
        }

        // Save data
        if ($this->m_personaje_carti->delete($persID, $p2cID)) {
            $m->displaySuccess('Cartea atribuita personajului a fost stearsa');
        } else {
            $m->displayErrors('Actiunea nu s-a putut realiza');
        }
    }

    // ---------------------------------------------------------------------------------------------

    function stergeCitat($persID, $id)
    {
        $m = new Mess();

        if (empty($persID) || empty($id)) {
            $m->addError("Persoana sau citatul invalid");
        }

        if ($m->hasErrors()) {
            $m->displayErrors();
        }

        // Save data.
        if ($this->m_citate->delete($persID, $id)) {
            $m->displaySuccess(true);
        } else {
            $m->displayErrors('Actiunea nu s-a putut realiza');
        }
    }
    // ---------------------------------------------------------------------------------------------


    /**
     * callback function - used in form validation
     * check the valability of a partial date
     *
     * @param mixed $str
     * @param mixed $param (pattern + row number)
     * @return bool
     */
    public function _validate_date($str, $param): bool
    {
        $param = explode(',', $param);

        $pattern = $param[0];
        $row_id  = $param[1];

        // if date is empty - skip it
        $zi     = post($pattern . '_zi_' . $row_id);
        $luna   = post($pattern . '_luna_' . $row_id);
        $an     = post($pattern . '_an_' . $row_id);
        if (empty($zi) && empty($luna) && empty($an)) {
            return true;
        }

        if (!Calendar::validateDate(array(post($pattern . '_zi_' . $row_id), post($pattern . '_luna_' . $row_id), post($pattern . '_an_' . $row_id)))) {
            $this->form_validation->set_message('_validate_date', 'Data este invalidă: ' . $str);
            return false;
        } else {
            return true;
        }
    }

    // ---------------------------------------------------------------------------------------------

    public function cautaLocalitate()
    {
        $searchTerm = isset($_GET['query']) ? trim(strip_tags($_GET['query'])) : null;
        if (!$searchTerm) {
            $this->displayAutocompleteResults($searchTerm, array());
        }

        // Initialize data.
        $results = array();

        // Fetch data.
        $data = $this->m_localitati->fetchForAutocomplete($searchTerm);
        if ($data && count($data)) {
            foreach ($data as $v) {
                $results[] = array(
                    'value' => $v['nume_complet'],
                    'id' => $v['id'],
                    'data' => array('category' => 'Localitate'),
                );
            }
        }

        // Return results.
        $this->displayAutocompleteResults($searchTerm, $results);
    }

    // ---------------------------------------------------------------------------------------------

    public function cautaNume()
    {
        $searchTerm = isset($_GET['query']) ? trim(strip_tags($_GET['query'])) : null;
        if (!$searchTerm) {
            $this->displayAutocompleteResults($searchTerm, array());
        }

        // Initialize data.
        $results = array();

        // Fetch data.
        $data = $this->m_personaje->fetchForAutocomplete($searchTerm);
        if ($data && count($data)) {
            foreach ($data as $v) {
                $results[] = array(
                    'value' => $v['nume_complet'],
                    'id'    => $v['id'],
                    'data'  => array('category' => 'Nume Prenume'),
                );
            }
        }

        // Return results.
        $this->displayAutocompleteResults($searchTerm, $results);
    }

    // ---------------------------------------------------------------------------------------------

    public function cautaId()
    {
        $searchTerm = isset($_GET['query']) ? trim(strip_tags($_GET['query'])) : null;
        if (!$searchTerm) {
            $this->displayAutocompleteResults($searchTerm, array());
        }

        // Initialize data.
        $results = array();

        // Fetch data.
        $data = $this->m_personaje->fetchIdForAutocomplete($searchTerm);
        if ($data && count($data)) {
            foreach ($data as $v) {
                $results[] = array(
                    'value' => $v['id'],
                    'id' => $v['id'],
                    'data' => array('category' => 'Id personaj'),
                );
            }
        }

        // Return results.
        $this->displayAutocompleteResults($searchTerm, $results);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * returneasa lista de carti din Biblioteca
     *
     */
    function getListaCartiBiblioteca()
    {
        $getter = get();
        $countRows = $this->db->count_all('fcp_biblioteca_carti');

        $columns = array(
            '0' => 'carte_id',
            '2' => 'autori',       // autor
            '3' => 'carte_titlu',
            '4' => 'editura',
            '5' => 'anul_publicatiei'
        );

        // prepare result
        $response = array(
            'draw'            =>    $getter['draw'] + 1,
            'recordsTotal'    =>    $countRows,
            'recordsFiltered' =>    $countRows,
            'data'            =>    array()
        );

        // columns order
        $columnsOrder = array();
        foreach ($getter['order'] as $item) {
            $columnsOrder[] = $columns[$item['column']] . ' ' . $item['dir'];
        }

        // search
        $search_text = ($getter['search']['value']) ?: false;

        $list = $this->m_biblioteca_carti->getAllWithFilters($getter['start'], $getter['length'], $columnsOrder, $search_text);

        if ($search_text) {
            $response['recordsFiltered'] = count($list);
        }

        foreach ($list as $item) {
            $actiuni = '<a href="javascript:void(0)" class="glyphicon glyphicon-arrow-left cta importrow" title="Importa carte"></a> ';

            $response['data'][] = [
                $actiuni . '<span class="row_id">' . $item['carte_id'] . '</span>',
                '<span class="row_autor">' . $item['autori'] . '</span>',
                '<span class="row_titlu">' . $item['carte_titlu'] . '</span>',
                '<span class="row_an">' . (($item['anul_publicatiei']) ?: '') . '</span>'
            ];
        }

        echo json_encode($response);
        die;
    }

    /**
     * @param string $key
     * @param CI_Form_validation $fv
     * @param mixed $item
     * @param array $newRows
     * @return array
     */
    private function parseDateTimeIntervalAndExtractNewRows(string $key, CI_Form_validation $fv, $item, array $newRows): array
    {
        foreach (['zi', 'luna', 'an'] as $time_interval) {
            // get data start
            if (preg_match('/^new_data_start_' . $time_interval . '_([0-9]*)/', $key, $matches)) {
                $fv->set_rules(
                    $key,
                    'Data start ' . $time_interval,
                    'trim|xss_clean|is_natural' . (($time_interval == 'an') ? '|callback__validate_date[new_data_start,' . $matches[1] . ']' : '')
                );
                $newRows[$matches[1]]['data_start_' . $time_interval] = $item;
            }

            // get data end
            if (preg_match('/^new_data_end_' . $time_interval . '_([0-9]*)/', $key, $matches)) {
                $fv->set_rules($key, 'Data end ' . $time_interval, 'trim|xss_clean|is_natural' . (($time_interval == 'an') ? '|callback__validate_date[new_data_end,' . $matches[1] . ']' : ''));
                $newRows[$matches[1]]['data_end_' . $time_interval] = $item;
            }
        }

        return $newRows;
    }
}
