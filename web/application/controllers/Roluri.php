<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Roluri extends Admin_Controller
{

    private $viewPath = 'admin/generale/roluri/';

    /**
     * @var $mess Mess
     */
    public $mess;

    /**
     *
     * @var $m_roluri M_roluri
     */
    public $m_roluri;

    function __construct()
    {
        parent::__construct();

        $this->load->model('m_roluri');
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Listare roluri
     */
    public function index()
    {
        // Get data.
        $data = $this->m_roluri->fetchList();

        // Set data.
        $viewData = array(
            'items' => $data,

            // Template data
            'page_title' => 'Lista roluri',
            'breadcrumb' => array(
                array(
                    'name' => 'Lista roluri',
                ),
            ),
        );

        // Render view.
        $this->_render($this->viewPath . 'list', $this->defaultTemplate, $viewData);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Adauga rol nou
     *
     */
    public function add()
    {
        $this->edit();
    }

    /**
     * Editeaza rol
     * @param int|bool $id
     */
    function edit($id = null)
    {
        $this->data['edit']   = $edit = ($id) ? (int)$id : null;

        // Get data by id
        if ($id) {
            $data = $this->m_roluri->load($id);
        } else {
            $data = array(
                'rol_nume' => null,
                'rol_detalii' => null,
                'parent_id' => 0,
            );
        }
        $this->data['data'] = $data;

        // ------------------------------

        // Parse post data
        if (post()) {

            /** @var CI_Form_validation $fv */
            $fv = $this->form_validation;

            // REGULI
            $rules_config = array(
                array(
                    'field'     => 'rol_nume',
                    'label'     => 'Nume',
                    'rules'     => 'trim|required|min_length[3]|xss_clean|strip_tags'
                ),
                array(
                    'field'     => 'rol_detalii',
                    'label'     => 'Detalii',
                    'rules'     => 'trim|xss_clean|strip_tags'
                ),
                array(
                    'field'     => 'parinte_id',
                    'label'     => 'Parinte',
                    'rules'     => 'trim|xss_clean|is_natural|strip_tags'
                ),
            );

            $fv->set_rules($rules_config);

            if ($fv->run()) {

                // Verifica unicitatea numelui
                $res = $this->m_roluri->fetchByName(post('rol_nume'), $id);
                if ($res && count($res)) {
                    $this->mess->displayErrors(array('rol_nume' => 'Rolul nu poate fi salvat. Exista un alt rol cu aceeasi denumire'));
                }

                try {
                    $this->m_roluri->save(post(), $id);
                } catch (Exception $e) {
                    $this->mess->displayErrors(array('rol_nume' => 'Datele nu pot fi salvate in baza de date -> ' . $e->getMessage()));
                }


                if ($this->input->is_ajax_request()) {
                    $this->mess->displaySuccess('Datele au fost salvate cu succes');
                }
            } else {

                if ($this->input->is_ajax_request()) {
                    $allErrors = $fv->error_array();
                    echo json_encode(array('error' => $allErrors));
                    exit;
                }
            }
        }

        // ------------------------------

        // Template data
        $this->data['page_title'] = $edit ? 'Editează rol' : 'Adaugă rol';
        $this->data['breadcrumb'] = array(
            array(
                'name' => 'Lista roluri',
                'href' => '/roluri/',
            ),
            array(
                'name' => $edit ? 'Editează rol' : 'Adaugă rol',
            ),
        );
        $this->data['parentItems'] = $this->m_roluri->fetchParents();

        $this->_render($this->viewPath . 'edit', $this->defaultTemplate);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Sterge rol
     * @param int $id
     */
    function delete($id)
    {
        $m = new Mess();

        $id = (int)$id;
        if (!$id) {
            $m->displayErrors("ID rol invalid");
        }

        if (!post('submited')) {
            $m->displayErrors("Formular invalid");
        }

        $data = $this->m_roluri->load($id);
        if (!$data || !count($data)) {
            $m->displayErrors("Rolul nu a fost gasit in baza de date");
        }

        // Verifica daca rolul este folosit deja.
        if ($data['is_used']) {
            $m->displayErrors("Rolul nu poate fi sters. Este folosit in relatia cu un personaj");
        }

        // Delete data.
        try {
            $this->m_roluri->delete($id);
            $m->displaySuccess(true);
        } catch (Exception $e) {
            $m->displayErrors('Nu am putut sterge rolul din baza de date -> ' . $e->getMessage());
        }
    }

    // ---------------------------------------------------------------------------------------------



}
