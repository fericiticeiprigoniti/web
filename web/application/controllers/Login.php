<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends Admin_Controller
{
    /**
     * Dashboard constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Login page
     */
    public function index()
    {
        $this->data['login_error'] = false;

        if (post()) {
            $username = post('username');
            $password = post('password');

            if ($this->aauth->login($username, $password, true)) {
                redirect('/index');
            } else {
                $this->data['login_error'] = true;
            }
        }

        $this->_render('admin/login', false);
    }

    /**
     * Logout
     */
    public function logout()
    {
        $this->aauth->logout();
        redirect('/');
    }

    /**
     * Connect to Asana using it's API and fetch tasks
     */
    private function dashboard()
    {
        // Set data.
        $viewData = array(
            // Template data
            'page_title' => 'Asana - Tasks',
            'breadcrumb' => array(),
        );

        // Render view.
        $this->_render('admin/main_dashboard', $this->defaultTemplate, $viewData);
    }
}
