<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Poezii extends Admin_Controller
{

    private $viewPath = 'admin/poezii/';

    // ---------------------------------------------------------------------------------------------

    function __construct()
    {

        parent::__construct();
        $this->load->helper('form');
        $this->load->model('m_personaje');
        $this->load->model('m_biblioteca_carti');

        // active Menu
        array_push($this->activeMenu, 'poezii');

        $this->load->library('pagination');
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * top Filters (work in progress)
     * List all
     *
     */
    public function index()
    {
        // Filters.
        $getData = $_GET;
        $filters = new PoezieFilters($getData);
        $filters->setParentId(0);

        // Limit.
        $page    = isset($_GET['_page']) ? (int)$_GET['_page'] : 1;
        $perPage = isset($_GET['_per_page']) ? (int)$_GET['_per_page'] : 50;
        $offset  = ($page - 1) * $perPage;

        // Fetch data
        $data = PoezieTable::getInstance()->fetchAll($filters, $perPage, $offset);

        // Get data.
        if (!isset($data['items']) && !isset($data['count'])) {
            die('Nu am putut prelua informatiile din baza de date');
        }

        // -----------------------------------

        // Initialize pagination.
        /**
         * @var array $pConfig
         */
        $pConfig = $this->config->item('pagination');
        $pConfig['base_url'] = General::url(array('_page' => null));
        $pConfig['total_rows'] = $data['count'];
        $pConfig['cur_page'] = $page;
        $pConfig['per_page'] = $perPage;
        $this->pagination->initialize($pConfig);

        // -----------------------------------

        // Set data.
        $viewData = [
            'filters'       => $filters,
            'items'         => $data['items'],
            'pagination'    => $this->pagination->create_links(),
            'pag' => [
                'start'         => $offset,
                'perPage'       => $perPage,
                'count'         => $data['count'],
                'itemsPerPage'  => [10, 20, 50, 100, 200, 500],
            ],
            'list_taguri'   => TagTable::getInstance()->fetchForSelect(),

            // Template data
            'page_title' => 'Listă poezii',
            'breadcrumb' => [
                [
                    'name' => 'Listă poezii',
                ]
            ],
        ];

        // active Menu
        array_push($this->activeMenu, 'list_poezii');

        // Render view.
        $this->_render($this->viewPath . 'list', $this->defaultTemplate, $viewData);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @throws Exception
     */
    public function add()
    {
        $this->edit();
    }

    /**
     * funtia de editare Poezie
     *
     * @param mixed $id
     * @throws Exception
     */
    public function edit($id = false)
    {
        //FIXME - Is this really necessary?
        ini_set('memory_limit', '2048M'); // or you could use 1G

        $id = $id ? (int)$id : null;

        // Get data by id
        if ($id) {
            if (!$objPoezie = PoezieTable::getInstance()->load($id)) show_404();

            // variante poezii
            $varPoeziiFilter = new PoezieFilters();
            $varPoeziiFilter->setParentId($id);
            $varPoeziiFilter->setOrderBy([$varPoeziiFilter::ORDER_BY_ORDER_ID_ASC]);

            $variantePoezii = PoezieTable::getInstance()->fetchAll($varPoeziiFilter);
        } else {
            $objPoezie = new PoezieItem();
        }

        /**
         * get Varianta
         * default - set the first item from list
         */
        if (get('v_poezie_id')) {
            if (!$varObjPoezie = PoezieTable::getInstance()->load((int)get('v_poezie_id'))) show_404();
        } elseif (get('varianta_noua')) {
            $varObjPoezie = new PoezieItem();
        } else {
            $varObjPoezie = isset($variantePoezii) && $variantePoezii['count'] ? $variantePoezii['items'][0] : new PoezieItem();
        }

        // selecteaza TAB activ + Submit Form
        switch (get('tab')) {
            case 'media':
                $this->_tab_media();
                $tabActiv = 'media';
                break;

            case 'variante':
                $this->_tab_variante($objPoezie, $varObjPoezie);
                $tabActiv = 'variante';
                break;

            case 'comentariu':
                $this->_tab_comentariu($objPoezie);
                $tabActiv = 'comentariu';
                break;

            case 'povestea':
                $this->_tab_povestea($objPoezie);
                $tabActiv = 'povestea';
                break;

            default:
                $this->_tab_generale($objPoezie);
                $tabActiv = 'generale';
                break;
        }

        // prepare user list for dropdown
        $useriObs = $this->aauth->list_users();
        $list_useri = [];
        foreach ($useriObs as $obj) {
            $list_useri[$obj->id] = $obj->username;
        }

        // filter only Personaje - rol Poeti
        $filters = [
            'rol' => [ROL_POET]
        ];
        $list_poeti = $this->m_personaje->fetchAll($filters);

        // Fetch active tag list
        $filters = new TagFilters();
        $filters->setIsDeleted(false);
        $filters->setOrderBy([$filters::ORDER_BY_NAME_ASC]);
        $activeTagList = TagTable::getInstance()->fetchForSelect($filters);

        // Fetch `picioare metrice` list :P
        $filters = new PiciorMetricFilters();
        $filters->setOrderBy([$filters::ORDER_BY_NAME_ASC]);
        $pmList = PiciorMetricTable::getInstance()->fetchForSelect($filters);

        // Fetch rhyme list
        $filters = new RimaFilters();
        $filters->setOrderBy([$filters::ORDER_BY_NAME_ASC]);
        $rhymeList = RimaTable::getInstance()->fetchForSelect($filters);

        // Fetch poetry species
        $filters = new SpeciePoezieFilters();
        $filters->setGenLiterarId(1);
        $filters->setOrderBy([$filters::ORDER_BY_NAME_ASC]);
        $lyricSpecies = SpeciePoezieTable::getInstance()->fetchForSelect($filters);

        $filters->setGenLiterarId(2);
        $epicSpecies = SpeciePoezieTable::getInstance()->fetchForSelect($filters);

        // Fetch creation period list :P
        $filters = new PerioadaCreatieFilters();
        $filters->setOrderBy([$filters::ORDER_BY_NAME_ASC]);
        $creationPeriodList = PerioadaCreatieTable::getInstance()->fetchForSelect($filters);

        // Fetch verse structure list
        $filters = new StructuraStrofaFilters();
        $filters->setOrderBy([$filters::ORDER_BY_ID_ASC]);
        $verseStructureList = StructuraStrofaTable::getInstance()->fetchForSelect($filters);

        // Template data
        $viewData = array(
            'poezie'           => $objPoezie,
            'varObjPoezieSeleted'     => $varObjPoezie,
            'tab_activ'        => $tabActiv,
            'edit'             => ($id) ? (int)$id : FALSE,

            // selectoare
            'poet_selectat'         => false,
            'list_autori'           => $list_poeti['items'],
            'list_carti'            => $this->m_biblioteca_carti->fetchAllForDropDown(),
            'list_publicatii'       => [],
            'list_taguri'           => $activeTagList,
            'list_perioade_creatie' => $creationPeriodList,
            'list_specii_liric'     => $lyricSpecies,
            'list_specii_epic'      => $epicSpecies,
            'list_structuri_strofa' => $verseStructureList,
            'list_rime'             => $rhymeList,
            'list_picior_metric'    => $pmList,

            'allTags'          => TagTable::getInstance()->fetchAll(),
            'tagsSaved'        => $objPoezie->getArrTagKeys(),
            'list_cicluri'     => !empty($objPoezie->getPersonajId()) ? PoezieCicluriTable::getInstance()->fetchForSelect(new PoezieCicluriFilters(['poetId' => $objPoezie->getPersonajId()])) : [],
            'list_useri'       => $list_useri,
            'list_variante'    => isset($variantePoezii) && $variantePoezii['count'] ? $variantePoezii['items'] : [],

            // Template data
            'page_title' => $id ? 'Editează poezie' : 'Adaugă poezie',
            'breadcrumb' => array(
                array(
                    'name' => 'Lista poezii',
                    'href' => '/poezii/',
                ),
                array(
                    'name' => $id ? 'Editează poezie' : 'Adaugă poezie',
                )
            ),
        );

        // active Menu
        array_push($this->activeMenu, 'list_poezii');

        $this->_render($this->viewPath . 'edit', $this->defaultTemplate, $viewData);
    }

    // ========================================================================================
    // TABURI - editare poezii

    /**
     * TAB - informatii generale
     *
     * @param PoezieItem $objPoezie
     * @return bool
     * @throws Exception
     */
    public function _tab_generale($objPoezie)
    {
        // Am dat de inteles ca este apelata aceasta functie numai cand este trimis un formular de tip POST.
        if (!post()) return false;

        $fv = $this->form_validation;

        /**
         * TAB  Generale
         *
         */
        $this->form_validation->set_rules('titlu',                  'Titlu poezie',    'trim|required|strip_tags|xss_clean');
        $this->form_validation->set_rules('titlu_varianta',         'Titlu varianta',  'trim|strip_tags|xss_clean');
        if (!$objPoezie->getId()) {
            $this->form_validation->set_rules('personaj_id',         'Autor',          'trim|required|xss_clean');         // autor
        }

        $this->form_validation->set_rules('continut_scurt',           'Continut scurt',     'required');
        $this->form_validation->set_rules('continut',                 'Continut',           'required');

        // surse
        $this->form_validation->set_rules('sursa_doc_carte_id',       'Carte',  'trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('sursa_doc_publicatie_id',  'Publicatie',  'trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('sursa_doc_start_page',     'Pag start',  'trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('sursa_doc_end_page',       'Pag end',  'trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('sursa_titlu',              'Titlu sursa',  'trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('sursa_titlu',              'Titlu sursa',  'trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('sursa_link',               'Link sursa online', 'trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('sursa_culegator',          'Sursa culegator', 'trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('observatii',               'Observatii', '');

        $this->form_validation->set_rules('comentariu',                'Comentariu',       '');
        $this->form_validation->set_rules('comentariu_user_id',        'Autor comentariu', 'trim|xss_clean' . ((post('comentariu')) ? '|required' : ''));

        $this->form_validation->set_rules('ciclu_id',                 'Ciclul poeziei', 'trim|xss_clean');
        $this->form_validation->set_rules('subiect_id',               'Subiect principal', 'trim|xss_clean');
        $this->form_validation->set_rules('perioada_creatiei_id',     'Perioada creatiei', 'trim|xss_clean');
        $this->form_validation->set_rules('data_creatiei',            'Data creatiei', 'trim|xss_clean');
        $this->form_validation->set_rules('locul_creatiei',           'Locul creatiei', 'trim|xss_clean');

        $this->form_validation->set_rules('specie_id',                'Specie', 'trim|xss_clean');
        $this->form_validation->set_rules('structura_strofa_id',      'Structura strofa', 'trim|xss_clean');
        $this->form_validation->set_rules('rima_id',                  'Rima', 'trim|xss_clean');
        $this->form_validation->set_rules('picior_metric_id',         'Picior metric', 'trim|xss_clean');
        $this->form_validation->set_rules('nr_strofe',                'Nr strofe', 'trim|xss_clean');

        $this->form_validation->set_rules('contextul_createi',        'Contextul creatiei', 'trim');

        $this->form_validation->set_rules('cuvinteCheie[]',             'Cuvinte cheie', 'trim|xss_clean');

        $this->form_validation->set_rules('order_id',                 'Order ID',       'trim|xss_clean');
        $this->form_validation->set_rules('data_publicare',           'Data publicarii', 'trim|xss_clean');
        $this->form_validation->set_rules('is_deleted',               'Poezie ștearsă', 'trim|xss_clean|numeric');

        if ($fv->run()) {

            if ($this->input->is_ajax_request()) {

                // setters
                $objPoezie->setTitlu(post('titlu'));
                $objPoezie->setTitluVarianta(post('titlu_varianta'));
                $objPoezie->setPersonajId(post('personaj_id'));
                $objPoezie->setContinutScurt(post('continut_scurt'));
                $objPoezie->setContinut(post('continut'));
                $objPoezie->setSursaDocCarteId(post('sursa_doc_carte_id'));
                $objPoezie->setSursaDocPublicatieId(post('sursa_doc_publicatie_id'));
                $objPoezie->setSursaDocStartPage(post('sursa_doc_start_page'));
                $objPoezie->setSursaDocEndPage(post('sursa_doc_end_page'));
                $objPoezie->setSursaTitlu(post('sursa_titlu'));
                $objPoezie->setSursaLink(post('sursa_link'));
                $objPoezie->setSursaCulegator(post('sursa_culegator'));

                $objPoezie->setIsPublished(post('is_published'));
                $objPoezie->setOrderId(post('order_id'));
                $objPoezie->setCicluId(post('ciclu_id'));
                $objPoezie->setSubjectId(post('subiect_id'));
                $objPoezie->setPerioadaCreatieiId(post('perioada_creatiei_id'));
                $objPoezie->setDataCreatiei(post('data_creatiei'));
                $objPoezie->setLoculCreatiei(post('locul_creatiei'));

                $objPoezie->setSpecieId(post('specie_id'));
                $objPoezie->setStructuraStrofaId(post('structura_strofa_id'));
                $objPoezie->setRimaId(post('rima_id'));
                $objPoezie->setPiciorMetricId(post('picior_metric_id'));
                $objPoezie->setNrStrofe(post('nr_strofe'));

                // details
                $objPoezie->setUserId($this->aauth->get_user_id());
                $objPoezie->setObservatii(post('observatii'));
                $objPoezie->setDataInsert(Calendar::sqlDateTime());

                $objPoezie->setIsDeleted(post('is_deleted'));

                // add Cuvinte cheie
                $objPoezie->setArrTagKeys(post('cuvinteCheie'));

                if (PoezieTable::getInstance()->save($objPoezie)) {
                    $this->mess->displaySuccess(
                        'Poezia a fost salvata cu succes!',
                        ['redirect_url' => '/poezii/edit/' . $objPoezie->getId()]
                    );
                }
            }
        } else {

            if ($this->input->is_ajax_request()) {
                $allErrors = $fv->error_array();
                echo json_encode(array('error' => $allErrors));
                exit;
            }
        }

        return false;
    }

    /**
     * TAB - Audio/media
     */
    public function _tab_media()
    {
        $this->form_validation->set_rules('media_title',            'Titlu', 'trim');
        $this->form_validation->set_rules('media_embeded_uniq_code', 'Codul unic', 'trim');
    }

    /**
     * * TAB - variante
     *
     * @param PoezieItem $objPoezie
     * @param PoezieItem $varObjPoezie
     * @return bool
     * @throws Exception
     */
    public function _tab_variante($objPoezie, $varObjPoezie)
    {
        // Am dat de inteles ca este apelata aceasta functie numai cand este trimis un formular de tip POST.
        if (!post()) return false;

        $fv = $this->form_validation;

        /**
         * TAB  Generale
         *
         */
        $this->form_validation->set_rules('titlu', 'Titlu poezie', 'trim|required|strip_tags|xss_clean');
        $this->form_validation->set_rules('titlu_varianta', 'Titlu varianta', 'trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('continut', 'Continut', '');

        // surse
        $this->form_validation->set_rules('sursa_doc_carte_id', 'Carte', 'trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('sursa_doc_publicatie_id', 'Publicatie', 'trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('sursa_doc_start_page', 'Pag start', 'trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('sursa_doc_end_page', 'Pag end', 'trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('sursa_titlu', 'Titlu sursa', 'trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('sursa_titlu', 'Titlu sursa', 'trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('sursa_link', 'Link sursa online', 'trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('sursa_culegator', 'Sursa culegator', 'trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('observatii', 'Observatii', '');

        if ($fv->run()) {

            if ($this->input->is_ajax_request()) {

                // setters
                $varObjPoezie->setParentId($objPoezie->getId());

                $varObjPoezie->setTitlu(post('titlu'));
                $varObjPoezie->setTitluVarianta(post('titlu_varianta'));
                $varObjPoezie->setPersonajId(post('personaj_id'));
                $varObjPoezie->setContinut(post('continut'));
                $varObjPoezie->setSursaDocCarteId(post('sursa_doc_carte_id'));
                $varObjPoezie->setSursaDocPublicatieId(post('sursa_doc_publicatie_id'));
                $varObjPoezie->setSursaDocStartPage(post('sursa_doc_start_page'));
                $varObjPoezie->setSursaDocEndPage(post('sursa_doc_end_page'));
                $varObjPoezie->setSursaTitlu(post('sursa_titlu'));
                $varObjPoezie->setSursaLink(post('sursa_link'));
                $varObjPoezie->setSursaCulegator(post('sursa_culegator'));

                // details
                $varObjPoezie->setUserId($this->aauth->get_user_id());
                $varObjPoezie->setObservatii(post('observatii'));
                $varObjPoezie->setDataInsert(Calendar::sqlDateTime());

                PoezieTable::getInstance()->save($varObjPoezie);

                $this->mess->displaySuccess(
                    'Varianta a fost salvata cu succes!',
                    ['redirect_url' => '/poezii/edit/' . $objPoezie->getId() . '?tab=variante&v_poezie_id=' . $varObjPoezie->getId()]
                );
            }
        } else {

            if ($this->input->is_ajax_request()) {
                $allErrors = $fv->error_array();
                echo json_encode(array('error' => $allErrors));
                exit;
            }
        }

        return false;
    }

    /**
     * Tab Comentariu poezie
     *
     * @param PoezieItem $objPoezie
     * @return void
     */
    public function _tab_comentariu($objPoezie)
    {
        // Am dat de inteles ca este apelata aceasta functie numai cand este trimis un formular de tip POST.
        if (!post()) return false;

        $fv = $this->form_validation;

        $this->form_validation->set_rules('comentariu',                'Comentariu',       '');
        $this->form_validation->set_rules('comentariu_user_id',        'Autor comentariu', 'trim|xss_clean' . ((post('comentariu')) ? '|required' : ''));

        if ($fv->run()) {

            if ($this->input->is_ajax_request()) {

                // add / update Comentariu
                if ($objPoezie->getId() && post('comentariu')) {
                    /**
                     * @var ArticolItem $objComentariu
                     */
                    $objComentariu = $objPoezie->getComentariuArticolId()
                        ? ArticolTable::getInstance()->load($objPoezie->getComentariuArticolId())
                        : new ArticolItem();
                    $objComentariu->setCatId(ART_CAT_COMMENTARY);
                    $objComentariu->setContinut(post('comentariu'));
                    $objComentariu->setTitlu($objPoezie->getTitlu() . ' (comentariu)');
                    $objComentariu->setUserId(post('comentariu_user_id'));

                    $objPoezie->setComentariu($objComentariu);
                }

                PoezieTable::getInstance()->save($varObjPoezie);

                $this->mess->displaySuccess(
                    'Comentariul a fost salvat cu succes!',
                    ['redirect_url' => '/poezii/edit/' . $objPoezie->getId() . '?tab=comentariu']
                );
            }
        } else {

            if ($this->input->is_ajax_request()) {
                $allErrors = $fv->error_array();
                echo json_encode(array('error' => $allErrors));
                exit;
            }
        }

        return false;
    }

    /**
     * TODOOO
     *
     * @param PoezieItem $objPoezie
     * @return void
     */
    public function _tab_povestea($objPoezie)
    {
        // Am dat de inteles ca este apelata aceasta functie numai cand este trimis un formular de tip POST.
        if (!post()) return false;

        $fv = $this->form_validation;

        $this->form_validation->set_rules('povestea',                'Povestea',       '');
        $this->form_validation->set_rules('povestea_user_id',        'Autor poveste', 'trim|xss_clean' . ((post('povestea')) ? '|required' : ''));

        if ($fv->run()) {

            if ($this->input->is_ajax_request()) {

                // add / update Poveste
                if ($objPoezie->getId() && post('povestea')) {
                    /**
                     * @var ArticolItem $objArticol
                     */
                    $objArticol = $objPoezie->getComentariuArticolId()
                        ? ArticolTable::getInstance()->load($objPoezie->getComentariuArticolId())
                        : new ArticolItem();
                    $objArticol->setCatId(ART_CAT_COMMENTARY);
                    $objArticol->setContinut(post('povestea'));
                    $objArticol->setTitlu($objPoezie->getTitlu() . ' (povestea)');
                    $objArticol->setUserId(post('povestea_user_id'));

                    $objPoezie->setComentariu($objArticol);
                }

                PoezieTable::getInstance()->save($varObjPoezie);

                $this->mess->displaySuccess(
                    'Povestea a fost salvata cu succes!',
                    ['redirect_url' => '/poezii/edit/' . $objPoezie->getId() . '?tab=povestea']
                );
            }
        } else {

            if ($this->input->is_ajax_request()) {
                $allErrors = $fv->error_array();
                echo json_encode(array('error' => $allErrors));
                exit;
            }
        }

        return false;
    }



    // =============================================================================================
    //                                           FILTRE
    // =============================================================================================


    public function cautaTitlu()
    {
        $searchTerm = isset($_GET['query']) ? trim(strip_tags($_GET['query'])) : null;

        if (!$searchTerm)
            $this->displayAutocompleteResults($searchTerm, array());

        // Initialize data.
        $results = [];

        // Fetch data.
        $data = PoezieTable::getInstance()->fetchForAutocomplete($searchTerm);
        if ($data && count($data)) {
            foreach ($data as $k => $v) {
                $results[] = array(
                    'value'     => $v->getTitlu(),
                    'id'        => $v->getId(),
                    'data'      => ['category' => 'Titlu']
                );
            }
        }

        // Return results.
        $this->displayAutocompleteResults($searchTerm, $results);
    }

    // ---------------------------------------------------------------------------------------------

    public function cautaAutor()
    {
        $searchTerm = trim(strip_tags(get('query')));
        if (!$searchTerm) {
            $this->displayAutocompleteResults($searchTerm, []);
        }

        // Set filters
        $filters = new PersonajFilters();
        $filters->setSearchTerm($searchTerm);
        $filters->setOrderBy([$filters::ORDER_BY_NAME_ASC]);

        // Fetch data for autocomplete
        $results = PersonajTable::getInstance()->fetchForAutocomplete($filters);

        // Return results
        $this->displayAutocompleteResults($searchTerm, $results);
    }
}
