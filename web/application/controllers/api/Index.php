<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->defaultTemplate = 'apidocs/template';
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Listare Taguri
     */
    public function index()
    {
        // Set data.
        $viewData = array(

            // Template data
            'page_title' => '',
            'breadcrumb' => array(
                array(
                    'name' => 'Lista sectiuni',
                ),
            ),
        );


        // Render view.
        $this->_render('apidocs/home', $this->defaultTemplate, $viewData);
    }
}