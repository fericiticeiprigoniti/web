<?php

declare(strict_types=1);

namespace controllers\api\helpers;

class Metadata
{
    public int $countTotal = 0;
    public int $countData = 0;
    public int $countOffset = 0;

    public function __construct(int $countTotal, int $countData, int $countOffset)
    {
        $this->countTotal = $countTotal;
        $this->countData = $countData;
        $this->countOffset = $countOffset;
    }
}
