<?php

declare(strict_types=1);

namespace controllers\api\helpers;

class ApiResponse
{
    public array $data = [];
    public Metadata $meta;

    public function __construct(array $data, Metadata $meta)
    {
        $this->data = $data;
        $this->meta = $meta;
    }
}
