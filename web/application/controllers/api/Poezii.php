<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;
use controllers\api\adapters\CarteAdapter;
use controllers\api\adapters\PerioadaCreatieAdapter;
use controllers\api\adapters\PersonajAdapter;
use controllers\api\adapters\PoezieAdapter;
use controllers\api\adapters\PoezieSimpleAdapter;
use controllers\api\helpers\ApiResponse;
use controllers\api\helpers\Metadata;

/**
 * API Poezii
 * Pentru lista generala de metode accesati link-ul:
 * http://fcp.cpco.ro/api/poezii/
 *
 * aici gasim toate detaliile, structura, specia, rima, picior metric, perioadele de creatie ale poeziilor
 */
class Poezii extends RestController
{
    private int $pageNumber;
    private int $perPage;
    private int $offset;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('m_autoload');

        // Set page, perPage and offset
        $this->pageNumber = $this->get('_page') ? (int)$this->get('_page') : 1;
        $this->perPage = $this->get('_per_page') ? (int)$this->get('_per_page') : 50;
        $this->offset = ($this->pageNumber - 1) * $this->perPage;
    }

    // ---------------------------------------------------------------------------------------------


    /**
     * Listare metode API disponibile
     */
    public function index_get()
    {
        if (ENVIRONMENT != 'development') {
            die('Incorrect API usage');
        }

        $doc = new Tools\ApiDocumenter(__CLASS__);

        $viewData = [
            'apiName' => $doc->apiName,
            'apiDescription' => nl2br($doc->apiDescription),
            'apiMethods'  => $doc->apiMethods,
        ];

        $this->load->view('apidocs/template', ['content' => $this->load->view('/apidocs/sectiune', $viewData, true)]);
    }


    /**
     * @title       Returneaza o listă cu poezii. Suporta paginatie (_page, _per_page). <br/>Ex: afiseaza 2 poezii random perioada creatie = 'create in detentie' => ./poezii/list?perioadaCreatieiId=4&_orderBy=order_rand&_page=1&_per_page=2
     *
     * @filters     PoezieFilters
     * @response    PoezieItem[]
     */
    public function list_get()
    {
        // Filters.
        $filters = new PoezieFilters($this->get());
        $filters->setIsPublished($filters::PUBLISHED_YES);
        $filters->setIsDeleted($filters::DELETED_NO);

        // Fetch data
        $response = PoezieTable::getInstance()->fetchAll($filters, $this->perPage, $this->offset);

        $results = [];
        if (isset($response['items'])) {
            foreach($response['items'] as $row){
                $results[] = PoezieSimpleAdapter::toWebserviceArray($row);
            }
        }

        $meta = new Metadata((int)$response['count'], count($response['items']), (int)$this->offset);
        $response = new ApiResponse($results, $meta);

        // Send data
        $this->set_response($response, RestController::HTTP_OK); // OK (200) being the HTTP response code
    }

    /**
     * @title       Detaliile complete ale unei poezii
     * @param int|null $poezieID
     * @throws Exception
     */
    public function poezie_get($poezieID = null)
    {
        if (!$poezieID) {
            show_404();
        }

        // Filters.
        $filters = new PoezieFilters($this->get());
        $filters->setId((int)$poezieID);
        $filters->setIsPublished($filters::PUBLISHED_YES);
        $filters->setIsDeleted($filters::DELETED_NO);

        // Fetch data
        $data = PoezieTable::getInstance()->fetchAll($filters, 1);

        if (empty($data['items'])) {
            show_404();
        }

        $poemItem = $data['items'][0];
        if (!$poemItem instanceof PoezieItem) {
            show_404();
        }

        $result = PoezieAdapter::toWebserviceArray($poemItem);

        // Send data
        $this->set_response($result, RestController::HTTP_OK); // OK (200) being the HTTP response code
    }

    /**
     * @title       Returneaza o lista cu perioadele de creatie (ex. 4- în detenție, 8- în exil)
     *
     * @filters     PerioadaCreatieFilters
     * @response    PerioadaCreatieItem[]
     */
    public function perioade_creatie_get()
    {
        // Filters.
        $filters = new PerioadaCreatieFilters($this->get());

        // Fetch data
        $data = PerioadaCreatieTable::getInstance()->fetchAll($filters, $this->perPage, $this->offset);

        $return = [];
        if (isset($data['items']) && count($data['items'])) {
            /** @var PerioadaCreatieItem $item */
            foreach($data['items'] as $item) {
                $return[] = PerioadaCreatieAdapter::toWebserviceArray($item);
            }
        }

        // Send data
        $this->set_response($return, RestController::HTTP_OK); // OK (200) being the HTTP response code
    }

    /**
     * @title       Lista elementelor de versificatie (rima, ritm, picior metric, specie, structura strofe)
     * @filters
     * @response    []
     */
    public function elemente_versificatie_get()
    {
        $data = [
            'rima'          => RimaTable::getInstance()->fetchForSelect(),
            'specia'        => SpeciePoezieTable::getInstance()->fetchForSelect(),
            'structura'     => StructuraStrofaTable::getInstance()->fetchForSelect(),
            'cicluri'       => PoezieCicluriTable::getInstance()->fetchForSelect(),
            'picior_metric' => PiciorMetricTable::getInstance()->fetchForSelect(),
        ];

        // Send data
        $this->set_response($data, RestController::HTTP_OK); // OK (200) being the HTTP response code
    }
}