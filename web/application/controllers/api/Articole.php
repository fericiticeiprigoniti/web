<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;
use controllers\api\adapters\ArticolCategorieAdapter;


/**
 * API Articole
 *
 * Pentru lista generala de metode accesati link-ul:
 * http://fcp.cpco.ro/api/articole/
 */
class Articole extends RestController
{

    /**
     * @var int
     */
    private $page;

    /**
     * @var int
     */
    private $perPage;

    /**
     * @var int
     */
    private $offset;


    public function __construct()
    {
        parent::__construct();

        /**
         * Toate modelele care tin de Biblioteca si Nomenclatoare sunt incarcate in mod automat,
         * doar atunci cand sunt initializate
         * Vezi fisierul config/hooks.php
         */
        $this->load->model('m_autoload');

        // Set page, perPage and offset
        $this->page = $this->get('_page') ? (int)$this->get('_page') : 1;
        $this->perPage = $this->get('_per_page') ? (int)$this->get('_per_page') : 10;
        $this->offset = ($this->page - 1) * $this->perPage;
    }

    /**
     * Listare metode API disponibile
     * @throws ReflectionException
     */
    public function index_get()
    {
        if (ENVIRONMENT != 'development') {
            die('Incorrect API usage');
        }

        $doc = new Tools\ApiDocumenter(__CLASS__);
        if (!$doc) {
            die("Can't load API documentation");
        }

        $viewData = [
            'apiName' => $doc->apiName,
            'apiDescription' => nl2br($doc->apiDescription),
            'apiMethods'  => $doc->apiMethods,

        ];

        $this->load->view('apidocs/template', ['content' => $this->load->view('/apidocs/sectiune', $viewData, true)]);
    }

    /**
     * @title       Returneaza o listă cu articole (publicate & active). Suporta paginatie (_page, _per_page). <br/>Ex: pentru 1 articol random se trimite ?_orderBy=order_rand&_page=1&_per_page=1.
     *
     * @filters     ArticolFilters
     * @response    ArticolItem[]
     */
    public function list_get()
    {
        // Filters.
        $filters = new ArticolFilters($this->get());
        $filters->setIsPublished(1);
        $filters->setIsDeleted(false);

        // Fetch data
        $response = ArticolTable::getInstance()->fetchForApi($filters, $this->perPage, $this->offset, $tCount);

        array_walk($response, function (&$row){
            if (isset($row['autorInfo'])){
                $row['autorInfo'] = array_filter_keys($row['autorInfo'], [
                    'id', 'nume','prenume', 'biografie_art_id'
                    
                    // todo POZA
                ]);
            }

            if (isset($row['personajInfo'])){
                // TODO - de inlocuit cu pozele originale
                $row['personajInfo']['avatar'] = getPiPersonajImage();
                $row['personajInfo']['biografiePoza'] = getPiPersonajImage();

                $row['personajInfo'] = array_filter_keys($row['personajInfo'], [
                    'id', 'prefix', 'nume', 'prenume','sex','dataNastere','dataAdormire', 'avatar', 'biografiePoza'
                ]);
            }

            if (isset($row['carteInfo'])){
                $row['carteInfo'] = array_filter_keys($row['carteInfo'], [
                    'id', 'titlu', 'subtitlu', 'anulPublicatiei', 'edituraNume', 'format', 'categorieId', 'categorieNume'
                ]);
            }

            if (isset($row['userInfo'])){
                $row['userInfo'] = array_filter_keys($row['userInfo'], [
                    'id','username'
                ]);
            }

            // TODO - publicatie
            // la fel ca la carte ID

            $row = array_filter_keys($row, [
                "id", "catId", "titlu", "continut",
                "autorInfo", "personajInfo", "carteInfo", "userInfo",
                'sursaLink', 'sursaTitlu',
                "sursaDocPublicatieId", "sursaDocStartPage", "sursaDocEndPage", "sursaCulegator",
                "dataInsert", "dataPublicare",
                "orderId", "hits", "userId",
                "seoMetadescription", "seoMetakeywords"
            ]);
        });

        $response = [
            'meta' => [
                'countTotal'  => (string)$tCount,
                'countData'   => (string)count($response),
                'countOffset' => (string)$this->offset
            ],
            'data' => $response
        ];

        // Send data
        $this->set_response($response, RestController::HTTP_OK); // OK (200) being the HTTP response code
    }

    /**
     * @title       Returneaza o listă cu categoriile articolelor. Suporta paginatie (_page, _per_page).
     *
     * @filters     ArticolCategoriiFilters
     * @response    ArticolCategorii[]
     */
    public function categorii_get()
    {
        // Filters.
        $filters = new ArticolCategoriiFilters($this->get());
        $filters->setIsPublished($filters::PUBLISHED_YES);

        // Fetch data - use Adapters
        $response = $this->fetchForApi($filters, $this->perPage, $this->offset, $tCount);

        $response = [
            'meta' => [
                'countTotal'  => (string)$tCount,
                'countData'   => (string)count($response),
                'countOffset' => (string)$this->offset
            ],
            'data' => $response
        ];

        // Send data
        $this->set_response($response, RestController::HTTP_OK); // OK (200) being the HTTP response code
    }

    /**
     * @param ArticolCategoriiFilters $filters
     * @param int|null $limit
     * @param int|null $offset
     * @param int $tCount
     * @return array
     */
    private function fetchForApi(ArticolCategoriiFilters $filters, ?int $limit = null, ?int $offset = null, &$tCount)
    {
        $data = ArticolCategoriiTable::getInstance()->fetchAll($filters, $limit, $offset);
        $return = [];
        if (isset($data['items']) && count($data['items'])) {
            /** @var ArticolCategoriiItem $item */
            foreach($data['items'] as $item) {
                $return[] = ArticolCategorieAdapter::toWebserviceArray($item);
            }
            $tCount = $data['count'];
        }
        return $return;
    }
}