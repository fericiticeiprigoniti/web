<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;
use controllers\api\adapters\TagAdapter;
use controllers\api\adapters\PersonajSimpleAdapter;

/**
 *
 * API Tags
 * Pentru lista generala de metode accesati link-ul:
 * http://fcp.cpco.ro/api/tags/
 *
 */
class Tags extends RestController
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('m_autoload');

        // Set page, perPage and offset
        $this->page = $this->get('_page') ? (int)$this->get('_page') : 1;
        $this->perPage = $this->get('_per_page') ? (int)$this->get('_per_page') : 1000; // default show all tags
        $this->offset = ($this->page - 1) * $this->perPage;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Listare metode API disponibile
     */
    public function index_get()
    {
        if (ENVIRONMENT != 'development') {
            die('Incorrect API usage');
        }

        $doc = new Tools\ApiDocumenter(__CLASS__);
        if (!$doc) {
            die("Can't load API documentation");
        }

        $viewData = [
            'apiName' => $doc->apiName,
            'apiDescription' => nl2br($doc->apiDescription),
            'apiMethods' => $doc->apiMethods,
        ];

        $this->load->view('apidocs/template', ['content' => $this->load->view('/apidocs/sectiune', $viewData, true)]);
    }

    /**
     * @title       Returneaza lista cuvintelor cheie. Limita '_perPage' = 1000
     *
     *
     * @filters     TagFilters
     * @response    TagItem[]
     */
    public function list_get()
    {
        // Filters.
        $filters = new TagFilters($this->get());

        // Fetch data
        $response = TagTable::getInstance()->fetchAll($filters, $this->perPage, $this->offset);

        $results = [];
        if(isset($response['items'])){
            foreach($response['items'] as $row){
                $results[] = TagAdapter::toWebserviceArray($row);
            }
        }

        $response = [
            'meta' => [
                'countTotal'  => (string)$response['count'],
                'countData'   => (string)count($response['items']),
                'countOffset' => (string)$this->offset
            ],
            'data' => $results
        ];

        // Send data
        $this->set_response($response, RestController::HTTP_OK); // OK (200) being the HTTP response code
    }
}
