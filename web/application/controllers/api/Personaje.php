<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;
use controllers\api\adapters\PersonajAdapter;
use controllers\api\adapters\PersonajSimpleAdapter;

/**
 * TODO
 *
 * API Personaje
 * Pentru lista generala de metode accesati link-ul:
 * http://fcp.cpco.ro/api/personaje/
 *
 * Personajele pot avea mai multe roluri (ex: poet, erou, autor, editor,coordonator,traducător,prefațator,torționar,mărturisitor,mucenic/martir etc)
 */
class Personaje extends RestController
{

    /**
     * @var int
     */
    private $page;

    /**
     * @var int
     */
    private $perPage;

    /**
     * @var int
     */
    private $offset;


    public function __construct()
    {
        parent::__construct();

        $this->load->model('m_autoload');

        $this->load->model('m_personaje');

        // Set page, perPage and offset
        $this->page = $this->get('_page') ? (int)$this->get('_page') : 1;
        $this->perPage = $this->get('_per_page') ? (int)$this->get('_per_page') : 50;
        $this->offset = ($this->page - 1) * $this->perPage;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Listare metode API disponibile
     */
    public function index_get()
    {
        if (ENVIRONMENT != 'development') {
            die('Incorrect API usage');
        }

        $doc = new Tools\ApiDocumenter(__CLASS__);
        if (!$doc) {
            die("Can't load API documentation");
        }

        $viewData = [
            'apiName' => $doc->apiName,
            'apiDescription' => nl2br($doc->apiDescription),
            'apiMethods'  => $doc->apiMethods,
        ];

        $this->load->view('apidocs/template', ['content' => $this->load->view('/apidocs/sectiune', $viewData, true)]);
    }

    /**
     * 
     * @title  Returneaza lista cu personaje
     *
     *
     * @filters     PersonajFilters
     * @response    PersonajItem[]
     */
    public function list_get()
    {
         // Filters.
        $filters = new PersonajFilters($this->get());

        // Fetch data
        $response = PersonajTable::getInstance()->fetchAll($filters, $this->perPage, $this->offset);

        $results = [];
        foreach($response['items'] as $row){
            $results[] = PersonajAdapter::toWebserviceArray($row);
        }

        $response = [
            'meta' => [
                'countTotal'  => (string)$response['count'],
                'countData'   => (string)count($results),
                'countOffset' => (string)$this->offset
            ],
            'data' => $results
        ];

        // Send data
        $this->set_response($response, RestController::HTTP_OK); // OK (200) being the HTTP response code   
    }

    /**
     * @title  Returneaza lista de comemorari +-X zile față de ziua curentă. Pentru modificarea numarului de zile vezi parametri daysBeforeCDay / daysAfterCDay
     *
     *
     * @filters     PersonajFilters
     * @response    PersonajItem[]
     */
    public function comemorari_get()
    {
        // Filters.
        $filters = new PersonajFilters($this->get());
        $filters->setShowComemorari(true);
        $filters->setOrderBy([$filters::ORDER_BY_COMEMORARI_ASC]);

        // Fetch data
        $response = PersonajTable::getInstance()->fetchAll($filters, $this->perPage, $this->offset);
        
        $results = [];
        foreach($response['items'] as $row)
        {
            $results[] = PersonajSimpleAdapter::toWebserviceArray($row);
        }

        $response = [
            'meta' => [
                'countTotal'  => (string)$response['count'],
                'countData'   => (string)count($results),
                'countOffset' => (string)$this->offset
            ],
            'data' => $results
        ];

        // Send data
        $this->set_response($response, RestController::HTTP_OK); // OK (200) being the HTTP response code
    }

    /**
     * @title  Filtreaza personajele dupa litera initiala a numelui (default este fara paginatie, daca se doreste paginare se seteaza parametrul _per_page=50)
     *
     * @response    PersonajItem[]
     */
    public function byletter_get($letter = false)
    {
        if(!$letter) show_error('Parameter letter is mandatory');

        // Filters.
        $filters = new PersonajFilters($this->get());
            $filters->setSearchLetter($letter);
            $filters->setOrderBy([$filters::ORDER_BY_NAME_ASC]);

        // Fetch data
        $this->perPage = get('_per_page') ?: 999;
        $response = PersonajTable::getInstance()->fetchAll($filters, $this->perPage, $this->offset);

        $results = [];
        foreach($response['items'] as &$row)
        {
            $row->countPoezii();
            $row->fetchOcupatiiArestare();
            $results[] = PersonajSimpleAdapter::toWebserviceArray($row);
        }

        $response = [
            'meta' => [
                'countTotal'  => (string)$response['count'],
                'countData'   => (string)count($results),
                'countOffset' => (string)$this->offset
            ],
            'data' => $results
        ];

        // Send data
        $this->set_response($response, RestController::HTTP_OK); // OK (200) being the HTTP response code
    }

    /**
     * @title Returneaza o lista cu rolurile personajelor
     * 
     * @filters     RolFilters
     * @response    RolItem[]
     */
    public function roluri_get(){
        // Filters.
        $filters = new RolFilters($this->get());

        // Fetch data
        $response = RolTable::getInstance()->fetchForApi($filters, $this->perPage, $this->offset, $tCount);

        array_walk($response, function (&$row){
           
            $row = array_filter_keys($row, [
                'id', 'nume', 'detalii', 'parentId', 'orderId'
            ]);
        });

        $response = [
            'meta' => [
                'countTotal'  => (string)$tCount,
                'countData'   => (string)count($response),
                'countOffset' => (string)$this->offset
            ],
            'data' => $response
        ];

        // Send data
        $this->set_response($response, RestController::HTTP_OK); // OK (200) being the HTTP response code
    }
}