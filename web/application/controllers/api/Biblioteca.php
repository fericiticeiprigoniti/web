<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

/**
 * API Biblioteca
 * Pentru lista generala de metode accesati link-ul:
 * http://fcp.cpco.ro/api/biblioteca/
 */
class Biblioteca extends RestController
{

    /**
     * @var int
     */
    private $page;

    /**
     * @var int
     */
    private $perPage;

    /**
     * @var int
     */
    private $offset;

    public function __construct()
    {
        parent::__construct();

        /**
         * Toate modelele care tin de Biblioteca si Nomenclatoare sunt incarcate in mod automat,
         * doar atunci cand sunt initializate
         * Vezi fisierul config/hooks.php
         */
        $this->load->model('m_autoload');

        // Set page, perPage and offset
        $this->page = $this->get('_page') ? (int)$this->get('_page') : 1;
        $this->perPage = $this->get('_per_page') ? (int)$this->get('_per_page') : 10;
        $this->offset = ($this->page - 1) * $this->perPage;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Listare metode API disponibile
     * @throws ReflectionException
     */
    public function index_get()
    {
        if (ENVIRONMENT != 'development') {
            die('Incorrect API usage');
        }

        $doc = new Tools\ApiDocumenter(__CLASS__);
        if (!$doc) {
            die("Can't load API documentation");
        }

        // parseaza comentariile fiecarei metode si extragem clasa XFilter pe care o folosim sa luam proprietatile
        $filters = new ReflectionClass('CarteFilters');

        $arrFilters = [];
        foreach($filters->getProperties() as $item){
            $arrFilters[] = [
                'type'  => $item->getDocComment(),
                'colName'    => $item->getName(),
            ];
        }
        //d($arrFilters);

        $viewData = [
            'apiName' => $doc->apiName,
            'apiDescription' => nl2br($doc->apiDescription),
            'apiMethods'  => $doc->apiMethods,

        ];

        $this->load->view('apidocs/template', ['content' => $this->load->view('/apidocs/sectiune', $viewData, true)]);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @title       Lista cu toate cartile
     * @filters     CarteFilters
     * @response    CarteItem[]
     */
    public function carti_get()
    {
        // Filters.
        $filters = new CarteFilters($this->get());
        $filters->setIsDeleted(false);

        // Fetch data
        $response = CarteTable::getInstance()->fetchForApi($filters, $this->perPage, $this->offset, $tCount);

        array_walk($response, function (&$row){
            $row = array_filter_keys($row, [
                'id', 'titlu', 'subtitlu', 'versiune', 'editia','parentId', 'format', 'anulPublicatiei', 'nrPagini', 'nrAnexe',
                'categorieInfo', 'autori', 'edituri' , 'localitateInfo' ,'colectieInfo', 'dimensiuneInfo'
            ]);
        });

        $response = [
            'meta' => [
                'countTotal'  => (string)$tCount,
                'countData'   => (string)count($response),
                'countOffset' => (string)$this->offset
            ],
            'data' => $response
        ];

        // Send data
        $this->set_response($response, RestController::HTTP_OK); // OK (200) being the HTTP response code
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @title       Lista cu autorii
     * @filters     AutorFilters
     * @response    AutorItem[]
     */
    public function autori_get()
    {
        // Filters.
        $filters = new AutorFilters($this->get());
        $filters->setIsDeleted(false);

        // Fetch data
        $response = AutorTable::getInstance()->fetchForApi($filters, $this->perPage, $this->offset, $tCount);

        array_walk($response, function (&$row){

            $row = array_filter_keys($row, [
                'id','nume','prenume','alias','poza','poza_thumb'
            ]);
        });

        // Send data
        $this->set_response($response, RestController::HTTP_OK); // OK (200) being the HTTP response code
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @title       Lista completa cu toate categoriile cartilor
     * @filters     CategorieCarteFilters
     * @response    CategorieCarteItem[]
     */
    public function categoriiCarti_get()
    {
        // Filters.
        $filters = new CategorieCarteFilters($this->get());

        // Fetch data
        $data = CategorieCarteTable::getInstance()->fetchForApi($filters, $this->perPage, $this->offset);

        // Send data
        $this->set_response($data, RestController::HTTP_OK); // OK (200) being the HTTP response code
    }



    /**
     * @title       Listează toate colectiile cărților (ex: Titlu & editură)
     * @filters     ColectieFilters
     * @response    ColectieItem[]
     */
    public function colectii_get()
    {
        // Filters.
        $filters = new ColectieFilters($this->get());

        // Fetch data
        $response = ColectieTable::getInstance()->fetchForApi($filters, $this->perPage, $this->offset, $tCount);

        array_walk($response, function (&$row) {
            if (isset($row['edituraInfo'])) {
                $row['edituraInfo'] = array_filter_keys($row['edituraInfo'], [
                    'id', 'nume', 'webSiteUrl', 'localitateId', 'alias'
                ]);
            }
        });

        array_walk($response, function (&$row){
            $row = array_filter_keys($row, [
                'id','titlu','edituraInfo'
            ]);
        });

        $response = [
            'meta' => [
                'countTotal'  => (string)$tCount,
                'countData'   => (string)count($response),
                'countOffset' => (string)$this->offset
            ],
            'data' => $response
        ];

        // Send data
        $this->set_response($response, RestController::HTTP_OK); // OK (200) being the HTTP response code
    }

    /**
     * @title       Listează toate dimensiunile cărților (ex: latime x inaltime)
     * @filters     DimensiuneFilters
     * @response    DimensiuneItem[]
     */
    public function dimensiuni_get()
    {
        // Filters.
        $filters = new DimensiuneFilters($this->get());

        // Fetch data
        $response = DimensiuneTable::getInstance()->fetchForApi($filters, $this->perPage, $this->offset, $tCount);

        array_walk($response, function (&$row){
            $row = array_filter_keys($row, [
                'id','latime','inaltime'
            ]);
        });

        $response = [
            'meta' => [
                'countTotal'  => (string)$tCount,
                'countData'   => (string)count($response),
                'countOffset' => (string)$this->offset
            ],
            'data' => $response
        ];

        // Send data
        $this->set_response($response, RestController::HTTP_OK); // OK (200) being the HTTP response code
    }

    /**
     * TODO
     */
    public function edituri_get(){}

    /**
     * @title       Listează toate tipurile de format (ex: Tiparit, Electronic)
     */
    public function formatCarti_get(){

        // Fetch data
        $response = CarteItem::fetchFormatTypes();

        // Send data
        $this->set_response($response, RestController::HTTP_OK); // OK (200) being the HTTP response code
    }
}
