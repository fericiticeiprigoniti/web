<?php

namespace controllers\api\adapters;

use EdituraItem;
use controllers\api\adapters\LocalitateAdapter;

/**
 * CodeIgniter Class
 */
class EdituraAdapter {

    /**
     * Return an array with summary data
     *
     * @param EdituraItem $item
     * @return array
     */
    public static function toWebserviceArray(EdituraItem $item)
    {
        $locInfo = ($objLocalitate = $item->getLocalitate()) ? LocalitateAdapter::toWebserviceArray($objLocalitate) : null;  

        return [
            'id'                => $item->getId(),
            'nume'              => $item->getNume(),
            'localitate'        => $locInfo
        ];
    }
}