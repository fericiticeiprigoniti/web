<?php

namespace controllers\api\adapters;

use ArticolCategoriiItem;

/**
 * CodeIgniter Class
 */
class ArticolCategorieAdapter {

    /**
     * Return an array with summary data
     *
     * @param ArticolCategoriiItem $item
     * @return array
     */
    public static function toWebserviceArray(ArticolCategoriiItem $item)
    {
        return [
            "id"        => $item->getId(),
            "parentId"  => $item->getParentId(),
            "nivel"     => $item->getNivel(),
            "nume"      => $item->getNume(),
            "alias"     => $item->getAlias()
        ];
    }
}