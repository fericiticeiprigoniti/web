<?php

namespace controllers\api\adapters;
use controllers\api\adapters\PersonajSimpleAdapter;

use TagItem;

/**
 * CodeIgniter Class
 */
class TagAdapter {

    /**
     * Return an array with summary data
     *
     * @param TagItem $item
     * @return array
     */
    public static function toWebserviceArray(TagItem $item)
    {
        $personajInfo = [];
        if ($objPersonaj = $item->getPersonaj())
            $personajInfo = PersonajSimpleAdapter::toWebserviceArray($objPersonaj);

        return [
            'id'               => $item->getId(),
            'nume'             => $item->getNume(),
            'observatii'       => $item->getObservatii(),
            'categorieId'      => $item->getCatId(),
            'personajInfo'     => $personajInfo ?: null
        ];
    }

}