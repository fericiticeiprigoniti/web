<?php

declare(strict_types=1);

namespace controllers\api\adapters;

use SpeciePoezieItem;

class SpeciePoezieAdapter
{
    public static function toWebserviceArray(SpeciePoezieItem $item): array
    {
        return [
            'id' => $item->getId(),
            'nume' => $item->getNume(),
            'alias' => $item->getAlias(),
            'genLiterarId' => $item->getGenLiterarId(),
            'order' => $item->getOrder(),
        ];
    }
}
