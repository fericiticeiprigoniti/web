<?php

namespace controllers\api\adapters;

use ArticolItem;
use controllers\api\adapters\PersonajSimpleAdapter;
use controllers\api\adapters\UserAdapter;
use controllers\api\adapters\CarteAdapter;
use controllers\api\adapters\ArticolCategorieAdapter;

/**
 * CodeIgniter Class
 */
class ArticolAdapter {

    /**
     * Return an array with summary data
     *
     * @param ArticolItem $item
     * @return array
     */
    public static function toWebserviceArray(ArticolItem $item)
    {
        $arrPersonajInfo = ($objPersonaj = $item->getPersonaj())? PersonajSimpleAdapter::toWebserviceArray($objPersonaj) : null;
        $arrUserInfo     = ($objUser = $item->getUser())? UserAdapter::toWebserviceArray($objUser) : null;
        $arrCarteInfo    = ($objCarte = $item->getCarte())? CarteAdapter::toWebserviceArray($objCarte) : null;
        $arrCategorieInfo= ($objArticolCategorie = $item->getCategorie())? ArticolCategorieAdapter::toWebserviceArray($objArticolCategorie) : null;

        return [
            "id"                => $item->getId(),
            "catId"             => $item->getCatId(),
            "titlu"             => $item->getTitlu(),
            "continut"          => $item->getContinut(),
            "sursa" => [
                "sursaLink"         => $item->getSursaLink(),
                "sursaTitlu"        => $item->getSursaTitlu(),
                "sursaDocCarteId"   => $item->getSursaDocCarteId(),
                "sursaDocPublicatieId"=> $item->getSursaDocPublicatieId(),
                "sursaCulegator"    => $item->getSursaCulegator(),
                "sursaDocStartPage" => $item->getSursaDocStartPage(),
                "sursaDocEndPage"   => $item->getSursaDocEndPage(),
                "carteInfo"=> $arrCarteInfo

            ],
            "dataPublicare"=> $item->getDataPublicare(),
            "seo" => [
                "seoMetadescription"=> $item->getSeoMetadescription(),
                "seoMetakeywords"   => $item->getSeoMetakeywords(),
            ],
            "categorieInfo"     => $arrCategorieInfo,
            "userInfo" => $arrUserInfo,

            //  pentru articolele - trebuie finalizat adminul cu adaugare articol
            // "arrTagKeys"=> null,  // TO IMPLEMENt
            // "autorInfo"=> null,
            "personajInfo"       => $arrPersonajInfo,

            "hits"=> $item->getHits()
        ];
    }
}