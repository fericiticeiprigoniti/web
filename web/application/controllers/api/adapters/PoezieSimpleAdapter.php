<?php

namespace controllers\api\adapters;

use PoezieItem;

class PoezieSimpleAdapter
{
    public static function toWebserviceArray(PoezieItem $item): array
    {
        $objPersonaj = $item->getPersonaj();
        $objUser = $item->getUser();
        $objCarte = $item->getCarte();

        return [
            'id' => $item->getId(),
            'parentId' => $item->getParentId(),
            'titlu' => $item->getTitlu(),
            'titluVarianta' => $item->getTitluVarianta(),
            'continut' => $item->getContinut(),
            'hits' => $item->getHits(),
            'dataInsert' => $item->getDataInsert(),

            'userInfo' => $objUser ? UserAdapter::toWebserviceArray($objUser) : null,
            'personajInfo' => $objPersonaj ? PersonajSimpleAdapter::toWebserviceArray($objPersonaj) : null,
            'carteInfo' => $objCarte ? CarteAdapter::toWebserviceArray($objCarte) : null,

            'dataCreatiei' => $item->getDataCreatiei(),
            'loculCreatiei' => $item->getLoculCreatiei(),
            'perioadaCreatieiId' => $item->getPerioadaCreatieiId(),
            'nrStrofe' => $item->getNrStrofe(),
        ];
    }
}
