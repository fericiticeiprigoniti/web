<?php

namespace controllers\api\adapters;

use LocalitateItem;

/**
 * CodeIgniter Class
 */
class LocalitateAdapter {

    /**
     * Return an array with summary data
     *
     * @param LocalitateItem $item
     * @return array
     */
    public static function toWebserviceArray(LocalitateItem $item)
    {
        return [
            'id'   => $item->getId(),
            'nume' => $item->getNume(),
            'numeComplet' => $item->getNumeComplet()
        ];
    }

}