<?php

namespace controllers\api\adapters;

use CarteItem;

class CarteAdapter
{
    public static function toWebserviceArray(CarteItem $item): array
    {
        $edituriInfo = [];
        $editura_1 = $item->getEditura();
        if ($editura_1) {
            $edituriInfo[] = EdituraAdapter::toWebserviceArray($editura_1);
        }

        return [
            'id'                => $item->getId(),
            'titlu'             => $item->getTitlu(),
            'subtitlu'          => $item->getSubtitlu(),
            'anulPublicatiei'   => $item->getAnulPublicatiei(),
            'edituraNume'       => $item->getEdituraNume(),
            'edituraLocalitate' => $item->getEditura() && $item->getEditura()->getLocalitate() ? $item->getEditura()->getLocalitate()->getNumeComplet() : null,
            'format'            => $item->getFormatName(),
            'categorieId'       => $item->getCategorieId(),
            'categorieNume'     => $item->getCategorieNume(),
            'edituriInfo'       => $edituriInfo
        ];
    }
}
