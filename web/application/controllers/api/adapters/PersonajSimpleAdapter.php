<?php

namespace controllers\api\adapters;

use PersonajItem;
use PersonajTable;

/**
 * CodeIgniter Class
 */
class PersonajSimpleAdapter {

    public static function toSmartObject($itemData)
    {
        $objItem = $itemData['id']? new PersonajItem() : PersonajTable::getInstance()->load($itemData['id']);

        if($itemData['nume']) $objItem->setNume($itemData['nume']);
        if($itemData['prenume']) $objItem->setNume($itemData['prenume']);
        
        return $objItem;
    }

    /**
     * Return an array with summary data
     *
     * @param PersonajItem $item
     * @return array
     */
    public static function toWebserviceArray(PersonajItem $item)
    {
        $locNastere = $item->getLocNastere();
        $resultsArray = [
            'id'        => $item->getId(),
            'prefix'    => $item->getPrefix(),
            'nume'      => $item->getNume(),
            'prenume'   => $item->getPrenume(),
            'sex'       => $item->getSex(),
            'dataNastere' => $item->getDataNastere(),
            'dataAdormire' => $item->getDataAdormire(),
            'avatar'     => getPiPersonajImage($item->getAvatar()),
            'nume_complet' => $item->getPrenume() . ' ' . $item->getNume(),
            'alias' => dash_it($item->getPrenume() . ' ' . $item->getNume()),
            'aniInchisoare' => $item->getAniInchisoare(),
        ];
    
        if (!is_null($item->getCountPoezii())) {
            $resultsArray['countPoezii'] = $item->getCountPoezii();
        }
        if (!is_null($item->fetchOcupatiiArestare())) {
            $resultsArray['ocupatiiArestare'] = $item->fetchOcupatiiArestare();
        }
        $resultsArray['locNastere'] = $locNastere ? LocalitateAdapter::toWebserviceArray($locNastere) : null;

        return $resultsArray;
    }

}