<?php

declare(strict_types=1);

namespace controllers\api\adapters;

use ArticolItem;

class ArticolSimpluAdapter
{
    public static function toWebserviceArray(ArticolItem $item): array
    {
        return [
            'id' => $item->getId(),
            'catId' => $item->getCatId(),
            'titlu' => $item->getTitlu(),
            'continut' => $item->getContinut(),
            'dataInsert' => $item->getDataInsert()->getTimestamp(),
            'dataPublicare' => $item->getDataPublicare()->getTimestamp(),
        ];
    }
}
