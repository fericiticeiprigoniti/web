<?php

namespace controllers\api\adapters;

use AauthUserItem;

/**
 * CodeIgniter Class
 */
class UserAdapter {

    /**
     * Return an array with summary data
     *
     * @param AauthUserItem $item
     * @return array
     */
    public static function toWebserviceArray(AauthUserItem $item)
    {
        return [
            'id'       => $item->getId(),
            'username' => $item->getUsername(),
            'nume'     => $item->getUserVar('nume'),
            'prenume'  => $item->getUserVar('prenume')
        ];
    }

}