<?php

namespace controllers\api\adapters;

use PersonajItem;

/**
 * CodeIgniter Class
 */
class PersonajAdapter {

    /**
     * Return an array with summary data
     *
     * @param PersonajItem $item
     * @return array
     */
    public static function toWebserviceArray(PersonajItem $item)
    {
        $locNastere = $item->getLocNastere() ? $item->getLocNastere()->toArray() : null;
        $locInmormantare = $item->getLocInmormantare() ? $item->getLocInmormantare()->toArray() : null;
        $biografieInfo = $item->getBiografie() ? ArticolAdapter::toWebserviceArray($item->getBiografie()) : null;

        return [
            'id'                    => $item->getId(),
            'prefix'                => $item->getPrefix(),
            'nume'                  => $item->getNume(),
            'prenume'               => $item->getPrenume(),
            'sex'                   => $item->getSex(),
            'dataNastere'           => $item->getDataNastere(),
            'dataAdormire'          => $item->getDataAdormire(),
            'avatar'                => getPiPersonajImage($item->getAvatar()),
            'nume_complet'          => $item->getPrenume() . ' ' . $item->getNume(),
            'alias'                 => dash_it($item->getPrenume() . ' ' . $item->getNume()),

            'numeAnterior'          => $item->getNumeAnterior(),
            'prenumeMonahism'       => $item->getPrenumeMonahism(),
            'numePseudonim'         => $item->getNumePseudonim(),
            'numePorecla'           => $item->getNumePorecla(),
            'prenumeTata'           => $item->getPrenumeTata(),
            'prenumeMama'           => $item->getPrenumeMama(),
            'countCopiiB'           => $item->getCountCopiiB(),
            'countCopiiF'           => $item->getCountCopiiF(),

            'durataDetentie'        => $item->getDurataDetentie(),
            'durataPrizonierat'     => $item->getDurataPrizonierat(),
            'durataDeportare'       => $item->getDurataDeportare(),
            'durataDomiciliuOblig'  => $item->getDurataDomiciliuOblig(),
            'aniCondamnare'         => $item->getAniCondamnare(),
            'aniInchisoare'         => $item->getAniInchisoare(),

            'decesLoculMortii'      => $item->getDecesLoculMortii(),
            'decesNumeCimitir'      => $item->getDecesNumeCimitir(),
            'locNastere'            => $locNastere,
            'locInmormantare'       => $locInmormantare,
            'decesMormantCoordLat'  => $item->getDecesMormantCoordLat(),
            'decesMormantCoordLong' => $item->getDecesMormantCoordLong(),

            'biografieInfo' => $biografieInfo,

            "seo" => [
                "seoMetadescription"=> $item->getSeoMetadescription(),
                "seoMetakeywords"   => $item->getSeoMetakeywords(),
            ],
        ];
    }
}