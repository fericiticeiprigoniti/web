<?php

declare(strict_types=1);

namespace controllers\api\adapters;

use PerioadaCreatieItem;

class PerioadaCreatieAdapter
{
    public static function toWebserviceArray(PerioadaCreatieItem $item): array
    {
        return [
            'id'   => $item->getId(),
            'nume' => $item->getNume(),
            'alias' => $item->getAlias()
        ];
    }
}
