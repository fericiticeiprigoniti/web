<?php

namespace controllers\api\adapters;

use PoezieItem;

class PoezieAdapter
{
    public static function toWebserviceArray(PoezieItem $item): array
    {
        $objPersonaj = $item->getPersonaj();
        $objCarte = $item->getCarte();
        $objComentariu = $item->getComentariu();
        $objPerioadaCreatie = $item->getPerioadaCreatie();
        $objSpecie = $item->getSpecie();
        $objUser = $item->getUser();

        return [
            'id' => $item->getId(),
            'parentId' => $item->getParentId(),
            'titlu' => $item->getTitlu(),
            'titluVarianta' => $item->getTitluVarianta(),
            'continut' => $item->getContinut(),

            'personajInfo' => $objPersonaj ? PersonajSimpleAdapter::toWebserviceArray($objPersonaj) : null,
            'carteInfo' => $objCarte ? CarteAdapter::toWebserviceArray($objCarte) : null,
            'comentariuInfo' => $objComentariu ? ArticolSimpluAdapter::toWebserviceArray($objComentariu) : null,
            'perioadaCreatieiInfo' => $objPerioadaCreatie ? PerioadaCreatieAdapter::toWebserviceArray($objPerioadaCreatie) : null,
            'specieInfo' => $objSpecie ? SpeciePoezieAdapter::toWebserviceArray($objSpecie) : null,
            'userInfo' => $objUser ? UserAdapter::toWebserviceArray($objUser) : null,

            'sursaLink' => $item->getSursaLink(),
            'sursaTitlu' => $item->getSursaTitlu(),
            'sursaDocCarteId' => $item->getSursaDocCarteId(),
            'sursaDocPublicatieId' => $item->getSursaDocPublicatieId(),
            'sursaDocStartPage' => $item->getSursaDocStartPage(),
            'sursaDocEndPage' => $item->getSursaDocEndPage(),
            'sursaCulegator' => $item->getSursaCulegator(),

            'structuraStrofaId' => $item->getStructuraStrofaId(),
            'rimaId' => $item->getRimaId(),
            'piciorMetricId' => $item->getPiciorMetricId(),
            'nrStrofe' => $item->getNrStrofe(),
            'cicluId' => $item->getCicluId(),
            'subjectId' => $item->getSubjectId(),
            'perioadaCreatieiId' => $item->getPerioadaCreatieiId(),
            'dataCreatiei' => $item->getDataCreatiei(),
            'loculCreatiei' => $item->getLoculCreatiei(),
            'locDetentieCreatieId' => $item->getLocDetentieCreatieId(),
            'specieId' => $item->getSpecieId(),
            'alternativesNo' => $item->getAlternativesNo(),
            'hits' => $item->getHits(),
            'orderId' => $item->getOrderId(),
            'dataInsert' => $item->getDataInsert(),
            'dataPublicare' => $item->getDataPublicare(),
        ];
    }
}
