<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

/**
 *
 * API Personaje
 * Pentru lista generala de metode accesati link-ul:
 * http://fcp.cpco.ro/api/citate/
 *
 */
class Citate extends RestController
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('m_autoload');

        // Set page, perPage and offset
        $this->page = $this->get('_page') ? (int)$this->get('_page') : 1;
        $this->perPage = $this->get('_per_page') ? (int)$this->get('_per_page') : 10;
        $this->offset = ($this->page - 1) * $this->perPage;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Listare metode API disponibile
     */
    public function index_get()
    {
        if (ENVIRONMENT != 'development') {
            die('Incorrect API usage');
        }

        $doc = new Tools\ApiDocumenter(__CLASS__);
        if (!$doc) {
            die("Can't load API documentation");
        }

        $viewData = [
            'apiName' => $doc->apiName,
            'apiDescription' => nl2br($doc->apiDescription),
            'apiMethods' => $doc->apiMethods,
        ];

        $this->load->view('apidocs/template', ['content' => $this->load->view('/apidocs/sectiune', $viewData, true)]);
    }

    /**
     * @title       Returneaza unul sau mai multe citate random. $limit default este 1.
     *
     *
     * @filters     CitatFilters
     * @response    CitatItem[]
     */
    public function random_get($limit = 1)
    {
        $limit = (int)$limit;

        // Filters.
        $filters = new CitatFilters($this->get());
        $filters->setOrderBy([$filters::ORDER_BY_RAND]);

        // Fetch data
        $response = CitatTable::getInstance()->fetchForApi($filters, $limit, 1, $tCount);

        array_walk($response, function (&$row){
            if (isset($row['personajInfo'])){
                $row['personajInfo'] = array_filter_keys($row['personajInfo'], [
                    'id', 'prefix', 'nume', 'prenume','sex','dataNastere','dataAdormire', 'avatar', 'biografiePoza'
                ]);
            }

            if (isset($row['carteInfo'])){
                $row['carteInfo'] = array_filter_keys($row['carteInfo'], [
                    'id', 'titlu', 'subtitlu', 'anulPublicatiei', 'edituraNume', 'format', 'categorieId', 'categorieNume'
                ]);
            }

            $row = array_filter_keys($row, [
                'id', 'continut', 'sursa', 'pagStart', 'pagEnd', 'locPatimireId', 'vizibilPi',
                'personajInfo', 'carteInfo'
            ]);
        });

        $response = [
            'meta' => [
                'countTotal'  => (string)$tCount,
                'countData'   => (string)count($response),
                'countOffset' => (string)$this->offset
            ],
            'data' => $response
        ];

        // Send data
        $this->set_response($response, RestController::HTTP_OK); // OK (200) being the HTTP response code
    }
}
