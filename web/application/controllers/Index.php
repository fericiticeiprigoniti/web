<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Index extends Admin_Controller
{
    /**
     * Dashboard constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Index page
     */
    public function index()
    {
        // Set data.
        $viewData = array(
            // Template data
            'page_title' => 'Asana - Tasks',
            'breadcrumb' => array(),
        );

        // Render view.
        $this->_render('admin/main_dashboard', $this->defaultTemplate, $viewData);
    }

    /**
     * Logout
     */
    public function logout()
    {
        $this->aauth->logout();
        redirect('/');
    }
}
