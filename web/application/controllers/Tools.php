<?php

use Tools\ConsoleColor;

class Tools extends CI_Controller
{
    
    
    public function index()
    {
        die("\nCodeigniter Tools\n\n" . $this->getTextAvailableCommands());
    }
    
    // ---------------------------------------------------------------------------------------------
    
    public function commands()
    {
        die("\n" . $this->getTextAvailableCommands());
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Usage:
     * $ cd /path/to/project;
     * $ php index.php tools createModel tableName modelName
     * Ex: php index.php tools createModel fcp_articole Articole
     *
     * @param string $tableName
     * @param string $modelName
     */
    public function createModel($tableName='', $modelName='')
    {
        if (empty($tableName)) {
            die($this->getTextCreateModelUsage());
        }

        try {
            $model = new Tools\ModelGenerator();
            $model->init($tableName, $modelName);
            $model->createModels('Generated');
        } catch (Exception $e) {
            die($this->getTextWithError($e->getMessage()));
        }
    
        var_dump($model->getColumns());
        die;
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * @param string $tableName
     * @param string $modelName
     */
    public function model($tableName='', $modelName='')
    {
        $this->createModel($tableName, $modelName);
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * @return string
     */
    private function getTextAvailableCommands()
    {
        $c = new ConsoleColor();
        $txt = <<<EOTXT
Available commands:
  {$c->brown}commands{$c->reset}
  {$c->brown}model{$c->reset}            (alias of: {$c->brown}createModel{$c->reset})
\n
EOTXT;
        return $txt;
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * @return string
     */
    private function getTextCreateModelUsage()
    {
        $c = new ConsoleColor();
        $txt = <<<EOTXT

{$c->lightMagenta}createModel{$c->reset} usage:

{$c->brown}php index.php tools createModel tableName [modelName]{$c->reset}
\n
EOTXT;
        return $txt;
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * @param string $error
     * @return string
     */
    private function getTextWithError($error)
    {
        $c = new ConsoleColor();
        $txt = <<<EOTXT
An error has occured!
{$c->red}{$error}{$c->reset}
\n
EOTXT;
        return $txt;
    }
    
    
}