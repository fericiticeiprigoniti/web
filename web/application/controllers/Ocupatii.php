<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Ocupatii extends Admin_Controller
{

    private $viewPath = 'admin/generale/ocupatii/';

    /**
     * @var $mess Mess
     */
    public $mess;

    /**
     *
     * @var $m_ocupatii M_ocupatii
     */
    public $m_ocupatii;

    function __construct()
    {
        parent::__construct();

        $this->load->library('pagination');
        $this->load->helper(array('form', 'url'));
        $this->load->model('m_ocupatii');
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Listare ocupaţii
     */
    public function index()
    {
        // Get data.
        $data = $this->m_ocupatii->fetchList();

        // Set data.
        $viewData = array(
            'items' => $data,

            // Template data
            'page_title' => 'Lista ocupaţii',
            'breadcrumb' => array(
                array(
                    'name' => 'Lista ocupaţii',
                ),
            ),
        );

        // Render view.
        $this->_render($this->viewPath . 'list', $this->defaultTemplate, $viewData);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Adauga ocupatie noua
     *
     */
    public function add()
    {
        $this->edit();
    }

    /**
     * Editeaza ocupatie
     * @param int|bool $id
     */
    function edit($id = null)
    {
        $this->data['edit'] = $id ? (int)$id : null;

        // Get data by id
        if ($id) {
            $data = $this->m_ocupatii->load($id);
        } else {
            $data = array(
                'nume' => null,
                'parent_id' => 0,
            );
        }
        $this->data['data'] = $data;

        // ------------------------------

        // Parse post data
        if (post()) {

            /** @var CI_Form_validation $fv */
            $fv = $this->form_validation;

            // REGULI
            $rules_config = array(
                array(
                    'field'     => 'nume',
                    'label'     => 'Nume',
                    'rules'     => 'trim|required|min_length[3]|xss_clean|strip_tags'
                ),
                array(
                    'field'     => 'parinte_id',
                    'label'     => 'Parinte',
                    'rules'     => 'trim|xss_clean|is_natural|strip_tags'
                ),
            );

            $fv->set_rules($rules_config);

            if ($fv->run()) {

                // Verifica unicitatea numelui
                $res = $this->m_ocupatii->fetchByName(post('nume'), $id);
                if ($res && count($res)) {
                    $this->mess->displayErrors(array('nume' => 'Ocupatia nu poate fi salvata. Exista o alta ocupatie cu aceeasi denumire'));
                }

                try {
                    $this->m_ocupatii->save(post(), $id);
                } catch (Exception $e) {
                    $this->mess->displayErrors(array('nume' => 'Datele nu pot fi salvate in baza de date -> ' . $e->getMessage()));
                }


                if ($this->input->is_ajax_request()) {
                    $this->mess->displaySuccess('Datele au fost salvate cu succes');
                }
            } else {

                if ($this->input->is_ajax_request()) {
                    $allErrors = $fv->error_array();
                    echo json_encode(array('error' => $allErrors));
                    exit;
                }
            }
        }

        // ------------------------------

        // Template data
        $this->data['page_title'] = $id ? 'Editează ocupaţie' : 'Adaugă ocupaţie';
        $this->data['breadcrumb'] = array(
            array(
                'name' => 'Lista ocupaţii',
                'href' => '/ocupatii/',
            ),
            array(
                'name' => $id ? 'Editează ocupaţie' : 'Adaugă ocupaţie',
            ),
        );
        $this->data['parentItems'] = $this->m_ocupatii->fetchParents();

        $this->_render($this->viewPath . 'edit', $this->defaultTemplate);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Sterge ocupatie
     * @param int $id
     */
    function delete($id)
    {
        $m = new Mess();

        $id = (int)$id;
        if (!$id) {
            $m->displayErrors("ID ocupatie invalid");
        }

        if (!post('submited')) {
            $m->displayErrors("Formular invalid");
        }

        $data = $this->m_ocupatii->load($id);
        if (!$data || !count($data)) {
            $m->displayErrors("Ocupatia nu a fost gasita in baza de date");
        }

        // Verifica daca ocupatia este folosita deja.
        if ($data['is_used']) {
            $m->displayErrors("Ocupatia nu poate fi stearsa. Este folosita in relatia cu un personaj");
        }

        // Delete data.
        try {
            $this->m_ocupatii->delete($id);
            $m->displaySuccess(true);
        } catch (Exception $e) {
            $m->displayErrors('Nu am putut sterge ocupatia din baza de date -> ' . $e->getMessage());
        }
    }

    // ---------------------------------------------------------------------------------------------



}
