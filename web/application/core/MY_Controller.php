<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    /** @var array */
    public $data = [];

    /** @var array */
    public $activeMenu = [];

    /** @var array */
    public $defaultTemplate = ['template'];

    /**
     * @var M_personaje $m_personaje
     */
    public $m_personaje;

    /**
     * @var M_localitati $m_localitati
     */
    public $m_localitati;

    /**
     * @var M_confesiuni $m_confesiuni
     */
    public $m_confesiuni;

    /**
     * @var M_nationalitati $m_nationalitati
     */
    public $m_nationalitati;

    /**
     * @var M_origini_sociale $m_origini_sociale
     */
    public $m_origini_sociale;

    /**
     * @var M_roluri $m_roluri
     */
    public $m_roluri;

    /**
     * @var M_ocupatii $m_ocupatii
     */
    public $m_ocupatii;

    /**
     * @var M_semnalmente_corporale $m_semnalmente_corporale
     */
    public $m_semnalmente_corporale;

    /**
     * @var M_functii $m_functii
     */
    public $m_functii;

    /**
     * @var M_locuri_patimire $m_locuri_patimire
     */
    public $m_locuri_patimire;

    /**
     * @var M_personaje_ocupatii $m_personaje_ocupatii
     */
    public $m_personaje_ocupatii;

    /**
     * @var M_personaje_condamnari $m_personaje_condamnari
     */
    public $m_personaje_condamnari;

    /**
     * @var M_apartenente_politice $m_apartenente_politice
     */
    public $m_apartenente_politice;

    /**
     * @var M_instante_condamnare $m_instante_condamnare
     */
    public $m_instante_condamnare;

    /**
     * @var M_personaje_itinerarii $m_personaje_itinerarii
     */
    public $m_personaje_itinerarii;

    /**
     * @var M_biblioteca_carti $m_biblioteca_carti
     */
    public $m_biblioteca_carti;

    /**
     * @var M_personaje_carti $m_personaje_carti
     */
    public $m_personaje_carti;

    /**
     * @var M_citate $m_citate
     */
    public $m_citate;

    /** @var M_utilizatori */
    public $M_utilizatori;

    /** @var M_poezii */
    public $M_poezii;

    /** @var M_poezii_specii */
    public $M_poezii_specii;

    /** @var M_structuristrofa */
    public $M_structuristrofa;

    /** @var M_rime */
    public $M_rime;

    /** @var M_picioaremetrice */
    public $M_picioaremetrice;

    /** @var M_articole */
    public $M_articole;

    /**
     * MY_Controller constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('text');
        $this->data['ctrl_class'][] = $this->router->class;
        $this->data['breadcrumbs'] = [];
    }

    /**
     * Functie de render views cu ierarhie de template-uri
     *
     * @param string $view
     * @param string $templates
     * @param array  $data
     */
    public function _render($view, $templates = 'template', array $data = [])
    {

        $templateData = ($data) ? $data : $this->data;

        if (!$templates) {
            $content = $this->load->view($view, $templateData, true);
        } else {
            if (!is_array($templates)) {
                $templates = array($templates);
            }

            $content['content'] = $this->load->view($view, $templateData, true);
            $content = array_merge($templateData, $content);

            $c = count($templates);
            for ($i = 0; $i < $c; $i++) {
                if ($i != $c - 1) {
                    $content['content'] = $this->load->view($templates[$i], $content, true);
                } else {
                    $content = $this->load->view($templates[$i], $content, true);
                }
            }
        }

        // injecting CSRF TOKENS
        if ($this->config->item('csrf_protection') === TRUE) {
            // Inject into form
            $content = preg_replace(
                '/(<(form|FORM)[^>]*(method|METHOD)="(post|POST)"[^>]*>)/',
                '$0<input type="hidden" name="' . $this->security->get_csrf_token_name() . '" value="' . $this->security->get_csrf_hash() . '" />',
                $content
            );
            // Inject into <head>
            $content = preg_replace(
                '/(<\/head>)/',
                '<meta name="csrf-name" content="' . $this->security->get_csrf_token_name() . '">' . "\n" . '<meta name="csrf-token" content="' . $this->security->get_csrf_hash() . '">' . "\n" . '$0',
                $content
            );
        }

        $this->output->_display($content);
    }
}
