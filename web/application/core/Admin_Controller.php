<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_Controller extends MY_Controller
{
    /** @var Aauth */
    public $aauth;

    /** @var Mess */
    public $mess;

    /**
     * Admin_Controller constructor.
     */
    public function __construct()
    {
        parent::__construct();

        // Init Messenger object
        $this->mess = new Mess();

        $this->defaultTemplate = array('admin/template');

        // d($this->aauth->is_loggedin());

        // d($this->session->userdata());
        // dd($this->session->userdata('loggedin'));

        if (!$this->aauth->is_loggedin() && !($this->router->class == 'login' && $this->router->method == 'index')) {
            // Redirect loggedin user to dashboard
            redirect('/login');
            exit();
        }

        /**
         * activate DEBUG mode
         */
        if (get('debug')) {
            $this->output->enable_profiler(true);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function _render($view, $templates = 'template', array $data = [])
    {
        parent::_render($view, $templates, $data);
    }

    /**
     * @param        $content
     * @param string $templates
     * @param bool   $data
     */
    public function _render_content($content, $templates = 'template', $data = false)
    {
        // Am comentat-o pentru ca nu era referentiata.
        //parent::_render_content($content, $templates, $data);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param array $data
     */
    public function sendJsonResponse($data)
    {
        header('Content-type: application/json');
        echo json_encode($data);
        exit;
    }

    /**
     * Display autocomplete results in json format and exit.
     * @param string $searchTerm
     * @param array $data
     * @param array $elapsedTime
     */
    public function displayAutocompleteResults($searchTerm, array $data, array $elapsedTime = array())
    {
        $result = array(
            'query'         => $searchTerm,
            'suggestions'   => $data,
            'elapsed'       => $elapsedTime,
        );

        $this->sendJsonResponse($result);
    }
}
