<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Pagination config file
 */

$config['pagination'] = array(
	'per_page' 			=> 10,
	'num_links' 		=> 2,
	'full_tag_open'     => '<ul class="pagination">',
	'full_tag_close'    => '</ul>',
	'cur_tag_open'      => '&nbsp;<li class="active"><span>',
	'cur_tag_close'     => '</span></li>',
	'num_tag_open'      => '<li>',
	'num_tag_close'     => '</li>',
	'next_link'         => false,
	'prev_link'         => false,
	'last_tag_open'     => '<li>',
	'last_tag_close'    => '</li>',
	'first_tag_open'    => '<li>',
	'first_tag_close'   => '</li>',
	'use_page_numbers' 	=> true,
	'page_query_string'	=> true,
	'query_string_segment'	=> '_page',
);