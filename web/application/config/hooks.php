<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/
/**
 * Enables autoloading of base controllers and most used models.
 */
$hook['pre_system'] = array(
    'class'    => 'MY_Autoloader',
    'function' => 'register',
    'filename' => 'MY_Autoloader.php',
    'filepath' => 'hooks',
    'params'   => array(
        APPPATH . 'core/'
        , APPPATH . 'models/Administrativ/'
        , APPPATH . 'models/Biblioteca/'
        , APPPATH . 'models/Nomenclator/'
        , APPPATH . 'models/Generale/'
        , APPPATH . 'models/Personaje/'
        , APPPATH . 'models/Poetiiinchisorilor/'
        , APPPATH . 'models/Media/'
    )
);