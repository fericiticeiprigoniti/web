<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

// -------------------------------------------------------------------------------------------------

/**
 * This constant is used for caching specific collection of data.
 * For example: you have a list with 50 rows and for each row you load a specific model/item,
 * but there are some cases when that model repeats itself.
 * For the first row, the model is loaded from the database, but for the next rows the model is loaded from cache.
 */
define('MODEL_CACHING_ENABLED', true);

// -------------------------------------------------------------------------------------------------

/**
* store general data
* it only works with serialized array
*/
define('STARE_CIVILA', serialize(['1' => 'necăsătorit', '2' => 'căsătorit', '3' => 'divorțat', '4' => 'văduv(ă)']));

// pentru condamnari
define('CONDAMNARI_PEDEPSE_TIPURI', serialize(['1' => 'inchisoare corectionara', '2' => 'temnită grea', '3' => 'munca silnica', '4' => 'munca silnica pe viata'])); // tipuri de detentie

//personaje 2 carti
define('PERSONAJE2CARTI_TIP', serialize(['1' => 'autor', '2' => 'despre personaj']));

define('CARTI_IN_FORMAT', serialize(['1' => 'format tipărit', '2' => 'electronic', '3' => 'tipărit + electronic']));

// Media - video - audio
defined('MEDIA_TIP_VIDEO')        OR define('MEDIA_TIP_VIDEO', 1);
defined('MEDIA_TIP_AUDIO')        OR define('MEDIA_TIP_AUDIO', 2);

// for asociation tables - join Media with poezii/autori
defined('MEDIA_JOIN_POEZII')        OR define('MEDIA_JOIN_POEZII', 1);
defined('MEDIA_JOIN_AUTORI')        OR define('MEDIA_JOIN_AUTORI', 2);

// Taguri
define('CAT_TAGS', serialize(['1' => 'toponime', '2' => 'antroponime','3' => 'evenimente']));

// Roluri personaje
defined('ROL_POET')  OR define('ROL_POET', 12);

// Limit for fetchForAutocomplete
defined('LIMIT_FOR_AUTOCOMPLETE')  OR define('LIMIT_FOR_AUTOCOMPLETE', 20);

/**
 * CATEGORII ARTICOLE
 */
defined('ART_CAT_COMMENTARY')  OR define('ART_CAT_COMMENTARY', 3); // COMENTARIU POEZIE