<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_personaje_ocupatii extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    /**
    * Returneaza lista de ocupatii
    * @param bool $keyValue
    * @return array
    */
    public function fetchAll($keyValue=false)
    {
        $sql = "SELECT * FROM fcp_personaje_ocupatii
                WHERE parent_id != 0
                ORDER BY nume ASC";
                
        if ($keyValue) {
            $result = array();
            $query = $this->db->query($sql);
            foreach ($query->result() as $row) {
                $result[$row->id] = $row->nume;
            }
        } else {
            $result = $this->db->query($sql)->result_array();
        }
        return $result;
    }
    
    /**
    * GET ocupatii by Parent ID
    * @param mixed $parentID
    * @param mixed $keyValue
    * @return array
    */
    public function getList($parentID, $keyValue = false)
    {
        $parentID = (int)$parentID;
        
        $sql = "SELECT * FROM fcp_personaje_ocupatii 
                WHERE parent_id = ?
                ORDER BY nume ASC";
                
        if ($keyValue) {
            $result = array();
            $query = $this->db->query($sql, array($parentID));
            foreach ($query->result() as $row) {
                $result[$row->id] = $row->rol_nume;
            }
        } else {
            $result = $this->db->query($sql, array($parentID))->result_array();
        }
        return $result;    
    }
}