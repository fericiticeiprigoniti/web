<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_categorii extends CI_Model {


    const TABLE = 'fcp_articole_categorii';

    const DELETED_ANY = 2;
    const DELETED_YES = 1;
    const DELETED_NO = 0;

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Fetch list
     * @param array $filters
     * @return array
     */
    public function fetchList($filters=array())
    {
        $sqlWhere = array();
        if (!empty($filters['id'])) {
            $sqlWhere[] = "categorie.id = " . (int)$filters['id'];
        }
        if (!empty($filters['exclude_id'])) {
            $sqlWhere[] = "categorie.id <> " . (int)$filters['exclude_id'];
        }
        if (!empty($filters['parent_id'])) {
            $sqlWhere[] = "categorie.parent_id = " . (int)$filters['parent_id'];
        }
        if (!empty($filters['categorie_nume'])) {
            $sqlWhere[] = "categorie.nume LIKE " . $this->db->escape($filters['categorie_nume']);
        }

        if (isset($filters['deleted'])) {
            if ($filters['deleted'] == self::DELETED_YES) {
                $sqlWhere[] = "categorie.is_deleted = 1";
            }
            if ($filters['deleted'] == self::DELETED_NO) {
                $sqlWhere[] = "categorie.is_deleted = 0";
            }
        } else {
            $sqlWhere[] = "categorie.is_deleted = 0";
        }

        $sqlWhere = count($sqlWhere) ? implode("\n\tAND ", $sqlWhere) : '';
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sql = <<<EOSQL
SELECT
    categorie.*
    , parent.nume AS nume_parinte
    , COUNT(a2c.id) > 0 AS is_used
    , COUNT(a2c.id) AS used_in_articles_count
FROM
    fcp_articole_categorii AS categorie
    LEFT JOIN fcp_articole_categorii AS parent ON categorie.parent_id = parent.id
    LEFT JOIN fcp_articole AS a2c ON categorie.id = a2c.cat_id
WHERE
    1
    $sqlWhere
GROUP BY
    categorie.id
ORDER BY
    is_deleted, nume_parinte, nume;
EOSQL;

        return $this->db->query($sql)->result_array();
    }

    /**
     * Load element by id
     * @param int $id
     * @return array
     */
    public function load($id)
    {
        $data = $this->fetchList(['id'=>$id, 'deleted'=>self::DELETED_ANY]);
        return $data[0] ?? [];
    }

    /**
     * @param string $name
     * @param int|null $excludedId
     * @return array
     */
    public function fetchByName($name, $excludedId = null)
    {
        return $this->fetchList(['categorie_nume'=>$name, 'exclude_id'=>$excludedId]);
    }

    /**
     * @param string $name
     * @param int|null $parentId
     * @param int|null $excludedId
     * @return array
     */
    public function fetchBrothers($name, $parentId = null, $excludedId = null)
    {
        return $this->fetchList(['categorie_nume'=>$name, 'parent_id' => $parentId, 'exclude_id'=>$excludedId]);
    }

    /**
     * @param int $parent_id
     * @return array
     */
    public function fetchChildren($parent_id)
    {
        return $this->fetchList(['parent_id' => $parent_id]);
    }

    /**
     * Fetch parent items
     * @param bool $keyValue
     * @return array
     */
    public function fetchParents($keyValue=false)
    {
        $sql = "SELECT * FROM fcp_articole_categorii
                WHERE parent_id = 0
                ORDER BY nume";

        $result = [];

        if ($keyValue) {
            $query = $this->db->query($sql);
            foreach ($query->result() as $row) {
                $result[$row->id] = $row->nume;
            }
            return $result;
        }

        return $this->db->query($sql)->result_array();
    }

    public function save($data, $id=null)
    {
        $id = (int)$id;
        $eData = [
            'nume'      => (string)$data['nume'],
            'alias'     => (string)$data['alias'],
            'parent_id' => (int)$data['parent_id'],
            'is_published' => (int)$data['is_published'],
            'order_id'  => (int)$data['order_id'],
        ];

        if (!$id) {
            $eData['order_id'] = 999;
            $res = $this->db->insert(self::TABLE, $eData);
        } else {
            $res = $this->db->update(self::TABLE, $eData, ['id'=>$id]);
        }

        return $res;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete($id)
    {
        $id  = (int)$id;
        $eData = ['is_deleted' => 1];

        return $this->db->update(self::TABLE, $eData, ['id'=>$id]);
    }

    /**
     * @param int $id
     * @return bool
     */
    public function restore($id)
    {
        $id  = (int)$id;
        $eData = ['is_deleted' => 0];

        return $this->db->update(self::TABLE, $eData, ['id'=>$id]);
    }

    /**
     * @param array $filters
     * @return array
     */
    public function fetchForSelect($filters)
    {
        $res  = $this->fetchList($filters);

        if (!count($res)) {
            return [];
        }

        $data = [];
        foreach($res as $v) {
            $data[$v['id']] = $v['nume'];
        }

        return $data;
    }

}
