<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_articole extends CI_Model {

    const TABLE = 'fcp_articole';

    function __construct()
    {
        parent::__construct();
    }

    public function load($id)
    {
        return $this->db->get_where(self::TABLE, ['id' => (int)$id])->row_array();
    }

    /**
     * Returneaza lista cu articole
     *
     * @param mixed $pageNO
     * @return array
     */
    public function fetchAll($filters=array(), $order=array(), $limit=array()) {

        // Set filters.
        $sqlWhere = array();
        $sqlHaving = array();
        if (!empty($filters)) {
            foreach($filters as $k=>$v) {
                if ($v == '') {		// Preliminary check.
                    continue;
                }
                switch($k){
                    case 'id':
                        $sqlWhere[] = "fcp_poezii.id = " . $this->db->escape($v);
                        break;
                    case 'titlu':
                        $sqlWhere[] = "titlu like " . $this->db->escape("%" . $v . "%") . "";
                        break;
                    case 'autor':
                        $sqlWhere[] = "fcp_personaje.nume like " . $this->db->escape("%" . $v . "%") . "";
                        break;
                    /*case 'nume_complet':
                        $sqlWhere[] = "CONCAT_WS(' ', p.prefix, p.nume, p.prenume, p.alias) LIKE " . $this->db->escape("%$v%");
                        break;
                    case 'prefix':
                    case 'nume':
                    case 'prenume':
                    case 'alias':
                    case 'observatii':
                    case 'observatii_interne':
                        $sqlWhere[] = "p.$k LIKE " . $this->db->escape("%$v%");
                        break;
                    case 'data_nastere_de_la':
                        if (Calendar::checkDate($v)) {
                            $v = Calendar::convertDate2Mysql($v);
                            $sqlWhere[] = "p.data_nastere <= {$v}";
                        }
                        break;
                    case 'data_nastere_pana_la':
                        if (Calendar::checkDate($v)) {
                            $v = Calendar::convertDate2Mysql($v);
                            $sqlWhere[] = "p.data_nastere >= {$v}";
                        }
                        break;
                    case 'data_adormire_de_la':
                        if (Calendar::checkDate($v)) {
                            $v = Calendar::convertDate2Mysql($v);
                            $sqlWhere[] = "p.data_adormire <= {$v}";
                        }
                        break;
                    case 'data_adormire_pana_la':
                        if (Calendar::checkDate($v)) {
                            $v = Calendar::convertDate2Mysql($v);
                            $sqlWhere[] = "p.data_adormire >= {$v}";
                        }
                        break;
                    case 'loc_nastere':
                        $sqlWhere[] = "loc_n.loc_nume LIKE " . $this->db->escape("%$v%");
                        break;
                    case 'nationalitate':
                        $sqlWhere[] = "nat.nume LIKE " . $this->db->escape("%$v%");
                        break;
                    case 'rol':
                        $mRoluri = new M_roluri();
                        $listaRoluri = $mRoluri->fetchAll(true);
                        if (in_array($v,array_keys($listaRoluri))) {
                            $v = (int)$v;
                            $sqlWhere[] = "p2r.rol_id = {$v}";
                        }
                        break;
                    case 'ocupatie':
                        $mOcupatii = new M_ocupatii();
                        $listaOcupatii = $mOcupatii->fetchAll(true);
                        if (in_array($v,array_keys($listaOcupatii))) {
                            $v = (int)$v;
                            $sqlWhere[] = "p2o.ocupatie_id = {$v}";
                        }
                        break;
                    case 'luna_adormire':
                        $lunileAnului = $this->fetchLunileAnului();
                        if (in_array($v,array_keys($lunileAnului))) {
                            $v = (int)$v;
                            $sqlWhere[] = "MONTH(p.data_adormire) = {$v}";
                        }
                        break;



                    // TODO
                    case 'sex':
                    case 'status':

                        // work in progress...

                        break;*/
                }
            }
        }
        $sqlWhere = implode(" AND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? ' AND ' . $sqlWhere : '';

        $sqlHaving = implode(" AND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? ' HAVING ' . $sqlHaving : '';

        // -------------------------------

        // Set order.
        $sqlOrder = "titlu";
        $defOrderType = "ASC";
        $orderItems = array(
            'id', 'titlu', 'nume_autor'
        );
        if (isset($order['by']) && !empty($order['by']) && in_array($order['by'], $orderItems)) {
            $sqlOrder = $order['by'];
        }
        if (isset($order['dir']) && !empty($order['dir'])) {
            $sqlOrder .= ' ' . (in_array(strtoupper($order['dir']), array('ASC','DESC')) ? $order['dir'] : $defOrderType);
        } else {
            $sqlOrder .= ' ' . $defOrderType;
        }
        $sqlOrder = !empty($sqlOrder) ? "ORDER BY\n\t" . $sqlOrder : '';

        // -------------------------------

        // Set limit.
        if (isset($limit['start']) && isset($limit['per_page'])) {
            $start = (int)$limit['start'];
            $per_page = (int)$limit['per_page'];
            $sqlLimit = "LIMIT $start, $per_page";
        } elseif (isset($limit['start'])) {
            $start = (int)$limit['start'];
            $sqlLimit = "LIMIT $start";
        } else {
            $sqlLimit = '';
        }

        $sql = "
          SELECT fcp_articole.*
          FROM fcp_articole
          WHERE
          1
          {$sqlWhere} AND fcp_articole.is_deleted = 0
          {$sqlOrder}
          {$sqlLimit}"
        ;

        $items = $this->db->query($sql)->result_array();

        return array(
            'items' => $items,
            'count' => count($items),
        );
    }



    /**
    * Insert OR Update
    *
    * @param mixed $data (data sent from post())
    * @param mixed $id (optional)
    */
    public function save($data, $id = null)
    {
        $eData = $this->normalizeModelInputData($data);

        if( !$id )
        {
            return ($this->db->insert(self::TABLE, $eData))? $this->db->insert_id() : FALSE;
        }else{
            return $this->db->update(self::TABLE, $eData, ['id' => (int)$id]);
        }

    }

    // ---------------------------------------------------------------------------------------------

    /**
     * return key -> value array
     * @param array $filters
     * @return array
     */
    public function fetchForSelect($filters = array())
    {
        if (!$filters) {
            $filters = array();
        }
        $result = $this->fetchAll($filters);

        $data = array();
        if (isset($result['items']) && count($result['items'])) {
            /** @var TagItem $objItem */
            foreach($result['items'] as $item) {
                $data[$item['id']] = $item['titlu'];
            }
        }

        return $data;
    }

    // ---------------------------------------------------------------------------------------------


    public function normalizeModelInputData($data)
    {
        $ret = array(
            "titlu"                 => ( array_key_exists('titlu', $data) && !empty($data['titlu']) ) ?             $data['titlu'] : NULL,
            "continut"              => ( array_key_exists('continut', $data) && !empty($data['continut']) ) ?       $data['continut'] : NULL,
            "personaj_id"           => ( array_key_exists('personaj_id', $data) && !empty($data['personaj_id']) ) ? (int)$data['personaj_id'] : NULL,
            "continut"              => ( array_key_exists('continut', $data) && !empty($data['continut'])) ?        $data['continut'] : NULL,
            "user_id"               => $this->aauth->get_user()->id,   // my username id
            "sursa_link"            => ( array_key_exists('sursa_link', $data) && !empty($data['sursa_link'])) ?    $data['sursa_link'] : NULL,
            "sursa_titlu"           => ( array_key_exists('sursa_titlu', $data) && !empty($data['sursa_titlu'])) ?  $data['sursa_titlu'] : NULL,
            "sursa_doc_carte_id"    => ( array_key_exists('sursa_doc_carte_id', $data) && !empty($data['sursa_doc_carte_id'])) ? (int)$data['sursa_doc_carte_id'] : NULL,
            "sursa_doc_publicatie_id"=> ( array_key_exists('sursa_doc_publicatie_id', $data) && !empty($data['sursa_doc_publicatie_id'])) ? (int)$data['sursa_doc_publicatie_id'] : NULL,
            "sursa_doc_start_page"  => array_key_exists('sursa_doc_start_page', $data) ?(int)$data['sursa_doc_start_page'] : null,
            "sursa_doc_end_page"    => array_key_exists('sursa_doc_end_page', $data) ?  (int)$data['sursa_doc_end_page'] : null,
            "data_insert"           => Calendar::sqlDateTime(),
            "data_publicare"        => array_key_exists('data_publicare', $data)? Calendar::convertDate2Mysql($data['data_publicare']) : null,
            "order_id"              => (array_key_exists('order_id', $data) && !empty($data['order_id']))? (int)$data['order_id'] :  999,
            "is_deleted"            => array_key_exists('is_deleted', $data) ? (int)$data['is_deleted'] : 0,
        );

        return $ret;
    }

}
