<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_localitati extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    // ---------------------------------------------------------------------------------------------

	/**
     * Fetch data for autocomplete
     * @param string $searchTerm
     * @return bool|array
     */
    public function fetchForAutocomplete($searchTerm)
    {
        // Clean search term.
        $searchTerm = Strings::sanitizeForDatabaseUsage($searchTerm);

        // Extract words.
        $words = Strings::splitIntoWords($searchTerm);
        if (!count($words)) {
            return false;
        }

        // Set where condition
        $subCond = array();
        foreach ($words as $word) {
            $word = $this->db->escape("%$word%");
            $subCond[] = "(tip_loc.nume LIKE $word OR loc.nume LIKE $word OR loc.nume_anterior LIKE $word OR judet.nume LIKE $word)";
        }
        $sqlWhere = 'AND ' . implode("\n\tAND ", $subCond);

        // Fetch data
        $sql = <<<EOSQL
SELECT
    loc.id
    , CONCAT_WS(', '
        , CONCAT(loc.tip_localitate, ' ', loc.nume)
        , IF(loc.nume_anterior IS NOT NULL, CONCAT(' (', loc.nume_anterior, ')'), NULL)
        , IF(parent_loc.nume IS NOT NULL, CONCAT(parent_loc.tip_localitate, ' ', parent_loc.nume), NULL)
        , CONCAT('judet ', judet.nume)
    ) AS nume_complet
FROM
    nom_localitati AS loc
    LEFT JOIN nom_localitati AS parent_loc ON loc.parent_id = parent_loc.id
    INNER JOIN nom_localitati_tip AS tip_loc ON tip_loc.cod = loc.tip_localitate
    INNER JOIN nom_judete AS judet ON judet.id = loc.id_judet
WHERE
    1
    $sqlWhere
ORDER BY
    tip_loc.ord ASC
LIMIT ?
EOSQL;
        $data = $this->db->query($sql, [LIMIT_FOR_AUTOCOMPLETE])->result_array();
        return $data;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Fetch list
     * @param array $filters
     * @return array
     */
    public function fetchList($filters=array())
    {
        $sqlWhere = array();
        if (isset($filters['id']) && !empty($filters['id'])) {
            $sqlWhere[] = "loc.id = " . (int)$filters['id'];
        }
        if (isset($filters['exclude_id']) && !empty($filters['exclude_id'])) {
            $sqlWhere[] = "loc.id <> " . (int)$filters['exclude_id'];
        }
        $sqlWhere = count($sqlWhere) ? implode("\n\tAND ", $sqlWhere) : '';
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sql = <<<EOSQL
SELECT
    loc.*
    , CONCAT_WS(', '
        , CONCAT(loc.tip_localitate, ' ', loc.nume)
        , IF(loc.nume_anterior IS NOT NULL, CONCAT(' (', loc.nume_anterior, ')'), NULL)
        , IF(parent_loc.nume IS NOT NULL, CONCAT(parent_loc.tip_localitate, ' ', parent_loc.nume), NULL)
        , CONCAT('judet ', judet.nume)
    ) AS nume_complet
FROM
    nom_localitati AS loc
    LEFT JOIN nom_localitati AS parent_loc ON loc.parent_id = parent_loc.id
    INNER JOIN nom_localitati_tip AS tip_loc ON tip_loc.cod = loc.tip_localitate
    INNER JOIN nom_judete AS judet ON judet.id = loc.id_judet
WHERE
    1
    $sqlWhere
GROUP BY
    loc.id
ORDER BY
    nume_complet
EOSQL;
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Load element by id
     * @param int $id
     * @return array
     */
    public function load($id)
    {
        $data = $this->fetchList(array('id'=>$id));
        return isset($data[0]) ? $data[0] : array();
    }


}