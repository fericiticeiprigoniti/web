<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_locuri_patimire_tip extends CI_Model {

    const TABLE = 'fcp_locuri_patimire_tip';

    function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Returneaza lista cu tipurile locurilor de patimire / surghiun
     * @param bool $keyValue
     * @return array
     */
    public function fetchAll($keyValue = false)
    {
        $sql = <<<EOSQL
SELECT
    tip.*
FROM
    fcp_locuri_patimire_tip AS tip
ORDER BY
    tip.id
EOSQL;
        if ($keyValue) {
            $result = array();
            $query = $this->db->query($sql);
            foreach ($query->result() as $row) {
                $result[$row->id] = $row->descriere;
            }
        } else {
            $result = $this->db->query($sql)->result_array();
        }
        return $result;
    }

}
