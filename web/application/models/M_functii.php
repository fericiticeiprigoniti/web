<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_functii extends CI_Model {


    const TABLE = 'fcp_personaje_functii';


    function __construct()
    {
        parent::__construct();
    }

    /**
     * returneaza lista de functii
     *
     * @param bool $keyValue
     * @return array
     */
    public function fetchAll($keyValue = false)
    {
        $sql = "SELECT * FROM fcp_personaje_functii ORDER BY id ASC";
                
        if ($keyValue) {
            $result = array();
            $query = $this->db->query($sql);
            foreach ($query->result() as $row) {
                $result[$row->id] = $row->nume;
            }
        } else {
            $result = $this->db->query($sql)->result_array();
        }
        return $result;
    }
    
    /**
     * adauga o functie cu data inceput - sfarsit unui personaj
     *
     * @param mixed $persID
     * @param mixed $details
     * @return CI_DB_active_record|CI_DB_result
     */
    public function addFunctiePersonaj($persID, $details)
    {
        return $this->db->insert('fcp_personaje2functii', array('personaj_id' => (int)$persID,
                                                                 'functie_id' => (int)$details['functie_id'],
                                                                 'data_start' => Calendar::validateDate(array($details['data_start_zi'],$details['data_start_luna'],$details['data_start_an'])),    
                                                                 'data_end'   => Calendar::validateDate(array($details['data_end_zi'],$details['data_end_luna'],$details['data_end_an']))
                                                                 ));
    }

    /**
     * sterge o functie atribuita unui personaj
     *
     * @param mixed $persID
     * @param mixed $funcID
     * @return CI_DB_active_record|CI_DB_result
     */
    public function deleteFunctiePersonaj($persID, $funcID)
    {
        return $this->db->delete('fcp_personaje2functii', array('personaj_id' => (int)$persID,
                                                                'id'          => (int)$funcID));
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Fetch list
     * @param array $filters
     * @return array
     */
    public function fetchList($filters=array())
    {
        $sqlWhere = array();
        if (isset($filters['id']) && !empty($filters['id'])) {
            $sqlWhere[] = "f.id = " . (int)$filters['id'];
        }
        if (isset($filters['exclude_id']) && !empty($filters['exclude_id'])) {
            $sqlWhere[] = "f.id <> " . (int)$filters['exclude_id'];
        }
        if (isset($filters['nume']) && !empty($filters['nume'])) {
            $sqlWhere[] = "f.nume LIKE " . $this->db->escape($filters['nume']);
        }
        $sqlWhere = count($sqlWhere) ? implode("\n\tAND ", $sqlWhere) : '';
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sql = <<<EOSQL
SELECT
    f.*
    , COUNT(p2f.id) AS is_used
FROM
    fcp_personaje_functii AS f
    LEFT JOIN fcp_personaje2functii AS p2f ON f.id = p2f.functie_id
WHERE
    1
    $sqlWhere
GROUP BY
    f.id
ORDER BY
    nume ASC;
EOSQL;
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    /**
     * Load element by id
     * @param int $id
     * @return array
     */
    public function load($id)
    {
        $data = $this->fetchList(array('id'=>$id));
        return isset($data[0]) ? $data[0] : array();
    }

    /**
     * @param string $name
     * @param int|null $excludedId
     * @return array
     */
    public function fetchByName($name, $excludedId = null)
    {
        return $this->fetchList(array('nume'=>$name, 'exclude_id'=>$excludedId));
    }

    public function save($data, $id=null)
    {
        $id = (int)$id;
        $eData = array(
            'nume' => $data['nume'],
        );

        if (!$id) {
            $res = $this->db->insert(self::TABLE, $eData);
        } else {
            $res = $this->db->update(self::TABLE, $eData, array('id'=>$id));
        }
        return $res;
    }

    public function delete($id)
    {
        $id = (int)$id;
        $res = $this->db->delete(self::TABLE, array('id'=>$id));
        return $res;
    }

}