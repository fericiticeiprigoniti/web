<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_semnalmente_corporale extends CI_Model {

    const TABLE = 'fcp_personaje_semnalmente';

    function __construct()
    {
        parent::__construct();
    }

    /**
     * returneaza lista de semnalmente corporale
     *
     * @param bool $keyValue
     * @return array
     */
    public function fetchAll($keyValue = false)
    {
        $sql = "SELECT * FROM fcp_personaje_semnalmente ORDER BY id ASC";
                
        if ($keyValue) {
            $result = array();
            $query = $this->db->query($sql);
            foreach ($query->result() as $row) {
                $result[$row->id] = $row->nume;
            }
        } else {
            $result = $this->db->query($sql)->result_array();
        }
        return $result;
    }

    /**
     * Fetch list
     * @param array $filters
     * @return array
     */
    public function fetchList($filters=array())
    {
        $sqlWhere = array();
        if (isset($filters['id']) && !empty($filters['id'])) {
            $sqlWhere[] = "s.id = " . (int)$filters['id'];
        }
        if (isset($filters['exclude_id']) && !empty($filters['exclude_id'])) {
            $sqlWhere[] = "s.id <> " . (int)$filters['exclude_id'];
        }
        if (isset($filters['nume']) && !empty($filters['nume'])) {
            $sqlWhere[] = "s.nume LIKE " . $this->db->escape($filters['nume']);
        }
        $sqlWhere = count($sqlWhere) ? implode("\n\tAND ", $sqlWhere) : '';
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sql = <<<EOSQL
SELECT
    s.*
    , COUNT(p2s.id) AS is_used
FROM
    fcp_personaje_semnalmente AS s
    LEFT JOIN fcp_personaje2semnalmente AS p2s ON s.id = p2s.semn_id
WHERE
    1
    $sqlWhere
GROUP BY
    s.id
ORDER BY
    nume;
EOSQL;
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    /**
     * Load element by id
     * @param int $id
     * @return array
     */
    public function load($id)
    {
        $data = $this->fetchList(array('id'=>$id));
        return isset($data[0]) ? $data[0] : array();
    }

    /**
     * @param string $name
     * @param int|null $excludedId
     * @return array
     */
    public function fetchByName($name, $excludedId = null)
    {
        return $this->fetchList(array('nume'=>$name, 'exclude_id'=>$excludedId));
    }

    public function save($data, $id=null)
    {
        $id = (int)$id;
        $eData = array(
            'nume' => $data['nume'],
        );

        if (!$id) {
            $res = $this->db->insert(self::TABLE, $eData);
        } else {
            $res = $this->db->update(self::TABLE, $eData, array('id'=>$id));
        }
        return $res;
    }

    public function delete($id)
    {
        $id = (int)$id;
        $res = $this->db->delete(self::TABLE, array('id'=>$id));
        return $res;
    }

}