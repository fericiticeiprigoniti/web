<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_apartenente_politice extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    /**
    * Returneaza lista de apartenente politice
    * @param bool $keyValue
    * @return array
    */ 
    public function fetchAll($keyValue=false)
    {
        $sql = "SELECT * FROM fcp_apartenente_politice
                ORDER BY nume ASC";
                
        if ($keyValue) {
            $result = array();
            $query = $this->db->query($sql);
            foreach ($query->result() as $row) {
                $result[$row->id] = $row->nume;
            }
        } else {
            $result = $this->db->query($sql)->result_array();
        }
        return $result;
    }
}