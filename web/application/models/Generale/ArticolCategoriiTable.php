<?php

/**
 * Class ArticolCategoriiTable
 * @table fcp_articole_categorii
 */
class ArticolCategoriiTable extends CI_Model
{
    private static ?ArticolCategoriiTable $instance = null;

    /**
     * Singleton
     * ArticolCategoriiTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * We can later dependency inject and thus unit test this
     * @return ArticolCategoriiTable
     */
    public static function getInstance(): self
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param mixed $id
     * @throws Exception
     */
    public function load($id): ?ArticolCategoriiItem
    {
        if (!$id) {
            throw new Exception("Invalid ArticolCategorii id");
        }

        $filters = new ArticolCategoriiFilters(['id' => $id]);
        $data = $this->fetchAll($filters, 1);

        return $data['items'][0] ?? null;
    }

    /**
     * @param mixed $id
     */
    public static function get($id): ?ArticolCategoriiItem
    {
        if (empty($id)) {
            return null;
        }

        try {
            return self::getInstance()->load($id);
        } catch (Throwable $e) {
            log_message('error', $e->getMessage());
            return null;
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param ArticolCategoriiFilters $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchAll(ArticolCategoriiFilters $filters, $limit = null, $offset = null)
    {
        // Set where and having conditions.
        $sqlWhere = [];

        if ($filters->getId()) {
            $sqlWhere[] = "id = {$filters->getId()}";
        }
        if ($filters->getExcludedId()) {
            $sqlWhere[] = "id <> {$filters->getExcludedId()}";
        }
        if ($filters->getParentId()) {
            $sqlWhere[] = "parent_id = {$filters->getParentId()}";
        }

        //    status_id  tinyint unsigned default 0 null comment '0 - in lucru / unpublished; 1 - activ / published',
        if ($filters->isPublished()) {
            $val = $filters->isPublished() === $filters::PUBLISHED_YES ? 1 : 0;
            $sqlWhere[] = "is_published = $val";
        }

        if ($filters->getSearchTerm()) {
            // Clean search term, extract words and set where condition.
            $searchTerm = Strings::sanitizeForDatabaseUsage($filters->getSearchTerm());
            $words = Strings::splitIntoWords($searchTerm);
            if (count($words)) {
                $subCond = array();
                foreach ($words as $word) {
                    $word = $this->db->escape("%$word%");
                    $subCond[] = "(id LIKE $word OR nume LIKE $word OR alias LIKE $word)";
                }
                $sqlWhere[] = implode("\n\tAND ", $subCond);
            }
        }

        $sqlWhere = implode("\n\tAND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        // ------------------------------------

        $sqlOrder = array();
        if (count($filters->getOrderBy())) {
            foreach($filters->getOrderBy() as $ord) {
                switch($ord) {
                    case $filters::ORDER_BY_ID_ASC:
                        $sqlOrder[] = "id ASC";
                        break;
                    case $filters::ORDER_BY_ID_DESC:
                        $sqlOrder[] = "id DESC";
                        break;
        }
            }
        }
        if (!count($sqlOrder)) {
            $sqlOrder[] = "id DESC";
        }
        $sqlOrder = implode(", ", $sqlOrder);

        // ------------------------------------

        // Set limit.
        $sqlLimit = '';
        if (!is_null($offset) && !is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$offset . ', ' . (int)$limit;
        } elseif (!is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$limit;
        }

        // ------------------------------------

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    *
FROM
    fcp_articole_categorii
WHERE
    1
    $sqlWhere
GROUP BY id
ORDER BY $sqlOrder
$sqlLimit
EOSQL;

        /** @var CI_DB_result $result */
        $result = $this->db->query($sql);
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        $items = array();
        foreach($result->result('array') as $row){
            $items[] = new ArticolCategoriiItem($row);
        }

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param ArticolCategoriiItem $item
     * @return bool|int
     */
    public function save(ArticolCategoriiItem $item)
    {
        // Set data.
        $eData = $this->setData($item);

        // Insert/Update.
        if (!$item->getId()) {
            $res = $this->db->insert('fcp_articole_categorii', $eData);
        } else {
            $res = $this->db->update('fcp_articole_categorii', $eData, 'id = ' . $item->getId());
        }

        return $res;
    }

    private function setData(ArticolCategoriiItem $item): array
    {
        return [
            'parent_id' => $item->getParentId() ?: null,
            'nivel' => $item->getNivel() ?: null,
            'nume' => $item->getNume() ?: null,
            'alias' => $item->getAlias() ?: null,
            'order_id' => $item->getOrderId() ?: null,
            'is_deleted' => $item->getIsDeleted() ?: null,
        ];
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param ArticolCategoriiItem $item
     * @return mixed
     */
    public function delete(ArticolCategoriiItem $item)
    {
        return $this->db->delete('fcp_articole_categorii', 'id = ' . $item->getId());
    }
}
