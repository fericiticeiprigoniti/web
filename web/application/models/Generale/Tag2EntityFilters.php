<?php

class Tag2EntityFilters
{

    const ORDER_BY_ID_ASC = 'id_asc';
    const ORDER_BY_ID_DESC = 'id_desc';

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $excludedId;

    /**
     * @var int
     */
    private $tagId;

    /**
     * @var int
     */
    private $poezieId;

    /**
     * @var int
     */
    private $articolId;

    /**
     * @var int
     */
    private $carteId;

    /**
     * @var array
     */
    private $orderBy = [];

    // ---------------------------------------------------------------------------------------------

    public function __construct(array $getData = [])
    {
        if (isset($getData['id'])) {
            $this->setId($getData['id']);
        }
        if (isset($getData['excludedId'])) {
            $this->setExcludedId($getData['excludedId']);
        }
        if (isset($getData['tagId'])) {
            $this->setTagId($getData['tagId']);
        }
        if (isset($getData['poezieId'])) {
            $this->setPoezieId($getData['poezieId']);
        }
        if (isset($getData['articolId'])) {
            $this->setArticolId($getData['articolId']);
        }
        if (isset($getData['carteId'])) {
            $this->setCarteId($getData['carteId']);
        }
        if (isset($getData['_orderBy'])) {
            $this->setOrderBy($getData['_orderBy']);
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return int
     */
    public function getExcludedId()
    {
        return $this->excludedId;
    }

    /**
     * @param int $excludedId
     */
    public function setExcludedId($excludedId)
    {
        $this->excludedId = $excludedId ? (int)$excludedId : null;
    }

    /**
     * @return int
     */
    public function getTagId()
    {
        return $this->tagId;
    }

    /**
     * @param int $tagId
     */
    public function setTagId($tagId)
    {
        $this->tagId = $tagId ? (int)$tagId : null;
    }

    /**
     * @return int
     */
    public function getPoezieId()
    {
        return $this->poezieId;
    }

    /**
     * @param int $poezieId
     */
    public function setPoezieId($poezieId)
    {
        $this->poezieId = $poezieId ? (int)$poezieId : null;
    }

    /**
     * @return int
     */
    public function getArticolId()
    {
        return $this->articolId;
    }

    /**
     * @param int $articolId
     */
    public function setArticolId($articolId)
    {
        $this->articolId = $articolId ? (int)$articolId : null;
    }

    /**
     * @return int
     */
    public function getCarteId()
    {
        return $this->carteId;
    }

    /**
     * @param int $carteId
     */
    public function setCarteId($carteId)
    {
        $this->carteId = $carteId ? (int)$carteId : null;
    }

    /**
     * @return array
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param array $orderBy
     */
    public function setOrderBy(array $orderBy)
    {
        $orderBy = (is_array($orderBy) && count($orderBy) ? $orderBy : array());
        if (count($orderBy)) {
            $orderItems = array_keys(self::fetchOrderItems());
            foreach ($orderBy as $k=>$v) {
                if (!in_array($v, $orderItems)) {
                    unset($orderBy[$k]);
                }
            }
        }
        $this->orderBy = $orderBy;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return array
     */
    public static function fetchOrderItems()
    {
        return array(
            self::ORDER_BY_ID_ASC => 'id_asc',
            self::ORDER_BY_ID_DESC => 'id_desc',
        );
    }


}
