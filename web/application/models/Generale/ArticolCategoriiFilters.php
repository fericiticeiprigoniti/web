<?php

class ArticolCategoriiFilters
{

    const ORDER_BY_ID_ASC = 'id_asc';
    const ORDER_BY_ID_DESC = 'id_desc';

    const PUBLISHED_YES     = 'yes';
    const PUBLISHED_NO      = 'no';


    /**
     * @var int 
     * @example Id-ul categoriei
     */
    private $id;

    /**
     * @var int
     * @example Exclude o categorie din lista cu raspunsuri
     */
    private $excludedId;

    /**
     * @var int
     * @example 
     */
    private $parentId;

    /**
     * @var string
     * @docs_ignore
     */
    private $isPublished;

    /**
     * @var string
     */
    private $searchTerm;

    /**
     * @var array
     */
    private array $orderBy = [];

    // ---------------------------------------------------------------------------------------------

    public function __construct(array $getData = [])
    {
        if (isset($getData['id'])) {
            $this->setId($getData['id']);
        }
        if (isset($getData['excludedId'])) {
            $this->setExcludedId($getData['excludedId']);
        }

        if (isset($getData['parentId'])) {
            $this->setParentId($getData['parentId']);
        }

        if (isset($getData['isPublished'])) {
            $this->setIsPublished($getData['isPublished']);
        }

        if (isset($getData['searchTerm'])) {
            $this->setSearchTerm($getData['searchTerm']);
        }

        if (isset($getData['_orderBy'])) {
            $this->setOrderBy($getData['_orderBy']);
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return int
     */
    public function getExcludedId()
    {
        return $this->excludedId;
    }

    /**
     * @param int $excludedId
     */
    public function setExcludedId($excludedId)
    {
        $this->excludedId = $excludedId ? (int)$excludedId : null;
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param int $parentId
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId ? (int)$parentId : null;
    }

    /**
     * @return array
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param array $orderBy
     */
    public function setOrderBy(array $orderBy)
    {
        $orderBy = (count($orderBy) ? $orderBy : []);

        if (!count($orderBy)) {
            return;
        }

        $orderItems = array_keys(self::fetchOrderItems());
        foreach ($orderBy as $k=>$v) {
            if (!in_array($v, $orderItems)) {
                unset($orderBy[$k]);
            }
        }

        $this->orderBy = $orderBy;
    }

    /**
     * @return string|null
     */
    public function isPublished()
    {
        return $this->isPublished;
    }

    /**
     * @var string|null $value
     */
    public function setIsPublished($value)
    {
        $this->isPublished = is_null($value) ? null : trim(strip_tags($value));
    }

    /**
     * @return string|null
     */
    public function getSearchTerm()
    {
        return $this->searchTerm;
    }

    /**
     * @param string|null $val
     */
    public function setSearchTerm($val)
    {
        $this->searchTerm = is_null($val) ? null : trim(strip_tags($val));
    }


    // ---------------------------------------------------------------------------------------------

    /**
     * @return array
     */
    public static function fetchOrderItems()
    {
        return array(
            self::ORDER_BY_ID_ASC => 'id_asc',
            self::ORDER_BY_ID_DESC => 'id_desc',
        );
    }

    /**
     * @return string[]
     */
    public static function fetchPublishedList()
    {
        return [
            self::PUBLISHED_YES   => 'Publicat',
            self::PUBLISHED_NO    => 'Nepublicat',
        ];
    }
}
