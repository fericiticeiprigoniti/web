<?php

/**
 * Class Tag2EntityTable
 * @table fcp_taguri2entitati
 */
class Tag2EntityTable extends CI_Model
{

    /**
     * Used for caching items
     * @var Tag2EntityItem[]
     */
    public static $collection = [];

    /**
     * @var Tag2EntityTable
     */
    private static $instance;


    /**
     * Singleton
     * Tag2EntityTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * We can later dependency inject and thus unit test this
     * @return Tag2EntityTable
     */
    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @param int $id
     */
    public static function resetCollectionId($id)
    {
        if (isset(self::$collection[$id])) {
            unset(self::$collection[$id]);
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param mixed $id
     * @return Tag2EntityItem|null
     * @throws Exception
     */
    public function load($id)
    {
        if (!$id) {
            throw new Exception("Invalid Tag2Entity id");
        }

        // If data is cached return it
        if (MODEL_CACHING_ENABLED && isset(self::$collection[$id])) {
            return self::$collection[$id];
        }

        // Fetch data
        $filters = new Tag2EntityFilters(array('id'=>$id));
        $data = $this->fetchAll($filters);

        // Cache result
        self::$collection[$id] = isset($data['items'][0]) ? $data['items'][0] : null;

        // Return cached result
        return self::$collection[$id];
    }

    /**
     * Reset item from static collection and load a new one
     * @param mixed $id
     * @return Tag2EntityItem|null
     * @throws Exception
     */
    public function reload($id)
    {
        self::resetCollectionId($id);
        return $this->load($id);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param Tag2EntityFilters $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchAll(Tag2EntityFilters $filters = null, $limit = null, $offset = null)
    {
        // Set where and having conditions.
        $sqlWhere = array();
        $sqlHaving = array();
        if ($filters) {
            if ($filters->getId()) {
                $sqlWhere[] = "id = {$filters->getId()}";
            }
            if ($filters->getExcludedId()) {
                $sqlWhere[] = "id <> {$filters->getExcludedId()}";
            }
            if ($filters->getTagId()) {
                $sqlWhere[] = "tag_id = {$filters->getTagId()}";
            }
            if ($filters->getPoezieId()) {
                $sqlWhere[] = "poezie_id = {$filters->getPoezieId()}";
            }
            if ($filters->getArticolId()) {
                $sqlWhere[] = "articol_id = {$filters->getArticolId()}";
            }
            if ($filters->getCarteId()) {
                $sqlWhere[] = "carte_id = {$filters->getCarteId()}";
            }
        }

        $sqlWhere = implode("\n\tAND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sqlHaving = implode("\n\tAND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? "HAVING " . $sqlHaving : '';

        // ------------------------------------

        $sqlOrder = array();
        if ($filters && count($filters->getOrderBy())) {
            foreach($filters->getOrderBy() as $ord) {
                switch($ord) {
                    case $filters::ORDER_BY_ID_ASC:
                        $sqlOrder[] = "id ASC";
                        break;
                    case $filters::ORDER_BY_ID_DESC:
                        $sqlOrder[] = "id DESC";
                        break;
        }
            }
        }
        if (!count($sqlOrder)) {
            $sqlOrder[] = "id DESC";
        }
        $sqlOrder = implode(", ", $sqlOrder);

        // ------------------------------------

        // Set limit.
        $sqlLimit = '';
        if (!is_null($offset) && !is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$offset . ', ' . (int)$limit;
        } elseif (!is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$limit;
        }

        // ------------------------------------

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    *
FROM
    fcp_taguri2entitati
WHERE
    1
    $sqlWhere
GROUP BY id
$sqlHaving
ORDER BY $sqlOrder
$sqlLimit
EOSQL;

        /** @var CI_DB_result $result */
        $result = $this->db->query($sql);
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        $items = array();
        foreach($result->result('array') as $row){
            $items[] = new Tag2EntityItem($row);
        }

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param Tag2EntityItem $item
     * @return bool|int
     */
    public function save(Tag2EntityItem $item)
    {
        // Set data.
        $eData = array(
            'tag_id' => $item->getTagId() ? $item->getTagId() : null,
            'poezie_id' => $item->getPoezieId() ? $item->getPoezieId() : null,
            'articol_id' => $item->getArticolId() ? $item->getArticolId() : null,
            'carte_id' => $item->getCarteId() ? $item->getCarteId() : null,
        );

        // Insert/Update.
        if (!$item->getId()) {
            $res = $this->db->insert('fcp_taguri2entitati', $eData);
        } else {
            $res = $this->db->update('fcp_taguri2entitati', $eData, 'id = ' . $item->getId());
        }

        // Remove old item from cache
        self::resetCollectionId($item->getId());

        return $res;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param Tag2EntityItem $item
     * @return mixed
     */
    public function delete(Tag2EntityItem $item)
    {
        // Remove old item from cache
        self::resetCollectionId($item->getId());

        return $this->db->delete('fcp_taguri2entitati', 'id = ' . $item->getId());
    }

    // ---------------------------------------------------------------------------------------------

     // ---------------------------------------------------------------------------------------------

    /**
     * @param int $tagId
     * @return int[]
     */
    public function fetchPoetryIdListByTagId($tagId)
    {
        return $this->fetchPoetryIdListByTagIdList([$tagId]);
    }

    /**
     * @param int[] $tagIdList
     * @return int[]
     */
    public function fetchPoetryIdListByTagIdList(array $tagIdList)
    {
        $tagIdList = array_map('intval', $tagIdList);
        $tagList = count($tagIdList) ? implode(',', $tagIdList) : 'NULL';
        $result = $this->db->query("SELECT DISTINCT poezie_id FROM fcp_taguri2entitati WHERE tag_id IN ($tagList)");

        $idList = array();
        foreach($result->result('array') as $row){
            $idList[] = (int)$row['poezie_id'];
        }

        return $idList;
    }

    /**
     * @param int $poetryId
     * @return int[]
     */
    public function fetchTagIdListByPoetryId($poetryId)
    {
        $poetryId = (int)$poetryId;
        $result = $this->db->query("SELECT DISTINCT tag_id FROM fcp_taguri2entitati WHERE poezie_id = $poetryId");

        $idList = array();
        foreach($result->result('array') as $row){
            $idList[] = (int)$row['tag_id'];
        }

        return $idList;
    }

     /**
     * @param int $articleId
     * @return int[]
     */
    public function fetchTagIdListByArticleId($articleId)
    {
        $articleId = (int)$articleId;
        $result = $this->db->query("SELECT DISTINCT tag_id FROM fcp_taguri2entitati WHERE articol_id = $articleId");

        $idList = array();
        foreach($result->result('array') as $row){
            $idList[] = (int)$row['tag_id'];
        }

        return $idList;
    }

}
