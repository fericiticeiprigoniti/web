<?php

class ArticolItem
{
    use ArrayOrJson;

    const TIP_SURSA_CARTE = 1;
    const TIP_SURSA_REVISTA = 2;

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $catId;

    /**
     * @var string
     */
    private $titlu;

    /**
     * @var string
     */
    private $continut;

    /**
     * @var int
     */
    private $autorId;

    /**
     * @var int
     */
    private $personajId;

    /**
     * @var int
     */
    private $userId;

    /**
     * @var AauthuserItem
     */
    private $user;

    /**
     * @var string
     */
    private $sursaLink;

    /**
     * @var string
     */
    private $sursaTitlu;

    /**
     * @var int
     */
    private $sursaDocCarteId;

    /**
     * @var int
     */
    private $sursaDocPublicatieId;

    /**
     * @var string
     */
    private $sursaCulegator;

    /**
     * @var int
     */
    private $sursaDocStartPage;

    /**
     * @var int
     */
    private $sursaDocEndPage;

    /**
     * @var DateTime
     */
    private $dataInsert;

    /**
     * @var DateTime
     */
    private $dataPublicare;

    /**
     * @var string
     */
    private $seoMetadescription;

    /**
     * @var string
     */
    private $seoMetakeywords;

    /**
     * @var int
     */
    private $orderId;

    /**
     * @var int
     */
    private $hits;

    /**
     * @var bool
     */
    private $isDeleted;

    /**
     * @var bool
     */
    private $isPublished;

    private $arrTagKeys;

    # ==========================================================================
    #
    #   Asociation table -> fcp_autori
    #
    # ==========================================================================

    /**
     * @var AutorItem $objAutor
     */
    private $objAutor;

    # ==========================================================================
    #
    #   Asociation table -> fcp_personaje
    #
    # ==========================================================================

    /**
     * @var PersonajItem $objPersonaj
     */
    private $objPersonaj;

    # ==========================================================================
    #
    #   Asociation table -> fcp_biblioteca_carti
    #
    # ==========================================================================

    /**
     * @var CarteItem $objCarte
     */
    private $objCarte;


    # ==========================================================================
    #
    #   Asociation table -> fcp_articole_categorii
    #
    # ==========================================================================

    /**
     * @var ArticolCategoriiItem|null $objCategorie
     */
    private $objCategorie;


    // ---------------------------------------------------------------------------------------------

    public function __construct(array $dbRow = array())
    {
        if (!count($dbRow)) {
            return;
        }

        $this->id               = (int)$dbRow['id'];
        $this->catId            = (int)$dbRow['cat_id'];
        $this->titlu            = $dbRow['titlu'];
        $this->continut         = $dbRow['continut'];
        $this->autorId          = $dbRow['autor_id'];
        $this->personajId       = $dbRow['personaj_id'];
        $this->userId           = $dbRow['user_id'];
        $this->sursaLink        = $dbRow['sursa_link'];
        $this->sursaTitlu        = $dbRow['sursa_titlu'];
        $this->sursaDocCarteId      = $dbRow['sursa_doc_carte_id'] ? (int)$dbRow['sursa_doc_carte_id'] : null;
        $this->sursaDocPublicatieId = $dbRow['sursa_doc_publicatie_id'] ? (int)$dbRow['sursa_doc_publicatie_id'] : null;
        $this->sursaDocStartPage    = $dbRow['sursa_doc_start_page'] ? (int)$dbRow['sursa_doc_start_page'] : null;
        $this->sursaDocEndPage      = $dbRow['sursa_doc_end_page'] ? (int)$dbRow['sursa_doc_end_page'] : null;
        $this->sursaCulegator       = $dbRow['sursa_culegator'] ? (string)$dbRow['sursa_culegator'] : null;

        $this->dataInsert = Calendar::mysqlDateTimeToDateTimeObject($dbRow['data_insert']);
        $this->dataPublicare = Calendar::mysqlDateToDateTime($dbRow['data_publicare']);

        $this->seoMetadescription = $dbRow['seo_metadescription'] ? $dbRow['seo_metadescription'] : null;
        $this->seoMetakeywords    = $dbRow['seo_metakeywords'] ? $dbRow['seo_metakeywords'] : null;

        $this->orderId      = $dbRow['order_id'];
        $this->hits         = $dbRow['hits'];
        $this->isDeleted    = (bool)$dbRow['is_deleted'];
        $this->isPublished  = $dbRow['is_published'] ? (int)$dbRow['is_published'] : 0;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @param int|null $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getTitlu()
    {
        return $this->titlu;
    }

    /**
     * @return int|null
     */
    public function getCatId()
    {
        return $this->catId;
    }

    /**
     * @param int|null $catId
     */
    public function setCatId($catId)
    {
        $this->catId = $catId ? (int)$catId : null;
    }

    /**
     * @param string|null $titlu
     * @throws Exception
     */
    public function setTitlu($titlu)
    {
        $this->titlu = trim(strip_tags($titlu));
        if (empty($this->titlu)) {
            throw new Exception('Completati titlul articolului');
        }
    }


    /**
     * @return string|null
     */
    public function getContinut()
    {
        return $this->continut;
    }

    /**
     * @param string|null $continut
     * @throws Exception
     */
    public function setContinut($continut)
    {
        $this->continut = $continut ? trim($continut) : null;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getAutorId()
    {
        return $this->autorId;
    }

    /**
     * @param int|null $autorId
     * @throws Exception
     */
    public function setAutorId($autorId)
    {
        $this->autorId = (int)$autorId;
    }

    /**
     * @return int|null
     */
    public function getPersonajId()
    {
        return $this->personajId;
    }

    /**
     * @param int|null $personajId
     * @throws Exception
     */
    public function setPersonajId($personajId)
    {
        $this->personajId = $personajId ? (int)$personajId : null;

        if ($this->personajId) {
            $this->personaj = PersonajTable::getInstance()->load($this->personajId);
            if (!$this->personaj) {
                throw new Exception('Autorul nu a fost gasit in baza de date');
            }
        }
    }

    /**
     * @return PersonajItem|null
     * @throws Exception
     */
    public function getPersonaj()
    {
        if ($this->objPersonaj) {
            return $this->objPersonaj;
        } elseif ($this->personajId) {
            $this->objPersonaj = PersonajTable::getInstance()->load($this->personajId);
            return $this->objPersonaj;
        } else {
            return null;
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int|null $userId
     * @throws Exception
     */
    public function setUserId($userId)
    {
        $this->userId = $userId ? (int)$userId : null;
        if ($this->userId) {
            $this->user = AauthUserTable::getInstance()->load($this->userId);
            if (!$this->user) {
                throw new Exception('Utilizatorul nu a fost gasit in baza de date');
            }
        }
    }

    /**
     * @return AauthuserItem|null
     * @throws Exception
     */
    public function getUser()
    {
        if ($this->user) {
            return $this->user;
        } elseif ($this->userId) {
            $this->user = AauthUserTable::getInstance()->load($this->userId);
            return $this->user;
        } else {
            return null;
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return string|null
     */
    public function getSursaLink()
    {
        return $this->sursaLink;
    }

    /**
     * @param string|null $sursaLink
     * @throws Exception
     */
    public function setSursalink($sursaLink)
    {
        $this->sursaLink = $sursaLink ? trim(strip_tags($sursaLink)) : null;
    }

    /**
     * @return string|null
     */
    public function getSursaTitlu()
    {
        return $this->sursaTitlu;
    }

    /**
     * @param string|null $sursaTitlu
     * @throws Exception
     */
    public function setSursatitlu($sursaTitlu)
    {
        $this->sursaTitlu = $sursaTitlu ? trim(strip_tags($sursaTitlu)) : null;
    }

    /**
     * @return int
     */
    public function getSursaDocCarteId()
    {
        return $this->sursaDocCarteId;
    }

    /**
     * @param int $sursaDocCarteId
     */
    public function setSursaDocCarteId($sursaDocCarteId)
    {
        $this->sursaDocCarteId = $sursaDocCarteId ? (int)$sursaDocCarteId : null;
    }

    /**
     * @return int
     */
    public function getSursaDocPublicatieId()
    {
        return $this->sursaDocPublicatieId;
    }

    /**
     * @param int $sursaDocPublicatieId
     */
    public function setSursaDocPublicatieId($sursaDocPublicatieId)
    {
        $this->sursaDocPublicatieId = $sursaDocPublicatieId ? (int)$sursaDocPublicatieId : null;
    }

    /**
     * @return int|null
     */
    public function getSursaDocStartPage()
    {
        return $this->sursaDocStartPage;
    }

    /**
     * @param int|null $sursaDocStartPage
     * @throws Exception
     */
    public function setSursaDocStartPage($sursaDocStartPage)
    {
        $this->sursaDocStartPage = $sursaDocStartPage ? (int)$sursaDocStartPage : null;
    }

    /**
     * @return int|null
     */
    public function getSursaDocEndPage()
    {
        return $this->sursaDocEndPage;
    }

    /**
     * @param int|null $sursaDocEndPage
     * @throws Exception
     */
    public function setSursaDocEndPage($sursaDocEndPage)
    {
        $this->sursaDocEndPage = $sursaDocEndPage ? (int)$sursaDocEndPage : null;
    }

    /**
     * @return string
     */
    public function getSursaCulegator()
    {
        return $this->sursaCulegator;
    }

    /**
     * @param string $sursaCulegator
     */
    public function setSursaCulegator($sursaCulegator)
    {
        $this->sursaCulegator = $sursaCulegator ? (string)$sursaCulegator : null;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return DateTime
     */
    public function getDataInsert()
    {
        return $this->dataInsert;
    }

    /**
     * @return DateTime
     */
    public function getDataPublicare()
    {
        return $this->dataPublicare;
    }

    /**
     * @param DateTime|null $date
     */
    public function setDataPublicare(DateTime $date = null)
    {
        $this->dataPublicare = $date;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int|null $orderId
     * @throws Exception
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId ? (int)$orderId : null;
    }

    /**
     * @return int|null
     */
    public function getHits()
    {
        return $this->hits ?? 0;
    }

    /**
     * @param int|null $hits
     * @throws Exception
     */
    public function setHits($hits)
    {
        $this->hits = $hits ? (int)$hits : null;
    }


    /**
     * @return string
     */
    public function getSeoMetadescription()
    {
        return $this->seoMetadescription;
    }

    /**
     * @param string $description
     */
    public function setSeoMetadescription($description)
    {
        $this->seoMetadescription = $description ? (string)$description : null;
    }

    /**
     * @return string
     */
    public function getSeoMetakeywords()
    {
        return $this->seoMetakeywords;
    }

    /**
     * @param string $keywords
     */
    public function setSeoMetakeywords($keywords)
    {
        $this->seoMetakeywords = $keywords ? (string)$keywords : null;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int
     */
    public function isPublished()
    {
        return $this->isPublished;
    }

    /**
     * @param int $isPublished
     */
    public function setIsPublished($isPublished)
    {
        // TODO - de vazut daca lasam string sau INT
        $this->isPublished = $isPublished ? (int)$isPublished : null;
    }

    /**
     * @return boolean
     */
    public function isDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @var boolean $value
     */
    public function setIsDeleted($value)
    {
        $this->isDeleted = (bool)$value;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return string
     */
    public function getSearchResult()
    {
        return $this->id . ' | ' . $this->titlu;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * @return AutorItem|null
     * @throws Exception
     */
    public function getAutor()
    {
        $this->objAutor = $this->objAutor ?: AutorTable::get($this->autorId);
        return $this->objAutor;
    }

    /**
     * @return CarteItem|null
     */
    public function getCarte()
    {
        $this->objCarte = $this->objCarte ?: CarteTable::get($this->sursaDocCarteId);
        return $this->objCarte;
    }

    /**
     * @param array
     */
    public function setArrTagKeys($tags)
    {
        $this->arrTagKeys = $tags;
    }

    /**
     * returneaza lista cu tag keys
     *
     * @return array|bool
     */
    public function getArrTagKeys()
    {
        if( $this->arrTagKeys )
        {
            return $this->arrTagKeys;
        }elseif( $list = $this->fetchTagList() ){

            foreach($list as $tag)
            {
                $this->arrTagKeys[] = $tag->getId();
            }

            return $this->arrTagKeys;
        }else{
            return [];
        }
    }

    /**
     * TODo
     *
     * @return TagItem[]|null
     */
    public function fetchTagList()
    {
        if (!$this->id) {
            return null;
        }

        $tagIdList = Tag2EntityTable::getInstance()->fetchTagIdListByArticleId($this->id);
        if (!count($tagIdList)) {
            return null;
        }

        $filters = new TagFilters();
        $filters->setIdList($tagIdList);
        $res = TagTable::getInstance()->fetchAll($filters);

        return $res && isset($res['items']) ? $res['items'] : [];
    }

    public function getCategorie(): ?ArticolCategoriiItem
    {
        $this->objCategorie = $this->objCategorie ?: ArticolCategoriiTable::get($this->catId);
        return $this->objCategorie;
    }
}
