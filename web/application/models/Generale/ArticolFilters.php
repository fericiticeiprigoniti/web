<?php

class ArticolFilters
{

    const ORDER_BY_ID_ASC = 'id_asc';
    const ORDER_BY_ID_DESC = 'id_desc';
    const ORDER_BY_TITLU_ASC = 'titlu_asc';
    const ORDER_BY_TITLU_DESC = 'titlu_desc';
    const ORDER_BY_PERSONAJ_ASC = 'personaj_asc';
    const ORDER_BY_PERSONAJ_DESC = 'personaj_desc';
    const ORDER_BY_DATA_INSERT_ASC = 'data_insert_asc';
    const ORDER_BY_DATA_INSERT_DESC = 'data_insert_desc';
    const ORDER_BY_DATA_PUBLICARE_ASC = 'data_publicare_asc';
    const ORDER_BY_DATA_PUBLICARE_DESC = 'data_publicare_desc';
    const ORDER_BY_ORDER_ASC = 'order_asc';
    const ORDER_BY_ORDER_DESC = 'order_desc';
    const ORDER_BY_RAND       = 'order_rand';

    const PUBLISHED_YES     = 'yes';
    const PUBLISHED_NO      = 'no';

    /**
     * @var int
     * @example Id-ul articolului
     */
    private $id;

    /**
     * @var int
     * @example Excluse un articol din lista cu raspunsuri
     */
    private $excludedId;

    /**
     * @var int
     * @example Categoria
     */
    private $catId;

    /**
     * @var string
     * @example Caută in titlul articolului
     */
    private $titluLike;

    /**
     * @var string
     * @example Caută un text in continutul articolului
     */
    private $continutLike;

    /**
     * @var int
     * @example ID-ul personajului
     */
    private $personajId;

    /**
     * @var int
     * @example ID-ul utilizatorului care a adaugat poezie
     */
    private $userId;

    /**
     * @var bool
     * @docs_ignore
     */
    private $isDeleted;

    /**
     * @var bool
     * @docs_ignore
     */
    private $isPublished;

    /**
     * @var string
     */
    private $searchTerm;

    /**
     * @var array | string
     * @example Suporta: id_asc / id_desc / titlu_asc / titlu_desc / personaj_asc / personaj_desc / data_insert_asc / data_insert_desc / data_publicare_asc / data_publicare_desc / order_asc / order_desc / order_rand
     *
     */
    private $orderBy = [];

    // ---------------------------------------------------------------------------------------------

    public function __construct(array $getData = array())
    {
        if (isset($getData['id'])) {
            $this->setId($getData['id']);
        }
        if (isset($getData['excludedId'])) {
            $this->setExcludedId($getData['excludedId']);
        }
        if (isset($getData['catId'])) {
            $this->setCatId($getData['catId']);
        }
        if (isset($getData['titluLike'])) {
            $this->setTitluLike($getData['titluLike']);
        }
        if (isset($getData['continutLike'])) {
            $this->setContinutLike($getData['continutLike']);
        }
        if (isset($getData['personajId'])) {
            $this->setPersonajId($getData['personajId']);
        }
        if (isset($getData['userId'])) {
            $this->setUserId($getData['userId']);
        }
        if (isset($getData['isDeleted'])) {
            $this->setIsDeleted($getData['isDeleted']);
        }
        if (isset($getData['searchTerm'])) {
            $this->setSearchTerm($getData['searchTerm']);
        }

        if (isset($getData['_orderBy'])) {
            $arr_order = is_array($getData['_orderBy'])? $getData['_orderBy'] : [$getData['_orderBy']];
            $this->setOrderBy($arr_order);
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return int|null
     */
    public function getExcludedId()
    {
        return $this->excludedId;
    }

    /**
     * @param int|null $excludedId
     */
    public function setExcludedId($excludedId)
    {
        $this->excludedId = $excludedId ? (int)$excludedId : null;
    }

    /**
     * @return int|null
     */
    public function getCatId()
    {
        return $this->catId;
    }

    /**
     * @param int|null $catId
     */
    public function setCatId($catId)
    {
        $this->catId = $catId ? (int)$catId : null;
    }
    
    /**
     * @return string|null
     */
    public function getTitluLike()
    {
        return $this->titluLike;
    }
    
    /**
     * @param string|null $titluLike
     */
    public function setTitluLike($titluLike)
    {
        $this->titluLike = is_null($titluLike) ? null : trim(strip_tags($titluLike));
    }

    /**
     * @return string|null
     */
    public function getContinutLike()
    {
        return $this->continutLike;
    }

    /**
     * @param string|null $continutLike
     */
    public function setContinutLike($continutLike)
    {
        $this->continutLike = is_null($continutLike) ? null : trim(strip_tags($continutLike));
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * @return int|null
     */
    public function getPersonajId()
    {
        return $this->personajId;
    }
    
    /**
     * @param int|null $personajId
     */
    public function setPersonajId($personajId)
    {
        $this->personajId = $personajId ? (int)$personajId : null;
    }
    
    /**
     * @return int|null
     */
    public function getUserId()
    {
        return $this->userId;
    }
    
    /**
     * @param int|null $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId ? (int)$userId : null;
    }
    
    // ---------------------------------------------------------------------------------------------

    /**
     * @return int
     */
    public function isPublished()
    {
        return $this->isPublished;
    }

    /**
     * @param int $isPublished
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished ? (int)$isPublished : null;
    }

    /**
     * @return bool|null
     */
    public function isDeleted()
    {
        return $this->isDeleted;
    }
    
    /**
     * @param bool|null $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = !is_null($isDeleted) ? (bool)$isDeleted : null;
    }

    /**
     * @return string|null
     */
    public function getSearchTerm()
    {
        return $this->searchTerm;
    }

    /**
     * @param string|null $val
     */
    public function setSearchTerm($val)
    {
        $this->searchTerm = is_null($val) ? null : trim(strip_tags($val));
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return array
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param array $orderBy
     */
    public function setOrderBy(array $orderBy)
    {
        $orderBy = (is_array($orderBy) && count($orderBy) ? $orderBy : []);
        if (count($orderBy)) {
            $orderItems = array_keys(self::fetchOrderItems());
            foreach ($orderBy as $k=>$v) {
                if (!in_array($v, $orderItems)) {
                    unset($orderBy[$k]);
                }
            }
        }
        $this->orderBy = $orderBy;
    }

    /**
     * @return array
     */
    public static function fetchOrderItems()
    {
        return array(
            self::ORDER_BY_ID_ASC => 'ID - ASC',
            self::ORDER_BY_ID_DESC => 'ID - DESC',
            self::ORDER_BY_TITLU_ASC => 'Titlu - ASC',
            self::ORDER_BY_TITLU_DESC => 'Titlu - DESC',
            self::ORDER_BY_PERSONAJ_ASC => 'Personaj - ASC',
            self::ORDER_BY_PERSONAJ_DESC => 'Personaj - DESC',
            self::ORDER_BY_DATA_INSERT_ASC => 'Data insert - ASC',
            self::ORDER_BY_DATA_INSERT_DESC => 'Data insert - DESC',
            self::ORDER_BY_DATA_PUBLICARE_ASC => 'Data publicare - ASC',
            self::ORDER_BY_DATA_PUBLICARE_DESC => 'Data publicare - DESC',
            self::ORDER_BY_ORDER_ASC => 'Order - ASC',
            self::ORDER_BY_ORDER_DESC => 'Order - DESC',
            self::ORDER_BY_RAND    => 'Order - RAND',
        );
    }

    /**
     * @return string[]
     */
    public static function fetchPublishedList()
    {
        return [
            self::PUBLISHED_YES   => 'Publicat',
            self::PUBLISHED_NO    => 'Nepublicat',
        ];
    }

}