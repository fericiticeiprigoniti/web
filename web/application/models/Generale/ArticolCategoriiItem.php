<?php

class ArticolCategoriiItem
{
    use ArrayOrJson;
    
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $parentId;

    /**
     * @var int
     */
    private $nivel;

    /**
     * @var string
     */
    private $nume;

    /**
     * @var string
     */
    private $alias;

    /**
     * @var int
     */
    private $orderId;

    /**
     * @var int
     */
    private $isDeleted;


    // ---------------------------------------------------------------------------------------------

    public function __construct(array $dbRow = [])
    {
        if ( !count($dbRow) ) {
            return;
        }

        $this->id = $dbRow['id'] ? (int)$dbRow['id'] : null;
        $this->parentId = $dbRow['parent_id'] ? (int)$dbRow['parent_id'] : null;
        $this->nivel = $dbRow['nivel'] ? (int)$dbRow['nivel'] : null;
        $this->nume = $dbRow['nume'] ? (string)$dbRow['nume'] : null;
        $this->alias = $dbRow['alias'] ? (string)$dbRow['alias'] : null;
        $this->orderId = $dbRow['order_id'] ? (int)$dbRow['order_id'] : null;
        $this->isDeleted = $dbRow['is_deleted'] ? (int)$dbRow['is_deleted'] : null;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param int $parentId
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId ? (int)$parentId : null;
    }

    /**
     * @return int
     */
    public function getNivel()
    {
        return $this->nivel;
    }

    /**
     * @param int $nivel
     */
    public function setNivel($nivel)
    {
        $this->nivel = $nivel ? (int)$nivel : null;
    }

    /**
     * @return string
     */
    public function getNume()
    {
        return $this->nume;
    }

    /**
     * @param string $nume
     */
    public function setNume($nume)
    {
        $this->nume = $nume ? (string)$nume : null;
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     */
    public function setAlias($alias)
    {
        $this->alias = $alias ? (string)$alias : null;
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId ? (int)$orderId : null;
    }

    /**
     * @return int
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param int $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted ? (int)$isDeleted : null;
    }


}
