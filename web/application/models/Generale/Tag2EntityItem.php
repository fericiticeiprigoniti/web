<?php

class Tag2EntityItem
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $tagId;

    /**
     * @var int
     */
    private $poezieId;

    /**
     * @var int
     */
    private $articolId;

    /**
     * @var int
     */
    private $carteId;


    // ---------------------------------------------------------------------------------------------

    public function __construct(array $dbRow = [])
    {
        if ( !count($dbRow) ) {
            return;
        }

        $this->id = $dbRow['id'] ? (int)$dbRow['id'] : null;
        $this->tagId = $dbRow['tag_id'] ? (int)$dbRow['tag_id'] : null;
        $this->poezieId = $dbRow['poezie_id'] ? (int)$dbRow['poezie_id'] : null;
        $this->articolId = $dbRow['articol_id'] ? (int)$dbRow['articol_id'] : null;
        $this->carteId = $dbRow['carte_id'] ? (int)$dbRow['carte_id'] : null;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return int
     */
    public function getTagId()
    {
        return $this->tagId;
    }

    /**
     * @param int $tagId
     */
    public function setTagId($tagId)
    {
        $this->tagId = $tagId ? (int)$tagId : null;
    }

    /**
     * @return int
     */
    public function getPoezieId()
    {
        return $this->poezieId;
    }

    /**
     * @param int $poezieId
     */
    public function setPoezieId($poezieId)
    {
        $this->poezieId = $poezieId ? (int)$poezieId : null;
    }

    /**
     * @return int
     */
    public function getArticolId()
    {
        return $this->articolId;
    }

    /**
     * @param int $articolId
     */
    public function setArticolId($articolId)
    {
        $this->articolId = $articolId ? (int)$articolId : null;
    }

    /**
     * @return int
     */
    public function getCarteId()
    {
        return $this->carteId;
    }

    /**
     * @param int $carteId
     */
    public function setCarteId($carteId)
    {
        $this->carteId = $carteId ? (int)$carteId : null;
    }


}
