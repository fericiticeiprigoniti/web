<?php

/**
 * Class ArticolTable
 * @table fcp_articole
 */
class ArticolTable extends CI_Model
{

    /**
     * @var ArticolTable
     */
    private static $instance;


    /**
     * Singleton
     * ArticolTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    // ---------------------------------------------------------------------------------------------

    /**
     *  we can later dependency inject and thus unit test this
     * @return ArticolTable
     */
    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param int $id
     * @throws Exception
     */
    public function load($id): ?ArticolItem
    {
        $id = (int)$id;
        if (!$id) {
            throw new Exception('ID articol este invalid');
        }

        $filters = new ArticolFilters(array('id'=>$id));
        $data = $this->fetchAll($filters);

        return $data['items'][0] ?? null;
    }

    /**
     * @param int $id
     */
    public static function get($id): ?ArticolItem
    {
        if (!$id) {
            return null;
        }

        try {
            return self::getInstance()->load($id);
        } catch (Throwable $e) {
            log_message('error', $e->getMessage());
            return null;
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchAll(ArticolFilters $filters, $limit = null, $offset = null)
    {
        // Set where and having conditions.
        $sqlWhere = [];
        $sqlHaving = [];

        if ($filters->getId()) {
            $sqlWhere[] = "articol.id = {$filters->getId()}";
        }
        if ($filters->getExcludedId()) {
            $sqlWhere[] = "articol.id <> {$filters->getExcludedId()}";
        }
        if ($filters->getCatId()) {
            $sqlWhere[] = "articol.cat_id = {$filters->getCatId()}";
        }
        if ($filters->getTitluLike()) {
            $sqlWhere[] = "articol.titlu LIKE " . $this->db->escape("%{$filters->getTitluLike()}%");
        }
        if ($filters->getContinutLike()) {
            $sqlWhere[] = "articol.continut LIKE " . $this->db->escape("%{$filters->getContinutLike()}%");
        }
        if ($filters->getPersonajId()) {
            $sqlWhere[] = "articol.personaj_id = {$filters->getPersonajId()}";
        }
        if ($filters->getUserId()) {
            $sqlWhere[] = "articol.user_id = {$filters->getUserId()}";
        }
        if (!is_null($filters->isPublished())) {
            if ($filters->isPublished()) {
                $sqlWhere[] = "articol.is_published > 0";
            } else {
                $sqlWhere[] = "(articol.is_published IS NULL OR articol.is_published = 0)";
            }
        }
        if (!is_null($filters->isDeleted())) {
            if ($filters->isDeleted()) {
                $sqlWhere[] = "articol.is_deleted > 0";
            } else {
                $sqlWhere[] = "(articol.is_deleted IS NULL OR articol.is_deleted = 0)";
            }
        }
        if ($filters->getSearchTerm()) {
            // Clean search term, extract words and set where condition.
            $searchTerm = Strings::sanitizeForDatabaseUsage($filters->getSearchTerm());
            $words = Strings::splitIntoWords($searchTerm);
            if (count($words)) {
                $subCond = array();
                foreach ($words as $word) {
                    $word = $this->db->escape("%$word%");
                    $subCond[] = "(articol.id LIKE $word OR articol.titlu LIKE $word)";
                }
                $sqlWhere[] = implode("\n\tAND ", $subCond);
            }
        }
        $sqlWhere = implode("\n\tAND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sqlHaving = implode("\n\tAND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? "HAVING " . $sqlHaving : '';

        // ------------------------------------

        // Set order by.
        $sqlOrder = array();
        if (count($filters->getOrderBy())) {
            foreach($filters->getOrderBy() as $ord) {
                switch($ord) {
                    case $filters::ORDER_BY_ID_ASC:
                        $sqlOrder[] = "id ASC";
                        break;
                    case $filters::ORDER_BY_ID_DESC:
                        $sqlOrder[] = "id DESC";
                        break;
                    case $filters::ORDER_BY_TITLU_ASC:
                        $sqlOrder[] = "titlu ASC";
                        break;
                    case $filters::ORDER_BY_TITLU_DESC:
                        $sqlOrder[] = "titlu DESC";
                        break;
                    case $filters::ORDER_BY_PERSONAJ_ASC:
                        $sqlOrder[] = "personaj_nume ASC";
                        break;
                    case $filters::ORDER_BY_PERSONAJ_DESC:
                        $sqlOrder[] = "personaj_nume DESC";
                        break;
                    case $filters::ORDER_BY_DATA_INSERT_ASC:
                        $sqlOrder[] = "data_insert ASC";
                        break;
                    case $filters::ORDER_BY_DATA_INSERT_DESC:
                        $sqlOrder[] = "data_insert DESC";
                        break;
                    case $filters::ORDER_BY_DATA_PUBLICARE_ASC:
                        $sqlOrder[] = "data_publicare ASC";
                        break;
                    case $filters::ORDER_BY_DATA_PUBLICARE_DESC:
                        $sqlOrder[] = "data_publicare DESC";
                        break;
                    case $filters::ORDER_BY_ORDER_ASC:
                        $sqlOrder[] = "order_id ASC";
                        break;
                    case $filters::ORDER_BY_ORDER_DESC:
                        $sqlOrder[] = "order_id DESC";
                        break;
                    case $filters::ORDER_BY_RAND:
                        $sqlOrder[] = "RAND()";
                        break;
                }
            }
        }
        if (!count($sqlOrder)) {
            $sqlOrder[] = "id ASC";
        }
        $sqlOrder = implode(", ", $sqlOrder);

        // ------------------------------------

        // Set limit.
        $sqlLimit = '';
        if (!is_null($offset) && !is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$offset . ', ' . (int)$limit;
        } elseif (!is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$limit;
        }

        // ------------------------------------

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    articol.*
    , CONCAT_WS(' ', pers.nume, pers.prenume) AS personaj_nume
FROM
    fcp_articole AS articol
    LEFT JOIN fcp_personaje AS pers ON articol.personaj_id = pers.id
WHERE
    1
    $sqlWhere
GROUP BY articol.id
$sqlHaving
ORDER BY $sqlOrder
$sqlLimit
EOSQL;

        /** @var CI_DB_result $result */
        $result = $this->db->query($sql);
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        $items = array();
        foreach($result->result('array') as $row){
            $items[] = new ArticolItem($row);
        }

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param ArticolItem $item
     * @return bool
     */
    public function save(ArticolItem &$item)
    {
        // Set data.
        $eData = [
            'cat_id'            => $item->getCatId() ?: null,
            'titlu'             => $item->getTitlu() ?: null,
            'continut'          => $item->getContinut() ?: null,
            'autor_id'          => $item->getAutorId() ?: null,
            'personaj_id'       => $item->getPersonajId() ?: null,
            'sursa_link'        => $item->getSursaLink() ?: null,
            'sursa_titlu'       => $item->getSursaTitlu() ?: null,
            'sursa_doc_carte_id'    => $item->getSursaDocCarteId() ?: null,
            'sursa_doc_publicatie_id' => $item->getSursaDocPublicatieId() ?: null,
            'sursa_doc_start_page'  => $item->getSursaDocStartPage() ?: null,
            'sursa_doc_end_page'    => $item->getSursaDocEndPage() ?: null,
            'sursa_culegator'       => $item->getSursaCulegator() ?: null,
            'data_insert'        => $item->getDataInsert() ? $item->getDataInsert()->format('Y-m-d H:i:s') : Calendar::sqlDateTime(),
            'data_publicare'     => $item->getDataPublicare() ? $item->getDataPublicare()->format('Y-m-d') : null,
            'seo_metadescription'=> $item->getSeoMetadescription() ?: null,
            'seo_metakeywords'   => $item->getSeoMetakeywords() ?: null,
            'order_id'          => $item->getOrderId() ?: null,
            'hits'              => $item->getHits() ?: null,
            'user_id'           => $item->getUserId() ?: $this->aauth->get_user_id(),
            'is_published'      => $item->isPublished() ?: 0,
            'is_deleted'        => $item->isDeleted() ?: 0
        ];

        // Insert/Update.
        $id = null;
        if (!$item->getId()) {
            $this->db->insert('fcp_articole', $eData);
            $id  = $this->db->insert_id();
            $item->setId($id);
        } else {
            $this->db->update('fcp_articole', $eData, 'id = ' . $item->getId());
        }

        # ========================================================================================
        # ADD or Update cuvinte cheie
        # from fcp_taguri
        #
        // clean all tags
        $this->db->delete('fcp_taguri2entitati', ['articol_id' => (int)$item->getId()]);
        if( $arrTagKeys = $item->getArrTagKeys() )
        {
            foreach($arrTagKeys as $tag)
            {
                $tag2entity = new Tag2EntityItem();
                $tag2entity->setTagId((int)$tag);
                $tag2entity->setArticolId((int)$item->getId());

                Tag2EntityTable::getInstance()->save($tag2entity);
            }
        }

        return $item->getId() ?: $id;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param ArticolItem $item
     * @return mixed
     */
    public function delete(ArticolItem $item)
    {
        return $this->db->delete('fcp_articole', 'id = ' . $item->getId());
    }

    // ---------------------------------------------------------------------------------------------
    
    /**
     * @param string $searchTerm
     * @param int $limit
     * @return ArticolItem[]
     */
    public function fetchForAutocomplete($searchTerm, $limit=LIMIT_FOR_AUTOCOMPLETE)
    {
        $filters = new ArticolFilters(['searchTerm'=>$searchTerm]);
        $data = $this->fetchAll($filters, $limit);

        return $data['items'] ?? [];
    }
    
    // ---------------------------------------------------------------------------------------------

    /**
     * @param ArticolFilters|null $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchForApi(ArticolFilters $filters, $limit = null, $offset = null, &$tCount)
    {
        $data = $this->fetchAll($filters, $limit, $offset);

        if (!isset($data['items']) || !count($data['items'])) {
            return [];
        }

        $return = [];
        /** @var ArticolItem $item */
        foreach($data['items'] as $item) {
            $result = $item->toArray();
            if ($autorInfo = $item->getAutor())
                $result['autorInfo'] = $autorInfo->toArray();
            if ($personajInfo = $item->getPersonaj())
                $result['personajInfo'] = $personajInfo->toArray();
            if ($carteInfo = $item->getCarte())
                $result['carteInfo'] = $carteInfo->toArray();
            if ($userInfo = $item->getUser())
                $result['userInfo'] = $userInfo->toArray();

            $return[] = $result;
        }

        $tCount = $data['count'];

        return $return;
    }
    
}
