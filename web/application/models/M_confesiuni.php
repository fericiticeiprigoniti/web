<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_confesiuni extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Returneaza lista de confesiuni
     * @param bool $keyValue
     * @return array
     */
    public function fetchAll($keyValue = false)
    {
        $sql = "SELECT * FROM fcp_confesiuni";
        if ($keyValue) {
            $result = array();
            $query = $this->db->query($sql);
            foreach ($query->result() as $row) {
                $result[$row->id] = $row->nume;
            }
        } else {
            $result = $this->db->query($sql)->result_array();
        }
        return $result;
    }
}