<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_origini_sociale extends CI_Model {

    const TABLE = 'fcp_personaje_origini_sociale';

    function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Returneaza lista cu originile sociale
     * @param bool $keyValue
     * @return array
     */
    public function fetchAll($keyValue = false)
    {
        $sql = "SELECT * FROM fcp_personaje_origini_sociale ORDER BY nume";
        if ($keyValue) {
            $result = array();
            $query = $this->db->query($sql);
            foreach ($query->result() as $row) {
                $result[$row->id] = $row->nume;
            }
        } else {
            $result = $this->db->query($sql)->result_array();
        }
        return $result;
    }

    /**
     * Fetch list
     * @param array $filters
     * @return array
     */
    public function fetchList($filters=array())
    {
        $sqlWhere = array();
        if (isset($filters['id']) && !empty($filters['id'])) {
            $sqlWhere[] = "orig.id = " . (int)$filters['id'];
        }
        if (isset($filters['exclude_id']) && !empty($filters['exclude_id'])) {
            $sqlWhere[] = "orig.id <> " . (int)$filters['exclude_id'];
        }
        if (isset($filters['nume']) && !empty($filters['nume'])) {
            $sqlWhere[] = "orig.nume LIKE " . $this->db->escape($filters['nume']);
        }
        $sqlWhere = count($sqlWhere) ? implode("\n\tAND ", $sqlWhere) : '';
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sql = <<<EOSQL
SELECT
    orig.*
    , COUNT(pers.id) AS is_used
FROM
    fcp_personaje_origini_sociale AS orig
    LEFT JOIN fcp_personaje AS pers ON orig.id = pers.orig_sociala_id
WHERE
    1
    $sqlWhere
GROUP BY
    orig.id
ORDER BY
    nume;
EOSQL;

        return $this->db->query($sql)->result_array();
    }

    /**
     * Load element by id
     * @param int $id
     * @return array
     */
    public function load($id)
    {
        $data = $this->fetchList(array('id'=>$id));
        return isset($data[0]) ? $data[0] : array();
    }

    /**
     * @param string $name
     * @param int|null $excludedId
     * @return array
     */
    public function fetchByName($name, $excludedId = null)
    {
        return $this->fetchList(array('nume'=>$name, 'exclude_id'=>$excludedId));
    }

    public function save($data, $id=null)
    {
        $id = (int)$id;
        $eData = array(
            'nume' => $data['nume'],
            'detalii' => !empty($data['detalii']) ? $data['detalii'] : null,
        );

        if (!$id) {
            $res = $this->db->insert(self::TABLE, $eData);
        } else {
            $res = $this->db->update(self::TABLE, $eData, array('id'=>$id));
        }
        return $res;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->db->delete(self::TABLE, array('id'=>(int)$id));
    }


}
