<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_locuri_patimire extends CI_Model
{

    const TABLE = 'fcp_locuri_patimire';

    function __construct()
    {
        parent::__construct();
    }

    public function load($id)
    {
        $result = $this->fetchAll(array('id' => $id));
        return isset($result['items'][0]) ? $result['items'][0] : null;
    }

    /**
     * Returneaza lista cu locuri patimire
     *
     * @param bool $keyValue
     * @param array $filters
     * @param array $order
     * @param array $limit
     * @return array
     */
    public function fetchAll($filters = array(), $order = array(), $limit = array())
    {

        // Set filters.
        $sqlWhere = array();
        $sqlHaving = array();
        if (!empty($filters)) {
            foreach ($filters as $k => $v) {
                if ($v == '') {        // Preliminary check.
                    continue;
                }
                switch ($k) {
                    case 'id':
                        $sqlWhere[] = "loc.id = " . $this->db->escape($v);
                        break;
                }
            }
        }
        $sqlWhere = implode(" AND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? ' AND ' . $sqlWhere : '';

        $sqlHaving = implode(" AND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? ' HAVING ' . $sqlHaving : '';

        // -------------------------------

        // Set order.
        $sqlOrder = "loc.locatie_nume, tip.descriere";
        $defOrderType = "ASC";
        $orderItems = array(
            'id',
            'titlu',
            'nume_autor',
            'locatie_nume',
            'destinatie',
            'tip_loc_surghiun',
            'localitate_nume',
            'judet_nume'
        );
        if (isset($order['by']) && !empty($order['by']) && in_array($order['by'], $orderItems)) {
            $sqlOrder = $order['by'];
        }
        if (isset($order['dir']) && !empty($order['dir'])) {
            $sqlOrder .= ' ' . (in_array(strtoupper($order['dir']), array('ASC', 'DESC')) ? $order['dir'] : $defOrderType);
        } else {
            $sqlOrder .= ' ' . $defOrderType;
        }
        $sqlOrder = !empty($sqlOrder) ? "ORDER BY\n\t" . $sqlOrder : '';

        // -------------------------------

        // Set limit.
        if (isset($limit['start']) && isset($limit['per_page'])) {
            $start = (int)$limit['start'];
            $per_page = (int)$limit['per_page'];
            $sqlLimit = "LIMIT $start, $per_page";
        } elseif (isset($limit['start'])) {
            $start = (int)$limit['start'];
            $sqlLimit = "LIMIT $start";
        } else {
            $sqlLimit = '';
        }

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    loc.*
    , tip.descriere AS tip_loc_surghiun
    , tip.acronim AS tip_loc_surghiun_acronim
    , localitate.nume AS localitate_nume
    , judet.nume AS judet_nume
FROM
    fcp_locuri_patimire AS loc
    LEFT JOIN fcp_locuri_patimire_tip AS tip ON loc.tip_surghiun_id = tip.id
    LEFT JOIN nom_localitati localitate on loc.localitate_id = localitate.id
    LEFT JOIN nom_judete judet on localitate.id_judet = judet.id
WHERE
    1
    {$sqlWhere}
    {$sqlOrder}
    {$sqlLimit}
EOSQL;
        $items = $this->db->query($sql)->result_array();
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    /**
     * pregatim pentru selector lista cu toate locurile (mai putin cele care sunt sterse)
     *
     * @return void
     */
    public function fetchForSelect()
    {
        $items = $this->fetchAll();
        $items = $items['items'];
        foreach ($items as $row) {
            if ($row['is_deleted']) continue;

            $result[$row['id']] = $row['locatie_nume'] . ' (' . $row['tip_loc_surghiun_acronim'] . ')';
        }
        return $result;
    }

    public function save($data, $id = null)
    {
        $id = (int)$id;
        $eData = array(
            'tip_surghiun_id'       => (int)$data['tip_surghiun_id'],
            'locatie_nume'          => (string)$data['locatie_nume'],
            'locatie_supranume'     => (string)$data['locatie_supranume'],
            'locatie_cod_unitate'   => (string)$data['locatie_cod_unitate'],
            'destinatie'            => (string)$data['destinatie'],
            'localitate_id'         => (string)$data['localitate_id'],
            'locatie_coord_lat'     => !empty($data['locatie_coord_lat']) ? (float)$data['locatie_coord_lat'] : null,
            'locatie_coord_long'    => !empty($data['locatie_coord_long']) ? (float)$data['locatie_coord_long'] : null,
            'descriere_art_id'      => !empty($data['descriere_art_id']) ? (int)$data['descriere_art_id'] : null,
            'is_deleted'            => isset($data['is_deleted']) ? (int)$data['is_deleted'] : 0,
        );

        if (!$id) {
            $this->db->insert(self::TABLE, $eData);
            $id  = $this->db->insert_id();
        } else {
            $this->db->update(self::TABLE, $eData, array('id' => $id));
        }

        return $id;
    }

    public function delete($id)
    {
        return $this->db->update(self::TABLE, ['is_deleted' => 1], ['id' => (int)$id]);
    }

    /**
     * @return ArticolItem|null
     * @throws Exception
     */
    public function getDescriere($descriereArtId)
    {
        if (!$descriereArtId) {
            return null;
        }
        return ArticolTable::getInstance()->load($descriereArtId);
    }
}
