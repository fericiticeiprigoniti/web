<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_personaje_carti extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    /**
    * GET carti by Personaj ID
    * @param mixed $persID
    * @return array
    */
    public function fetchAll($persID)
    {
        $persID = (int)$persID;

        $sql = "SELECT p2c.id as p2c_id, p2c.carte_tip as carte_tip, carte.id as carte_id, carte.titlu as carte_titlu, carte.*, editura.nume as editura, GROUP_CONCAT(autor.nume SEPARATOR ', ') as autori
                FROM fcp_personaje2carti as p2c
                LEFT JOIN fcp_biblioteca_carti as carte ON carte.id = p2c.carte_id
                LEFT JOIN fcp_biblioteca_edituri as editura ON editura.id = carte.editura_id
                LEFT JOIN fcp_biblioteca_autori2carti as a2c ON a2c.carte_id = carte.id
                LEFT JOIN fcp_autori as autor ON autor.id = a2c.autor_id
                WHERE p2c.personaj_id = ?
                GROUP BY p2c_id ";

        return $this->db->query($sql, array($persID))->result_array();
    }

    public function add($persID, $fields)
    {
        $sql = "INSERT INTO fcp_personaje2carti (carte_id, personaj_id, carte_tip) VALUES (?,?,?) ON DUPLICATE KEY UPDATE carte_tip = ?";
        return $this->db->query($sql, array($fields['carte_id'], $fields['personaj_id'], $fields['carte_tip'], $fields['carte_tip']));
    }

    public function update($persID, $itemID, $fields)
    {
        return $this->db->update('fcp_personaje2locuripatimire', $fields, array('personaj_id' => (int)$persID, 'id' => (int)$itemID));
    }

    /**
    * delete carte atribuita - Tab Carti
    *
    * @param mixed $persID
    * @param mixed $itemID
    */
    public function delete($persID, $itemID)
    {
        return $this->db->delete('fcp_personaje2carti', array('personaj_id' => (int)$persID,
                                                              'id'          => (int)$itemID));
    }
}