<?php

/**
 * Class SpeciePoezieTable
 * @table fcp_poezii_specii
 */
class SpeciePoezieTable extends CI_Model
{

    /**
     * @var SpeciePoezieTable
     */
    private static $instance;


    /**
     * Singleton
     * SpeciePoezieTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * We can later dependency inject and thus unit test this
     * @return SpeciePoezieTable
     */
    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param mixed $id
     * @throws Exception
     */
    public function load($id): ?SpeciePoezieItem
    {
        if (!$id) {
            throw new Exception("ID invalid pt modelul SpeciePoezie");
        }

        $filters = new SpeciePoezieFilters(['id' => $id]);
        $data = $this->fetchAll($filters);

        return $data['items'][0] ?? null;
    }

    /**
     * @param mixed $id
     */
    public static function get($id): ?SpeciePoezieItem
    {
        if (empty($id)) {
            return null;
        }

        try {
            return self::getInstance()->load($id);
        } catch (Throwable $e) {
            log_message('error', $e->getMessage());
            return null;
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param SpeciePoezieFilters $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchAll(SpeciePoezieFilters $filters = null, $limit = null, $offset = null)
    {
        // Set where and having conditions.
        $sqlWhere = array();
        $sqlHaving = array();
        if ($filters) {
            if ($filters->getId()) {
                $sqlWhere[] = "id = {$filters->getId()}";
            }
            if ($filters->getExcludedId()) {
                $sqlWhere[] = "id <> {$filters->getExcludedId()}";
            }
            if ($filters->getGenLiterarId()) {
                $sqlWhere[] = "gen_literar_id = {$filters->getGenLiterarId()}";
            }
        }

        $sqlWhere = implode("\n\tAND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sqlHaving = implode("\n\tAND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? "HAVING " . $sqlHaving : '';

        // ------------------------------------

        $sqlOrder = array();
        if ($filters && count($filters->getOrderBy())) {
            foreach($filters->getOrderBy() as $ord) {
                switch($ord) {
                    case $filters::ORDER_BY_ID_ASC:
                        $sqlOrder[] = "id ASC";
                        break;
                    case $filters::ORDER_BY_ID_DESC:
                        $sqlOrder[] = "id DESC";
                        break;
                    case $filters::ORDER_BY_NAME_ASC:
                        $sqlOrder[] = "nume ASC";
                        break;
                    case $filters::ORDER_BY_NAME_DESC:
                        $sqlOrder[] = "nume DESC";
                        break;
                    case $filters::ORDER_BY_ORDER_ASC:
                        $sqlOrder[] = "order ASC";
                        break;
                    case $filters::ORDER_BY_ORDER_DESC:
                        $sqlOrder[] = "order DESC";
                        break;
        }
            }
        }
        if (!count($sqlOrder)) {
            $sqlOrder[] = "id DESC";
        }
        $sqlOrder = implode(", ", $sqlOrder);

        // ------------------------------------

        // Set limit.
        $sqlLimit = '';
        if (!is_null($offset) && !is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$offset . ', ' . (int)$limit;
        } elseif (!is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$limit;
        }

        // ------------------------------------

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    *
FROM
    fcp_poezii_specii
WHERE
    1
    $sqlWhere
GROUP BY id
$sqlHaving
ORDER BY $sqlOrder
$sqlLimit
EOSQL;

        /** @var CI_DB_result $result */
        $result = $this->db->query($sql);
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        $items = array();
        foreach($result->result('array') as $row){
            $items[] = new SpeciePoezieItem($row);
        }

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param SpeciePoezieItem $item
     * @return bool|int
     */
    public function save(SpeciePoezieItem $item)
    {
        // Set data.
        $eData = array(
            'nume'           => $item->getNume() ? $item->getNume() : null,
            'alias'          => $item->getAlias() ? $item->getAlias() : null,
            'gen_literar_id' => $item->getGenLiterarId() ? $item->getGenLiterarId() : null,
            'order'          => $item->getOrder() ? $item->getOrder() : null,
        );

        // Insert/Update.
        if (!$item->getId()) {
            $res = $this->db->insert('fcp_poezii_specii', $eData);
        } else {
            $res = $this->db->update('fcp_poezii_specii', $eData, 'id = ' . $item->getId());
        }

        return $res;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param SpeciePoezieItem $item
     * @return mixed
     */
    public function delete(SpeciePoezieItem $item)
    {
        return $this->db->delete('fcp_poezii_specii', 'id = ' . $item->getId());
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param SpeciePoezieFilters|null $filters
     * @return array key -> value
     */
    public function fetchForSelect(SpeciePoezieFilters $filters = null)
    {
        if (!$filters) {
            $filters = new SpeciePoezieFilters();
        }

        $result = $this->fetchAll($filters);

        $data = [];
        if (isset($result['items']) && count($result['items'])) {
            /** @var SpeciePoezieItem $objItem */
            foreach($result['items'] as $objItem) {
                $data[$objItem->getId()] = $objItem->getNume();
            }
        }

        return $data;
    }

}
