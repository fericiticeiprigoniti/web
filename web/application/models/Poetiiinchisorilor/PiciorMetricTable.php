<?php

/**
 * Class PiciorMetricTable
 * @table fcp_poezii_picioaremetrice
 */
class PiciorMetricTable extends CI_Model
{

    /**
     * @var PiciorMetricTable
     */
    private static $instance;


    /**
     * Singleton
     * PiciorMetricTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * We can later dependency inject and thus unit test this
     * @return PiciorMetricTable
     */
    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param mixed $id
     * @return PiciorMetricItem|null
     * @throws Exception
     */
    public function load($id)
    {
        if (!$id) {
            throw new Exception("Invalid PiciorMetric id");
        }
        $filters = new PiciorMetricFilters(array('id'=>$id));
        $data = $this->fetchAll($filters);

        return isset($data['items'][0]) ? $data['items'][0] : null;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param PiciorMetricFilters|null $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchAll(PiciorMetricFilters $filters = null, $limit = null, $offset = null)
    {
        // Set where and having conditions.
        $sqlWhere = array();
        $sqlHaving = array();
        if ($filters) {
            if ($filters->getId()) {
                $sqlWhere[] = "id = {$filters->getId()}";
            }
            if ($filters->getExcludedId()) {
                $sqlWhere[] = "id <> {$filters->getExcludedId()}";
            }
        }

        $sqlWhere = implode("\n\tAND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sqlHaving = implode("\n\tAND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? "HAVING " . $sqlHaving : '';

        // ------------------------------------

        $sqlOrder = array();
        if ($filters && count($filters->getOrderBy())) {
            foreach($filters->getOrderBy() as $ord) {
                switch($ord) {
                    case $filters::ORDER_BY_ID_ASC:
                        $sqlOrder[] = "id ASC";
                        break;
                    case $filters::ORDER_BY_ID_DESC:
                        $sqlOrder[] = "id DESC";
                        break;
                    case $filters::ORDER_BY_NAME_ASC:
                        $sqlOrder[] = "nume ASC";
                        break;
                    case $filters::ORDER_BY_NAME_DESC:
                        $sqlOrder[] = "nume DESC";
                        break;
        }
            }
        }
        if (!count($sqlOrder)) {
            $sqlOrder[] = "id DESC";
        }
        $sqlOrder = implode(", ", $sqlOrder);

        // ------------------------------------

        // Set limit.
        $sqlLimit = '';
        if (!is_null($offset) && !is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$offset . ', ' . (int)$limit;
        } elseif (!is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$limit;
        }

        // ------------------------------------

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    *
FROM
    fcp_poezii_picioaremetrice
WHERE
    1
    $sqlWhere
GROUP BY id
$sqlHaving
ORDER BY $sqlOrder
$sqlLimit
EOSQL;

        /** @var CI_DB_result $result */
        $result = $this->db->query($sql);
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        $items = array();
        foreach($result->result('array') as $row){
            $items[] = new PiciorMetricItem($row);
        }

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param PiciorMetricItem $item
     * @return bool|int
     */
    public function save(PiciorMetricItem $item)
    {
        // Set data.
        $eData = array(
            'nume' => $item->getNume() ? $item->getNume() : null,
            'alias' => $item->getAlias() ? $item->getAlias() : null,
            'order' => $item->getOrder() ? $item->getOrder() : null,
        );

        // Insert/Update.
        if (!$item->getId()) {
            $res = $this->db->insert('fcp_poezii_picioaremetrice', $eData);
        } else {
            $res = $this->db->update('fcp_poezii_picioaremetrice', $eData, 'id = ' . $item->getId());
        }

        return $res;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param PiciorMetricItem $item
     * @return mixed
     */
    public function delete(PiciorMetricItem $item)
    {
        return $this->db->delete('fcp_poezii_picioaremetrice', 'id = ' . $item->getId());
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param PiciorMetricFilters|null $filters
     * @return array key -> value
     */
    public function fetchForSelect(PiciorMetricFilters $filters = null)
    {
        if (!$filters) {
            $filters = new PiciorMetricFilters();
        }

        $result = $this->fetchAll($filters);

        $data = [];
        if (isset($result['items']) && count($result['items'])) {
            /** @var PiciorMetricItem $objItem */
            foreach($result['items'] as $objItem) {
                $data[$objItem->getId()] = $objItem->getNume();
            }
        }

        return $data;
    }

}
