<?php

class PoezieCicluriItem
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $poetId;


    // ---------------------------------------------------------------------------------------------

    public function __construct(array $dbRow = [])
    {
        if ( !count($dbRow) ) {
            return;
        }

        $this->id     = $dbRow['id'] ? (int)$dbRow['id'] : null;
        $this->name   = $dbRow['name'] ? (string)$dbRow['name'] : null;
        $this->poetId = $dbRow['poet_id'] ? (int)$dbRow['poet_id'] : null;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name ? (string)$name : null;
    }

    /**
     * @return int
     */
    public function getPoetId()
    {
        return $this->poetId;
    }

    /**
     * @param int $poetId
     */
    public function setPoetId($poetId)
    {
        $this->poetId = $poetId ? (int)$poetId : null;
    }


}
