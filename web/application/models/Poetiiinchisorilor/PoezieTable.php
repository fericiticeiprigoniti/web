<?php

use controllers\api\adapters\CarteAdapter;
use controllers\api\adapters\PersonajAdapter;

/**
 * Class PoezieTable
 * @table fcp_poezii
 */
class PoezieTable extends CI_Model
{

    /**
     * @var PoezieTable
     */
    private static $instance;


    /**
     * Singleton
     * PoezieTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * We can later dependency inject and thus unit test this
     * @return PoezieTable
     */
    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param mixed $id
     * @return PoezieItem|null
     * @throws Exception
     */
    public function load($id)
    {
        if (!$id) {
            throw new Exception("Invalid Poezie id");
        }
        $filters = new PoezieFilters(array('id'=>$id));
        $data = $this->fetchAll($filters);

        return isset($data['items']) && isset($data['items'][0]) ? $data['items'][0] : null;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param PoezieFilters|null $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchAll(PoezieFilters $filters = null, $limit = null, $offset = null)
    {
        // Set where and having conditions.
        $sqlWhere = [];
        $sqlHaving = [];

        if ($filters->getId()) {
            $sqlWhere[] = "poezie.id = {$filters->getId()}";
        }
        if ($filters->getExcludedId()) {
            $sqlWhere[] = "poezie.id <> {$filters->getExcludedId()}";
        }
        if ($filters->getTitleLike()) {
            $sqlWhere[] = "poezie.titlu LIKE " . $this->db->escape("%{$filters->getTitleLike()}%");
        }
        if ($filters->getPersonajId()) {
            $sqlWhere[] = "poezie.personaj_id = {$filters->getPersonajId()}";
        }
        if ($filters->getContentLike()) {
            $sqlWhere[] = "poezie.continut LIKE " . $this->db->escape("%{$filters->getContentLike()}%");
        }
        if ($filters->getComentariuArticolId()) {
            $sqlWhere[] = "poezie.comentariu_articol_id = {$filters->getComentariuArticolId()}";
        }
        if ($filters->getComentariuUserId()) {
            $sqlWhere[] = "poezie.comentariu_user_id = {$filters->getComentariuUserId()}";
        }
        if ($filters->getSursaDocCarteId()) {
            $sqlWhere[] = "poezie.sursa_doc_carte_id = {$filters->getSursaDocCarteId()}";
        }
        if ($filters->getSursaDocPublicatieId()) {
            $sqlWhere[] = "poezie.sursa_doc_publicatie_id = {$filters->getSursaDocPublicatieId()}";
        }
        if ($filters->getSubjectId()) {
            $sqlWhere[] = "poezie.subiect_id = {$filters->getSubjectId()}";
        }
        if ($filters->getPerioadaCreatieiId()) {
            $sqlWhere[] = "poezie.perioada_creatiei_id = {$filters->getPerioadaCreatieiId()}";
        }
        if ($filters->getCreationPeriodLike()) {
            $sqlWhere[] = "poezie.data_creatiei LIKE " . $this->db->escape("%{$filters->getCreationPeriodLike()}%");
        }
        if ($filters->getCreationPlaceLike()) {
            $sqlWhere[] = "poezie.locul_creatiei LIKE " . $this->db->escape("%{$filters->getCreationPlaceLike()}%");
        }
        if ($filters->getSpecieId()) {
            $sqlWhere[] = "poezie.specie_id = {$filters->getSpecieId()}";
        }
        if ($filters->getStructuraStrofaId()) {
            $sqlWhere[] = "poezie.structura_strofa_id = {$filters->getStructuraStrofaId()}";
        }
        if ($filters->getRimaId()) {
            $sqlWhere[] = "poezie.rima_id = {$filters->getRimaId()}";
        }
        if ($filters->getPiciorMetricId()) {
            $sqlWhere[] = "poezie.picior_metric_id = {$filters->getPiciorMetricId()}";
        }
        if (!is_null($filters->getParentId())) {
            $sqlWhere[] = "poezie.parent_id = {$filters->getParentId()}";
        }
        if ($filters->getUserId()) {
            $sqlWhere[] = "poezie.user_id = {$filters->getUserId()}";
        }
        if ($filters->getTagIdList() && count($filters->getTagIdList())) {
            $poetryIdList = Tag2EntityTable::getInstance()->fetchPoetryIdListByTagIdList($filters->getTagIdList());
            $poetryList = count($poetryIdList) ? implode(',', $poetryIdList) : 'NULL';
            $sqlWhere[] = "poezie.id IN ({$poetryList})";
        }

        if (!is_null($filters->isPublished())) {
            if ($filters->isPublished() == $filters::PUBLISHED_YES) {
                $sqlWhere[] = "poezie.is_published > 0";
            } elseif ($filters->isPublished() == $filters::PUBLISHED_NO) {
                $sqlWhere[] = "(poezie.is_published IS NULL OR poezie.is_published = 0)";
            }
        }
        if (!is_null($filters->isDeleted())) {
            if ($filters->isDeleted() == $filters::DELETED_YES) {
                $sqlWhere[] = "poezie.is_deleted > 0";
            } elseif ($filters->isDeleted() == $filters::DELETED_NO) {
                $sqlWhere[] = "(poezie.is_deleted IS NULL OR poezie.is_deleted = 0)";
            }
        }

        if ($filters->getLyricsNo()) {
            $col = 'poezie.nr_strofe';
            $lyricsNoMapping = [
                $filters::LYRICS_NO_1_5     => "$col >= 1 AND $col <= 5",
                $filters::LYRICS_NO_6_10    => "$col >= 6 AND $col <= 10",
                $filters::LYRICS_NO_11_15   => "$col >= 11 AND $col <= 15",
                $filters::LYRICS_NO_16_20   => "$col >= 16 AND $col <= 20",
                $filters::LYRICS_NO_21_30   => "$col >= 21 AND $col <= 30",
                $filters::LYRICS_NO_31_50   => "$col >= 31 AND $col <= 50",
                $filters::LYRICS_NO_51_100  => "$col >= 51 AND $col <= 100",
                $filters::LYRICS_NO_100plus => "$col > 100",
            ];

            $sqlWhere[] = $lyricsNoMapping[$filters->getLyricsNo()] ?? null;
        }

        if ($filters->getAlternativesNo()) {
            $col = 'nr_variante';
            $alternativesNoMapping = [
                $filters::ALTERNATIVES_NO_1_2   => "$col >= 1 AND $col <= 2",
                $filters::ALTERNATIVES_NO_3_5   => "$col >= 3 AND $col <= 5",
                $filters::ALTERNATIVES_NO_6_10  => "$col >= 6 AND $col <= 10",
                $filters::ALTERNATIVES_NO_11_20 => "$col >= 11 AND $col <= 20",
                $filters::ALTERNATIVES_NO_21_30 => "$col >= 21 AND $col <= 30",
                $filters::ALTERNATIVES_NO_30plus => "$col > 30",
            ];

            $sqlHaving[] = $alternativesNoMapping[$filters->getAlternativesNo()] ?? null;
        }

        $sqlWhere = implode("\n\tAND ", array_filter($sqlWhere));
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sqlHaving = implode("\n\tAND ", array_filter($sqlHaving));
        $sqlHaving = !empty($sqlHaving) ? "HAVING " . $sqlHaving : '';

        // ------------------------------------

        $sqlOrder = [];
        $orderMapping = [
            $filters::ORDER_BY_ID_ASC           => "id ASC",
            $filters::ORDER_BY_ID_DESC          => "id DESC",
            $filters::ORDER_BY_ORDER_ID_ASC     => "order_id ASC",
            $filters::ORDER_BY_ORDER_ID_DESC    => "order_id DESC",
            $filters::ORDER_BY_RAND             => "RAND()",
        ];
        if (count($filters->getOrderBy())) {
            foreach($filters->getOrderBy() as $ord) {
                $sqlOrder[] = $orderMapping[$ord] ?? null;
            }
        }

        $sqlOrder = array_filter($sqlOrder);
        if (!count($sqlOrder)) {
            $sqlOrder[] = "id DESC";
        }
        $sqlOrder = implode(", ", $sqlOrder);

        // ------------------------------------

        // Set limit.
        $sqlLimit = '';
        if (!is_null($offset) && !is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$offset . ', ' . (int)$limit;
        } elseif (!is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$limit;
        }

        // ------------------------------------

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    poezie.*
    , COUNT(variante.id) AS nr_variante
FROM
    fcp_poezii AS poezie
    LEFT JOIN fcp_poezii AS variante ON poezie.id = variante.parent_id
WHERE
    1
    $sqlWhere
GROUP BY poezie.id
$sqlHaving
ORDER BY $sqlOrder
$sqlLimit
EOSQL;

        /** @var CI_DB_result $result */
        $result = $this->db->query($sql);
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        $items = array();
        foreach($result->result('array') as $row){
            $items[] = new PoezieItem($row);
        }

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param PoezieItem $item
     * @return PoezieItem
     * @throws Exception
     */
    public function save(PoezieItem &$item)
    {
        $this->db->trans_begin();

        // Set data.
        $eData = array(
            'parent_id'             => $item->getParentId() ? $item->getParentId() : 0,
            'titlu'                 => $item->getTitlu() ? $item->getTitlu() : null,
            'titlu_varianta'        => $item->getTitluVarianta() ? $item->getTitluVarianta() : null,
            'personaj_id'           => $item->getPersonajId() ? $item->getPersonajId() : null,
            'continut_scurt'        => $item->getContinutScurt() ?: null,
            'continut'              => $item->getContinut() ? $item->getContinut() : null,
            'sursa_link'            => $item->getSursaLink() ? $item->getSursaLink() : null,
            'sursa_titlu'           => $item->getSursaTitlu() ? $item->getSursaTitlu() : null,
            'sursa_doc_carte_id'    => $item->getSursaDocCarteId() ? $item->getSursaDocCarteId() : null,
            'sursa_doc_publicatie_id' => $item->getSursaDocPublicatieId() ? $item->getSursaDocPublicatieId() : null,
            'sursa_doc_start_page'  => $item->getSursaDocStartPage() ? $item->getSursaDocStartPage() : null,
            'sursa_doc_end_page'    => $item->getSursaDocEndPage() ? $item->getSursaDocEndPage() : null,
            'sursa_culegator'       => $item->getSursaCulegator() ? $item->getSursaCulegator() : null,
            'ciclu_id'              => $item->getCicluId() ? $item->getCicluId() : null,
            'subiect_id'            => $item->getSubjectId() ? $item->getSubjectId() : null,
            'perioada_creatiei_id'  => $item->getPerioadaCreatieiId() ? $item->getPerioadaCreatieiId() : null,
            'data_creatiei'         => $item->getDataCreatiei() ? $item->getDataCreatiei() : null,
            'locul_creatiei'        => $item->getLoculCreatiei() ? $item->getLoculCreatiei() : null,
            'loc_detentie_creatie_id' => $item->getLocDetentieCreatieId() ? $item->getLocDetentieCreatieId() : null,
            'specie_id'             => $item->getSpecieId() ? $item->getSpecieId() : null,
            'structura_strofa_id'   => $item->getStructuraStrofaId() ? $item->getStructuraStrofaId() : null,
            'rima_id'               => $item->getRimaId() ? $item->getRimaId() : null,
            'picior_metric_id'      => $item->getPiciorMetricId() ? $item->getPiciorMetricId() : null,
            'nr_strofe'             => $item->getNrStrofe() ? $item->getNrStrofe() : null,
            'data_insert'           => $item->getDataInsert() ? $item->getDataInsert() : Calendar::sqlDateTime(),
            'data_publicare'        => $item->getDataPublicare() ?: null,
            'order_id'              => $item->getOrderId() ? $item->getOrderId() : null,
            'hits'                  => $item->getHits() ? $item->getHits() : null,
            'is_deleted'            => $item->isDeleted() ? $item->isDeleted() : 0,
            'is_published'          => $item->isPublished() ? $item->isPublished() : 0,
            'user_id'               => $item->getUserId() ? $item->getUserId() : null,
            'observatii'            => $item->getObservatii() ? $item->getObservatii() : null,
        );


        # ========================================================================================
        #  ADD or Update article
        # from fcp_articole
        #
        if( $objComentariu = $item->getComentariu() )
        {
            /**
             * @var ArticolItem $objComentariu
             */
            if( ArticolTable::getInstance()->save($objComentariu) ){
                $artID = $objComentariu->getId();
                $eData['comentariu_articol_id'] = $artID;
            }
        }

        // Insert/Update.
        if (!$item->getId()) {
            $this->db->insert('fcp_poezii', $eData);
            $id = $this->db->insert_id();
            $item->setId($id);
        } else {
            $this->db->update('fcp_poezii', $eData, 'id = ' . $item->getId());
        }

        # ========================================================================================
        #  ADD or Update cuvinte cheie
        # from fcp_taguri
        #
        // clean all tags
        $this->db->delete('fcp_taguri2entitati', ['poezie_id' => (int)$item->getId()]);
        if( $arrTagKeys = $item->getArrTagKeys() )
        {
            foreach($arrTagKeys as $tag)
            {
                $tag2entity = new Tag2EntityItem();
                $tag2entity->setTagId((int)$tag);
                $tag2entity->setPoezieId((int)$item->getId());

                Tag2EntityTable::getInstance()->save($tag2entity);
            }
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }

        return $item;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param string $searchTerm
     * @param int $limit
     * @return array|PoezieItem[]
     */
    public function fetchForAutocomplete($searchTerm, $limit=LIMIT_FOR_AUTOCOMPLETE)
    {
        $filters = new PoezieFilters(['searchTerm' => $searchTerm]);
        $data = $this->fetchAll($filters, $limit);

        return $data['items'] ?? [];
    }

}
