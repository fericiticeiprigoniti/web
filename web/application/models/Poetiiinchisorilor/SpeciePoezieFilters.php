<?php

class SpeciePoezieFilters
{

    const ORDER_BY_ID_ASC     = 'id_asc';
    const ORDER_BY_ID_DESC    = 'id_desc';
    const ORDER_BY_NAME_ASC   = 'name_asc';
    const ORDER_BY_NAME_DESC  = 'name_desc';
    const ORDER_BY_ORDER_ASC  = 'order_asc';
    const ORDER_BY_ORDER_DESC = 'order_desc';
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $excludedId;

    /**
     * @var int
     */
    private $genLiterarId;

    /**
     * @var array
     */
    private $orderBy = [];

    // ---------------------------------------------------------------------------------------------

    public function __construct(array $getData = [])
    {
        if (isset($getData['id'])) {
            $this->setId($getData['id']);
        }
        if (isset($getData['excludedId'])) {
            $this->setExcludedId($getData['excludedId']);
        }
        if (isset($getData['genLiterarId'])) {
            $this->setGenLiterarId($getData['genLiterarId']);
        }
        if (isset($getData['_orderBy'])) {
            $this->setOrderBy($getData['_orderBy']);
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return int
     */
    public function getExcludedId()
    {
        return $this->excludedId;
    }

    /**
     * @param int $excludedId
     */
    public function setExcludedId($excludedId)
    {
        $this->excludedId = $excludedId ? (int)$excludedId : null;
    }

    /**
     * @return int
     */
    public function getGenLiterarId()
    {
        return $this->genLiterarId;
    }

    /**
     * @param int $genLiterarId
     */
    public function setGenLiterarId($genLiterarId)
    {
        $this->genLiterarId = $genLiterarId ? (int)$genLiterarId : null;
    }

    /**
     * @return array
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param array $orderBy
     */
    public function setOrderBy(array $orderBy)
    {
        $orderBy = (is_array($orderBy) && count($orderBy) ? $orderBy : array());
        if (count($orderBy)) {
            $orderItems = array_keys(self::fetchOrderItems());
            foreach ($orderBy as $k=>$v) {
                if (!in_array($v, $orderItems)) {
                    unset($orderBy[$k]);
                }
            }
        }
        $this->orderBy = $orderBy;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return array
     */
    public static function fetchOrderItems()
    {
        return array(
            self::ORDER_BY_ID_ASC     => 'id_asc',
            self::ORDER_BY_ID_DESC    => 'id_desc',
            self::ORDER_BY_NAME_ASC   => 'name_asc',
            self::ORDER_BY_NAME_DESC  => 'name_desc',
            self::ORDER_BY_ORDER_ASC  => 'order_asc',
            self::ORDER_BY_ORDER_DESC => 'order_desc',
        );
    }


}
