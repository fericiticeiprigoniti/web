<?php

/**
 * Class PerioadaCreatieTable
 * @table fcp_poezii_perioadecreatie
 */
class PerioadaCreatieTable extends CI_Model
{
    /**
     * Used for caching items
     * @var PerioadaCreatieItem[]
     */
    public static array $collection = [];
    private static ?self $instance = null;

    /**
     * Singleton
     * PerioadaCreatieTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public static function getInstance(): self {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @param int $id
     */
    public static function resetCollectionId($id)
    {
        if (isset(self::$collection[$id])) {
            unset(self::$collection[$id]);
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param mixed $id
     * @throws Exception
     */
    public function load($id): ?PerioadaCreatieItem
    {
        $id = (int)$id;
        if (!$id) {
            throw new Exception("Invalid PerioadaCreatie id");
        }

        // If data is cached return it
        if (MODEL_CACHING_ENABLED && isset(self::$collection[$id])) {
            return self::$collection[$id];
        }

        // Fetch data
        $filters = new PerioadaCreatieFilters(['id' => $id]);
        $data = $this->fetchAll($filters);

        // Cache result
        self::$collection[$id] = $data['items'][0] ?? null;

        // Return cached result
        return self::$collection[$id];
    }

    /**
     * @param mixed $id
     */
    public static function get($id): ?PerioadaCreatieItem
    {
        if (empty($id)) {
            return null;
        }

        try {
            return self::getInstance()->load($id);
        } catch (Throwable $e) {
            log_message('error', $e->getMessage());
            return null;
        }
    }

    /**
     * Reset item from static collection and load a new one
     * @param int $id
     * @return PerioadaCreatieItem|null
     * @throws Exception
     */
    public function reload($id)
    {
        self::resetCollectionId($id);
        return $this->load($id);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param PerioadaCreatieFilters|null $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchAll(PerioadaCreatieFilters $filters = null, $limit = null, $offset = null)
    {
        // Set where and having conditions.
        $sqlWhere = array();
        $sqlHaving = array();
        if ($filters) {
            if ($filters->getId()) {
                $sqlWhere[] = "id = {$filters->getId()}";
            }
            if ($filters->getExcludedId()) {
                $sqlWhere[] = "id <> {$filters->getExcludedId()}";
            }
        }

        $sqlWhere = implode("\n\tAND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sqlHaving = implode("\n\tAND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? "HAVING " . $sqlHaving : '';

        // ------------------------------------

        $sqlOrder = array();
        if ($filters && count($filters->getOrderBy())) {
            foreach($filters->getOrderBy() as $ord) {
                switch($ord) {
                    case $filters::ORDER_BY_ID_ASC:
                        $sqlOrder[] = "id ASC";
                        break;
                    case $filters::ORDER_BY_ID_DESC:
                        $sqlOrder[] = "id DESC";
                        break;
                    case $filters::ORDER_BY_NAME_ASC:
                        $sqlOrder[] = "nume ASC";
                        break;
                    case $filters::ORDER_BY_NAME_DESC:
                        $sqlOrder[] = "nume DESC";
                        break;
        }
            }
        }
        if (!count($sqlOrder)) {
            $sqlOrder[] = "id DESC";
        }
        $sqlOrder = implode(", ", $sqlOrder);

        // ------------------------------------

        // Set limit.
        $sqlLimit = '';
        if (!is_null($offset) && !is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$offset . ', ' . (int)$limit;
        } elseif (!is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$limit;
        }

        // ------------------------------------

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    *
FROM
    fcp_poezii_perioadecreatie
WHERE
    1
    $sqlWhere
GROUP BY id
$sqlHaving
ORDER BY $sqlOrder
$sqlLimit
EOSQL;

        /** @var CI_DB_result $result */
        $result = $this->db->query($sql);
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        $items = array();
        foreach($result->result('array') as $row){
            $items[] = new PerioadaCreatieItem($row);
        }

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param PerioadaCreatieItem $item
     */
    public function save(PerioadaCreatieItem $item): void
    {
        // Set data.
        $eData = [
            'nume' => $item->getNume() ?: null,
            'alias' => $item->getAlias() ?: null,
        ];

        // Insert/Update.
        if (!$item->getId()) {
            $this->db->insert('fcp_poezii_perioadecreatie', $eData);
        } else {
            $this->db->update('fcp_poezii_perioadecreatie', $eData, 'id = ' . $item->getId());
        }

        // Remove old item from cache
        self::resetCollectionId($item->getId());
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param PerioadaCreatieItem $item
     * @return mixed
     */
    public function delete(PerioadaCreatieItem $item)
    {
        // Remove old item from cache
        self::resetCollectionId($item->getId());

        return $this->db->delete('fcp_poezii_perioadecreatie', 'id = ' . $item->getId());
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param PerioadaCreatieFilters|null $filters
     * @return array key -> value
     */
    public function fetchForSelect(PerioadaCreatieFilters $filters = null)
    {
        if (!$filters) {
            $filters = new PerioadaCreatieFilters();
        }

        $result = $this->fetchAll($filters);

        $data = [];
        if (isset($result['items']) && count($result['items'])) {
            /** @var PerioadaCreatieItem $objItem */
            foreach($result['items'] as $objItem) {
                $data[$objItem->getId()] = $objItem->getNume();
            }
        }

        return $data;
    }
}
