<?php

class PoezieItem
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $titlu;

    /**
     * @var string
     */
    private $titluVarianta;

    /**
     * @var int
     */
    private $personajId;

    /**
     * @var string
     */
    private $continutScurt;

    /**
     * @var string
     */
    private $continut;

    /**
     * @var int
     */
    private $comentariuArticolId;

    /**
     * @var int
     */
    private $comentariuUserId;

    /**
     * @var string
     */
    private $sursaLink;

    /**
     * @var string
     */
    private $sursaTitlu;

    /**
     * @var int
     */
    private $sursaDocCarteId;

    /**
     * @var int
     */
    private $sursaDocPublicatieId;

    /**
     * @var int
     */
    private $sursaDocStartPage;

    /**
     * @var int
     */
    private $sursaDocEndPage;

    /**
     * @var string
     */
    private $sursaCulegator;

    /**
     * @var int
     */
    private $cicluId;

    /**
     * @var int
     */
    private $subjectId;

    /**
     * @var int
     */
    private $perioadaCreatieiId;

    /**
     * @var string
     */
    private $dataCreatiei;

    /**
     * @var string
     */
    private $loculCreatiei;

    /**
     * @var int
     */
    private $locDetentieCreatieId;

    /**
     * @var int
     */
    private $specieId;

    /**
     * @var SpeciePoezieItem
     */
    private $objSpecie;

    /**
     * @var int
     */
    private $structuraStrofaId;

    /**
     * @var int
     */
    private $rimaId;

    /**
     * @var int
     */
    private $piciorMetricId;

    /**
     * @var int
     */
    private $nrStrofe;

    /**
     * @var string
     */
    private $dataInsert;

    /**
     * @var string
     */
    private $dataPublicare;

    /**
     * @var int
     */
    private $orderId;

    /**
     * @var int
     */
    private $hits;

    /**
     * @var int
     */
    private $isDeleted;

    /**
     * @var int
     */
    private $isPublished;

    /**
     * @var int
     */
    private $parentId;

    /**
     * @var int
     */
    private $userId;

    /**
     * @var string
     */
    private $observatii;

    /**
     * Nr. variante
     * @var int
     */
    private $alternativesNo;

    # ==========================================================================
    #
    #   Asociation table -> fcp_personaje
    #
    # ==========================================================================

    /**
     * @var PersonajItem $objPersonaj
     */
    private $objPersonaj;

    # ==========================================================================
    #
    #   Asociation table -> aauth_users
    #
    # ==========================================================================

    /**
     * @var AauthUserItem $objUser
     */
    private $objUser;

    # ==========================================================================
    #
    #   Asociation table -> fcp_articole
    #
    # ==========================================================================

    /**
     * @var ArticolItem $objComentariu
     */
    private $objComentariu;

    # ==========================================================================
    #
    #   Asociation table -> fcp_poezii_perioadecreatie
    #
    # ==========================================================================

    /**
     * @var PerioadaCreatieItem $objPerioadaCreatie
     */
    private $objPerioadaCreatie;

    # ==========================================================================
    #
    #   Asociation - media tables
    #
    # ==========================================================================

    /**
     * @var MediaItem[] - array with MediaItem objects
     */
    private $arrMedia = [];

    # ==========================================================================
    #
    #   Asociation table -> fcp_biblioteca_carti
    #
    # ==========================================================================

    /**
     * @var CarteItem $objCarte
     */
    private $objCarte;

    /**
     * @var TagItem[]
     */
    private $tagList;

    private $arrTagKeys;

    // ---------------------------------------------------------------------------------------------

    use ArrayOrJson;

    public function __construct(array $dbRow = [])
    {
        if ( !count($dbRow) ) {
            return;
        }

        $this->id                   = $dbRow['id'] ? (int)$dbRow['id'] : null;
        $this->titlu                = $dbRow['titlu'] ? (string)$dbRow['titlu'] : null;
        $this->titluVarianta        = $dbRow['titlu_varianta'] ? (string)$dbRow['titlu_varianta'] : null;
        $this->personajId           = $dbRow['personaj_id'] ? (int)$dbRow['personaj_id'] : null;
        $this->continutScurt        = $dbRow['continut_scurt'] ? (string)$dbRow['continut_scurt'] : null;
        $this->continut             = $dbRow['continut'] ? (string)$dbRow['continut'] : null;
        $this->comentariuArticolId  = $dbRow['comentariu_articol_id'] ? (int)$dbRow['comentariu_articol_id'] : null;
        //$this->comentariuUserId     = $dbRow['comentariu_user_id'] ? (int)$dbRow['comentariu_user_id'] : null;
        $this->sursaLink            = $dbRow['sursa_link'] ? (string)$dbRow['sursa_link'] : null;
        $this->sursaTitlu           = $dbRow['sursa_titlu'] ? (string)$dbRow['sursa_titlu'] : null;
        $this->sursaDocCarteId      = $dbRow['sursa_doc_carte_id'] ? (int)$dbRow['sursa_doc_carte_id'] : null;
        $this->sursaDocPublicatieId = $dbRow['sursa_doc_publicatie_id'] ? (int)$dbRow['sursa_doc_publicatie_id'] : null;
        $this->sursaDocStartPage    = $dbRow['sursa_doc_start_page'] ? (int)$dbRow['sursa_doc_start_page'] : null;
        $this->sursaDocEndPage      = $dbRow['sursa_doc_end_page'] ? (int)$dbRow['sursa_doc_end_page'] : null;
        $this->sursaCulegator       = $dbRow['sursa_culegator'] ? (string)$dbRow['sursa_culegator'] : null;
        $this->cicluId              = $dbRow['ciclu_id'] ? (int)$dbRow['ciclu_id'] : null;
        $this->subjectId            = $dbRow['subiect_id'] ? (int)$dbRow['subiect_id'] : null;
        $this->perioadaCreatieiId   = $dbRow['perioada_creatiei_id'] ? (int)$dbRow['perioada_creatiei_id'] : null;
        $this->dataCreatiei         = $dbRow['data_creatiei'] ? (string)$dbRow['data_creatiei'] : null;
        $this->loculCreatiei        = $dbRow['locul_creatiei'] ? (string)$dbRow['locul_creatiei'] : null;
        $this->locDetentieCreatieId = $dbRow['loc_detentie_creatie_id'] ? (int)$dbRow['loc_detentie_creatie_id'] : null;
        $this->specieId             = $dbRow['specie_id'] ? (int)$dbRow['specie_id'] : null;
        $this->structuraStrofaId    = $dbRow['structura_strofa_id'] ? (int)$dbRow['structura_strofa_id'] : null;
        $this->rimaId               = $dbRow['rima_id'] ? (int)$dbRow['rima_id'] : null;
        $this->piciorMetricId       = $dbRow['picior_metric_id'] ? (int)$dbRow['picior_metric_id'] : null;
        $this->nrStrofe             = $dbRow['nr_strofe'] ? (int)$dbRow['nr_strofe'] : null;
        $this->dataInsert           = $dbRow['data_insert'] ? (string)$dbRow['data_insert'] : null;
        $this->dataPublicare        = $dbRow['data_publicare'] ? (string)$dbRow['data_publicare'] : null;
        $this->orderId              = $dbRow['order_id'] ? (int)$dbRow['order_id'] : null;
        $this->hits                 = $dbRow['hits'] ? (int)$dbRow['hits'] : null;
        $this->isDeleted            = $dbRow['is_deleted'] ? (int)$dbRow['is_deleted'] : null;
        $this->isPublished          = $dbRow['is_published'] ? (int)$dbRow['is_published'] : null;
        $this->parentId             = $dbRow['parent_id'] ? (int)$dbRow['parent_id'] : null;
        $this->userId               = $dbRow['user_id'] ? (int)$dbRow['user_id'] : null;
        $this->observatii           = $dbRow['observatii'] ? (string)$dbRow['observatii'] : null;
        $this->alternativesNo       = $dbRow['nr_variante'] ? (int)$dbRow['nr_variante'] : null;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return string
     */
    public function getTitlu()
    {
        return $this->titlu;
    }

    /**
     * @param string $titlu
     */
    public function setTitlu($titlu)
    {
        $this->titlu = $titlu ? (string)$titlu : null;
    }

    /**
     * @return string
     */
    public function getTitluVarianta()
    {
        return $this->titluVarianta;
    }

    /**
     * @param string $titluVarianta
     */
    public function setTitluVarianta($titluVarianta)
    {
        $this->titluVarianta = $titluVarianta ? (string)$titluVarianta : null;
    }

    /**
     * @return int
     */
    public function getPersonajId()
    {
        return $this->personajId;
    }

    /**
     * @param int $personajId
     */
    public function setPersonajId($personajId)
    {
        $this->personajId = $personajId ? (int)$personajId : null;
    }

    /**
     * @return string|null
     */
    public function getContinutScurt()
    {
        return $this->continutScurt;
    }

    /**
     * @param string|null $continutScurt
     * @throws Exception
     */
    public function setContinutScurt($continut)
    {
        $this->continutScurt = $continut ? trim($continut) : null;
    }
    
    /**
     * @return string
     */
    public function getContinut()
    {
        return $this->continut;
    }

    /**
     * @param string $continut
     */
    public function setContinut($continut)
    {
        $this->continut = $continut ? (string)$continut : null;
    }

    /**
     * @return int
     */
    public function getComentariuArticolId()
    {
        return $this->comentariuArticolId;
    }

    /**
     * @param int $comentariuArticolId
     */
    public function setComentariuArticolId($comentariuArticolId)
    {
        $this->comentariuArticolId = $comentariuArticolId ? (int)$comentariuArticolId : null;
    }

    /**
     * @return int
     */
    public function getComentariuUserId()
    {
        return $this->comentariuUserId;
    }

    /**
     * @param int $comentariuUserId
     */
    public function setComentariuUserId($comentariuUserId)
    {
        $this->comentariuUserId = $comentariuUserId ? (int)$comentariuUserId : null;
    }

    /**
     * @return string
     */
    public function getSursaLink()
    {
        return $this->sursaLink;
    }

    /**
     * @param string $sursaLink
     */
    public function setSursaLink($sursaLink)
    {
        $this->sursaLink = $sursaLink ? (string)$sursaLink : null;
    }

    /**
     * @return string
     */
    public function getSursaTitlu()
    {
        return $this->sursaTitlu;
    }

    /**
     * @param string $sursaTitlu
     */
    public function setSursaTitlu($sursaTitlu)
    {
        $this->sursaTitlu = $sursaTitlu ? (string)$sursaTitlu : null;
    }

    /**
     * @return int
     */
    public function getSursaDocCarteId()
    {
        return $this->sursaDocCarteId;
    }

    /**
     * @param int $sursaDocCarteId
     */
    public function setSursaDocCarteId($sursaDocCarteId)
    {
        $this->sursaDocCarteId = $sursaDocCarteId ? (int)$sursaDocCarteId : null;
    }

    /**
     * @return int
     */
    public function getSursaDocPublicatieId()
    {
        return $this->sursaDocPublicatieId;
    }

    /**
     * @param int $sursaDocPublicatieId
     */
    public function setSursaDocPublicatieId($sursaDocPublicatieId)
    {
        $this->sursaDocPublicatieId = $sursaDocPublicatieId ? (int)$sursaDocPublicatieId : null;
    }

    /**
     * @return int
     */
    public function getSursaDocStartPage()
    {
        return $this->sursaDocStartPage;
    }

    /**
     * @param int $sursaDocStartPage
     */
    public function setSursaDocStartPage($sursaDocStartPage)
    {
        $this->sursaDocStartPage = $sursaDocStartPage ? (int)$sursaDocStartPage : null;
    }

    /**
     * @return int
     */
    public function getSursaDocEndPage()
    {
        return $this->sursaDocEndPage;
    }

    /**
     * @param int $sursaDocEndPage
     */
    public function setSursaDocEndPage($sursaDocEndPage)
    {
        $this->sursaDocEndPage = $sursaDocEndPage ? (int)$sursaDocEndPage : null;
    }

    /**
     * @return string
     */
    public function getSursaCulegator()
    {
        return $this->sursaCulegator;
    }

    /**
     * @param string $sursaCulegator
     */
    public function setSursaCulegator($sursaCulegator)
    {
        $this->sursaCulegator = $sursaCulegator ? (string)$sursaCulegator : null;
    }

    /**
     * @return int
     */
    public function getCicluId()
    {
        return $this->cicluId;
    }

    /**
     * @param int $cicluId
     */
    public function setCicluId($cicluId)
    {
        $this->cicluId = $cicluId ? (int)$cicluId : null;
    }

    /**
     * @return int
     */
    public function getSubjectId()
    {
        return $this->subjectId;
    }

    /**
     * @param int $subjectId
     */
    public function setSubjectId($subjectId)
    {
        $this->subjectId = $subjectId ? (int)$subjectId : null;
    }

    /**
     * @return int
     */
    public function getPerioadaCreatieiId()
    {
        return $this->perioadaCreatieiId;
    }

    /**
     * @param int $perioadaCreatieiId
     */
    public function setPerioadaCreatieiId($perioadaCreatieiId)
    {
        $this->perioadaCreatieiId = $perioadaCreatieiId ? (int)$perioadaCreatieiId : null;
    }

    /**
     * @return string
     */
    public function getDataCreatiei()
    {
        return $this->dataCreatiei;
    }

    /**
     * @param string $dataCreatiei
     */
    public function setDataCreatiei($dataCreatiei)
    {
        $this->dataCreatiei = $dataCreatiei ? (string)$dataCreatiei : null;
    }

    /**
     * @return string
     */
    public function getLoculCreatiei()
    {
        return $this->loculCreatiei;
    }

    /**
     * @param string $loculCreatiei
     */
    public function setLoculCreatiei($loculCreatiei)
    {
        $this->loculCreatiei = $loculCreatiei ? (string)$loculCreatiei : null;
    }

    /**
     * @return int
     */
    public function getLocDetentieCreatieId()
    {
        return $this->locDetentieCreatieId;
    }

    /**
     * @param int $locDetentieCreatieId
     */
    public function setLocDetentieCreatieId($locDetentieCreatieId)
    {
        $this->locDetentieCreatieId = $locDetentieCreatieId ? (int)$locDetentieCreatieId : null;
    }

    /**
     * @return int
     */
    public function getSpecieId()
    {
        return $this->specieId;
    }

    /**
     * @param int $specieId
     */
    public function setSpecieId($specieId)
    {
        $this->specieId = $specieId ? (int)$specieId : null;
    }

    public function getSpecie(): ?SpeciePoezieItem
    {
        $this->objSpecie = $this->objSpecie ?: SpeciePoezieTable::get($this->specieId);
        return $this->objSpecie;
    }

    /**
     * @return int
     */
    public function getStructuraStrofaId()
    {
        return $this->structuraStrofaId;
    }

    /**
     * @param int $structuraStrofaId
     */
    public function setStructuraStrofaId($structuraStrofaId)
    {
        $this->structuraStrofaId = $structuraStrofaId ? (int)$structuraStrofaId : null;
    }

    /**
     * @return int
     */
    public function getRimaId()
    {
        return $this->rimaId;
    }

    /**
     * @param int $rimaId
     */
    public function setRimaId($rimaId)
    {
        $this->rimaId = $rimaId ? (int)$rimaId : null;
    }

    /**
     * @return int
     */
    public function getPiciorMetricId()
    {
        return $this->piciorMetricId;
    }

    /**
     * @param int $piciorMetricId
     */
    public function setPiciorMetricId($piciorMetricId)
    {
        $this->piciorMetricId = $piciorMetricId ? (int)$piciorMetricId : null;
    }

    /**
     * @return int
     */
    public function getNrStrofe()
    {
        return $this->nrStrofe;
    }

    /**
     * @param int $nrStrofe
     */
    public function setNrStrofe($nrStrofe)
    {
        $this->nrStrofe = $nrStrofe ? (int)$nrStrofe : null;
    }

    /**
     * @return string
     */
    public function getDataInsert()
    {
        return $this->dataInsert;
    }

    /**
     * @param string $dataInsert
     */
    public function setDataInsert($dataInsert)
    {
        $this->dataInsert = $dataInsert ? (string)$dataInsert : null;
    }

    /**
     * @return string|null
     */
    public function getDataPublicare()
    {
        return $this->dataPublicare;
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId ? (int)$orderId : null;
    }

    /**
     * @return int
     */
    public function getHits()
    {
        return $this->hits;
    }

    /**
     * @param int $hits
     */
    public function setHits($hits)
    {
        $this->hits = $hits ? (int)$hits : null;
    }

    /**
     * @return int
     */
    public function isDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param int $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted ? (int)$isDeleted : null;
    }

    /**
     * @return int
     */
    public function isPublished()
    {
        return $this->isPublished;
    }

    /**
     * @param int $isPublished
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished ? (int)$isPublished : null;
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param int $parentId
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId ? (int)$parentId : 0;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId ? (int)$userId : null;
    }

    /**
     * @return string
     */
    public function getObservatii()
    {
        return $this->observatii;
    }

    /**
     * @param string $observatii
     */
    public function setObservatii($observatii)
    {
        $this->observatii = $observatii ? (string)$observatii : null;
    }

    /**
     * @return int
     */
    public function getAlternativesNo()
    {
        return $this->alternativesNo;
    }

    // -----------------------------------------------------------------------------------------------------------------

    public function getCarte(): ?CarteItem
    {
        $this->objCarte = $this->objCarte ?: CarteTable::get($this->sursaDocCarteId);
        return $this->objCarte;
    }

    public function getPersonaj(): ?PersonajItem
    {
        $this->objPersonaj = $this->objPersonaj ?: PersonajTable::get($this->personajId);
        return $this->objPersonaj;
    }

    public function getUser(): ?AauthUserItem
    {
        $this->objUser = $this->objUser ?: AauthUserTable::get($this->userId);
        return $this->objUser;
    }

    // -------------------------------------------------------------------------

    /**
     * @return MediaItem[]
     */
    public function getMedia()
    {
        if ($this->arrMedia) {

            return $this->arrMedia;

        } elseif ($this->id) {

            $filters = new MediaFilters();
            $filters->setMediaTipId(MEDIA_TIP_VIDEO);
            $filters->setJoin(MEDIA_JOIN_POEZII, $this->id);

            $res = MediaTable::getInstance()->fetchAll($filters);

            if (empty($res['items'])) {
                $this->arrMedia = [];
            } else {
                $this->arrMedia = $res['items'];
            }

            return $this->arrMedia;

        } else {
            return [];
        }
    }

    /**
     * @param array
     */
    public function setArrTagKeys($tags)
    {
        $this->arrTagKeys = $tags;
    }

    /**
     * returneaza lista cu tag keys
     *
     * @return array|bool
     */
    public function getArrTagKeys()
    {
        if( $this->arrTagKeys )
        {
            return $this->arrTagKeys;
        }elseif( $list = $this->fetchTagList() ){

            foreach($list as $tag)
            {
                $this->arrTagKeys[] = $tag->getId();
            }

            return $this->arrTagKeys;
        }else{
            return [];
        }
    }

    // ----------------------------- ADDON  ------------------------------------

    public function getPerioadaCreatie(): ?PerioadaCreatieItem
    {
        $this->objPerioadaCreatie = $this->objPerioadaCreatie ?: PerioadaCreatieTable::get($this->perioadaCreatieiId);
        return $this->objPerioadaCreatie;
    }

    # ==========================================================================
    #
    #   Asociation table -> fcp_articole
    #
    # ==========================================================================

    public function getComentariu(): ?ArticolItem
    {
        $this->objComentariu = $this->objComentariu ?: ArticolTable::get($this->comentariuArticolId);
        return $this->objComentariu;
    }

    /**
     * @param ArticolItem $objArticol
     */
    public function setComentariu($objArticol)
    {
        $this->objComentariu = $objArticol;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * @return TagItem[]|null
     */
    public function fetchTagList()
    {
        if ($this->tagList) {
            return $this->tagList;
        }

        if (!$this->id) {
            return null;
        }

        $tagIdList = Tag2EntityTable::getInstance()->fetchTagIdListByPoetryId($this->id);
        if (!count($tagIdList)) {
            return null;
        }

        $filters = new TagFilters();
        $filters->setIdList($tagIdList);
        $res = TagTable::getInstance()->fetchAll($filters);
        $this->tagList = $res && isset($res['items']) ? $res['items'] : array();

        return $this->tagList;
    }

    /**
     * returneaza lista predefinita pentru select2
     *
     * @return array|bool
     */
    public function fetchTagListSelect2()
    {
        $results = false;

        if( $list = $this->fetchTagList() )
        {
            $results = [];
            foreach($list as $tag)
            {
                $results[] = [
                    'id'   => $tag->getId(),
                    'text' => $tag->getNume()
                ];
            }
        }

        return $results? json_encode($results) : false;
    }

}
