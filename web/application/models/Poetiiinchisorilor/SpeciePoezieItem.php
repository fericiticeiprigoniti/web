<?php

class SpeciePoezieItem
{
    use ArrayOrJson;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $nume;

    /**
     * @var string
     */
    private $alias;

    /**
     * @var int
     */
    private $genLiterarId;

    /**
     * @var int
     */
    private $order;


    // ---------------------------------------------------------------------------------------------

    public function __construct(array $dbRow = [])
    {
        if ( !count($dbRow) ) {
            return;
        }

        $this->id = $dbRow['id'] ? (int)$dbRow['id'] : null;
        $this->nume = $dbRow['nume'] ? (string)$dbRow['nume'] : null;
        $this->alias = $dbRow['alias'] ? (string)$dbRow['alias'] : null;
        $this->genLiterarId = $dbRow['gen_literar_id'] ? (int)$dbRow['gen_literar_id'] : null;
        $this->order = $dbRow['order'] ? (int)$dbRow['order'] : null;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return string
     */
    public function getNume()
    {
        return $this->nume;
    }

    /**
     * @param string $nume
     */
    public function setNume($nume)
    {
        $this->nume = $nume ? (string)$nume : null;
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     */
    public function setAlias($alias)
    {
        $this->alias = $alias ? (string)$alias : null;
    }

    /**
     * @return int
     */
    public function getGenLiterarId()
    {
        return $this->genLiterarId;
    }

    /**
     * @param int $genLiterarId
     */
    public function setGenLiterarId($genLiterarId)
    {
        $this->genLiterarId = $genLiterarId ? (int)$genLiterarId : null;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder($order)
    {
        $this->order = $order ? (int)$order : null;
    }


}
