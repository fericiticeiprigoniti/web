<?php

/**
 * Class PoezieCicluriTable
 * @table fcp_poezii_cicluri
 */
class PoezieCicluriTable extends CI_Model
{

    /**
     * @var PoezieCicluriTable
     */
    private static $instance;


    /**
     * Singleton
     * PoezieCicluriTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * We can later dependency inject and thus unit test this
     * @return PoezieCicluriTable
     */
    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param mixed $id
     * @return PoezieCicluriItem|null
     * @throws Exception
     */
    public function load($id)
    {
        if (!$id) {
            throw new Exception("Invalid PoezieCicluri id");
        }
        $filters = new PoezieCicluriFilters(array('id'=>$id));
        $data = $this->fetchAll($filters);
        $item = isset($data['items']) && isset($data['items'][0]) ? $data['items'][0] : null;
        return $item;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param PoezieCicluriFilters $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchAll(PoezieCicluriFilters $filters = null, $limit = null, $offset = null)
    {
        // Set where and having conditions.
        $sqlWhere = array();
        $sqlHaving = array();
        if ($filters) {
            if ($filters->getId()) {
                $sqlWhere[] = "id = {$filters->getId()}";
            }
            if ($filters->getExcludedId()) {
                $sqlWhere[] = "id <> {$filters->getExcludedId()}";
            }
            if ($filters->getPoetId()) {
                $sqlWhere[] = "poet_id = {$filters->getPoetId()}";
            }
        }

        $sqlWhere = implode("\n\tAND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sqlHaving = implode("\n\tAND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? "HAVING " . $sqlHaving : '';

        // ------------------------------------

        $sqlOrder = array();
        if ($filters && count($filters->getOrderBy())) {
            foreach($filters->getOrderBy() as $ord) {
                switch($ord) {
                    case $filters::ORDER_BY_ID_ASC:
                        $sqlOrder[] = "id ASC";
                        break;
                    case $filters::ORDER_BY_ID_DESC:
                        $sqlOrder[] = "id DESC";
                        break;
        }
            }
        }
        if (!count($sqlOrder)) {
            $sqlOrder[] = "id DESC";
        }
        $sqlOrder = implode(", ", $sqlOrder);

        // ------------------------------------

        // Set limit.
        $sqlLimit = '';
        if (!is_null($offset) && !is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$offset . ', ' . (int)$limit;
        } elseif (!is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$limit;
        }

        // ------------------------------------

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    *
FROM
    fcp_poezii_cicluri
WHERE
    1
    $sqlWhere
GROUP BY id
$sqlHaving
ORDER BY $sqlOrder
$sqlLimit
EOSQL;

        /** @var CI_DB_result $result */
        $result = $this->db->query($sql);
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        $items = array();
        foreach($result->result('array') as $row){
            $items[] = new PoezieCicluriItem($row);
        }

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param PoezieCicluriItem $item
     * @return bool|int
     */
    public function save(PoezieCicluriItem $item)
    {
        // Set data.
        $eData = array(
            'name'    => $item->getName() ? $item->getName() : null,
            'poet_id' => $item->getPoetId() ? $item->getPoetId() : null,
        );

        // Insert/Update.
        if (!$item->getId()) {
            $res = $this->db->insert('fcp_poezii_cicluri', $eData);
        } else {
            $res = $this->db->update('fcp_poezii_cicluri', $eData, 'id = ' . $item->getId());
        }

        return $res;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param PoezieCicluriItem $item
     * @return mixed
     */
    public function delete(PoezieCicluriItem $item)
    {
        return $this->db->delete('fcp_poezii_cicluri', 'id = ' . $item->getId());
    }

    // ---------------------------------------------------------------------------------------------

    // return key -> value
    public function fetchForSelect($filters = false)
    {
        if( !$filters ) $filters = new PoezieCicluriFilters();
        $result = $this->fetchAll($filters);

        $data = [];
        if (isset($result['items']) && count($result['items'])) {
            /** @var PoezieCicluriItem $objItem */
            foreach($result['items'] as $objItem) {
                $data[$objItem->getId()] = $objItem->getName();
            }
        }

        return $data;
    }

}
