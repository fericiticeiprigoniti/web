<?php

class PoezieFilters
{

    const ORDER_BY_ID_ASC = 'id_asc';
    const ORDER_BY_ID_DESC = 'id_desc';
    const ORDER_BY_ORDER_ID_ASC = 'order_id_asc';
    const ORDER_BY_ORDER_ID_DESC = 'order_id_desc';
    const ORDER_BY_RAND          = 'order_rand';

    const PUBLISHED_YES     = 'yes';
    const PUBLISHED_NO      = 'no';

    const DELETED_YES       = 'yes';
    const DELETED_NO        = 'no';

    // Strofe
    const LYRICS_NO_1_5     = '1-5';
    const LYRICS_NO_6_10    = '6-10';
    const LYRICS_NO_11_15   = '11-15';
    const LYRICS_NO_16_20   = '16-20';
    const LYRICS_NO_21_30   = '21-30';
    const LYRICS_NO_31_50   = '31-50';
    const LYRICS_NO_51_100  = '51-100';
    const LYRICS_NO_100plus = '100plus';

    // Variante
    const ALTERNATIVES_NO_1_2     = '1-2';
    const ALTERNATIVES_NO_3_5     = '3-5';
    const ALTERNATIVES_NO_6_10    = '6-10';
    const ALTERNATIVES_NO_11_20   = '11-20';
    const ALTERNATIVES_NO_21_30   = '21-30';
    const ALTERNATIVES_NO_30plus  = '30plus';

    /**
     * @var int
     * @example Id-ul poeziei (daca este setat ID-ul poeziei livram toate detaliile extinse specie/rima/perioada/structura etc)
     */
    private $id;

    /**
     * @var int
     * @example Dacă se dorește să fie exclus un ID din response
     */
    private $excludedId;

    /**
     * @var string
     * @example Cauta in titlul poeziei
     */
    private $titleLike;

    /**
     * @var int
     * @example ID-ul personalului -> /personaje/list
     */
    private $personajId;

    /**
     * @var PersonajItem
     * @docs_ignore
     */
    private $personaj;

    /**
     * @var string
     * @example Caută un text in continutul poeziei
     */
    private $contentLike;

    /**
     * @var int
     */
    private $comentariuArticolId;

    /**
     * @var int
     * @example Cauta poeziile care au comentarii scrise de un anumit utilizator
     */
    private $comentariuUserId;

    /**
     * @var int
     * @example ID-ul cartii din care pace parte poezia
     */
    private $sursaDocCarteId;

    /**
     * @var int
     */
    private $sursaDocPublicatieId;

    /**
     * @var int
     */
    private $subjectId;

    /**
     * @var int
     * @example Perioada de creatie -> /poezii/perioade_creatie
     */
    private $perioadaCreatieiId;

    /**
     * @var string
     */
    private $creationPeriodLike;

    /**
     * @var string
     */
    private $creationPlaceLike;

    /**
     * @var int
     */
    private $specieId;

    /**
     * @var int
     */
    private $structuraStrofaId;

    /**
     * @var int
     */
    private $rimaId;

    /**
     * @var int
     */
    private $piciorMetricId;

    /**
     * @var int
     * @example Folosit in cazul in care dorim afisarea tuturor variantelor a unei poezii date
     */
    private $parentId;

    /**
     * @var int
     * @example ID-ul utilizatorului care a adaugat poezie
     */
    private $userId;

    /**
     * @var array
     */
    private $tagIdList = [];

    /**
     * @var string|null
     * @docs_ignore
     */
    private ?string $isPublished = null;

    /**
     * @var string
     * @docs_ignore
     */
    private $isDeleted;

    /**
     * @var string
     * @example Suporta: 1-5 / 6-10 / 11-15 / 16-20 / 21-30 / 31-50 / 51-100 / 100plus
     */
    private $lyricsNo;

    /**
     * @var string
     * @example Suporta: 1-2 / 3-5 / 6-10 / 11-20 / 21-30 / 30plus
     */
    private $alternativesNo;

    /**
     * @var array | string
     * @example Suporta: id_asc / id_desc / order_id_asc / order_id_desc / order_rand
     */
    private $orderBy = [];

    // ---------------------------------------------------------------------------------------------

    public function __construct(array $getData = [])
    {
        if (isset($getData['id'])) {
            $this->setId($getData['id']);
        }
        if (isset($getData['excludedId'])) {
            $this->setExcludedId($getData['excludedId']);
        }
        if (isset($getData['titleLike'])) {
            $this->setTitleLike($getData['titleLike']);
        }
        if (isset($getData['personajId'])) {
            $this->setPersonajId($getData['personajId']);
        }
        if (isset($getData['contentLike'])) {
            $this->setContentLike($getData['contentLike']);
        }
        if (isset($getData['comentariuArticolId'])) {
            $this->setComentariuArticolId($getData['comentariuArticolId']);
        }
        if (isset($getData['comentariuUserId'])) {
            $this->setComentariuUserId($getData['comentariuUserId']);
        }
        if (isset($getData['sursaDocCarteId'])) {
            $this->setSursaDocCarteId($getData['sursaDocCarteId']);
        }
        if (isset($getData['sursaDocPublicatieId'])) {
            $this->setSursaDocPublicatieId($getData['sursaDocPublicatieId']);
        }
        if (isset($getData['subjectId'])) {
            $this->setSubjectId($getData['subjectId']);
        }
        if (isset($getData['perioadaCreatieiId'])) {
            $this->setPerioadaCreatieiId($getData['perioadaCreatieiId']);
        }
        if (isset($getData['creationPeriodLike'])) {
            $this->setCreationPeriodLike($getData['creationPeriodLike']);
        }
        if (isset($getData['creationPlaceLike'])) {
            $this->setCreationPlaceLike($getData['creationPlaceLike']);
        }
        if (isset($getData['specieId'])) {
            $this->setSpecieId($getData['specieId']);
        }
        if (isset($getData['structuraStrofaId'])) {
            $this->setStructuraStrofaId($getData['structuraStrofaId']);
        }
        if (isset($getData['rimaId'])) {
            $this->setRimaId($getData['rimaId']);
        }
        if (isset($getData['piciorMetricId'])) {
            $this->setPiciorMetricId($getData['piciorMetricId']);
        }
        if (isset($getData['parentId'])) {
            $this->setParentId($getData['parentId']);
        }
        if (isset($getData['userId'])) {
            $this->setUserId($getData['userId']);
        }
        if (isset($getData['tagIdList'])) {
            $this->setTagIdList($getData['tagIdList']);
        }
        if (isset($getData['isPublished'])) {
            $this->setIsPublished($getData['isPublished']);
        }
        if (isset($getData['isDeleted'])) {
            $this->setIsDeleted($getData['isDeleted']);
        }
        if (isset($getData['lyricsNo'])) {
            $this->setLyricsNo($getData['lyricsNo']);
        }
        if (isset($getData['alternativesNo'])) {
            $this->setAlternativesNo($getData['alternativesNo']);
        }

        if (isset($getData['_orderBy'])) {
            $arr_order = is_array($getData['_orderBy'])? $getData['_orderBy'] : [$getData['_orderBy']];
            $this->setOrderBy($arr_order);
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return int
     */
    public function getExcludedId()
    {
        return $this->excludedId;
    }

    /**
     * @param int $excludedId
     */
    public function setExcludedId($excludedId)
    {
        $this->excludedId = $excludedId ? (int)$excludedId : null;
    }

    /**
     * @return string
     */
    public function getTitleLike()
    {
        return $this->titleLike;
    }

    /**
     * @param string $titleLike
     */
    public function setTitleLike($titleLike)
    {
        $this->titleLike = $titleLike ? (string)$titleLike : null;
    }

    /**
     * @return int
     */
    public function getPersonajId()
    {
        return $this->personajId;
    }

    /**
     * @param int $personajId
     */
    public function setPersonajId($personajId)
    {
        $this->personajId = $personajId ? (int)$personajId : null;
    }

    /**
     * @return PersonajItem|null
     * @throws Exception
     */
    public function getPersonaj()
    {
        if ($this->personaj) {
            return $this->personaj;
        } elseif ($this->personajId) {
            $this->personaj = PersonajTable::getInstance()->load($this->personajId);
            return $this->personaj;
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getContentLike()
    {
        return $this->contentLike;
    }

    /**
     * @param string $contentLike
     */
    public function setContentLike($contentLike)
    {
        $this->contentLike = $contentLike ? (string)$contentLike : null;
    }

    /**
     * @return int
     */
    public function getComentariuArticolId()
    {
        return $this->comentariuArticolId;
    }

    /**
     * @param int $comentariuArticolId
     */
    public function setComentariuArticolId($comentariuArticolId)
    {
        $this->comentariuArticolId = $comentariuArticolId ? (int)$comentariuArticolId : null;
    }

    /**
     * @return int
     */
    public function getComentariuUserId()
    {
        return $this->comentariuUserId;
    }

    /**
     * @param int $comentariuUserId
     */
    public function setComentariuUserId($comentariuUserId)
    {
        $this->comentariuUserId = $comentariuUserId ? (int)$comentariuUserId : null;
    }

    /**
     * @return int
     */
    public function getSursaDocCarteId()
    {
        return $this->sursaDocCarteId;
    }

    /**
     * @param int $sursaDocCarteId
     */
    public function setSursaDocCarteId($sursaDocCarteId)
    {
        $this->sursaDocCarteId = $sursaDocCarteId ? (int)$sursaDocCarteId : null;
    }

    /**
     * @return int
     */
    public function getSursaDocPublicatieId()
    {
        return $this->sursaDocPublicatieId;
    }

    /**
     * @param int $sursaDocPublicatieId
     */
    public function setSursaDocPublicatieId($sursaDocPublicatieId)
    {
        $this->sursaDocPublicatieId = $sursaDocPublicatieId ? (int)$sursaDocPublicatieId : null;
    }

    /**
     * @return int
     */
    public function getSubjectId()
    {
        return $this->subjectId;
    }

    /**
     * @param int $subjectId
     */
    public function setSubjectId($subjectId)
    {
        $this->subjectId = $subjectId ? (int)$subjectId : null;
    }

    /**
     * @return int
     */
    public function getPerioadaCreatieiId()
    {
        return $this->perioadaCreatieiId;
    }

    /**
     * @param int $perioadaCreatieiId
     */
    public function setPerioadaCreatieiId($perioadaCreatieiId)
    {
        $this->perioadaCreatieiId = $perioadaCreatieiId ? (int)$perioadaCreatieiId : null;
    }

    /**
     * @return string
     */
    public function getCreationPeriodLike()
    {
        return $this->creationPeriodLike;
    }

    /**
     * @param string $creationPeriodLike
     */
    public function setCreationPeriodLike($creationPeriodLike)
    {
        $this->creationPeriodLike = $creationPeriodLike ? (string)$creationPeriodLike : null;
    }

    /**
     * @return string
     */
    public function getCreationPlaceLike()
    {
        return $this->creationPlaceLike;
    }

    /**
     * @param string $creationPlaceLike
     */
    public function setCreationPlaceLike($creationPlaceLike)
    {
        $this->creationPlaceLike = $creationPlaceLike ? (string)$creationPlaceLike : null;
    }

    /**
     * @return int
     */
    public function getSpecieId()
    {
        return $this->specieId;
    }

    /**
     * @param int $specieId
     */
    public function setSpecieId($specieId)
    {
        $this->specieId = $specieId ? (int)$specieId : null;
    }

    /**
     * @return int
     */
    public function getStructuraStrofaId()
    {
        return $this->structuraStrofaId;
    }

    /**
     * @param int $structuraStrofaId
     */
    public function setStructuraStrofaId($structuraStrofaId)
    {
        $this->structuraStrofaId = $structuraStrofaId ? (int)$structuraStrofaId : null;
    }

    /**
     * @return int
     */
    public function getRimaId()
    {
        return $this->rimaId;
    }

    /**
     * @param int $rimaId
     */
    public function setRimaId($rimaId)
    {
        $this->rimaId = $rimaId ? (int)$rimaId : null;
    }

    /**
     * @return int
     */
    public function getPiciorMetricId()
    {
        return $this->piciorMetricId;
    }

    /**
     * @param int $piciorMetricId
     */
    public function setPiciorMetricId($piciorMetricId)
    {
        $this->piciorMetricId = $piciorMetricId ? (int)$piciorMetricId : null;
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param int $parentId
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId ? (int)$parentId : 0;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId ? (int)$userId : null;
    }

    /**
     * @return array
     */
    public function getTagIdList()
    {
        return $this->tagIdList;
    }

    /**
     * @param array $tagIdList
     */
    public function setTagIdList(array $tagIdList)
    {
        $this->tagIdList = array_map('intval', $tagIdList);
    }

    /**
     * @return string|null
     */
    public function isPublished()
    {
        return $this->isPublished;
    }

    /**
     * @var string|null $value
     */
    public function setIsPublished($value): void
    {
        $this->isPublished = !empty($value) ? (string)$value : null;
    }

    /**
     * @return string[]
     */
    public static function fetchPublishedList()
    {
        return [
            self::PUBLISHED_YES   => 'Publicat',
            self::PUBLISHED_NO    => 'Nepublicat',
        ];
    }

    /**
     * @return string|null
     */
    public function isDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @var string|null $value
     */
    public function setIsDeleted($value)
    {
        $this->isDeleted = !empty($value) ? (string)$value : null;
    }

    /**
     * @return string[]
     */
    public static function fetchDeletedList(): array
    {
        return [
            self::DELETED_YES   => 'Ștearsă',
            self::DELETED_NO    => 'Neștearsă',
        ];
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return string
     */
    public function getLyricsNo()
    {
        return $this->lyricsNo;
    }

    /**
     * @param string $lyricsNo
     */
    public function setLyricsNo($lyricsNo)
    {
        $list = self::fetchLyricsNoList();
        $this->lyricsNo = isset($list[$lyricsNo]) ? $lyricsNo : null;
    }

    /**
     * @return string[]
     */
    public static function fetchLyricsNoList(): array
    {
        return [
            self::LYRICS_NO_1_5     => '1-5 strofe',
            self::LYRICS_NO_6_10    => '6-10 strofe',
            self::LYRICS_NO_11_15   => '11-15 strofe',
            self::LYRICS_NO_16_20   => '16-20 strofe',
            self::LYRICS_NO_21_30   => '21-30 strofe',
            self::LYRICS_NO_31_50   => '31-50 strofe',
            self::LYRICS_NO_51_100  => '51-100 strofe',
            self::LYRICS_NO_100plus => '100+',
        ];
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return string
     */
    public function getAlternativesNo()
    {
        return $this->alternativesNo;
    }

    /**
     * @param string $alternativesNo
     */
    public function setAlternativesNo($alternativesNo)
    {
        $list = self::fetchAlternativesNoList();
        $this->alternativesNo = isset($list[$alternativesNo]) ? $alternativesNo : null;
    }

    /**
     * @return string[]
     */
    public static function fetchAlternativesNoList(): array
    {
        return [
            self::ALTERNATIVES_NO_1_2    => '1-2 variante',
            self::ALTERNATIVES_NO_3_5    => '3-5 variante',
            self::ALTERNATIVES_NO_6_10   => '6-10 variante',
            self::ALTERNATIVES_NO_11_20  => '11-20 variante',
            self::ALTERNATIVES_NO_21_30  => '21-30 variante',
            self::ALTERNATIVES_NO_30plus => '30+',
        ];
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return array
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param array $orderBy
     */
    public function setOrderBy(array $orderBy)
    {
        $orderBy = count($orderBy) ? $orderBy : [];

        if (!count($orderBy)) {
            $this->orderBy = [];
            return;
        }

        $orderItems = array_keys(self::fetchOrderItems());
        foreach ($orderBy as $k=>$v) {
            if (!in_array($v, $orderItems)) {
                unset($orderBy[$k]);
            }
        }

        $this->orderBy = $orderBy;
    }

    /**
     * @return array
     */
    public static function fetchOrderItems()
    {
        return [
            self::ORDER_BY_ID_ASC       => 'Id - ASC',
            self::ORDER_BY_ID_DESC      => 'Id - DESC',
            self::ORDER_BY_ORDER_ID_ASC => 'Order - ASC',
            self::ORDER_BY_ORDER_ID_DESC=> 'Order - DESC',
            self::ORDER_BY_RAND         => 'Order - RAND',
        ];
    }


}
