<?php

/**
 * Class DimensiuneTable
 * @table fcp_biblioteca_carti_dimensiuni
 */
class DimensiuneTable extends CI_Model
{

    /**
     * @var DimensiuneTable
     */
    private static $instance;


    /**
     * Singleton
     * DimensiuneTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    // ---------------------------------------------------------------------------------------------

    /**
     *  we can later dependency inject and thus unit test this
     * @return DimensiuneTable
     */
    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }


    // ---------------------------------------------------------------------------------------------

    /**
     * @param DimensiuneItem $dimensiune
     * @return bool|int
     */
    public function save(DimensiuneItem $dimensiune)
    {
        // Set data.
        $eData = array(
            'latime'   => $dimensiune->getLatime(),
            'inaltime' => $dimensiune->getInaltime(),
        );
        
        // Insert/Update.
        if (!$dimensiune->getId()) {
            $res = $this->db->insert('fcp_biblioteca_carti_dimensiuni', $eData);
        } else {
            $res = $this->db->update('fcp_biblioteca_carti_dimensiuni', $eData, "id = " . $dimensiune->getId());
        }
        
        // Return result.
        return $res;
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * @param DimensiuneItem $dimensiune
     * @return mixed
     * @throws Exception
     */
    public function delete(CategorieCarteItem $dimensiune)
    {
        // Verifica daca aceasta categorie este folosita.
        if ($dimensiune->foundUsages()) {
            throw new Exception("Dimensiunea este folosita in relatia cu alte elemente");
        }
    
        return $this->db->delete('fcp_biblioteca_carti_dimensiuni', "id = " . $dimensiune->getId());
    }
    
    // ---------------------------------------------------------------------------------------------

    /**
     * @param int $id
     * @return DimensiuneItem|null
     * @throws Exception
     */
    public function load($id)
    {
        $id = (int)$id;
        if (!$id) {
            throw new Exception('ID dimensiune este invalid');
        }
        $filters = new DimensiuneFilters(array('id'=>$id));
        $data = $this->fetchAll($filters);
        $item = isset($data['items']) && isset($data['items'][0]) ? $data['items'][0] : null;
        return $item;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param DimensiuneFilters $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchAll(DimensiuneFilters $filters = null, $limit = null, $offset = null)
    {
        // Set where and having conditions.
        $sqlWhere = array();
        $sqlHaving = array();
        if ($filters) {
            if ($filters->getId()) {
                $sqlWhere[] = "dim.id = {$filters->getId()}";
            }
            if ($filters->getExcludedId()) {
                $sqlWhere[] = "dim.id <> {$filters->getExcludedId()}";
            }
            if ($filters->getLatime()) {
                $sqlWhere[] = "dim.latime = {$filters->getLatime()}";
            }
            if ($filters->getInaltime()) {
                $sqlWhere[] = "dim.inaltime = {$filters->getInaltime()}";
            }
            if ($filters->getSearchTerm()) {
                // Clean search term, extract words and set where condition.
                $searchTerm = Strings::sanitizeForDatabaseUsage($filters->getSearchTerm());
                $words = Strings::splitIntoWords($searchTerm);
                if (count($words)) {
                    $subCond = array();
                    foreach ($words as $word) {
                        $word = $this->db->escape("%$word%");
                        $subCond[] = "(dim.id LIKE $word OR dim.latime LIKE $word OR dim.inaltime LIKE $word)";
                    }
                    $sqlWhere[] = implode("\n\tAND ", $subCond);
                }
            }
        }
        $sqlWhere = implode("\n\tAND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sqlHaving = implode("\n\tAND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? "HAVING " . $sqlHaving : '';

        // ------------------------------------

        // Set order by.
        $sqlOrder = array();
        if ($filters && count($filters->getOrderBy())) {
            foreach($filters->getOrderBy() as $ord) {
                switch($ord) {
                    case $filters::ORDER_BY_ID_ASC:
                        $sqlOrder[] = "id ASC";
                        break;
                    case $filters::ORDER_BY_ID_DESC:
                        $sqlOrder[] = "id DESC";
                        break;
                    case $filters::ORDER_BY_LATIME_ASC:
                        $sqlOrder[] = "latime ASC";
                        break;
                    case $filters::ORDER_BY_LATIME_DESC:
                        $sqlOrder[] = "latime DESC";
                        break;
                    case $filters::ORDER_BY_INALTIME_ASC:
                        $sqlOrder[] = "inaltime ASC";
                        break;
                    case $filters::ORDER_BY_INALTIME_DESC:
                        $sqlOrder[] = "inaltime DESC";
                        break;
                }
            }
        }
        if (!count($sqlOrder)) {
            $sqlOrder[] = "id ASC";
        }
        $sqlOrder = implode(", ", $sqlOrder);

        // ------------------------------------

        // Set limit.
        $sqlLimit = '';
        if (!is_null($offset) && !is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$offset . ', ' . (int)$limit;
        } elseif (!is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$limit;
        }

        // ------------------------------------

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    dim.*
FROM
    fcp_biblioteca_carti_dimensiuni AS dim
WHERE
    1
    $sqlWhere
GROUP BY dim.id
$sqlHaving
ORDER BY $sqlOrder
$sqlLimit
EOSQL;

        /** @var CI_DB_result $result */
        $result = $this->db->query($sql);
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        $items = array();
        foreach($result->result('array') as $row){
            $items[] = new DimensiuneItem($row);
        }

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------
    
    /**
     * @param DimensiuneFilters|null $filters
     * @return array
     */
    public function fetchForSelect(DimensiuneFilters $filters = null)
    {
        $result = $this->fetchAll($filters);
        $data = array();
        if (isset($result['items']) && count($result['items'])) {
            /** @var DimensiuneItem $dim */
            foreach($result['items'] as $dim) {
                $data[$dim->getId()] = $dim->getName();
            }
        }
        return $data;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param string $searchTerm
     * @param int $limit
     * @return array|DimensiuneItem[]
     */
    public function fetchForAutocomplete($searchTerm, $limit=LIMIT_FOR_AUTOCOMPLETE)
    {
        $filters = new DimensiuneFilters(array('searchTerm'=>$searchTerm));
        $data = $this->fetchAll($filters, $limit);
        return isset($data['items']) ? $data['items'] : array();
    }
    
    // ---------------------------------------------------------------------------------------------

    /**
     * @param DimensiuneFilters|null $filters
     * @param null $limit
     * @param null $offset
     * @param $tCount - return FOUND_ROWS
     * @return array
     */
    public function fetchForApi(DimensiuneFilters $filters = null, $limit = null, $offset = null, &$tCount)
    {
        $data = $this->fetchAll($filters, $limit, $offset);

        $return = [];
        if (isset($data['items']) && count($data['items'])) {
            /** @var DimensiuneItem $item */
            foreach($data['items'] as $item) {
                $return[] = $item->toArray();
            }
            $tCount = $data['count'];
        }

        return $return;
    }
    
}