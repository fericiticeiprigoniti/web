<?php

/**
 * Class CarteTable
 * @table fcp_biblioteca_carti
 */
class CarteTable extends CI_Model
{

    /**
     * @var CarteTable
     */
    private static $instance;

    /**
     * Used for caching items
     * @var TagItem[]
     */
    public static $collection = array();


    /**
     * Singleton
     * CarteTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    // ---------------------------------------------------------------------------------------------

    /**
     *  we can later dependency inject and thus unit test this
     * @return CarteTable
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param int $id
     */
    public static function resetCollectionId($id)
    {
        if (isset(self::$collection[$id])) {
            unset(self::$collection[$id]);
        }
    }


    /**
     * @param $id
     * @return CarteItem|null
     * @throws Exception
     */
    public function load($id): ?CarteItem
    {
        $id = (int)$id;
        if (!$id) {
            throw new Exception('ID carte este invalid');
        }
        $filters = new CarteFilters(array('id' => $id));
        $data = $this->fetchAll($filters);

        return $data['items'][0] ?? null;
    }

    /**
     * @param int $id
     */
    public static function get($id): ?CarteItem
    {
        if (!$id) {
            return null;
        }

        try {
            return self::getInstance()->load($id);
        } catch (Throwable $e) {
            log_message('error', $e->getMessage());
            return null;
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param CarteFilters $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchAll(CarteFilters $filters = null, $limit = null, $offset = null)
    {

        // Set where and having conditions.
        $sqlWhere = array();
        $sqlHaving = array();
        if ($filters) {
            if ($filters->getId()) {
                $sqlWhere[] = "carte.id = {$filters->getId()}";
            }
            if ($filters->getExcludedId()) {
                $sqlWhere[] = "carte.id <> {$filters->getExcludedId()}";
            }
            if ($filters->getTitlu()) {
                $sqlWhere[] = "carte.titlu LIKE " . $this->db->escape("%{$filters->getTitlu()}%");
            }
            if ($filters->getSubtitlu()) {
                $sqlWhere[] = "carte.subtitlu LIKE " . $this->db->escape("%{$filters->getSubtitlu()}%");
            }
            if ($filters->getCategorieId()) {
                $sqlWhere[] = "carte.cat_id = {$filters->getCategorieId()}";
            }
            if ($filters->getParentId()) {
                $sqlWhere[] = "carte.parent_id = {$filters->getParentId()}";
            }
            if ($filters->getAutorId()) {
                $sqlWhere[] = "autor.id = {$filters->getAutorId()}";
            }
            if ($filters->getLocalitateId()) {
                $sqlWhere[] = "carte.localitate_id = {$filters->getLocalitateId()}";
            }
            if ($filters->getVersiune()) {
                $sqlWhere[] = "carte.versiune = {$filters->getVersiune()}";
            }
            if ($filters->getEditia()) {
                $sqlWhere[] = "carte.editia = {$filters->getEditia()}";
            }
            if ($filters->getEdituraId()) {
                $sqlWhere[] = "carte.editura_id = {$filters->getEdituraId()}";
            }
            if ($filters->getColectieId()) {
                $sqlWhere[] = "carte.colectie_id = {$filters->getColectieId()}";
            }
            if ($filters->getAnulPublicatiei()) {
                $sqlWhere[] = "carte.anul_publicatiei = {$filters->getAnulPublicatiei()}";
            }
            if ($filters->getNrPaginiMin()) {
                $sqlWhere[] = "carte.nr_pagini >= {$filters->getNrPaginiMin()}";
            }
            if ($filters->getNrPaginiMax()) {
                $sqlWhere[] = "carte.nr_pagini <= {$filters->getNrPaginiMax()}";
            }
            if ($filters->getDimensiuneId()) {
                $sqlWhere[] = "carte.dimensiune_id = {$filters->getDimensiuneId()}";
            }
            if ($filters->getObservatii()) {
                $sqlWhere[] = "carte.observatii LIKE " . $this->db->escape("%{$filters->getObservatii()}%");
            }
            if ($filters->getObservatiiExterne()) {
                $sqlWhere[] = "carte.observatii_externe LIKE " . $this->db->escape("%{$filters->getObservatiiExterne()}%");
            }
            if ($filters->getFormat()) {
                $sqlWhere[] = "carte.disponibil_in_format = {$filters->getFormat()}";
            }
            if (!is_null($filters->areCoperta())) {
                if ($filters->areCoperta()) {
                    $sqlWhere[] = "carte.coperta IS NOT NULL AND carte.coperta <> ''";
                } else {
                    $sqlWhere[] = "(carte.coperta IS NULL OR carte.coperta = '')";
                }
            }
            if (!is_null($filters->isPublished())) {
                if ($filters->isPublished()) {
                    $sqlWhere[] = "carte.is_published > 0";
                } else {
                    $sqlWhere[] = "(carte.is_published IS NULL OR carte.is_published = 0)";
                }
            }
            if (!is_null($filters->isDeleted())) {
                if ($filters->isDeleted()) {
                    $sqlWhere[] = "carte.is_deleted > 0";
                } else {
                    $sqlWhere[] = "(carte.is_deleted IS NULL OR carte.is_deleted = 0)";
                }
            }

            if ($filters->getSearchTerm()) {
                // Clean search term, extract words and set where condition.
                $searchTerm = Strings::sanitizeForDatabaseUsage($filters->getSearchTerm());
                $words = Strings::splitIntoWords($searchTerm);
                if (count($words)) {
                    $subCond = array();
                    foreach ($words as $word) {
                        $word = $this->db->escape("%$word%");
                        $subCond[] = "(carte.id LIKE $word OR carte.titlu LIKE $word OR carte.subtitlu LIKE $word OR autor.nume LIKE $word)";
                    }
                    $sqlWhere[] = implode("\n\tAND ", $subCond);
                }
            }
        }
        $sqlWhere = implode("\n\tAND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sqlHaving = implode("\n\tAND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? "HAVING " . $sqlHaving : '';

        // ------------------------------------

        // Set order by.
        $sqlOrder = array();
        if ($filters && count($filters->getOrderBy())) {
            foreach ($filters->getOrderBy() as $ord) {
                switch ($ord) {
                    case $filters::ORDER_BY_ID_ASC:
                        $sqlOrder[] = "id ASC";
                        break;
                    case $filters::ORDER_BY_ID_DESC:
                        $sqlOrder[] = "id DESC";
                        break;
                    case $filters::ORDER_BY_TITLU_ASC:
                        $sqlOrder[] = "titlu ASC";
                        break;
                    case $filters::ORDER_BY_TITLU_DESC:
                        $sqlOrder[] = "titlu DESC";
                        break;
                    case $filters::ORDER_BY_SUBTITLU_ASC:
                        $sqlOrder[] = "subtitlu ASC";
                        break;
                    case $filters::ORDER_BY_SUBTITLU_DESC:
                        $sqlOrder[] = "subtitlu DESC";
                        break;
                    case $filters::ORDER_BY_AUTORI_ASC:
                        $sqlOrder[] = "nume_autori ASC";
                        break;
                    case $filters::ORDER_BY_AUTORI_DESC:
                        $sqlOrder[] = "nume_autori DESC";
                        break;
                    case $filters::ORDER_BY_CATEGORIE_ASC:
                        $sqlOrder[] = "nume_categorie ASC";
                        break;
                    case $filters::ORDER_BY_CATEGORIE_DESC:
                        $sqlOrder[] = "nume_categorie DESC";
                        break;
                    case $filters::ORDER_BY_EDITURA_ASC:
                        $sqlOrder[] = "nume_editura ASC";
                        break;
                    case $filters::ORDER_BY_EDITURA_DESC:
                        $sqlOrder[] = "nume_editura DESC";
                        break;
                    case $filters::ORDER_BY_ANUL_PUBLICATIEI_ASC:
                        $sqlOrder[] = "anul_publicatiei ASC";
                        break;
                    case $filters::ORDER_BY_ANUL_PUBLICATIEI_DESC:
                        $sqlOrder[] = "anul_publicatiei DESC";
                        break;
                    case $filters::ORDER_BY_FORMAT_ASC:
                        $sqlOrder[] = "disponibil_in_format ASC";
                        break;
                    case $filters::ORDER_BY_FORMAT_DESC:
                        $sqlOrder[] = "disponibil_in_format DESC";
                        break;
                }
            }
        }
        if (!count($sqlOrder)) {
            $sqlOrder[] = "id ASC";
        }
        $sqlOrder = implode(", ", $sqlOrder);

        // ------------------------------------

        // Set limit.
        $sqlLimit = '';
        if (!is_null($offset) && !is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$offset . ', ' . (int)$limit;
        } elseif (!is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$limit;
        }

        // ------------------------------------

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    carte.*
    , categ.nume AS nume_categorie
    , editura.nume AS nume_editura
    , GROUP_CONCAT(autor.nume SEPARATOR ', ') AS nume_autori

FROM
    fcp_biblioteca_carti AS carte
    LEFT JOIN fcp_biblioteca_carti_categorii AS categ ON carte.cat_id = categ.id
    LEFT JOIN fcp_biblioteca_edituri AS editura ON editura.id = carte.editura_id
    LEFT JOIN fcp_biblioteca_autori2carti AS a2c ON a2c.carte_id = carte.id
    LEFT JOIN fcp_autori AS autor ON autor.id = a2c.autor_id
WHERE
    (a2c.rol_id IS NULL OR a2c.rol_id IN (1,2,3))
    $sqlWhere
GROUP BY carte.id
$sqlHaving
ORDER BY $sqlOrder
$sqlLimit
EOSQL;

        /** @var CI_DB_result $result */
        $result = $this->db->query($sql);
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        $items = array();
        foreach ($result->result('array') as $row) {
            $items[] = new CarteItem($row);
        }

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param CarteItem $item
     * @return bool|int
     */
    public function save(CarteItem $item)
    {
        // Set data.
        $eData = array(
            'cat_id' => $item->getCategorieId() ? $item->getCategorieId() : null,
            'parent_id' => $item->getParentId() ? $item->getParentId() : null,
            'versiune' => $item->getVersiune() ? $item->getVersiune() : null,
            'titlu' => $item->getTitlu() ? $item->getTitlu() : null,
            'subtitlu' => $item->getSubtitlu() ? $item->getSubtitlu() : null,
            'localitate_id' => $item->getLocalitateId() ? $item->getLocalitateId() : null,
            'oras' => $item->getOras() ? $item->getOras() : null,
            'editia' => $item->getEditia() ? $item->getEditia() : null,
            'editura_id' => $item->getEdituraId() ? $item->getEdituraId() : null,
            'editura2_id' => $item->getEditura2Id() ? $item->getEditura2Id() : null,
            'colectie_id' => $item->getColectieId() ? $item->getColectieId() : null,
            'anul_publicatiei' => $item->getAnulPublicatiei() ? $item->getAnulPublicatiei() : null,
            'nr_pagini' => $item->getNrPagini() ? $item->getNrPagini() : null,
            'nr_anexe' => $item->getNrAnexe() ? $item->getNrAnexe() : null,
            'dimensiune_id' => $item->getDimensiuneId() ? $item->getDimensiuneId() : null,
            'coperta' => $item->getCoperta() ? $item->getCoperta() : null,
            'introducere_art_id' => $item->getIntroducereArticolId() ? $item->getIntroducereArticolId() : null,
            'rezumat_art_id' => $item->getRezumatArticolId() ? $item->getRezumatArticolId() : null,
            'recenzie_art_id' => $item->getRecenzieArticolId() ? $item->getRecenzieArticolId() : null,
            'observatii' => $item->getObservatii() ? $item->getObservatii() : null,
            'disponibil_in_format' => $item->getFormat() ? $item->getFormat() : null,
            'is_published' => $item->isPublished() ? $item->isPublished() : null,
            'is_deleted' => $item->isDeleted() ? $item->isDeleted() : null,
            'observatii_externe' => $item->getObservatiiExterne() ? $item->getObservatiiExterne() : null,
        );

        // Insert/Update.
        if (!$item->getId()) {
            $res = $this->db->insert('fcp_biblioteca_carti', $eData);
        } else {
            $res = $this->db->update('fcp_biblioteca_carti', $eData, 'id = ' . $item->getId());
        }

        // Remove old item from cache
        self::resetCollectionId($item->getId());

        return $res;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param CarteItem $item
     * @return mixed
     */
    public function delete(CarteItem $item)
    {
        // Remove old item from cache
        self::resetCollectionId($item->getId());

        return $this->db->delete('fcp_biblioteca_carti', 'id = ' . $item->getId());
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param string $searchTerm
     * @param int $limit
     * @return array|CarteItem[]
     */
    public function fetchForAutocomplete($searchTerm, $limit = LIMIT_FOR_AUTOCOMPLETE)
    {
        $filters = new CarteFilters(array('searchTerm' => $searchTerm));
        $data = $this->fetchAll($filters, $limit);
        return isset($data['items']) ? $data['items'] : array();
    }

    // ---------------------------------------------------------------------------------------------

    public function fetchForSelect(CarteFilters $filters = null)
    {
        $result = $this->fetchAll($filters);
        $data = array();
        if (isset($result['items']) && count($result['items'])) {
            /** @var CarteItem $carte */
            foreach ($result['items'] as $carte) {
                $data[$carte->getId()] = generateSource($carte->getAutori(), $carte->getTitlu(), $carte->getEditia(), $carte->getEdituraNume());
            }
        }

        return $data;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param CarteFilters|null $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchForApi(CarteFilters $filters = null, $limit = null, $offset = null, &$tCount)
    {
        $data = $this->fetchAll($filters, $limit, $offset);
        $return = [];
        if (isset($data['items']) && count($data['items'])) {
            /** @var CarteItem $item */
            foreach ($data['items'] as $item) {
                $results = $item->toArray();
                $results['categorieInfo'] = $item->getCategorie()->toArray();

                if ($autori = $item->getAutoriObj()) {
                    foreach ($autori as $autor) {
                        $results['autori'][] = $autor->toArray();
                    }
                }

                if ($localitate = $item->getLocalitate())
                    $results['localitateInfo'] = $localitate->toArray();

                if ($editura_1 = $item->getEditura())
                    $results['edituri'][] = $editura_1->toArray();

                if ($editura_2 = $item->getEditura())
                    $results['edituri'][] = $editura_2->toArray();

                if ($colectie = $item->getColectie())
                    $results['colectieInfo'] = $colectie->toArray();

                if ($dimensiune = $item->getDimensiune())
                    $results['dimensiuneInfo'] = $dimensiune->toArray();

                $return[] = $results;
            }
            $tCount = $data['count'];
        }
        return $return;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param int $id
     * @return CarteAutor|null
     * @throws Exception
     */
    public function loadCarteAutor($id)
    {
        $id = (int)$id;
        if (!$id) {
            throw new Exception('ID carte-autor este invalid');
        }
        $data = $this->fetchAllAutori(array('id' => $id));
        $item = isset($data['items']) && isset($data['items'][0]) ? $data['items'][0] : null;
        return $item;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param array $filters
     * @return array
     */
    public function fetchAllAutori(array $filters = array())
    {
        // Set where and having conditions.
        $sqlWhere = array();
        if (count($filters)) {
            if (isset($filters['id']) && !empty($filters['id'])) {
                $sqlWhere[] = "a2c.id = " . (int)$filters['id'];
            }
            if (isset($filters['carte_id']) && !empty($filters['carte_id'])) {
                $sqlWhere[] = "a2c.carte_id = " . (int)$filters['carte_id'];
            }
        }
        $sqlWhere = implode("\n\tAND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        // ------------------------------------

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    a2c.*
FROM
    fcp_biblioteca_autori2carti AS a2c
WHERE
    1
    $sqlWhere
ORDER BY
    a2c.id ASC
EOSQL;

        /** @var CI_DB_result $result */
        $result = $this->db->query($sql);
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        $items = array();
        foreach ($result->result('array') as $row) {
            $items[] = new CarteAutor($row);
        }

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------


}
