<?php


class AutorItem
{

    use ArrayOrJson;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $nume;

    /**
     * @var string
     */
    private $prenume;

    /**
     * @var string
     */
    private $alias;
    
    /**
     * @var int
     */
    private $biografieArticolId;
    
    /**
     * @var ArticolItem
     */
    private $biografieArticol;

    /**
     * @var string
     */
    private $backupBiografie;

    /**
     * @var string
     */
    private $pozaPath;

    /**
     * @var string
     */
    private $poza;

    /**
     * @var string
     */
    private $pozaThumb;

    /**
     * @var bool
     */
    private $isDeleted;

    // ---------------------------------------------------------------------------------------------

    public function __construct(array $dbRow = array())
    {
        if (!count($dbRow)) {
            return;
        }
        $this->id = (int)$dbRow['id'];
        $this->nume = $dbRow['nume'];
        $this->prenume = $dbRow['prenume'];
        $this->alias = $dbRow['alias'];
        $this->biografieArticolId = $dbRow['biografie_art_id'];
        $this->backupBiografie = $dbRow['backup_biografie'];
        $this->pozaPath = $dbRow['poza_path'];
        $this->poza = $dbRow['poza'];
        $this->pozaThumb = $dbRow['poza_thumb'];
        $this->isDeleted = (bool)$dbRow['is_deleted'];
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getNume()
    {
        return $this->nume;
    }

    /**
     * @param string|null $nume
     * @throws Exception
     */
    public function setNume($nume)
    {
        $this->nume = trim(strip_tags($nume));
        if (empty($this->nume)) {
            throw new Exception('Completati numele autorului');
        }
    }

    /**
     * @return string|null
     */
    public function getPrenume()
    {
        return $this->prenume;
    }

    /**
     * @param string|null $prenume
     * @throws Exception
     */
    public function setPrenume($prenume)
    {
        $this->prenume = $prenume ? trim(strip_tags($prenume)) : null;
    }

    /**
     * @return string|null
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param string|null $alias
     * @throws Exception
     */
    public function setAlias($alias)
    {
        $this->alias = $alias ? trim(strip_tags($alias)) : null;
    }

    // ---------------------------------------------------------------------------------------------
    
    /**
     * @return int|null
     */
    public function getBiografieArticolId()
    {
        return $this->biografieArticolId;
    }
    
    /**
     * @param int|null $biografieArticolId
     * @throws Exception
     */
    public function setBiografieArticolId($biografieArticolId)
    {
        $this->biografieArticolId = $biografieArticolId ? (int)$biografieArticolId : null;
        if ($this->biografieArticolId) {
            $this->biografieArticol = ArticolTable::getInstance()->load($this->biografieArticolId);
            if (!$this->biografieArticol) {
                throw new Exception('Articolul pentru biografie nu a fost gasit in baza de date');
            }
        }
    }
    
    /**
     * @return ArticolItem|null
     */
    public function getBiografieArticol()
    {
        if ($this->biografieArticol) {
            return $this->biografieArticol;
        } elseif ($this->biografieArticolId) {
            $this->biografieArticol = ArticolTable::getInstance()->load($this->biografieArticolId);
            return $this->biografieArticol;
        } else {
            return null;
        }
    }
    
    /**
     * @return string|null
     */
    public function getBackupBiografie()
    {
        return $this->backupBiografie;
    }

    // ---------------------------------------------------------------------------------------------
    
    /**
     * @return string|null
     */
    public function getPozaPath()
    {
        return $this->pozaPath;
    }
    
    /**
     * @param string|null $pozaPath
     */
    public function setPozaPath($pozaPath)
    {
        $this->pozaPath = is_null($pozaPath) ? null : trim(strip_tags($pozaPath));
    }

    /**
     * @return string|null
     */
    public function getPoza()
    {
        return $this->poza;
    }
    
    /**
     * @param string|null $poza
     */
    public function setPoza($poza)
    {
        $this->poza = is_null($poza) ? null : trim(strip_tags($poza));
    }
    
    /**
     * @return string|null
     */
    public function getPozaThumb()
    {
        return $this->pozaThumb;
    }
    
    /**
     * @param string|null $pozaThumb
     */
    public function setPozaThumb($pozaThumb)
    {
        $this->pozaThumb = is_null($pozaThumb) ? null : trim(strip_tags($pozaThumb));
    }

    /**
     * @return boolean
     */
    public function isDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @var boolean $value
     */
    public function setIsDeleted($value)
    {
        $this->isDeleted = (bool)$value;
    }


    // ---------------------------------------------------------------------------------------------


    /**
     * @return string|null
     */
    public function getFullname()
    {
        return implode(' ', [$this->nume, $this->prenume]);
    }

    // ---------------------------------------------------------------------------------------------
    
    /**
     * @return string
     */
    public function getSearchResult()
    {
        return $this->id . ' | ' . $this->nume . ' | ' . $this->prenume . ' | ' . $this->alias;
    }

}