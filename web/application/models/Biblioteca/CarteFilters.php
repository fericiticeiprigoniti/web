<?php

class CarteFilters
{

    const VALUE_YES = 'yes';
    const VALUE_NO = 'no';

    const ORDER_BY_ID_ASC = 'id_asc';
    const ORDER_BY_ID_DESC = 'id_desc';
    const ORDER_BY_TITLU_ASC = 'titlu_asc';
    const ORDER_BY_TITLU_DESC = 'titlu_desc';
    const ORDER_BY_SUBTITLU_ASC = 'subtitlu_asc';
    const ORDER_BY_SUBTITLU_DESC = 'subtitlu_desc';
    const ORDER_BY_AUTORI_ASC = 'autori_asc';
    const ORDER_BY_AUTORI_DESC = 'autori_desc';
    const ORDER_BY_CATEGORIE_ASC = 'categorie_asc';
    const ORDER_BY_CATEGORIE_DESC = 'categorie_desc';
    const ORDER_BY_EDITURA_ASC = 'editura_asc';
    const ORDER_BY_EDITURA_DESC = 'editura_desc';
    const ORDER_BY_ANUL_PUBLICATIEI_ASC = 'anul_publicatiei_asc';
    const ORDER_BY_ANUL_PUBLICATIEI_DESC = 'anul_publicatiei_desc';
    const ORDER_BY_FORMAT_ASC = 'format_asc';
    const ORDER_BY_FORMAT_DESC = 'format_desc';

    /**
     * @var int
     * @example ID-ul cartii
     */
    private $id;

    /**
     * @var int
     * @example Exclude ID_ul unei carti din response
     */
    private $excludedId;

    /**
     * @var CarteItem
     * @docs_ignore
     */
    private $carte;

    /**
     * @var string
     * @docs_ignore
     */
    private $titlu;

    /**
     * @var string
     * @docs_ignore
     */
    private $subtitlu;

    /**
     * @var int
     * @example ID-ul categoriei -> ./biblioteca/categoriiCarti
     */
    private $categorieId;

    /**
     * @var CategorieCarteItem
     * @docs_ignore
     */
    private $categorie;

    /**
     * @var int
     */
    private $parentId;

    /**
     * @var CarteItem
     * @docs_ignore
     */
    private $parent;

    /**
     * @var int
     * @example ID-ul autorului -> ./biblioteca/autori
     */
    private $autorId;

    /**
     * @var AutorItem
     * @docs_ignore
     */
    private $autor;

    /**
     * @var int
     * @example ID_ul localitatii
     */
    private $localitateId;

    /**
     * @var LocalitateItem
     * @docs_ignore
     */
    private $localitate;

    /**
     * @var int
     */
    private $versiune;

    /**
     * @var int
     */
    private $editia;

    /**
     * @var int
     * @example ID-ul editurii -> ./biblioteca/edituri
     */
    private $edituraId;

    /**
     * @var EdituraItem
     * @docs_ignore
     */
    private $editura;

    /**
     * @var int
     * @example Colectia din care face parte cartea -> ./biblioteca/colectii
     */
    private $colectieId;

    /**
     * @var ColectieItem
     * @docs_ignore
     */
    private $colectie;

    /**
     * @var int
     * @example Anul cand a fost publicata cartea
     */
    private $anulPublicatiei;

    /**
     * @var int
     */
    private $nrPaginiMin;

    /**
     * @var int
     */
    private $nrPaginiMax;

    /**
     * @var int
     */
    private $dimensiuneId;

    /**
     * @var DimensiuneItem
     * @docs_ignore
     */
    private $dimensiune;

    /**
     * @var string
     * @docs_ignore
     */
    private $observatii;

    /**
     * @var string
     * @docs_ignore
     */
    private $observatiiExterne;

    /**
     * @var int
     */
    private $format;

    /**
     * @var bool|null
     *  @example 1 = are coperta / 0 = fara coperta
     */
    private $areCoperta;

    /**
     * @var bool
     *  @example 1 = publicat / 0 = in lucru
     * @docs_ignore
     */
    private $isPublished;

    /**
     * @var bool
     * @example 1 = inactiv / 0 = activ
     * @docs_ignore
     */
    private $isDeleted;

    /**
    * @var string
    */
    private $searchTerm;

    /**
     * @var array
     * @example Suporta: id_asc / id_desc / titlu_asc / titlu_desc / subtitlu_asc / subtitlu_desc / autori_asc / autori_desc / categorie_asc / categorie_desc / editura_asc / editura_desc /anul_publicatiei_asc /anul_publicatiei_desc / format_asc / format_desc
     */
    private $orderBy = [];

    // ---------------------------------------------------------------------------------------------

    public function __construct(array $getData = array())
    {
        if (isset($getData['id'])) {
            $this->setId($getData['id']);
        }
        if (isset($getData['excludedId'])) {
            $this->setExcludedId($getData['excludedId']);
        }
        if (isset($getData['titlu'])) {
            $this->setTitlu($getData['titlu']);
        }
        if (isset($getData['subtitlu'])) {
            $this->setSubtitlu($getData['subtitlu']);
        }
        if (isset($getData['categorieId'])) {
            $this->setCategorieId($getData['categorieId']);
        }
        if (isset($getData['parentId'])) {
            $this->setParentId($getData['parentId']);
        }
        if (isset($getData['autorId'])) {
            $this->setAutorId($getData['autorId']);
        }
        if (isset($getData['localitateId'])) {
            $this->setLocalitateId($getData['localitateId']);
        }
        if (isset($getData['versiune'])) {
            $this->setVersiune($getData['versiune']);
        }
        if (isset($getData['editia'])) {
            $this->setEditia($getData['editia']);
        }
        if (isset($getData['edituraId'])) {
            $this->setEdituraId($getData['edituraId']);
        }
        if (isset($getData['colectieId'])) {
            $this->setColectieId($getData['colectieId']);
        }
        if (isset($getData['anulPublicatiei'])) {
            $this->setAnulPublicatiei($getData['anulPublicatiei']);
        }
        if (isset($getData['nrPaginiMin'])) {
            $this->setNrPaginiMin($getData['nrPaginiMin']);
        }
        if (isset($getData['nrPaginiMax'])) {
            $this->setNrPaginiMax($getData['nrPaginiMax']);
        }
        if (isset($getData['dimensiuneId'])) {
            $this->setDimensiuneId($getData['dimensiuneId']);
        }
        if (isset($getData['observatii'])) {
            $this->setObservatii($getData['observatii']);
        }
        if (isset($getData['observatiiExterne'])) {
            $this->setObservatiiExterne($getData['observatii_externe']);
        }
        if (isset($getData['format'])) {
            $this->setFormat($getData['format']);
        }
        if (isset($getData['areCoperta'])) {
            $this->setAreCoperta($getData['areCoperta']);
        }
        if (isset($getData['isPublished'])) {
            $this->setIsPublished($getData['isPublished']);
        }
        if (isset($getData['isDeleted'])) {
            $this->setIsDeleted($getData['isDeleted']);
        }

        if (isset($getData['searchTerm'])) {
            $this->setSearchTerm($getData['searchTerm']);
        }

        if (isset($getData['_orderBy'])) {
            $this->setOrderBy($getData['_orderBy']);
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return int|null
     */
    public function getExcludedId()
    {
        return $this->excludedId;
    }

    /**
     * @param int|null $excludedId
     */
    public function setExcludedId($excludedId)
    {
        $this->excludedId = $excludedId ? (int)$excludedId : null;
    }

    /**
     * @return CarteItem|null
     */
    public function getCarte()
    {
        if ($this->carte) {
            return $this->carte;
        } elseif ($this->id) {
            $this->carte = CarteTable::getInstance()->load($this->id);
            return $this->carte;
        } else {
            return null;
        }
    }

    /**
     * @return string|null
     */
    public function getTitlu()
    {
        return $this->titlu;
    }

    /**
     * @param string|null $titlu
     */
    public function setTitlu($titlu)
    {
        $this->titlu = is_null($titlu) ? null : trim(strip_tags($titlu));
    }

    /**
     * @return string|null
     */
    public function getSubtitlu()
    {
        return $this->subtitlu;
    }

    /**
     * @param string|null $subtitlu
     */
    public function setSubtitlu($subtitlu)
    {
        $this->subtitlu = is_null($subtitlu) ? null : trim(strip_tags($subtitlu));
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getCategorieId()
    {
        return $this->categorieId;
    }

    /**
     * @param int|null $categorieId
     */
    public function setCategorieId($categorieId)
    {
        $this->categorieId = $categorieId ? (int)$categorieId : null;
    }

    /**
     * @return CategorieCarteItem|null
     */
    public function getCategorie()
    {
        if ($this->categorie) {
            return $this->categorie;
        } elseif ($this->categorieId) {
            $this->categorie = CategorieCarteTable::getInstance()->load($this->categorieId);
            return $this->categorie;
        } else {
            return null;
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param int|null $parentId
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId ? (int)$parentId : null;
    }

    /**
     * @return CarteItem|null
     */
    public function getParent()
    {
        if ($this->parent) {
            return $this->parent;
        } elseif ($this->parentId) {
            $this->parent = CarteTable::getInstance()->load($this->parentId);
            return $this->parent;
        } else {
            return null;
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getAutorId()
    {
        return $this->autorId;
    }

    /**
     * @param int|null $autorId
     */
    public function setAutorId($autorId)
    {
        $this->autorId = $autorId ? (int)$autorId : null;
    }

    /**
     * @return AutorItem|null
     */
    public function getAutor()
    {
        if ($this->autor) {
            return $this->autor;
        } elseif ($this->autorId) {
            $this->autor = AutorTable::getInstance()->load($this->autorId);
            return $this->autor;
        } else {
            return null;
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getLocalitateId()
    {
        return $this->localitateId;
    }

    /**
     * @param int|null $localitateId
     */
    public function setLocalitateId($localitateId)
    {
        $this->localitateId = $localitateId ? (int)$localitateId : null;
    }

    /**
     * @return LocalitateItem|null
     */
    public function getLocalitate()
    {
        if ($this->localitate) {
            return $this->localitate;
        } elseif ($this->localitateId) {
            $this->localitate = LocalitateTable::getInstance()->load($this->localitateId);
            return $this->localitate;
        } else {
            return null;
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getVersiune()
    {
        return $this->versiune;
    }

    /**
     * @param int|null $versiune
     */
    public function setVersiune($versiune)
    {
        $this->versiune = $versiune ? (int)$versiune : null;
    }

    /**
     * @return int|null
     */
    public function getEditia()
    {
        return $this->editia;
    }

    /**
     * @param int|null $editia
     */
    public function setEditia($editia)
    {
        $this->editia = $editia ? (int)$editia : null;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getEdituraId()
    {
        return $this->edituraId;
    }

    /**
     * @param int|null $edituraId
     */
    public function setEdituraId($edituraId)
    {
        $this->edituraId = $edituraId ? (int)$edituraId : null;
    }

    /**
     * @return EdituraItem|null
     */
    public function getEditura()
    {
        if ($this->editura) {
            return $this->editura;
        } elseif ($this->edituraId) {
            $this->editura = EdituraTable::getInstance()->load($this->edituraId);
            return $this->editura;
        } else {
            return null;
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getColectieId()
    {
        return $this->colectieId;
    }

    /**
     * @param int|null $colectieId
     */
    public function setColectieId($colectieId)
    {
        $this->colectieId = $colectieId ? (int)$colectieId : null;
    }

    /**
     * @return ColectieItem|null
     */
    public function getColectie()
    {
        if ($this->colectie) {
            return $this->colectie;
        } elseif ($this->colectieId) {
            $this->colectie = ColectieTable::getInstance()->load($this->colectieId);
            return $this->colectie;
        } else {
            return null;
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getAnulPublicatiei()
    {
        return $this->anulPublicatiei;
    }

    /**
     * @param int|null $anulPublicatiei
     */
    public function setAnulPublicatiei($anulPublicatiei)
    {
        $this->anulPublicatiei = $anulPublicatiei ? (int)$anulPublicatiei : null;
    }

    /**
     * @return int|null
     */
    public function getNrPaginiMin()
    {
        return $this->nrPaginiMin;
    }

    /**
     * @param int|null $nrPaginiMin
     */
    public function setNrPaginiMin($nrPaginiMin)
    {
        $this->nrPaginiMin = $nrPaginiMin ? (int)$nrPaginiMin : null;
    }

    /**
     * @return int|null
     */
    public function getNrPaginiMax()
    {
        return $this->nrPaginiMax;
    }

    /**
     * @param int|null $nrPaginiMax
     */
    public function setNrPaginiMax($nrPaginiMax)
    {
        $this->nrPaginiMax = $nrPaginiMax ? (int)$nrPaginiMax : null;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getDimensiuneId()
    {
        return $this->dimensiuneId;
    }

    /**
     * @param int|null $dimensiuneId
     */
    public function setDimensiuneId($dimensiuneId)
    {
        $this->dimensiuneId = $dimensiuneId ? (int)$dimensiuneId : null;
    }

    /**
     * @return DimensiuneItem|null
     */
    public function getDimensiune()
    {
        if ($this->dimensiune) {
            return $this->dimensiune;
        } elseif ($this->dimensiuneId) {
            $this->dimensiune = DimensiuneTable::getInstance()->load($this->dimensiuneId);
            return $this->dimensiune;
        } else {
            return null;
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return string|null
     */
    public function getObservatii()
    {
        return $this->observatii;
    }

    /**
     * @param string|null $observatii
     */
    public function setObservatii($observatii)
    {
        $this->observatii = is_null($observatii) ? null : trim(strip_tags($observatii));
    }


    /**
     * @return string|null
     */
    public function getObservatiiExterne()
    {
        return $this->observatiiExterne;
    }
    
    /**
     * @param string|null $observatii
     */
    public function setObservatiiExterne($observatiiExterne)
    {
        $this->observatiiExterne = is_null($observatiiExterne) ? null : trim(strip_tags($observatiiExterne));
    }

    /**
     * @return int|null
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param int|null $format
     * @throws Exception
     */
    public function setFormat($format)
    {
        $this->format = $format ? (int)$format : null;
        if ($this->format) {
            $formatTypes = CarteItem::fetchFormatTypes();
            if (!isset($formatTypes[$this->format])) {
                throw new Exception('Formatul selectat este invalid');
            }
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return bool|null
     */
    public function areCoperta()
    {
        return $this->areCoperta;
    }

    /**
     * @return bool|null
     */
    public function nuAreCoperta()
    {
        return !is_null($this->areCoperta) && !$this->areCoperta;
    }

    /**
     * @param bool|null $areCoperta
     */
    public function setAreCoperta($areCoperta)
    {
        $this->areCoperta = is_null($areCoperta) ? null : (bool)$areCoperta;
    }

    /**
     * @return bool|null
     */
    public function isPublished()
    {
        return $this->isPublished;
    }

    /**
     * @return bool|null
     */
    public function isNotPublished()
    {
        return !is_null($this->isPublished) && !$this->isPublished;
    }

    /**
     * @param bool|null $isPublished
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = is_null($isPublished) ? null : (bool)$isPublished;
    }

    /**
     * @return bool|null
     */
    public function isDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @return bool|null
     */
    public function isNotDeleted()
    {
        return !is_null($this->isDeleted) && !$this->isDeleted;
    }

    /**
     * @param bool|null $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = is_null($isDeleted) ? null : (bool)$isDeleted;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return string|null
     */
    public function getSearchTerm()
    {
        return $this->searchTerm;
    }

    /**
     * @param string|null $val
     */
    public function setSearchTerm($val)
    {
        $this->searchTerm = is_null($val) ? null : trim(strip_tags($val));
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return array
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param array $orderBy
     */
    public function setOrderBy(array $orderBy)
    {
        $orderBy = (is_array($orderBy) && count($orderBy) ? $orderBy : array());
        if (count($orderBy)) {
            $orderItems = array_keys(self::fetchOrderItems());
            foreach ($orderBy as $k=>$v) {
                if (!in_array($v, $orderItems)) {
                    unset($orderBy[$k]);
                }
            }
        }
        $this->orderBy = $orderBy;
    }

    /**
     * @return array
     */
    public static function fetchOrderItems()
    {
        return array(
            self::ORDER_BY_ID_ASC => 'ID - ASC',
            self::ORDER_BY_ID_DESC => 'ID - DESC',
            self::ORDER_BY_TITLU_ASC => 'Titlu - ASC',
            self::ORDER_BY_TITLU_DESC => 'Titlu - DESC',
            self::ORDER_BY_SUBTITLU_ASC => 'Subtitlu - ASC',
            self::ORDER_BY_SUBTITLU_DESC => 'Subtitlu - DESC',
            self::ORDER_BY_AUTORI_ASC => 'Autori - ASC',
            self::ORDER_BY_AUTORI_DESC => 'Autori - DESC',
            self::ORDER_BY_CATEGORIE_ASC => 'Categorie - ASC',
            self::ORDER_BY_CATEGORIE_DESC => 'Categorie - DESC',
            self::ORDER_BY_EDITURA_ASC => 'Editura - ASC',
            self::ORDER_BY_EDITURA_DESC => 'Editura - DESC',
            self::ORDER_BY_ANUL_PUBLICATIEI_ASC => 'Anul publicatiei - ASC',
            self::ORDER_BY_ANUL_PUBLICATIEI_DESC => 'Anul publicatiei - DESC',
            self::ORDER_BY_FORMAT_ASC => 'Format - ASC',
            self::ORDER_BY_FORMAT_DESC => 'Format - DESC',
        );
    }

}