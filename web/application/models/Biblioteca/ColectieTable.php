<?php

/**
 * Class ColectieTable
 * @table fcp_biblioteca_colectii
 */
class ColectieTable extends CI_Model
{

    /**
     * @var ColectieTable
     */
    private static $instance;


    /**
     * Singleton
     * ColectieTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    // ---------------------------------------------------------------------------------------------

    /**
     *  we can later dependency inject and thus unit test this
     * @return ColectieTable
     */
    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param int $id
     * @return ColectieItem|null
     * @throws Exception
     */
    public function load($id)
    {
        $id = (int)$id;
        if (!$id) {
            throw new Exception('ID colectie este invalid');
        }
        $filters = new ColectieFilters(array('id'=>$id));
        $data = $this->fetchAll($filters);
        $item = isset($data['items']) && isset($data['items'][0]) ? $data['items'][0] : null;
        return $item;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param ColectieFilters $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchAll(ColectieFilters $filters = null, $limit = null, $offset = null)
    {
        // Set where and having conditions.
        $sqlWhere = array();
        $sqlHaving = array();
        if ($filters) {
            if ($filters->getId()) {
                $sqlWhere[] = "colectie.id = {$filters->getId()}";
            }
            if ($filters->getExcludedId()) {
                $sqlWhere[] = "colectie.id <> {$filters->getExcludedId()}";
            }
            if ($filters->getTitlu()) {
                $sqlWhere[] = "colectie.titlu LIKE " . $this->db->escape($filters->getTitlu());
            }
            if ($filters->getEdituraId()) {
                $sqlWhere[] = "colectie.editura_id = {$filters->getEdituraId()}";
            }
            if ($filters->getSearchTerm()) {
                // Clean search term, extract words and set where condition.
                $searchTerm = Strings::sanitizeForDatabaseUsage($filters->getSearchTerm());
                $words = Strings::splitIntoWords($searchTerm);
                if (count($words)) {
                    $subCond = array();
                    foreach ($words as $word) {
                        $word = $this->db->escape("%$word%");
                        $subCond[] = "(colectie.id LIKE $word OR colectie.titlu LIKE $word)";
                    }
                    $sqlWhere[] = implode("\n\tAND ", $subCond);
                }
            }
        }
        $sqlWhere = implode("\n\tAND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sqlHaving = implode("\n\tAND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? "HAVING " . $sqlHaving : '';

        // ------------------------------------

        // Set order by.
        $sqlOrder = array();
        if (count($filters->getOrderBy())) {
            foreach($filters->getOrderBy() as $ord) {
                switch($ord) {
                    case $filters::ORDER_BY_ID_ASC:
                        $sqlOrder[] = "id ASC";
                        break;
                    case $filters::ORDER_BY_ID_DESC:
                        $sqlOrder[] = "id DESC";
                        break;
                    case $filters::ORDER_BY_TITLU_ASC:
                        $sqlOrder[] = "titlu ASC";
                        break;
                    case $filters::ORDER_BY_TITLU_DESC:
                        $sqlOrder[] = "titlu DESC";
                        break;
                }
            }
        }
        if (!count($sqlOrder)) {
            $sqlOrder[] = "id ASC";
        }
        $sqlOrder = implode(", ", $sqlOrder);

        // ------------------------------------

        // Set limit.
        $sqlLimit = '';
        if (!is_null($offset) && !is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$offset . ', ' . (int)$limit;
        } elseif (!is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$limit;
        }

        // ------------------------------------

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    colectie.*
FROM
    fcp_biblioteca_colectii AS colectie
WHERE
    1
    $sqlWhere
GROUP BY colectie.id
$sqlHaving
ORDER BY $sqlOrder
$sqlLimit
EOSQL;

        /** @var CI_DB_result $result */
        $result = $this->db->query($sql);
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        $items = array();
        foreach($result->result('array') as $row){
            $items[] = new ColectieItem($row);
        }

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------
    
    /**
     * @param string $searchTerm
     * @param int $limit
     * @return array|ColectieItem[]
     */
    public function fetchForAutocomplete($searchTerm, $limit=LIMIT_FOR_AUTOCOMPLETE)
    {
        $filters = new ColectieFilters(array('searchTerm'=>$searchTerm));
        $data = $this->fetchAll($filters, $limit);
        return isset($data['items']) ? $data['items'] : array();
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param ColectieFilters|null $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchForApi(ColectieFilters $filters = null, $limit = null, $offset = null, &$tCount)
    {
        $data = $this->fetchAll($filters, $limit, $offset);

        $return = [];
        if (isset($data['items']) && count($data['items'])) {
            /** @var ColectieItem $item */
            foreach($data['items'] as $item) {
                $result = $item->toArray();

                if ($edituraInfo = $item->getEditura())
                    $result['edituraInfo'] = $edituraInfo->toArray();

                $return[] = $result;
            }
            $tCount = $data['count'];
        }

        return $return;
    }
    
}