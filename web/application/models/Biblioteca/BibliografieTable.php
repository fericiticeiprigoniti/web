<?php

/**
 * Class BibliografieTable
 * @table fcp_biblioteca_bibliografii
 */
class BibliografieTable extends CI_Model
{

    /**
     * Used for caching items
     * @var BibliografieItem[]
     */
    public static $collection = [];

    /**
     * @var BibliografieTable
     */
    private static $instance;


    /**
     * Singleton
     * BibliografieTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * We can later dependency inject and thus unit test this
     * @return BibliografieTable
     */
    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @param int $id
     */
    public static function resetCollectionId($id)
    {
        if (isset(self::$collection[$id])) {
            unset(self::$collection[$id]);
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param mixed $id
     * @return BibliografieItem|null
     * @throws Exception
     */
    public function load($id)
    {
        if (!$id) {
            throw new Exception("Invalid Bibliografie id");
        }

        // If data is cached return it
        if (MODEL_CACHING_ENABLED && isset(self::$collection[$id])) {
            return self::$collection[$id];
        }

        // Fetch data
        $filters = new BibliografieFilters(array('id'=>$id));
        $data = $this->fetchAll($filters);

        // Cache result
        self::$collection[$id] = isset($data['items'][0]) ? $data['items'][0] : null;

        // Return cached result
        return self::$collection[$id];
    }

    /**
     * Reset item from static collection and load a new one
     * @param mixed $id
     * @return BibliografieItem|null
     * @throws Exception
     */
    public function reload($id)
    {
        self::resetCollectionId($id);
        return $this->load($id);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param BibliografieFilters $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchAll(BibliografieFilters $filters = null, $limit = null, $offset = null)
    {
        // Set where and having conditions.
        $sqlWhere = array();
        $sqlHaving = array();
        if ($filters) {
            if ($filters->getId()) {
                $sqlWhere[] = "id = {$filters->getId()}";
            }
            if ($filters->getExcludedId()) {
                $sqlWhere[] = "id <> {$filters->getExcludedId()}";
            }
        }

        $sqlWhere = implode("\n\tAND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sqlHaving = implode("\n\tAND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? "HAVING " . $sqlHaving : '';

        // ------------------------------------

        $sqlOrder = array();
        if ($filters && count($filters->getOrderBy())) {
            foreach($filters->getOrderBy() as $ord) {
                switch($ord) {
                    case $filters::ORDER_BY_ID_ASC:
                        $sqlOrder[] = "id ASC";
                        break;
                    case $filters::ORDER_BY_ID_DESC:
                        $sqlOrder[] = "id DESC";
                        break;
        }
            }
        }
        if (!count($sqlOrder)) {
            $sqlOrder[] = "id DESC";
        }
        $sqlOrder = implode(", ", $sqlOrder);

        // ------------------------------------

        // Set limit.
        $sqlLimit = '';
        if (!is_null($offset) && !is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$offset . ', ' . (int)$limit;
        } elseif (!is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$limit;
        }

        // ------------------------------------

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    *
FROM
    fcp_biblioteca_bibliografii
WHERE
    1
    $sqlWhere
GROUP BY id
$sqlHaving
ORDER BY $sqlOrder
$sqlLimit
EOSQL;

        /** @var CI_DB_result $result */
        $result = $this->db->query($sql);
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        $items = array();
        foreach($result->result('array') as $row){
            $items[] = new BibliografieItem($row);
        }

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param BibliografieItem $item
     * @return bool|int
     */
    public function save(BibliografieItem $item)
    {
        // Set data.
        $eData = array(
            'titlu' => $item->getTitlu() ? $item->getTitlu() : null,
            'descriere' => $item->getDescriere() ? $item->getDescriere() : null,
            'is_deleted' => $item->getIsDeleted() ? $item->getIsDeleted() : null,
            'is_published' => $item->getIsPublished() ? $item->getIsPublished() : null,
        );

        // Insert/Update.
        if (!$item->getId()) {
            $res = $this->db->insert('fcp_biblioteca_bibliografii', $eData);
            $item->setId($this->db->insert_id());
        } else {
            $res = $this->db->update('fcp_biblioteca_bibliografii', $eData, 'id = ' . $item->getId());
        }

        $arrCarti = $item->getCarti();
        foreach($arrCarti as $carteID)
        {
            $sql = "INSERT INTO fcp_biblioteca_bibliografii2carti (bibliografie_id, carte_id)
                        VALUES (?, ?)
                        ON DUPLICATE KEY UPDATE carte_id = ?";
            $this->db->query($sql,[$item->getId(), (int)$carteID, (int)$carteID]);
        }

        // Remove old item from cache
        self::resetCollectionId($item->getId());

        return $res;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param BibliografieItem $item
     * @return mixed
     */
    public function delete(BibliografieItem $item)
    {
        // Remove old item from cache
        self::resetCollectionId($item->getId());

        return $this->db->delete('fcp_biblioteca_bibliografii', 'id = ' . $item->getId());
    }

    // ---------------------------------------------------------------------------------------------


}
