<?php

class CategorieCarteFilters
{

    const ORDER_BY_ID_ASC = 'id_asc';
    const ORDER_BY_ID_DESC = 'id_desc';
    const ORDER_BY_NUME_ASC = 'nume_asc';
    const ORDER_BY_NUME_DESC = 'nume_desc';
    const ORDER_BY_PARENT_ASC = 'parinte_asc';
    const ORDER_BY_PARENT_DESC = 'parinte_desc';
    const ORDER_BY_ORDER_ASC = 'order_asc';
    const ORDER_BY_ORDER_DESC = 'order_desc';


    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $excludedId;

    /**
     * @var string
     */
    private $nume;
    
    /**
     * @var int
     */
    private $parentId;

    /**
     * @var CategorieCarteItem
     */
    private $parent;

    /**
     * @var string
     */
    private $searchTerm;

    /**
     * @var array
     */
    private $orderBy = [];


    // ---------------------------------------------------------------------------------------------

    public function __construct(array $getData = array())
    {
        if (isset($getData['id'])) {
            $this->setId($getData['id']);
        }
        if (isset($getData['excludedId'])) {
            $this->setExcludedId($getData['excludedId']);
        }
        if (isset($getData['nume'])) {
            $this->setNume($getData['nume']);
        }
        if (isset($getData['parentId'])) {
            $this->setParentId($getData['parentId']);
        }
        if (isset($getData['searchTerm'])) {
            $this->setSearchTerm($getData['searchTerm']);
        }

        if (isset($getData['_orderBy'])) {
            $this->setOrderBy($getData['_orderBy']);
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return int|null
     */
    public function getExcludedId()
    {
        return $this->excludedId;
    }

    /**
     * @param int|null $excludedId
     */
    public function setExcludedId($excludedId)
    {
        $this->excludedId = $excludedId ? (int)$excludedId : null;
    }

    /**
     * @return string|null
     */
    public function getNume()
    {
        return $this->nume;
    }

    /**
     * @param string|null $nume
     */
    public function setNume($nume)
    {
        $this->nume = is_null($nume) ? null : trim(strip_tags($nume));
    }

    /**
     * @return int|null
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param int|null $parentId
     */
    public function setParentId($parentId)
    {
        $this->parentId = !is_null($parentId) ? (int)$parentId : null;
    }

    /**
     * @return CategorieCarteItem|null
     * @throws Exception
     */
    public function getParent()
    {
        if ($this->parent) {
            return $this->parent;
        } elseif ($this->parentId) {
            $this->parent = CategorieCarteTable::getInstance()->load($this->parentId);
            return $this->parent;
        } else {
            return null;
        }
    }

    /**
     * @return string|null
     */
    public function getSearchTerm()
    {
        return $this->searchTerm;
    }

    /**
     * @param string|null $val
     */
    public function setSearchTerm($val)
    {
        $this->searchTerm = is_null($val) ? null : trim(strip_tags($val));
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return array
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param array $orderBy
     */
    public function setOrderBy(array $orderBy)
    {
        $orderBy = (is_array($orderBy) && count($orderBy) ? $orderBy : array());
        if (count($orderBy)) {
            $orderItems = array_keys(self::fetchOrderItems());
            foreach ($orderBy as $k=>$v) {
                if (!in_array($v, $orderItems)) {
                    unset($orderBy[$k]);
                }
            }
        }
        $this->orderBy = $orderBy;
    }

    /**
     * @return array
     */
    public static function fetchOrderItems()
    {
        return array(
            self::ORDER_BY_ID_ASC => 'ID - ASC',
            self::ORDER_BY_ID_DESC => 'ID - DESC',
            self::ORDER_BY_NUME_ASC => 'Nume - ASC',
            self::ORDER_BY_NUME_DESC => 'Nume - DESC',
            self::ORDER_BY_PARENT_ASC => 'Parent - ASC',
            self::ORDER_BY_PARENT_DESC => 'Parent - DESC',
            self::ORDER_BY_ORDER_ASC => 'Order - ASC',
            self::ORDER_BY_ORDER_DESC => 'Order - DESC',
        );
    }

}