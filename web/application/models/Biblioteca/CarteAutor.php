<?php


class CarteAutor
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $carteId;

    /**
     * @var CarteItem
     */
    private $carte;

    /**
     * @var int
     */
    private $autorId;

    /**
     * @var AutorItem
     */
    private $autor;

    /**
     * @var int
     */
    private $rolId;

    /**
     * @var RolItem
     */
    private $rol;

    // ---------------------------------------------------------------------------------------------

    public function __construct(array $dbRow = array())
    {
        if (!count($dbRow)) {
            return;
        }
        $this->id = (int)$dbRow['id'];
        $this->carteId = $dbRow['carte_id'];
        $this->autorId = $dbRow['autor_id'];
        $this->rolId = $dbRow['rol_id'];
    }

    // ---------------------------------------------------------------------------------------------
    
    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getCarteId()
    {
        return $this->carteId;
    }
    
    /**
     * @param int|null $carteId
     * @throws Exception
     */
    public function setCarteId($carteId)
    {
        $this->carteId = $carteId ? (int)$carteId : null;
        if ($this->carteId) {
            $this->carte = CarteTable::getInstance()->load($this->carteId);
            if (!$this->carte) {
                throw new Exception('Cartea selectata nu a fost gasita in baza de date');
            }
        }
    }

    /**
     * @return CarteItem|null
     */
    public function getCarte()
    {
        if ($this->carte) {
            return $this->carte;
        } elseif ($this->carteId) {
            $this->carte = CarteTable::getInstance()->load($this->carteId);
            return $this->carte;
        } else {
            return null;
        }
    }

    /**
     * @return int|null
     */
    public function getAutorId()
    {
        return $this->autorId;
    }
    
    /**
     * @param int|null $autorId
     * @throws Exception
     */
    public function setAutorId($autorId)
    {
        $this->autorId = $autorId ? (int)$autorId : null;
        if ($this->autorId) {
            $this->autor = AutorTable::getInstance()->load($this->autorId);
            if (!$this->autor) {
                throw new Exception('Autorul selectat nu a fost gasit in baza de date');
            }
        }
    }

    /**
     * @return AutorItem|null
     */
    public function getAutor()
    {
        if ($this->autor) {
            return $this->autor;
        } elseif ($this->autorId) {
            $this->autor = AutorTable::getInstance()->load($this->autorId);
            return $this->autor;
        } else {
            return null;
        }
    }

    /**
     * @return int|null
     */
    public function getRolId()
    {
        return $this->rolId;
    }
    
    /**
     * @param int|null $rolId
     * @throws Exception
     */
    public function setRolId($rolId)
    {
        $this->rolId = $rolId ? (int)$rolId : null;
        if ($this->rolId) {
            $this->rol = RolTable::getInstance()->load($this->rolId);
            if (!$this->rol) {
                throw new Exception('Rolul selectat nu a fost gasit in baza de date');
            }
        }
    }

    /**
     * @return RolItem|null
     */
    public function getRol()
    {
        if ($this->rol) {
            return $this->rol;
        } elseif ($this->rolId) {
            $this->rol = RolTable::getInstance()->load($this->rolId);
            return $this->rol;
        } else {
            return null;
        }
    }


}