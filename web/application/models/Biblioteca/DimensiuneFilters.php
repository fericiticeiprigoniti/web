<?php

class DimensiuneFilters
{

    const ORDER_BY_ID_ASC = 'id_asc';
    const ORDER_BY_ID_DESC = 'id_desc';
    const ORDER_BY_LATIME_ASC = 'latime_asc';
    const ORDER_BY_LATIME_DESC = 'latime_desc';
    const ORDER_BY_INALTIME_ASC = 'inaltime_asc';
    const ORDER_BY_INALTIME_DESC = 'inaltime_desc';

    /**
     * @var int
     * @example ID-ul dimensiunii
     */
    private $id;

    /**
     * @var int
     */
    private $excludedId;

    /**
     * @var int
     * @example Lățimea unei carti în "cm"
     */
    private $latime;

    /**
     * @var int
     * @example Inaltimea unei carti în "cm"
     */
    private $inaltime;

    /**
     * @var string
     * @docs_ignore
     */
    private $searchTerm;

    /**
     * @var array
     * @example Suporta: id_asc / id_desc / latime_asc / latime_desc / inaltime_asc / inaltime_desc
     */
    private $orderBy = [];

    // ---------------------------------------------------------------------------------------------

    public function __construct(array $getData = array())
    {
        if (isset($getData['id'])) {
            $this->setId($getData['id']);
        }
        if (isset($getData['excludedId'])) {
            $this->setExcludedId($getData['excludedId']);
        }
        if (isset($getData['latime'])) {
            $this->setLatime($getData['latime']);
        }
        if (isset($getData['inaltime'])) {
            $this->setInaltime($getData['inaltime']);
        }
        if (isset($getData['searchTerm'])) {
            $this->setSearchTerm($getData['searchTerm']);
        }

        if (isset($getData['_orderBy'])) {
            $this->setOrderBy($getData['_orderBy']);
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return int|null
     */
    public function getExcludedId()
    {
        return $this->excludedId;
    }

    /**
     * @param int|null $excludedId
     */
    public function setExcludedId($excludedId)
    {
        $this->excludedId = $excludedId ? (int)$excludedId : null;
    }
    
    /**
     * @return int|null
     */
    public function getLatime()
    {
        return $this->latime;
    }
    
    /**
     * @param int|null $latime
     */
    public function setLatime($latime)
    {
        $this->latime = $latime ? (int)$latime : null;
    }
    
    /**
     * @return int|null
     */
    public function getInaltime()
    {
        return $this->inaltime;
    }
    
    /**
     * @param int|null $inaltime
     */
    public function setInaltime($inaltime)
    {
        $this->inaltime = $inaltime ? (int)$inaltime : null;
    }
    
    /**
     * @return string|null
     */
    public function getSearchTerm()
    {
        return $this->searchTerm;
    }

    /**
     * @param string|null $val
     */
    public function setSearchTerm($val)
    {
        $this->searchTerm = is_null($val) ? null : trim(strip_tags($val));
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return array
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param array $orderBy
     */
    public function setOrderBy(array $orderBy)
    {
        $orderBy = (is_array($orderBy) && count($orderBy) ? $orderBy : array());
        if (count($orderBy)) {
            $orderItems = array_keys(self::fetchOrderItems());
            foreach ($orderBy as $k=>$v) {
                if (!in_array($v, $orderItems)) {
                    unset($orderBy[$k]);
                }
            }
        }
        $this->orderBy = $orderBy;
    }

    /**
     * @return array
     */
    public static function fetchOrderItems()
    {
        return array(
            self::ORDER_BY_ID_ASC => 'ID - ASC',
            self::ORDER_BY_ID_DESC => 'ID - DESC',
            self::ORDER_BY_LATIME_ASC => 'Latime - ASC',
            self::ORDER_BY_LATIME_DESC => 'Latime - DESC',
            self::ORDER_BY_INALTIME_ASC => 'Inaltime - ASC',
            self::ORDER_BY_INALTIME_DESC => 'Inaltime - DESC',
        );
    }

}