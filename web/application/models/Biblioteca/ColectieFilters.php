<?php

class ColectieFilters
{

    const ORDER_BY_ID_ASC = 'id_asc';
    const ORDER_BY_ID_DESC = 'id_desc';
    const ORDER_BY_TITLU_ASC = 'titlu_asc';
    const ORDER_BY_TITLU_DESC = 'titlu_desc';


    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $excludedId;

    /**
     * @var string
     */
    private $titlu;

    /**
     * @var int
     */
    private $edituraId;

    /**
     * @var EdituraItem
     */
    private $editura;

    /**
     * @var string
     */
    private $searchTerm;

    /**
     * @var array
     */
    private $orderBy = [];


    // ---------------------------------------------------------------------------------------------

    public function __construct(array $getData = array())
    {
        if (isset($getData['id'])) {
            $this->setId($getData['id']);
        }
        if (isset($getData['excludedId'])) {
            $this->setExcludedId($getData['excludedId']);
        }
        if (isset($getData['titlu'])) {
            $this->setTitlu($getData['titlu']);
        }
        if (isset($getData['edituraId'])) {
            $this->setEdituraId($getData['edituraId']);
        }
        if (isset($getData['searchTerm'])) {
            $this->setSearchTerm($getData['searchTerm']);
        }

        if (isset($getData['_orderBy'])) {
            $this->setOrderBy($getData['_orderBy']);
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return int|null
     */
    public function getExcludedId()
    {
        return $this->excludedId;
    }

    /**
     * @param int|null $excludedId
     */
    public function setExcludedId($excludedId)
    {
        $this->excludedId = $excludedId ? (int)$excludedId : null;
    }
    
    /**
     * @return string|null
     */
    public function getTitlu()
    {
        return $this->titlu;
    }
    
    /**
     * @param string|null $titlu
     */
    public function setTitlu($titlu)
    {
        $this->titlu = is_null($titlu) ? null : trim(strip_tags($titlu));
    }

    /**
     * @return int|null
     */
    public function getEdituraId()
    {
        return $this->edituraId;
    }
    
    /**
     * @param int|null $edituraId
     */
    public function setEdituraId($edituraId)
    {
        $this->edituraId = $edituraId ? (int)$edituraId : null;
    }
    
    /**
     * @return EdituraItem|null
     */
    public function getEditura()
    {
        if ($this->editura) {
            return $this->editura;
        } elseif ($this->edituraId) {
            $this->editura = EdituraTable::getInstance()->load($this->edituraId);
            return $this->editura;
        } else {
            return null;
        }
    }

    /**
     * @return string|null
     */
    public function getSearchTerm()
    {
        return $this->searchTerm;
    }

    /**
     * @param string|null $val
     */
    public function setSearchTerm($val)
    {
        $this->searchTerm = is_null($val) ? null : trim(strip_tags($val));
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return array
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param array $orderBy
     */
    public function setOrderBy(array $orderBy)
    {
        $orderBy = (is_array($orderBy) && count($orderBy) ? $orderBy : array());
        if (count($orderBy)) {
            $orderItems = array_keys(self::fetchOrderItems());
            foreach ($orderBy as $k=>$v) {
                if (!in_array($v, $orderItems)) {
                    unset($orderBy[$k]);
                }
            }
        }
        $this->orderBy = $orderBy;
    }

    /**
     * @return array
     */
    public static function fetchOrderItems()
    {
        return array(
            self::ORDER_BY_ID_ASC => 'ID - ASC',
            self::ORDER_BY_ID_DESC => 'ID - DESC',
            self::ORDER_BY_TITLU_ASC => 'Titlu - ASC',
            self::ORDER_BY_TITLU_DESC => 'Titlu - DESC',
        );
    }

}