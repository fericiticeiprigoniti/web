<?php


class CarteItem
{

    use ArrayOrJson;

    /** Disponibil in format */
    const FORMAT_INDISPONIBIL = 0;
    const FORMAT_TIPARIT = 1;
    const FORMAT_ELECTRONIC = 2;
    const FORMAT_TIPARIT_ELECTRONIC = 3;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $titlu;

    /**
     * @var string
     */
    private $subtitlu;

    /**
     * @var int
     */
    private $categorieId;

    /**
     * @var string
     */
    private $categorieNume;

    /**
     * @var CategorieCarteItem
     */
    private $categorie;

    /**
     * @var int
     */
    private $parentId;

    /**
     * @var CarteItem
     */
    private $parent;

    /**
     * @var AutorItem[]
     */
    private $autori = [];

    /**
     * @var string
     */
    private $numeAutori;

    /**
     * @var string
     */
    private $autorOld;

    /**
     * @var int
     */
    private $localitateId;

    /**
     * @var LocalitateItem
     */
    private $localitate;

    /**
     * @var string
     */
    private $localitateOld;

    /**
     * @var string
     */
    private $oras;

    /**
     * @var int
     */
    private $versiune;

    /**
     * @var int
     */
    private $editia;

    /**
     * @var int
     */
    private $edituraId;

    /**
     * @var EdituraItem
     */
    private $editura;

    /**
     * @var string
     */
    private $edituraNume;

    /**
     * @var int
     */
    private $editura2Id;

    /**
     * @var EdituraItem
     */
    private $editura2;

    /**
     * @var int
     */
    private $colectieId;

    /**
     * @var ColectieItem
     */
    private $colectie;

    /**
     * @var int
     */
    private $anulPublicatiei;

    /**
     * @var int
     */
    private $nrPagini;

    /**
     * @var int
     */
    private $nrAnexe;

    /**
     * @var int
     */
    private $dimensiuneId;

    /**
     * @var DimensiuneItem
     */
    private $dimensiune;

    /**
     * @var string
     */
    private $dimensiuneOld;

    /**
     * @var string
     */
    private $coperta;

    /**
     * @var int
     */
    private $introducereArticolId;

    /**
     * @var ArticolItem
     */
    private $introducereArticol;

    /**
     * @var int
     */
    private $rezumatArticolId;

    /**
     * @var ArticolItem
     */
    private $rezumatArticol;

    /**
     * @var int
     */
    private $recenzieArticolId;

    /**
     * @var ArticolItem
     */
    private $recenzieArticol;

    /**
     * @var string
     */
    private $observatii;

    /**
     * @var string
     */
    private $observatiiExterne;

    /**
     * @var int
     */
    private $format;

    /**
     * @var bool
     */
    private $isPublished;

    /**
     * @var bool
     */
    private $isDeleted;

    // ---------------------------------------------------------------------------------------------

    public function __construct(array $dbRow = array())
    {
        if (!count($dbRow)) {
            return;
        }
        $this->id = (int)$dbRow['id'];
        $this->titlu = $dbRow['titlu'];
        $this->subtitlu = $dbRow['subtitlu'];
        $this->categorieId = $dbRow['cat_id'];
        $this->categorieNume = $dbRow['nume_categorie'];
        $this->parentId = $dbRow['parent_id'];

        $this->numeAutori = $dbRow['nume_autori'];
        $this->autorOld = $dbRow['backup_bAutor'];

        $this->localitateId = $dbRow['localitate_id'];
        $this->localitateOld = $dbRow['backup_localitate'];
        $this->oras = $dbRow['oras'];
        $this->versiune = $dbRow['versiune'];
        $this->editia = $dbRow['editia'];
        $this->edituraId = $dbRow['editura_id'];
        $this->edituraNume = $dbRow['nume_editura'];
        $this->editura2Id = $dbRow['editura2_id'];
        $this->colectieId = $dbRow['colectie_id'];

        $this->anulPublicatiei = $dbRow['anul_publicatiei'];
        $this->nrPagini = $dbRow['nr_pagini'];
        $this->nrAnexe = $dbRow['nr_anexe'];
        $this->coperta = $dbRow['coperta'];
        $this->dimensiuneId = $dbRow['dimensiune_id'];
        $this->dimensiuneOld = $dbRow['backup_dimensiuni'];

        $this->introducereArticolId = $dbRow['introducere_art_id'];
        $this->rezumatArticolId = $dbRow['rezumat_art_id'];
        $this->recenzieArticolId = $dbRow['recenzie_art_id'];
        $this->observatii = $dbRow['observatii'];
        $this->observatiiExterne = $dbRow['observatii_externe'];
        $this->format = $dbRow['disponibil_in_format'];

        $this->isPublished = (bool)$dbRow['is_published'];
        $this->isDeleted = (bool)$dbRow['is_deleted'];
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitlu()
    {
        return $this->titlu;
    }

    /**
     * @param string|null $titlu
     * @throws Exception
     */
    public function setTitlu($titlu)
    {
        $this->titlu = trim(strip_tags($titlu));
        if (empty($this->titlu)) {
            throw new Exception('Completati titlul cartii');
        }
    }

    /**
     * @return string|null
     */
    public function getSubtitlu()
    {
        return $this->subtitlu;
    }

    /**
     * @param string|null $subtitlu
     */
    public function setSubtitlu($subtitlu)
    {
        $this->subtitlu = is_null($subtitlu) ? null : trim(strip_tags($subtitlu));
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getCategorieId()
    {
        return $this->categorieId;
    }

    /**
     * @param int|null $categorieId
     * @throws Exception
     */
    public function setCategorieId($categorieId)
    {
        $this->categorieId = $categorieId ? (int)$categorieId : null;
        if ($this->categorieId) {
            $this->categorie = CategorieCarteTable::getInstance()->load($this->categorieId);
            if (!$this->categorie) {
                throw new Exception('Categoria selectata nu a fost gasita in baza de date');
            }
            $this->categorieNume = $this->categorie->getNume();
        }
    }

    /**
     * @return CategorieCarteItem|null
     */
    public function getCategorie()
    {
        if ($this->categorie) {
            return $this->categorie;
        } elseif ($this->categorieId) {
            $this->categorie = CategorieCarteTable::getInstance()->load($this->categorieId);
            return $this->categorie;
        } else {
            return null;
        }
    }

    /**
     * @return string|null
     */
    public function getCategorieNume()
    {
        return $this->categorieNume;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param int|null $parentId
     * @throws Exception
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId ? (int)$parentId : null;
        if ($this->parentId) {
            $this->parent = CarteTable::getInstance()->load($this->parentId);
            if (!$this->parent) {
                throw new Exception('Cartea parinte selectata nu a fost gasita in baza de date');
            }
        }
    }

    /**
     * @return CarteItem|null
     */
    public function getParent()
    {
        if ($this->parent) {
            return $this->parent;
        } elseif ($this->parentId) {
            $this->parent = CarteTable::getInstance()->load($this->parentId);
            return $this->parent;
        } else {
            return null;
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return AutorItem[]
     */
    public function getAutoriObj()
    {
        if ($this->autori) {
            return $this->autori;
        } elseif ($this->id) {
            $autoriCarte = CarteTable::getInstance()->fetchAllAutori(array('carte_id' => $this->id));
            if (!isset($autoriCarte['items']) || !count($autoriCarte['items'])) {
                return array();
            }
            $this->autori = array();
            /** @var CarteAutor $autorCarte */
            foreach ($autoriCarte['items'] as $autorCarte) {
                $this->autori[] = $autorCarte->getAutor();
            }
            return $this->autori;
        } else {
            return array();
        }
    }

    /**
     * return the list with authors
     *
     * @return string
     */
    public function getAutori()
    {
        $authors = [];
        if ($list = $this->getAutoriObj()) {
            foreach ($list as $item) {
                $authors[] = $item->getNume();
            }
        }

        return implode(', ', $authors);
    }

    /**
     * @return string|null
     */
    public function getNumeAutori()
    {
        return $this->numeAutori;
    }

    /**
     * @return string|null
     */
    public function getAutorOld()
    {
        return $this->autorOld;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getLocalitateId()
    {
        return $this->localitateId;
    }

    /**
     * @param int|null $localitateId
     * @throws Exception
     */
    public function setLocalitateId($localitateId)
    {
        $this->localitateId = $localitateId ? (int)$localitateId : null;
        if ($this->localitateId) {
            $this->localitate = LocalitateTable::getInstance()->load($this->localitateId);
            if (!$this->localitate) {
                throw new Exception('Localitatea selectata nu a fost gasita in baza de date');
            }
        }
    }

    /**
     * @return LocalitateItem|null
     */
    public function getLocalitate()
    {
        if ($this->localitate) {
            return $this->localitate;
        } elseif ($this->localitateId) {
            $this->localitate = LocalitateTable::getInstance()->load($this->localitateId);
            return $this->localitate;
        } else {
            return null;
        }
    }

    /**
     * @return string|null
     */
    public function getLocalitateOld()
    {
        return $this->localitateOld;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getVersiune()
    {
        return $this->versiune;
    }

    /**
     * @param int|null $versiune
     */
    public function setVersiune($versiune)
    {
        $this->versiune = $versiune ? (int)$versiune : null;
    }


    /**
     * @return string|null
     */
    public function getOras()
    {
        return $this->oras;
    }

    /**
     * @param string|null $oras
     */
    public function setOras($oras)
    {
        $this->oras = $oras ?? null;
    }

    /**
     * @return int|null
     */
    public function getEditia()
    {
        return $this->editia;
    }

    /**
     * @param int|null $editia
     */
    public function setEditia($editia)
    {
        $this->editia = $editia ? (int)$editia : null;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getEdituraId()
    {
        return $this->edituraId;
    }

    /**
     * @param int|null $edituraId
     * @throws Exception
     */
    public function setEdituraId($edituraId)
    {
        $this->edituraId = $edituraId ? (int)$edituraId : null;
        if ($this->edituraId) {
            $this->editura = EdituraTable::getInstance()->load($this->edituraId);
            if (!$this->editura) {
                throw new Exception('Editura selectata nu a fost gasita in baza de date');
            }
            $this->edituraNume = $this->editura->getNume();
        }
    }

    /**
     * @return EdituraItem|null
     */
    public function getEditura()
    {
        if ($this->editura) {
            return $this->editura;
        } elseif ($this->edituraId) {
            $this->editura = EdituraTable::getInstance()->load($this->edituraId);
            return $this->editura;
        } else {
            return null;
        }
    }

    /**
     * @return string|null
     */
    public function getEdituraNume()
    {
        return $this->edituraNume;
    }

    /**
     * @return int|null
     */
    public function getEditura2Id()
    {
        return $this->editura2Id;
    }

    /**
     * @param int|null $editura2Id
     * @throws Exception
     */
    public function setEditura2Id($editura2Id)
    {
        $this->editura2Id = $editura2Id ? (int)$editura2Id : null;
        if ($this->editura2Id) {
            $this->editura2 = EdituraTable::getInstance()->load($this->editura2Id);
            if (!$this->editura2) {
                throw new Exception('Editura2 selectata nu a fost gasita in baza de date');
            }
        }
    }

    /**
     * @return EdituraItem|null
     */
    public function getEditura2()
    {
        if ($this->editura2) {
            return $this->editura2;
        } elseif ($this->editura2Id) {
            $this->editura2 = EdituraTable::getInstance()->load($this->editura2Id);
            return $this->editura2;
        } else {
            return null;
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getColectieId()
    {
        return $this->colectieId;
    }

    /**
     * @param int|null $colectieId
     * @throws Exception
     */
    public function setColectieId($colectieId)
    {
        $this->colectieId = $colectieId ? (int)$colectieId : null;
        if ($this->colectieId) {
            $this->colectie = ColectieTable::getInstance()->load($this->colectieId);
            if (!$this->colectie) {
                throw new Exception('Colectia selectata nu a fost gasita in baza de date');
            }
        }
    }

    /**
     * @return ColectieItem|null
     */
    public function getColectie()
    {
        if ($this->colectie) {
            return $this->colectie;
        } elseif ($this->colectieId) {
            $this->colectie = ColectieTable::getInstance()->load($this->colectieId);
            return $this->colectie;
        } else {
            return null;
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getAnulPublicatiei()
    {
        return $this->anulPublicatiei;
    }

    /**
     * @param int|null $anulPublicatiei
     */
    public function setAnulPublicatiei($anulPublicatiei)
    {
        $this->anulPublicatiei = $anulPublicatiei ? (int)$anulPublicatiei : null;
    }

    /**
     * @return int|null
     */
    public function getNrPagini()
    {
        return $this->nrPagini;
    }

    /**
     * @param int|null $nrPagini
     */
    public function setNrPagini($nrPagini)
    {
        $this->nrPagini = $nrPagini ? (int)$nrPagini : null;
    }

    /**
     * @return int|null
     */
    public function getNrAnexe()
    {
        return $this->nrAnexe;
    }

    /**
     * @param int|null $nrAnexe
     */
    public function setNrAnexe($nrAnexe)
    {
        $this->nrAnexe = $nrAnexe ? (int)$nrAnexe : null;
    }

    /**
     * @return string|null
     */
    public function getCoperta()
    {
        return $this->coperta;
    }

    /**
     * @param string|null $coperta
     */
    public function setCoperta($coperta)
    {
        $this->coperta = is_null($coperta) ? null : trim(strip_tags($coperta));
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getDimensiuneId()
    {
        return $this->dimensiuneId;
    }

    /**
     * @param int|null $dimensiuneId
     * @throws Exception
     */
    public function setDimensiuneId($dimensiuneId)
    {
        $this->dimensiuneId = $dimensiuneId ? (int)$dimensiuneId : null;
        if ($this->dimensiuneId) {
            $this->dimensiune = DimensiuneTable::getInstance()->load($this->dimensiuneId);
            if (!$this->dimensiune) {
                throw new Exception('Dimensiunea selectata nu a fost gasita in baza de date');
            }
        }
    }

    /**
     * @return DimensiuneItem|null
     */
    public function getDimensiune()
    {
        if ($this->dimensiune) {
            return $this->dimensiune;
        } elseif ($this->dimensiuneId) {
            $this->dimensiune = DimensiuneTable::getInstance()->load($this->dimensiuneId);
            return $this->dimensiune;
        } else {
            return null;
        }
    }

    /**
     * @return string|null
     */
    public function getDimensiuneOld()
    {
        return $this->dimensiuneOld;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getIntroducereArticolId()
    {
        return $this->introducereArticolId;
    }

    /**
     * @param int|null $introducereArticolId
     * @throws Exception
     */
    public function setIntroducereArticolId($introducereArticolId)
    {
        $this->introducereArticolId = $introducereArticolId ? (int)$introducereArticolId : null;
        if ($this->introducereArticolId) {
            $this->introducereArticol = ArticolTable::getInstance()->load($this->introducereArticolId);
            if (!$this->introducereArticol) {
                throw new Exception('Articolul pentru introducere nu a fost gasit in baza de date');
            }
        }
    }

    /**
     * @return ArticolItem|null
     */
    public function getIntroducereArticol()
    {
        if ($this->introducereArticol) {
            return $this->introducereArticol;
        } elseif ($this->introducereArticolId) {
            $this->introducereArticol = ArticolTable::getInstance()->load($this->introducereArticolId);
            return $this->introducereArticol;
        } else {
            return null;
        }
    }

    /**
     * @return int|null
     */
    public function getRezumatArticolId()
    {
        return $this->rezumatArticolId;
    }

    /**
     * @param int|null $rezumatArticolId
     * @throws Exception
     */
    public function setRezumatArticolId($rezumatArticolId)
    {
        $this->rezumatArticolId = $rezumatArticolId ? (int)$rezumatArticolId : null;
        if ($this->rezumatArticolId) {
            $this->rezumatArticol = ArticolTable::getInstance()->load($this->rezumatArticolId);
            if (!$this->rezumatArticol) {
                throw new Exception('Articolul pentru rezumat nu a fost gasit in baza de date');
            }
        }
    }

    /**
     * @return ArticolItem|null
     */
    public function getRezumatArticol()
    {
        if ($this->rezumatArticol) {
            return $this->rezumatArticol;
        } elseif ($this->rezumatArticolId) {
            $this->rezumatArticol = ArticolTable::getInstance()->load($this->rezumatArticolId);
            return $this->rezumatArticol;
        } else {
            return null;
        }
    }

    /**
     * @return int|null
     */
    public function getRecenzieArticolId()
    {
        return $this->recenzieArticolId;
    }

    /**
     * @param int|null $recenzieArticolId
     * @throws Exception
     */
    public function setRecenzieArticolId($recenzieArticolId)
    {
        $this->recenzieArticolId = $recenzieArticolId ? (int)$recenzieArticolId : null;
        if ($this->recenzieArticolId) {
            $this->recenzieArticol = ArticolTable::getInstance()->load($this->recenzieArticolId);
            if (!$this->recenzieArticol) {
                throw new Exception('Articolul pentru recenzie nu a fost gasit in baza de date');
            }
        }
    }

    /**
     * @return ArticolItem|null
     */
    public function getRecenzieArticol()
    {
        if ($this->recenzieArticol) {
            return $this->recenzieArticol;
        } elseif ($this->recenzieArticolId) {
            $this->recenzieArticol = ArticolTable::getInstance()->load($this->recenzieArticolId);
            return $this->recenzieArticol;
        } else {
            return null;
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return string|null
     */
    public function getObservatii()
    {
        return $this->observatii;
    }

    /**
     * @param string|null $observatii
     */
    public function setObservatii($observatii)
    {
        $this->observatii = is_null($observatii) ? null : trim(strip_tags($observatii));
    }

    /**
     * @return string|null
     */
    public function getObservatiiExterne()
    {
        return $this->observatiiExterne;
    }

    /**
     * @param string|null $observatii
     */
    public function setObservatiiExterne($observatiiExterne)
    {
        $this->observatiiExterne = is_null($observatiiExterne) ? null : trim(strip_tags($observatiiExterne));
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param int $format
     * @throws Exception
     */
    public function setFormat($format)
    {
        $format = (int)$format;
        $formatTypes = self::fetchFormatTypes();
        if (isset($formatTypes[$format])) {
            $this->format = $format;
        } else {
            throw new Exception('Formatul de disponibilitate a cartii este invalid');
        }
    }

    /**
     * @return string|null
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @return string|null
     */
    public function getFormatName()
    {
        $formatTypes = self::fetchFormatTypes();
        return (isset($formatTypes[$this->format])) ? $formatTypes[$this->format] : null;
    }

    public function isPrintedFormat()
    {
        return $this->format === self::FORMAT_TIPARIT;
    }

    public function isElectronicFormat()
    {
        return $this->format === self::FORMAT_ELECTRONIC;
    }

    public function isPrintedAndElectronicFormat()
    {
        return $this->format === self::FORMAT_TIPARIT_ELECTRONIC;
    }

    public static function fetchFormatTypes()
    {
        return array(
            self::FORMAT_INDISPONIBIL => 'Indisponibil',
            self::FORMAT_TIPARIT => 'Tiparit',
            self::FORMAT_ELECTRONIC => 'Electronic',
            self::FORMAT_TIPARIT_ELECTRONIC => 'Tiparit + Electronic',
        );
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return boolean
     */
    public function isPublished()
    {
        return $this->isPublished;
    }

    /**
     * @var boolean $value
     */
    public function setIsPublished($value)
    {
        $this->isPublished = (bool)$value;
    }

    /**
     * @return boolean
     */
    public function isDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @var boolean $value
     */
    public function setIsDeleted($value)
    {
        $this->isDeleted = (bool)$value;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return string
     */
    public function getSearchResult()
    {
        return $this->id . ' | ' . $this->titlu . ' | ' . $this->subtitlu;
    }

    // ---------------------------------------------------------------------------------------------


}
