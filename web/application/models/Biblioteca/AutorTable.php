<?php

/**
 * Class AutorTable
 * @table fcp_autori
 */
class AutorTable extends CI_Model
{

    /**
     * @var AutorTable
     */
    private static $instance;


    /**
     * Singleton
     * AutorTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    // ---------------------------------------------------------------------------------------------

    /**
     *  we can later dependency inject and thus unit test this
     * @return AutorTable
     */
    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param int $id
     * @return AutorItem|null
     * @throws Exception
     */
    public function load($id): ?AutorItem
    {
        $id = (int)$id;
        if (!$id) {
            throw new Exception('ID autor este invalid');
        }
        $filters = new AutorFilters(array('id'=>$id));
        $data = $this->fetchAll($filters);

        return $data['items'][0] ?? null;
    }

    /**
     * @param int $id
     */
    public static function get($id): ?AutorItem
    {
        if (!$id) {
            return null;
        }

        try {
            return self::getInstance()->load($id);
        } catch (Throwable $e) {
            log_message('error', $e->getMessage());
            return null;
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param AutorFilters $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchAll(AutorFilters $filters, $limit = null, $offset = null)
    {
        // Set where and having conditions.
        $sqlWhere = array();
        $sqlHaving = array();

        if ($filters->getId()) {
            $sqlWhere[] = "autor.id = {$filters->getId()}";
        }
        if ($filters->getIdList() && count($filters->getIdList())) {
            $idList = implode(',', $filters->getIdList());
            $sqlWhere[] = "autor.id IN ($idList)";
        }
        if ($filters->getExcludedId()) {
            $sqlWhere[] = "autor.id <> {$filters->getExcludedId()}";
        }
        if ($filters->getNumeLike()) {
            $sqlWhere[] = "autor.nume LIKE " . $this->db->escape("%{$filters->getNumeLike()}%");
        }
        if ($filters->getPrenumeLike()) {
            $sqlWhere[] = "autor.prenume LIKE " . $this->db->escape("%{$filters->getPrenumeLike()}%");
        }
        if ($filters->getAliasLike()) {
            $sqlWhere[] = "autor.alias LIKE " . $this->db->escape("%{$filters->getAliasLike()}%");
        }
        if ($filters->getSearchTerm()) {
            // Clean search term, extract words and set where condition.
            $searchTerm = Strings::sanitizeForDatabaseUsage($filters->getSearchTerm());
            $words = Strings::splitIntoWords($searchTerm);
            if (count($words)) {
                $subCond = array();
                foreach ($words as $word) {
                    $word = $this->db->escape("%$word%");
                    $subCond[] = "(id LIKE $word OR nume LIKE $word OR prenume LIKE $word OR alias LIKE $word)";
                }
                $sqlWhere[] = implode("\n\tAND ", $subCond);
            }
        }

        $sqlWhere = implode("\n\tAND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sqlHaving = implode("\n\tAND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? "HAVING " . $sqlHaving : '';

        // ------------------------------------

        // Set order by.
        $sqlOrder = array();
        if (count($filters->getOrderBy())) {
            foreach($filters->getOrderBy() as $ord) {
                switch($ord) {
                    case $filters::ORDER_BY_ID_ASC:
                        $sqlOrder[] = "id ASC";
                        break;
                    case $filters::ORDER_BY_ID_DESC:
                        $sqlOrder[] = "id DESC";
                        break;
                    case $filters::ORDER_BY_NUME_ASC:
                        $sqlOrder[] = "nume ASC";
                        break;
                    case $filters::ORDER_BY_NUME_DESC:
                        $sqlOrder[] = "nume DESC";
                        break;
                    case $filters::ORDER_BY_PRENUME_ASC:
                        $sqlOrder[] = "prenume ASC";
                        break;
                    case $filters::ORDER_BY_PRENUME_DESC:
                        $sqlOrder[] = "prenume DESC";
                        break;
                }
            }
        }
        if (!count($sqlOrder)) {
            $sqlOrder[] = "id ASC";
        }
        $sqlOrder = implode(", ", $sqlOrder);

        // ------------------------------------

        // Set limit.
        $sqlLimit = '';
        if (!is_null($offset) && !is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$offset . ', ' . (int)$limit;
        } elseif (!is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$limit;
        }

        // ------------------------------------

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    autor.*
FROM
    fcp_autori AS autor
WHERE
    1
    $sqlWhere
GROUP BY autor.id
$sqlHaving
ORDER BY $sqlOrder
$sqlLimit
EOSQL;

        /** @var CI_DB_result $result */
        $result = $this->db->query($sql);
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        $items = array();
        foreach($result->result('array') as $row){
            $items[] = new AutorItem($row);
        }

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------
    
    /**
     * @param string $searchTerm
     * @param int $limit
     * @return array|AutorItem[]
     */
    public function fetchForAutocomplete($searchTerm, $limit=LIMIT_FOR_AUTOCOMPLETE)
    {
        $filters = new AutorFilters(['searchTerm'=>$searchTerm]);
        $data = $this->fetchAll($filters, $limit);

        return $data['items'] ?? [];
    }
    
    // ---------------------------------------------------------------------------------------------

    // return key -> value
    public function fetchForSelect()
    {
        $filters = new AutorFilters();
        $result = $this->fetchAll($filters);

        $data = [];
        if (isset($result['items']) && count($result['items'])) {
            /** @var AutorItem $objItem */
            foreach($result['items'] as $objItem) {
                $data[$objItem->getId()] = $objItem->getFullname();
            }
        }

        return $data;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param AutorFilters $filters
     * @param int|null $limit
     * @param int|null $offset
     * @param int $tCount
     * @return array
     */
    public function fetchForApi(AutorFilters $filters, $limit = null, $offset = null, &$tCount)
    {
        $data = $this->fetchAll($filters, $limit, $offset);
        $return = [];
        if (isset($data['items']) && count($data['items'])) {
            /** @var AutorItem $item */
            foreach($data['items'] as $item) {
                $return[] = $item->toArray();
            }
            $tCount = $data['count'];
        }
        return $return;
    }

}