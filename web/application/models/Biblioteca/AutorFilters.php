<?php

class AutorFilters
{

    const ORDER_BY_ID_ASC = 'id_asc';
    const ORDER_BY_ID_DESC = 'id_desc';
    const ORDER_BY_NUME_ASC = 'nume_asc';
    const ORDER_BY_NUME_DESC = 'nume_desc';
    const ORDER_BY_PRENUME_ASC = 'prenume_asc';
    const ORDER_BY_PRENUME_DESC = 'prenume_desc';

    /**
     * @var int
     * @example ID-ul autorului
     */
    private $id;

    /**
     * @var int
     * @example Ignora un autor din lista
     */
    private $excludedId;

    /**
     * @var int[]
     * @example Returneaza toti autorii dupa o lista cu ID
     */
    private $idList;

    /**
     * @var string
     * @example Cauta un autor dupa nume
     */
    private $numeLike;

    /**
     * @var string
     * @example Cauta un autor dupa prenume
     */
    private $prenumeLike;

    /**
     * @var string
     * @docs_ignore
     */
    private $aliasLike;

    /**
     * @var bool
     * @example 1 = inactive / 0 = active
     * @docs_ignore
     */
    private $isDeleted;

    /**
     * @var string
     */
    private $searchTerm;

    /**
     * @var array | string
     * @example Suporta: id_asc/ id_desc / nume_asc / nume_desc / prenume_asc / prenume_desc
     */
    private $orderBy = [];

    // ---------------------------------------------------------------------------------------------

    public function __construct(array $getData = array())
    {
        if (isset($getData['id'])) {
            $this->setId($getData['id']);
        }
        if (isset($getData['excludedId'])) {
            $this->setExcludedId($getData['excludedId']);
        }
        if (isset($getData['idList'])) {
            $this->setIdList($getData['idList']);
        }
        if (isset($getData['numeLike'])) {
            $this->setNumeLike($getData['numeLike']);
        }
        if (isset($getData['prenumeLike'])) {
            $this->setPrenumeLike($getData['prenumeLike']);
        }
        if (isset($getData['aliasLike'])) {
            $this->setAliasLike($getData['aliasLike']);
        }
        if (isset($getData['isDeleted'])) {
            $this->setIsDeleted($getData['isDeleted']);
        }
        if (isset($getData['searchTerm'])) {
            $this->setSearchTerm($getData['searchTerm']);
        }

        if (isset($getData['_orderBy'])) {
            $arr_order = is_array($getData['_orderBy'])? $getData['_orderBy'] : [$getData['_orderBy']];
            $this->setOrderBy($arr_order);
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return int|null
     */
    public function getExcludedId()
    {
        return $this->excludedId;
    }

    /**
     * @param int|null $excludedId
     */
    public function setExcludedId($excludedId)
    {
        $this->excludedId = $excludedId ? (int)$excludedId : null;
    }
    
    /**
     * @return int[]|null
     */
    public function getIdList()
    {
        return $this->idList;
    }

    /**
     * @param array|null $idList
     * @example Se poate trimite o lista cu ID-urile autorilor
     */
    public function setIdList($idList)
    {
        $this->idList = array_map('intval', $idList);
    }
    
    /**
     * @return string|null
     */
    public function getNumeLike()
    {
        return $this->numeLike;
    }
    
    /**
     * @param string|null $numeLike
     */
    public function setNumeLike($numeLike)
    {
        $this->numeLike = is_null($numeLike) ? null : trim(strip_tags($numeLike));
    }

    /**
     * @return string|null
     */
    public function getPrenumeLike()
    {
        return $this->prenumeLike;
    }

    /**
     * @param string|null $prenumeLike
     */
    public function setPrenumeLike($prenumeLike)
    {
        $this->prenumeLike = is_null($prenumeLike) ? null : trim(strip_tags($prenumeLike));
    }

    /**
     * @return string|null
     */
    public function getAliasLike()
    {
        return $this->aliasLike;
    }

    /**
     * @param string|null $aliasLike
     */
    public function setAliasLike($aliasLike)
    {
        $this->aliasLike = is_null($aliasLike) ? null : trim(strip_tags($aliasLike));
    }

    /**
     * @return bool|null
     */
    public function isDeleted()
    {
        return $this->isDeleted;
    }
    
    /**
     * @param bool|null $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = !is_null($isDeleted) ? (bool)$isDeleted : null;
    }

    /**
     * @return string|null
     */
    public function getSearchTerm()
    {
        return $this->searchTerm;
    }

    /**
     * @param string|null $val
     */
    public function setSearchTerm($val)
    {
        $this->searchTerm = is_null($val) ? null : trim(strip_tags($val));
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return array
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param array $orderBy
     */
    public function setOrderBy(array $orderBy)
    {
        $orderBy = (is_array($orderBy) && count($orderBy) ? $orderBy : array());
        if (count($orderBy)) {
            $orderItems = array_keys(self::fetchOrderItems());
            foreach ($orderBy as $k=>$v) {
                if (!in_array($v, $orderItems)) {
                    unset($orderBy[$k]);
                }
            }
        }
        $this->orderBy = $orderBy;
    }

    /**
     * @return array
     */
    public static function fetchOrderItems()
    {
        return array(
            self::ORDER_BY_ID_ASC => 'ID - ASC',
            self::ORDER_BY_ID_DESC => 'ID - DESC',
            self::ORDER_BY_NUME_ASC => 'Nume - ASC',
            self::ORDER_BY_NUME_DESC => 'Nume - DESC',
            self::ORDER_BY_PRENUME_ASC => 'Prenume - ASC',
            self::ORDER_BY_PRENUME_DESC => 'Prenume - DESC',
        );
    }

}