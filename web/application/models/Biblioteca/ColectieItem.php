<?php


class ColectieItem
{
    use ArrayOrJson;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $titlu;

    /**
     * @var int
     */
    private $edituraId;
    
    /**
     * @var EdituraItem
     */
    private $editura;

    // ---------------------------------------------------------------------------------------------

    public function __construct(array $dbRow = array())
    {
        if (!count($dbRow)) {
            return;
        }
        $this->id = (int)$dbRow['id'];
        $this->titlu = $dbRow['titlu'];
        $this->edituraId = $dbRow['editura_id'];
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitlu()
    {
        return $this->titlu;
    }

    /**
     * @param string|null $titlu
     * @throws Exception
     */
    public function setTitlu($titlu)
    {
        $this->titlu = trim(strip_tags($titlu));
        if (empty($this->titlu)) {
            throw new Exception('Completati titlul colectiei');
        }
    }

    /**
     * @return int|null
     */
    public function getEdituraId()
    {
        return $this->edituraId;
    }
    
    /**
     * @param int|null $edituraId
     * @throws Exception
     */
    public function setEdituraId($edituraId)
    {
        $this->edituraId = $edituraId ? (int)$edituraId : null;
        if ($this->edituraId) {
            $this->editura = EdituraTable::getInstance()->load($this->edituraId);
            if (!$this->editura) {
                throw new Exception('Editura nu a fost gasita in baza de date');
            }
        }
    }
    
    /**
     * @return EdituraItem|null
     */
    public function getEditura()
    {
        if ($this->editura) {
            return $this->editura;
        } elseif ($this->edituraId) {
            $this->editura = EdituraTable::getInstance()->load($this->edituraId);
            return $this->editura;
        } else {
            return null;
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return string
     */
    public function getSearchResult()
    {
        return $this->id . ' | ' . $this->titlu;
    }

}