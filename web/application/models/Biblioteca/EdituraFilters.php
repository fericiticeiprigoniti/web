<?php

class EdituraFilters
{

    const ORDER_BY_ID_ASC = 'id_asc';
    const ORDER_BY_ID_DESC = 'id_desc';
    const ORDER_BY_NUME_ASC = 'nume_asc';
    const ORDER_BY_NUME_DESC = 'nume_desc';


    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $excludedId;

    /**
     * @var string
     */
    private $numeLike;

    /**
     * @var string
     */
    private $aliasLike;

    /**
     * @var int
     */
    private $localitateId;

    /**
     * @var LocalitateItem
     */
    private $localitate;

    /**
     * @var bool
     */
    private $isDeleted;

    /**
     * @var string
     */
    private $searchTerm;

    /**
     * @var array
     */
    private $orderBy = [];


    // ---------------------------------------------------------------------------------------------

    public function __construct(array $getData = array())
    {
        if (isset($getData['id'])) {
            $this->setId($getData['id']);
        }
        if (isset($getData['excludedId'])) {
            $this->setExcludedId($getData['excludedId']);
        }
        if (isset($getData['numeLike'])) {
            $this->setNumeLike($getData['numeLike']);
        }
        if (isset($getData['aliasLike'])) {
            $this->setAliasLike($getData['aliasLike']);
        }
        if (isset($getData['localitateId'])) {
            $this->setLocalitateId($getData['localitateId']);
        }
        if (isset($getData['isDeleted'])) {
            $this->setIsDeleted($getData['isDeleted']);
        }
        if (isset($getData['searchTerm'])) {
            $this->setSearchTerm($getData['searchTerm']);
        }

        if (isset($getData['_orderBy'])) {
            $this->setOrderBy($getData['_orderBy']);
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return int|null
     */
    public function getExcludedId()
    {
        return $this->excludedId;
    }

    /**
     * @param int|null $excludedId
     */
    public function setExcludedId($excludedId)
    {
        $this->excludedId = $excludedId ? (int)$excludedId : null;
    }
    
    /**
     * @return string|null
     */
    public function getNumeLike()
    {
        return $this->numeLike;
    }
    
    /**
     * @param string|null $numeLike
     */
    public function setNumeLike($numeLike)
    {
        $this->numeLike = is_null($numeLike) ? null : trim(strip_tags($numeLike));
    }

    /**
     * @return string|null
     */
    public function getAliasLike()
    {
        return $this->aliasLike;
    }

    /**
     * @param string|null $aliasLike
     */
    public function setAliasLike($aliasLike)
    {
        $this->aliasLike = is_null($aliasLike) ? null : trim(strip_tags($aliasLike));
    }
    
    /**
     * @return int|null
     */
    public function getLocalitateId()
    {
        return $this->localitateId;
    }
    
    /**
     * @param int|null $localitateId
     */
    public function setLocalitateId($localitateId)
    {
        $this->localitateId = $localitateId ? (int)$localitateId : null;
    }
    
    /**
     * @return LocalitateItem|null
     */
    public function getLocalitate()
    {
        if ($this->localitate) {
            return $this->localitate;
        } elseif ($this->localitateId) {
            $this->localitate = LocalitateTable::getInstance()->load($this->localitateId);
            return $this->localitate;
        } else {
            return null;
        }
    }
    
    /**
     * @return bool|null
     */
    public function isDeleted()
    {
        return $this->isDeleted;
    }
    
    /**
     * @param bool|null $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = !is_null($isDeleted) ? (bool)$isDeleted : null;
    }

    /**
     * @return string|null
     */
    public function getSearchTerm()
    {
        return $this->searchTerm;
    }

    /**
     * @param string|null $val
     */
    public function setSearchTerm($val)
    {
        $this->searchTerm = is_null($val) ? null : trim(strip_tags($val));
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return array
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param array $orderBy
     */
    public function setOrderBy(array $orderBy)
    {
        $orderBy = (is_array($orderBy) && count($orderBy) ? $orderBy : array());
        if (count($orderBy)) {
            $orderItems = array_keys(self::fetchOrderItems());
            foreach ($orderBy as $k=>$v) {
                if (!in_array($v, $orderItems)) {
                    unset($orderBy[$k]);
                }
            }
        }
        $this->orderBy = $orderBy;
    }

    /**
     * @return array
     */
    public static function fetchOrderItems()
    {
        return array(
            self::ORDER_BY_ID_ASC => 'ID - ASC',
            self::ORDER_BY_ID_DESC => 'ID - DESC',
            self::ORDER_BY_NUME_ASC => 'Nume - ASC',
            self::ORDER_BY_NUME_DESC => 'Nume - DESC',
        );
    }

}