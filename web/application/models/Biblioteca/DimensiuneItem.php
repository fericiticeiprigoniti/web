<?php


class DimensiuneItem
{
    use ArrayOrJson;

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $latime;

    /**
     * @var int
     */
    private $inaltime;

    // ---------------------------------------------------------------------------------------------

    public function __construct(array $dbRow = array())
    {
        if (!count($dbRow)) {
            return;
        }
        $this->id = (int)$dbRow['id'];
        $this->latime = $dbRow['latime'];
        $this->inaltime = $dbRow['inaltime'];
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getLatime()
    {
        return $this->latime;
    }

    /**
     * @param int|null $latime
     * @throws Exception
     */
    public function setLatime($latime)
    {
        $this->latime = (int)$latime;
        if ($this->latime <= 0) {
            throw new Exception('Latimea este invalida');
        }
    }

    /**
     * @return int|null
     */
    public function getInaltime()
    {
        return $this->inaltime;
    }

    /**
     * @param int|null $inaltime
     * @throws Exception
     */
    public function setInaltime($inaltime)
    {
        $this->inaltime = (int)$inaltime;
        if ($this->inaltime <= 0) {
            throw new Exception('Inaltimea este invalida');
        }
    }

    /**
     * @param string $concatString
     * @return string
     */
    public function getName($concatString='x')
    {
        return $this->latime . $concatString . $this->inaltime;
    }

    /**
     * @return string
     */
    public function getSearchResult()
    {
        return $this->id . ' | ' . $this->getName();
    }

}