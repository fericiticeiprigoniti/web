<?php


class EdituraItem
{
    use ArrayOrJson;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $nume;

    /**
     * @var string
     */
    private $alias;

    /**
     * @var string
     */
    private $website;
    
    /**
     * @var int
     */
    private $localitateId;
    
    /**
     * @var LocalitateItem
     */
    private $localitate;

    /**
     * @var string
     */
    private $localitateOld;

    /**
     * @var bool
     */
    private $isDeleted;

    // ---------------------------------------------------------------------------------------------

    public function __construct(array $dbRow = [])
    {
        if ( !count($dbRow) ) {
            return;
        }

        $this->id = $dbRow['id'] ? (int)$dbRow['id'] : null;
        $this->nume = $dbRow['nume'] ? (string)$dbRow['nume'] : null;
        $this->website = $dbRow['website'] ? (string)$dbRow['website'] : null;
        $this->localitateNumeBackup = $dbRow['localitate_nume_backup'] ? (string)$dbRow['localitate_nume_backup'] : null;
        $this->localitateId = $dbRow['loc_id'] ? (int)$dbRow['loc_id'] : null;
        $this->alias = $dbRow['alias'] ? (string)$dbRow['alias'] : null;
        $this->isDeleted = $dbRow['is_deleted'] ? (int)$dbRow['is_deleted'] : null;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getNume()
    {
        return $this->nume;
    }

    /**
     * @param string|null $nume
     * @throws Exception
     */
    public function setNume($nume)
    {
        $this->nume = trim(strip_tags($nume));
        if (empty($this->nume)) {
            throw new Exception('Completati numele editurii');
        }
    }

    /**
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param string $website
     */
    public function setWebsite($website)
    {
        $this->website = $website ? (string)$website : null;
    }


    /**
     * @return string|null
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param string|null $alias
     * @throws Exception
     */
    public function setAlias($alias)
    {
        $this->alias = $alias ? trim(strip_tags($alias)) : null;
    }

    /**
     * @return int|null
     */
    public function getLocalitateId()
    {
        return $this->localitateId;
    }
    
    /**
     * @param int|null $localitateId
     * @throws Exception
     */
    public function setLocalitateId($localitateId)
    {
        $this->localitateId = $localitateId ? (int)$localitateId : null;
        if ($this->localitateId) {
            $this->localitate = LocalitateTable::getInstance()->load($this->localitateId);
            if (!$this->localitate) {
                throw new Exception('Localitatea nu a fost gasit in baza de date');
            }
        }
    }
    
    /**
     * @return LocalitateItem|null
     */
    public function getLocalitate()
    {
        if ($this->localitate) {
            return $this->localitate;
        } elseif ($this->localitateId) {
            $this->localitate = LocalitateTable::getInstance()->load($this->localitateId);
            return $this->localitate;
        } else {
            return null;
        }
    }
    
    /**
     * @return string|null
     */
    public function getLocalitateOld()
    {
        return $this->localitateOld;
    }

    /**
     * @return boolean
     */
    public function isDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @var boolean $value
     */
    public function setIsDeleted($value)
    {
        $this->isDeleted = (bool)$value;
    }

    /**
     * @return string
     */
    public function getSearchResult()
    {
        return $this->id . ' | ' . $this->nume . ' | ' . $this->alias;
    }

}