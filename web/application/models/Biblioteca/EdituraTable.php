<?php

/**
 * Class EdituraTable
 * @table fcp_biblioteca_edituri
 */
class EdituraTable extends CI_Model
{

    /**
     * Used for caching items
     * @var EdituraItem[]
     */
    public static $collection = [];

    /**
     * @var EdituraTable
     */
    private static $instance;

    /**
     * Singleton
     * EdituraTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    // ---------------------------------------------------------------------------------------------

    /**
     *  we can later dependency inject and thus unit test this
     * @return EdituraTable
     */
    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param int $id
     */
    public static function resetCollectionId($id)
    {
        if (isset(self::$collection[$id])) {
            unset(self::$collection[$id]);
        }
    }

    /**
     * @param int $id
     * @return EdituraItem|null
     * @throws Exception
     */
    public function load($id)
    {
        $id = (int)$id;

        if (!$id) {
            throw new Exception('ID editura este invalid');
        }

        // If data is cached return it
        if (MODEL_CACHING_ENABLED && isset(self::$collection[$id])) {
            return self::$collection[$id];
        }

        $filters = new EdituraFilters(array('id'=>$id));
        $data = $this->fetchAll($filters);

        self::$collection[$id] = $data['items'][0] ?? null;

        return self::$collection[$id];
    }

    /**
     * Reset item from static collection and load a new one
     * @param mixed $id
     * @return EdituraItem|null
     * @throws Exception
     */
    public function reload($id)
    {
        self::resetCollectionId($id);
        return $this->load($id);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param EdituraFilters|null $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchAll(EdituraFilters $filters = null, $limit = null, $offset = null)
    {
        // Set where and having conditions.
        $sqlWhere = [];
        $sqlHaving = [];
        if ($filters) {
            if ($filters->getId()) {
                $sqlWhere[] = "editura.id = {$filters->getId()}";
            }
            if ($filters->getExcludedId()) {
                $sqlWhere[] = "editura.id <> {$filters->getExcludedId()}";
            }
            if ($filters->getNumeLike()) {
                $sqlWhere[] = "editura.nume LIKE " . $this->db->escape("%{$filters->getNumeLike()}%");
            }
            if ($filters->getAliasLike()) {
                $sqlWhere[] = "editura.alias LIKE " . $this->db->escape("%{$filters->getAliasLike()}%");
            }
            if ($filters->getLocalitateId()) {
                $sqlWhere[] = "editura.loc_id = {$filters->getLocalitateId()}";
            }
            if (!is_null($filters->isDeleted())) {
                if ($filters->isDeleted()) {
                    $sqlWhere[] = "editura.is_deleted > 0";
                } else {
                    $sqlWhere[] = "(editura.is_deleted IS NULL OR editura.is_deleted = 0)";
                }
            }
            if ($filters->getSearchTerm()) {
                // Clean search term, extract words and set where condition.
                $searchTerm = Strings::sanitizeForDatabaseUsage($filters->getSearchTerm());
                $words = Strings::splitIntoWords($searchTerm);
                if (count($words)) {
                    $subCond = array();
                    foreach ($words as $word) {
                        $word = $this->db->escape("%$word%");
                        $subCond[] = "(editura.id LIKE $word OR editura.nume LIKE $word OR editura.alias LIKE $word)";
                    }
                    $sqlWhere[] = implode("\n\tAND ", $subCond);
                }
            }
        }
        $sqlWhere = implode("\n\tAND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sqlHaving = implode("\n\tAND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? "HAVING " . $sqlHaving : '';

        // ------------------------------------

        // Set order by.
        $sqlOrder = array();
        if (count($filters->getOrderBy())) {
            foreach($filters->getOrderBy() as $ord) {
                switch($ord) {
                    case $filters::ORDER_BY_ID_ASC:
                        $sqlOrder[] = "id ASC";
                        break;
                    case $filters::ORDER_BY_ID_DESC:
                        $sqlOrder[] = "id DESC";
                        break;
                    case $filters::ORDER_BY_NUME_ASC:
                        $sqlOrder[] = "nume ASC";
                        break;
                    case $filters::ORDER_BY_NUME_DESC:
                        $sqlOrder[] = "nume DESC";
                        break;
                }
            }
        }
        if (!count($sqlOrder)) {
            $sqlOrder[] = "id ASC";
        }
        $sqlOrder = implode(", ", $sqlOrder);

        // ------------------------------------

        // Set limit.
        $sqlLimit = '';
        if (!is_null($offset) && !is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$offset . ', ' . (int)$limit;
        } elseif (!is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$limit;
        }

        // ------------------------------------

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    editura.*
FROM
    fcp_biblioteca_edituri AS editura
WHERE
    1
    $sqlWhere
GROUP BY editura.id
$sqlHaving
ORDER BY $sqlOrder
$sqlLimit
EOSQL;

        /** @var CI_DB_result $result */
        $result = $this->db->query($sql);
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        $items = [];
        foreach($result->result('array') as $row){
            $items[] = new EdituraItem($row);
        }

        return [
            'items' => $items,
            'count' => $count,
        ];
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param EdituraItem $item
     * @return bool
     */
    public function save(EdituraItem $item)
    {
        // Set data.
        $eData = [
            'nume' => $item->getNume() ?: null,
            'website' => $item->getWebsite() ?: null,
            'loc_id' => $item->getLocalitateId() ?: null,
            'alias' => $item->getAlias() ?: null,
            'is_deleted' => $item->isDeleted() ? 1 : 0,
        ];

        // Insert/Update.
        if (!$item->getId()) {
            $res = $this->db->insert('fcp_biblioteca_edituri', $eData);
        } else {
            $res = $this->db->update('fcp_biblioteca_edituri', $eData, 'id = ' . $item->getId());
        }

        // Remove old item from cache
        self::resetCollectionId($item->getId());

        return $res;
    }

    /**
     * @param EdituraItem $item
     * @return mixed
     */
    public function delete(EdituraItem $item)
    {
        // Remove old item from cache
        self::resetCollectionId($item->getId());

        return $this->db->delete('fcp_biblioteca_edituri', 'id = ' . $item->getId());
    }

    // ---------------------------------------------------------------------------------------------
    
    /**
     * @param string $searchTerm
     * @param int $limit
     * @return array|EdituraItem[]
     */
    public function fetchForAutocomplete($searchTerm, $limit=LIMIT_FOR_AUTOCOMPLETE)
    {
        $filters = new EdituraFilters(['searchTerm'=>$searchTerm]);
        $data = $this->fetchAll($filters, $limit);

        return $data['items'] ?? [];
    }
    
    // ---------------------------------------------------------------------------------------------
    
}