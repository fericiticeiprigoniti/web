<?php


class CategorieCarteItem
{

    use ArrayOrJson;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $nume;

    /**
     * @var int
     */
    private $orderId;

    /**
     * @var int
     */
    private $parentId;

    /**
     * @var CategorieCarteItem
     */
    private $parent;

    // ---------------------------------------------------------------------------------------------

    public function __construct(array $dbRow = array())
    {
        if (!count($dbRow)) {
            return;
        }
        $this->id = (int)$dbRow['id'];
        $this->nume = $dbRow['nume'];
        $this->orderId = $dbRow['order_id'] ? (int)$dbRow['order_id'] : null;
        $this->parentId = $dbRow['parent_id'] ? (int)$dbRow['parent_id'] : null;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getNume()
    {
        return $this->nume;
    }

    /**
     * @param string|null $nume
     * @throws Exception
     */
    public function setNume($nume)
    {
        $this->nume = trim(strip_tags($nume));
        if (empty($this->nume)) {
            throw new Exception('Completati numele');
        } else {
            // Verificam duplicat.
            $filters = new CategorieCarteFilters();
            $filters->setExcludedId($this->id);
            $filters->setNume($this->nume);
            $filters->setParentId($this->parentId);
            $res = CategorieCarteTable::getInstance()->fetchAll($filters);
            if (isset($res['items']) && count($res['items'])) {
                throw new Exception('Numele categoriei nu este unic' . ($this->parentId ? ', pentru parintele selectat' : ''));
            }
        }
    }
    
    /**
     * @return int|null
     */
    public function getOrderId()
    {
        return $this->orderId;
    }
    
    /**
     * @param int|null $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId ? (int)$orderId : null;
    }

    /**
     * @return int|null
     */
    public function getParentId()
    {
        return $this->parentId;
    }
    
    /**
     * @param int|null $parentId
     * @throws Exception
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId ? (int)$parentId : null;
        $this->parent = CategorieCarteTable::getInstance()->load($this->parentId);
        if (!is_null($this->parentId) && !$this->parent) {
            throw new Exception('Categoria parinte este invalida');
        }
    }

    public function getParent()
    {
        if ($this->parent) {
            return $this->parent;
        } elseif ($this->parentId) {
            $this->parent = CategorieCarteTable::getInstance()->load($this->parentId);
            return $this->parent;
        } else {
            return null;
        }
    }
    
    // ---------------------------------------------------------------------------------------------

    /**
     * @return string
     */
    public function getSearchResult()
    {
        return $this->id . ' | ' . $this->nume;
    }
    
    /**
     * @return bool
     */
    public function foundUsages()
    {
        if (!$this->getId()) {
            return false;
        }
        
        $filters = new CarteFilters();
        $filters->setCategorieId($this->getId());
        $result = CarteTable::getInstance()->fetchAll($filters, 1);
        return isset($result['count']) && $result['count'];
    }

}