<?php

class BibliografieItem extends CI_Model
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $titlu;

    /**
     * @var string
     */
    private $descriere;

    /**
     * @var int
     */
    private $isDeleted;

    /**
     * @var int
     */
    private $isPublished;

    /**
     * Lista cu casti asociate
     *
     * @var array
     */
    private $arrCarti;


    // ---------------------------------------------------------------------------------------------

    public function __construct(array $dbRow = [])
    {
        if ( !count($dbRow) ) {
            return;
        }

        $this->id = $dbRow['id'] ? (int)$dbRow['id'] : null;
        $this->titlu = $dbRow['titlu'] ? (string)$dbRow['titlu'] : null;
        $this->descriere = $dbRow['descriere'] ? (string)$dbRow['descriere'] : null;
        $this->isDeleted = $dbRow['is_deleted'] ? (int)$dbRow['is_deleted'] : null;
        $this->isPublished = $dbRow['is_published'] ? (int)$dbRow['is_published'] : null;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return string
     */
    public function getTitlu()
    {
        return $this->titlu;
    }

    /**
     * @param string $titlu
     */
    public function setTitlu($titlu)
    {
        $this->titlu = $titlu ? (string)$titlu : null;
    }

    /**
     * @return string
     */
    public function getDescriere()
    {
        return $this->descriere;
    }

    /**
     * @param string $descriere
     */
    public function setDescriere($descriere)
    {
        $this->descriere = $descriere ? (string)$descriere : null;
    }

    /**
     * @return int
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param int $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted ? (int)$isDeleted : null;
    }

    /**
     * @return int
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }

    /**
     * @param int $isPublished
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished ? (int)$isPublished : null;
    }

    /**
     * @return int
     */
    public function getCarti()
    {
        if(!$this->arrCarti && $this->getId()){
            $results = $this->db->query('SELECT * FROM fcp_biblioteca_bibliografii2carti WHERE bibliografie_id = ?', [$this->getId()])->result_array();

            if($results){
                $ids = [];
                foreach($results as $item){
                    $ids[] = $item['carte_id'];
                }
                $this->arrCarti = $ids;
            }
        }

        return $this->arrCarti;
    }

    /**
     * @param int $array
     */
    public function setCarti($carti)
    {
        $this->arrCarti = $carti;
    }


}
