<?php

/**
 * Class CategorieCarteTable
 * @table fcp_biblioteca_carti_categorii
 */
class CategorieCarteTable extends CI_Model
{

    /**
     * @var CategorieCarteTable
     */
    private static $instance;


    /**
     * Singleton
     * CategorieCarteTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *  we can later dependency inject and thus unit test this
     * @return CategorieCarteTable
     */
    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param CategorieCarteItem $categorie
     * @return bool|int
     */
    public function save(CategorieCarteItem $categorie)
    {
        // Set data.
        $eData = array(
            'nume' => $categorie->getNume(),
            'order_id' => $categorie->getOrderId(),
            'parent_id' => $categorie->getParentId(),
        );
        
        // Insert/Update.
        if (!$categorie->getId()) {
            $res = $this->db->insert('fcp_biblioteca_carti_categorii', $eData);
        } else {
            $res = $this->db->update('fcp_biblioteca_carti_categorii', $eData, "id = " . $categorie->getId());
        }
        
        // Return result.
        return $res;
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * @param CategorieCarteItem $categorie
     * @return mixed
     * @throws Exception
     */
    public function delete(CategorieCarteItem $categorie)
    {
        // Verifica daca aceasta categorie este folosita.
        if ($categorie->foundUsages()) {
            throw new Exception("Categoria este folosita in relatia cu alte elemente");
        }
    
        return $this->db->delete('fcp_biblioteca_carti_categorii', "id = " . $categorie->getId());
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * @param int $id
     * @return CategorieCarteItem|null
     * @throws Exception
     */
    public function load($id)
    {
        $id = (int)$id;
        if (!$id) {
            throw new Exception('ID categorie carte este invalid');
        }
        $filters = new CategorieCarteFilters(array('id'=>$id));
        $data = $this->fetchAll($filters);
        $item = isset($data['items']) && isset($data['items'][0]) ? $data['items'][0] : null;
        return $item;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param CategorieCarteFilters $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchAll(CategorieCarteFilters $filters = null, $limit = null, $offset = null)
    {

        // Set where and having conditions.
        $sqlWhere = array();
        $sqlHaving = array();
        if ($filters) {
            if ($filters->getId()) {
                $sqlWhere[] = "categ.id = {$filters->getId()}";
            }

            if ($filters->getExcludedId()) {
                $sqlWhere[] = "categ.id <> {$filters->getExcludedId()}";
            }
            if ($filters->getNume()) {
                $sqlWhere[] = "categ.nume LIKE " . $this->db->escape($filters->getNume());
            }
            if (!is_null($filters->getParentId())) {
                $sqlWhere[] = "categ.parent_id = {$filters->getParentId()}";
            }
            if ($filters->getSearchTerm()) {
                // Clean search term, extract words and set where condition.
                $searchTerm = Strings::sanitizeForDatabaseUsage($filters->getSearchTerm());
                $words = Strings::splitIntoWords($searchTerm);
                if (count($words)) {
                    $subCond = array();
                    foreach ($words as $word) {
                        $word = $this->db->escape("%$word%");
                        $subCond[] = "(categ.id LIKE $word OR categ.nume LIKE $word)";
                    }
                    $sqlWhere[] = implode("\n\tAND ", $subCond);
                }
            }
        }
        $sqlWhere = implode("\n\tAND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sqlHaving = implode("\n\tAND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? "HAVING " . $sqlHaving : '';

        // ------------------------------------

        // Set order by.
        $sqlOrder = array();
        if ($filters && count($filters->getOrderBy())) {
            foreach($filters->getOrderBy() as $ord) {
                switch($ord) {
                    case $filters::ORDER_BY_ID_ASC:
                        $sqlOrder[] = "id ASC";
                        break;
                    case $filters::ORDER_BY_ID_DESC:
                        $sqlOrder[] = "id DESC";
                        break;
                    case $filters::ORDER_BY_NUME_ASC:
                        $sqlOrder[] = "nume ASC";
                        break;
                    case $filters::ORDER_BY_NUME_DESC:
                        $sqlOrder[] = "nume DESC";
                        break;
                    case $filters::ORDER_BY_PARENT_ASC:
                        $sqlOrder[] = "parinte ASC";
                        break;
                    case $filters::ORDER_BY_PARENT_DESC:
                        $sqlOrder[] = "parinte DESC";
                        break;
                    case $filters::ORDER_BY_ORDER_ASC:
                        $sqlOrder[] = "order_id ASC";
                        break;
                    case $filters::ORDER_BY_ORDER_DESC:
                        $sqlOrder[] = "order_id DESC";
                        break;
                }
            }
        }
        if (!count($sqlOrder)) {
            $sqlOrder[] = "parinte, order_id, nume";
        }
        $sqlOrder = implode(", ", $sqlOrder);

        // ------------------------------------

        // Set limit.
        $sqlLimit = '';
        if (!is_null($offset) && !is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$offset . ', ' . (int)$limit;
        } elseif (!is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$limit;
        }

        // ------------------------------------

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    categ.*
    , parent.nume AS parinte
FROM
    fcp_biblioteca_carti_categorii AS categ
    LEFT JOIN fcp_biblioteca_carti_categorii AS parent ON categ.parent_id = parent.id
WHERE
    1
    $sqlWhere
GROUP BY categ.id
$sqlHaving
ORDER BY $sqlOrder
$sqlLimit
EOSQL;

        /** @var CI_DB_result $result */
        $result = $this->db->query($sql);
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        $items = array();
        foreach($result->result('array') as $row){
            $items[] = new CategorieCarteItem($row);
        }

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------
    
    /**
     * @param CategorieCarteFilters|null $filters
     * @return array
     */
    public function fetchForSelect(CategorieCarteFilters $filters = null)
    {
        $result = $this->fetchAll($filters);
        $data = array();
        if (isset($result['items']) && count($result['items'])) {
            /** @var CategorieCarteItem $categ */
            foreach($result['items'] as $categ) {
                $data[$categ->getId()] = $categ->getNume();
            }
        }
        return $data;
    }

     /**
     * @param CategorieCarteFilters|null $filters
     * @return array
     */
    public function fetchForSelectWithParent(CategorieCarteFilters $filters = null)
    {
        $result = $this->fetchAll($filters);
        $data = array();
        if (isset($result['items']) && count($result['items'])) {
            /** @var CategorieCarteItem $categ */
            foreach($result['items'] as $categ) {
                $data[$categ->getId()] = (!is_null($categ->getParent())? $categ->getParent()->getNume() .' | ' : '') . $categ->getNume();
            }
        }
        return $data;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param string $searchTerm
     * @param int $limit
     * @return array|CategorieCarteItem[]
     */
    public function fetchForAutocomplete($searchTerm, $limit=LIMIT_FOR_AUTOCOMPLETE)
    {
        $filters = new CategorieCarteFilters(array('searchTerm'=>$searchTerm));
        $data = $this->fetchAll($filters, $limit);
        return isset($data['items']) ? $data['items'] : array();
    }

    // ---------------------------------------------------------------------------------------------
    
    /**
     * @return CategorieCarteItem[]|null
     */
    public function fetchParents()
    {
        $filters = new CategorieCarteFilters();
        $filters->setParentId(0);
        $data = $this->fetchAll($filters);
        $result = isset($data['items']) ? $data['items'] : null;
        return $result;
    }
    
    // ---------------------------------------------------------------------------------------------

    /**
     * @return array
     */
    public function fetchParentsForSelect()
    {
        $result = $this->fetchParents();
        $data = array();
        if ($result) {
            foreach($result as $categ) {
                $data[$categ->getId()] = $categ->getNume();
            }
        }
        return $data;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param CategorieCarteFilters|null $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchForApi(CategorieCarteFilters $filters = null, $limit = null, $offset = null)
    {
        $data = $this->fetchAll($filters, $limit, $offset);
        $return = array();
        if (isset($data['items']) && count($data['items'])) {
            /** @var CategorieCarteItem $item */
            foreach($data['items'] as $item) {
                $return[] = $item->toArray();
            }
        }
        return $return;
    }


}