<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_locuri_patimire_citate extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Returneaza lista de citate
     * @param int $id
     * @param bool $keyValue
     * @return array
     */
    public function fetchAll($id, $keyValue = false)
    {
        $sql = <<<EOSQL
SELECT 
    citat.*
    , carte.titlu AS titlu_carte
FROM 
    fcp_locuri_patimire_citate as citat
    LEFT JOIN fcp_biblioteca_carti AS carte ON citat.carte_id = carte.id
WHERE 
    citat.loc_patimire_id = ?
EOSQL;
;

        if ($keyValue) {
            $result = array();
            $query = $this->db->query($sql, array((int)$id));
            foreach ($query->result() as $row) {
                $result[$row->id] = $row->nume;
            }
        } else {
            $result = $this->db->query($sql,array((int)$id))->result_array();
        }
        return $result;
    }

    /**
     * adauga un citat - unui personaj
     *
     * @param mixed $id
     * @param mixed $data
     * @return bool
     */
    public function add($id, $data)
    {
        return $this->db->insert('fcp_locuri_patimire_citate', array('loc_patimire_id'    => (int)$id,
                                                               'personaj_id'    => 0 , // TODO - set proper value
                                                               'content'       => $data['content'],
                                                               'carte_id'       => $data['carte_id'],
                                                               'sursa'          => $data['sursa'],
                                                               'pag_start'      => $data['pag_start'],
                                                               'pag_end'        => $data['pag_end'],
        ));
    }

    function update($id, $data)
    {
        return $this->db->update('fcp_locuri_patimire_citate', $data, array('id' => $id));
    }

    public function delete($locPatimireId, $citatId)
    {
        return $this->db->delete('fcp_locuri_patimire_citate', array('loc_patimire_id' => (int)$locPatimireId,
                                                               'id'          => (int)$citatId));
    }

    function getInfo($citatID)
    {
        return $this->db->get_where('fcp_locuri_patimire_citate', array('id' => (int)$citatID))->row_array();
    }
}