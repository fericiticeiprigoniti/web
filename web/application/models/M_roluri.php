<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_roluri extends CI_Model {


    const TABLE = 'fcp_personaje_roluri';


    function __construct()
    {
        parent::__construct();
    }

    /**
     * Returneaza lista de roluri
     * @param bool $keyValue
     * @return array
     */
    public function fetchAll($keyValue=false)
    {
        $sql = "SELECT * FROM fcp_personaje_roluri
                WHERE parent_id != 0
                ORDER BY parent_id, order_id";

        if ($keyValue) {
            $result = array();
            $query = $this->db->query($sql);
            foreach ($query->result() as $row) {
                $result[$row->id] = $row->rol_nume;
            }
        } else {
            $result = $this->db->query($sql)->result_array();
        }
        return $result;
    }

    /**
     * GET roluri by Parent ID
     * @param mixed $parentID
     * @param mixed $keyValue
     * @return array
     */
    public function getRoluri($parentID, $keyValue = false)
    {
        $parentID = (int)$parentID;

        $sql = "SELECT * FROM fcp_personaje_roluri
                WHERE parent_id = ?
                ORDER BY parent_id, order_id";

        if ($keyValue) {
            $result = array();
            $query = $this->db->query($sql, array($parentID));
            foreach ($query->result() as $row) {
                $result[$row->id] = $row->rol_nume;
            }
        } else {
            $result = $this->db->query($sql, array($parentID))->result_array();
        }
        return $result;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Fetch list
     * @param array $filters
     * @return array
     */
    public function fetchList($filters=array())
    {
        $sqlWhere = array();
        if (isset($filters['id']) && !empty($filters['id'])) {
            $sqlWhere[] = "rol.id = " . (int)$filters['id'];
        }
        if (isset($filters['exclude_id']) && !empty($filters['exclude_id'])) {
            $sqlWhere[] = "rol.id <> " . (int)$filters['exclude_id'];
        }
        if (isset($filters['parent_id']) && !empty($filters['parent_id'])) {
            $sqlWhere[] = "rol.parent_id = " . (int)$filters['parent_id'];
        }
        if (isset($filters['rol_nume']) && !empty($filters['rol_nume'])) {
            $sqlWhere[] = "rol.rol_nume LIKE " . $this->db->escape($filters['rol_nume']);
        }
        $sqlWhere = count($sqlWhere) ? implode("\n\tAND ", $sqlWhere) : '';
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sql = <<<EOSQL
SELECT
    rol.*
    , parent.rol_nume AS parinte
    , COUNT(p2r.id) AS is_used
FROM
    fcp_personaje_roluri AS rol
    LEFT JOIN fcp_personaje_roluri AS parent ON rol.parent_id = parent.id
    LEFT JOIN fcp_personaje2roluri AS p2r ON rol.id = p2r.rol_id
WHERE
    1
    $sqlWhere
GROUP BY
    rol.id
ORDER BY
    parinte, rol_nume;
EOSQL;
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    /**
     * Load element by id
     * @param int $id
     * @return array
     */
    public function load($id)
    {
        $data = $this->fetchList(array('id'=>$id));
        return isset($data[0]) ? $data[0] : array();
    }

    /**
     * @param string $name
     * @param int|null $excludedId
     * @return array
     */
    public function fetchByName($name, $excludedId = null)
    {
        return $this->fetchList(array('rol_nume'=>$name, 'exclude_id'=>$excludedId));
    }

    /**
     * Fetch parent items
     * @param bool $keyValue
     * @return array
     */
    public function fetchParents($keyValue=false)
    {
        $sql = "SELECT * FROM fcp_personaje_roluri
                WHERE parent_id = 0
                ORDER BY rol_nume";

        if ($keyValue)
        {
            $result = array();
            $query = $this->db->query($sql);
            foreach ($query->result() as $row) {
                $result[$row->id] = $row->rol_nume;
            }
        } else {
            $result = $this->db->query($sql)->result_array();
        }
        return $result;
    }

    public function save($data, $id=null)
    {
        $id = (int)$id;
        $eData = array(
            'rol_nume'      => $data['rol_nume'],
            'rol_detalii'   => !empty($data['rol_detalii']) ? $data['rol_detalii'] : null,
            'parent_id'     => (int)$data['parent_id'],
        );

        if (!$id)
        {
            $eData['order_id'] = 999;
            $res = $this->db->insert(self::TABLE, $eData);
        } else {
            $res = $this->db->update(self::TABLE, $eData, array('id'=>$id));
        }

        return $res;
    }

    public function delete($id)
    {
        $id  = (int)$id;
        $res = $this->db->delete(self::TABLE, array('id'=>$id));

        return $res;
    }

    public function fetchForSelect($filters)
    {
        $res  = $this->fetchList($filters);
        $data = array();

        if (count($res))
        {
            foreach($res as $v) {
                $data[$v['id']] = $v['rol_nume'];
            }
        }
        return $data;
    }

}