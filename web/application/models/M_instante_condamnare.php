<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_instante_condamnare extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    /**
    * Returneaza lista de ocupatii
    * @param bool $keyValue
    * @return array
    */
    public function fetchAll($keyValue=false)
    {
        $sql = "SELECT *
                FROM fcp_instante_condamnare as ic
                LEFT JOIN nom_localitati as loc ON loc.id = ic.loc_id
                ORDER BY ic.nume ASC";

        if ($keyValue) {
            $result = array();
            $query = $this->db->query($sql);
            foreach ($query->result() as $row) {
                $result[$row->id] = $row->nume;
            }
        } else {
            $result = $this->db->query($sql)->result_array();
        }
        return $result;
    }
}