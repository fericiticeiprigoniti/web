<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_personaje_itinerarii extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * GET itinerarii by Personaj ID
     * @param mixed $persID
     * @return array
     */
    public function fetchAll($persID)
    {
        $this->load->model('m_personaje_condamnari');
        $nomenclator_tip_represiune = $this->m_personaje_condamnari->fetchPersonajeDetentieIntrareTip();
    
        $sql = "SELECT 
                    t1.*, 
                    t2.represiune_tip_id, t2.represiune_data_start, t2.represiune_data_end, t2.id as condamnare_id
                FROM fcp_personaje2locuripatimire as t1
                LEFT JOIN fcp_personaje_episoade_represive t2 ON t1.condamnare_id  = t2.id
                WHERE t1.personaj_id = ?
                ORDER BY order_id ASC";
    
        $result = $this->db->query($sql, [(int)$persID])->result_array();
    
        foreach ($result as &$item) {
            $data_start = $item['represiune_data_start'] ? date("d.m.Y", strtotime($item['represiune_data_start'])) : '--';
            $data_end = $item['represiune_data_end'] ? date("d.m.Y", strtotime($item['represiune_data_end'])) : '--';
    
            $item['condamnare'] = $item['condamnare_id']
                ? $nomenclator_tip_represiune[$item['represiune_tip_id']] . " ({$data_start} / {$data_end})"
                : '';
        }
    
        return $result;
    }
    
    

    public function add($fields)
    {
        return $this->db->insert('fcp_personaje2locuripatimire', $fields);
    }

    public function update($itemID, $fields)
    {
        return $this->db->update('fcp_personaje2locuripatimire', $fields, array('id' => (int)$itemID));
    }

    /**
     * delete itinerariu - Tab Itinerarii COndamnari
     * 
     * @param mixed $persID
     * @param mixed $itemID
     */
    public function delete($persID, $itemID)
    {
        return $this->db->delete('fcp_personaje2locuripatimire', array(
            'personaj_id' => (int)$persID,
            'id'          => (int)$itemID
        ));
    }
}
