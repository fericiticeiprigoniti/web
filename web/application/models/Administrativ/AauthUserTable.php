<?php

/**
 * Class AauthUserTable
 * @table aauth_users
 */
class AauthUserTable extends CI_Model
{

    /**
     * @var AauthUserTable
     */
    private static $instance;


    /**
     * Singleton
     * AauthUserTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * We can later dependency inject and thus unit test this
     * @return AauthUserTable
     */
    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param mixed $id
     * @return AauthUserItem|null
     * @throws Exception
     */
    public function load($id)
    {
        if (!$id) {
            throw new Exception("Invalid AauthUser id");
        }

        $filters = new AauthUserFilters(['id' => $id]);
        $data = $this->fetchAll($filters);

        return $data['items'][0] ?? null;
    }

    /**
     * @param int $id
     */
    public static function get($id): ?AauthUserItem
    {
        if (!$id) {
            return null;
        }

        try {
            return self::getInstance()->load($id);
        } catch (Throwable $e) {
            log_message('error', $e->getMessage());
            return null;
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param AauthUserFilters|null $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchAll(AauthUserFilters $filters = null, $limit = null, $offset = null)
    {
        // Set where and having conditions.
        $sqlWhere = array();
        $sqlHaving = array();
        if ($filters) {
            if ($filters->getId()) {
                $sqlWhere[] = "id = {$filters->getId()}";
            }
            if ($filters->getExcludedId()) {
                $sqlWhere[] = "id <> {$filters->getExcludedId()}";
            }
        }

        $sqlWhere = implode("\n\tAND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sqlHaving = implode("\n\tAND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? "HAVING " . $sqlHaving : '';

        // ------------------------------------

        $sqlOrder = array();
        if ($filters && count($filters->getOrderBy())) {
            foreach($filters->getOrderBy() as $ord) {
                switch($ord) {
                    case $filters::ORDER_BY_ID_ASC:
                        $sqlOrder[] = "id ASC";
                        break;
                    case $filters::ORDER_BY_ID_DESC:
                        $sqlOrder[] = "id DESC";
                        break;
        }
            }
        }
        if (!count($sqlOrder)) {
            $sqlOrder[] = "id DESC";
        }
        $sqlOrder = implode(", ", $sqlOrder);

        // ------------------------------------

        // Set limit.
        $sqlLimit = '';
        if (!is_null($offset) && !is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$offset . ', ' . (int)$limit;
        } elseif (!is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$limit;
        }

        // ------------------------------------

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    *
FROM
    aauth_users
WHERE
    1
    $sqlWhere
GROUP BY id
$sqlHaving
ORDER BY $sqlOrder
$sqlLimit
EOSQL;

        /** @var CI_DB_result $result */
        $result = $this->db->query($sql);
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        $items = array();
        foreach($result->result('array') as $row){
            $items[] = new AauthUserItem($row);
        }

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param AauthUserItem $item
     * @return bool|int
     */
    public function save(AauthUserItem $item)
    {
        // Set data.
        $eData = array(
            'email' => $item->getEmail() ? $item->getEmail() : null,
            'pass' => $item->getPass() ? $item->getPass() : null,
            'username' => $item->getUsername() ? $item->getUsername() : null,
            'banned' => $item->getBanned() ? $item->getBanned() : null,
            'last_login' => $item->getLastLogin() ? $item->getLastLogin() : null,
            'last_activity' => $item->getLastActivity() ? $item->getLastActivity() : null,
            'date_created' => $item->getDateCreated() ? $item->getDateCreated() : null,
            'forgot_exp' => $item->getForgotExp() ? $item->getForgotExp() : null,
            'remember_time' => $item->getRememberTime() ? $item->getRememberTime() : null,
            'remember_exp' => $item->getRememberExp() ? $item->getRememberExp() : null,
            'verification_code' => $item->getVerificationCode() ? $item->getVerificationCode() : null,
            'totp_secret' => $item->getTotpSecret() ? $item->getTotpSecret() : null,
            'ip_address' => $item->getIpAddress() ? $item->getIpAddress() : null,
        );

        // Insert/Update.
        if (!$item->getId()) {
            $res = $this->db->insert('aauth_users', $eData);
        } else {
            $res = $this->db->update('aauth_users', $eData, 'id = ' . $item->getId());
        }

        return $res;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param AauthUserItem $item
     * @return mixed
     */
    public function delete(AauthUserItem $item)
    {
        return $this->db->delete('aauth_users', 'id = ' . $item->getId());
    }

    // ---------------------------------------------------------------------------------------------


}
