<?php

class AauthUserItem extends CI_Model
{
    use ArrayOrJson;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $pass;

    /**
     * @var string
     */
    private $username;

    /**
     * @var int
     */
    private $banned;

    /**
     * @var string
     */
    private $lastLogin;

    /**
     * @var string
     */
    private $lastActivity;

    /**
     * @var string
     */
    private $dateCreated;

    /**
     * @var string
     */
    private $forgotExp;

    /**
     * @var string
     */
    private $rememberTime;

    /**
     * @var string
     */
    private $rememberExp;

    /**
     * @var string
     */
    private $verificationCode;

    /**
     * @var string
     */
    private $totpSecret;

    /**
     * @var string
     */
    private $ipAddress;


    // ---------------------------------------------------------------------------------------------

    public function __construct(array $dbRow = [])
    {
        if ( !count($dbRow) ) {
            return;
        }

        $this->id = $dbRow['id'] ? (int)$dbRow['id'] : null;
        $this->email = $dbRow['email'] ? (string)$dbRow['email'] : null;
        $this->pass = $dbRow['pass'] ? (string)$dbRow['pass'] : null;
        $this->username = $dbRow['username'] ? (string)$dbRow['username'] : null;
        $this->banned = $dbRow['banned'] ? (int)$dbRow['banned'] : null;
        $this->lastLogin = $dbRow['last_login'] ? (string)$dbRow['last_login'] : null;
        $this->lastActivity = $dbRow['last_activity'] ? (string)$dbRow['last_activity'] : null;
        $this->dateCreated = $dbRow['date_created'] ? (string)$dbRow['date_created'] : null;
        $this->forgotExp = $dbRow['forgot_exp'] ? (string)$dbRow['forgot_exp'] : null;
        $this->rememberTime = $dbRow['remember_time'] ? (string)$dbRow['remember_time'] : null;
        $this->rememberExp = $dbRow['remember_exp'] ? (string)$dbRow['remember_exp'] : null;
        $this->verificationCode = $dbRow['verification_code'] ? (string)$dbRow['verification_code'] : null;
        $this->totpSecret = $dbRow['totp_secret'] ? (string)$dbRow['totp_secret'] : null;
        $this->ipAddress = $dbRow['ip_address'] ? (string)$dbRow['ip_address'] : null;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email ? (string)$email : null;
    }

    /**
     * @return string
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @param string $pass
     */
    public function setPass($pass)
    {
        $this->pass = $pass ? (string)$pass : null;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username ? (string)$username : null;
    }

    /**
     * @return int
     */
    public function getBanned()
    {
        return $this->banned;
    }

    /**
     * @param int $banned
     */
    public function setBanned($banned)
    {
        $this->banned = $banned ? (int)$banned : null;
    }

    /**
     * @return string
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * @param string $lastLogin
     */
    public function setLastLogin($lastLogin)
    {
        $this->lastLogin = $lastLogin ? (string)$lastLogin : null;
    }

    /**
     * @return string
     */
    public function getLastActivity()
    {
        return $this->lastActivity;
    }

    /**
     * @param string $lastActivity
     */
    public function setLastActivity($lastActivity)
    {
        $this->lastActivity = $lastActivity ? (string)$lastActivity : null;
    }

    /**
     * @return string
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param string $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated ? (string)$dateCreated : null;
    }

    /**
     * @return string
     */
    public function getForgotExp()
    {
        return $this->forgotExp;
    }

    /**
     * @param string $forgotExp
     */
    public function setForgotExp($forgotExp)
    {
        $this->forgotExp = $forgotExp ? (string)$forgotExp : null;
    }

    /**
     * @return string
     */
    public function getRememberTime()
    {
        return $this->rememberTime;
    }

    /**
     * @param string $rememberTime
     */
    public function setRememberTime($rememberTime)
    {
        $this->rememberTime = $rememberTime ? (string)$rememberTime : null;
    }

    /**
     * @return string
     */
    public function getRememberExp()
    {
        return $this->rememberExp;
    }

    /**
     * @param string $rememberExp
     */
    public function setRememberExp($rememberExp)
    {
        $this->rememberExp = $rememberExp ? (string)$rememberExp : null;
    }

    /**
     * @return string
     */
    public function getVerificationCode()
    {
        return $this->verificationCode;
    }

    /**
     * @param string $verificationCode
     */
    public function setVerificationCode($verificationCode)
    {
        $this->verificationCode = $verificationCode ? (string)$verificationCode : null;
    }

    /**
     * @return string
     */
    public function getTotpSecret()
    {
        return $this->totpSecret;
    }

    /**
     * @param string $totpSecret
     */
    public function setTotpSecret($totpSecret)
    {
        $this->totpSecret = $totpSecret ? (string)$totpSecret : null;
    }

    /**
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * @param string $ipAddress
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress ? (string)$ipAddress : null;
    }

     //tested
    /**
     * Get User Variable by key
     * Return string of variable value or FALSE
     * @param string $key
     * @param int $user_id ; if not given current user
     * @return bool|string , FALSE if var is not set, the value of var if set
     */
    public function getUserVar( $key, $user_id = FALSE){

        if ( !$user_id )
            $user_id = $this->getId('id');

        $sql = "SELECT * FROM aauth_user_variables
                WHERE user_id = ? AND data_key = ?";

        if($result = $this->db->query($sql, [$user_id, $key])->row_array())
         return $result['value'];

        return false;
    }

    /**
     * Get fullname
     *
     * @return void
     */
    public function getFullname()
    {
       return $this->getUserVar('nume') . " ". $this->getUserVar('prenume');
    }

}
