<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_biblioteca_carti extends CI_Model
{

    /** Disponibil in format */
    const FORMAT_TIPARIT = 1;
    const FORMAT_ELECTRONIC = 2;
    const FORMAT_TIPARIT_ELECTRONIC = 3;


    function __construct()
    {
        parent::__construct();
    }

    // ---------------------------------------------------------------------------------------------

    public static function fetchFormatTypes()
    {
        return array(
            self::FORMAT_TIPARIT            => 'Tiparit',
            self::FORMAT_ELECTRONIC         => 'Electronic',
            self::FORMAT_TIPARIT_ELECTRONIC => 'Tiparit + Electronic',
        );
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Lista carti
     * @param array $filters
     * @param array $order
     * @param array $limit
     * @return array
     */
    public function fetchAll($filters=array(), $order=array(), $limit=array())
    {
        // Set filters.
        $sqlWhere = array();
        $sqlHaving = array();
        if (!empty($filters)) {
            foreach($filters as $k=>$v) {
                if ($v == '') {		// Preliminary check.
                    continue;
                }
                switch($k) {

                    //TODO - implement filters...

                }
            }
        }
        $sqlWhere = implode(" AND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? ' AND ' . $sqlWhere : '';

        $sqlHaving = implode(" AND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? ' HAVING ' . $sqlHaving : '';

        // -------------------------------

        // Set order.
        $sqlOrder = "id";
        $defOrderType = "ASC";
        $orderItems = array(
            'id', 'autori', 'titlu', 'categorie', 'editura', 'anul_publicatiei', 'disponibil_in_format'
        );
        if (isset($order['by']) && !empty($order['by']) && in_array($order['by'], $orderItems)) {
            $sqlOrder = $order['by'];
            switch ($sqlOrder) {
//                case 'nume_complet':
//                    $sqlOrder = "CONCAT_WS(' ', p.nume, p.prenume)";
//                    break;
            }
        }
        if (isset($order['dir']) && !empty($order['dir'])) {
            $sqlOrder .= ' ' . (in_array(strtoupper($order['dir']), array('ASC','DESC')) ? $order['dir'] : $defOrderType);
        } else {
            $sqlOrder .= ' ' . $defOrderType;
        }
        $sqlOrder = !empty($sqlOrder) ? "ORDER BY\n\t" . $sqlOrder : '';

        // -------------------------------

        // Set limit.
        if (isset($limit['start']) && isset($limit['per_page'])) {
            $start = (int)$limit['start'];
            $per_page = (int)$limit['per_page'];
            $sqlLimit = "LIMIT $start, $per_page";
        } elseif (isset($limit['start'])) {
            $start = (int)$limit['start'];
            $sqlLimit = "LIMIT $start";
        } else {
            $sqlLimit = '';
        }

        // -------------------------------

        // Get data (items and count).
        $sql = <<<sql
SELECT SQL_CALC_FOUND_ROWS
    carte.*
    , editura.nume AS editura
    , GROUP_CONCAT(autor.nume SEPARATOR ', ') AS autori
    , categ.nume AS categorie

FROM
    fcp_biblioteca_carti AS carte
    LEFT JOIN fcp_biblioteca_edituri AS editura ON editura.id = carte.editura_id
    LEFT JOIN fcp_biblioteca_autori2carti AS a2c ON a2c.carte_id = carte.id
    LEFT JOIN fcp_autori AS autor ON autor.id = a2c.autor_id
    LEFT JOIN fcp_biblioteca_carti_categorii AS categ ON carte.cat_id = categ.id
WHERE
    (a2c.rol_id IS NULL OR a2c.rol_id IN (1,2,3))
    $sqlWhere
GROUP BY
    carte.id
$sqlHaving
$sqlOrder
$sqlLimit
sql;
        $items = $this->db->query($sql)->result_array();
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Load element by id
     * @param int $id
     * @return array|bool
     */
    public function load($id)
    {
        $res = $this->fetchAll(array('id'=>$id));
        if (isset($res['items']) && isset($res['items'][0])) {
            return $res['items'][0];
        } else {
            return false;
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Returneaza lista cu carti
     * @param mixed $pageNO
     * @param bool $withPagination
     * @return array
     */
    public function fetchList($pageNO = false, $withPagination = false)
    {
        $pageNO     = $pageNO ? $pageNO : 1;
        $perPage    = $this->config->item('admin_items_per_page');
        $offset     = ($pageNO - 1) * $perPage;

        $sql = "SELECT carte.id as carte_id, carte.titlu as carte_titlu, carte.*, editura.nume as editura, GROUP_CONCAT(autor.nume SEPARATOR ', ') as autori
                FROM fcp_biblioteca_carti as carte
                LEFT JOIN fcp_biblioteca_edituri as editura ON editura.id = carte.editura_id
                LEFT JOIN fcp_biblioteca_autori2carti as a2c ON a2c.carte_id = carte.id
                LEFT JOIN fcp_autori as autor ON autor.id = a2c.autor_id
                WHERE (a2c.rol_id IS NULL OR a2c.rol_id IN (1,2,3))
                GROUP BY carte.id"
            . ($withPagination ? "LIMIT ?, ?" : "");

        if ($withPagination) {
            return $this->db->query($sql, array($offset, $perPage))->result_array();
        } else {
            return $this->db->query($sql)->result_array();
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
    * return and prepare all results for Dropdown
    */
    public function fetchAllForDropDown()
    {
        $list = $this->fetchList();

        $results = array();
        foreach($list as $item)
        {
            $results[$item['carte_id']] = generateSource($item['autori'],$item['carte_titlu'], $item['editia'], $item['editura']);
        }

        return $results;
    }

    // ---------------------------------------------------------------------------------------------

    /**
    * returneaza lista cu cartile in functie de filtrele selectate
    */
    function getAllWithFilters($limitStart = 0, $limitLength = 50, $columnsOrder , $search = false)
    {
        $cond[] = '(1)';
        $values = array();

        if($search) {
            $cond[] = '(autor.nume LIKE ? OR carte.titlu LIKE ? OR editura.nume LIKE ?)';
            $values[] = '%' . $search . '%';
            $values[] = '%' . $search . '%';
            $values[] = '%' . $search . '%';
        }
        $order = ($columnsOrder)? 'ORDER BY '. implode(' ,', $columnsOrder) : '';

        array_push($values, (int)$limitStart, (int)$limitLength);

        $sql = "SELECT carte.id as carte_id, carte.titlu as carte_titlu, carte.*, editura.nume as editura, GROUP_CONCAT(autor.nume SEPARATOR ', ') as autori
                FROM fcp_biblioteca_carti as carte
                LEFT JOIN fcp_biblioteca_edituri as editura ON editura.id = carte.editura_id
                LEFT JOIN fcp_biblioteca_autori2carti as a2c ON a2c.carte_id = carte.id
                LEFT JOIN fcp_autori as autor ON autor.id = a2c.autor_id
                WHERE (a2c.rol_id IS NULL OR a2c.rol_id IN (1,2,3)) AND ".implode(' AND ', $cond)."
                GROUP BY carte.id
                {$order}
                LIMIT ?, ?";

        return $this->db->query($sql, $values)->result_array();
    }

    // ---------------------------------------------------------------------------------------------


}
