<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Aceasta model are un singur scop, de a incarca clasa CI_Model.
 * Daca nu incarcam mai intai in memorie clasa CI_Model,
 * modelele incarcate automat gen Biblioteca/* sau Nomencloator/* nu vor functiona.
 */
class M_autoload extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

}
