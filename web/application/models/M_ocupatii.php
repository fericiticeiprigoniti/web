<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ocupatii extends CI_Model {

    const TABLE = 'fcp_personaje_ocupatii';


    function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Returneaza lista de ocupatii
     * @param bool $keyValue
     * @return array
     */
    public function fetchAll($keyValue=false)
    {
        $sql = "SELECT * FROM fcp_personaje_ocupatii ORDER BY nume";
        if ($keyValue) {
            $result = array();
            $query = $this->db->query($sql);
            foreach ($query->result() as $row) {
                $result[$row->id] = $row->nume;
            }
        } else {
            $result = $this->db->query($sql)->result_array();
        }
        return $result;
    }

    /**
     * GET ocupatii by Parent ID
     * @param mixed $parentID
     * @param mixed $keyValue
     * @return array
     */
    public function getOcupatii($parentID, $keyValue = false)
    {
        $parentID = (int)$parentID;

        $sql = "SELECT * FROM fcp_personaje_ocupatii
                WHERE parent_id = ?
                ORDER BY parent_id, nume";

        if ($keyValue) {
            $result = array();
            $query = $this->db->query($sql, array($parentID));
            foreach ($query->result() as $row) {
                $result[$row->id] = $row->nume;
            }
        } else {
            $result = $this->db->query($sql, array($parentID))->result_array();
        }
        return $result;
    }

    /**
     * Fetch list
     * @param array $filters
     * @return array
     */
    public function fetchList($filters=array())
    {
        $sqlWhere = array();
        if (isset($filters['id']) && !empty($filters['id'])) {
            $sqlWhere[] = "o.id = " . (int)$filters['id'];
        }
        if (isset($filters['exclude_id']) && !empty($filters['exclude_id'])) {
            $sqlWhere[] = "o.id <> " . (int)$filters['exclude_id'];
        }
        if (isset($filters['nume']) && !empty($filters['nume'])) {
            $sqlWhere[] = "o.nume LIKE " . $this->db->escape($filters['nume']);
        }
        $sqlWhere = count($sqlWhere) ? implode("\n\tAND ", $sqlWhere) : '';
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sql = <<<EOSQL
SELECT
    o.*
    , parent.nume AS parinte
    , COUNT(p2o.id) AS is_used
FROM
    fcp_personaje_ocupatii AS o
    LEFT JOIN fcp_personaje_ocupatii AS parent ON o.parent_id = parent.id
    LEFT JOIN fcp_personaje2ocupatii AS p2o ON o.id = p2o.ocupatie_id
WHERE
    1
    $sqlWhere
GROUP BY
    o.id
ORDER BY
    parinte, nume;
EOSQL;
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    /**
     * Load element by id
     * @param int $id
     * @return array
     */
    public function load($id)
    {
        $data = $this->fetchList(array('id'=>$id));
        return isset($data[0]) ? $data[0] : array();
    }

    /**
     * @param string $name
     * @param int|null $excludedId
     * @return array
     */
    public function fetchByName($name, $excludedId = null)
    {
        return $this->fetchList(array('nume'=>$name, 'exclude_id'=>$excludedId));
    }

    /**
     * Fetch parent items
     * @param bool $keyValue
     * @return array
     */
    public function fetchParents($keyValue=false)
    {
        $sql = "SELECT * FROM fcp_personaje_ocupatii
                WHERE parent_id = 0 OR parent_id IS NULL
                ORDER BY nume";

        if ($keyValue) {
            $result = array();
            $query = $this->db->query($sql);
            foreach ($query->result() as $row) {
                $result[$row->id] = $row->nume;
            }
        } else {
            $result = $this->db->query($sql)->result_array();
        }
        return $result;
    }

    public function save($data, $id=null)
    {
        $id = (int)$id;
        $eData = array(
            'nume' => $data['nume'],
            'parent_id' => (int)$data['parent_id'],
        );

        if (!$id) {
            $res = $this->db->insert(self::TABLE, $eData);
        } else {
            $res = $this->db->update(self::TABLE, $eData, array('id'=>$id));
        }
        return $res;
    }

    public function delete($id)
    {
        $id = (int)$id;
        $res = $this->db->delete(self::TABLE, array('id'=>$id));
        return $res;
    }


}