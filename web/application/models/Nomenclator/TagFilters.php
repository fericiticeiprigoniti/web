<?php

class TagFilters
{

    const ORDER_BY_ID_ASC       = 'id_asc';
    const ORDER_BY_ID_DESC      = 'id_desc';
    const ORDER_BY_NAME_ASC     = 'name_asc';
    const ORDER_BY_NAME_DESC    = 'name_desc';
    const ORDER_BY_RAND         = 'order_rand';

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $catId;

    /**
     * @var int
     */
    private $excludedId;

    /**
     * @var int[]
     */
    private $idList = [];

    /**
    * @var string
    * @example Cauta dupa un anumit nume
    */
    private $nume;

    /**
     * @var bool
     * @docs_ignore
     */
    private $isDeleted;

    /**
     * @var string
     * @example Suporta: id_asc / id_desc / name_asc / name_desc / order_rand
     */
    private $orderBy = [];

    /**
     * @var string
     * @example Cauta dupa indicele alfabetic (ex: B)
     * 
     */
    private $searchLetter;

    // ---------------------------------------------------------------------------------------------

    public function __construct(array $getData = [])
    {
        if (isset($getData['id'])) {
            $this->setId($getData['id']);
        }
        if (isset($getData['cat_id'])) {
            $this->setCatId($getData['cat_id']);
        }
        if (isset($getData['excludedId'])) {
            $this->setExcludedId($getData['excludedId']);
        }
        if (isset($getData['idList'])) {
            $this->setIdList($getData['idList']);
        }
        if (isset($getData['nume'])) {
            $this->setNume($getData['nume']);
        }

        if (isset($getData['_orderBy'])) {
            $this->setOrderBy($getData['_orderBy']);
        }

        if (isset($getData['searchLetter'])) {
            $this->setSearchLetter($getData['searchLetter']);
        }

        // !!! TODO - sa face join cu poeziile 
        // sa afisam doar tagurile care apar in poezii 
        if (isset($getData['usedInPoems'])) {
            $this->setNume($getData['usedInPoems']);
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return int
     */
    public function getCatId()
    {
        return $this->catId;
    }

    /**
     * @param int $catId
     */
    public function setCatId($catId)
    {
        $this->catId = $catId ? (int)$catId : null;
    }

    /**
     * @return int
     */
    public function getExcludedId()
    {
        return $this->excludedId;
    }

    /**
     * @param int $excludedId
     */
    public function setExcludedId($excludedId)
    {
        $this->excludedId = $excludedId ? (int)$excludedId : null;
    }

    /**
     * @return int[]
     */
    public function getIdList()
    {
        return $this->idList;
    }

    /**
     * @param int[] $idList
     */
    public function setIdList($idList)
    {
        $this->idList = !is_array($idList) ? [(int)$idList] : array_map('intval', $idList);
    }

    /**
     * @return string
     */
    public function getNume()
    {
        return $this->nume;
    }

    /**
     * @param string $nume
     */
    public function setNume($nume)
    {
        $this->nume = $nume ? $nume : null;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param bool $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = (bool)$isDeleted;
    }

    // ---------------------------------------------------------------------------------------------

      /**
     * @return string|null
     */
    public function getSearchLetter()
    {
        return $this->searchLetter;
    }

    /**
     * @param string|null $val
     */
    public function setSearchLetter($val)
    {
        $this->searchLetter = is_null($val) ? null : trim(strip_tags($val));
    }
    
    /**
     * @return array
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param array $orderBy
     */
    public function setOrderBy(array $orderBy)
    {
        $orderBy = (is_array($orderBy) && count($orderBy) ? $orderBy : array());
        if (count($orderBy)) {
            $orderItems = array_keys(self::fetchOrderItems());
            foreach ($orderBy as $k=>$v) {
                if (!in_array($v, $orderItems)) {
                    unset($orderBy[$k]);
                }
            }
        }
        $this->orderBy = $orderBy;
    }

    /**
     * @return array
     */
    public static function fetchOrderItems()
    {
        return array(
            self::ORDER_BY_ID_ASC       => 'Id - ASC',
            self::ORDER_BY_ID_DESC      => 'Id - DESC',
            self::ORDER_BY_NAME_ASC     => 'Nume - ASC',
            self::ORDER_BY_NAME_DESC    => 'Nume - DESC',
            self::ORDER_BY_RAND    => 'Order - Rand',
        );
    }


}
