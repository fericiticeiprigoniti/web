<?php

/**
 * Class JudetTable
 * @table nom_judete
 */
class JudetTable extends CI_Model
{

    const DEFAULT_COUNTRY_CODE = 'RO';

    /**
     * @var JudetTable
     */
    private static $instance;


    /**
     * Singleton
     * JudetTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    // ---------------------------------------------------------------------------------------------

    /**
     *  we can later dependency inject and thus unit test this
     * @return JudetTable
     */
    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param int $id
     * @return JudetItem|null
     * @throws Exception
     */
    public function load($id)
    {
        $id = (int)$id;
        if (!$id) {
            throw new Exception('ID judet este invalid');
        }
        $filters = new JudetFilters(array('id'=>$id));
        $data = $this->fetchAll($filters);
        $item = isset($data['items']) && isset($data['items'][0]) ? $data['items'][0] : null;
        return $item;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param JudetFilters $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchAll(JudetFilters $filters = null, $limit = null, $offset = null)
    {
        // Set where and having conditions.
        $sqlWhere = array();
        $sqlHaving = array();
        if ($filters) {
            if ($filters->getId()) {
                $sqlWhere[] = "judet.id = {$filters->getId()}";
            }
            if ($filters->getExcludedId()) {
                $sqlWhere[] = "judet.id <> {$filters->getExcludedId()}";
            }
            if ($filters->getCodTara()) {
                $sqlWhere[] = "judet.cod_tara = " . $this->db->escape($filters->getCodTara());
            }
            if ($filters->getCod()) {
                $sqlWhere[] = "judet.cod LIKE " . $this->db->escape($filters->getCod());
            }
            if ($filters->getNume()) {
                $sqlWhere[] = "judet.nume LIKE " . $this->db->escape($filters->getNume());
            }
            if ($filters->getNumeAnterior()) {
                $sqlWhere[] = "judet.nume_anterior LIKE " . $this->db->escape($filters->getNumeAnterior());
            }
            if ($filters->getSearchTerm()) {
                // Clean search term, extract words and set where condition.
                $searchTerm = Strings::sanitizeForDatabaseUsage($filters->getSearchTerm());
                $words = Strings::splitIntoWords($searchTerm);
                if (count($words)) {
                    $subCond = array();
                    foreach ($words as $word) {
                        $word = $this->db->escape("%$word%");
                        $subCond[] = "(judet.id LIKE $word OR judet.cod_tara LIKE $word OR judet.cod LIKE $word OR judet.nume LIKE $word)";
                    }
                    $sqlWhere[] = implode("\n\tAND ", $subCond);
                }
            }
        }
        $sqlWhere = implode("\n\tAND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sqlHaving = implode("\n\tAND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? "HAVING " . $sqlHaving : '';

        // ------------------------------------

        // Set order by.
        $sqlOrder = array();
        if (count($filters->getOrderBy())) {
            foreach($filters->getOrderBy() as $ord) {
                switch($ord) {
                    case $filters::ORDER_BY_ID_ASC:
                        $sqlOrder[] = "id ASC";
                        break;
                    case $filters::ORDER_BY_ID_DESC:
                        $sqlOrder[] = "id DESC";
                        break;
                    case $filters::ORDER_BY_COD_ASC:
                        $sqlOrder[] = "cod ASC";
                        break;
                    case $filters::ORDER_BY_COD_DESC:
                        $sqlOrder[] = "cod DESC";
                        break;
                    case $filters::ORDER_BY_NUME_ASC:
                        $sqlOrder[] = "nume ASC";
                        break;
                    case $filters::ORDER_BY_NUME_DESC:
                        $sqlOrder[] = "nume DESC";
                        break;
                    case $filters::ORDER_BY_TARA_ASC:
                        $sqlOrder[] = "cod_tara ASC";
                        break;
                    case $filters::ORDER_BY_TARA_DESC:
                        $sqlOrder[] = "cod_tara DESC";
                        break;
                }
            }
        }
        if (!count($sqlOrder)) {
            $sqlOrder[] = "id ASC";
        }
        $sqlOrder = implode(", ", $sqlOrder);

        // ------------------------------------

        // Set limit.
        $sqlLimit = '';
        if (!is_null($offset) && !is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$offset . ', ' . (int)$limit;
        } elseif (!is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$limit;
        }

        // ------------------------------------

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    judet.*
FROM
    nom_judete AS judet
WHERE
    1
    $sqlWhere
GROUP BY judet.id
$sqlHaving
ORDER BY $sqlOrder
$sqlLimit
EOSQL;

        /** @var CI_DB_result $result */
        $result = $this->db->query($sql);
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        $items = array();
        foreach($result->result('array') as $row){
            $items[] = new JudetItem($row);
        }

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------
    
    /**
     * @param string $searchTerm
     * @param int $limit
     * @return array|JudetItem[]
     */
    public function fetchForAutocomplete($searchTerm, $limit=LIMIT_FOR_AUTOCOMPLETE)
    {
        $filters = new JudetFilters(array('searchTerm'=>$searchTerm));
        $data = $this->fetchAll($filters, $limit);
        return isset($data['items']) ? $data['items'] : array();
    }
    
    // ---------------------------------------------------------------------------------------------

    public function fetchForSelect($codTara=null)
    {
        $codTara = !$codTara ? $this::DEFAULT_COUNTRY_CODE : trim(strip_tags($codTara));
        $filters = new JudetFilters();
        $filters->setCodTara($codTara);
        $result = $this->fetchAll($filters);
        $data = array();
        if (isset($result['items']) && count($result['items'])) {
            /** @var JudetItem $judet */
            foreach($result['items'] as $judet) {
                $data[$judet->getId()] = $judet->getNume();
            }
        }
        return $data;
    }

    // ---------------------------------------------------------------------------------------------

    
}