<?php

class TagItem
{
    use ArrayOrJson;
    
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $catId;

    /**
     * @var string
     */
    private $nume;

    /**
     * @var string
     */
    private $observatii;

    /**
     * @var int
     */
    private $userId;

    /**
     * @var AauthuserItem
     */
    private $user;

    /**
     * @var int
     */
    private $isFavorit;

    /**
     * @var string
     */
    private $dataInsert;

    /**
     * @var int
     */
    private $hits;

    /**
     * @var int
     */
    private $personajId;

    /**
     * @var int
     */
    private $isDeleted;


    # ==========================================================================
    #
    #   Asociation table -> fcp_personaje
    #
    # ==========================================================================

    /**
     * @var PersonajItem $objPersonaj
     */
    private $objPersonaj;

    // ---------------------------------------------------------------------------------------------

    public function __construct(array $dbRow = [])
    {
        if ( !count($dbRow) ) {
            return;
        }

        $this->id           = $dbRow['id'] ? (int)$dbRow['id'] : null;
        $this->catId        = $dbRow['cat_id'] ? (int)$dbRow['cat_id'] : null;
        $this->nume         = $dbRow['nume'] ? (string)$dbRow['nume'] : null;
        $this->observatii   = $dbRow['observatii'] ? (string)$dbRow['observatii'] : null;
        $this->userId       = $dbRow['user_id'] ? (int)$dbRow['user_id'] : null;
        $this->isFavorit    = $dbRow['is_favorit'] ? (int)$dbRow['is_favorit'] : null;
        $this->dataInsert   = $dbRow['data_insert'] ? (string)$dbRow['data_insert'] : null;
        $this->hits         = $dbRow['hits'] ? (int)$dbRow['hits'] : null;
        $this->personajId   = $dbRow['personaj_id'] ? (int)$dbRow['personaj_id'] : null;
        $this->isDeleted    = $dbRow['is_deleted'] ? (int)$dbRow['is_deleted'] : null;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return int
     */
    public function getCatId()
    {
        return $this->catId;
    }

    /**
     * @param int $catId
     */
    public function setCatId($catId)
    {
        $this->catId = $catId ? (int)$catId : null;
    }

    /**
     * @return string
     */
    public function getNume()
    {
        return $this->nume;
    }

    /**
     * @param string $nume
     */
    public function setNume($nume)
    {
        $this->nume = $nume ? (string)$nume : null;
    }

    /**
     * @return string
     */
    public function getObservatii()
    {
        return $this->observatii;
    }

    /**
     * @param string $observatii
     */
    public function setObservatii($observatii)
    {
        $this->observatii = $observatii ? (string)$observatii : null;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId ? (int)$userId : null;
    }

    /**
     * @return AauthuserItem|null
     * @throws Exception
     */
    public function getUser()
    {
        if ($this->user) {
            return $this->user;
        } elseif ($this->userId) {
            $this->user = AauthUserTable::getInstance()->load($this->userId);
            return $this->user;
        } else {
            return null;
        }
    }

    /**
     * @return int
     */
    public function getIsFavorit()
    {
        return $this->isFavorit;
    }

    /**
     * @param int $isFavorit
     */
    public function setIsFavorit($isFavorit)
    {
        $this->isFavorit = $isFavorit ? (int)$isFavorit : null;
    }

    /**
     * @return string
     */
    public function getDataInsert()
    {
        return $this->dataInsert;
    }

    /**
     * @param string $dataInsert
     */
    public function setDataInsert($dataInsert)
    {
        $this->dataInsert = $dataInsert ? (string)$dataInsert : null;
    }

    /**
     * @return int
     */
    public function getHits()
    {
        return $this->hits;
    }

    /**
     * @param int $hits
     */
    public function setHits($hits)
    {
        $this->hits = $hits ? (int)$hits : null;
    }

    /**
     * @return int
     */
    public function getPersonajId()
    {
        return $this->personajId;
    }

    /**
     * @param int $personajId
     */
    public function setPersonajId($personajId)
    {
        $this->personajId = $personajId ? (int)$personajId : null;
    }

    /**
     * @return PersonajItem|null
     * @throws Exception
     */
    public function getPersonaj()
    {
        if ($this->objPersonaj) {
            return $this->objPersonaj;
        } elseif ($this->personajId) {
            $this->objPersonaj = PersonajTable::getInstance()->load($this->personajId);
            return $this->objPersonaj;
        } else {
            return null;
        }
    }

    /**
     * @return int
     */
    public function isDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param int $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted ? (int)$isDeleted : null;
    }

}
