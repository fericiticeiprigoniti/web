<?php

class JudetFilters
{

    const ORDER_BY_ID_ASC = 'id_asc';
    const ORDER_BY_ID_DESC = 'id_desc';
    const ORDER_BY_COD_ASC = 'cod_asc';
    const ORDER_BY_COD_DESC = 'cod_desc';
    const ORDER_BY_NUME_ASC = 'nume_asc';
    const ORDER_BY_NUME_DESC = 'nume_desc';
    const ORDER_BY_TARA_ASC = 'tara_asc';
    const ORDER_BY_TARA_DESC = 'tara_desc';


    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $excludedId;

    /**
     * @var string
     */
    private $codTara;

    /**
     * @var string
     */
    private $cod;

    /**
     * @var string
     */
    private $nume;

    /**
     * @var string
     */
    private $numeAnterior;

    /**
     * @var string
     */
    private $searchTerm;

    /**
     * @var array
     */
    private $orderBy = [];


    // ---------------------------------------------------------------------------------------------

    public function __construct(array $getData = array())
    {
        if (isset($getData['id'])) {
            $this->setId($getData['id']);
        }
        if (isset($getData['excludedId'])) {
            $this->setExcludedId($getData['excludedId']);
        }
        if (isset($getData['codTara'])) {
            $this->setCodTara($getData['codTara']);
        }
        if (isset($getData['cod'])) {
            $this->setCod($getData['cod']);
        }
        if (isset($getData['nume'])) {
            $this->setNume($getData['nume']);
        }
        if (isset($getData['numeAnterior'])) {
            $this->setNumeAnterior($getData['numeAnterior']);
        }
        if (isset($getData['searchTerm'])) {
            $this->setSearchTerm($getData['searchTerm']);
        }

        if (isset($getData['_orderBy'])) {
            $this->setOrderBy($getData['_orderBy']);
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return int|null
     */
    public function getExcludedId()
    {
        return $this->excludedId;
    }

    /**
     * @param int|null $excludedId
     */
    public function setExcludedId($excludedId)
    {
        $this->excludedId = $excludedId ? (int)$excludedId : null;
    }

    /**
     * @return string|null
     */
    public function getCodTara()
    {
        return $this->codTara;
    }

    /**
     * @param string|null $codTara
     */
    public function setCodTara($codTara)
    {
        $this->codTara = $codTara ? trim(strip_tags($codTara)) : null;
    }

    /**
     * @return string|null
     */
    public function getCod()
    {
        return $this->cod;
    }

    /**
     * @param string|null $cod
     */
    public function setCod($cod)
    {
        $this->cod = is_null($cod) ? null : trim(strip_tags($cod));
    }

    /**
     * @return string|null
     */
    public function getNume()
    {
        return $this->nume;
    }

    /**
     * @param string|null $nume
     */
    public function setNume($nume)
    {
        $this->nume = is_null($nume) ? null : trim(strip_tags($nume));
    }

    /**
     * @return string|null
     */
    public function getNumeAnterior()
    {
        return $this->numeAnterior;
    }

    /**
     * @param string|null $numeAnterior
     */
    public function setNumeAnterior($numeAnterior)
    {
        $this->numeAnterior = is_null($numeAnterior) ? null : trim(strip_tags($numeAnterior));
    }

    /**
     * @return string|null
     */
    public function getSearchTerm()
    {
        return $this->searchTerm;
    }

    /**
     * @param string|null $val
     */
    public function setSearchTerm($val)
    {
        $this->searchTerm = is_null($val) ? null : trim(strip_tags($val));
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return array
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param array $orderBy
     */
    public function setOrderBy(array $orderBy)
    {
        $orderBy = (is_array($orderBy) && count($orderBy) ? $orderBy : array());
        if (count($orderBy)) {
            $orderItems = array_keys(self::fetchOrderItems());
            foreach ($orderBy as $k=>$v) {
                if (!in_array($v, $orderItems)) {
                    unset($orderBy[$k]);
                }
            }
        }
        $this->orderBy = $orderBy;
    }

    /**
     * @return array
     */
    public static function fetchOrderItems()
    {
        return array(
            self::ORDER_BY_ID_ASC => 'ID - ASC',
            self::ORDER_BY_ID_DESC => 'ID - DESC',
            self::ORDER_BY_COD_ASC => 'Cod - ASC',
            self::ORDER_BY_COD_DESC => 'Cod - DESC',
            self::ORDER_BY_NUME_ASC => 'Nume - ASC',
            self::ORDER_BY_NUME_DESC => 'Nume - DESC',
            self::ORDER_BY_TARA_ASC => 'Tara - ASC',
            self::ORDER_BY_TARA_DESC => 'Tara - DESC',
        );
    }

}