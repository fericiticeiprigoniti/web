<?php

class LocalitateFilters
{

    const ORDER_BY_ID_ASC = 'id_asc';
    const ORDER_BY_ID_DESC = 'id_desc';
    const ORDER_BY_NUME_ASC = 'nume_asc';
    const ORDER_BY_NUME_DESC = 'nume_desc';
    const ORDER_BY_JUDET_ASC = 'judet_asc';
    const ORDER_BY_JUDET_DESC = 'judet_desc';
    const ORDER_BY_TIP_LOCALITATE_ASC = 'tip_localitate_asc';
    const ORDER_BY_TIP_LOCALITATE_DESC = 'tip_localitate_desc';


    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $excludedId;

    /**
     * @var int
     */
    private $idJudet;

    /**
     * @var string
     */
    private $nume;

    /**
     * @var string
     */
    private $numeAnterior;

    /**
     * @var string
     */
    private $tipLocalitateCod;

    /**
     * @var int
     */
    private $parentId;

    /**
     * @var string
     */
    private $searchTerm;

    /**
     * @var array
     */
    private $orderBy = [];


    // ---------------------------------------------------------------------------------------------

    public function __construct(array $getData = array())
    {
        if (isset($getData['id'])) {
            $this->setId($getData['id']);
        }
        if (isset($getData['idJudet'])) {
            $this->setIdJudet($getData['idJudet']);
        }
        if (isset($getData['excludedId'])) {
            $this->setExcludedId($getData['excludedId']);
        }
        if (isset($getData['nume'])) {
            $this->setNume($getData['nume']);
        }
        if (isset($getData['numeAnterior'])) {
            $this->setNumeAnterior($getData['numeAnterior']);
        }
        if (isset($getData['tipLocalitate'])) {
            $this->setTipLocalitateCod($getData['tipLocalitate']);
        }
        if (isset($getData['parentId'])) {
            $this->setParentId($getData['parentId']);
        }
        if (isset($getData['searchTerm'])) {
            $this->setSearchTerm($getData['searchTerm']);
        }

        if (isset($getData['_orderBy'])) {
            $this->setOrderBy($getData['_orderBy']);
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return int|null
     */
    public function getExcludedId()
    {
        return $this->excludedId;
    }

    /**
     * @param int|null $excludedId
     */
    public function setExcludedId($excludedId)
    {
        $this->excludedId = $excludedId ? (int)$excludedId : null;
    }

    /**
     * @return int|null
     */
    public function getIdJudet()
    {
        return $this->idJudet;
    }

    /**
     * @param int|null $idJudet
     */
    public function setIdJudet($idJudet)
    {
        $this->idJudet = $idJudet ? (int)$idJudet : null;
    }

    /**
     * @return string|null
     */
    public function getNume()
    {
        return $this->nume;
    }

    /**
     * @param string|null $nume
     */
    public function setNume($nume)
    {
        $this->nume = is_null($nume) ? null : trim(strip_tags($nume));
    }

    /**
     * @return string|null
     */
    public function getNumeAnterior()
    {
        return $this->numeAnterior;
    }

    /**
     * @param string|null $numeAnterior
     */
    public function setNumeAnterior($numeAnterior)
    {
        $this->numeAnterior = is_null($numeAnterior) ? null : trim(strip_tags($numeAnterior));
    }

    /**
     * @return string|null
     */
    public function getTipLocalitateCod()
    {
        return $this->tipLocalitateCod;
    }

    /**
     * @param string|null $tipLocalitateCod
     */
    public function setTipLocalitateCod($tipLocalitateCod)
    {
        $this->tipLocalitateCod = is_null($tipLocalitateCod) ? null : trim(strip_tags($tipLocalitateCod));
    }

    /**
     * @return int|null
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param int|null $parentId
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId ? (int)$parentId : null;
    }

    /**
     * @return string|null
     */
    public function getSearchTerm()
    {
        return $this->searchTerm;
    }

    /**
     * @param string|null $val
     */
    public function setSearchTerm($val)
    {
        $this->searchTerm = is_null($val) ? null : trim(strip_tags($val));
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return array
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param array $orderBy
     */
    public function setOrderBy(array $orderBy)
    {
        $orderBy = (is_array($orderBy) && count($orderBy) ? $orderBy : array());
        if (count($orderBy)) {
            $orderItems = array_keys(self::fetchOrderItems());
            foreach ($orderBy as $k=>$v) {
                if (!in_array($v, $orderItems)) {
                    unset($orderBy[$k]);
                }
            }
        }
        $this->orderBy = $orderBy;
    }

    /**
     * @return array
     */
    public static function fetchOrderItems()
    {
        return array(
            self::ORDER_BY_ID_ASC => 'ID - ASC',
            self::ORDER_BY_ID_DESC => 'ID - DESC',
            self::ORDER_BY_NUME_ASC => 'Nume - ASC',
            self::ORDER_BY_NUME_DESC => 'Nume - DESC',
            self::ORDER_BY_JUDET_ASC => 'Judet - ASC',
            self::ORDER_BY_JUDET_DESC => 'Judet - DESC',
            self::ORDER_BY_TIP_LOCALITATE_ASC => 'Tip localitate - ASC',
            self::ORDER_BY_TIP_LOCALITATE_DESC => 'Tip localitate - DESC',
        );
    }

}