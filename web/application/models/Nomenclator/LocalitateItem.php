<?php

class LocalitateItem
{

    use ArrayOrJson;

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $idJudet;

    /**
     * @var JudetItem
     */
    private $judet;

    /**
     * @var string
     */
    private $nume;

    /**
     * @var string
     */
    private $numeAnterior;

    /**
     * @var string
     */
    private $tipLocalitateCod;

    /**
     * @var LocalitateTipItem
     */
    private $tipLocalitate;

    /**
     * @var int
     */
    private $parentId;

    /**
     * @var LocalitateItem
     */
    private $parent;

    /**
     * @var string
     */
    private $numeComplet;

    // ---------------------------------------------------------------------------------------------

    public function __construct(array $dbRow = array())
    {
        if (!count($dbRow)) {
            return;
        }
        $this->id = (int)$dbRow['id'];
        $this->idJudet = (int)$dbRow['id_judet'];
        $this->nume = $dbRow['nume'];
        $this->numeAnterior = $dbRow['nume_anterior'];
        $this->tipLocalitateCod = $dbRow['tip_localitate'];
        $this->parentId = $dbRow['parent_id'];
        $this->numeComplet = $dbRow['nume_complet'];
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getNume()
    {
        return $this->nume;
    }

    /**
     * @param string|null $nume
     * @throws Exception
     */
    public function setNume($nume)
    {
        $this->nume = trim(strip_tags($nume));
        if (empty($this->nume)) {
            throw new Exception('Completati numele localitatii');
        } else {
            $filters = new LocalitateFilters();
            $filters->setExcludedId($this->id);
            $filters->setIdJudet($this->idJudet);
            $filters->setNume($this->nume);
            $res = LocalitateTable::getInstance()->fetchAll($filters);
            if (isset($res['items']) && count($res['items'])) {
                throw new Exception('Numele localitatii nu este unic in cadrul aceluiasi judet');
            }
        }
    }

    /**
     * @return string|null
     */
    public function getNumeAnterior()
    {
        return $this->numeAnterior;
    }

    /**
     * @param string|null $numeAnterior
     * @throws Exception
     */
    public function setNumeAnterior($numeAnterior)
    {
        $this->numeAnterior = $numeAnterior ? trim(strip_tags($numeAnterior)) : null;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getIdJudet()
    {
        return $this->idJudet;
    }

    /**
     * @param int|null $idJudet
     * @throws Exception
     */
    public function setIdJudet($idJudet)
    {
        $this->idJudet = $idJudet ? (int)$idJudet : null;
        if (!$this->idJudet) {
            throw new Exception('Selectati un judet');
        }
        if ($this->idJudet) {
            $this->judet = JudetTable::getInstance()->load($this->idJudet);
            if (!$this->judet) {
                throw new Exception('Judetul selectat este invalid');
            }
        }
    }

    public function getJudet()
    {
        if ($this->judet) {
            return $this->judet;
        } elseif ($this->idJudet) {
            $this->judet = JudetTable::getInstance()->load($this->idJudet);
            return $this->judet;
        } else {
            return null;
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return string|null
     */
    public function getTipLocalitateCod()
    {
        return $this->tipLocalitateCod;
    }

    /**
     * @param string|null $tipLocalitateCod
     * @throws Exception
     */
    public function setTipLocalitateCod($tipLocalitateCod)
    {
        $this->tipLocalitateCod = $tipLocalitateCod ? trim(strip_tags($tipLocalitateCod)) : null;
        if (!$this->tipLocalitateCod) {
            throw new Exception('Selectati tipul de localitate');
        }
        $this->tipLocalitate = LocalitateTipTable::getInstance()->load($this->tipLocalitateCod);
        if (!$this->tipLocalitate) {
            throw new Exception('Tipul de localitate selectat este invalid');
        }
    }

    public function getTipLocalitate()
    {
        if ($this->tipLocalitate) {
            return $this->tipLocalitate;
        } elseif ($this->tipLocalitateCod) {
            $this->tipLocalitate = LocalitateTipTable::getInstance()->load($this->tipLocalitateCod);
            return $this->tipLocalitate;
        } else {
            return null;
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param int|null $parentId
     * @throws Exception
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId ? (int)$parentId : null;
        if ($this->parentId) {
            $this->parent = LocalitateTable::getInstance()->load($this->parentId);
            if (!$this->parent) {
                throw new Exception('Localitatea parinte este invalida');
            }
        }
    }

    public function getParent()
    {
        if ($this->parent) {
            return $this->parent;
        } elseif ($this->parentId) {
            $this->parent = LocalitateTable::getInstance()->load($this->parentId);
            return $this->parent;
        } else {
            return null;
        }
    }
    
    // ---------------------------------------------------------------------------------------------

    /**
     * @return string|null
     */
    public function getNumeComplet()
    {
        return $this->numeComplet;
    }

}