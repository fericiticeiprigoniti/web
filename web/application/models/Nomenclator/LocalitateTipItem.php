<?php


class LocalitateTipItem
{

    /**
     * @var string
     */
    private $cod;

    /**
     * @var string
     */
    private $nume;

    /**
     * @var int
     */
    private $order;

    // ---------------------------------------------------------------------------------------------

    public function __construct(array $dbRow = array())
    {
        if (!count($dbRow)) {
            return;
        }
        $this->cod = $dbRow['cod'];
        $this->nume = $dbRow['nume'];
        $this->order = $dbRow['ord'];
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return string|null
     */
    public function getCod()
    {
        return $this->cod;
    }

    /**
     * @param string|null $cod
     * @throws Exception
     */
    public function setCod($cod)
    {
        $this->cod = trim(strip_tags($cod));
        if (empty($this->cod)) {
            throw new Exception('Completati codul');
        } elseif (strlen($this->cod) > 4) {
            throw new Exception('Codul poate contine maxim 4 caractere');
        } else {
            $filters = new LocalitateTipFilters();
            $filters->setCodExclus($this->cod);
            $res = LocalitateTipTable::getInstance()->fetchAll($filters);
            if (isset($res['items']) && count($res['items'])) {
                throw new Exception('Codul tipului de localitate nu este unic');
            }
        }
    }

    /**
     * @return string|null
     */
    public function getNume()
    {
        return $this->nume;
    }

    /**
     * @param string|null $nume
     * @throws Exception
     */
    public function setNume($nume)
    {
        $this->nume = trim(strip_tags($nume));
        if (empty($this->nume)) {
            throw new Exception('Completati numele tipului de localitate');
        }
    }

    /**
     * @return int|null
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param int|null $order
     * @throws Exception
     */
    public function setOrder($order)
    {
        $this->order = $order ? (int)$order : null;
    }

}