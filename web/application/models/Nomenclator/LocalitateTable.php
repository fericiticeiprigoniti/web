<?php

/**
 * Class LocalitateTable
 * @table nom_localitati
 */
class LocalitateTable extends CI_Model
{

    /**
     * @var LocalitateTable
     */
    private static $instance;


    /**
     * Singleton
     * LocalitateTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    // ---------------------------------------------------------------------------------------------

    /**
     *  we can later dependency inject and thus unit test this
     * @return LocalitateTable
     */
    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param int $id
     * @return LocalitateItem|null
     * @throws Exception
     */
    public function load($id)
    {
        $id = (int)$id;
        if (!$id) {
            throw new Exception('ID localitate este invalid');
        }
        $filters = new LocalitateFilters(array('id'=>$id));
        $data = $this->fetchAll($filters);

        return $data['items'][0] ?? null;
    }

    /**
     * @param int $id
     */
    public static function get($id): ?LocalitateItem
    {
        if (!$id) {
            return null;
        }

        try {
            return self::getInstance()->load($id);
        } catch (Throwable $e) {
            log_message('error', $e->getMessage());
            return null;
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param LocalitateFilters $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchAll(LocalitateFilters $filters = null, $limit = null, $offset = null)
    {
        // Set where and having conditions.
        $sqlWhere = [];
        $sqlHaving = [];

        if ($filters) {
            if ($filters->getId()) {
                $sqlWhere[] = "loc.id = {$filters->getId()}";
            }
            if ($filters->getExcludedId()) {
                $sqlWhere[] = "loc.id <> {$filters->getExcludedId()}";
            }
            if ($filters->getIdJudet()) {
                $sqlWhere[] = "loc.id_judet = {$filters->getIdJudet()}";
            }
            if ($filters->getNume()) {
                $sqlWhere[] = "loc.nume LIKE " . $this->db->escape($filters->getNume());
            }
            if ($filters->getNumeAnterior()) {
                $sqlWhere[] = "loc.nume_anterior LIKE " . $this->db->escape($filters->getNumeAnterior());
            }
            if ($filters->getTipLocalitateCod()) {
                $sqlWhere[] = "loc.tip_localitate = " . $this->db->escape($filters->getTipLocalitateCod());
            }
            if ($filters->getParentId()) {
                $sqlWhere[] = "loc.parent_id = {$filters->getParentId()}";
            }
            if ($filters->getSearchTerm()) {
                // Clean search term, extract words and set where condition.
                $searchTerm = Strings::sanitizeForDatabaseUsage($filters->getSearchTerm());
                $words = Strings::splitIntoWords($searchTerm);
                if (count($words)) {
                    $subCond = array();
                    foreach ($words as $word) {
                        $word = $this->db->escape("%$word%");
                        $subCond[] = "nume_complet LIKE $word";
                    }
                    $sqlHaving[] = implode("\n\tAND ", $subCond);
                }
            }
        }

        $sqlWhere = implode("\n\tAND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sqlHaving = implode("\n\tAND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? "HAVING " . $sqlHaving : '';

        // ------------------------------------

        // Set order by.
        $sqlOrder = array();
        if ( $filters && count($filters->getOrderBy())) {
            foreach($filters->getOrderBy() as $ord) {
                switch($ord) {
                    case $filters::ORDER_BY_ID_ASC:
                        $sqlOrder[] = "id ASC";
                        break;
                    case $filters::ORDER_BY_ID_DESC:
                        $sqlOrder[] = "id DESC";
                        break;
                    case $filters::ORDER_BY_NUME_ASC:
                        $sqlOrder[] = "nume ASC";
                        break;
                    case $filters::ORDER_BY_NUME_DESC:
                        $sqlOrder[] = "nume DESC";
                        break;
                    case $filters::ORDER_BY_JUDET_ASC:
                        $sqlOrder[] = "judet.nume ASC";
                        break;
                    case $filters::ORDER_BY_JUDET_DESC:
                        $sqlOrder[] = "judet.nume DESC";
                        break;
                    case $filters::ORDER_BY_TIP_LOCALITATE_ASC:
                        $sqlOrder[] = "tip_localitate ASC";
                        break;
                    case $filters::ORDER_BY_TIP_LOCALITATE_DESC:
                        $sqlOrder[] = "tip_localitate DESC";
                        break;
                }
            }
        }
        if (!count($sqlOrder)) {
            $sqlOrder[] = "id ASC";
        }
        $sqlOrder = implode(", ", $sqlOrder);

        // ------------------------------------

        // Set limit.
        $sqlLimit = '';
        if (!is_null($offset) && !is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$offset . ', ' . (int)$limit;
        } elseif (!is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$limit;
        }

        // ------------------------------------

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    loc.*
    , CONCAT_WS(', '
        , CONCAT(loc.tip_localitate, ' ', loc.nume)
        , IF(loc.nume_anterior IS NOT NULL, CONCAT(' (', loc.nume_anterior, ')'), NULL)
        , IF(parent_loc.nume IS NOT NULL, CONCAT(parent_loc.tip_localitate, ' ', parent_loc.nume), NULL)
        , CONCAT('judet ', judet.nume)
    ) AS nume_complet
FROM
    nom_localitati AS loc
    LEFT JOIN nom_localitati AS parent_loc ON loc.parent_id = parent_loc.id
    INNER JOIN nom_localitati_tip AS tip_loc ON tip_loc.cod = loc.tip_localitate
    INNER JOIN nom_judete AS judet ON judet.id = loc.id_judet
WHERE
    1
    $sqlWhere
GROUP BY loc.id
$sqlHaving
ORDER BY $sqlOrder
$sqlLimit
EOSQL;

        /** @var CI_DB_result $result */
        $result = $this->db->query($sql);
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        $items = array();
        foreach($result->result('array') as $row){
            $items[] = new LocalitateItem($row);
        }

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------
    
    /**
     * @param string $searchTerm
     * @param int $limit
     * @return LocalitateItem[]
     */
    public function fetchForAutocomplete($searchTerm, $limit=LIMIT_FOR_AUTOCOMPLETE)
    {
        $filters = new LocalitateFilters(array('searchTerm'=>$searchTerm));
        $data = $this->fetchAll($filters, $limit);
        return $data['items'] ?? [];
    }

    /**
    * return key -> value array
    * @param LocalitateFilters|null $filters
    * @return array
    */
    public function fetchForSelect(LocalitateFilters $filters = null)
    {
        $result = $this->fetchAll($filters);

        $data = [];
        if (isset($result['items']) && count($result['items'])) {
            /** @var LocalitateItem $objItem */
            foreach($result['items'] as $objItem) {
                $data[$objItem->getId()] = $objItem->getNumeComplet();
            }
        }

        return $data;
    }
}