<?php

/**
 * Class TaraTable
 * @table nom_tari
 */
class TaraTable extends CI_Model
{

    /**
     * @var TaraTable
     */
    private static $instance;


    /**
     * Singleton
     * TaraTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    // ---------------------------------------------------------------------------------------------

    /**
     *  we can later dependency inject and thus unit test this
     * @return TaraTable
     */
    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param string $cod
     * @return TaraItem|null
     * @throws Exception
     */
    public function load($cod)
    {
        if (!$cod) {
            throw new Exception('Cod tara invalid');
        }
        $filters = new TaraFilters(array('cod'=>$cod));
        $data = $this->fetchAll($filters);
        $item = isset($data['items']) && isset($data['items'][0]) ? $data['items'][0] : null;
        return $item;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param TaraFilters $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchAll(TaraFilters $filters = null, $limit = null, $offset = null)
    {
        // Set where and having conditions.
        $sqlWhere = array();
        $sqlHaving = array();
        if ($filters) {
            if ($filters->getCod()) {
                $sqlWhere[] = "tara.cod LIKE " . $this->db->escape($filters->getCod());
            }
            if ($filters->getCodExclus()) {
                $sqlWhere[] = "tara.cod NOT LIKE " . $this->db->escape($filters->getCod());
            }
            if ($filters->getNume()) {
                $sqlWhere[] = "tara.nume LIKE " . $this->db->escape($filters->getNume());
            }
            if ($filters->getNumeAnterior()) {
                $sqlWhere[] = "tara.nume_anterior LIKE " . $this->db->escape($filters->getNumeAnterior());
            }
            if ($filters->getSearchTerm()) {
                // Clean search term, extract words and set where condition.
                $searchTerm = Strings::sanitizeForDatabaseUsage($filters->getSearchTerm());
                $words = Strings::splitIntoWords($searchTerm);
                if (count($words)) {
                    $subCond = array();
                    foreach ($words as $word) {
                        $word = $this->db->escape("%$word%");
                        $subCond[] = "(tara.cod LIKE $word OR tara.nume LIKE $word)";
                    }
                    $sqlWhere[] = implode("\n\tAND ", $subCond);
                }
            }
        }
        $sqlWhere = implode("\n\tAND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sqlHaving = implode("\n\tAND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? "HAVING " . $sqlHaving : '';

        // ------------------------------------

        // Set order by.
        $sqlOrder = array();
        if (count($filters->getOrderBy())) {
            foreach($filters->getOrderBy() as $ord) {
                switch($ord) {
                    case $filters::ORDER_BY_COD_ASC:
                        $sqlOrder[] = "cod ASC";
                        break;
                    case $filters::ORDER_BY_COD_DESC:
                        $sqlOrder[] = "cod DESC";
                        break;
                    case $filters::ORDER_BY_NUME_ASC:
                        $sqlOrder[] = "nume ASC";
                        break;
                    case $filters::ORDER_BY_NUME_DESC:
                        $sqlOrder[] = "nume DESC";
                        break;
                    case $filters::ORDER_BY_ORDER_ASC:
                        $sqlOrder[] = "ord ASC";
                        break;
                    case $filters::ORDER_BY_ORDER_DESC:
                        $sqlOrder[] = "ord DESC";
                        break;
                }
            }
        }
        if (!count($sqlOrder)) {
            $sqlOrder[] = "cod ASC";
        }
        $sqlOrder = implode(", ", $sqlOrder);

        // ------------------------------------

        // Set limit.
        $sqlLimit = '';
        if (!is_null($offset) && !is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$offset . ', ' . (int)$limit;
        } elseif (!is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$limit;
        }

        // ------------------------------------

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    tara.*
FROM
    nom_tari AS tara
WHERE
    1
    $sqlWhere
GROUP BY tara.id
$sqlHaving
ORDER BY $sqlOrder
$sqlLimit
EOSQL;

        /** @var CI_DB_result $result */
        $result = $this->db->query($sql);
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        $items = array();
        foreach($result->result('array') as $row){
            $items[] = new TaraItem($row);
        }

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------
    
    /**
     * @param string $searchTerm
     * @param int $limit
     * @return array|TaraItem[]
     */
    public function fetchForAutocomplete($searchTerm, $limit=LIMIT_FOR_AUTOCOMPLETE)
    {
        $filters = new TaraFilters(array('searchTerm'=>$searchTerm));
        $data = $this->fetchAll($filters, $limit);
        return isset($data['items']) ? $data['items'] : array();
    }
    
    // ---------------------------------------------------------------------------------------------

    public function fetchForSelect()
    {
        $result = $this->fetchAll();
        $data = array();
        if (isset($result['items']) && count($result['items'])) {
            /** @var TaraItem $tara */
            foreach($result['items'] as $tara) {
                $data[$tara->getCod()] = $tara->getNume();
            }
        }
        return $data;
    }

    // ---------------------------------------------------------------------------------------------

    
}