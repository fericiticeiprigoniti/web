<?php


class JudetItem
{

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $codTara;

    /**
     * @var TaraItem
     */
    public $tara;

    /**
     * @var string
     */
    public $cod;

    /**
     * @var string
     */
    public $nume;

    /**
     * @var string
     */
    public $numeAnterior;

    // ---------------------------------------------------------------------------------------------

    public function __construct(array $dbRow = array())
    {
        if (!count($dbRow)) {
            return;
        }
        $this->id = (int)$dbRow['id'];
        $this->codTara = (int)$dbRow['cod_tara'];
        $this->cod = $dbRow['cod'];
        $this->nume = $dbRow['nume'];
        $this->numeAnterior = $dbRow['nume_anterior'];
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getCod()
    {
        return $this->cod;
    }

    /**
     * @param string|null $cod
     * @throws Exception
     */
    public function setCod($cod)
    {
        $this->cod = trim(strip_tags($cod));
        if (empty($this->cod)) {
            throw new Exception('Completati codul judetului');
        } else {
            $filters = new JudetFilters();
            $filters->setExcludedId($this->id);
            $filters->setCodTara($this->codTara);
            $filters->setCod($this->cod);
            $res = JudetTable::getInstance()->fetchAll($filters);
            if (isset($res['items']) && count($res['items'])) {
                throw new Exception('Codul judetului nu este unic in cadrul tarii');
            }
        }
    }

    /**
     * @return string|null
     */
    public function getNume()
    {
        return $this->nume;
    }

    /**
     * @param string|null $nume
     * @throws Exception
     */
    public function setNume($nume)
    {
        $this->nume = trim(strip_tags($nume));
        if (empty($this->nume)) {
            throw new Exception('Completati numele judetului');
        } else {
            $filters = new JudetFilters();
            $filters->setExcludedId($this->id);
            $filters->setCodTara($this->codTara);
            $filters->setNume($this->nume);
            $res = JudetTable::getInstance()->fetchAll($filters);
            if (isset($res['items']) && count($res['items'])) {
                throw new Exception('Numele judetului nu este unic in cadrul tarii');
            }
        }
    }

    /**
     * @return string|null
     */
    public function getNumeAnterior()
    {
        return $this->numeAnterior;
    }

    /**
     * @param string|null $numeAnterior
     * @throws Exception
     */
    public function setNumeAnterior($numeAnterior)
    {
        $this->numeAnterior = $numeAnterior ? trim(strip_tags($numeAnterior)) : null;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return string|null
     */
    public function getCodTara()
    {
        return $this->codTara;
    }

    /**
     * @param string|null $codTara
     * @throws Exception
     */
    public function setCodTara($codTara)
    {
        $this->codTara = $codTara ? trim(strip_tags($codTara)) : null;
        if (!$this->codTara) {
            throw new Exception('Selectati tara');
        }
        if ($this->codTara) {
            $this->tara = TaraTable::getInstance()->load($this->codTara);
            if (!$this->tara) {
                throw new Exception('Tara selectata este invalida');
            }
        }
    }

    /**
     * @return TaraItem|null
     */
    public function getTara()
    {
        if ($this->tara) {
            return $this->tara;
        } elseif ($this->codTara) {
            $this->tara = TaraTable::getInstance()->load($this->codTara);
            return $this->tara;
        } else {
            return null;
        }
    }

    // ---------------------------------------------------------------------------------------------

}