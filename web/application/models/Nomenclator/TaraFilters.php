<?php

class TaraFilters
{

    const ORDER_BY_COD_ASC = 'cod_asc';
    const ORDER_BY_COD_DESC = 'cod_desc';
    const ORDER_BY_NUME_ASC = 'nume_asc';
    const ORDER_BY_NUME_DESC = 'nume_desc';
    const ORDER_BY_ORDER_ASC = 'order_asc';
    const ORDER_BY_ORDER_DESC = 'order_desc';


    /**
     * @var string
     */
    private $cod;

    /**
     * @var string
     */
    private $codExclus;

    /**
     * @var string
     */
    private $nume;

    /**
     * @var string
     */
    private $numeAnterior;

    /**
     * @var string
     */
    private $lang;

    /**
     * @var string
     */
    private $searchTerm;

    /**
     * @var array
     */
    private $orderBy = [];


    // ---------------------------------------------------------------------------------------------

    public function __construct(array $getData = array())
    {
        if (isset($getData['cod'])) {
            $this->setCod($getData['cod']);
        }
        if (isset($getData['codExclus'])) {
            $this->setCodExclus($getData['codExclus']);
        }
        if (isset($getData['nume'])) {
            $this->setNume($getData['nume']);
        }
        if (isset($getData['lang'])) {
            $this->setLang($getData['lang']);
        }
        if (isset($getData['numeAnterior'])) {
            $this->setNumeAnterior($getData['numeAnterior']);
        }
        if (isset($getData['searchTerm'])) {
            $this->setSearchTerm($getData['searchTerm']);
        }

        if (isset($getData['_orderBy'])) {
            $this->setOrderBy($getData['_orderBy']);
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return string|null
     */
    public function getCod()
    {
        return $this->cod;
    }

    /**
     * @param string|null $cod
     */
    public function setCod($cod)
    {
        $this->cod = is_null($cod) ? null : trim(strip_tags($cod));
    }
    
    /**
     * @return string|null
     */
    public function getCodExclus()
    {
        return $this->codExclus;
    }
    
    /**
     * @param string|null $codExclus
     */
    public function setCodExclus($codExclus)
    {
        $this->codExclus = $codExclus ? trim(strip_tags($codExclus)) : null;
    }

    /**
     * @return string|null
     */
    public function getNume()
    {
        return $this->nume;
    }

    /**
     * @param string|null $nume
     */
    public function setNume($nume)
    {
        $this->nume = is_null($nume) ? null : trim(strip_tags($nume));
    }

    /**
     * @return string|null
     */
    public function getNumeAnterior()
    {
        return $this->numeAnterior;
    }

    /**
     * @param string|null $numeAnterior
     */
    public function setNumeAnterior($numeAnterior)
    {
        $this->numeAnterior = is_null($numeAnterior) ? null : trim(strip_tags($numeAnterior));
    }
    
    /**
     * @return string|null
     */
    public function getLang()
    {
        return $this->lang;
    }
    
    /**
     * @param string|null $lang
     */
    public function setLang($lang)
    {
        $this->lang = is_null($lang) ? null : trim(strip_tags($lang));
    }

    /**
     * @return string|null
     */
    public function getSearchTerm()
    {
        return $this->searchTerm;
    }

    /**
     * @param string|null $val
     */
    public function setSearchTerm($val)
    {
        $this->searchTerm = is_null($val) ? null : trim(strip_tags($val));
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return array
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param array $orderBy
     */
    public function setOrderBy(array $orderBy)
    {
        $orderBy = (is_array($orderBy) && count($orderBy) ? $orderBy : array());
        if (count($orderBy)) {
            $orderItems = array_keys(self::fetchOrderItems());
            foreach ($orderBy as $k=>$v) {
                if (!in_array($v, $orderItems)) {
                    unset($orderBy[$k]);
                }
            }
        }
        $this->orderBy = $orderBy;
    }

    /**
     * @return array
     */
    public static function fetchOrderItems()
    {
        return array(
            self::ORDER_BY_COD_ASC => 'Cod - ASC',
            self::ORDER_BY_COD_DESC => 'Cod - DESC',
            self::ORDER_BY_NUME_ASC => 'Nume - ASC',
            self::ORDER_BY_NUME_DESC => 'Nume - DESC',
            self::ORDER_BY_ORDER_ASC => 'Order - ASC',
            self::ORDER_BY_ORDER_DESC => 'Order - DESC',
        );
    }

}