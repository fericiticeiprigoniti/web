<?php


class TaraItem
{

    /**
     * @var string
     */
    public $cod;

    /**
     * @var string
     */
    public $nume;

    /**
     * @var string
     */
    public $numeAnterior;

    /**
     * @var string
     */
    public $lang;

    /**
     * @var int
     */
    public $order;

    // ---------------------------------------------------------------------------------------------

    public function __construct(array $dbRow = array())
    {
        if (!count($dbRow)) {
            return;
        }
        $this->cod = $dbRow['cod'];
        $this->nume = $dbRow['nume'];
        $this->numeAnterior = $dbRow['nume_anterior'];
        $this->lang = $dbRow['lang'];
        $this->ord = $dbRow['ord'];
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return string|null
     */
    public function getCod()
    {
        return $this->cod;
    }

    /**
     * @return string|null
     */
    public function getNume()
    {
        return $this->nume;
    }

    /**
     * @return string|null
     */
    public function getNumeAnterior()
    {
        return $this->numeAnterior;
    }
    
    /**
     * @return string|null
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * @return int|null
     */
    public function getOrder()
    {
        return $this->order;
    }


}