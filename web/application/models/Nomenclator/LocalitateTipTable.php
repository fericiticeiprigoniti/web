<?php

/**
 * Class LocalitateTipTable
 * @table nom_localitati_tip
 */
class LocalitateTipTable extends CI_Model
{

    /**
     * @var LocalitateTipTable
     */
    private static $instance;


    /**
     * Singleton
     * LocalitateTipTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    // ---------------------------------------------------------------------------------------------

    /**
     *  we can later dependency inject and thus unit test this
     * @return LocalitateTipTable
     */
    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param string $cod
     * @return LocalitateTipItem|null
     * @throws Exception
     */
    public function load($cod)
    {
        if (!$cod) {
            throw new Exception('Cod tip localitate este invalid');
        }
        $filters = new LocalitateTipFilters(array('cod'=>$cod));
        $data = $this->fetchAll($filters);
        $item = isset($data['items']) && isset($data['items'][0]) ? $data['items'][0] : null;
        return $item;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param LocalitateTipFilters $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchAll(LocalitateTipFilters $filters = null, $limit = null, $offset = null)
    {
        // Set where and having conditions.
        $sqlWhere = array();
        $sqlHaving = array();
        if ($filters) {
            if ($filters->getCod()) {
                $sqlWhere[] = "tip_loc.cod = " . $this->db->escape($filters->getCod());
            }
            if ($filters->getCodExclus()) {
                $sqlWhere[] = "tip_loc.cod <> " . $this->db->escape($filters->getCodExclus());
            }
            if ($filters->getNume()) {
                $sqlWhere[] = "tip_loc.nume LIKE " . $this->db->escape($filters->getNume());
            }
            if ($filters->getOrder()) {
                $sqlWhere[] = "tip_loc.ord = {$filters->getOrder()}";
            }
        }
        $sqlWhere = implode("\n\tAND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sqlHaving = implode("\n\tAND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? "HAVING " . $sqlHaving : '';

        // ------------------------------------

        // Set order by.
        $sqlOrder = array();
        if (!count($sqlOrder)) {
            $sqlOrder[] = "ord ASC";
        }
        $sqlOrder = implode(", ", $sqlOrder);

        // ------------------------------------

        // Set limit.
        $sqlLimit = '';
        if (!is_null($offset) && !is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$offset . ', ' . (int)$limit;
        } elseif (!is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$limit;
        }

        // ------------------------------------

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    tip_loc.*
FROM
    nom_localitati_tip AS tip_loc
WHERE
    1
    $sqlWhere
GROUP BY tip_loc.id
$sqlHaving
ORDER BY $sqlOrder
$sqlLimit
EOSQL;

        /** @var CI_DB_result $result */
        $result = $this->db->query($sql);
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        $items = array();
        foreach($result->result('array') as $row){
            $items[] = new LocalitateTipItem($row);
        }

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------
    
    public function fetchForSelect()
    {
        $result = $this->fetchAll();
        $data = array();
        if (isset($result['items']) && count($result['items'])) {
            /** @var LocalitateTipItem $tip */
            foreach($result['items'] as $tip) {
                $data[$tip->getCod()] = $tip->getNume();
            }
        }
        return $data;
    }

    // ---------------------------------------------------------------------------------------------

    
}