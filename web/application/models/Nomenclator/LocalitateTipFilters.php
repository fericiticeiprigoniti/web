<?php

class LocalitateTipFilters
{

    /**
     * @var string
     */
    private $cod;

    /**
     * @var string
     */
    private $codExclus;

    /**
     * @var string
     */
    private $nume;

    /**
     * @var int
     */
    private $order = [];

    // ---------------------------------------------------------------------------------------------

    public function __construct(array $getData = array())
    {
        if (isset($getData['cod'])) {
            $this->setCod($getData['cod']);
        }
        if (isset($getData['cod_exclus'])) {
            $this->setCodExclus($getData['cod_exclus']);
        }
        if (isset($getData['nume'])) {
            $this->setNume($getData['nume']);
        }
        if (isset($getData['ord'])) {
            $this->setOrder($getData['ord']);
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return string|null
     */
    public function getCod()
    {
        return $this->cod;
    }

    /**
     * @param string|null $cod
     */
    public function setCod($cod)
    {
        $this->cod = is_null($cod) ? null : trim(strip_tags($cod));
    }

    /**
     * @return string|null
     */
    public function getCodExclus()
    {
        return $this->codExclus;
    }

    /**
     * @param string|null $codExclus
     */
    public function setCodExclus($codExclus)
    {
        $this->codExclus = is_null($codExclus) ? null : trim(strip_tags($codExclus));
    }

    /**
     * @return string|null
     */
    public function getNume()
    {
        return $this->nume;
    }

    /**
     * @param string|null $nume
     */
    public function setNume($nume)
    {
        $this->nume = is_null($nume) ? null : trim(strip_tags($nume));
    }
    
    /**
     * @return int|null
     */
    public function getOrder()
    {
        return $this->order;
    }
    
    /**
     * @param int|null $order
     */
    public function setOrder($order)
    {
        $this->order = $order ? (int)$order : null;
    }


}