<?php

/**
 * Class TagTable
 * @table fcp_taguri
 */
class TagTable extends CI_Model
{

    /**
     * Used for caching items
     * @var TagItem[]
     */
    public static $collection = array();

    /**
     * @var TagTable
     */
    private static $instance;


    /**
     * Singleton
     * TagTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * We can later dependency inject and thus unit test this
     * @return TagTable
     */
    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @param int $id
     */
    public static function resetCollectionId($id)
    {
        if (isset(self::$collection[$id])) {
            unset(self::$collection[$id]);
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param mixed $id
     * @return TagItem|null
     * @throws Exception
     */
    public function load($id)
    {
        if (!$id) {
            throw new Exception("Invalid Tag id");
        }

        // If data is cached return it
        if (MODEL_CACHING_ENABLED && isset(self::$collection[$id])) {
            return self::$collection[$id];
        }

        // Fetch data
        $filters = new TagFilters(array('id'=>$id));
        $data = $this->fetchAll($filters);

        // Cache result
        self::$collection[$id] = isset($data['items'][0]) ? $data['items'][0] : null;

        // Return cached result
        return self::$collection[$id];
    }

    /**
     * Reset item from static collection and load a new one
     * @param int $id
     * @return TagItem|null
     * @throws Exception
     */
    public function reload($id)
    {
        self::resetCollectionId($id);
        return $this->load($id);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param TagFilters $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchAll(TagFilters $filters = null, $limit = null, $offset = null)
    {
        // Set where and having conditions.
        $sqlWhere = array();
        $sqlHaving = array();
        if ($filters) {
            if ($filters->getId()) {
                $sqlWhere[] = "id = {$filters->getId()}";
            }
            if ($filters->getExcludedId()) {
                $sqlWhere[] = "id <> {$filters->getExcludedId()}";
            }
            if ($filters->getCatId()) {
                $sqlWhere[] = "cat_id = {$filters->getCatId()}";
            }
            if ($filters->getIdList() && count($filters->getIdList())) {
                $idList = implode(',', $filters->getIdList());
                $sqlWhere[] = "id IN ({$idList})";
            }
            if ($filters->getNume()) {
                $sqlWhere[] = "nume LIKE {$this->db->escape($filters->getNume())}";
            }
            if (!is_null($filters->isDeleted())) {
                $sqlWhere[] = "is_deleted = " . ($filters->isDeleted() ? '1' : '0');
            }

            if ($filters->getSearchLetter()) {
                // search by first letter
                $searchLetter = Strings::sanitizeForDatabaseUsage($filters->getSearchLetter());
                $sqlWhere[] = "nume REGEXP '^[{$searchLetter}].*$'";
            }
        }

        $sqlWhere = implode("\n\tAND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sqlHaving = implode("\n\tAND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? "HAVING " . $sqlHaving : '';

        // ------------------------------------

        $sqlOrder = array();
        if ($filters && count($filters->getOrderBy())) {
            foreach($filters->getOrderBy() as $ord) {
                switch($ord) {
                    case $filters::ORDER_BY_ID_ASC:
                        $sqlOrder[] = "id ASC";
                        break;
                    case $filters::ORDER_BY_ID_DESC:
                        $sqlOrder[] = "id DESC";
                        break;
                    case $filters::ORDER_BY_NAME_ASC:
                        $sqlOrder[] = "nume ASC";
                        break;
                    case $filters::ORDER_BY_NAME_DESC:
                        $sqlOrder[] = "nume DESC";
                        break;
                    case $filters::ORDER_BY_RAND:
                        $sqlOrder[] = "RAND()";
                        break;
        }
            }
        }
        if (!count($sqlOrder)) {
            $sqlOrder[] = "id DESC";
        }
        $sqlOrder = implode(", ", $sqlOrder);

        // ------------------------------------

        // Set limit.
        $sqlLimit = '';
        if (!is_null($offset) && !is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$offset . ', ' . (int)$limit;
        } elseif (!is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$limit;
        }

        // ------------------------------------

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    *
FROM
    fcp_taguri
WHERE
    1
    $sqlWhere
GROUP BY id
$sqlHaving
ORDER BY $sqlOrder
$sqlLimit
EOSQL;

        /** @var CI_DB_result $result */
        $result = $this->db->query($sql);
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        $items = array();
        foreach($result->result('array') as $row){
            $items[] = new TagItem($row);
        }

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param TagItem $item
     * @return bool|int
     */
    public function save(TagItem $item)
    {
        // Set data.
        $eData = array(
            'cat_id'        => $item->getCatId() ? $item->getCatId() : null,
            'nume'          => $item->getNume() ? $item->getNume() : null,
            'observatii'    => $item->getObservatii() ? $item->getObservatii() : null,
            'is_favorit'    => $item->getIsFavorit() ? $item->getIsFavorit() : null,
            'personaj_id'   => $item->getPersonajId() ? $item->getPersonajId() : null,
            'user_id'       => $item->getUserId() ? $item->getUserId() : $this->aauth->get_user_id(),
            'data_insert'   => !$item->getId() ? Calendar::sqlDateTime() : null,
            'hits'          => $item->getHits() ? $item->getHits() : null,
            'is_deleted'    => $item->isDeleted() ? 1 : 0,
        );

        // Insert/Update.
        if (!$item->getId()) {
            $res = $this->db->insert('fcp_taguri', $eData);
            $item->setId($this->db->insert_id());
        } else {
            $res = $this->db->update('fcp_taguri', $eData, 'id = ' . $item->getId());
        }

        // Remove old item from cache
        self::resetCollectionId($item->getId());

        return $res;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param TagItem $item
     * @return mixed
     */
    public function delete(TagItem $item)
    {
        $res = $this->db->update('fcp_taguri', ['is_deleted' => 1], 'id = ' . $item->getId());

        // Remove old item from cache
        self::resetCollectionId($item->getId());

        return $res;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * return key -> value array
     * @param TagFilters|null $filters
     * @return array
     */
    public function fetchForSelect(TagFilters $filters = null)
    {
        if (!$filters) {
            $filters = new TagFilters();
        }
        $result = $this->fetchAll($filters);

        $data = [];
        if (isset($result['items']) && count($result['items'])) {
            /** @var TagItem $objItem */
            foreach($result['items'] as $objItem) {
                $data[$objItem->getId()] = $objItem->getNume();
            }
        }

        return $data;
    }

}
