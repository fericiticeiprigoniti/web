<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_personaje_condamnari extends CI_Model
{

    const COND_TIP_IESIRE_LA_TERMEN      = 1;
    const COND_TIP_IESIRE_DECES_DETENTIE = 2;
    const COND_TIP_IESIRE_EXECUTAT       = 3;
    const COND_TIP_IESIRE_ACHITARE       = 4;
    const COND_TIP_IESIRE_GRATIERE       = 5;
    const COND_TIP_IESIRE_CLASARE_CAUZA  = 6;
    const COND_TIP_IESIRE_EVADARE        = 7;

    const DETENTIE_AREST_PREVENTIV      = 1;
    const DETENTIE_CONDAMNARE           = 2;
    const DETENTIE_INTERNARE_ADMINISTRATIVA  = 3;
    const DETENTIE_PRIZONIERAT          = 4;
    const DETENTIE_DEPORTARE            = 5;
    const DETENTIE_DEPORTARE_AFARA_TARII = 6;
    const DETENTIE_DOM_LAGAR            = 7;
    const DETENTIE_DOM_POST_DETENTIE    = 8;

    function __construct()
    {
        parent::__construct();
    }

    /**
     * GET condamnari by Personaj ID
     * @param mixed $persID
     * @return array
     */
    public function fetchAll($persID)
    {
        $persID = (int)$persID;

        $sql = "SELECT 
                    t1.*, t2.nr_sentinta, t2.data_sentinta, t3.nume as nume_instanta_judecata
                FROM fcp_personaje_episoade_represive as t1
                LEFT JOIN fcp_sentinte_detinuti as t2 ON t2.id = t1.sentinta_id
                LEFT JOIN fcp_instante_judecata t3 ON t3.id = instanta_judecata_id
                WHERE personaj_id = ?
                ORDER BY represiune_data_start ASC";

        return $this->db->query($sql, [$persID])->result_array();
    }

    public function add(array $fields)
    {
        return $this->db->insert('fcp_personaje_episoade_represive', $fields);
    }

    public function update($persID, $condamnareID, $fields)
    {
        return $this->db->update('fcp_personaje_episoade_represive', $fields, array('personaj_id' => (int)$persID, 'id' => (int)$condamnareID));
    }

    /**
     * delete condamnare - Tab Condamnari
     * 
     * @param mixed $persID
     * @param mixed $condamnareID
     */
    public function delete($persID, $condamnareID)
    {
        return $this->db->delete('fcp_personaje_episoade_represive', array(
            'personaj_id' => (int)$persID,
            'id'          => (int)$condamnareID
        ));
    }


    /**
     * pregatim pentru selector list cu toate locurile (mai putin cele care sunt sterse)
     *
     * @return void
     */
    public function fetchForSelect($persID)
    {
        $result = [];

        $items = $this->fetchAll($persID);
        $this->load->model('m_personaje_condamnari');
        $nomenclator_tip_represiune = $this->m_personaje_condamnari->fetchPersonajeDetentieIntrareTip();

        foreach ($items as $row) {
            if (empty($row['represiune_tip_id'])) continue;

            $result[$row['id']] = $nomenclator_tip_represiune[$row['represiune_tip_id']]
                . ' (' . (Calendar::convertDateMysql2Ro($row['represiune_data_start']) ?: '--')
                . ' / ' . (Calendar::convertDateMysql2Ro($row['represiune_data_end']) ?: '--') . ')';
        }

        return $result;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return array
     */
    public function fetchTipIesireListForSelect(): array
    {
        return [
            self::COND_TIP_IESIRE_LA_TERMEN      => 'la termen',
            self::COND_TIP_IESIRE_DECES_DETENTIE => 'deces în detenție',
            self::COND_TIP_IESIRE_EXECUTAT       => 'executat',
            self::COND_TIP_IESIRE_ACHITARE       => 'achitare',
            self::COND_TIP_IESIRE_GRATIERE       => 'grațiere',
            self::COND_TIP_IESIRE_CLASARE_CAUZA  => 'clasare cauză',
            self::COND_TIP_IESIRE_EVADARE        => 'evadare',
        ];
    }

    public function fetchPersonajeDetentieIntrareTip(): array
    {
        return [
            self::DETENTIE_AREST_PREVENTIV => 'arest preventiv',
            self::DETENTIE_CONDAMNARE => 'condamnare',
            self::DETENTIE_INTERNARE_ADMINISTRATIVA => 'internare administrativă',
            self::DETENTIE_PRIZONIERAT => 'prizonierat',
            self::DETENTIE_DEPORTARE => 'deportare',
            self::DETENTIE_DEPORTARE_AFARA_TARII => 'deportare în afara țării',
            self::DETENTIE_DOM_LAGAR => 'domiciliu obligatoriu în lagăr',
            self::DETENTIE_DOM_POST_DETENTIE => 'domiciliu obligatoriu post-detenție'
        ];
    }

    public function fetchPersonajeDetentieIntrareTipDescription(): array
    {
        return [
            self::DETENTIE_AREST_PREVENTIV => 'detenție care nu a fost urmată de trimiterea în judecată (s-a dispus scoaterea din cauză) sau a fost finalizată cu achitarea, chiar dacă s-a efectuat trimiterea în judecată.',
            self::DETENTIE_CONDAMNARE => 'detenție care a fost urmată de o condamnare penală.',
            self::DETENTIE_INTERNARE_ADMINISTRATIVA => 'detenție practicată în timpul regimului comunist, în baza unei decizii administrative dispuse de Securitatate, deci fără judecată, și concretizată de obicei în trimiterea în lagăr sau colonie de muncă, pe o durată ce putea fi cuprinsă între 6 și 60 de luni.',
            self::DETENTIE_PRIZONIERAT => 'detenție suportată atât în lagărele din U.R.S.S., ca urmare a participării și capturării pe frontul de Est, cât și în lagărele din Cehoslovacia, Germania sau Ungaria, ca urmare a capturării pe frontul de Vest.',
            self::DETENTIE_DEPORTARE => 'deportare în cadrul teritoriului național.',
            self::DETENTIE_DEPORTARE_AFARA_TARII => 'deportare în afara teritoriului național.',
            self::DETENTIE_DOM_LAGAR => 'detenție în lagăr sau o locație asimilată regimului de lagăr (de obicei mănăstiri), ca formă de represiune practicată de autoritățile române în perioada interbelică, pentru pentru legionari și comuniști, dar și de Germania nazistă în cazul legionarilor, până la capitularea din anul 1945.',
            self::DETENTIE_DOM_POST_DETENTIE => 'sau a celor care s-au dovedit a fi opozanți vocali și pe parcursul detenției.'
        ];
    }
}
