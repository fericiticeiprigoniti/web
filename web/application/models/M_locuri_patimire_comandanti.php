<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_locuri_patimire_comandanti extends CI_Model {

    const TABLE = 'fcp_locuri_patimire_comandanti';
    const ROL_COMANDANT_UNITATE_DETENTIE = 33;

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Returneaza lista comandantilor unui loc de patimire / surghiun
     * @param int|null $id
     * @param bool $keyValue
     * @return array
     */
    public function fetchAll($id = null, $keyValue = false)
    {
        $sql = <<<EOSQL
SELECT
    comandant.*
    , CONCAT_WS(' ', pers.nume, pers.prenume) AS comandant_nume
    , grad_militar.nume AS grad_militar_nume
FROM
    fcp_locuri_patimire_comandanti AS comandant
    LEFT JOIN fcp_personaje AS pers ON comandant.personaj_id = pers.id
    LEFT JOIN fcp_grade_militare AS grad_militar ON comandant.grad_id = grad_militar.id
WHERE 
    comandant.loc_patimire_id = ?
ORDER BY
    comandant.id
EOSQL;
        if ($keyValue) {
            $result = array();
            $query = $this->db->query($sql, array((int)$id));
            foreach ($query->result() as $row) {
                $result[$row->id] = $row->descriere;
            }
        } else {
            $result = $this->db->query($sql,array((int)$id))->result_array();
        }
        return $result;
    }

    public function save($data, $id=null)
    {
        $id = (int)$id;
        $eData = array(
            'personaj_id' => (int)$data['personaj_id'],
            'loc_patimire_id' => (int)$data['loc_patimire_id'],
            'grad_id' => (int)$data['grad_id'],
            'data_start' => (string)$data['data_start'],
            'data_end' => (string)$data['data_end'],
        );

        if (!$id)
        {
            $res = $this->db->insert(self::TABLE, $eData);
        } else {
            $res = $this->db->update(self::TABLE, $eData, array('id'=>$id));
        }

        return $res;
    }

    public function delete($id)
    {
        return $this->db->delete(self::TABLE, array('id' => (int)$id));
    }


}
