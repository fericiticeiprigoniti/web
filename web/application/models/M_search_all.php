<?php


class M_search_all
{

    const TIP_PERSONAJ = 'personaj';
    const TIP_AUTOR = 'autor';
    const TIP_CARTE = 'carte';
    const TIP_CATEGORIE_CARTE = 'categorie_carte';
    const TIP_ARTICOL = 'articol';
    const TIP_EDITURA = 'editura';
    const TIP_COLECTIE = 'colectie';


    // ---------------------------------------------------------------------------------------------

    public static function fetchTypes()
    {
        return array(
            self::TIP_PERSONAJ => 'Personaje (Nume complet)',
            self::TIP_AUTOR => 'Autori (Id | Nume | Prenume | Alias)',
            self::TIP_CARTE => "Carti (Id | Titlu | Subtitlu)",
            self::TIP_CATEGORIE_CARTE => 'Categorii carti (Id | Nume)',
            self::TIP_ARTICOL => 'Articole (Id/Data | Titlu)',
            self::TIP_EDITURA => 'Edituri (Id | Nume)',
            self::TIP_COLECTIE => 'Colectii (Id | Titlu)'
        );
    }

    // ---------------------------------------------------------------------------------------------

    public static function searchForItem($type, $searchTerm)
    {
        // Init data.
        $results = array();

        switch ($type) {
            case self::TIP_PERSONAJ:
                $results = self::searchPersonaj($searchTerm);
                break;
            case self::TIP_AUTOR:
                $results = self::searchAutor($searchTerm);
                break;
            case self::TIP_CARTE:
                $results = self::searchCarte($searchTerm);
                break;
            case self::TIP_CATEGORIE_CARTE:
                $results = self::searchCategorieCarte($searchTerm);
                break;
            case self::TIP_ARTICOL:
                $results = self::searchArticol($searchTerm);
                break;
            case self::TIP_EDITURA:
                $results = self::searchEditura($searchTerm);
                break;
            case self::TIP_COLECTIE:
                $results = self::searchColectie($searchTerm);
                break;
        }

        return $results;
    }

    // ---------------------------------------------------------------------------------------------

    public static function searchPersonaj($searchTerm)
    {
        // Init data.
        $results = array();
        $types = self::fetchTypes();

        // Fetch data and parse it
        $mPersonaje = new M_personaje();
        $data = $mPersonaje->fetchForAutocomplete($searchTerm);
        if ($data && count($data)) {
            foreach ($data as $k => $v) {
                $results[] = array(
                    'value' => $v['nume_prenume'],
                    'id' => $v['id'],
                    'data' => array('category' => $types[self::TIP_PERSONAJ]),
                    'url' => '/personaje/editeaza/' . $v['id'],
                );
            }
        }

        return $results;
    }

    // ---------------------------------------------------------------------------------------------

    public static function searchAutor($searchTerm)
    {
        // Init data.
        $results = array();
        $types = self::fetchTypes();

        // Fetch data.
        $data = AutorTable::getInstance()->fetchForAutocomplete($searchTerm);
        if ($data && count($data)) {
            /** @var AutorItem $autor */
            foreach ($data as $autor) {
                $results[] = array(
                    'value' => $autor->getSearchResult(),
                    'id' => $autor->getId(),
                    'data' => array('category' => $types[self::TIP_AUTOR]),
                    'url' => '/biblioteca/autor/edit/' . $autor->getId(),
                );
            }
        }

        return $results;
    }

    // ---------------------------------------------------------------------------------------------

    public static function searchCarte($searchTerm)
    {
        // Init data.
        $results = array();
        $types = self::fetchTypes();

        // Fetch data.
        $data = CarteTable::getInstance()->fetchForAutocomplete($searchTerm);
        if ($data && count($data)) {
            /** @var CarteItem $carte */
            foreach ($data as $carte) {
                $results[] = array(
                    'value' => $carte->getSearchResult(),
                    'id' => $carte->getId(),
                    'data' => array('category' => $types[self::TIP_CARTE]),
                    'url' => '/biblioteca/carti/edit/' . $carte->getId(),
                );
            }
        }

        return $results;
    }

    // ---------------------------------------------------------------------------------------------

    public static function searchCategorieCarte($searchTerm)
    {
        // Init data.
        $results = array();
        $types = self::fetchTypes();

        // Fetch data.
        $data = CategorieCarteTable::getInstance()->fetchForAutocomplete($searchTerm);
        if ($data && count($data)) {
            /** @var CategorieCarteItem $categorieCarte */
            foreach ($data as $categorieCarte) {
                $results[] = array(
                    'value' => $categorieCarte->getSearchResult(),
                    'id' => $categorieCarte->getId(),
                    'data' => array('category' => $types[self::TIP_CATEGORIE_CARTE]),
                    'url' => '/biblioteca/categorie_carte/edit/' . $categorieCarte->getId(),
                );
            }
        }

        return $results;
    }

    // ---------------------------------------------------------------------------------------------

    public static function searchArticol($searchTerm)
    {
        // Init data.
        $results = array();
        $types = self::fetchTypes();

        // Fetch data.
        $data = ArticolTable::getInstance()->fetchForAutocomplete($searchTerm);
        if ($data && count($data)) {
            /** @var ArticolItem $articol */
            foreach ($data as $articol) {
                $results[] = array(
                    'value' => $articol->getSearchResult(),
                    'id' => $articol->getId(),
                    'data' => array('category' => $types[self::TIP_ARTICOL]),
                    'url' => '/biblioteca/articol/edit/' . $articol->getId(),
                );
            }
        }

        return $results;
    }

    // ---------------------------------------------------------------------------------------------

    public static function searchEditura($searchTerm)
    {
        // Init data.
        $results = array();
        $types = self::fetchTypes();

        // Fetch data.
        $data = EdituraTable::getInstance()->fetchForAutocomplete($searchTerm);
        if ($data && count($data)) {
            /** @var EdituraItem $editura */
            foreach ($data as $editura) {
                $results[] = array(
                    'value' => $editura->getSearchResult(),
                    'id' => $editura->getId(),
                    'data' => array('category' => $types[self::TIP_EDITURA]),
                    'url' => '/biblioteca/editura/edit/' . $editura->getId(),
                );
            }
        }

        return $results;
    }

    // ---------------------------------------------------------------------------------------------

    public static function searchColectie($searchTerm)
    {
        // Init data.
        $results = array();
        $types = self::fetchTypes();

        // Fetch data.
        $data = ColectieTable::getInstance()->fetchForAutocomplete($searchTerm);
        if ($data && count($data)) {
            /** @var ColectieItem $colectie */
            foreach ($data as $colectie) {
                $results[] = array(
                    'value' => $colectie->getSearchResult(),
                    'id' => $colectie->getId(),
                    'data' => array('category' => $types[self::TIP_COLECTIE]),
                    'url' => '/biblioteca/colectie/edit/' . $colectie->getId(),
                );
            }
        }

        return $results;
    }

    // ---------------------------------------------------------------------------------------------


}
