<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_grade_militare extends CI_Model {


    const TABLE = 'fcp_grade_militare';


    function __construct()
    {
        parent::__construct();
    }

    /**
     * Fetch list
     * @param array $filters
     * @return array
     */
    public function fetchList($filters=array())
    {
        $sqlWhere = array();
        if (isset($filters['id']) && !empty($filters['id'])) {
            $sqlWhere[] = "id = " . (int)$filters['id'];
        }
        if (isset($filters['nume']) && !empty($filters['nume'])) {
            $sqlWhere[] = "nume LIKE " . $this->db->escape($filters['nume']);
        }
        $sqlWhere = count($sqlWhere) ? implode("\n\tAND ", $sqlWhere) : '';
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sql = <<<EOSQL
SELECT
    *
FROM
    fcp_grade_militare 
WHERE
    1
    $sqlWhere
EOSQL;
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    /**
     * Load element by id
     * @param int $id
     * @return array
     */
    public function load($id)
    {
        $data = $this->fetchList(array('id'=>$id));
        return isset($data[0]) ? $data[0] : array();
    }

    public function save($data, $id=null)
    {
        $id = (int)$id;
        $eData = array(
            'nume'      => $data['nume'],
        );

        if (!$id)
        {
            $res = $this->db->insert(self::TABLE, $eData);
        } else {
            $res = $this->db->update(self::TABLE, $eData, array('id'=>$id));
        }

        return $res;
    }

    public function delete($id)
    {
        $id  = (int)$id;
        $res = $this->db->delete(self::TABLE, array('id'=>$id));

        return $res;
    }

    public function fetchForSelect($filters = array())
    {
        $res  = $this->fetchList($filters);
        $data = array();

        if (count($res))
        {
            foreach($res as $v) {
                $data[$v['id']] = $v['nume'];
            }
        }
        return $data;
    }

}