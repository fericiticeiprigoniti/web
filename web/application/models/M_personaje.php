<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_personaje extends CI_Model
{


    function __construct()
    {
        parent::__construct();
    }

    /**
     * returneaza un array cu proprietatile cerute, sau doar proprietate ceruta daca e una
     *
     * @param array|null $fields
     * @param mixed $orderBy - column name
     * @param mixed $order - ASC | DESC
     * @return
     */
    function getProperty($fields = null, $orderBy = 'nume', $order = 'ASC')
    {
        $values = array();
        $cond = '1';

        if ($fields) {
            foreach ($fields as $key => $field) {
                if ($field != '') {
                    $cond .= ' AND ' . $key . ' = ?';
                    array_push($values, $field);
                }
            }
        }

        array_push($values, $orderBy, $order);
        $sql = "SELECT prs.*, prs.id as personaj_id, GROUP_CONCAT(rol_id) as roluri
                FROM fcp_personaje as prs
                LEFT JOIN fcp_personaje2roluri AS p2r ON p2r.personaj_id = prs.id
                WHERE {$cond}
                GROUP BY prs.id
                ORDER BY ? ?";

        return $this->db->query($sql, $values)->row_array();
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Lista personaje
     * @param array $filters
     * @param array $order
     * @param array $limit
     * @return array
     */
    public function fetchAll($filters = array(), $order = array(), $limit = array())
    {
        // Set filters.
        $sqlWhere = array();
        $sqlHaving = array();
        if (!empty($filters)) {
            foreach ($filters as $k => $v) {
                if ($v == '') {        // Preliminary check.
                    continue;
                }
                switch ($k) {

                        // Id filters
                    case 'id':
                    case 'nume_id':
                    case 'nationalitate':
                    case 'rol':
                    case 'ocupatie_arestare':
                    case 'confesiune':
                    case 'origine_sociala':
                    case 'loc_surghiun':
                        if ($k == 'id') {
                            $col = 'p.id';
                        } elseif ($k == 'nume_id') {
                            $col = 'p.id';
                        } elseif ($k == 'nationalitate') {
                            $col = 'nat.id';
                        } elseif ($k == 'rol') {
                            $col = 'p2r.rol_id';
                        } elseif ($k == 'ocupatie_arestare') {
                            $col = 'p2o.ocupatie_id';
                        } elseif ($k == 'confesiune') {
                            $col = 'p.confesiune_id';
                        } elseif ($k == 'origine_sociala') {
                            $col = 'p.orig_sociala_id';
                        } elseif ($k == 'loc_surghiun') {
                            $col = 'p2lp.loc_patimire_id';
                        } else {
                            $col = 'dont_know_this_column_name';
                        }
                        if (is_array($v) && count($v)) {
                            $v = array_map('intval', $v);
                            $v = implode(',', $v);
                            $sqlWhere[] = "{$col} IN ({$v})";
                        } else {
                            $v = (int)$v;
                            $sqlWhere[] = "{$col} = {$v}";
                        }
                        break;
                    case 'ocupatie_consacrata':
                        if (is_array($v) && count($v)) {
                            $v = array_map('intval', $v);
                            $v = implode(',', $v);
                            $sqlWhere[] = "p2o.ocupatie_id IN ({$v})";
                        } else {
                            $v = (int)$v;
                            $sqlWhere[] = "p2o.ocupatie_id = {$v}";
                        }
                        $sqlWhere[] = "p2o.is_primary = 1";
                        break;

                        // Date filters
                    case 'data_nastere_de_la':
                        if (Calendar::checkDate($v)) {
                            $v = Calendar::convertDate2Mysql($v);
                            $sqlWhere[] = "p.data_nastere <= {$v}";
                        }
                        break;
                    case 'data_nastere_pana_la':
                        if (Calendar::checkDate($v)) {
                            $v = Calendar::convertDate2Mysql($v);
                            $sqlWhere[] = "p.data_nastere >= {$v}";
                        }
                        break;
                    case 'data_adormire_de_la':
                        if (Calendar::checkDate($v)) {
                            $v = Calendar::convertDate2Mysql($v);
                            $sqlWhere[] = "p.data_adormire <= {$v}";
                        }
                        break;
                    case 'data_adormire_pana_la':
                        if (Calendar::checkDate($v)) {
                            $v = Calendar::convertDate2Mysql($v);
                            $sqlWhere[] = "p.data_adormire >= {$v}";
                        }
                        break;

                    case 'loc_nastere':
                        $sqlWhere[] = "loc.id = " . (int)$v;
                        break;

                    case 'luna_adormire':
                        $lunileAnului = Calendar::fetchLunileAnului();
                        if (in_array($v, array_keys($lunileAnului))) {
                            $v = (int)$v;
                            $sqlWhere[] = "MONTH(p.data_adormire) = {$v}";
                        }
                        break;
                    case 'sex':
                        if ($v == 'm') {
                            $sqlWhere[] = "p.sex = 1";
                        } elseif ($v == 'f') {
                            $sqlWhere[] = "p.sex = 0";
                        }
                        break;
                    case 'status':
                        if ($v == 'in_lucru') {
                            $sqlWhere[] = "p.status_id = 0";
                        } elseif ($v == 'activ') {
                            $sqlWhere[] = "p.status_id = 1";
                        } elseif ($v == 'sters') {
                            $sqlWhere[] = "p.is_deleted = 1";
                        }
                        break;
                }
            }
        }
        $sqlWhere = implode(" AND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? ' AND ' . $sqlWhere : '';

        $sqlHaving = implode(" AND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? ' HAVING ' . $sqlHaving : '';

        // -------------------------------

        // Set order.
        $sqlOrder = "nume_complet";
        $defOrderType = "ASC";
        $orderItems = array(
            'id',
            'nume_complet',
            'avatar_poza_id',
            'sex',
            'data_nastere',
            'data_adormire',
            'nr_roluri',
            'loc_nastere',
            'nationalitate',
            'nr_ocupatii',
            'ocupatii_socioprofesionale',
            'zi_adormire'
        );
        if (!empty($order['by']) && in_array($order['by'], $orderItems)) {
            $sqlOrder = $order['by'];
        }
        if (isset($order['dir']) && !empty($order['dir'])) {
            $sqlOrder .= ' ' . (in_array(strtoupper($order['dir']), array('ASC', 'DESC')) ? $order['dir'] : $defOrderType);
        } else {
            $sqlOrder .= ' ' . $defOrderType;
        }
        $sqlOrder = !empty($sqlOrder) ? "ORDER BY\n\t" . $sqlOrder : '';

        // -------------------------------

        // Set limit.
        if (isset($limit['start']) && isset($limit['per_page'])) {
            $start = (int)$limit['start'];
            $perPage = (int)$limit['per_page'];
            $sqlLimit = "LIMIT $start, $perPage";
        } elseif (isset($limit['per_page'])) {
            $perPage = (int)$limit['per_page'];
            $sqlLimit = "LIMIT $perPage";
        } else {
            $sqlLimit = '';
        }

        // -------------------------------

        // Tabel temporar pentru `roluri`.
        $tmpTable = 'tmp_roluri';
        $this->db->query("DROP TABLE IF EXISTS {$tmpTable}");
        $sql = <<<EOSQL
CREATE TEMPORARY TABLE {$tmpTable} AS
SELECT
    p2r.personaj_id
    , COUNT(rol.id) AS cnt
    , GROUP_CONCAT(rol.rol_nume ORDER BY rol.parent_id, rol.order_id ASC SEPARATOR ', ') AS roluri
FROM
    fcp_personaje2roluri AS p2r
    LEFT JOIN fcp_personaje_roluri AS rol ON p2r.rol_id = rol.id
GROUP BY
    p2r.personaj_id
EOSQL;
        $this->db->query($sql);
        $this->db->query("ALTER TABLE {$tmpTable} ADD UNIQUE INDEX `idx_personaj_id` (`personaj_id`)");

        // -------------------------------

        // Tabel temporar pentru `ocupatii`.
        $tmpTable = 'tmp_ocupatii';
        $this->db->query("DROP TABLE IF EXISTS {$tmpTable}");
        $sql = <<<EOSQL
CREATE TEMPORARY TABLE {$tmpTable} AS
SELECT
    p2o.personaj_id
    , COUNT(po.id) AS cnt
    , GROUP_CONCAT(po.nume ORDER BY po.parent_id, po.nume ASC SEPARATOR ', ') AS ocupatii
FROM
    fcp_personaje2ocupatii AS p2o
    LEFT JOIN fcp_personaje_ocupatii AS po ON p2o.ocupatie_id = po.id
GROUP BY
    p2o.personaj_id
EOSQL;
        $this->db->query($sql);
        $this->db->query("ALTER TABLE {$tmpTable} ADD UNIQUE INDEX `idx_personaj_id` (`personaj_id`)");

        // -------------------------------

        // Get data (items and count).
        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    p.*
    , DAY(p.data_adormire) AS zi_adormire
    , CONCAT_WS(', ',
        p.nume
        , p.prenume
        , p.prefix
    ) AS nume_complet
    , tmp_roluri.roluri
    , tmp_roluri.cnt AS nr_roluri

    , CONCAT_WS(', '
        , CONCAT(loc.tip_localitate, ' ', loc.nume)
        , CONCAT(' (', loc.nume_anterior, ')')
        , CONCAT(parent_loc.tip_localitate, ' ', parent_loc.nume)
        , CONCAT('judet ', judet.nume)
    ) AS loc_nastere

    , CONCAT_WS(', '
        , CONCAT(loc_d.tip_localitate, ' ', loc_d.nume)
        , CONCAT(' (', loc_d.nume_anterior, ')')
        , CONCAT(parent_loc_d.tip_localitate, ' ', parent_loc_d.nume)
        , CONCAT('judet ', judet_d.nume)
    ) AS loc_inmormantare

    , conf.nume AS confesiune
    , nat.nume AS nationalitate

    , tmp_ocupatii.ocupatii
    , tmp_ocupatii.cnt AS nr_ocupatii

    , CASE true
        WHEN p.is_deleted = 1 THEN 'sters'
        WHEN p.status_id = 0 THEN 'in_lucru'
        WHEN p.status_id = 1 THEN 'activ'
    END AS status

FROM
	fcp_personaje AS p
    LEFT JOIN fcp_personaje2roluri AS p2r ON p.id = p2r.personaj_id
    LEFT JOIN tmp_roluri ON p.id = tmp_roluri.personaj_id

-- Localitate nastere
    LEFT JOIN nom_localitati AS loc ON p.nastere_loc_id = loc.id
    LEFT JOIN nom_localitati AS parent_loc ON loc.parent_id = parent_loc.id
    LEFT JOIN nom_localitati_tip AS tip_loc ON tip_loc.cod = loc.tip_localitate
    LEFT JOIN nom_judete AS judet ON judet.id = loc.id_judet

-- Localitatea inmormantarii
    LEFT JOIN nom_localitati AS loc_d ON p.deces_loc_inmormantare_id = loc_d.id
    LEFT JOIN nom_localitati AS parent_loc_d ON loc_d.parent_id = parent_loc_d.id
    LEFT JOIN nom_localitati_tip AS tip_loc_d ON tip_loc_d.cod = loc_d.tip_localitate
    LEFT JOIN nom_judete AS judet_d ON judet_d.id = loc_d.id_judet

    LEFT JOIN fcp_nationalitati AS nat ON p.nationalitate_id = nat.id
    LEFT JOIN fcp_personaje2ocupatii AS p2o ON p.id = p2o.personaj_id
    LEFT JOIN tmp_ocupatii ON p.id = tmp_ocupatii.personaj_id
    LEFT JOIN fcp_confesiuni AS conf ON p.confesiune_id = conf.id
    LEFT JOIN fcp_personaje2locuripatimire AS p2lp ON p.id = p2lp.personaj_id
WHERE
	1
	$sqlWhere
GROUP BY
	p.id
$sqlHaving
$sqlOrder
$sqlLimit
EOSQL;
        $items = $this->db->query($sql)->result_array();
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;

        // Parse data and set complex names
        if ($items && count($items)) {
            foreach ($items as $k => $v) {
                $items[$k]['nume_complet'] = $this->processItemAndGetFullName($v, false);
                $items[$k]['nume_extrainfo'] = $this->processItemAndGetFullNameExtrainfo($v);
            }
        }

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Load element by id
     * @param int $id
     * @return array|bool
     */
    public function load($id)
    {
        $res = $this->fetchAll(array('id' => $id));
        if (isset($res['items']) && isset($res['items'][0])) {
            return $res['items'][0];
        } else {
            return false;
        }
    }

    // ---------------------------------------------------------------------------------------------

    public function fetchAllForDropDown()
    {
        $pageNO     = 1;
        $perPage    = $this->config->item('admin_items_per_page');
        $offset     = ($pageNO - 1) * $perPage;

        $sql = "SELECT CONCAT(nume,' ', prenume) as label, id as value
                FROM fcp_personaje";

        return $this->db->query($sql, array($offset, $perPage))->result_array();
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * returneaza lista de semnalmente a unui personaj
     *
     * @param mixed $persID
     * @return array
     */
    public function fetchSemnalmente($persID)
    {
        $persID = (int)$persID;
        $sql = "SELECT ps.*, p2s.detalii
                FROM fcp_personaje_semnalmente as ps
                LEFT JOIN fcp_personaje2semnalmente as p2s ON p2s.semn_id = ps.id
                WHERE p2s.personaj_id = ?
                ORDER BY ps.id ASC";

        $result = array();
        $query = $this->db->query($sql, array($persID));
        foreach ($query->result() as $row) {
            $result[$row->id] = $row->detalii;
        }

        return $result;
    }

    /**
     * get detalii semnalment persoana
     *
     * @param mixed $persID
     * @param mixed $semnID
     */
    public function fetchSemnalment($persID, $semnID)
    {
        $sql = "SELECT ps.*, p2s.detalii
                FROM fcp_personaje_semnalmente as ps
                LEFT JOIN fcp_personaje2semnalmente as p2s ON p2s.semn_id = ps.id
                WHERE p2s.personaj_id = ? AND ps.id = ?";

        return $this->db->query($sql, array((int)$persID, (int)$semnID))->row_array();
    }

    /**
     * adauga semnalment nou
     *
     * @param array $details
     * @return CI_DB_active_record|CI_DB_result
     */
    public function addSemnalment($details)
    {
        return $this->db->insert('fcp_personaje2semnalmente', array(
            'personaj_id' => (int)$details['persID'],
            'semn_id'     => (int)$details['semn_id'],
            'detalii'     => !empty($details['detalii']) ? $details['detalii'] : null
        ));
    }

    public function updateSemnalment($semnID, $text)
    {
        return $this->db->update('fcp_personaje2semnalmente', array('detalii' => $text), array('semn_id' => (int)$semnID));
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * returneaza lista de distinctii a unui personaj
     *
     * @param mixed $persID
     * @return array
     */
    public function fetchRealizari($persID)
    {
        $sql = "SELECT *
                FROM fcp_personaje_realizari_socioprofesionale
                WHERE personaj_id = ?
                ORDER BY id ASC";

        return $this->db->query($sql, [(int)$persID])->result_array();
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * get pedepsele de detentie a unui personaj
     *
     * @param mixed $persID
     */
    public function fetchPedepse($persID)
    {
        $persID = (int)$persID;
        $sql = "SELECT *
                FROM fcp_personaje_pedepse_detentie
                WHERE personaj_id = ?
                ORDER BY order_id ASC";

        return $this->db->query($sql, array($persID))->result_array();
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * adauga o realizare socio profesioanla - unui personaj
     *
     * @param mixed $persID
     * @param mixed $details
     * @return CI_DB_active_record|CI_DB_result
     */
    public function addRealizare($persID, $details)
    {
        return $this->db->insert('fcp_personaje_realizari_socioprofesionale', array(
            'personaj_id'   => (int)$persID,
            'titlu_primit'  => $details['realizare'],
            'data'          => $details['data']
        ));
    }

    /**
     * delete realizare socio profesionala - personaj
     *
     * @param mixed $persID
     * @param mixed $id
     * @return CI_DB_active_record|CI_DB_result
     */
    public function deleteRealizare($persID, $id)
    {
        return $this->db->delete('fcp_personaje_realizari_socioprofesionale', array(
            'personaj_id' => (int)$persID,
            'id'          => (int)$id
        ));
    }

    /**
     * delete itinerariu detentie - personaj
     *
     * @param mixed $persID
     * @param mixed $itemID
     * @return CI_DB_active_record|CI_DB_result
     */
    public function deleteItinerariu($persID, $itemID)
    {
        return $this->db->delete('fcp_personaje2locuripatimire', array(
            'personaj_id' => (int)$persID,
            'id'          => (int)$itemID
        ));
    }


    // ---------------------------------------------------------------------------------------------

    /**
     * get detalii condamnare persoana
     *
     * @param mixed $persID
     * @param mixed $condID
     */
    public function fetchCondamnare($persID, $condID)
    {
        $sql = "SELECT *
                FROM fcp_personaje_episoade_represive
                WHERE personaj_id = ? AND id = ?";

        return $this->db->query($sql, array((int)$persID, (int)$condID))->row_array();
    }

    /**
     * returneaza lista de condamnari a unui personaj
     *
     * @param mixed $persID
     */
    public function fetchAllCondamnari($persID)
    {
        $sql = "SELECT *
                FROM fcp_personaje_episoade_represive
                WHERE personaj_id = ?";

        return $this->db->query($sql, array((int)$persID))->row_array();
    }

    /**
     * adauga pedeapsa disciplinara
     *
     * @param array $details
     * @return CI_DB_active_record|CI_DB_result
     */
    public function addPedeapsaDisciplinara($details)
    {
        return $this->db->insert('fcp_personaje_pedepse_detentie', $details);
    }

    /**
     * delete pedeapsa disciplinara - personaj
     *
     * @param mixed $persID
     * @param mixed $pedeapsaID
     * @return CI_DB_active_record|CI_DB_result
     */
    public function deletePedeapsaDisciplinaraPersonaj($persID, $pedeapsaID)
    {
        return $this->db->delete('fcp_personaje_pedepse_detentie', array(
            'personaj_id' => (int)$persID,
            'id'          => (int)$pedeapsaID
        ));
    }

    /**
     * update pedepse disciplinare
     *
     * @param mixed $pedeapsaID
     * @param array $data - fields
     *
     * @return bool
     */
    public function updatePedeapsaDisciplinara($pedeapsaID, $data)
    {
        return $this->db->update('fcp_personaje_pedepse_detentie', $data, array('id' => (int)$pedeapsaID));
    }

    /**
     * returneaza lista de sentinte detinuti
     *
     * @param mixed $keyValue
     * @return array
     */
    public function fetchAllSentinteDetinuti($keyValue = false)
    {
        $sql = "SELECT t1.*, t2.nume as nume_instanta_judecata
                FROM fcp_sentinte_detinuti as t1
                LEFT JOIN fcp_instante_judecata t2 ON t2.id = instanta_judecata_id 
                ORDER BY nume_lot_detinuti";

        if ($keyValue) {
            $result = array();
            $query = $this->db->query($sql);
            foreach ($query->result() as $row) {
                $result[$row->id] = $row->nr_sentinta
                    . '/' . Calendar::convertDateMysql2Ro($row->data_sentinta)
                    . ', ' . $row->nume_instanta_judecata;
            }
        } else {
            $result = $this->db->query($sql)->result_array();
        }

        return $result;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Fetch data for autocomplete
     * @param string $searchTerm
     * @param array $filters
     * @return bool|array
     */
    public function fetchForAutocomplete($searchTerm, $filters = array())
    {
        // Clean search term.
        $searchTerm = Strings::sanitizeForDatabaseUsage($searchTerm);

        // Extract words.
        $words = Strings::splitIntoWords($searchTerm);
        if (!count($words)) {
            return false;
        }

        // Set filters.
        $sqlWhere = array();
        if (!empty($filters)) {
            foreach ($filters as $k => $v) {
                if ($v == '') {        // Preliminary check.
                    continue;
                }
                switch ($k) {
                    case 'rol_id':
                        $sqlWhere[] = "p2r.rol_id = {$v}";
                        break;
                }
            }
        }

        // Set where condition
        $subCond = array();
        foreach ($words as $word) {
            $word = $this->db->escape("%$word%");
            $cond = "p.prefix LIKE $word OR p.nume LIKE $word OR p.prenume LIKE $word";
            $cond .= " OR p.alias LIKE $word";
            $cond .= " OR p.nume_anterior LIKE $word";
            $cond .= " OR p.prenume_monahism LIKE $word";
            $cond .= " OR p.nume_pseudonim LIKE $word";
            $cond .= " OR p.nume_porecla LIKE $word";
            $subCond[] = "($cond)";
        }
        $sqlWhere[] = implode("\n\tAND ", $subCond);



        $sqlWhere = implode(" AND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? ' AND ' . $sqlWhere : '';


        // Fetch data
        $sql = <<<EOSQL
SELECT
    p.id
    , p.prefix
    , p.nume
    , p.prenume
    , p.alias
    , p.nume_anterior
    , p.prenume_monahism
    , p.nume_pseudonim
    , p.nume_porecla
    , CONCAT_WS(', '
        , p.nume
        , p.prenume
    ) AS nume_prenume
FROM
    fcp_personaje AS p
    LEFT JOIN fcp_personaje2roluri AS p2r ON p.id = p2r.personaj_id
WHERE
    1
    $sqlWhere
GROUP BY
    p.id
ORDER BY
    nume_prenume ASC
LIMIT ?
EOSQL;
        $data = $this->db->query($sql, [LIMIT_FOR_AUTOCOMPLETE])->result_array();

        // Parse data and set complex names
        if ($data && count($data)) {
            foreach ($data as $k => $v) {
                $data[$k]['nume_complet'] = $this->processItemAndGetFullName($v);
            }
        }

        return $data;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Fetch id for autocomplete
     * @param string $searchTerm
     * @return bool|array
     */
    public function fetchIdForAutocomplete($searchTerm)
    {
        // Clean search term.
        $searchTerm = Strings::sanitizeForDatabaseUsage($searchTerm);

        // Extract words.
        $words = Strings::splitIntoWords($searchTerm);
        if (!count($words)) {
            return false;
        }

        // Set where condition
        $subCond = array();
        foreach ($words as $word) {
            $word = $this->db->escape("%$word%");
            $subCond[] = "(p.id LIKE $word)";
        }
        $sqlWhere = 'AND ' . implode("\n\tAND ", $subCond);

        // Fetch data
        $sql = <<<EOSQL
SELECT
    p.id
FROM
    fcp_personaje AS p
WHERE
    1
    $sqlWhere
ORDER BY
    id ASC
LIMIT 10
EOSQL;
        return $this->db->query($sql)->result_array();
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param array $item
     * @param bool withExtrainfo (false - return only name with prefix)
     * @return string
     */
    private function processItemAndGetFullName(array $item, $withExtrainfo = true)
    {
        $numeComplet = trim($item['nume']) . ', ' . trim($item['prenume']);
        $numeComplet .= !empty(trim($item['prefix'])) ? ', ' . trim($item['prefix']) : '';

        // Extra
        if ($withExtrainfo) {
            if ($item['alias'] || $item['nume_anterior'] || $item['prenume_monahism'] || $item['nume_pseudonim'] || $item['nume_porecla']) {
                $extra = [];
                if ($item['alias']) {
                    $extra[] = 'alias ' . $item['alias'];
                }
                if ($item['nume_anterior']) {
                    $extra[] = 'anterior ' . $item['nume_anterior'];
                }
                if ($item['prenume_monahism']) {
                    $extra[] = 'in monahism ' . $item['prenume_monahism'];
                }
                if ($item['nume_pseudonim']) {
                    $extra[] = 'pseudonim ' . $item['nume_pseudonim'];
                }
                if ($item['nume_porecla']) {
                    $extra[] = 'porecla ' . $item['nume_porecla'];
                }
                $extraList = implode(', ', $extra);
                $numeComplet .= " ($extraList)";
            }
        }

        return $numeComplet;
    }

    /**
     * Adauga doar extrainformatiile
     *
     * @param array $item
     * @return void
     */
    private function processItemAndGetFullNameExtrainfo(array $item)
    {
        $numeComplet = "";

        // Extra
        if ($item['alias'] || $item['nume_anterior'] || $item['prenume_monahism'] || $item['nume_pseudonim'] || $item['nume_porecla']) {
            $extra = [];
            // if ($item['alias']) {
            //     $extra[] = 'alias ' . $item['alias'];
            // }
            if ($item['nume_anterior']) {
                $extra[] = 'anterior ' . $item['nume_anterior'];
            }
            if ($item['prenume_monahism']) {
                $extra[] = 'in monahism ' . $item['prenume_monahism'];
            }
            if ($item['nume_pseudonim']) {
                $extra[] = 'pseudonim ' . $item['nume_pseudonim'];
            }
            if ($item['nume_porecla']) {
                $extra[] = 'porecla ' . $item['nume_porecla'];
            }

            $extraList = implode(', ', $extra);
            if (!empty($extraList)) {
                $numeComplet .= " ($extraList)";
            }
        }

        return $numeComplet;
    }
}
