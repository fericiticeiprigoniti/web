<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_citate extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Returneaza lista de citate
     * @param int $persID
     * @param bool $keyValue
     * @return array
     */
    public function fetchAll($persID, $keyValue = false)
    {
        $sql = "SELECT * FROM fcp_personaje_citate WHERE personaj_id = ?";

        if ($keyValue) {
            $result = array();
            $query = $this->db->query($sql, array((int)$persID));
            foreach ($query->result() as $row) {
                $result[$row->id] = $row->nume;
            }
        } else {
            $result = $this->db->query($sql,array((int)$persID))->result_array();
        }
        return $result;
    }

    /**
    * adauga un citat - unui personaj
    *
    * @param mixed $persID
    * @param mixed $data
    * @return CI_DB_active_record|CI_DB_result
    */
    public function add($persID, $data)
    {
        return $this->db->insert('fcp_personaje_citate', array('personaj_id'    => (int)$persID,
                                                               'continut'       => $data['continut'],
                                                               'carte_id'       => $data['carte_id'],
                                                               'sursa'          => $data['sursa'],
                                                               'observatii'     => $data['observatii'],
                                                               'pag_start'      => $data['pag_start'],
                                                               'pag_end'        => $data['pag_end'],
                                                               'insert_data'    => Calendar::sqlDateTime()
        ));
    }

    function update($id, $data)
    {
        return $this->db->update('fcp_personaje_citate', $data, array('id' => $id));
    }

    public function delete($persID, $id)
    {
        return $this->db->delete('fcp_personaje_citate', array('personaj_id' => (int)$persID,
                                                               'id'          => (int)$id));
    }

    function getInfo($citatID)
    {
        return $this->db->get_where('fcp_personaje_citate', array('id' => (int)$citatID))->row_array();
    }
}
