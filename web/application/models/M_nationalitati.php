<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_nationalitati extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Returneaza lista de nationalitati
     * @param bool $keyValue
     * @return array
     */
    public function fetchAll($keyValue=false)
    {
        $sql = "SELECT * FROM fcp_nationalitati ORDER BY nume";
        if ($keyValue) {
            $result = array();
            $query = $this->db->query($sql);
            foreach ($query->result() as $row) {
                $result[$row->id] = $row->nume;
            }
        } else {
            $result = $this->db->query($sql)->result_array();
        }
        return $result;
    }
}