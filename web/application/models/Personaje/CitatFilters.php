<?php

class CitatFilters
{

    const ORDER_BY_ID_ASC = 'id_asc';
    const ORDER_BY_ID_DESC = 'id_desc';
    const ORDER_BY_RAND = 'order_rand';

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $excludedId;

    /**
     * @var int
     */
    private $personajId;

    /**
     * @var int
     */
    private $locPatimireId;

    /**
     * @var int
     * @example Afiseaza citatele pentru proiectul poetiiinchisorilor
     */
    private $vizibilPi;

    /**
     * @var int
     * @example 1 = inactive / 0 = active
     */
    private $isDeleted;

    /**
     * @var array
     * @example Suporta: id_asc / id_desc / order_rand
     */
    private $orderBy = [];

    // ---------------------------------------------------------------------------------------------

    public function __construct(array $getData = [])
    {
        if (isset($getData['id'])) {
            $this->setId($getData['id']);
        }
        if (isset($getData['excludedId'])) {
            $this->setExcludedId($getData['excludedId']);
        }
        if (isset($getData['personajId'])) {
            $this->setPersonajId($getData['personajId']);
        }
        if (isset($getData['locPatimireId'])) {
            $this->setLocPatimireId($getData['locPatimireId']);
        }
        if (isset($getData['vizibilPi'])) {
            $this->setVizibilPi($getData['vizibilPi']);
        }
        if (isset($getData['isDeleted'])) {
            $this->setIsDeleted($getData['isDeleted']);
        }
        if (isset($getData['_orderBy'])) {
            $this->setOrderBy($getData['_orderBy']);
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return int
     */
    public function getExcludedId()
    {
        return $this->excludedId;
    }

    /**
     * @param int $excludedId
     */
    public function setExcludedId($excludedId)
    {
        $this->excludedId = $excludedId ? (int)$excludedId : null;
    }

    /**
     * @return int
     */
    public function getPersonajId()
    {
        return $this->personajId;
    }

    /**
     * @param int $personajId
     */
    public function setPersonajId($personajId)
    {
        $this->personajId = $personajId ? (int)$personajId : null;
    }

    /**
     * @return int
     */
    public function getLocPatimireId()
    {
        return $this->locPatimireId;
    }

    /**
     * @param int $locPatimireId
     */
    public function setLocPatimireId($locPatimireId)
    {
        $this->locPatimireId = $locPatimireId ? (int)$locPatimireId : null;
    }

    /**
     * @return bool|null
     */
    public function getVizibilPi()
    {
        return $this->vizibilPi;
    }

    /**
     * @param bool|null $vizibilPi
     */
    public function setVizibilPi($vizibilPi)
    {
        $this->vizibilPi = !is_null($vizibilPi) ? (bool)$vizibilPi : null;
    }

    /**
     * @return bool|null
     */
    public function isDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param bool|null $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = !is_null($isDeleted) ? (bool)$isDeleted : null;
    }

    /**
     * @return array
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param array $orderBy
     */
    public function setOrderBy(array $orderBy)
    {
        $orderBy = (is_array($orderBy) && count($orderBy) ? $orderBy : array());
        if (count($orderBy)) {
            $orderItems = array_keys(self::fetchOrderItems());
            foreach ($orderBy as $k=>$v) {
                if (!in_array($v, $orderItems)) {
                    unset($orderBy[$k]);
                }
            }
        }
        $this->orderBy = $orderBy;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return array
     */
    public static function fetchOrderItems()
    {
        return array(
            self::ORDER_BY_ID_ASC  => 'id_asc',
            self::ORDER_BY_ID_DESC => 'id_desc',
            self::ORDER_BY_RAND    => 'Order - RAND',
        );
    }


}
