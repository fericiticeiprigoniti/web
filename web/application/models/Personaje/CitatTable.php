<?php

/**
 * Class CitatTable
 * @table fcp_personaje_citate
 */
class CitatTable extends CI_Model
{

    /**
     * Used for caching items
     * @var CitatItem[]
     */
    public static $collection = [];

    /**
     * @var CitatTable
     */
    private static $instance;


    /**
     * Singleton
     * CitatTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * We can later dependency inject and thus unit test this
     * @return CitatTable
     */
    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @param int $id
     */
    public static function resetCollectionId($id)
    {
        if (isset(self::$collection[$id])) {
            unset(self::$collection[$id]);
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param mixed $id
     * @return CitatItem|null
     * @throws Exception
     */
    public function load($id)
    {
        if (!$id) {
            throw new Exception("Invalid Citat id");
        }

        // If data is cached return it
        if (MODEL_CACHING_ENABLED && isset(self::$collection[$id])) {
            return self::$collection[$id];
        }

        // Fetch data
        $filters = new CitatFilters(array('id'=>$id));
        $data = $this->fetchAll($filters);

        // Cache result
        self::$collection[$id] = isset($data['items'][0]) ? $data['items'][0] : null;

        // Return cached result
        return self::$collection[$id];
    }

    /**
     * Reset item from static collection and load a new one
     * @param mixed $id
     * @return CitatItem|null
     * @throws Exception
     */
    public function reload($id)
    {
        self::resetCollectionId($id);
        return $this->load($id);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param CitatFilters $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchAll(CitatFilters $filters = null, $limit = null, $offset = null)
    {
        // Set where and having conditions.
        $sqlWhere = array();
        $sqlHaving = array();
        if ($filters) {
            if ($filters->getId()) {
                $sqlWhere[] = "id = {$filters->getId()}";
            }
            if ($filters->getExcludedId()) {
                $sqlWhere[] = "id <> {$filters->getExcludedId()}";
            }
            if ($filters->getPersonajId()) {
                $sqlWhere[] = "personaj_id = {$filters->getPersonajId()}";
            }
            if ($filters->getLocPatimireId()) {
                $sqlWhere[] = "loc_patimire_id = {$filters->getLocPatimireId()}";
            }
            if (!is_null($filters->getVizibilPi())) {
                if ($filters->getVizibilPi()) {
                    $sqlWhere[] = "vizibil_pi > 0";
                } else {
                    $sqlWhere[] = "(vizibil_pi IS NULL OR vizibil_pi = 0)";
                }
            }
            if (!is_null($filters->isDeleted())) {
                if ($filters->isDeleted()) {
                    $sqlWhere[] = "is_deleted > 0";
                } else {
                    $sqlWhere[] = "(is_deleted IS NULL OR is_deleted = 0)";
                }
            }
        }

        $sqlWhere = implode("\n\tAND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sqlHaving = implode("\n\tAND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? "HAVING " . $sqlHaving : '';

        // ------------------------------------

        $sqlOrder = array();
        if ($filters && count($filters->getOrderBy())) {
            foreach($filters->getOrderBy() as $ord) {
                switch($ord) {
                    case $filters::ORDER_BY_ID_ASC:
                        $sqlOrder[] = "id ASC";
                        break;
                    case $filters::ORDER_BY_ID_DESC:
                        $sqlOrder[] = "id DESC";
                        break;
                    case $filters::ORDER_BY_RAND:
                        $sqlOrder[] = "RAND()";
                        break;
        }
            }
        }
        if (!count($sqlOrder)) {
            $sqlOrder[] = "id DESC";
        }
        $sqlOrder = implode(", ", $sqlOrder);

        // ------------------------------------

        // Set limit.
        $sqlLimit = '';
        if (!is_null($offset) && !is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$offset . ', ' . (int)$limit;
        } elseif (!is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$limit;
        }

        // ------------------------------------

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    *
FROM
    fcp_personaje_citate
WHERE
    1
    $sqlWhere
GROUP BY id
$sqlHaving
ORDER BY $sqlOrder
$sqlLimit
EOSQL;

        /** @var CI_DB_result $result */
        $result = $this->db->query($sql);
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        $items = array();
        foreach($result->result('array') as $row){
            $items[] = new CitatItem($row);
        }

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param CitatItem $item
     * @return bool|int
     */
    public function save(CitatItem $item)
    {
        // Set data.
        $eData = array(
            'personaj_id' => $item->getPersonajId() ? $item->getPersonajId() : null,
            'continut' => $item->getContinut() ? $item->getContinut() : null,
            'carte_id' => $item->getCarteId() ? $item->getCarteId() : null,
            'sursa' => $item->getSursa() ? $item->getSursa() : null,
            'loc_patimire_id' => $item->getLocPatimireId() ? $item->getLocPatimireId() : null,
            'observatii' => $item->getObservatii() ? $item->getObservatii() : null,
            'sync_citateortodoxe' => $item->getSyncCitateortodoxe() ? $item->getSyncCitateortodoxe() : null,
            'cron' => $item->getCron() ? $item->getCron() : null,
            'pag_start' => $item->getPagStart() ? $item->getPagStart() : null,
            'pag_end' => $item->getPagEnd() ? $item->getPagEnd() : null,
            'insert_data' => $item->getInsertData() ? $item->getInsertData() : null,
            'vizibil_pi' => $item->getVizibilPi() ? $item->getVizibilPi() : null,
            'is_deleted' => $item->getIsDeleted() ? $item->getIsDeleted() : null,
        );

        // Insert/Update.
        if (!$item->getId()) {
            $res = $this->db->insert('fcp_personaje_citate', $eData);
        } else {
            $res = $this->db->update('fcp_personaje_citate', $eData, 'id = ' . $item->getId());
        }

        // Remove old item from cache
        self::resetCollectionId($item->getId());

        return $res;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param CitatItem $item
     * @return mixed
     */
    public function delete(CitatItem $item)
    {
        // Remove old item from cache
        self::resetCollectionId($item->getId());

        return $this->db->delete('fcp_personaje_citate', 'id = ' . $item->getId());
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param CitatFilters|null $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchForApi(CitatFilters $filters = null, $limit = null, $offset = null, &$tCount)
    {
        $data = $this->fetchAll($filters, $limit, $offset);
        $return = [];
        if (isset($data['items']) && count($data['items'])) {
            /** @var CitatItem $item */
            foreach($data['items'] as $item) {
                $result = $item->toArray();
                if ($personajInfo = $item->getPersonaj())
                    $result['personajInfo'] = $personajInfo->toArray();
                if ($carteInfo = $item->getCarte())
                    $result['carteInfo'] = $carteInfo->toArray();

                $return[] = $result;
            }
            $tCount = $data['count'];
        }
        return $return;
    }

}
