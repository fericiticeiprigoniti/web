<?php

class PersonajFilters
{

    const ORDER_BY_ID_ASC   = 'id_asc';
    const ORDER_BY_ID_DESC  = 'id_desc';
    const ORDER_BY_NAME_ASC = 'name_asc';
    const ORDER_BY_NAME_DESC= 'name_desc';
    const ORDER_BY_COMEMORARI_ASC = 'comemorari_asc';
    const ORDER_BY_COMEMORARI_DESC = 'comemorari_asc';

    /**
     * @var int
     * @example ID-ul personajului
     */
    private $id;

    /**
     * @var int
     * @example Exclude un Personaj din lista rezultată
     */
    private $excludedId;

    /**
     * @var int
     * @example Rolul personajului
     */
    private $rolID;

    /**
     * @var int
     * @example Numarul de zile inainte de ziua curenta. Default este setat 0.
     */
    private $daysBeforeCDay = 0;

    /**
     * @var int
     * @example Numarul de zile după ziua curentă. Default este setat 2.
     */
    private $daysAfterCDay = 2;

    /**
     * @var int
     * @example Pentru acest endpoint este selectat implicit valoarea 1
     */
    private $showComemorari;

     /**
     * @var string
     */
    private $searchLetter;

    /**
     * @var string
     */
    private $searchTerm;

    /**
     * @var array
     * @example Suporta: id_asc / id_desc / name_asc / name_desc / comemorari_asc / comemorari_desc
     */
    private $orderBy = [];

       /**
     * @var int
     */
    private $statusId;

    // ---------------------------------------------------------------------------------------------

    public function __construct(array $getData = [])
    {
        if (isset($getData['id'])) {
            $this->setId($getData['id']);
        }
        if (isset($getData['excludedId'])) {
            $this->setExcludedId($getData['excludedId']);
        }
        if (isset($getData['rolID'])) {
            $this->setRolID($getData['rolID']);
        }
        if (isset($getData['daysBeforeCDay'])) {
            $this->setComemorariBeforeCurrentDay($getData['daysBeforeCDay']);
        }
        if (isset($getData['daysAfterCDay'])) {
            $this->setComemorariAfterCurrentDay($getData['daysAfterCDay']);
        }
        
        if (isset($getData['statusId'])) {
            $this->setRolID($getData['statusId']);
        }

        if (isset($getData['searchLetter'])) {
            $this->setSearchLetter($getData['searchLetter']);
        }
        if (isset($getData['searchTerm'])) {
            $this->setSearchTerm($getData['searchTerm']);
        }

        if (isset($getData['_orderBy'])) {
            $this->setOrderBy($getData['_orderBy']);
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return int
     */
    public function getExcludedId()
    {
        return $this->excludedId;
    }

    /**
     * @param int $excludedId
     */
    public function setExcludedId($excludedId)
    {
        $this->excludedId = $excludedId ? (int)$excludedId : null;
    }

    /**
     * @return int
     */
    public function getRolID(){
        return $this->rolID;
    }

    /**
     * @param int $val
     */
    public function setRolID($val){
        $this->rolID = $val ? (int)$val : null;
    }

       /**
     * @return int
     */
    public function getStatusId(){
        return $this->statusId;
    }

    /**
     * @param int $val
     */
    public function setStatusId($val){
        $this->statusId = $val ? (int)$val : null;
    }

     /**
     * @return string|null
     */
    public function getSearchLetter()
    {
        return $this->searchLetter;
    }

    /**
     * @param string|null $val
     */
    public function setSearchLetter($val)
    {
        $this->searchLetter = is_null($val) ? null : trim(strip_tags($val));
    }

    /**
     * @return string|null
     */
    public function getSearchTerm()
    {
        return $this->searchTerm;
    }

    /**
     * @param string|null $val
     */
    public function setSearchTerm($val)
    {
        $this->searchTerm = is_null($val) ? null : trim(strip_tags($val));
    }


    /**
     * @return string|null
     */
    public function getComemorariBeforeCurrentDay()
    {
        return $this->daysBeforeCDay;
    }

    /**
     * Default 0 - set from current date
     *
     * @param string|null $val
     */
    public function setComemorariBeforeCurrentDay($val)
    {
        $this->daysBeforeCDay = is_null($val) ? $this->daysBeforeCDay : (int)trim(strip_tags($val));
    }

    /**
     * @return string|null
     */
    public function getComemorariAfterCurrentDay()
    {
        return $this->daysAfterCDay;
    }

    /**
     * Default - set 2 days after current day
     *
     * @param string|null $val
     */
    public function setComemorariAfterCurrentDay($val)
    {
        $this->daysAfterCDay = is_null($val) ? $this->daysAfterCDay : (int)trim(strip_tags($val));
    }

    /**
     * @return bool|intr
     */
    public function showComemorari()
    {
        return $this->showComemorari;
    }

    /**
     * @param $int $val
     */
    public function setShowComemorari($val)
    {
        $this->showComemorari = (int)trim(strip_tags($val));
    }


    // ---------------------------------------------------------------------------------------------

    /**
     * @return array
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param array $orderBy
     */
    public function setOrderBy(array $orderBy)
    {
        $orderBy = (is_array($orderBy) && count($orderBy) ? $orderBy : array());
        if (count($orderBy)) {
            $orderItems = array_keys(self::fetchOrderItems());
            foreach ($orderBy as $k=>$v) {
                if (!in_array($v, $orderItems)) {
                    unset($orderBy[$k]);
                }
            }
        }
        $this->orderBy = $orderBy;
    }

    /**
     * @return array
     */
    public static function fetchOrderItems()
    {
        return array(
            self::ORDER_BY_ID_ASC   => 'Id - ASC',
            self::ORDER_BY_ID_DESC  => 'Id - DESC',
            self::ORDER_BY_NAME_ASC => 'Nume - ASC',
            self::ORDER_BY_NAME_DESC=> 'Nume - DESC',
            self::ORDER_BY_COMEMORARI_ASC => 'Comemorari luna_zi - ASC',
            self::ORDER_BY_COMEMORARI_ASC => 'Comemorari luna_zi - DESC'
        );
    }


}
