<?php

/**
 * Class RolTable
 * @table fcp_personaje_roluri
 */
class RolTable extends CI_Model
{

    /**
     * @var RolTable
     */
    private static $instance;


    /**
     * Singleton
     * RolTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    // ---------------------------------------------------------------------------------------------

    /**
     *  we can later dependency inject and thus unit test this
     * @return RolTable
     */
    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param int $id
     * @return RolItem|null
     * @throws Exception
     */
    public function load($id)
    {
        $id = (int)$id;
        if (!$id) {
            throw new Exception('ID rol este invalid');
        }
        $filters = new RolFilters(array('id'=>$id));
        $data = $this->fetchAll($filters);
        $item = isset($data['items']) && isset($data['items'][0]) ? $data['items'][0] : null;
        return $item;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param RolFilters $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchAll(RolFilters $filters = null, $limit = null, $offset = null)
    {
        // Set where and having conditions.
        $sqlWhere = array();
        $sqlHaving = array();
        if ($filters) {
            if ($filters->getId()) {
                $sqlWhere[] = "rol.id = {$filters->getId()}";
            }
            if ($filters->getExcludedId()) {
                $sqlWhere[] = "rol.id <> {$filters->getExcludedId()}";
            }
            if ($filters->getNumeLike()) {
                $sqlWhere[] = "rol.rol_nume LIKE " . $this->db->escape("%{$filters->getNumeLike()}%");
            }
            if ($filters->getDetaliiLike()) {
                $sqlWhere[] = "rol.rol_detalii LIKE " . $this->db->escape("%{$filters->getDetaliiLike()}%");
            }
            if ($filters->getParentId()) {
                $sqlWhere[] = "rol.parent_id = {$filters->getParentId()}";
            }
            if ($filters->getSearchTerm()) {
                // Clean search term, extract words and set where condition.
                $searchTerm = Strings::sanitizeForDatabaseUsage($filters->getSearchTerm());
                $words = Strings::splitIntoWords($searchTerm);
                if (count($words)) {
                    $subCond = array();
                    foreach ($words as $word) {
                        $word = $this->db->escape("%$word%");
                        $subCond[] = "(rol.id LIKE $word OR rol.rol_nume LIKE $word)";
                    }
                    $sqlWhere[] = implode("\n\tAND ", $subCond);
                }
            }
        }
        $sqlWhere = implode("\n\tAND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sqlHaving = implode("\n\tAND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? "HAVING " . $sqlHaving : '';

        // ------------------------------------

        // Set order by.
        $sqlOrder = array();
        if ($filters && count($filters->getOrderBy())) {
            foreach($filters->getOrderBy() as $ord) {
                switch($ord) {
                    case $filters::ORDER_BY_ID_ASC:
                        $sqlOrder[] = "id ASC";
                        break;
                    case $filters::ORDER_BY_ID_DESC:
                        $sqlOrder[] = "id DESC";
                        break;
                    case $filters::ORDER_BY_NUME_ASC:
                        $sqlOrder[] = "rol_nume ASC";
                        break;
                    case $filters::ORDER_BY_NUME_DESC:
                        $sqlOrder[] = "rol_nume DESC";
                        break;
                    case $filters::ORDER_BY_PARENT_ASC:
                        $sqlOrder[] = "parent_id ASC";
                        break;
                    case $filters::ORDER_BY_PARENT_DESC:
                        $sqlOrder[] = "parent_id DESC";
                        break;
                    case $filters::ORDER_BY_ORDER_ASC:
                        $sqlOrder[] = "order_id ASC";
                        break;
                    case $filters::ORDER_BY_ORDER_DESC:
                        $sqlOrder[] = "order_id DESC";
                        break;
                }
            }
        }
        if (!count($sqlOrder)) {
            $sqlOrder[] = "id ASC";
        }
        $sqlOrder = implode(", ", $sqlOrder);

        // ------------------------------------

        // Set limit.
        $sqlLimit = '';
        if (!is_null($offset) && !is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$offset . ', ' . (int)$limit;
        } elseif (!is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$limit;
        }

        // ------------------------------------

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    rol.*
FROM
    fcp_personaje_roluri AS rol
WHERE
    1
    $sqlWhere
GROUP BY rol.id
$sqlHaving
ORDER BY $sqlOrder
$sqlLimit
EOSQL;

        /** @var CI_DB_result $result */
        $result = $this->db->query($sql);
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        $items = array();
        foreach($result->result('array') as $row){
            $items[] = new RolItem($row);
        }

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------
    
    public function fetchForSelect(RolFilters $filters=null)
    {
        $result = $this->fetchAll($filters);
        $data = array();
        if (isset($result['items']) && count($result['items'])) {
            /** @var RolItem $rol */
            foreach($result['items'] as $rol) {
                $data[$rol->getId()] = $rol->getNume();
            }
        }
        return $data;
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * @param string $searchTerm
     * @param int $limit
     * @return array|RolItem[]
     */
    public function fetchForAutocomplete($searchTerm, $limit=LIMIT_FOR_AUTOCOMPLETE)
    {
        $filters = new RolFilters(array('searchTerm'=>$searchTerm));
        $data = $this->fetchAll($filters, $limit);
        return isset($data['items']) ? $data['items'] : array();
    }
     
    // ---------------------------------------------------------------------------------------------

    /**
     * @param RolFilters|null $filters
     * @param int|null $limit
     * @param int|null $offset
     * @param int $tCount - total items
     *
     * @return array
     */
    public function fetchForApi(RolFilters $filters = null, $limit = null, $offset = null, &$tCount)
    {
        $data = $this->fetchAll($filters, $limit, $offset);

        $return = array();
        if (isset($data['items']) && count($data['items'])) {
            /** @var RolItem $item */
            foreach($data['items'] as $item) {
                $return[] = $item->toArray();
            }

            $tCount = $data['count'];
        }

        return $return;
    }
    
}