<?php

/**
 * Class PersonajTable
 * @table fcp_personaje
 */
class PersonajTable extends CI_Model
{

    /**
     * Used for caching items
     * @var PersonajItem[]
     */
    public static $collection = array();

    /**
     * @var PersonajTable
     */
    private static $instance;


    /**
     * Singleton
     * PersonajTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * We can later dependency inject and thus unit test this
     * @return PersonajTable
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @param int $id
     */
    public static function resetCollectionId($id)
    {
        if (isset(self::$collection[$id])) {
            unset(self::$collection[$id]);
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param mixed $id
     * @return PersonajItem|null
     * @throws Exception
     */
    public function load($id)
    {
        $id = (int)$id;
        if (!$id) {
            throw new Exception("Invalid Personaj id");
        }

        // If data is cached return it
        if (MODEL_CACHING_ENABLED && isset(self::$collection[$id])) {
            return self::$collection[$id];
        }

        // Fetch data
        $filters = new PersonajFilters(array('id' => $id));
        $data = $this->fetchAll($filters);

        // Cache result
        self::$collection[$id] = $data['items'][0] ?? null;

        // Return cached result
        return self::$collection[$id];
    }

    public static function get($id): ?PersonajItem
    {
        if (!$id) {
            return null;
        }

        try {
            return self::getInstance()->load($id);
        } catch (Throwable $e) {
            log_message('error', $e->getMessage());
            return null;
        }
    }

    /**
     * Reset item from static collection and load a new one
     * @param int $id
     * @return PersonajItem|null
     * @throws Exception
     */
    public function reload($id)
    {
        self::resetCollectionId($id);
        return $this->load($id);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param PersonajFilters $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchAll(PersonajFilters $filters = null, $limit = null, $offset = null)
    {
        // Set where and having conditions.
        $sqlWhere = array();
        $sqlHaving = array();
        if ($filters) {
            if ($filters->getId()) {
                $sqlWhere[] = "p.id = {$filters->getId()}";
            }
            if ($filters->getExcludedId()) {
                $sqlWhere[] = "p.id <> {$filters->getExcludedId()}";
            }

            if ($filters->getSearchLetter()) {
                // search by first letter
                $searchLetter = Strings::sanitizeForDatabaseUsage($filters->getSearchLetter());
                $sqlWhere[] = "nume REGEXP '^[{$searchLetter}].*$'";
            }

            if ($filters->getSearchTerm()) {
                // Clean search term, extract words and set where condition.
                $searchTerm = Strings::sanitizeForDatabaseUsage($filters->getSearchTerm());
                $words = Strings::splitIntoWords($searchTerm);
                if (count($words)) {
                    $subCond = array();
                    foreach ($words as $word) {
                        $word = $this->db->escape("%$word%");
                        $cond = "p.prefix LIKE $word OR p.nume LIKE $word OR p.prenume LIKE $word";
                        $cond .= " OR p.alias LIKE $word";
                        $cond .= " OR p.nume_anterior LIKE $word";
                        $cond .= " OR p.prenume_monahism LIKE $word";
                        $cond .= " OR p.nume_pseudonim LIKE $word";
                        $cond .= " OR p.nume_porecla LIKE $word";
                        $subCond[] = "($cond)";
                    }
                    $sqlWhere[] = implode("\n\tAND ", $subCond);
                }
            }

            // pentru comemorari
            if ($filters->showComemorari()) {
                $sqlWhere[] = "data_adormire IS NOT NULL 
                                AND DATE_FORMAT(CURDATE(),'%d') - {$filters->getComemorariBeforeCurrentDay()} <= DATE_FORMAT(data_adormire,'%d') 
                                AND DATE_FORMAT(data_adormire,'%d') <= DATE_FORMAT(CURDATE(),'%d') + {$filters->getComemorariAfterCurrentDay()}
                                AND DATE_FORMAT(CURDATE(),'%m') = DATE_FORMAT(data_adormire,'%m')";
            }

            if ($filters->getRolID()) {
                $sqlWhere[] = "pr.rol_id = {$filters->getRolID()}";
            }

            if ($filters->getStatusId()) {
                $sqlWhere[] = "pr.status_id = {$filters->getStatusId()}";
            }
        }

        $sqlWhere = implode("\n\tAND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sqlHaving = implode("\n\tAND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? "HAVING " . $sqlHaving : '';

        // ------------------------------------

        $sqlOrder = array();
        if ($filters && count($filters->getOrderBy())) {
            foreach ($filters->getOrderBy() as $ord) {
                switch ($ord) {
                    case $filters::ORDER_BY_ID_ASC:
                        $sqlOrder[] = "id ASC";
                        break;
                    case $filters::ORDER_BY_ID_DESC:
                        $sqlOrder[] = "id DESC";
                        break;
                    case $filters::ORDER_BY_NAME_ASC:
                        $sqlOrder[] = "nume_prenume ASC";
                        break;
                    case $filters::ORDER_BY_NAME_DESC:
                        $sqlOrder[] = "nume_prenume DESC";
                        break;
                    case $filters::ORDER_BY_COMEMORARI_ASC:
                        $sqlOrder[] = "luna_zi ASC";
                        break;
                    case $filters::ORDER_BY_COMEMORARI_DESC:
                        $sqlOrder[] = "luna_zi DESC";
                        break;
                }
            }
        }
        if (!count($sqlOrder)) {
            $sqlOrder[] = "id DESC";
        }
        $sqlOrder = implode(", ", $sqlOrder);

        // ------------------------------------

        // Set limit.
        $sqlLimit = '';
        if (!is_null($offset) && !is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$offset . ', ' . (int)$limit;
        } elseif (!is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$limit;
        }

        // ------------------------------------

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    p.*
    ,DATE_FORMAT(CURDATE(),'%Y') - DATE_FORMAT(data_adormire,'%Y') as difAni
    ,DATE_FORMAT(data_adormire,'%m%d') as luna_zi
    ,CONCAT_WS(', '
        , p.nume
        , p.prenume
    ) AS nume_prenume
    ,GROUP_CONCAT(pr.rol_id SEPARATOR '<;>') AS list_roluri
FROM
    fcp_personaje AS p
    LEFT JOIN fcp_personaje2roluri as pr ON pr.personaj_id = p.id
WHERE
    1
    $sqlWhere
GROUP BY p.id
$sqlHaving
ORDER BY $sqlOrder
$sqlLimit
EOSQL;

        /** @var CI_DB_result $result */
        $result = $this->db->query($sql);
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        $items = array();
        foreach ($result->result('array') as $row) {
            $items[] = new PersonajItem($row);
        }

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param PersonajItem $item
     * @return bool|int
     */
    public function save(PersonajItem &$item)
    {
        // Set data.
        $eData = [
            'mCatID_backup' => $item->getMCatIDBackup() ?: null,
            'prefix' => $item->getPrefix() ?: null,
            'nume' => $item->getNume() ?: null,
            'prenume' => $item->getPrenume() ?: null,
            'alias' => $item->getAlias() ?: null,
            'sex' => $item->getSex() ?? null,
            'data_nastere' => $item->getDataNastere() ?: null,
            'data_adormire' => $item->getDataAdormire() ?: null,
            'avatar' => $item->getAvatar() ?: null,
            'biografie_poza' => $item->getBiografiePoza() ?: null,
            'biografie_articol_id' => $item->getBiografieArticolId() ?: null,
            'observatii' => $item->getObservatii() ?: null,
            'observatii_user_id' => $item->getObservatiiUserId() ?: null,
            'observatii_interne' => $item->getObservatiiInterne() ?: null,
            'is_favorit' => !is_null($item->getIsFavorit()) ? $item->getIsFavorit() : null,
            'status_id' => !is_null($item->getStatusId()) ? $item->getStatusId() : null,

            'nume_anterior' => $item->getNumeAnterior() ?: null,
            'prenume_monahism' => $item->getPrenumeMonahism() ?: null,
            'nume_pseudonim' => $item->getNumePseudonim() ?: null,
            'nume_porecla' => $item->getNumePorecla() ?: null,
            'durata_detentie' => $item->getDurataDetentie() ?: null,
            'durata_prizonierat' => $item->getDurataPrizonierat() ?: null,
            'durata_deportare' => $item->getDurataDeportare() ?: null,
            'durata_domiciliu_oblig' => $item->getDurataDomiciliuOblig() ?: null,
            // 'ani_condamnare' => $item->getAniCondamnare() ? $item->getAniCondamnare() : null,
            // 'ani_inchisoare' => $item->getAniInchisoare() ? $item->getAniInchisoare() : null,
            'nastere_loc_id' => $item->getNastereLocId() ?? null,
            'deces_locul_mortii' => $item->getDecesLoculMortii() ?: null,
            'deces_loc_inmormantare_id' => $item->getDecesLocInmormantareId() ?: null,
            'deces_nume_cimitir' => $item->getDecesNumeCimitir() ?: null,
            'deces_mormant_coord_lat' => $item->getDecesMormantCoordLat() ?: null,
            'deces_mormant_coord_long' => $item->getDecesMormantCoordLong() ?: null,
            'confesiune_id' => $item->getConfesiuneId() ?? null,
            'nationalitate_id' => $item->getNationalitateId() ?? null,
            'prenume_tata' => $item->getPrenumeTata() ?: null,
            'prenume_mama' => $item->getPrenumeMama() ?: null,
            'ocupatii_socioprofesionale' => $item->getOcupatiiSocioprofesionale() ?? null,
            'orig_sociala_id' => $item->getOrigSocialaId() ?? null,
            'count_copii_b' => $item->getCountCopiiB() ?: null,
            'count_copii_f' => $item->getCountCopiiF() ?: null,
            'seo_metadescription' => $item->getSeoMetadescription() ?: null,
            'seo_metakeywords'   => $item->getSeoMetakeywords() ?: null,
            'is_detinut_politic' => $item->getIsDetinutPolitic() ? 1 : 0,
            'is_deleted' => $item->isDeleted() ? 1 : 0,
        ];

        // Insert/Update.
        if (!$item->getId()) {
            $res = $this->db->insert('fcp_personaje', $eData);
            $id  = $this->db->insert_id();
            $item->setId($id);
        } else {
            $res = $this->db->update('fcp_personaje', $eData, 'id = ' . $item->getId());
        }

        // Remove old item from cache
        self::resetCollectionId($item->getId());

        return $res;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param PersonajItem $item
     * @return mixed
     */
    public function delete(PersonajItem $item)
    {
        // Remove old item from cache
        self::resetCollectionId($item->getId());

        return $this->db->delete('fcp_personaje', 'id = ' . $item->getId());
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param PersonajFilters|null $filters
     * @param int|null $limit
     * @return array
     */
    public function fetchForAutocomplete(PersonajFilters $filters = null, $limit = LIMIT_FOR_AUTOCOMPLETE)
    {
        $data = $this->fetchAll($filters, $limit);

        $result = array();
        if (isset($data['items']) && count($data['items'])) {
            /** @var PersonajItem $item */
            foreach ($data['items'] as $item) {
                $result[] = array(
                    'value'     => $item->getFullName(),
                    'id'        => $item->getId(),
                    'data'      => ['category' => 'Nume complet']
                );
            }
        }

        return $result;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param PersonajFilters|null $filters
     * @param int|null $limit
     * @param int|null $offset
     * @param int $tCount - total items
     *
     * @return array
     */
    public function fetchForApi(PersonajFilters $filters = null, $limit = null, $offset = null, &$tCount)
    {
        $data = $this->fetchAll($filters, $limit, $offset);

        $return = [];
        if (isset($data['items']) && count($data['items'])) {
            /** @var PersonajItem $item */
            foreach ($data['items'] as $item) {
                $result = $item->toArray();

                $locNastere = $item->getLocNastere();

                $result['locNastere'] = $locNastere ? $locNastere->toArray() : false;
            }

            $return[] = $result;

            $tCount = $data['count'];
        }

        return $return;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param PersonajFilters|null $filters
     * @return array
     */
    public function fetchForSelect(PersonajFilters $filters = null)
    {
        $result = $this->fetchAll($filters);
        $data = [];
        if (isset($result['items']) && count($result['items'])) {
            /** @var PersonajItem $dim */
            foreach ($result['items'] as $dim) {
                $data[$dim->getId()] = $dim->getFullName();
            }
        }
        return $data;
    }
}
