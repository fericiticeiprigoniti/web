<?php

/**
 * Class PersonajRolTable
 * @table fcp_personaje2roluri
 */
class PersonajRolTable extends CI_Model
{

    /**
     * @var PersonajRolTable
     */
    private static $instance;


    /**
     * Singleton
     * PersonajRolTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * We can later dependency inject and thus unit test this
     * @return PersonajRolTable
     */
    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param mixed $id
     * @return PersonajRolItem|null
     * @throws Exception
     */
    public function load($id)
    {
        if (!$id) {
            throw new Exception("Invalid PersonajRol id");
        }
        $filters = new PersonajRolFilters(array('id'=>$id));
        $data = $this->fetchAll($filters);
        $item = isset($data['items']) && isset($data['items'][0]) ? $data['items'][0] : null;
        return $item;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param PersonajRolFilters $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchAll(PersonajRolFilters $filters = null, $limit = null, $offset = null)
    {
        // Set where and having conditions.
        $sqlWhere = array();
        $sqlHaving = array();
        if ($filters) {
            if ($filters->getId()) {
                $sqlWhere[] = "id = {$filters->getId()}";
            }
            if ($filters->getExcludedId()) {
                $sqlWhere[] = "id <> {$filters->getExcludedId()}";
            }
            if ($filters->getPersonajId()) {
                $sqlWhere[] = "personaj_id = {$filters->getPersonajId()}";
            }
            if ($filters->getRolId()) {
                $sqlWhere[] = "rol_id = {$filters->getRolId()}";
            }
        }

        $sqlWhere = implode("\n\tAND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sqlHaving = implode("\n\tAND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? "HAVING " . $sqlHaving : '';

        // ------------------------------------

        $sqlOrder = array();
        if ($filters && count($filters->getOrderBy())) {
            foreach($filters->getOrderBy() as $ord) {
                switch($ord) {
                    case $filters::ORDER_BY_ID_ASC:
                        $sqlOrder[] = "id ASC";
                        break;
                    case $filters::ORDER_BY_ID_DESC:
                        $sqlOrder[] = "id DESC";
                        break;
        }
            }
        }
        if (!count($sqlOrder)) {
            $sqlOrder[] = "id DESC";
        }
        $sqlOrder = implode(", ", $sqlOrder);

        // ------------------------------------

        // Set limit.
        $sqlLimit = '';
        if (!is_null($offset) && !is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$offset . ', ' . (int)$limit;
        } elseif (!is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$limit;
        }

        // ------------------------------------

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    *
FROM
    fcp_personaje2roluri
WHERE
    1
    $sqlWhere
GROUP BY id
$sqlHaving
ORDER BY $sqlOrder
$sqlLimit
EOSQL;

        /** @var CI_DB_result $result */
        $result = $this->db->query($sql);
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        $items = array();
        foreach($result->result('array') as $row){
            $items[] = new PersonajRolItem($row);
        }

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param PersonajRolItem $item
     * @return bool|int
     */
    public function save(PersonajRolItem $item)
    {
        // Set data.
        $eData = array(
            'personaj_id' => $item->getPersonajId() ? $item->getPersonajId() : null,
            'rol_id' => $item->getRolId() ? $item->getRolId() : null,
        );

        // Insert/Update.
        if (!$item->getId()) {
            $res = $this->db->insert('fcp_personaje2roluri', $eData);
        } else {
            $res = $this->db->update('fcp_personaje2roluri', $eData, 'id = ' . $item->getId());
        }

        return $res;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param PersonajRolItem $item
     * @return mixed
     */
    public function delete(PersonajRolItem $item)
    {
        return $this->db->delete('fcp_personaje2roluri', 'id = ' . $item->getId());
    }

    
    // ---------------------------------------------------------------------------------------------

    /**
     * @param PersonajFilters|null $filters
     * @param int|null $limit
     * @param int|null $offset
     * @param int $tCount - total items
     *
     * @return array
     */
    public function fetchForApi(PersonajRolFilters $filters = null, $limit = null, $offset = null, &$tCount)
    {
        $data = $this->fetchAll($filters, $limit, $offset);

        $return = array();
        if (isset($data['items']) && count($data['items'])) {
            /** @var PersonajRolItem $item */
            foreach($data['items'] as $item) {
                $return[] = $item->toArray();
            }

            $tCount = $data['count'];
        }

        return $return;
    }

    // ---------------------------------------------------------------------------------------------

}
