<?php

class PersonajItem extends CI_Model
{

    use ArrayOrJson;

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $mCatIDBackup;

    /**
     * @var string
     */
    private $prefix;

    /**
     * @var string
     */
    private $nume;

    /**
     * @var string
     */
    private $prenume;

    /**
     * @var string
     */
    private $numePrenume;

    /**
     * @var string
     */
    private $alias;

    /**
     * @var int
     */
    private $sex;

    /**
     * @var string
     */
    private $dataNastere;

    /**
     * @var string
     */
    private $dataAdormire;

    /**
     * @var string
     */
    private $avatar;

    /**
     * @var string
     */
    private $biografiePoza;

    /**
     * @var string
     */
    private $biografieArticolId;

    /**
     * @var string
     */
    private $observatii;

    /**
     * @var int
     */
    private $observatiiUserId;

    /**
     * @var string
     */
    private $observatiiInterne;

    /**
     * @var int
     */
    private $isFavorit;

    /**
     * @var int
     */
    private $statusId;

    /**
     * @var string
     */
    private $numeAnterior;

    /**
     * @var string
     */
    private $prenumeMonahism;

    /**
     * @var string
     */
    private $numePseudonim;

    /**
     * @var string
     */
    private $numePorecla;

    /**
     * @var int
     */
    private $durataDetentie;

    /**
     * @var int
     */
    private $durataPrizonierat;

    /**
     * @var int
     */
    private $durataDeportare;

    /**
     * @var int
     */
    private $durataDomiciliuOblig;

    /**
     * @var int
     */
    private $aniCondamnare;

    /**
     * @var int
     */
    private $aniInchisoare;

    /**
     * @var int
     */
    private $nastereLocId;

    /**
     * @var string
     */
    private $decesLoculMortii;

    /**
     * @var int
     */
    private $decesLocInmormantareId;

    /**
     * @var string
     */
    private $decesNumeCimitir;

    /**
     * @var string
     */
    private $decesMormantCoordLat;

    /**
     * @var string
     */
    private $decesMormantCoordLong;

    /**
     * @var int
     */
    private $confesiuneId;

    /**
     * @var int
     */
    private $nationalitateId;

    /**
     * @var string
     */
    private $prenumeTata;

    /**
     * @var string
     */
    private $prenumeMama;

    /**
     * @var int
     */
    private $origSocialaId;

    private $ocupatiiSocioprofesionale;

    /**
     * @var int
     */
    private $countCopiiB;

    /**
     * @var int
     */
    private $countCopiiF;

    /**
     * @var bool
     */
    private $isDetinutPolitic;

    /**
     * @var bool
     */
    private $isDeleted;

    /**
     * @var string
     */
    private $seoMetadescription;

    /**
     * @var string
     */
    private $seoMetakeywords;

    // ---------------------------------------------------------------------------------------------

    /**
     * @var int
     */
    private $arrRoluri;

    /**
     * @var ArticolItem $objBiografie
     */
    private $objBiografie;

    // ---------------------------------------------------------------------------------------------

    /**
     * localitate nastere
     *
     * @var LocalitateItem $objLocNastere
     */
    private $objLocNastere;

    /**
     * localitate inmormantare
     *
     * @var LocalitateItem $objLocInmormantare
     */
    private $objLocInmormantare;

    /**
     * totalul de poezii active
     *
     * @var int
     */
    private $countPoezii;

    /**
     * Ocupatii la arestare
     *
     * @var array
     */
    private $ocupatiiArestare;

    public function __construct(array $dbRow = [])
    {
        if (!count($dbRow)) {
            return;
        }

        $this->id = $dbRow['id'] ? (int)$dbRow['id'] : null;
        $this->mCatIDBackup = $dbRow['mCatID_backup'] ? (int)$dbRow['mCatID_backup'] : null;
        $this->prefix = $dbRow['prefix'] ? (string)$dbRow['prefix'] : null;
        $this->nume = $dbRow['nume'] ? (string)$dbRow['nume'] : null;
        $this->prenume = $dbRow['prenume'] ? (string)$dbRow['prenume'] : null;
        $this->numePrenume = $dbRow['nume_prenume'] ? (string)$dbRow['nume_prenume'] : null;
        $this->alias = $dbRow['alias'] ? (string)$dbRow['alias'] : null;
        $this->sex = !is_null($dbRow['sex']) ? (int)$dbRow['sex'] : null;
        $this->dataNastere = $dbRow['data_nastere'] ? (string)$dbRow['data_nastere'] : null;
        $this->dataAdormire = $dbRow['data_adormire'] ? (string)$dbRow['data_adormire'] : null;
        $this->avatar = $dbRow['avatar'] ? (string)$dbRow['avatar'] : null;
        $this->biografiePoza = $dbRow['biografie_poza'] ? (string)$dbRow['biografie_poza'] : null;
        $this->biografieArticolId = $dbRow['biografie_articol_id'] ? (string)$dbRow['biografie_articol_id'] : null;
        $this->observatii = $dbRow['observatii'] ? (string)$dbRow['observatii'] : null;
        $this->observatiiUserId = $dbRow['observatii_user_id'] ? (int)$dbRow['observatii_user_id'] : null;
        $this->observatiiInterne = $dbRow['observatii_interne'] ? (string)$dbRow['observatii_interne'] : null;

        $this->numeAnterior         = $dbRow['nume_anterior'] ? (string)$dbRow['nume_anterior'] : null;
        $this->prenumeMonahism = $dbRow['prenume_monahism'] ? (string)$dbRow['prenume_monahism'] : null;
        $this->numePseudonim = $dbRow['nume_pseudonim'] ? (string)$dbRow['nume_pseudonim'] : null;
        $this->numePorecla = $dbRow['nume_porecla'] ? (string)$dbRow['nume_porecla'] : null;
        $this->durataDetentie = $dbRow['durata_detentie'] ? (int)$dbRow['durata_detentie'] : null;
        $this->durataPrizonierat = $dbRow['durata_prizonierat'] ? (int)$dbRow['durata_prizonierat'] : null;
        $this->durataDeportare = $dbRow['durata_deportare'] ? (int)$dbRow['durata_deportare'] : null;
        $this->durataDomiciliuOblig = $dbRow['durata_domiciliu_oblig'] ? (int)$dbRow['durata_domiciliu_oblig'] : null;
        $this->aniCondamnare = $dbRow['ani_condamnare'] ? (int)$dbRow['ani_condamnare'] : null;
        $this->aniInchisoare = $dbRow['ani_inchisoare'] ? (int)$dbRow['ani_inchisoare'] : null;
        $this->nastereLocId = $dbRow['nastere_loc_id'] ? (int)$dbRow['nastere_loc_id'] : null;
        $this->decesLoculMortii = $dbRow['deces_locul_mortii'] ? (string)$dbRow['deces_locul_mortii'] : null;
        $this->decesLocInmormantareId = $dbRow['deces_loc_inmormantare_id'] ? (int)$dbRow['deces_loc_inmormantare_id'] : null;
        $this->decesNumeCimitir = $dbRow['deces_nume_cimitir'] ? (string)$dbRow['deces_nume_cimitir'] : null;
        $this->decesMormantCoordLat = $dbRow['deces_mormant_coord_lat'] ? (string)$dbRow['deces_mormant_coord_lat'] : null;
        $this->decesMormantCoordLong = $dbRow['deces_mormant_coord_long'] ? (string)$dbRow['deces_mormant_coord_long'] : null;
        $this->confesiuneId = $dbRow['confesiune_id'] ? (int)$dbRow['confesiune_id'] : null;
        $this->nationalitateId = $dbRow['nationalitate_id'] ? (int)$dbRow['nationalitate_id'] : null;
        $this->prenumeTata = $dbRow['prenume_tata'] ? (string)$dbRow['prenume_tata'] : null;
        $this->prenumeMama = $dbRow['prenume_mama'] ? (string)$dbRow['prenume_mama'] : null;
        $this->origSocialaId = $dbRow['orig_sociala_id'] ? (int)$dbRow['orig_sociala_id'] : null;
        $this->countCopiiB = $dbRow['count_copii_b'] ? (int)$dbRow['count_copii_b'] : null;
        $this->countCopiiF = $dbRow['count_copii_f'] ? (int)$dbRow['count_copii_f'] : null;
        $this->ocupatiiSocioprofesionale = $dbRow['ocupatii_socioprofesionale'] ? $dbRow['ocupatii_socioprofesionale'] : null;

        $this->seoMetadescription = $dbRow['seo_metadescription'] ? $dbRow['seo_metadescription'] : null;
        $this->seoMetakeywords    = $dbRow['seo_metakeywords'] ? $dbRow['seo_metakeywords'] : null;

        $this->isDetinutPolitic     = !is_null($dbRow['is_detinut_politic']) ? (int)$dbRow['is_detinut_politic'] : null;
        $this->isFavorit            = !is_null($dbRow['is_favorit']) ? (int)$dbRow['is_favorit'] : null;
        $this->statusId             = !is_null($dbRow['status_id']) ? (int)$dbRow['status_id'] : 0;
        $this->isDeleted            = !is_null($dbRow['is_deleted']) ? (int)$dbRow['is_deleted'] : null;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return int
     */
    public function getMCatIDBackup()
    {
        return $this->mCatIDBackup;
    }

    /**
     * @param int $mCatIDBackup
     */
    public function setMCatIDBackup($mCatIDBackup)
    {
        $this->mCatIDBackup = $mCatIDBackup ? (int)$mCatIDBackup : null;
    }

    /**
     * @return string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * @param string $prefix
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix ? (string)$prefix : null;
    }

    /**
     * @return string
     */
    public function getNume()
    {
        return $this->nume;
    }

    /**
     * @param string $nume
     */
    public function setNume($nume)
    {
        $this->nume = $nume ? (string)$nume : null;
    }

    /**
     * @return string
     */
    public function getPrenume()
    {
        return $this->prenume;
    }

    /**
     * @param string $prenume
     */
    public function setPrenume($prenume)
    {
        $this->prenume = $prenume ? (string)$prenume : null;
    }

    /**
     * @return string
     */
    public function getNumePrenume()
    {
        return $this->numePrenume;
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     */
    public function setAlias($alias)
    {
        $this->alias = $alias ? (string)$alias : null;
    }

    /**
     * @return int
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * @param int $sex
     */
    public function setSex($sex)
    {
        $this->sex = !is_null($sex) ? (int)$sex : null;
    }

    /**
     * @return string
     */
    public function getDataNastere()
    {
        return $this->dataNastere;
    }

    /**
     * @param string $dataNastere
     */
    public function setDataNastere($dataNastere)
    {
        $this->dataNastere = $dataNastere ? (string)$dataNastere : null;
    }

    /**
     * @return string
     */
    public function getDataAdormire()
    {
        return $this->dataAdormire;
    }

    /**
     * @param string $dataAdormire
     */
    public function setDataAdormire($dataAdormire)
    {
        $this->dataAdormire = $dataAdormire ? (string)$dataAdormire : null;
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar ? (string)$avatar : null;
    }

    /**
     * @return string
     */
    public function getBiografiePoza()
    {
        return $this->biografiePoza;
    }

    /**
     * @param string $biografiePoza
     */
    public function setBiografiePoza($biografiePoza)
    {
        $this->biografiePoza = $biografiePoza ? (string)$biografiePoza : null;
    }

    /**
     * @return string
     */
    public function getBiografieArticolId()
    {
        return $this->biografieArticolId;
    }

    /**
     * @param string $biografieArticolId
     */
    public function setBiografieArticolId($biografieArticolId)
    {
        $this->biografieArticolId = $biografieArticolId ? (string)$biografieArticolId : null;
    }

    /**
     * @return string
     */
    public function getObservatii()
    {
        return $this->observatii;
    }

    /**
     * @param string $observatii
     */
    public function setObservatii($observatii)
    {
        $this->observatii = $observatii ? (string)$observatii : null;
    }

    /**
     * @return int
     */
    public function getObservatiiUserId()
    {
        return $this->observatiiUserId;
    }

    /**
     * @param int $observatiiUserId
     */
    public function setObservatiiUserId($observatiiUserId)
    {
        $this->observatiiUserId = $observatiiUserId ? (int)$observatiiUserId : null;
    }

    /**
     * @return string
     */
    public function getObservatiiInterne()
    {
        return $this->observatiiInterne;
    }

    /**
     * @param string $observatiiInterne
     */
    public function setObservatiiInterne($observatiiInterne)
    {
        $this->observatiiInterne = $observatiiInterne ? (string)$observatiiInterne : null;
    }


    /**
     * @return int
     */
    public function getIsDetinutPolitic()
    {
        return $this->isDetinutPolitic;
    }

    /**
     * @param int $val
     */
    public function setIsDetinutPolitic($val)
    {
        $this->isDetinutPolitic = $val ? (int)$val : 0;
    }

    /**
     * @return int
     */
    public function getIsFavorit()
    {
        return $this->isFavorit;
    }

    /**
     * @param int $isFavorit
     */
    public function setIsFavorit($isFavorit)
    {
        $this->isFavorit = $isFavorit ? (int)$isFavorit : 0;
    }

    /**
     * @return int
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * @param int $statusId
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId ? (int)$statusId : 0;
    }

    /**
     * @return string
     */
    public function getNumeAnterior()
    {
        return $this->numeAnterior;
    }

    /**
     * @param string $numeAnterior
     */
    public function setNumeAnterior($numeAnterior)
    {
        $this->numeAnterior = $numeAnterior ? (string)$numeAnterior : null;
    }

    /**
     * @return string
     */
    public function getPrenumeMonahism()
    {
        return $this->prenumeMonahism;
    }

    /**
     * @param string $prenumeMonahism
     */
    public function setPrenumeMonahism($prenumeMonahism)
    {
        $this->prenumeMonahism = $prenumeMonahism ? (string)$prenumeMonahism : null;
    }

    /**
     * @return string
     */
    public function getNumePseudonim()
    {
        return $this->numePseudonim;
    }

    /**
     * @param string $numePseudonim
     */
    public function setNumePseudonim($numePseudonim)
    {
        $this->numePseudonim = $numePseudonim ? (string)$numePseudonim : null;
    }

    /**
     * @return string
     */
    public function getNumePorecla()
    {
        return $this->numePorecla;
    }

    /**
     * @param string $numePorecla
     */
    public function setNumePorecla($numePorecla)
    {
        $this->numePorecla = $numePorecla ? (string)$numePorecla : null;
    }

    /**
     * @return int
     */
    public function getDurataDetentie()
    {
        return $this->durataDetentie;
    }

    /**
     * @param int $durataDetentie
     */
    public function setDurataDetentie($durataDetentie)
    {
        $this->durataDetentie = $durataDetentie ? (int)$durataDetentie : null;
    }

    /**
     * @return int
     */
    public function getDurataPrizonierat()
    {
        return $this->durataPrizonierat;
    }

    /**
     * @param int $durataPrizonierat
     */
    public function setDurataPrizonierat($durataPrizonierat)
    {
        $this->durataPrizonierat = $durataPrizonierat ? (int)$durataPrizonierat : null;
    }

    /**
     * @return int
     */
    public function getDurataDeportare()
    {
        return $this->durataDeportare;
    }

    /**
     * @param int $durataDeportare
     */
    public function setDurataDeportare($durataDeportare)
    {
        $this->durataDeportare = $durataDeportare ? (int)$durataDeportare : null;
    }

    /**
     * @return int
     */
    public function getDurataDomiciliuOblig()
    {
        return $this->durataDomiciliuOblig;
    }

    /**
     * @param int $durataDomiciliuOblig
     */
    public function setDurataDomiciliuOblig($durataDomiciliuOblig)
    {
        $this->durataDomiciliuOblig = $durataDomiciliuOblig ? (int)$durataDomiciliuOblig : null;
    }

    /**
     * @return int
     */
    public function getAniCondamnare()
    {
        return $this->aniCondamnare;
    }

    /**
     * @param int $aniCondamnare
     */
    public function setAniCondamnare($aniCondamnare)
    {
        $this->aniCondamnare = $aniCondamnare ? (int)$aniCondamnare : null;
    }

    /**
     * @return int
     */
    public function getAniInchisoare()
    {
        return $this->aniInchisoare;
    }

    /**
     * @param int $aniInchisoare
     */
    public function setAniInchisoare($aniInchisoare)
    {
        $this->aniInchisoare = $aniInchisoare ? (int)$aniInchisoare : null;
    }

    /**
     * @return int
     */
    public function getNastereLocId()
    {
        return $this->nastereLocId;
    }

    /**
     * @param int $nastereLocId
     */
    public function setNastereLocId($nastereLocId)
    {
        $this->nastereLocId = $nastereLocId ? (int)$nastereLocId : null;
    }

    /**
     * @return string
     */
    public function getDecesLoculMortii()
    {
        return $this->decesLoculMortii;
    }

    /**
     * @param string $decesLoculMortii
     */
    public function setDecesLoculMortii($decesLoculMortii)
    {
        $this->decesLoculMortii = $decesLoculMortii ? (string)$decesLoculMortii : null;
    }

    /**
     * @return int
     */
    public function getDecesLocInmormantareId()
    {
        return $this->decesLocInmormantareId;
    }

    /**
     * @param int $decesLocInmormantareId
     */
    public function setDecesLocInmormantareId($decesLocInmormantareId)
    {
        $this->decesLocInmormantareId = $decesLocInmormantareId ? (int)$decesLocInmormantareId : null;
    }

    /**
     * @return string
     */
    public function getDecesNumeCimitir()
    {
        return $this->decesNumeCimitir;
    }

    /**
     * @param string $decesNumeCimitir
     */
    public function setDecesNumeCimitir($decesNumeCimitir)
    {
        $this->decesNumeCimitir = $decesNumeCimitir ? (string)$decesNumeCimitir : null;
    }

    /**
     * @return string
     */
    public function getDecesMormantCoordLat()
    {
        return $this->decesMormantCoordLat;
    }

    /**
     * @param string $decesMormantCoordLat
     */
    public function setDecesMormantCoordLat($decesMormantCoordLat)
    {
        $this->decesMormantCoordLat = $decesMormantCoordLat ? (string)$decesMormantCoordLat : null;
    }

    /**
     * @return string
     */
    public function getDecesMormantCoordLong()
    {
        return $this->decesMormantCoordLong;
    }

    /**
     * @param string $decesMormantCoordLong
     */
    public function setDecesMormantCoordLong($decesMormantCoordLong)
    {
        $this->decesMormantCoordLong = $decesMormantCoordLong ? (string)$decesMormantCoordLong : null;
    }

    /**
     * @return int
     */
    public function getConfesiuneId()
    {
        return $this->confesiuneId;
    }

    /**
     * @param int $confesiuneId
     */
    public function setConfesiuneId($confesiuneId)
    {
        $this->confesiuneId = !is_null($confesiuneId) ? (int)$confesiuneId : null;
    }

    /**
     * @return int
     */
    public function getNationalitateId()
    {
        return $this->nationalitateId;
    }

    /**
     * @param int $nationalitateId
     */
    public function setNationalitateId($nationalitateId)
    {
        $this->nationalitateId = $nationalitateId ? (int)$nationalitateId : null;
    }

    /**
     * @return string
     */
    public function getPrenumeTata()
    {
        return $this->prenumeTata;
    }

    /**
     * @param string $prenumeTata
     */
    public function setPrenumeTata($prenumeTata)
    {
        $this->prenumeTata = $prenumeTata ? (string)$prenumeTata : null;
    }

    /**
     * @return string
     */
    public function getPrenumeMama()
    {
        return $this->prenumeMama;
    }

    /**
     * @param string $prenumeMama
     */
    public function setPrenumeMama($prenumeMama)
    {
        $this->prenumeMama = $prenumeMama ? (string)$prenumeMama : null;
    }

    /**
     * @return int
     */
    public function getOrigSocialaId()
    {
        return $this->origSocialaId;
    }

    /**
     * @param int $origSocialaId
     */
    public function setOrigSocialaId($origSocialaId)
    {
        $this->origSocialaId = $origSocialaId ? (int)$origSocialaId : null;
    }

    /**
     * @return int
     */
    public function getCountCopiiB()
    {
        return $this->countCopiiB;
    }

    /**
     * @param int $countCopiiB
     */
    public function setCountCopiiB($countCopiiB)
    {
        $this->countCopiiB = $countCopiiB ? (int)$countCopiiB : null;
    }

    /**
     * @return int
     */
    public function getCountCopiiF()
    {
        return $this->countCopiiF;
    }

    /**
     * @param int $countCopiiF
     */
    public function setCountCopiiF($countCopiiF)
    {
        $this->countCopiiF = $countCopiiF ? (int)$countCopiiF : null;
    }

    /**
     * @return string
     */
    public function getOcupatiiSocioprofesionale()
    {
        return $this->ocupatiiSocioprofesionale;
    }

    /**
     * @param string $txt
     */
    public function setOcupatiiSocioprofesionale($txt)
    {
        $this->ocupatiiSocioprofesionale = $txt ? (string)$txt : null;
    }


    /**
     * @return string
     */
    public function getSeoMetadescription()
    {
        return $this->seoMetadescription;
    }

    /**
     * @param string $description
     */
    public function setSeoMetadescription($description)
    {
        $this->seoMetadescription = $description ? (string)$description : null;
    }

    /**
     * @return string
     */
    public function getSeoMetakeywords()
    {
        return $this->seoMetakeywords;
    }

    /**
     * @param string $keywords
     */
    public function setSeoMetakeywords($keywords)
    {
        $this->seoMetakeywords = $keywords ? (string)$keywords : null;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param bool $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = (bool)$isDeleted;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param bool $withPrefix
     * @return string
     */
    public function getNumeComplet($withPrefix = true)
    {
        return ($withPrefix ? $this->prefix : '') . $this->nume . ' ' . $this->prenume;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        $fullName = trim($this->nume) . ', ' . trim($this->prenume);
        $fullName .= !empty(trim($this->prefix)) ? ', ' . trim($this->prefix) : '';

        // Extra
        if ($this->alias || $this->numeAnterior || $this->prenumeMonahism || $this->numePseudonim || $this->numePorecla) {
            $extra = [];
            if ($this->alias) {
                $extra[] = 'alias ' . $this->alias;
            }
            if ($this->numeAnterior) {
                $extra[] = 'anterior ' . $this->numeAnterior;
            }
            if ($this->prenumeMonahism) {
                $extra[] = 'in monahism ' . $this->prenumeMonahism;
            }
            if ($this->numePseudonim) {
                $extra[] = 'pseudonim ' . $this->numePseudonim;
            }
            if ($this->numePorecla) {
                $extra[] = 'porecla ' . $this->numePorecla;
            }
            $extraList = implode(', ', $extra);
            $fullName .= " ($extraList)";
        }

        return $fullName;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param bool $return_key - false -> return list with object/ TRUE - return list with keys
     * @return array|int|null
     * @throws Exception
     */
    public function getRoluri($return_key = false)
    {
        if ($this->arrRoluri) {

            return $this->arrRoluri;
        } elseif ($this->id) {

            $filters = new PersonajRolFilters();
            $filters->setPersonajId($this->getId());

            $results = [];
            if ($roluri = PersonajRolTable::getInstance()->fetchAll($filters)) {
                foreach ($roluri['items'] as $objPers2rol) {
                    if ($return_key) {
                        $objRol = RolTable::getInstance()->load($objPers2rol->getRolId());

                        $results[] = $objRol->getId();
                    } else {
                        // array with object
                        //$results[] = RolTable::getInstance()->load($objPers2rol->getRolId());
                        $results[] = $objPers2rol;
                    }
                }
            }

            $this->arrRoluri = $results;

            return $this->arrRoluri;
        } else {
            return null;
        }
    }


    /**
     * @return ArticolItem|null
     * @throws Exception
     */
    public function getBiografie()
    {
        $this->objBiografie = $this->objBiografie ?: ArticolTable::get($this->biografieArticolId);
        return $this->objBiografie;
    }

    public function getLocNastere(): ?LocalitateItem
    {
        $this->objLocNastere = $this->objLocNastere ?: LocalitateTable::get($this->nastereLocId);
        return $this->objLocNastere;
    }

    public function getLocInmormantare(): ?LocalitateItem
    {
        $this->objLocInmormantare = $this->objLocInmormantare ?: LocalitateTable::get($this->decesLocInmormantareId);
        return $this->objLocInmormantare;
    }

    /**
     * Total poezii (doar cele care nu sunt versiuni)
     *
     * @return int
     */
    public function countPoezii()
    {
        if ($this->countPoezii) {
            return $this->countPoezii;
        }

        $sql = "SELECT count(*) as total
                FROM fcp_poezii 
                WHERE personaj_id = ? AND is_published = 1 AND is_deleted = 0 AND parent_id = 0";

        if ($results = $this->db->query($sql, [$this->getId()])->row_array()) {
            $this->countPoezii = $results['total'];
        }

        return $this->countPoezii;
    }

    public function getCountPoezii()
    {
        return $this->countPoezii;
    }

    /**
     * Returneaza lista de ocupatii la arestare
     *
     * @return array
     */
    public function fetchOcupatiiArestare()
    {
        if ($this->ocupatiiArestare) {
            return $this->ocupatiiArestare;
        }

        $sql = "SELECT t2.* 
                FROM fcp_personaje_episoade_represive as t1 
                LEFT JOIN fcp_personaje_ocupatii as t2 ON t2.id = t1.arest_ocupatie_id
                WHERE t1.personaj_id = ?
                ORDER BY cond_pedeapsa_data_start";

        $this->ocupatiiArestare = $this->db->query($sql, [$this->getId()])->result_array();

        return $this->ocupatiiArestare;
    }
}
