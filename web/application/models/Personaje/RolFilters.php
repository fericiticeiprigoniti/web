<?php

class RolFilters
{

    const ORDER_BY_ID_ASC = 'id_asc';
    const ORDER_BY_ID_DESC = 'id_desc';
    const ORDER_BY_NUME_ASC = 'nume_asc';
    const ORDER_BY_NUME_DESC = 'nume_desc';
    const ORDER_BY_PARENT_ASC = 'parent_asc';
    const ORDER_BY_PARENT_DESC = 'parent_desc';
    const ORDER_BY_ORDER_ASC = 'order_asc';
    const ORDER_BY_ORDER_DESC = 'order_desc';


    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $excludedId;

    /**
     * @var string
     */
    private $numeLike;

    /**
     * @var string
     */
    private $detaliiLike;

    /**
     * @var int
     */
    private $parentId;

    /**
     * @var RolItem
     */
    private $parent;
    
    /**
     * @var int
     */
    private $orderId;

    /**
     * @var string
     */
    private $searchTerm;

    /**
     * @var array
     */
    private $orderBy = [];


    // ---------------------------------------------------------------------------------------------

    public function __construct(array $getData = array())
    {
        if (isset($getData['id'])) {
            $this->setId($getData['id']);
        }
        if (isset($getData['excludedId'])) {
            $this->setExcludedId($getData['excludedId']);
        }
        if (isset($getData['numeLike'])) {
            $this->setNumeLike($getData['numeLike']);
        }
        if (isset($getData['detaliiLike'])) {
            $this->setDetaliiLike($getData['detaliiLike']);
        }
        if (isset($getData['parentId'])) {
            $this->setParentId($getData['parentId']);
        }
        if (isset($getData['orderId'])) {
            $this->setOrderId($getData['orderId']);
        }
        if (isset($getData['searchTerm'])) {
            $this->setSearchTerm($getData['searchTerm']);
        }


    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return int|null
     */
    public function getExcludedId()
    {
        return $this->excludedId;
    }

    /**
     * @param int|null $excludedId
     */
    public function setExcludedId($excludedId)
    {
        $this->excludedId = $excludedId ? (int)$excludedId : null;
    }
    
    /**
     * @return string|null
     */
    public function getNumeLike()
    {
        return $this->numeLike;
    }
    
    /**
     * @param string|null $numeLike
     */
    public function setNumeLike($numeLike)
    {
        $this->numeLike = is_null($numeLike) ? null : trim(strip_tags($numeLike));
    }

    /**
     * @return string|null
     */
    public function getDetaliiLike()
    {
        return $this->detaliiLike;
    }

    /**
     * @param string|null $detaliiLike
     */
    public function setDetaliiLike($detaliiLike)
    {
        $this->detaliiLike = is_null($detaliiLike) ? null : trim(strip_tags($detaliiLike));
    }
    
    /**
     * @return int|null
     */
    public function getParentId()
    {
        return $this->parentId;
    }
    
    /**
     * @param int|null $parentId
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId ? (int)$parentId : null;
    }
    
    /**
     * @return RolItem|null
     */
    public function getParent()
    {
        if ($this->parent) {
            return $this->parent;
        } elseif ($this->parentId) {
            $this->parent = RolTable::getInstance()->load($this->parentId);
            return $this->parent;
        } else {
            return null;
        }
    }
    
    /**
     * @return int|null
     */
    public function getOrderId()
    {
        return $this->orderId;
    }
    
    /**
     * @param int|null $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId ? (int)$orderId : null;
    }
    
    /**
     * @return string|null
     */
    public function getSearchTerm()
    {
        return $this->searchTerm;
    }

    /**
     * @param string|null $val
     */
    public function setSearchTerm($val)
    {
        $this->searchTerm = is_null($val) ? null : trim(strip_tags($val));
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return array
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param array $orderBy
     */
    public function setOrderBy(array $orderBy)
    {
        $orderBy = (is_array($orderBy) && count($orderBy) ? $orderBy : array());
        if (count($orderBy)) {
            $orderItems = array_keys(self::fetchOrderItems());
            foreach ($orderBy as $k=>$v) {
                if (!in_array($v, $orderItems)) {
                    unset($orderBy[$k]);
                }
            }
        }
        $this->orderBy = $orderBy;
    }

    /**
     * @return array
     */
    public static function fetchOrderItems()
    {
        return array(
            self::ORDER_BY_ID_ASC => 'ID - ASC',
            self::ORDER_BY_ID_DESC => 'ID - DESC',
            self::ORDER_BY_NUME_ASC => 'Nume - ASC',
            self::ORDER_BY_NUME_DESC => 'Nume - DESC',
            self::ORDER_BY_PARENT_ASC => 'Parent - ASC',
            self::ORDER_BY_PARENT_DESC => 'Parent - DESC',
            self::ORDER_BY_ORDER_ASC => 'Order - ASC',
            self::ORDER_BY_ORDER_DESC => 'Order - DESC',
        );
    }

}