<?php

class PersonajRolItem
{
    use ArrayOrJson;
    
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $personajId;

    /**
     * @var int
     */
    private $rolId;


    // ---------------------------------------------------------------------------------------------

    public function __construct(array $dbRow = [])
    {
        if ( !count($dbRow) ) {
            return;
        }

        $this->id = $dbRow['id'] ? (int)$dbRow['id'] : null;
        $this->personajId = $dbRow['personaj_id'] ? (int)$dbRow['personaj_id'] : null;
        $this->rolId = $dbRow['rol_id'] ? (int)$dbRow['rol_id'] : null;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return int
     */
    public function getPersonajId()
    {
        return $this->personajId;
    }

    /**
     * @param int $personajId
     */
    public function setPersonajId($personajId)
    {
        $this->personajId = $personajId ? (int)$personajId : null;
    }

    /**
     * @return int
     */
    public function getRolId()
    {
        return $this->rolId;
    }

    /**
     * @param int $rolId
     */
    public function setRolId($rolId)
    {
        $this->rolId = $rolId ? (int)$rolId : null;
    }


}
