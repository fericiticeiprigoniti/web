<?php


class RolItem
{

    use ArrayOrJson;
    
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $nume;

    /**
     * @var string
     */
    private $detalii;
    
    /**
     * @var int
     */
    private $parentId;
    
    /**
     * @var RolItem
     */
    private $parent;

    /**
     * @var int
     */
    private $orderId;

    // ---------------------------------------------------------------------------------------------

    public function __construct(array $dbRow = array())
    {
        if (!count($dbRow)) {
            return;
        }
        $this->id = (int)$dbRow['id'];
        $this->nume = $dbRow['rol_nume'];
        $this->detalii = $dbRow['rol_detalii'];
        $this->parentId = $dbRow['parent_id'];
        $this->orderId = $dbRow['order_id'];
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getNume()
    {
        return $this->nume;
    }

    /**
     * @return string|null
     */
    public function getDetalii()
    {
        return $this->detalii;
    }

    /**
     * @return int|null
     */
    public function getParentId()
    {
        return $this->parentId;
    }
    
    /**
     * @return RolItem|null
     */
    public function getParent()
    {
        if ($this->parent) {
            return $this->parent;
        } elseif ($this->parentId) {
            $this->parent = RolTable::getInstance()->load($this->parentId);
            return $this->parent;
        } else {
            return null;
        }
    }
    
    /**
     * @return int|null
     */
    public function getOrderId()
    {
        return $this->orderId;
    }
    
}