<?php

class CitatItem
{
    use ArrayOrJson;

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $personajId;

    /**
     * @var string
     */
    private $continut;

    /**
     * @var string
     */
    private $carteId;

    /**
     * @var string
     */
    private $sursa;

    /**
     * @var int
     */
    private $locPatimireId;

    /**
     * @var string
     */
    private $observatii;

    /**
     * @var int
     */
    private $syncCitateortodoxe;

    /**
     * @var int
     */
    private $cron;

    /**
     * @var int
     */
    private $pagStart;

    /**
     * @var int
     */
    private $pagEnd;

    /**
     * @var string
     */
    private $insertData;

    /**
     * @var int
     */
    private $vizibilPi;

    /**
     * @var int
     */
    private $isDeleted;

    # ==========================================================================
    #
    #   Asociation table -> fcp_personaje
    #
    # ==========================================================================

    /**
     * @var PersonajItem $objPersonaj
     */
    private $objPersonaj;

    # ==========================================================================
    #
    #   Asociation table -> fcp_biblioteca_carti
    #
    # ==========================================================================

    /**
     * @var CarteItem $objCarte
     */
    private $objCarte;


    // ---------------------------------------------------------------------------------------------

    public function __construct(array $dbRow = [])
    {
        if ( !count($dbRow) ) {
            return;
        }

        $this->id = $dbRow['id'] ? (int)$dbRow['id'] : null;
        $this->personajId = $dbRow['personaj_id'] ? (int)$dbRow['personaj_id'] : null;
        $this->continut = $dbRow['continut'] ? (string)$dbRow['continut'] : null;
        $this->carteId = $dbRow['carte_id'] ? (string)$dbRow['carte_id'] : null;
        $this->sursa = $dbRow['sursa'] ? (string)$dbRow['sursa'] : null;
        $this->locPatimireId = $dbRow['loc_patimire_id'] ? (int)$dbRow['loc_patimire_id'] : null;
        $this->observatii = $dbRow['observatii'] ? (string)$dbRow['observatii'] : null;
        $this->syncCitateortodoxe = $dbRow['sync_citateortodoxe'] ? (int)$dbRow['sync_citateortodoxe'] : null;
        $this->cron = $dbRow['cron'] ? (int)$dbRow['cron'] : null;
        $this->pagStart = $dbRow['pag_start'] ? (int)$dbRow['pag_start'] : null;
        $this->pagEnd = $dbRow['pag_end'] ? (int)$dbRow['pag_end'] : null;
        $this->insertData = $dbRow['insert_data'] ? (string)$dbRow['insert_data'] : null;
        $this->vizibilPi = $dbRow['vizibil_pi'] ? (int)$dbRow['vizibil_pi'] : null;
        $this->isDeleted = $dbRow['is_deleted'] ? (int)$dbRow['is_deleted'] : null;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return int
     */
    public function getPersonajId()
    {
        return $this->personajId;
    }

    /**
     * @param int $personajId
     */
    public function setPersonajId($personajId)
    {
        $this->personajId = $personajId ? (int)$personajId : null;
    }

    /**
     * @return string
     */
    public function getContinut()
    {
        return $this->continut;
    }

    /**
     * @param string $continut
     */
    public function setContinut($continut)
    {
        $this->continut = $continut ? (string)$continut : null;
    }

    /**
     * @return string
     */
    public function getCarteId()
    {
        return $this->carteId;
    }

    /**
     * @param string $carteId
     */
    public function setCarteId($carteId)
    {
        $this->carteId = $carteId ? (string)$carteId : null;
    }

    /**
     * @return string
     */
    public function getSursa()
    {
        return $this->sursa;
    }

    /**
     * @param string $sursa
     */
    public function setSursa($sursa)
    {
        $this->sursa = $sursa ? (string)$sursa : null;
    }

    /**
     * @return int
     */
    public function getLocPatimireId()
    {
        return $this->locPatimireId;
    }

    /**
     * @param int $locPatimireId
     */
    public function setLocPatimireId($locPatimireId)
    {
        $this->locPatimireId = $locPatimireId ? (int)$locPatimireId : null;
    }

    /**
     * @return string
     */
    public function getObservatii()
    {
        return $this->observatii;
    }

    /**
     * @param string $observatii
     */
    public function setObservatii($observatii)
    {
        $this->observatii = $observatii ? (string)$observatii : null;
    }

    /**
     * @return int
     */
    public function getSyncCitateortodoxe()
    {
        return $this->syncCitateortodoxe;
    }

    /**
     * @param int $syncCitateortodoxe
     */
    public function setSyncCitateortodoxe($syncCitateortodoxe)
    {
        $this->syncCitateortodoxe = $syncCitateortodoxe ? (int)$syncCitateortodoxe : null;
    }

    /**
     * @return int
     */
    public function getCron()
    {
        return $this->cron;
    }

    /**
     * @param int $cron
     */
    public function setCron($cron)
    {
        $this->cron = $cron ? (int)$cron : null;
    }

    /**
     * @return int
     */
    public function getPagStart()
    {
        return $this->pagStart;
    }

    /**
     * @param int $pagStart
     */
    public function setPagStart($pagStart)
    {
        $this->pagStart = $pagStart ? (int)$pagStart : null;
    }

    /**
     * @return int
     */
    public function getPagEnd()
    {
        return $this->pagEnd;
    }

    /**
     * @param int $pagEnd
     */
    public function setPagEnd($pagEnd)
    {
        $this->pagEnd = $pagEnd ? (int)$pagEnd : null;
    }

    /**
     * @return string
     */
    public function getInsertData()
    {
        return $this->insertData;
    }

    /**
     * @param string $insertData
     */
    public function setInsertData($insertData)
    {
        $this->insertData = $insertData ? (string)$insertData : null;
    }

    /**
     * @return int
     */
    public function getVizibilPi()
    {
        return $this->vizibilPi;
    }

    /**
     * @param int $vizibilPi
     */
    public function setVizibilPi($vizibilPi)
    {
        $this->vizibilPi = $vizibilPi ? (int)$vizibilPi : null;
    }

    /**
     * @return int
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param int $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted ? (int)$isDeleted : null;
    }


    // -----------------------------------------------------------------------------------------------------------------

    /**
     * @return PersonajItem|null
     * @throws Exception
     */
    public function getPersonaj()
    {
        if ($this->objPersonaj) {
            return $this->objPersonaj;
        } elseif ($this->personajId) {
            $this->objPersonaj = PersonajTable::getInstance()->load($this->personajId);
            return $this->objPersonaj;
        } else {
            return null;
        }
    }

    /**
     * @return CarteItem|null
     * @throws Exception
     */
    public function getCarte()
    {
        if ($this->objCarte) {
            return $this->objCarte;
        } elseif ($this->carteId) {
            $this->objCarte = CarteTable::getInstance()->load($this->carteId);
            return $this->objCarte;
        }

        return null;
    }
}
