<?php

/**
 * Class MediaTable
 * @table fcp_media
 */
class MediaTable extends CI_Model
{

    /**
     * @var MediaTable
     */
    private static $instance;


    /**
     * Singleton
     * MediaTable constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * We can later dependency inject and thus unit test this
     * @return MediaTable
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param mixed $id
     * @return MediaItem|null
     * @throws Exception
     */
    public function load($id)
    {
        if (!$id) {
            throw new Exception("Invalid Media id");
        }
        $filters = new MediaFilters(array('id' => $id));
        $data = $this->fetchAll($filters);
        $item = isset($data['items']) && isset($data['items'][0]) ? $data['items'][0] : null;
        return $item;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param MediaFilters $filters
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function fetchAll(MediaFilters $filters = null, $limit = null, $offset = null)
    {
        // Set where and having conditions.
        $sqlJoin  = '';
        $sqlWhere = array();
        $sqlHaving = array();
        if ($filters) {
            if ($filters->getId()) {
                $sqlWhere[] = "id = {$filters->getId()}";
            }
            if ($filters->getExcludedId()) {
                $sqlWhere[] = "id <> {$filters->getExcludedId()}";
            }

            // video/audio
            if ($filters->getMediaTipId()) {
                $sqlWhere[] = "media_tip_id = {$filters->getMediaTipId()}";
            }

            // documentar/interviu etc
            if ($filters->getMediaCatId()) {
                $sqlWhere[] = "media_cat_id = {$filters->getMediaCatId()}";
            }

            // custom JOIN
            if ($filters->getJoinTableAsoc() && $filters->getJoinTableId()) {

                switch ($filters->getJoinTableAsoc()) {
                    case MEDIA_JOIN_POEZII:

                        $sqlJoin    = 'LEFT JOIN fcp_poezii2media as t2 ON media_id = id';
                        $sqlWhere[] = "t2.poezie_id = {$filters->getJoinTableId()}";
                        break;
                }
            }
        }

        $sqlWhere = implode("\n\tAND ", $sqlWhere);
        $sqlWhere = !empty($sqlWhere) ? "AND " . $sqlWhere : '';

        $sqlHaving = implode("\n\tAND ", $sqlHaving);
        $sqlHaving = !empty($sqlHaving) ? "HAVING " . $sqlHaving : '';

        // ------------------------------------

        $sqlOrder = array();
        if ($filters && count($filters->getOrderBy())) {
            foreach ($filters->getOrderBy() as $ord) {
                switch ($ord) {
                    case $filters::ORDER_BY_ID_ASC:
                        $sqlOrder[] = "id ASC";
                        break;
                    case $filters::ORDER_BY_ID_DESC:
                        $sqlOrder[] = "id DESC";
                        break;
                }
            }
        }
        if (!count($sqlOrder)) {
            $sqlOrder[] = "id DESC";
        }
        $sqlOrder = implode(", ", $sqlOrder);

        // ------------------------------------

        // Set limit.
        $sqlLimit = '';
        if (!is_null($offset) && !is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$offset . ', ' . (int)$limit;
        } elseif (!is_null($limit)) {
            $sqlLimit = 'LIMIT ' . (int)$limit;
        }

        // ------------------------------------

        $sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    t1.*
FROM
    fcp_media as t1
$sqlJoin
WHERE
    1
    $sqlWhere
GROUP BY id
$sqlHaving
ORDER BY $sqlOrder
$sqlLimit
EOSQL;

        /** @var CI_DB_result $result */
        $result = $this->db->query($sql);
        $count = $this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        $items = array();
        foreach ($result->result('array') as $row) {
            $items[] = new MediaItem($row);
        }

        return array(
            'items' => $items,
            'count' => $count,
        );
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param MediaItem $item
     * @return bool|int
     */
    public function save(MediaItem $item)
    {
        // Set data.
        $eData = array(
            'media_tip_id'      => $item->getMediaTipId() ? $item->getMediaTipId() : null,
            'media_cat_id'      => $item->getMediaCatId() ? $item->getMediaCatId() : null,
            'titlu'             => $item->getTitlu() ? $item->getTitlu() : null,
            'descriere'         => $item->getDescriere() ? $item->getDescriere() : null,
            'an_aparitie'       => $item->getAnAparitie() ? $item->getAnAparitie() : null,
            'producator'        => $item->getProducator() ? $item->getProducator() : null,
            'transcript'        => $item->getTranscript() ? $item->getTranscript() : null,
            'link'              => $item->getLink() ? $item->getLink() : null,
            'serial_id'         => $item->getSerialId() ? $item->getSerialId() : null,
            'durata_secunde'    => $item->getDurataSecunde() ? $item->getDurataSecunde() : null,
            'observatii'        => $item->getObservatii() ? $item->getObservatii() : null,
            'embeded_site'      => $item->getEmbededSite() ? $item->getEmbededSite() : null,
            'embeded_uniq_code' => $item->getEmbededUniqCode() ? $item->getEmbededUniqCode() : null,
            'is_deleted'        => $item->getIsDeleted() ? $item->getIsDeleted() : null,
        );

        // Insert/Update.
        if (!$item->getId()) {
            $res = $this->db->insert('fcp_media', $eData);
        } else {
            $res = $this->db->update('fcp_media', $eData, 'id = ' . $item->getId());
        }

        return $res;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param MediaItem $item
     * @return mixed
     */
    public function delete(MediaItem $item)
    {
        return $this->db->delete('fcp_media', 'id = ' . $item->getId());
    }

    // ---------------------------------------------------------------------------------------------


}
