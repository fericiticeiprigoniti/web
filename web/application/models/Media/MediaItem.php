<?php

class MediaItem
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $mediaTipId;

    /**
     * @var int
     */
    private $mediaCatId;

    /**
     * @var string
     */
    private $titlu;

    /**
     * @var string
     */
    private $descriere;

    /**
     * @var int
     */
    private $anAparitie;

    /**
     * @var string
     */
    private $producator;

    /**
     * @var string
     */
    private $transcript;

    /**
     * @var mixed
     */
    private $link;

    /**
     * @var int
     */
    private $serialId;

    /**
     * @var int
     */
    private $durataSecunde;

    /**
     * @var mixed
     */
    private $observatii;

    /**
     * @var int
     */
    private $embededSite;

    /**
     * @var string
     */
    private $embededUniqCode;

    /**
     * @var int
     */
    private $isDeleted;


    // ---------------------------------------------------------------------------------------------

    public function __construct(array $dbRow = [])
    {
        if ( !count($dbRow) ) {
            return;
        }

        $this->id               = $dbRow['id'] ? (int)$dbRow['id'] : null;
        $this->mediaTipId       = $dbRow['media_tip_id'] ? (int)$dbRow['media_tip_id'] : null;
        $this->mediaCatId       = $dbRow['media_cat_id'] ? (int)$dbRow['media_cat_id'] : null;
        $this->titlu            = $dbRow['titlu'] ? (string)$dbRow['titlu'] : null;
        $this->descriere        = $dbRow['descriere'] ? (string)$dbRow['descriere'] : null;
        $this->anAparitie       = $dbRow['an_aparitie'] ? (int)$dbRow['an_aparitie'] : null;
        $this->producator       = $dbRow['producator'] ? (string)$dbRow['producator'] : null;
        $this->transcript       = $dbRow['transcript'] ? (string)$dbRow['transcript'] : null;
        $this->link             = $dbRow['link'] ? $dbRow['link'] : null;
        $this->serialId         = $dbRow['serial_id'] ? (int)$dbRow['serial_id'] : null;
        $this->durataSecunde    = $dbRow['durata_secunde'] ? (int)$dbRow['durata_secunde'] : null;
        $this->observatii       = $dbRow['observatii'] ? $dbRow['observatii'] : null;
        $this->embededSite      = $dbRow['embeded_site'] ? (int)$dbRow['embeded_site'] : null;
        $this->embededUniqCode  = $dbRow['embeded_uniq_code'] ? (string)$dbRow['embeded_uniq_code'] : null;
        $this->isDeleted        = $dbRow['is_deleted'] ? (int)$dbRow['is_deleted'] : null;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return int
     */
    public function getMediaTipId()
    {
        return $this->mediaTipId;
    }

    /**
     * @param int $mediaTipId
     */
    public function setMediaTipId($mediaTipId)
    {
        $this->mediaTipId = $mediaTipId ? (int)$mediaTipId : null;
    }

    /**
     * @return int
     */
    public function getMediaCatId()
    {
        return $this->mediaCatId;
    }

    /**
     * @param int $mediaCatId
     */
    public function setMediaCatId($mediaCatId)
    {
        $this->mediaCatId = $mediaCatId ? (int)$mediaCatId : null;
    }

    /**
     * @return string
     */
    public function getTitlu()
    {
        return $this->titlu;
    }

    /**
     * @param string $titlu
     */
    public function setTitlu($titlu)
    {
        $this->titlu = $titlu ? (string)$titlu : null;
    }

    /**
     * @return string
     */
    public function getDescriere()
    {
        return $this->descriere;
    }

    /**
     * @param string $descriere
     */
    public function setDescriere($descriere)
    {
        $this->descriere = $descriere ? (string)$descriere : null;
    }

    /**
     * @return int
     */
    public function getAnAparitie()
    {
        return $this->anAparitie;
    }

    /**
     * @param int $anAparitie
     */
    public function setAnAparitie($anAparitie)
    {
        $this->anAparitie = $anAparitie ? (int)$anAparitie : null;
    }

    /**
     * @return string
     */
    public function getProducator()
    {
        return $this->producator;
    }

    /**
     * @param string $producator
     */
    public function setProducator($producator)
    {
        $this->producator = $producator ? (string)$producator : null;
    }

    /**
     * @return string
     */
    public function getTranscript()
    {
        return $this->transcript;
    }

    /**
     * @param string $transcript
     */
    public function setTranscript($transcript)
    {
        $this->transcript = $transcript ? (string)$transcript : null;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link ? $link : null;
    }

    /**
     * @return int
     */
    public function getSerialId()
    {
        return $this->serialId;
    }

    /**
     * @param int $serialId
     */
    public function setSerialId($serialId)
    {
        $this->serialId = $serialId ? (int)$serialId : null;
    }

    /**
     * @return int
     */
    public function getDurataSecunde()
    {
        return $this->durataSecunde;
    }

    /**
     * @param int $durataSecunde
     */
    public function setDurataSecunde($durataSecunde)
    {
        $this->durataSecunde = $durataSecunde ? (int)$durataSecunde : null;
    }

    /**
     * @return mixed
     */
    public function getObservatii()
    {
        return $this->observatii;
    }

    /**
     * @param mixed $observatii
     */
    public function setObservatii($observatii)
    {
        $this->observatii = $observatii ? $observatii : null;
    }

    /**
     * @return int
     */
    public function getEmbededSite()
    {
        return $this->embededSite;
    }

    /**
     * @param int $embededSite
     */
    public function setEmbededSite($embededSite)
    {
        $this->embededSite = $embededSite ? (int)$embededSite : null;
    }

    /**
     * @return string
     */
    public function getEmbededUniqCode()
    {
        return $this->embededUniqCode;
    }

    /**
     * @param string $embededUniqCode
     */
    public function setEmbededUniqCode($embededUniqCode)
    {
        $this->embededUniqCode = $embededUniqCode ? (string)$embededUniqCode : null;
    }

    /**
     * @return int
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param int $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted ? (int)$isDeleted : null;
    }


}
