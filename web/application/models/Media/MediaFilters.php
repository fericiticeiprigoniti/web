<?php

class MediaFilters
{

    const ORDER_BY_ID_ASC = 'id_asc';
    const ORDER_BY_ID_DESC = 'id_desc';

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $excludedId;

    /**
     * @var int
     */
    private $mediaTipId;

    /**
    * @var int
    */
    private $mediaCatId;

    /**
     * @var array
     */
    private $orderBy = [];

    // -------------------------------------------------------------------------

    /**
    * ex> MEDIA_JOIN_POEZII etc
    *
    * @var int
    */
    private $joinTableAsoc;

    /**
    * the asociation ID
    * ex> poezieID
    *
    * @var int
    */
    private $joinTableID;

    // ---------------------------------------------------------------------------------------------

    public function __construct(array $getData = [])
    {
        if (isset($getData['id'])) {
            $this->setId($getData['id']);
        }
        if (isset($getData['excludedId'])) {
            $this->setExcludedId($getData['excludedId']);
        }
        if (isset($getData['mediaTipId'])) {
            $this->setMediaTipId($getData['mediaTipId']);
        }
        if (isset($getData['mediaCatId'])) {
            $this->setMediaCatId($getData['mediaCatId']);
        }
        if (isset($getData['_orderBy'])) {
            $this->setOrderBy($getData['_orderBy']);
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id ? (int)$id : null;
    }

    /**
     * @return int
     */
    public function getExcludedId()
    {
        return $this->excludedId;
    }

    /**
     * @param int $excludedId
     */
    public function setExcludedId($excludedId)
    {
        $this->excludedId = $excludedId ? (int)$excludedId : null;
    }

    /**
    * @return int
    */
    public function getMediaTipId()
    {
        return $this->mediaTipId;
    }

    /**
    * @param int $mediaTipId
    */
    public function setMediaTipId($mediaTipId)
    {
        $this->mediaTipId = $mediaTipId ? (int)$mediaTipId : null;
    }

    /**
     * @return int
     */
    public function getMediaCatId()
    {
        return $this->mediaCatId;
    }

    /**
     * @param int $mediaCatId
     */
    public function setMediaCatId($mediaCatId)
    {
        $this->mediaCatId = $mediaCatId ? (int)$mediaCatId : null;
    }

    /**
     * @return array
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param array $orderBy
     */
    public function setOrderBy(array $orderBy)
    {
        $orderBy = (is_array($orderBy) && count($orderBy) ? $orderBy : array());
        if (count($orderBy)) {
            $orderItems = array_keys(self::fetchOrderItems());
            foreach ($orderBy as $k=>$v) {
                if (!in_array($v, $orderItems)) {
                    unset($orderBy[$k]);
                }
            }
        }
        $this->orderBy = $orderBy;
    }


    /**
     * @param int $joinTableAsoc
     * @param int $joinTableId
     */
    public function setJoin($joinTableAsoc, $joinTableId)
    {
        $this->joinTableAsoc = $joinTableAsoc ? (int)$joinTableAsoc : null;
        $this->joinTableId   = $joinTableId ? (int)$joinTableId : null;
    }

    public function getJoinTableAsoc()
    {
        return $this->joinTableAsoc;
    }

    public function getJoinTableId()
    {
        return $this->joinTableId;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return array
     */
    public static function fetchOrderItems()
    {
        return array(
            self::ORDER_BY_ID_ASC => 'id_asc',
            self::ORDER_BY_ID_DESC => 'id_desc',
        );
    }


}
