<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_admin_user extends CI_Model {

    private $user_data = array();
    private $is_logged_in = false;

    function __construct()
    {
        parent::__construct();

        if ( $this->user_data = $this->session->userdata('logged_in') )
        {
            $this->is_logged_in = true;
        }
    }

    function login($username,$password='')
    {
        $query = $this->db->get_where('_users',array(
                                                    'uUsername' => $username,
                                                    'uPassword' => MD5($password),
                                                    'uIsDeleted' => '0',
                                                    'uIsBlocked' => '0'
                                                    )
        );

        if($result = $query->row_array()){
            $sess_array = array(
                 'userID'       => $result['uID'],
                 'name'         => $result['uName'],
                 'username'     => $result['uUsername'],
                 'permisiuneID' => $result['uPermisionID']
               );
            $this->session->set_userdata('logged_in', $sess_array);

            $this->is_logged_in = true;
            
            // simple cookie for Roxy FIleman
            setcookie('fm_token', sha1(1), time()+7200);
        }

        return $this->is_logged_in;
    }

    /**
    * returneaza toate informatiile utilizatorului logat
    *
    */
    function getArray()
    {
        $dbPrefix = 'aauth';
        $sql = <<<EOSQL
SELECT
   u.*
   , upName as permisiuneNume
FROM
    {$dbPrefix}_users AS u
    LEFT JOIN {$dbPrefix}_users_permisions ON upLevel = uPermisionID
WHERE
    uID = ?
EOSQL;

        $sql_array = array($this->getUserID());

        $query = $this->db->query($sql,$sql_array);
        return ($query)? $query->row_array() : FALSE;
    }

    /**
     * returneaza toate informatiile unui utilizator
     *
     * @param mixed $userID
     * @return bool|array
     */
    function getUser($userID)
    {
        $sql = "SELECT
                    u.*, upName as permisiuneNume
                FROM {DB_PRE}_users u
                LEFT JOIN {DB_PRE}_users_permisions ON upLevel = uPermisionID
                WHERE uID = ?";

        $sql_array = array($userID);

        $query = $this->db->query($sql,$sql_array);
        return ($query)? $query->row_array() : FALSE;
    }

    function isUser()
    {
        return $this->is_logged_in;
    }

    /**
    * verifica permisiunile - daca este administrator
    *
    */
    function isSuperUser()
    {
        if($log = $this->session->userdata['logged_in'])
        {
            if($log['permisiuneID'] == 1)
            {
                return true;
            }
        }
        return false;
    }

    /**
    * verifica permisiunile - daca este administrator
    *
    */
    function isAdmin()
    {
        if($log = $this->session->userdata['logged_in'])
        {
            if($log['permisiuneID'] == 2)
            {
                return true;
            }
        }
        return false;
    }


    /**
    * returneaza user ID-ul din sesiune
    *
    */
    function getUserID()
    {
        if($log = $this->session->userdata('logged_in'))
        {
            return $log['userID'];
        }
        return false;
    }
    /**
    * returneaza username`ul din sesiune
    *
    */
    function getUsername()
    {
        if($log = $this->session->userdata('logged_in'))
        {
            return $log['username'];
        }
        return false;
    }
    
    /**
    * return name
    * 
    */
    function getName()
    {
        if($log = $this->session->userdata('logged_in'))
        {
            return $log['name'];
        }
        return false; 
    }

    /**
    * functia de Logout
    *
    */
    public function logout()
    {
        if ($this->session->userdata('logged_in'))
        {
            $this->session->unset_userdata('logged_in');
        }

        return !$this->isUser();
    }

    /**
     * verifica daca username`ul exista in baza de date (daca este unic)
     * EXCLUDE userul pe care esti logat
     *
     * @param $username
     * @param mixed $withSession = 1 (se verifica toti userii in afara de cel LOGAT -> din sesiune)
     * @return bool|array
     * @internal param mixed $user
     */
    function checkUser($username, $withSession = 0)
    {
        $cond = '';
        $log = $this->session->userdata('logged_in');
        $query = $this->db->get_where('_users',array('uUsername' => $username,'uID' => $log['userID']));

        if($withSession)
        {
            $cond = " AND uID <> " . $log['userID'];
        }

        $sql = "SELECT * FROM {DB_PRE}_users
                WHERE uUsername = ? {$cond}";

        $sql_array = array($username);

        $query = $this->db->query($sql,$sql_array);
        $result = $query->result_array();

        return ($result)? $result : FALSE;
    }

    function validareUser($username,$pass)
    {
        $sql = "SELECT * FROM {DB_PRE}_users
                WHERE uUsername = ? AND uPassword = ?";

        $sql_array = array($username,$pass);
        $query = $this->db->query($sql,$sql_array);

        return ($query->row_array())? TRUE : FALSE;
    }

    /**
    * afiseaza toti utilizatorii
    *
    */
    function getUsers()
    {
        $sql = "SELECT
                    u.uID as userID, uName as name, uEmail, uUsername as username, uPassword, uNrLoginGresite,
                    uDataInsert, uIsDeleted, uIsBlocked,
                    uPermisionID, upName as permisiuneNume
                FROM {DB_PRE}_users u
                LEFT JOIN {DB_PRE}_users_permisions ON upLevel = uPermisionID";

        $sql_array = array($this->getUsername());

        $query = $this->db->query($sql,$sql_array);
        return ($query)? $query->result_array() : FALSE;
    }

    /**
    * afiseaza permisiunile utilizatorilor
    *
    */
    function getPermisiuni()
    {
        $query = $this->db->get('_users_permisions');
        $result = $query->result_array();

        return ($result)? $result : FALSE;
    }

    function adaugaUser($fields)
    {
        return ($this->db->insert('_users', $fields))? TRUE : FALSE;
    }

    function sterge($uID)
    {
        $sql = "DELETE FROM {DB_PRE}_users WHERE uID = ?";
        return $this->db->query($sql, array($uID));
    }
    
    function update($id, $fields)
    {
        return ($this->db->update('_users', $fields, array('uID' => $id)))? TRUE : FALSE;
    }

}
/* End of file */