<div class="form-group top_search">
    <div class="input-group has-feedback">
        <input type="text" id="searchAllTerm" class="form-control" placeholder="search all..." />
        <span id="searchAllTermIcon" class="fa fa-search form-control-feedback right" aria-hidden="true"></span>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#searchAllTerm').devbridgeAutocomplete({
            serviceUrl: '/search_all',
            deferRequestBy: 300, // wait 300ms before making the ajax call.
            minChars: 1,
            noCache: true,
            groupBy: 'category',
            triggerSelectOnValidInput: false,
            onSelect: function(suggestion) {
                if (suggestion.url) {
                    openPopup(suggestion.url, {
                        width: 1000
                    });
                }
                if (suggestion.value.substring(0, 1) == '#') {
                    $('#searchAllTerm').val(suggestion.value + ' ');
                    $('#searchAllTerm').focus();
                    return false;
                }
            },
            onSearchStart: function(query) {
                $('.autocomplete-suggestions').append('<div class="loadingRemoteData"></div>');
                $('#searchAllTermIcon').removeClass('fa-search');
                $('#searchAllTermIcon').addClass('fa-spin fa-circle-o-notch');
            },
            onSearchComplete: function(query, suggestions) {
                $('#searchAllTermIcon').removeClass('fa-spin fa-circle-o-notch');
                $('#searchAllTermIcon').addClass('fa-search');
            }
        });
    });
</script>