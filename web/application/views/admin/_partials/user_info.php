<a href="javascript:" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
    <img src="/www/images/logo/Mantuitorul-Iisus-Hristos.jpg" alt="">
    <span class=" fa fa-angle-down"></span>
</a>
<ul class="dropdown-menu dropdown-usermenu pull-right">
    <li style="padding: 10px 20px; white-space: nowrap;">
        Bine ai venit <?= $this->aauth->get_user_var('nume') ?>
    </li>
    <li>
        <a href="/utilizatori/cont/<?= $this->aauth->get_user_id() ?>"> Profilul meu</a>
    </li>

    <!--    <li>-->
    <!--        <a href="javascript:">-->
    <!--            <span class="badge bg-red pull-right">50%</span>-->
    <!--            <span>Settings</span>-->
    <!--        </a>-->
    <!--    </li>-->

    <li>
        <a href="/logout">
            <i class="fa fa-sign-out pull-right"></i> Log Out
        </a>
    </li>
</ul>