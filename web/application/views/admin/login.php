<?php /** @var bool $login_error */ ?>
<!DOCTYPE html>
<html lang="ro">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="CPCO">

    <title>Login to FCP</title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="/www/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="/www/css/sb-login.css">
</head>

<body onload="setFocusOnUsername()">
    <div class="login_wrapp">
        
        <h2><img src="/www/images/logo/logo.png" border="0" width="300" height="32" alt="logo"></h2>
        <form name="form_login" method="post" action="" class="formular">
            <?php if($login_error): ?>
                <div class="alert alert-danger" role="alert">
                    <?= $this->aauth->print_errors() ?>
                </div>
            <?php endif; ?>
            
            <div class="input-group">
              <span class="input-group-addon"><span class="fa fa-user"></span></span>
              <input type="text" class="form-control" id="username" name="username" placeholder="username">
            </div><br/>
            
            <div class="input-group">
              <span class="input-group-addon"><span class="fa fa-key"></span></span>
              <input type="password" class="form-control" name="password" placeholder="password">
            </div><br />
            
            <div class="field">
                <input name="submit_login" class="btn btn-primary" type="submit" value="Autentificare" />
            </div>
        </form>
    </div>
    
</body>
</html>
<script>
function setFocusOnUsername() {
    document.getElementById("username").focus();
}
</script>