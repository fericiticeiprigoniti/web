<?php

/**
 * @var BibliografieItem $objItem
 * @var array $parentItems
 * @var string $page_title
 */
?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel tab-pane active">

            <div class="x_title">
                <h2><i class="fa fa-edit"></i> <?= $page_title ?></h2>

                <ul class="nav navbar-right panel_toolbox">
                    <li>
                        <button class="btn btn-primary" id="ajaxSubmit">
                            <i class="fa fa-save"></i> Save
                        </button>
                    </li>
                </ul>
                <br />
                <div class="clearfix"></div>
            </div>

            <div class="x_content">

                <form method="post" class="form-horizontal ajaxForm" action="<?= General::url() ?>" data-redirect-url="/biblioteca/bibliografii/">
                    <div class="col-md-6 col-md-offset-2">

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Titlu <span class="req">*</span></label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control" name="titlu" value="<?= html_escape($objItem->getTitlu()) ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Descriere <span class="req">*</span></label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control" name="descriere" value="<?= html_escape($objItem->getDescriere()) ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Publicat</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <select name="is_published" class="form-control select2">
                                    <option value="1" <?php echo ($objItem->getIsPublished() == 1) ? 'selected' : '' ?>>da</option>
                                    <option value="0" <?php echo ($objItem->getIsPublished() == 0) ? 'selected' : '' ?>>nu</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Șters</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <select name="is_deleted" class="form-control select2">
                                    <option value="1" <?= $objItem->getIsDeleted() == 1 ? 'selected' : '' ?>>da</option>
                                    <option value="0" <?= $objItem->getIsDeleted() == 0 ? 'selected' : '' ?>>nu</option>
                                </select>
                            </div>
                        </div>

                    </div>

                    <br />
                    <br />
                    <!-- Asociere carti -->
                    <?php $this->load->view('/admin/biblioteca/bibliografii/_partials/asociere_carti.php', $objItem) ?>
                    <!-- End Asociere carti -->

                </form>
            </div>
        </div>
    </div>
</div>