<?php

/**
 * @var CategorieCarteItem $categorie
 * @var array $parentItems
 * @var string $page_title
 */
?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-edit"></i> <?= $page_title ?></h2>
                <div class="clear"></div>
            </div>

            <div class="x_content">

                <form method="post" class="form-horizontal ajaxForm" action="<?= General::url() ?>" data-redirect-url="/biblioteca/categorii_carti/">
                    <div class="col-md-6 col-md-offset-2">

                        <?php if ($categorie->getId()): ?>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">ID</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" class="form-control" value="<?= $categorie->getId() ?>" readonly />
                                </div>
                            </div>
                        <?php endif; ?>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nume <span class="req">*</span></label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control" name="nume" value="<?= html_escape($categorie->getNume()) ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Parinte</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <select name="parentId" class="form-control select2 allowClear" data-placeholder="categoria parinte">
                                    <option></option>
                                    <?php foreach ($parentItems as $k => $v): ?>
                                        <?php $selected = $k == $categorie->getParentId() ? 'selected="selected"' : '' ?>
                                        <option <?= $selected ?> value="<?= html_escape($k) ?>"><?= html_escape($v) ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nr. ordine</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control" name="orderId" value="<?= html_escape($categorie->getOrderId()) ?>">
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="panel-footer">
                            <a href="/biblioteca/categorii_carti/" class="btn btn-default pull-left">
                                <i class="fa fa-arrow-left"></i> Lista categorii
                            </a>
                            <button type="submit" class="btn btn-primary pull-right">
                                <i class="fa fa-save"></i> Salvare
                            </button>
                            <div class="clear"></div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>