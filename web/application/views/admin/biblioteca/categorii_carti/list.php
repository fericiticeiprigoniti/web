<?php

/**
 * @var CategorieCarteItem[] $items
 */
?>

<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-table"></i>
                    <?= $page_title ?>
                    <span class="badge"><?= count($items) ?></span>
                </h2>
                <div class="clear"></div>
            </div>

            <div class="x_content">

                <div class="table-responsive">

                    <table class="table table-striped table-bordered table-hover table-highlight dataTable">
                        <thead>
                            <tr>
                                <th>Nr. crt.</th>
                                <th>ID</th>
                                <th>Categorie</th>
                                <th class="hidden-xs">Părinte</th>
                                <th class="hidden-xs">Nr. ordine</th>
                                <th class="actions">
                                    <div class="hidden-sm hidden-xs">
                                        <a href="/biblioteca/categorii_carti/edit/"
                                            class="btn btn-info btn-sm" role="button" style="width:100%">
                                            <i class="fa fa-plus"></i> Add
                                        </a>
                                    </div>
                                    <div class="hidden-md hidden-lg">
                                        <a href="/biblioteca/categorii_carti/edit/"
                                            class="btn btn-info btn-sm" role="button">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </th>
                            </tr>
                        </thead>

                        <?php if (count($items)): ?>
                            <tbody>
                                <?php $i = 0;
                                foreach ($items as $categorie): ?>
                                    <tr>
                                        <td class="center"><?= ++$i ?></td>
                                        <td class="center"><?= html_escape($categorie->getId()) ?></td>
                                        <td><?= html_escape($categorie->getNume()) ?></td>
                                        <td class="hidden-xs">
                                            <?= $categorie->getParent() ? html_escape($categorie->getParent()->getNume()) : '' ?>
                                        </td>
                                        <td class="hidden-xs text-center"><?= html_escape($categorie->getOrderId()) ?></td>
                                        <td class="actions" style="white-space: nowrap">
                                            <div class="hidden-sm hidden-xs">
                                                <a href="/biblioteca/categorii_carti/edit/<?= (int)$categorie->getId() ?>"
                                                    class="btn btn-sm btn-primary">
                                                    <i class="fa fa-edit bigger-110"></i> Edit
                                                </a>
                                                <a href="/biblioteca/categorii_carti/delete/<?= (int)$categorie->getId() ?>"
                                                    class="btn btn-sm btn-danger btnDelete"
                                                    data-name="categoria <?= html_escape($categorie->getNume()) ?>">
                                                    <i class="fa fa-trash-o bigger-110"></i> Delete
                                                </a>
                                            </div>
                                            <div class="hidden-md hidden-lg">
                                                <a href="/biblioteca/categorii_carti/edit/<?= (int)$categorie->getId() ?>"
                                                    class="btn btn-sm btn-primary" title="Edit">
                                                    <i class="fa fa-edit bigger-110"></i>
                                                </a>
                                                <a href="/biblioteca/categorii_carti/delete/<?= (int)$categorie->getId() ?>"
                                                    class="btn btn-sm btn-danger btnDelete"
                                                    data-name="categoria <?= html_escape($categorie->getNume()) ?>" title="Delete">
                                                    <i class="fa fa-trash-o bigger-110"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        <?php else: ?>
                            <tbody>
                                <tr>
                                    <td colspan="50" class="nodata"><?= html_escape('No data found') ?></td>
                                </tr>
                            </tbody>
                        <?php endif; ?>
                    </table>

                </div>

            </div>
        </div>

    </div>
</div>