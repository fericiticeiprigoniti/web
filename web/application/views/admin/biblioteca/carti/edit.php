<!-- Main content -->

<?php
if($this->session->flashdata('log')){?>
<div class="alert alert-success alert-dismissible fade in" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <?= $this->session->flashdata('log') ?>
</div>
<?php }?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= isset($autocomplete_page['subtitle'])? $autocomplete_page['subtitle'] : '' ?></h2>

                <ul class="nav navbar-right panel_toolbox">
                    <li>
                        <button class="btn btn-primary" id="ajaxSubmit">
                          <i class="fa fa-save"></i> Save
                        </button>
                    </li>
                </ul>
                <br/>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">

                <input type="hidden" name="tokenID" id="token" value="<?= rand(1,999) ?>" />

                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                  <ul id="myTab" class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="<?php echo ($tab_activ == 'generale')? 'active' : '' ?>">
                        <a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab"
                            aria-expanded="<?php echo ($tab_activ == 'generale')? 'true' : 'false' ?>">Informații generale</a>
                    </li>
                    <li role="presentation" class="<?php echo ($tab_activ == 'detalii')? 'active' : (!$edit? 'disabled disabledTab' : '') ?>">
                        <a href="#tab_content2" role="tab" id="profile-tab" <?= $edit ? 'data-toggle="tab"' : '' ?>
                            aria-expanded="<?php echo ($tab_activ == 'detalii')? 'true' : 'false' ?>">Detalii carte</a>
                    </li>
                    <li role="presentation" class="<?php echo ($tab_activ == 'cuvinte')? 'active' : (!$edit? 'disabled disabledTab' : '') ?>">
                        <a href="#tab_content3" role="tab" id="profile-tab3" <?= $edit ? 'data-toggle="tab"' : '' ?>
                            aria-expanded="<?php echo ($tab_activ == 'cuvinte')? 'true' : 'false' ?>">Cuvinte cheie</a>
                    </li>
                    <li role="presentation" class="<?php echo ($tab_activ == 'ebook')? 'active' : (!$edit? 'disabled disabledTab' : '') ?>">
                        <a href="#tab_content4" role="tab" id="profile-tab4" <?= $edit ? 'data-toggle="tab"' : '' ?>
                            aria-expanded="<?php echo ($tab_activ == 'ebook')? 'true' : 'false' ?>">eBook</a>
                    </li>

                  </ul>

                  <!-- Tab Content -->
                  <div id="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane fade <?=  ($tab_activ == 'generale')? 'active in' : '' ?>" id="tab_content1" aria-labelledby="home-tab">
                        <?php $this->load->view('/admin/biblioteca/carti/_partials/tab_generale') ?>
                    </div>

                    <?php if ($edit){?>
                        <div role="tabpanel" class="tab-pane fade <?=  ($tab_activ == 'detalii')? 'active in' : '' ?>" id="tab_content2" aria-labelledby="profile-tab">
                            <?php $this->load->view('/admin/biblioteca/carti/_partials/tab_detalii') ?>
                        </div>

                        <div role="tabpanel" class="tab-pane fade <?=  ($tab_activ == 'cuvinte')? 'active in' : '' ?>" id="tab_content3" aria-labelledby="profile-tab">
                            <?php $this->load->view('/admin/biblioteca/carti/_partials/tab_cuvinte') ?>
                        </div>

                        <div role="tabpanel" class="tab-pane fade <?= ($tab_activ == 'ebook')? 'active in' : '' ?>" id="tab_content4" aria-labelledby="profile-tab">
                            <?php $this->load->view('/admin/biblioteca/carti/_partials/tab_ebook') ?>
                        </div>

                    <?php }?>

                  </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){

    $('.table').on('click','.delete_row',function(){
        $(this).parents('tr').remove();
    })

    $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' });
})
</script>
