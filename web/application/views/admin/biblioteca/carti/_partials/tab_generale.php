<!-- TAB - Generale (informatii)-->
<?php
/**
 * @var CarteItem $objItem
 */
// d($objItem);
$categorii =  CategorieCarteTable::getInstance()->fetchForSelectWithParent();
?>

<?= validation_errors(); ?>
<form id="carte_generale"
    name="carte_generale"
    method="post"
    class="formular formular_autor form-horizontal form-label-left ajaxForm"
    action="<?= '/biblioteca/carti/' . (($edit) ? 'edit/' . $edit : 'add') . '?tab=generale' ?>">

    <div class="col-lg-9 col-md-9 col-sm-12">

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Titlu</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" name="titlu" style="color:#428bca;font-weight:bold;" class="form-control" value="<?= set_value('titlu', (($edit) ? $objItem->getTitlu() : '')) ?>" />
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Subtitlu</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" name="subtitlu" class="form-control" value="<?= set_value('subtitlu', (($edit) ? $objItem->getSubtitlu() : '')) ?>" />
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Categorie</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <select name="cat_id" class="form-control select2 allowClear" data-placeholder="selecteaza">
                    <option></option>
                    <?php echo_select_options($categorii, set_value('cat_id', ($edit ? $objItem->getCategorieId() : '')), ['_key', '_value']) ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label>Responsabili de carte <span class="fa fa-info-circle" title="Apar doar personajele active"></span></label>
            <label style="float:right">Rol responsabil de carte <span class="fa fa-info-circle" title="Toate rolurile editoriale"></span></label>
            <br />

            <?php
            if ($edit) {
                $k = 0;
                foreach ($objItem->getAutoriObj() as $k => $autor) {
            ?>
                    <div class="row" style="margin:5px 0px;">
                        <select class="select2 form-control inlineb allowClear" name="autoriList[]" data-placeholder="selecteaza" style="width:60%;float:left;">
                            <option></option>
                            <?php echo_select_options($personaje, $autor->getId(), array('aID', 'aName')); ?>
                        </select>
                        <select class="select2 form-control inlineb allowClear" name="autoriListRoluri[]" data-placeholder="selecteaza" style="width:30%;float:left;margin-left:20px;">
                            <option></option>
                            <?php echo_select_options($roluri, $autor->getId(), array('_key', '_value')); ?>
                        </select><br />
                    </div>
                    <?php }

                if ($k < 8) {
                    for ($i = $k; $i < 5 - $k; $i++) { ?>
                        <div class="row" style="margin:5px 0px">
                            <div class="col-md-6">
                                <select class="col-6 select2 form-control inlineb allowClear" name="autoriList[]" data-placeholder="selecteaza responsabil <?= $i + 1 ?>" style="width:60%;float:left;">
                                    <option></option>
                                    <?php echo_select_options($personaje, false, array('_key', '_value')); ?>
                                </select>
                            </div>

                            <div class="col-md-6">
                                <select class="col-6 select2 form-control inlineb allowClear" name="autoriListRoluri[]" data-placeholder="selecteaza rol editorial" style="width:30%;float:left;margin-left:20px;">
                                    <option></option>
                                    <?php echo_select_options($roluri, false, array('_key', '_value')); ?>
                                </select>
                            </div>
                        </div>
                    <?php }
                }
            } else {
                for ($i = 0; $i < 5; $i++) { ?>
                    <div class="row" style="margin:5px 0px">
                        <div class="col-md-6">
                            <select class="col-6 select2 form-control inlineb allowClear" name="autoriList[]" data-placeholder="selecteaza responsabil <?= $i + 1 ?>" style="width:60%;float:left;">
                                <option></option>
                                <?php echo_select_options($personaje, false, array('_key', '_value')); ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <select class="col-6 select2 form-control inlineb allowClear" name="autoriListRoluri[]" data-placeholder="selecteaza rol editorial" style="width:30%;margin-left:20px;float:left;">
                                <option></option>
                                <?php echo_select_options($roluri, false, array('_key', '_value')); ?>
                            </select>
                        </div>
                    </div>
            <?php }
            } ?>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Editura</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="hidden" id="edituraId" name="editura_id" value="<?= $edit ? html_escape($objItem->getEdituraId()) : '' ?>" />
                <input type="text" class="form-control devbridge-autocomplete" placeholder="selecteaza"
                    data-id="edituraId" data-url="/biblioteca/carti/cautaEditura"
                    value="<?= $objItem->getEditura() ? html_escape($objItem->getEditura()->getSearchResult()) : '' ?>" />
                <span class="fa fa-search form-control-feedback right" aria-hidden="true"></span>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Editura 2</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="hidden" id="editura2_id" name="editura2_id" value="<?= $edit ? html_escape($objItem->getEditura2Id()) : '' ?>" />
                <input type="text" class="form-control devbridge-autocomplete" placeholder="selecteaza"
                    data-id="editura2_id" data-url="/biblioteca/carti/cautaEditura"
                    value="<?= $objItem->getEditura2() ? html_escape($objItem->getEditura2()->getSearchResult()) : '' ?>" />
                <span class="fa fa-search form-control-feedback right" aria-hidden="true"></span>
            </div>
        </div>


        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Oras</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" name="oras" class="form-control" value="<?= set_value('oras', (($edit) ? $objItem->getOras() : '')) ?>" />
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Ediția</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" name="editia" class="form-control" value="<?= set_value('editia', (($edit) ? $objItem->getEditia() : '')) ?>" />
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Anul</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" name="anul_publicatiei" class="form-control" value="<?= set_value('anul_publicatiei', (($edit) ? $objItem->getAnulPublicatiei() : '')) ?>" />
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nr pagini</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" name="nr_pagini" class="form-control" value="<?= set_value('nr_pagini', (($edit) ? $objItem->getNrPagini() : '')) ?>" />
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nr anexe</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" name="nr_anexe" class="form-control" value="<?= set_value('nr_anexe', (($edit) ? $objItem->getNrAnexe() : '')) ?>" />
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Dimensiuni</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <select name="dimensiune_id" class="form-control allowClear select2-hidden-accessible" data-placeholder="selecteaza" tabindex="-1" aria-hidden="true">
                    <option></option>
                    <?php echo_select_options($dimensiuniCarte, set_value('dimensiune_id', $objItem->getDimensiuneId()), ['_key', '_value']) ?>
                </select>
            </div>
        </div>

    </div>


    <!-- right COLUMN -->
    <div class="col-lg-3 col-md-3 col-sm-12">
        <div class="alert alert-info2">

            <div class="form-group p0">
                <div class="col-lg-6 pl0">
                    <label>Publicat</label>
                    <select name="is_published" class="form-control select2">
                        <option value="1" <?= ($objItem->isPublished() == 1) ? 'selected' : '' ?>>da</option>
                        <option value="0" <?= ($objItem->isPublished() == 0) ? 'selected' : '' ?>>nu</option>
                    </select>
                </div>

                <div class="col-lg-6 pr0">
                    <label>Sters</label>
                    <select name="is_deleted" class="form-control select2">
                        <option value="1" <?= $objItem->isDeleted() == 1 ? 'selected' : '' ?>>da</option>
                        <option value="0" <?= $objItem->isDeleted() == 0 ? 'selected' : '' ?>>nu</option>
                    </select>
                </div>
            </div>

            <div class="form-group p0">
                <div class="col-lg-12 pl0">
                    <label>Disponibilitate</label>
                    <select name="disponibil_in_format" class="form-control select2 allowClear" data-placeholder="selecteaza">
                        <option></option>
                        <?php echo_select_options(CarteItem::fetchFormatTypes(), set_value('disponibil_in_format', $objItem->getFormat()), ['_key', '_value']) ?>
                    </select>
                </div>

            </div>
        </div>


        <div class="alert alert-info2">

            <div class="form-group">
                <label>Copertă</label><br />
                <div class="img-thumbnail">
                    <?php //$coperta = (isset($bookInfo))? (getCoperta($this->config->item('uploads_carti_path') . $bookInfo['pAlias'], $bookInfo['bPhoto'])) : getCoperta();
                    ?>
                    <img src="<?= $objItem->getCoperta() ?: '/www/images/default_book_cover.jpg' ?>" class="poza_coperta" />
                </div>
                <div class="form-group">
                    <input type="file" class="form-control" name="upload_coperta" id="upload_coperta" />
                    <?php if (isset($bookInfo) && $bookInfo['bPhoto']) { ?>
                        <a href="javascript:void(0)" class="remove_poza btn btn-danger" data-id="<?php echo $bookInfo['bID']; ?>">Șterge</a>
                    <?php } ?>
                </div>
            </div>


            <div class="form-group">
                <label>Observații</label>
                <textarea name="observatii" class="inlineb form-control" style="height:100px"><?= set_value('observatii', (($edit) ? $objItem->getObservatii() : '')); ?></textarea>
            </div>
        </div>

    </div>
    <!-- End RIght column -->

</form>