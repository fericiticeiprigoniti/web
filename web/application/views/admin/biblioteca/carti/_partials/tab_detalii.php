<!-- TAB - Detali carte -->

<?php
/**
 * @var CarteItem $objItem
 * @var ArticolItem $objIntroducereArticol
 * @var ArticolItem $objRezumatArticol
 * @var ArticolItem $objRecenzieArticol
 * @var int|null $edit
 */

$objIntroducereArticol = $objItem->getIntroducereArticol();
$objRezumatArticol = $objItem->getRezumatArticol();
$objRecenzieArticol = $objItem->getRecenzieArticol();

?>

<?= validation_errors(); ?>
<form id="carte_detalii"
    name="carte_detalii"
    method="post"
    class="formular formular_autor form-horizontal form-label-left ajaxForm"
    action="<?= '/biblioteca/' . ($edit ? "edit/{$edit}" : "add") . '?tab=detalii' ?>">

    <div class="col-lg-9 col-md-9 col-sm-12">

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Introducere/ Prefață</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <textarea name="introducere" class="tinymcesimple form-control"><?= set_value('introducere', (($edit && $objIntroducereArticol) ? $objIntroducereArticol->getContinut() : '')) ?></textarea>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Rezumat</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <textarea name="rezumat" class="tinymcesimple form-control"><?= set_value('rezumat', (($edit && $objRecenzieArticol) ? $objRecenzieArticol->getContinut() : '')) ?></textarea>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Recenzie</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <textarea name="recenzie" class="tinymcesimple form-control"><?= set_value('recenzie', (($edit && $objRecenzieArticol) ? $objRecenzieArticol->getContinut() : '')) ?></textarea>
            </div>
        </div>

    </div>
</form>