<?php

/**
 * @var CarteFilters $filters
 * @var array $dimensiuniCarte
 * @var array $tipuriFormat
 * @var object $order
 */

// Set order items.
$orderByItems = $filters->fetchOrderItems();
$selectedOrderItems = $filters->getOrderBy();

$yesNoValues = array(
    'yes' => 'Da',
    'no' => 'Nu',
);
?>

<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">

            <div class="x_title">
                <h2><i class="fa fa-filter"></i> Filtre</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link colapsed"><i class="fa fa-chevron-down"></i></a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <div class="x_content" style="display: none;">
                <form class="filter-form form-horizontal form-label-left" action="<?= current_url() ?>" method="get">
                    <input type="hidden" name="_page" value="1" />
                    <input type="hidden" name="_per_page" value="<?= (isset($_GET['_per_page']) ? (int)$_GET['_per_page'] : '') ?>" />

                    <div class="row">
                        <div class="col-sm-12 space-10"></div>
                    </div>
                    <div class="row">

                        <!-- Column 1 -->
                        <div class="col-sm-4 col-xs-12">

                            <div class="form-group has-feedback">
                                <input type="hidden" id="id" name="id" value="<?= html_escape($filters->getId()) ?>" />
                                <input type="text" class="form-control devbridge-autocomplete" placeholder="carte"
                                    data-id="id" data-url="/biblioteca/carti/cautaCarte"
                                    value="<?= $filters->getCarte() ? html_escape($filters->getCarte()->getSearchResult()) : '' ?>" />
                                <span class="fa fa-search form-control-feedback right" aria-hidden="true"></span>
                            </div>

                            <div class="form-group has-feedback">
                                <input type="hidden" id="parentId" name="parentId" value="<?= html_escape($filters->getParentId()) ?>" />
                                <input type="text" class="form-control devbridge-autocomplete"
                                    data-id="parentId" data-url="/biblioteca/carti/cautaParinte" placeholder="parinte"
                                    value="<?= $filters->getParent() ? html_escape($filters->getParent()->getSearchResult()) : '' ?>" />
                                <span class="fa fa-search form-control-feedback right" aria-hidden="true"></span>
                            </div>

                            <div class="form-group has-feedback">
                                <input type="hidden" id="categorieId" name="categorieId" value="<?= html_escape($filters->getCategorieId()) ?>" />
                                <input type="text" class="form-control devbridge-autocomplete" placeholder="categorie"
                                    data-id="categorieId" data-url="/biblioteca/carti/cautaCategorie"
                                    value="<?= $filters->getCategorie() ? html_escape($filters->getCategorie()->getSearchResult()) : '' ?>" />
                                <span class="fa fa-search form-control-feedback right" aria-hidden="true"></span>
                            </div>

                            <div class="form-group has-feedback">
                                <input type="hidden" id="autorId" name="autorId" value="<?= html_escape($filters->getAutorId()) ?>" />
                                <input type="text" class="form-control devbridge-autocomplete" placeholder="autor"
                                    data-id="autorId" data-url="/biblioteca/carti/cautaAutor"
                                    value="<?= $filters->getAutor() ? html_escape($filters->getAutor()->getSearchResult()) : '' ?>" />
                                <span class="fa fa-search form-control-feedback right" aria-hidden="true"></span>
                            </div>

                            <div class="form-group has-feedback">
                                <input type="hidden" id="localitateId" name="localitateId" value="<?= html_escape($filters->getLocalitateId()) ?>" />
                                <input type="text" class="form-control devbridge-autocomplete" placeholder="localitate"
                                    data-id="localitateId" data-url="/biblioteca/carti/cautaLocalitate"
                                    value="<?= $filters->getLocalitate() ? html_escape($filters->getLocalitate()->getNumeComplet()) : '' ?>" />
                                <span class="fa fa-search form-control-feedback right" aria-hidden="true"></span>
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" id="observatii" name="observatii"
                                    placeholder="observatii" value="<?= html_escape($filters->getObservatii()) ?>" />
                            </div>

                        </div>


                        <!-- Column 2 -->
                        <div class="col-sm-4 col-xs-12">

                            <div class="form-group has-feedback">
                                <input type="hidden" id="edituraId" name="edituraId" value="<?= html_escape($filters->getEdituraId()) ?>" />
                                <input type="text" class="form-control devbridge-autocomplete" placeholder="editura"
                                    data-id="edituraId" data-url="/biblioteca/carti/cautaEditura"
                                    value="<?= $filters->getEditura() ? html_escape($filters->getEditura()->getSearchResult()) : '' ?>" />
                                <span class="fa fa-search form-control-feedback right" aria-hidden="true"></span>
                            </div>

                            <div class="form-group has-feedback">
                                <input type="hidden" id="colectieId" name="colectieId" value="<?= html_escape($filters->getColectieId()) ?>" />
                                <input type="text" class="form-control devbridge-autocomplete" placeholder="colectie"
                                    data-id="colectieId" data-url="/biblioteca/carti/cautaColectie"
                                    value="<?= $filters->getColectie() ? html_escape($filters->getColectie()->getSearchResult()) : '' ?>" />
                                <span class="fa fa-search form-control-feedback right" aria-hidden="true"></span>
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" id="versiune" name="versiune"
                                    placeholder="versiune" value="<?= html_escape($filters->getVersiune()) ?>" />
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" id="editia" name="editia"
                                    placeholder="editia" value="<?= html_escape($filters->getEditia()) ?>" />
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" id="anulPublicatiei" name="anulPublicatiei"
                                    placeholder="anul publicatiei" value="<?= html_escape($filters->getAnulPublicatiei()) ?>" />
                            </div>

                            <div class="form-group">
                                <div class="col-xs-6" style="padding-left: 0">
                                    <input type="text" class="form-control" id="nrPaginiMin" name="nrPaginiMin"
                                        placeholder="nr. minim de pagini" value="<?= html_escape($filters->getNrPaginiMin()) ?>" />
                                </div>
                                <div class="col-xs-6" style="padding-right: 0">
                                    <input type="text" class="form-control" id="nrPaginiMax" name="nrPaginiMax"
                                        placeholder="nr. maxim de pagini" value="<?= html_escape($filters->getNrPaginiMax()) ?>" />
                                </div>
                            </div>

                        </div>


                        <!-- Column 3 -->
                        <div class="col-sm-4 col-xs-12">

                            <div class="form-group">
                                <select class="form-control allowClear" name="dimensiuneId" data-placeholder="dimensiune">
                                    <option></option>
                                    <?php foreach ($dimensiuniCarte as $oValue => $oName): ?>
                                        <?php $selected = ($oValue == $filters->getDimensiuneId()) ? 'selected="selected"' : ''; ?>
                                        <option <?= $selected ?> value="<?= html_escape($oValue) ?>"><?= html_escape($oName) ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <select class="form-control allowClear" name="format" data-placeholder="format">
                                    <option></option>
                                    <?php foreach ($tipuriFormat as $oValue => $oName): ?>
                                        <?php $selected = ($oValue == $filters->getFormat()) ? 'selected="selected"' : ''; ?>
                                        <option <?= $selected ?> value="<?= html_escape($oValue) ?>"><?= html_escape($oName) ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <select class="form-control allowClear" name="areCoperta" data-placeholder="are coperta">
                                    <option></option>
                                    <option <?= $filters->areCoperta() ? 'selected' : '' ?> value="1">Da</option>
                                    <option <?= $filters->nuAreCoperta() ? 'selected' : '' ?> value="0">Nu</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <select class="form-control allowClear" name="isPublished" data-placeholder="este publicata">
                                    <option></option>
                                    <option <?= $filters->isPublished() ? 'selected' : '' ?> value="1">Da</option>
                                    <option <?= $filters->isNotPublished() ? 'selected' : '' ?> value="0">Nu</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <select class="form-control allowClear" name="isDeleted" data-placeholder="este stearsa">
                                    <option></option>
                                    <option <?= $filters->isDeleted() ? 'selected' : '' ?> value="1">Da</option>
                                    <option <?= $filters->isNotDeleted() ? 'selected' : '' ?> value="0">Nu</option>
                                </select>
                            </div>

                        </div>

                    </div>

                    <div class="ln_solid" style="margin-top: 10px"></div>

                    <div class="row">
                        <div class="form-group col-md-8 col-sm-6 col-xs-12"></div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="form-group  has-feedback">
                                <select class="form-control selectOrderBy" id="_orderBy" name="_orderBy[]" multiple data-placeholder="<?= html_escape('order by') ?> ...">
                                    <option></option>
                                    <?php foreach ($orderByItems as $oValue => $oName): ?>
                                        <?php $selected = in_array($oValue, $selectedOrderItems) ? 'selected="selected"' : '' ?>
                                        <option <?= $selected ?> value="<?= html_escape($oValue) ?>"><?= html_escape($oName) ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="fa fa-sort form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group panel-footer" style="text-align: right">
                        <a href="<?= current_url() ?>" class="btn btn-warning">
                            <i class="fa fa-refresh"></i> Resetare
                        </a>
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-search"></i> Filtrare
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>