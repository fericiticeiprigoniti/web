<?php

/**
 * @var CarteItem[] $items
 * @var CarteItem $carte
 * @var object $filters
 * @var string $pagination
 * @var array $pag
 */
?>

<?php $this->load->view('/admin/biblioteca/carti/_partials/filters') ?>

<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-table"></i> Listă cărţi</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <div class="x_content">

                <div class="table-responsive">

                    <table class="table table-striped table-bordered table-hover table-highlight dataTable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th class="hidden-xs">Coperta</th>
                                <th>Autor</th>
                                <th>Titlu carte</th>
                                <th>Categorie</th>
                                <th class="hidden-xs">Editură / <br />Localitate</th>
                                <th class="hidden-xs">Anul publicării</th>
                                <th class="hidden-xs">Necompletat</th>
                                <th class="hidden-xs">Disponibil în format</th>
                                <th style="width: 1px">
                                    <div class="hidden-sm hidden-xs">
                                        <a href="/biblioteca/carti/edit/" class="btn btn-info btn-sm" role="button">
                                            <i class="fa fa-plus"></i> Add
                                        </a>
                                    </div>
                                    <div class="hidden-md hidden-lg">
                                        <a href="/biblioteca/carti/edit/" class="btn btn-info btn-sm" role="button">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </th>
                            </tr>
                        </thead>

                        <?php if (count($items)): ?>
                            <tbody>
                                <?php foreach ($items as $carte): ?>
                                    <tr>
                                        <td class="center"><?= html_escape($carte->getId()) ?></td>
                                        <td class="center hidden-xs"><?= html_escape($carte->getCoperta()) ?></td>
                                        <td><?= html_escape($carte->getNumeAutori()) ?></td>
                                        <td><?= html_escape($carte->getTitlu()) ?></td>
                                        <td><?= html_escape($carte->getCategorie()->getNume()) ?></td>
                                        <td class="hidden-xs">
                                            <?= html_escape($carte->getEdituraNume()) ?>
                                            <?= !empty($carte->getEdituraNume()) ? ' / <br />' : '' ?>
                                            <?= html_escape($carte->getLocalitateOld()) ?>
                                        </td>
                                        <td class="hidden-xs"><?= html_escape($carte->getAnulPublicatiei()) ?></td>

                                        <td>
                                            <span class="label <?= $carte->getDimensiune() ? 'label-success' : 'label-default' ?>">dimensiune</span>
                                            <span class="label <?= $carte->getIntroducereArticol() ? 'label-success' : 'label-default' ?>">introducere</span>
                                            <span class="label <?= $carte->getRezumatArticol() ? 'label-success' : 'label-default' ?>">rezumat</span>
                                            <span class="label <?= $carte->getRecenzieArticol() ? 'label-success' : 'label-default' ?>">recenzie</span>
                                            <!-- <span class="label label-default">doc scanat</span> -->
                                        </td>

                                        <td class="hidden-xs text-center"><?= html_escape($carte->getFormatName()) ?></td>
                                        <td style="white-space: nowrap">
                                            <div class="hidden-sm hidden-xs">
                                                <a class="btn btn-sm btn-primary"
                                                    href="/biblioteca/carti/edit/<?= (int)$carte->getId() ?>">
                                                    <i class="fa fa-edit bigger-110"></i> <?= html_escape('Edit') ?>
                                                </a>
                                            </div>
                                            <div class="hidden-md hidden-lg">
                                                <a href="/biblioteca/carti/edit/<?= (int)$carte->getId() ?>"
                                                    class="btn btn-sm btn-primary" title="<?= html_escape('Edit') ?>">
                                                    <i class="fa fa-edit bigger-110"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        <?php else: ?>
                            <tbody>
                                <tr>
                                    <td colspan="50" class="nodata"><?= html_escape('No data found') ?></td>
                                </tr>
                            </tbody>
                        <?php endif; ?>
                        <tfoot>
                            <tr>
                                <th colspan="50" class="panel-footer">
                                    <div style="float: left">
                                        <?= $this->load->view('/_partial/table/paginator', $pag, true) ?>
                                    </div>
                                    <div style="float: right">
                                        <?= $pagination ?>
                                    </div>
                                </th>
                            </tr>
                        </tfoot>
                    </table>

                </div>

            </div>
        </div>

    </div>
</div>