<?php

/**
 * @var CarteItem $carte
 */

?>

<pre><?php var_dump($carte) ?></pre>

<style type="text/css">
    .img-thumbnail img {
        width: 100%;
    }

    .img-thumbnail {
        margin-bottom: 10px;
    }
</style>




<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Cărți <small>adaugă carte</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/" class="fa fa-dashboard"> Dashboard</a></li>
            <li><a href="/carti" class="fa fa-book"> Cărți</a></li>
            <li class="active"><?php echo ($edit) ? 'Editează' : 'Adaugă'; ?> carte <?php echo ($edit) ? '' : 'nouă'; ?></li>
        </ol>
    </div>
</div>
<!-- /.row -->

<!-- Validation message -->
<?php if (isset($log)) { ?>
    <div class="alert alert-<?php echo $log['type']; ?>">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $log['message']; ?>
        <?php echo validation_errors(); ?>
    </div>
<?php } ?>

<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li class="active"><a href="#informatii" role="tab" data-toggle="tab">1. Informații generale</a></li>
    <li><a href="#detalii" role="tab" data-toggle="tab">2. Detalii carte</a></li>
    <li><a href="#cuvinte" role="tab" data-toggle="tab">3. Cuvinte cheie</a></li>
    <li><a href="#electronic" role="tab" data-toggle="tab">4. Format electronic</a></li>
</ul>

<!-- Tab panes -->
<form name="adauga-carte" method="post" action="" class="">
    <input type="hidden" name="tokenID" id="token" value="<?php echo rand(1, 999); ?>" />
    <div class="tab-content">
        <div class="tab-pane active" id="informatii">

            <div class="col-lg-5">
                <div class="form-group">
                    <label>Titlu carte*</label>
                    <input type="text" id="bName" name="bName" style="color:#428bca;font-weight:bold;" class="form-control" value="<?php echo set_value('bName', (($edit) ? $bookInfo['bName'] : '')); ?>" />
                </div>

                <div class="form-group">
                    <label>Subtitlu</label>
                    <input type="text" id="bNameVarianta" name="bNameVarianta" class="form-control" value="<?php echo set_value('bNameVarianta', (($edit) ? $bookInfo['bNameVarianta'] : '')); ?>" />
                </div>

                <div class="form-group">
                    <label>Categorie</label>
                    <select name="bCategoryID" class="form-control select2 allowClear">
                        <option></option>
                        <?php foreach ($list_carti_categorii as $group) { ?>
                            <optgroup label="<?php echo $group['name']; ?>">
                                <?php echo_select_options($group['options'], set_value('bCategoryID', $bookInfo['bCategoryID']), array('bcID', 'bcName')); ?>
                            </optgroup>
                        <?php } ?>
                    </select>
                </div>

                <div class="form-group">
                    <label>Responsabili de carte</label>
                    <label style="float:right">Rol responsabil de carte</label>
                    <br />
                    <?php
                    if (isset($bookInfo)) {
                        $k = 0;
                        foreach ($bookInfo['autori'] as $k => $autor) { ?>
                            <div class="row" style="margin:5px 0px;">
                                <select class="select2 form-control inlineb allowClear" name="autoriList[]" data-placeholder="selecteaza" style="width:60%;float:left;">
                                    <option></option>
                                    <?php echo_select_options($list_autori, $autor['a2bAuthorID'], array('aID', 'aName')); ?>
                                </select>
                                <select class="select2 form-control inlineb allowClear" name="autoriListRoluri[]" data-placeholder="selecteaza" style="width:30%;float:left;margin-left:20px;">
                                    <option></option>
                                    <?php echo_select_options($list_roluri_autori, $autor['a2bRolID'], array('_key', '_value')); ?>
                                </select><br />
                            </div>
                            <?php }

                        if ($k < 8) {
                            for ($i = $k; $i < 8 - $k; $i++) { ?>
                                <div class="row" style="margin:5px 0px">
                                    <select class="select2 form-control inlineb allowClear" name="autoriList[]" data-placeholder="selecteaza" style="width:60%;float:left;">
                                        <option></option>
                                        <?php echo_select_options($list_autori, false, array('aID', 'aName')); ?>
                                    </select>
                                    <select class="select2 form-control inlineb allowClear" name="autoriListRoluri[]" data-placeholder="selecteaza" style="width:30%;float:left;margin-left:20px;">
                                        <option></option>
                                        <?php echo_select_options($list_roluri_autori, false, array('_key', '_value')); ?>
                                    </select><br />
                                </div>
                            <?php }
                        }
                    } else {
                        for ($i = 0; $i < 8; $i++) { ?>
                            <div class="row" style="margin:5px 0px">
                                <span style="float:left;margin-right:5px;"><?php echo $i + 1; ?></span>
                                <select class="select2 form-control inlineb allowClear" name="autoriList[]" data-placeholder="selecteaza" style="width:60%;float:left;">
                                    <option></option>
                                    <?php echo_select_options($list_autori, false, array('aID', 'aName')); ?>
                                </select>
                                <select class="select2 form-control inlineb allowClear" name="autoriListRoluri[]" data-placeholder="selecteaza" style="width:30%;margin-left:20px;float:left;">
                                    <option></option>
                                    <?php echo_select_options($list_roluri_autori, false, array('_key', '_value')); ?>
                                </select><br />
                            </div>
                    <?php }
                    } ?>
                </div>

                <div class="form-group">
                    <label>Editura</label>
                    <select class="form-control select2 allowClear" name="bPublishingID" data-placeholder="selecteaza">
                        <option></option>
                        <?php echo_select_options($list_edituri, set_value('bPublishingID', $bookInfo['bPublishingID']), array('pID', 'pName')); ?>
                    </select>
                </div>

                <div class="input-group mb5">
                    <span class="input-group-addon" style="width:100px">Oraș</span>
                    <input type="text" id="bLocality" name="bLocality" class="form-control" value="<?php echo set_value('bLocality', (($edit) ? $bookInfo['bLocality'] : '')); ?>" />
                </div>

                <div class="input-group mb5">
                    <span class="input-group-addon" style="width:100px">Ediția</span>
                    <input type="text" id="bEdition" name="bEdition" class="form-control" value="<?php echo set_value('bEdition', (($edit) ? $bookInfo['bEdition'] : '')); ?>" />
                </div>

                <div class="input-group mb5">
                    <span class="input-group-addon" style="width:100px">Anul</span>
                    <input type="text" id="bYearPublication" name="bYearPublication" class="form-control" value="<?php echo set_value('bYearPublication', (($edit) ? $bookInfo['bYearPublication'] : '')); ?>" />
                </div>

                <div class="input-group mb5">
                    <span class="input-group-addon" style="width:100px">Nr Pagini</span>
                    <input type="text" id="bPages" name="bPages" class="form-control" value="<?php echo set_value('bPages', (($edit) ? $bookInfo['bPages'] : '')); ?>" />
                </div>

                <div class="input-group mb5">
                    <span class="input-group-addon" style="width:100px">Nr Anexe</span>
                    <input type="text" id="bNrAnexe" name="bNrAnexe" class="form-control" value="<?php echo set_value('bNrAnexe', (($edit) ? $bookInfo['bNrAnexe'] : '')); ?>" />
                </div>

                <div class="input-group mb5">
                    <span class="input-group-addon" style="width:100px">Dimensiuni</span>
                    <input type="text" id="bDimensions" name="bDimensions" class="form-control" placeholder="13 x 20" value="<?php echo set_value('bDimensions', (($edit) ? $bookInfo['bDimensions'] : '')); ?>" />
                </div>

                <div class="form-group">
                    <label>Disponibilitate</label>
                    <select class="form-control select2 allowClear" name="bAvailable" data-placeholder="selecteaza">
                        <option></option>
                        <?php echo_select_options($carti_disponibilitate, set_value('bAvailable', $bookInfo['bAvailable']), array('_key', '_value')); ?>
                    </select>
                </div>

                <div class="form-group">
                    <label>Observații</label>
                    <textarea id="bObservations" name="bObservations" class="inlineb form-control" style="height:100px"><?php echo set_value('bObservations', (($edit) ? $bookInfo['bObservations'] : '')); ?></textarea>
                </div>

                <div class="form-group">
                    <input type="submit" value="Salveaza" name="save" class="btn btn-primary" />
                </div>
            </div>

            <div class="col-lg-2">
                <label>Copertă</label><br />
                <div class="img-thumbnail">
                    <?php $coperta = (isset($bookInfo)) ? (getCoperta($this->config->item('uploads_carti_path') . $bookInfo['pAlias'], $bookInfo['bPhoto'])) : getCoperta(); ?>
                    <img src="<?php echo $coperta; ?>" class="poza_coperta" />
                </div>
                <div class="form-group">
                    <input type="file" class="form-control" name="upload_coperta" id="upload_coperta" />
                    <?php if (isset($bookInfo) && $bookInfo['bPhoto']) { ?>
                        <a href="javascript:void(0)" class="remove_poza btn btn-danger" data-id="<?php echo $bookInfo['bID']; ?>">Șterge</a>
                    <?php } ?>
                </div>
            </div>

        </div>


        <!-- TAB 2 - Detalii carte -->
        <div class="tab-pane" id="detalii">
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Introducere/ Prefață</label>
                    <textarea id="bIntroduction" name="bIntroduction" class="tinymce form-control"><?php echo set_value('bIntroduction', (($edit) ? $bookInfo['bIntroduction'] : '')); ?></textarea>
                </div>

                <div class="form-group">
                    <label>Cuprins</label>
                    <textarea id="bSummary" name="bSummary" class="tinymce form-control"><?php echo set_value('bSummary', (($edit) ? $bookInfo['bSummary'] : '')); ?></textarea>
                </div>

                <div class="form-group">
                    <input type="submit" value="Salveaza" name="save" class="btn btn-primary" />
                </div>
            </div>
        </div>
        <!-- end TAB 2 - Detalii carte -->

        <!-- TAB 3 - Cuvinte cheie -->
        <div class="tab-pane" id="cuvinte">
            <div class="logs"></div>

            <?php if (isset($bookInfo)) { ?>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-plus fa-fw"></i> Adaugă Antroponim</h3>
                            </div>
                            <div class="panel-body">
                                <label>Selectează cuvântul cheie</label>
                                <select name="selAntroponim" class="form-control select2 allowClear">
                                    <option></option>
                                    <?php echo_select_options($list_toate_tagurile['antroponime'], false, array('_key', '_value')); ?>
                                </select><br>

                                <label>Paginile</label>
                                <textarea cols="2" class="form-control"></textarea>
                                <br>
                                <button type="button" class="btn btn-primary add_tag" data-tip="1">Salvează</button>
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-4">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-plus fa-fw"></i> Adaugă Toponim</h3>
                            </div>
                            <div class="panel-body">
                                <label>Selectează cuvântul cheie</label>
                                <select name="selToponim" class="form-control select2 allowClear">
                                    <option></option>
                                    <?php echo_select_options($list_toate_tagurile['toponime'], false, array('_key', '_value')); ?>
                                </select><br>

                                <label>Paginile</label>
                                <textarea cols="2" rows="" class="form-control"></textarea>
                                <br>
                                <button type="button" class="btn btn-primary add_tag" data-tip="2">Salvează</button>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-plus fa-fw"></i> Adaugă Eveniment</h3>
                            </div>
                            <div class="panel-body">
                                <label>Selectează cuvântul cheie</label>
                                <select name="selEveniment" class="form-control select2 allowClear">
                                    <option></option>
                                    <?php echo_select_options($list_toate_tagurile['evenimente'], false, array('_key', '_value')); ?>
                                </select><br>

                                <label>Paginile</label>
                                <textarea cols="2" rows="" class="form-control"></textarea>
                                <br>
                                <button type="button" class="btn btn-primary">Salvează</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-list fa-fw"></i> Listă Antroponime</h3>
                            </div>
                            <div class="panel-body p0">
                                <?php if (isset($list_carte_taguri['antroponime'])) { ?>
                                    <table id="tabel_antroponime" class="table table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th>Cuvânt cheie</th>
                                                <th>Pagini</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($list_carte_taguri['antroponime'] as $item) { ?>
                                                <tr>
                                                    <td><?php echo $item['tagName']; ?></td>
                                                    <td><?php echo $item['t2bPagination']; ?></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                <?php } else { ?>
                                    nu există nici un antroponim
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-list fa-fw"></i> Listă Toponime</h3>
                            </div>
                            <div class="panel-body p0">
                                <?php if (isset($list_carte_taguri['toponime'])) { ?>
                                    <table id="tabel_toponime" class="table table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th>Cuvânt cheie</th>
                                                <th>Pagini</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($list_carte_taguri['toponime'] as $item) { ?>
                                                <tr>
                                                    <td><?php echo $item['tagName']; ?></td>
                                                    <td><?php echo $item['t2bPagination']; ?></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                <?php } else { ?>
                                    nu există nici un antroponim
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-list fa-fw"></i> Listă Evenimente</h3>
                            </div>
                            <div class="panel-body p0">
                                <?php if (isset($list_carte_taguri['evenimente'])) { ?>
                                    <table id="tabel_evenimente" class="table table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th>Cuvânt cheie</th>
                                                <th>Pagini</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($list_carte_taguri['evenimente'] as $item) { ?>
                                                <tr>
                                                    <td><?php echo $item['tagName']; ?></td>
                                                    <td><?php echo $item['t2bPagination']; ?></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                <?php } else { ?>
                                    nu există nici un eveniment
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <div class="alert alert-warning" role="alert">Acest formular se activează doar la editarea cărții.</div>
            <?php } ?>
        </div>

        <!-- TAB 4 - format electronic -->
        <div class="tab-pane" id="electronic">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="bDocScan" class="inlineb">Nume fisier scanat doc/pdf <small title="cartea in format electronic - trebuie pusa pe FTP">(?)</small></label>
                    <input type="file" class="form-control" name="upload_document" id="upload_document" />
                </div>

                <div class="form-group doc_preview">
                    <?php if (isset($bookInfo['bDocScan'])) { ?>
                        <iframe src="<?php echo getEBook($this->config->item('uploads_carti_el_path') . $bookInfo['pAlias'], $bookInfo['bDocScan']); ?>" height="850" width="800"></iframe>
                    <?php } ?>
                </div>

                <div class="form-group">
                    <label>Link sursă online</label>
                    <input type="text" name="" class="form-control" />
                </div>

                <div class="form-group">
                    <label>Titlu sursă online</label>
                    <input type="text" name="" class="form-control" />
                </div>

                <div class="form-group">
                    <input type="submit" value="Salveaza" name="save" class="btn btn-primary" />
                </div>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        $('#tabel_antroponime, #tabel_toponime, #tabel_evenimente').dataTable({
            "iDisplayLength": 50,
            "order": [
                [0, "asc"]
            ],
            "dom": '<"top"f>rt<"bottom"ip><"clear">'
        });

        $('.allowClear').select2({
            placeholder: "selecteaza",
            allowClear: true
        });


        <?php if (isset($bookInfo)) { ?>
            $('body').on('click', '.add_tag', function() {
                var btn = $(this);

                $.ajax({
                    url: '/carti/adaugaTag2Carte',
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        'bID': '<?php echo $bookInfo['bID']; ?>',
                        'id': $(btn).parents('.panel-body').find('.select2').select2('val'),
                        'pag': $(btn).parents('.panel-body').find('textarea').val()
                    },
                    success: function(data) {
                        $('.logs').append('<div class="alert alert-success">Cuvântul cheie a fost adăugat cu succes.</div>');

                        setTimeout(function() {
                            $('.logs').find('.alert').remove();
                        }, 2000);
                    }
                })
            })
        <?php } ?>

        $("#upload_coperta").uploadify({
            buttonClass: 'upload_button',
            multi: true,
            fileSizeLimit: '50000KB',
            height: 30,
            width: 160,
            buttonText: 'Adaugă copertă',
            swf: '/admin/useful/uploadify/uploadify.swf',
            uploader: '/admin/uploadFiles/uploadCoperta/' + $('#token').val(),
            fileTypeExts: '*.gif; *.jpeg; *.jpg; *.png',
            onUploadSuccess: function(file, data, response) {
                var resp = JSON.parse(data);

                if (resp.error) {
                    alert(resp.error);
                } else {
                    $('.poza_coperta').attr('src', '/<?php echo $this->config->item("uploads_carti_path"); ?>/_tmp/' + resp.filename + resp.file_ext);
                }
            }
        });

        $("#upload_document").uploadify({
            buttonClass: 'upload_button',
            multi: true,
            fileSizeLimit: '400KB',
            height: 30,
            width: 160,
            buttonText: 'Adaugă document',
            swf: '/admin/useful/uploadify/uploadify.swf',
            uploader: '/admin/uploadFiles/uploadDocument/' + $('#token').val(),
            fileTypeExts: '*.pdf',
            onUploadSuccess: function(file, data, response) {
                var resp = JSON.parse(data);

                if (resp.error) {
                    alert(resp.error);
                } else {
                    $('.doc_preview')
                        .append('<iframe src="/<?php echo $this->config->item("uploads_carti_el_path"); ?>/_tmp/' + resp.filename + resp.file_ext + '" width="700" height="600"></iframe>');
                }
            }
        });

    })
</script>