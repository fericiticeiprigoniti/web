<?php

/**
 * @var DimensiuneItem $dimensiune
 * @var array $parentItems
 * @var string $page_title
 */
?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-edit"></i> <?= $page_title ?></h2>
                <div class="clear"></div>
            </div>

            <div class="x_content">

                <form method="post" class="form-horizontal ajaxForm" action="<?= General::url() ?>" data-redirect-url="/biblioteca/dimensiuni/">
                    <div class="row">

                        <?php if ($dimensiune->getId()): ?>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">ID</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" class="form-control" value="<?= $dimensiune->getId() ?>" readonly />
                                </div>
                            </div>
                        <?php endif; ?>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Latime <span class="req">*</span></label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control" name="latime" value="<?= html_escape($dimensiune->getLatime()) ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Înălțime <span class="req">*</span></label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control" name="inaltime" value="<?= html_escape($dimensiune->getInaltime()) ?>">
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="panel-footer">
                                <a href="/biblioteca/dimensiuni/" class="btn btn-default pull-left">
                                    <i class="fa fa-arrow-left"></i> Lista categorii
                                </a>
                                <button type="submit" class="btn btn-primary pull-right">
                                    <i class="fa fa-save"></i> Salvare
                                </button>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>