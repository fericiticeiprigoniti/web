<?php
/**
 * @var DimensiuneItem[] $items
 */
?>

<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-table"></i> 
                    Listă dimensiuni cărţi
                    <span class="badge"><?= count($items) ?></span>
                </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <div class="x_content">

                <div class="table-responsive">

                    <table class="table table-striped table-bordered table-hover table-highlight dataTable">
                        <thead>
                        <tr>
                            <th style="width:50px">ID</th>
                            <th>Latime</th>
                            <th>Înălțime</th>
                            <th class="actions">
                                <a href="/biblioteca/dimensiuni/add"
                                    class="btn btn-sm btn-info wide"
                                    title="<?= html_escape('Add') ?>">
                                    <i class="fa fa-plus"></i> <span class="hidden-sm hidden-xs"><?= html_escape('Add') ?></span>
                                </a>
                            </th>
                        </tr>
                        </thead>

                        <?php if (count($items)): ?>
                            <tbody>
                            <?php $i=0; foreach ($items as $v): ?>
                                <tr>
                                    <td class="center"><?= html_escape($v->getId()) ?></td>
                                    <td><?= html_escape($v->getLatime()) ?></td>
                                    <td><?= html_escape($v->getInaltime()) ?></td>
                                    <td class="actions" style="white-space: nowrap">
                                        <a href="/biblioteca/dimensiuni/edit/<?= (int)$v->getId() ?>"
                                            class="btn btn-sm btn-primary"
                                            title="<?= html_escape('Edit') ?>">
                                            <i class="fa fa-edit bigger-110"></i> <span class="hidden-sm hidden-xs"><?= html_escape('Edit') ?></span>
                                        </a>

                                        <a href="/biblioteca/dimensiuni/delete/<?= (int)$v->getId() ?>"
                                            class="btn btn-sm btn-danger"
                                            title="<?= html_escape('Sterge') ?>">
                                            <i class="fa fa-trash-o bigger-110"></i> <span class="hidden-sm hidden-xs"><?= html_escape('Șterge') ?></span>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        <?php else: ?>
                            <tbody>
                            <tr>
                                <td colspan="50" class="nodata"><?= html_escape('No data found') ?></td>
                            </tr>
                            </tbody>
                        <?php endif; ?>
                    </table>

                </div>

            </div>
        </div>

    </div>
</div>
