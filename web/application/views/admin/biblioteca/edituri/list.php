<?php
/**
 * @var EdituraItem[] $items
 */
?>

<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_title">
				<h2><i class="fa fa-table"></i> 
					<?= $page_title ?> 
					<span class="badge"><?= count($items) ?></span>
				</h2>
				<div class="clear"></div>
			</div>

            <div class="x_content">

                <div class="table-responsive">

                    <table class="table table-striped table-bordered table-hover table-highlight dataTable">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nume</th>
                            <th>Alias</th>
                            <th>Localitate</th>
                            <th class="actions">
                                <a href="/biblioteca/edituri/edit/"
                                    class="btn btn-info btn-sm" role="button">
                                    <i class="fa fa-plus"></i>
                                    <span class="hidden-sm hidden-xs"> Add </span>
                                </a>
                            </th>
                        </tr>
                        </thead>

                        <?php if (count($items)): ?>
                            <tbody>
                            <?php $i=0; foreach ($items as $v): ?>
                                <tr>
                                    <td class="center"><?= html_escape($v->getId()) ?></td>
                                    <td>
                                        <?= html_escape($v->getNume()) ?>

                                        <?php if($v->getWebsite()){?>
                                        <div class="gray font11">
											Website: <a href="<?= $v->getWebsite() ?>"><?= $v->getWebsite() ?></a>
										</div>
										<?php }?>
                                    </td>
                                    
                                    <td><?= html_escape($v->getAlias()) ?></td>
                                    <td>
                                        <?= $v->getLocalitateId()? html_escape($v->getLocalitate()->getNumeComplet()) : '' ?>
                                    </td>
                                    
                                    <td class="actions" style="white-space: nowrap">
                                        <a href="/biblioteca/edituri/edit/<?= (int)$v->getId() ?>"
                                            class="btn btn-sm btn-primary" title="Edit">
                                            <i class="fa fa-edit bigger-110"></i>
                                            <span class="hidden-sm hidden-xs"> Edit</span>
                                        </a>
                                        <a href="/biblioteca/edituri/delete/<?= (int)$v->getId() ?>"
                                            class="btn btn-sm btn-danger btnDelete"
                                            data-name="categoria <?= html_escape($v->getNume()) ?>" title="Delete">
                                            <i class="fa fa-trash-o bigger-110"></i>
                                            <span class="hidden-sm hidden-xs"> Delete</span>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        <?php else: ?>
                            <tbody>
                            <tr>
                                <td colspan="50" class="nodata"><?= html_escape('No data found') ?></td>
                            </tr>
                            </tbody>
                        <?php endif; ?>
                    </table>

                </div>

            </div>
        </div>

    </div>
</div>
