<?php

/**
 * @var EdituraItem $editura
 * @var array $parentItems
 * @var string $page_title
 */

?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-edit"></i> <?= $page_title ?></h2>
                <div class="clear"></div>
            </div>

            <div class="x_content">

                <form method="post" class="form-horizontal ajaxForm" action="<?= General::url() ?>" data-redirect-url="/biblioteca/edituri/">
                    <div class="col-md-6 col-md-offset-2">

                        <?php if ($editura->getId()): ?>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">ID</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" class="form-control" value="<?= $editura->getId() ?>" readonly />
                                </div>
                            </div>
                        <?php endif; ?>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nume <span class="req">*</span></label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control" name="nume" value="<?= html_escape($editura->getNume()) ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Alias</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control" name="alias" value="<?= html_escape($editura->getAlias()) ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Website</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control" name="website" value="<?= html_escape($editura->getWebsite()) ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Localitate</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="hidden" id="loc_id" name="loc_id" value="<?= html_escape($editura->getLocalitateId()) ?>" />
                                <input type="text" class="form-control devbridge-autocomplete" placeholder="Localitate"
                                    data-id="loc_id" data-url="/biblioteca/edituri/cautaLocalitate"
                                    value="<?= $editura->getLocalitateId() ? $editura->getLocalitate()->getNumeComplet() : '' ?>" />
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="panel-footer">
                            <a href="/biblioteca/edituri/" class="btn btn-default pull-left">
                                <i class="fa fa-arrow-left"></i> Lista edituri
                            </a>
                            <button type="submit" class="btn btn-primary pull-right">
                                <i class="fa fa-save"></i> Salvare
                            </button>
                            <div class="clear"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>