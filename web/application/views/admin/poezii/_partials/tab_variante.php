<!-- TAB - Variante -->
<?php
/**
 * @var PoezieItem $varObjPoezieSeleted
 */
?>
<form name="poezii_variante"
    method="post"
    action="<?= '/poezii/' . (($edit) ? 'edit/' . $edit : 'add') . '?tab=variante'
                . (get('varianta_noua') ? '&varianta_noua=true' : '')
                . (get('v_poezie_id') ? '&v_poezie_id=' . get('v_poezie_id') : '')
            ?>"
    class="formular formular_poezie form-horizontal form-label-left ajaxForm"
    <?= $edit ? '' : 'data-redirect-url="/poezii"' ?>>

    <div class="col-lg-8 col-md-8 col-sm-12">

        <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Titlu</label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                <input type="text" class="form-control" name="titlu" value="<?= set_value('titlu', Strings::fill($varObjPoezieSeleted->getTitlu())) ?>">
                <?php echo form_error('titlu'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Titlu varianta</label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                <input type="text" class="form-control" name="titlu_varianta" value="<?php echo set_value('titlu_varianta', Strings::fill($varObjPoezieSeleted->getTitluVarianta())) ?>">
                <?php echo form_error('titlu_varianta'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Conținut<span class="red">*</span></label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                <textarea id="continut_poezie" name="continut" class="tinymce form-control"><?= set_value('continut', $varObjPoezieSeleted->getContinut()) ?></textarea>
            </div>
        </div>

        <div class="alert alert-info">
            Detalii sursa
            <hr />
            <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Carte</label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <select name="sursa_doc_carte_id" class="form-control select2 allowClear" data-placeholder="selecteaza">
                        <option></option>
                        <?php echo_select_options($list_carti, set_value('sursa_doc_carte_id', Strings::fill($varObjPoezieSeleted->getSursaDocCarteId())), ['_key', '_value']) ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Publicatie</label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <select name="sursa_doc_publicatie_id" class="form-control select2 allowClear" data-placeholder="selecteaza">
                        <option></option>
                        <?php //echo_select_options($list_publicatii, set_value('sursa_doc_publicatie_id', Strings::fill($varObjPoezieSeleted->getSursaDocPublicatieId())));
                        ?>1
                    </select>
                </div>
            </div>


            <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Paginare</label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <input type="text" name="sursa_doc_start_page" class="form-control inlineb"
                        style="width:75px"
                        placeholder="inceput"
                        value="<?= set_value('sursa_doc_start_page', Strings::fill($varObjPoezieSeleted->getSursaDocStartPage())) ?>" />
                    -
                    <input type="text" name="sursa_doc_end_page"
                        class="form-control inlineb" style="width:75px"
                        placeholder="sfarsit"
                        value="<?= set_value('sursa_doc_end_page', Strings::fill($varObjPoezieSeleted->getSursaDocEndPage())) ?>" />
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Titlu (sursă online)</label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <input type="text" class="form-control" name="sursa_titlu" value="<?= set_value('sursa_titlu', Strings::fill($varObjPoezieSeleted->getSursaTitlu())) ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Link (sursă online)</label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <input type="text" class="form-control" name="sursa_link" value="<?= set_value('sursa_link', Strings::fill($varObjPoezieSeleted->getSursaLink())) ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Culegător</label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <input type="text" class="form-control" name="sursa_culegator" value="<?= set_value('sursa_culegator', Strings::fill($varObjPoezieSeleted->getSursaCulegator())) ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Observații</label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <textarea name="observatii" class="mce-tinymce tinymcesimple form-control"><?= set_value('observatii', $varObjPoezieSeleted->getObservatii()) ?></textarea>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-12">
        <div class="x_content">
            <table class="table_carti_personaj table jambo_table">
                <thead>
                    <tr>
                        <th width='20px'></th>
                        <th width='50px'>ID</th>
                        <th>Titlu</th>
                        <th width='50px' class="center p0">
                            <a href="/poezii/edit/<?= $poezie->getId() . '?tab=variante&varianta_noua=true' ?>" class="btn btn-info btn-xs" role="button" title="Afauga variantă nouă">
                                <i class="fa fa-plus"></i>
                            </a>
                        </th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    if (!empty($list_variante)) {
                        foreach ($list_variante as $vObjPoezie) { ?>
                            <tr data-id="<?= $vObjPoezie->getId() ?>" <?= (isset($varObjPoezieSeleted) && $varObjPoezieSeleted->getId() == $vObjPoezie->getId()) ? 'class="bg-light-gray"' : '' ?>>
                                <td>
                                    <?php if (isset($varObjPoezieSeleted) && $varObjPoezieSeleted->getId() == $vObjPoezie->getId()) { ?>
                                        <span class="fa fa-arrow-left green"></span>
                                    <?php } ?>
                                </td>
                                <td><?= $vObjPoezie->getId() ?></td>
                                <td>
                                    <?= $vObjPoezie->getTitlu() ?>
                                </td>

                                <td class="center">
                                    <a class="btn btn-xs btn-danger"
                                        href="/poezii/edit/<?= $poezie->getId() . '?tab=variante&v_poezie_id=' . $vObjPoezie->getId() ?>"
                                        title="Editeaza">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </td>
                            </tr>
                    <?php }
                    } ?>
                </tbody>
            </table>
        </div>

    </div>
</form>



<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {

    });
</script>