<?php

/**
 * @var PoezieItem $poezie
 * @var int $edit
 * @var array $list_autori
 * @var array $list_carti
 * @var array $list_useri
 * @var array $list_cicluri
 * @var array $list_taguri
 * @var array $list_perioade_creatie
 * @var array $list_specii_liric
 * @var array $list_specii_epic
 * @var array $list_structuri_strofa
 * @var array $list_rime
 * @var array $list_picior_metric
 * @var array $tagsSaved
 *
 */
?>
<form id="poezii_generale"
    name="poezii_generale"
    method="post"
    action="<?= '/poezii/' . (($edit) ? "edit/{$edit}" : "add") . '?tab=generale' ?>"
    class="formular formular_poezie form-horizontal form-label-left ajaxForm"
    <?= $edit ? '' : 'data-redirect-url="/poezii"' ?>>

    <div class="col-lg-9 col-md-9 col-sm-12">

        <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Titlu</label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                <input type="text" class="form-control" name="titlu" value="<?= set_value('titlu', Strings::fill($poezie->getTitlu())) ?>">
                <?php echo form_error('titlu'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Titlu varianta</label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                <input type="text" class="form-control" name="titlu_varianta" value="<?php echo set_value('titlu_varianta', Strings::fill($poezie->getTitluVarianta())) ?>">
                <?php echo form_error('titlu_varianta'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Autor</label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                <select name="personaj_id" class="form-control select2 allowClear" data-placeholder="- alege un autor -">
                    <option></option>
                    <?php echo_select_options($list_autori, set_value('personaj_id', Strings::fill($poezie->getPersonajId())), ['id', 'nume_complet']) ?>
                </select>

                <?= form_error('personaj_id') ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Conținut<span class="red">*</span></label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                <textarea id="continut_poezie" name="continut" class="tinymce form-control"><?= set_value('continut', $poezie->getContinut()) ?></textarea>
            </div>
        </div>

        <div class="alert alert-info">
            Detalii sursa
            <hr />
            <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Carte</label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <select name="sursa_doc_carte_id" class="form-control select2 allowClear" data-placeholder="selecteaza">
                        <option></option>
                        <?php echo_select_options($list_carti, set_value('sursa_doc_carte_id', Strings::fill($poezie->getSursaDocCarteId())), ['_key', '_value']) ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Publicatie</label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <select name="sursa_doc_publicatie_id" class="form-control select2 allowClear" data-placeholder="selecteaza">
                        <option></option>
                        <?php //echo_select_options($list_publicatii, set_value('sursa_doc_publicatie_id', Strings::fill($poezie->getSursaDocPublicatieId())));
                        ?>1
                    </select>
                </div>
            </div>


            <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Paginare</label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <input type="text" name="sursa_doc_start_page" class="form-control inlineb"
                        style="width:75px"
                        placeholder="inceput"
                        value="<?= set_value('sursa_doc_start_page', Strings::fill($poezie->getSursaDocStartPage())) ?>" />
                    -
                    <input type="text" name="sursa_doc_end_page"
                        class="form-control inlineb" style="width:75px"
                        placeholder="sfarsit"
                        value="<?= set_value('sursa_doc_end_page', Strings::fill($poezie->getSursaDocEndPage())) ?>" />
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Titlu (sursă online)</label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <input type="text" class="form-control" name="sursa_titlu" value="<?= set_value('sursa_titlu', Strings::fill($poezie->getSursaTitlu())) ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Link (sursă online)</label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <input type="text" class="form-control" name="sursa_link" value="<?= set_value('sursa_link', Strings::fill($poezie->getSursaLink())) ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Culegător</label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <input type="text" class="form-control" name="sursa_culegator" value="<?= set_value('sursa_culegator', Strings::fill($poezie->getSursaCulegator())) ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Observații</label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <textarea name="observatii" class="mce-tinymce tinymcesimple form-control"><?= set_value('observatii', $poezie->getObservatii()) ?></textarea>
                </div>
            </div>
        </div>
    </div>

    <!-- right COLUMN -->
    <div class="col-lg-3 col-md-3 col-sm-12">
        <div class="alert alert-info2">

            <div class="form-group p0">
                <div class="col-lg-6 pl0">
                    <label>Publicat</label>
                    <select name="is_published" class="form-control select2">
                        <option value="1" <?php echo ($poezie->isPublished() == 1) ? 'selected' : '' ?>>da</option>
                        <option value="0" <?php echo ($poezie->isPublished() == 0) ? 'selected' : '' ?>>nu</option>
                    </select>
                    <?php echo form_error('status_id') ?>
                </div>

                <div class="col-lg-6 pr0">
                    <label>Sters</label>
                    <select name="is_deleted" class="form-control select2">
                        <option value="1" <?= ($poezie->isDeleted() == 1) ? 'selected' : '' ?>>da</option>
                        <option value="0" <?= ($poezie->isDeleted() == 0) ? 'selected' : '' ?>>nu</option>
                    </select>
                    <?= form_error('is_deleted') ?>
                </div>
            </div>

            <div class="form-group p0">
                <div class="col-lg-6 pl0">
                    <label>Data publicare</label>
                    <input type="text" class="form-control datepicker"
                        placeholder="zi/luna/an"
                        data-inputmask="'mask': '99/99/9999'"
                        name="data_publicare"
                        value="<?php set_value('data_publicare', Calendar::convertDateMysql2Ro($poezie->getDataPublicare())) ?>">
                    <?= form_error('data_publicare'); ?>
                </div>

                <div class="col-lg-6 pr0">
                    <label>Order ID</label>
                    <input type="number" name="order_id" class="form-control" value="<?= set_value('order_id', Strings::fill($poezie->getOrderId())) ?>" />
                </div>
            </div>

        </div>


        <!-- Detalii semantica -->

        <div class="alert alert-info2">
            <div class="form-group">
                <label>Conținut scurt<span class="red">*</span></label>

                <div class="col-md-10 col-sm-10 col-xs-12">
                    <textarea name="continut_scurt" class="tinymcesimple form-control"><?= set_value('continut_scurt', $poezie->getContinutScurt()) ?></textarea>
                </div>
            </div>

            <div class="form-group">
                <label>Ciclul poeziei</label>

                <select name="ciclu_id" class="form-control select2 allowClear" data-placeholder="selecteaza">
                    <option></option>
                    <?php echo_select_options($list_cicluri, set_value('ciclu_id', Strings::fill($poezie->getCicluId())), ['_key', '_value']); ?>
                </select>
            </div>

            <div class="form-group">
                <label>Subiectul poeziei</label>
                <select name="subiect_id" class="form-control select2 allowClear" data-placeholder="selecteaza">
                    <option></option>
                    <?php echo_select_options($list_taguri, set_value('subiect_id', Strings::fill($poezie->getSubjectId())), ['_key', '_value']) ?>
                </select>
            </div>

            <div class="form-group">
                <label>Perioada creației</label>
                <select name="perioada_creatiei_id" class="form-control select2 allowClear" data-placeholder="selecteaza">
                    <option></option>
                    <?php echo_select_options($list_perioade_creatie, set_value('perioada_creatiei_id', Strings::fill($poezie->getPerioadaCreatieiId())), ['_key', '_value']) ?>
                </select>
            </div>

            <div class="form-group">
                <label for="data_creatiei" class="inlineb">Data creației</label>
                <textarea id="data_creatiei" name="data_creatiei" style="height:80px;" class="form-control"><?= set_value('data_creatiei', Strings::fill($poezie->getDataCreatiei())) ?></textarea>
            </div>

            <div class="form-group">
                <label for="locul_creatiei" class="inlineb">Locul creației</label>
                <textarea id="locul_creatiei" name="locul_creatiei" style="height:80px;" class="form-control"><?= set_value('locul_creatiei', Strings::fill($poezie->getLoculCreatiei())) ?></textarea>
            </div>

            <div class="form-group">
                <label>Specia poeziei</label>
                <select name="specie_id" class="form-control select2 allowClear" data-placeholder="selecteaza">
                    <option></option>
                    <optgroup label="Genul liric">
                        <?php echo_select_options($list_specii_liric, set_value('specie_id', Strings::fill($poezie->getSpecieId())), ['_key', '_value']) ?>
                    </optgroup>
                    <optgroup label="Genul epic">
                        <?php echo_select_options($list_specii_epic, set_value('specie_id', Strings::fill($poezie->getSpecieId())), ['_key', '_value']) ?>
                    </optgroup>

                </select>
            </div>

            <div class="form-group">
                <label>Structură strofă</label>
                <select name="structura_strofa_id" class="form-control select2 allowClear" data-placeholder="selecteaza">
                    <option></option>
                    <?php echo_select_options($list_structuri_strofa, set_value('structura_strofa_id', Strings::fill($poezie->getStructuraStrofaId())), ['_key', '_value']) ?>
                </select>
            </div>

            <div class="form-group">
                <label>Rimă</label>
                <select name="rima_id" class="form-control select2 allowClear" data-placeholder="selecteaza">
                    <option></option>
                    <?php echo_select_options($list_rime, set_value('rima_id', Strings::fill($poezie->getRimaId())), ['_key', '_value']); ?>
                </select>
            </div>

            <div class="form-group">
                <label>Picior metric</label>
                <select name="picior_metric_id" class="form-control select2 allowClear" data-placeholder="selecteaza">
                    <option></option>
                    <?php echo_select_options($list_picior_metric, set_value('picior_metric_id', Strings::fill($poezie->getPiciorMetricId())), ['_key', '_value']) ?>
                </select>
            </div>

            <div class="form-group">
                <label for="nr_strofe">Nr strofe</label>
                <input type="text" id="nr_strofe" name="nr_strofe" class="form-control" value="<?= set_value('nr_strofe', Strings::fill($poezie->getNrStrofe())) ?>" />
            </div>

            <div class="form-group">
                <label for="cuvinteCheie">Cuvinte cheie</label>

                <select name="cuvinteCheie[]" class="tags2" multiple>
                    <?php foreach ($list_taguri as $key => $item) { ?>
                        <option value="<?= $key ?>" <?= in_array($key, $tagsSaved) ? 'selected="selected"' : '' ?>><?= $item ?></option>
                    <?php } ?>
                </select>

            </div>
        </div>

    </div>
    <!-- end right COLUMN -->

</form>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {

        $('.tags2').select2({
            tags: true,
            //multiple: true,
            // tokenSeparators: [',', ' '],
            // minimumInputLength: 2,
            // minimumResultsForSearch: 10,
            // ajax: {
            //     url: '/admin/taguri/getTags',
            //     dataType: "json",
            //     type: "GET",
            //     data: function (params) {
            //
            //         var queryParameters = {
            //             term: params.term
            //         }
            //         return queryParameters;
            //     },
            //     processResults: function (data) {
            //         return {
            //             results: $.map(data, function (item) {
            //                 return {
            //                     text: item.tag_value,
            //                     id: item.tag_id
            //                 }
            //             })
            //         };
            //     }
            // }
        });

        // var data = {
        //    id: 1,
        //    text: 'Barn owl'
        // };
        ////var data = JSON.parse('<? //= $tagsSaved 
                                    ?>//');
        //console.log(data);
        //
        //// select default
        // var newOption = new Option(data.text, data.id, false, true);
        // console.log(newOption);
        // $('.tags2').append(newOption).trigger('change');
        // $('.tags2').append(newOption).trigger('change');

        // $('.tags2').trigger('change');

    });
</script>