<?php

/**
 * @var PoezieItem $poezie
 * @var int|null $edit
 */
?>


<form name=""
    method="post"
    action="<?= '/poezii/' . ($edit ? 'edit/' . $edit : 'add') . '?tab=comentariu' ?>"
    class="formular formular_poezie form-horizontal form-label-left ajaxForm">

    <div class="col-lg-9 col-md-9 col-sm-12">

        <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Comentariu</label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                <textarea name="comentariu" class="tinymce form-control"><?= set_value('comentariu', $poezie->getComentariu() ? $poezie->getComentariu()->getContinut() : '') ?></textarea>
            </div>

        </div>

        <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Autor</label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                <select name="comentariu_user_id" class="form-control select2 allowClear" data-placeholder="selecteaza">
                    <option></option>
                    <?php echo_select_options($list_useri, set_value('comentariu_user_id', ($poezie->getComentariu() ? Strings::fill($poezie->getComentariu()->getUserId()) : false)), ['_key', '_value']); ?>
                </select>
                <?= form_error('comentariu_user_id') ?>
            </div>
        </div>
    </div>
</form>