<?php

/**
 * @var PoezieFilters $filters
 * @var object $order
 */

// Set order items.
$orderByItems = $filters::fetchOrderItems();
$selectedOrderItems = $filters->getOrderBy();

$tagList = TagTable::getInstance()->fetchForSelect();
$creationPeriodList = PerioadaCreatieTable::getInstance()->fetchForSelect();
$speciesList = SpeciePoezieTable::getInstance()->fetchForSelect();

?>
<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">

            <div class="x_title">
                <h2><i class="fa fa-filter"></i> Filtre</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link colapsed"><i class="fa fa-chevron-down"></i></a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <div class="x_content" style="display: none;">
                <form class="filter-form form-horizontal form-label-left" action="<?= current_url() ?>" method="get">
                    <input type="hidden" name="_page" value="1" />
                    <input type="hidden" name="_per_page" value="<?= get('_per_page') ?>" />


                    <div class="row">
                        <div class="col-sm-12 space-10"></div>
                    </div>
                    <div class="row">

                        <!-- Column 1 -->
                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group has-feedback">
                                <input type="hidden" id="personajId" name="personajId"
                                    value="<?= html_escape($filters->getPersonajId()) ?>" />
                                <input type="text" class="form-control devbridge-autocomplete" placeholder="Autor"
                                    data-id="personajId" data-url="/poezii/cautaAutor"
                                    value="<?= $filters->getPersonaj() ? html_escape($filters->getPersonaj()->getFullName()) : null ?>" />
                                <span class="fa fa-search form-control-feedback right" aria-hidden="true"></span>
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Titlu"
                                    id="titleLike" name="titleLike"
                                    value="<?= html_escape($filters->getTitleLike()) ?>" />
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Continut"
                                    id="contentLike" name="contentLike"
                                    value="<?= html_escape($filters->getContentLike()) ?>" />
                            </div>

                            <div class="form-group">
                                <select class="form-control allowClear" name="subjectId" data-placeholder="Subiect">
                                    <option></option>
                                    <?php foreach ($tagList as $oValue => $oName): ?>
                                        <?php $selected = ($oValue == $filters->getSubjectId() ? 'selected="selected"' : '') ?>
                                        <option <?= $selected ?> value="<?= html_escape($oValue) ?>"><?= html_escape($oName) ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                        </div>

                        <!-- Column 2 -->
                        <div class="col-sm-4 col-xs-12">

                            <div class="form-group">
                                <select class="form-control allowClear" name="perioadaCreatieiId" data-placeholder="Perioada creatiei">
                                    <option></option>
                                    <?php foreach ($creationPeriodList as $oValue => $oName): ?>
                                        <?php $selected = ($oValue == $filters->getPerioadaCreatieiId() ? 'selected="selected"' : '') ?>
                                        <option <?= $selected ?> value="<?= html_escape($oValue) ?>"><?= html_escape($oName) ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <select class="form-control allowClear" name="specieId" data-placeholder="Specia poeziei">
                                    <option></option>
                                    <?php foreach ($speciesList as $oValue => $oName): ?>
                                        <?php $selected = ($oValue == $filters->getSpecieId() ? 'selected="selected"' : '') ?>
                                        <option <?= $selected ?> value="<?= html_escape($oValue) ?>"><?= html_escape($oName) ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Data creatiei"
                                    id="creationPeriodLike" name="creationPeriodLike"
                                    value="<?= html_escape($filters->getCreationPeriodLike()) ?>" />
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Locul creatiei"
                                    id="creationPlaceLike" name="creationPlaceLike"
                                    value="<?= html_escape($filters->getCreationPlaceLike()) ?>" />
                            </div>
                        </div>

                        <!-- Column 3 -->
                        <div class="col-sm-4 col-xs-12">

                            <div class="form-group has-feedback">
                                <select class="form-control" name="tagIdList[]" multiple data-placeholder="Cuvinte cheie">
                                    <option></option>
                                    <?php foreach ($tagList as $oValue => $oName): ?>
                                        <?php $selected = in_array($oValue, $filters->getTagIdList()) ? 'selected="selected"' : '' ?>
                                        <option <?= $selected ?> value="<?= html_escape($oValue) ?>"><?= html_escape($oName) ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="fa fa-sort form-control-feedback right" aria-hidden="true"></span>
                            </div>

                            <div class="form-group">
                                <select class="form-control allowClear" name="lyricsNo" data-placeholder="Nr strofe">
                                    <option></option>
                                    <?php foreach ($filters::fetchLyricsNoList() as $oValue => $oName): ?>
                                        <?php $selected = ($oValue == $filters->getLyricsNo() ? 'selected="selected"' : '') ?>
                                        <option <?= $selected ?> value="<?= html_escape($oValue) ?>"><?= html_escape($oName) ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <select class="form-control allowClear" name="alternativesNo" data-placeholder="Nr variante">
                                    <option></option>
                                    <?php foreach ($filters::fetchAlternativesNoList() as $oValue => $oName): ?>
                                        <?php $selected = ($oValue == $filters->getAlternativesNo() ? 'selected="selected"' : '') ?>
                                        <option <?= $selected ?> value="<?= html_escape($oValue) ?>"><?= html_escape($oName) ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <select class="form-control allowClear" name="isPublished" data-placeholder="Publicat">
                                    <option></option>
                                    <?php foreach ($filters::fetchPublishedList() as $oValue => $oName): ?>
                                        <?php $selected = ($oValue == $filters->isPublished() ? 'selected="selected"' : '') ?>
                                        <option <?= $selected ?> value="<?= html_escape($oValue) ?>"><?= html_escape($oName) ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <select class="form-control allowClear" name="isDeleted" data-placeholder="Sters">
                                    <option></option>
                                    <?php foreach ($filters::fetchDeletedList() as $oValue => $oName): ?>
                                        <?php $selected = ($oValue == $filters->isDeleted() ? 'selected="selected"' : '') ?>
                                        <option <?= $selected ?> value="<?= html_escape($oValue) ?>"><?= html_escape($oName) ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="ln_solid" style="margin-top: 10px"></div>

                    <div class="row">
                        <div class="form-group col-sm-6 col-xs-12"></div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group has-feedback">
                                <select class="form-control selectOrderBy" id="_orderBy" name="_orderBy[]"
                                    multiple data-placeholder="order by...">
                                    <option></option>
                                    <?php foreach ($orderByItems as $oValue => $oName): ?>
                                        <?php $selected = in_array($oValue, $selectedOrderItems) ? 'selected="selected"' : '' ?>
                                        <option <?= $selected ?> value="<?= html_escape($oValue) ?>"><?= html_escape($oName) ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="fa fa-sort form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group panel-footer" style="text-align: right">
                        <a href="<?= current_url() ?>" class="btn btn-warning">
                            <i class="fa fa-refresh"></i> Resetare
                        </a>
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-search"></i> Filtrare
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>