<?php

/**
 * @var PoezieItem $poezie
 * @var int|null $edit
 */
?>


<form name=""
    method="post"
    action="<?= '/poezii/' . ($edit ? 'edit/' . $edit : 'add') . '?tab=media' ?>"
    class="formular formular_poezie form-horizontal form-label-left ajaxForm">

    <div class="col-lg-9 col-md-9 col-sm-12">

        <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Titlu</label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                <input type="text" class="form-control" name="media_title" value="<?= set_value('titlu') ?>">
                <?php echo form_error('media_title'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Cod unic Youtube</label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                <input type="text" class="form-control" name="media_embeded_uniq_code" value="<?= set_value('embeded_uniq_code') ?>">
                <?php echo form_error('media_embeded_uniq_code'); ?>
            </div>
        </div>


        <?php if (count($poezie->getMedia())): ?>

            <hr>
            <h2>Videotecă</h2>

            <?php foreach ($poezie->getMedia() as $item): ?>

                <div class="form-group">
                    <label class="control-label col-md-2 col-sm-2 col-xs-12">Titlu</label>
                    <div class="col-md-10 col-sm-10 col-xs-12">
                        <input type="text" class="form-control" name="title" value="<?= set_value('titlu', $item->getTitlu()) ?>">
                        <?php echo form_error('titlu'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-2 col-sm-2 col-xs-12">Cod unic Youtube</label>
                    <div class="col-md-10 col-sm-10 col-xs-12">
                        <input type="text" class="form-control" name="embeded_uniq_code" value="<?= set_value('embeded_uniq_code', $item->getEmbededUniqCode()) ?>">
                        <?php echo form_error('embeded_uniq_code'); ?>
                    </div>
                </div>

            <?php endforeach; ?>
        <?php endif; ?>

    </div>
</form>