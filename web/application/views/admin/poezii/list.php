<?php

/**
 * @var PoezieItem[] $items
 * @var PoezieFilters $filters
 * @var object $order
 * @var string $pagination
 * @var array $pag
 * @var array $list_taguri
 * @var array $list_specii
 */
?>

<!-- FILTRE -->
<?php $this->load->view('/admin/poezii/_partials/filters') ?>

<div class="row">
	<div class="col-xs-12">
		<div class="x_panel">

			<div class="x_title">
				<h2><i class="fa fa-table"></i> Lista poeziilor</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				</ul>
				<div class="clear"></div>
			</div>

			<div class="x_content">

				<div class="table-responsive">

					<table class="table table-striped table-bordered table-hover table-highlight dataTable">
						<thead>
							<tr>
								<th style="width:15px">ID</th>
								<th style="width:10px;" title="Flag - in lucru"></th>
								<th>Titlu (Titlu alternativ)</th>
								<th>Autor</th>
								<th>Perioada creatiei</th>
								<th>Data/locul creației</th>
								<th>Specia</th>
								<th>Hits</th>

								<th style="width: 1px">
									<div>
										<a href="/poezii/add/" class="btn btn-info btn-sm" role="button">
											<i class="fa fa-plus"></i>
											<span class="hidden-sm hidden-xs"><?= html_escape('Add') ?></span>
										</a>
									</div>
								</th>
							</tr>
						</thead>

						<?php if (count($items)): ?>
							<tbody>
								<?php foreach ($items as $k => $v):

									$comentariu_poezie = !empty($v->getComentariuArticolId()) ? '<span class="glyphicon glyphicon-pencil" title="comentariu poezie"></span> ' : '';

									// flag - in lucru
									$flag              = !$v->isPublished() ? '<span title="Poezie in lucru - Nepublicata" class="glyphicon glyphicon-bookmark"></span>' : '';

									// if media video & audio
									$interpretare      = false ? '<span class="glyphicon glyphicon-music" title="interpretare"></span> ' : '';
									$recital           = false ? '<span class="fa fa-microphone" title="recital"></span>' : '';

									$embeded = '<div class="pull-right">' . $comentariu_poezie . $interpretare . $recital . '</div>';

									// taguri
									$tagList = $v->fetchTagList();
									$cuvCheie = [];
									if ($tagList && count($tagList)) {
										foreach ($tagList as $tagItem) {
											$cuvCheie[] = html_escape($tagItem->getNume());
										}
									}

									$objPerioadaCreatie = $v->getPerioadaCreatie();
									$objSpecie = $v->getSpecie();
								?>
									<tr>
										<td class="center"><?= html_escape($v->getId()) ?></td>
										<td class="center"><?= $flag ?></td>
										<td>
											<?php
											echo html_escape($v->getTitlu());
											echo $v->getTitluVarianta() ? ' (' . $v->getTitluVarianta() . ')' : '';
											?>

											<?= $v->getComentariuArticolId() ? '<i class="fa fa-pencil" title="Are comentariu"></i>' : '' ?>

											<br />
											<?= $v->getSubjectId() ? '<small class="gray">' . $list_taguri[$v->getSubjectId()] . (!empty($cuvCheie) ? ' |' : '') . '</small>' : '' ?>
											<?= !empty($cuvCheie) ? '<small class="gray">Cuv. cheie: ' . implode(', ', $cuvCheie) . '</small>' : '' ?>

											<?php if ($v->isDeleted()) { ?>
												<br />
												<span class="labelStatus label label-danger">Ștearsă</span>
											<?php } ?>
										</td>
										<td><?= !empty($v->getPersonaj()) ? html_escape($v->getPersonaj()->getNumeComplet()) : '' ?></td>
										<td><?= $objPerioadaCreatie ? html_escape($objPerioadaCreatie->getNume()) : '' ?></td>
										<td>
											<?= $v->getDataCreatiei() ?> / <?= $v->getLoculCreatiei() ?>
										</td>
										<td><?= $objSpecie ? html_escape($objSpecie->getNume()) : '' ?></td>
										<td><?= $v->getHits() ?></td>

										<td style="white-space: nowrap">
											<div>
												<a href="/poezii/edit/<?= (int)$v->getId() ?>"
													class="btn btn-sm btn-primary"
													title="<?= html_escape('Edit') ?>">
													<i class="fa fa-edit bigger-110"></i> <span class="hidden-sm hidden-xs"><?= html_escape('Edit') ?></span>
												</a>
											</div>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						<?php else: ?>
							<tbody>
								<tr>
									<td colspan="50" class="nodata"><?= html_escape('No data found') ?></td>
								</tr>
							</tbody>
						<?php endif; ?>
						<tfoot>
							<tr>
								<th colspan="50">
									<div style="float: left">
										<?= $this->load->view('/_partial/table/paginator', $pag, true) ?>
									</div>
									<div style="float: right">
										<?= $pagination ?>
									</div>
								</th>
							</tr>
						</tfoot>
					</table>

				</div>

			</div>
		</div>

	</div>
</div>