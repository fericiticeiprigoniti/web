<?php
$tabs = [
    [
        'title' => 'Informații generale',
        'tab_active' => 'generale',
    ],
    [
        'title' => 'Media (Audio/Video)',
        'tab_active' => 'media',
    ],
    [
        'title' => 'Variante',
        'tab_active' => 'variante',
    ],
    [
        'title' => 'Comentariu',
        'tab_active' => 'comentariu',
    ],
    [
        'title' => 'Povestea',
        'tab_active' => 'povestea',
    ],
];
?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-edit"></i> <?= $page_title ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li>
                        <button class="btn btn-primary" id="ajaxSubmit">
                            <i class="fa fa-save"></i> Save
                        </button>
                    </li>
                </ul>
                <div class="clear"></div>
            </div>

            <div class="x_content">
                <input type="hidden" name="tokenID" id="token" value="<?php echo rand(1, 999) ?>" />

                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs" role="tablist">

                        <?php foreach ($tabs as $key => $tab) { ?>
                            <li role="presentation" class="<?php echo (!empty($tab_activ) && $tab_activ == $tab['tab_active']) ? 'active' : '' ?>">
                                <a href="#tab_content<?= $key ?>" id="profile-tab" role="tab" <?= $edit ? 'data-toggle="tab"' : '' ?>
                                    aria-expanded="<?= (!empty($tab_activ) && $tab_activ == $tab['tab_active']) ? 'true' : 'false' ?>"><?= $tab['title'] ?></a>
                            </li>
                        <?php } ?>

                    </ul>

                    <div id="myTabContent" class="tab-content">
                        <?php foreach ($tabs as $key => $tab) { ?>
                            <div role="tabpanel" class="tab-pane fade <?= (!empty($tab_activ) && $tab_activ == $tab['tab_active']) ? 'active in' : '' ?>" id="tab_content<?= $key ?>" aria-labelledby="profile-tab">
                                <?php $this->load->view("/admin/poezii/_partials/tab_{$tab['tab_active']}") ?>
                            </div>
                        <?php } ?>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {

        $('.preview').click(function(e) {
            e.preventDefault();
        })

        $('.allowClear').select2({
            placeholder: "selecteaza",
            allowClear: true
        });

        $("#cuvinteCheie").select2({
            tags: true,
            minimumInputLength: 2,
            allowClear: true,
            ajax: {
                url: '/admin/taguri/getTags',
                dataType: "json",
                data: function(term, page) {
                    return {
                        q: term
                    };
                },
                results: function(data, page) {
                    var ret = {
                        results: []
                    };
                    for (tag in data.tags) {
                        tag = data.tags[tag];
                        ret.results.push({
                            id: tag.id,
                            text: tag.text
                        });
                    }
                    return ret;
                }
            }
        });
    });
</script>