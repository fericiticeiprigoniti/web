<?php

/**
 * @var TagItem $objItem
 */
?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-edit"></i> <?= $page_title ?></h2>
                <div class="clear"></div>
            </div>

            <div class="x_content">

                <form method="post"
                    class="form-horizontal ajaxForm"
                    action="<?= General::url() ?>"
                    data-redirect-url="/taguri/">

                    <div class="col-md-6 col-md-offset-2">

                        <?php if ($objItem->getId()): ?>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">ID</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" class="form-control" value="<?= $objItem->getId() ?>" readonly />
                                </div>
                            </div>
                        <?php endif; ?>

                        <div class="form-group">
                            <?php $fieldName = 'nume' ?>
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nume <span class="req">*</span></label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" name="<?= $fieldName ?>" class="form-control" value="<?= set_value($fieldName, $objItem->getNume()) ?>">
                                <?= form_error($fieldName); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <?php $fieldName = 'personaj_id' ?>
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Personaj</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <select name="<?= $fieldName ?>" class="form-control select2 allowClear" data-placeholder="selecteaza">
                                    <option></option>
                                    <?php echo_select_options($list_personaje, set_value($fieldName, $objItem->getCatId()), ['_key', '_value']) ?>
                                </select>
                                <?= form_error($fieldName) ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <?php $fieldName = 'cat_id' ?>
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Categorie</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <select name="<?= $fieldName ?>" class="form-control select2 allowClear" data-placeholder="selecteaza">
                                    <option></option>
                                    <?php echo_select_options($nom_cat_tags, set_value($fieldName, $objItem->getCatId()), ['_key', '_value']) ?>
                                </select>
                                <?= form_error($fieldName) ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <?php $fieldName = 'observatii' ?>
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Observatii</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <textarea name="<?= $fieldName ?>" class="form-control" style="height:100px"><?= set_value($fieldName, $objItem->getObservatii()) ?></textarea>
                                <?= form_error($fieldName) ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <?php $fieldName = 'is_favorit' ?>
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Favorit</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <select name="<?= $fieldName ?>" class="form-control select2" data-placeholder="selecteaza">
                                    <?php
                                    $list = ['0' => 'nu', '1' => 'da'];
                                    foreach ($list as $k => $v): ?>
                                        <?php $selected = $k == $objItem->getIsFavorit() ? 'selected="selected"' : '' ?>
                                        <option <?= $selected ?> value="<?= html_escape($k) ?>"><?= html_escape($v) ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="panel-footer">
                            <span class="req">*</span> - <i>Câmpuri obligatorii</i>
                            <button type="submit" class="btn btn-primary pull-right">
                                <i class="fa fa-save"></i> Salvare
                            </button>
                            <div class="clear"></div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>