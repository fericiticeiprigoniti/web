<?php

/**
 * @var TagItem[] $items
 */
?>

<div class="row">
	<div class="col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2><i class="fa fa-table"></i>
					<?= $page_title ?>
					<span class="badge"><?= $pag['count'] ?></span>
				</h2>
				<div class="clear"></div>
			</div>

			<div class="x_content">

				<div class="table-responsive">

					<table class="table table-striped table-bordered table-hover table-highlight dataTable">
						<thead>
							<tr>
								<th class="center">ID</th>
								<th>Nume</th>
								<th>Categorie</th>
								<th>Adaugat de</th>
								<th>Data</th>
								<th>Hits</th>

								<th class="actions">
									<div class="">
										<a href="/taguri/edit/" class="btn btn-info btn-sm" role="button" style="width:100%">
											<i class="fa fa-plus"></i><span class="hidden-sm hidden-xs"> Add</span>
										</a>
									</div>
								</th>
							</tr>
						</thead>

						<?php if (count($items)): ?>
							<tbody>
								<?php $i = 0;
								foreach ($items as $k => $v): ?>
									<tr>
										<td class="center"><?= html_escape($v->getId()) ?></td>
										<td>
											<div>
												<?= html_escape($v->getNume()) ?>
												<?php if ($v->getIsFavorit()) { ?>
													<span class="fa fa-star" title="Setat ca favorit"></span>
												<?php } ?>
											</div>

											<?php if ($v->getPersonaj()) { ?>
												<div class="gray font11">
													Personaj:
													<?= html_escape($v->getPersonaj()->getFullName()) ?>
												</div>
											<?php } ?>

											<?php if ($v->getObservatii()) { ?>
												<div class="gray font11">
													Observatie:
													<?= html_escape($v->getObservatii()) ?>
												</div>
											<?php } ?>
										</td>
										<td>
											<?= $v->getCatId() ? $nom_cat_tags[$v->getCatId()] : '' ?>
										</td>
										<td><span class="fa fa-user"></span> <?= $v->getUser()->getFullname() ?></td>
										<td><?= html_escape($v->getDataInsert()) ?></td>
										<td><?= html_escape($v->getHits()) ?></td>

										<td class="actions" style="white-space: nowrap">
											<a href="/taguri/edit/<?= (int)$v->getId() ?>" class="btn btn-sm btn-primary" title="Edit">
												<i class="fa fa-edit bigger-110"></i><span class="hidden-sm hidden-xs"> Edit</span>
											</a>
											<a href="/taguri/delete/<?= (int)$v->getId() ?>" class="btn btn-sm btn-danger btnDelete"
												data-name="cuvant cheie <?= html_escape($v->getNume()) ?>" title="Delete">
												<i class="fa fa-trash-o bigger-110"></i><span class="hidden-sm hidden-xs"> Delete</span>
											</a>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						<?php else: ?>
							<tbody>
								<tr>
									<td colspan="50" class="nodata"><?= html_escape('No data found') ?></td>
								</tr>
							</tbody>
						<?php endif; ?>

						<tfoot>
							<tr>
								<th colspan="50" class="panel-footer">
									<div style="float: left">
										<?= $this->load->view('/_partial/table/paginator', $pag, true) ?>
									</div>
									<div style="float: right">
										<?= $pagination ?>
									</div>
								</th>
							</tr>
						</tfoot>
					</table>

				</div>

			</div>
		</div>

	</div>
</div>