<?php

/**
 * @var array $items
 */
?>

<div class="row">
	<div class="col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2><i class="fa fa-table"></i> Lista funcţii</h2>
				<div class="clear"></div>
			</div>

			<div class="x_content">

				<div class="table-responsive">

					<table class="table table-striped table-bordered table-hover table-highlight dataTable">
						<thead>
							<tr>
								<th class="center" style="width:50px">Nr. crt.</th>
								<th class="center">ID</th>
								<th>Nume</th>
								<th class="actions">
									<div class="hidden-sm hidden-xs">
										<a href="/functii/edit/" class="btn btn-info btn-sm" role="button" style="width:100%">
											<i class="fa fa-plus"></i> Add
										</a>
									</div>
									<div class="hidden-md hidden-lg">
										<a href="/functii/edit/" class="btn btn-info btn-sm" role="button" title="Add">
											<i class="fa fa-plus"></i>
										</a>
									</div>
								</th>
							</tr>
						</thead>

						<?php if (count($items)): ?>
							<tbody>
								<?php $i = 0 ?>
								<?php foreach ($items as $k => $v): ?>
									<tr>
										<td class="center"><?= ++$i ?></td>
										<td class="center"><?= html_escape($v['id']) ?></td>
										<td><?= html_escape($v['nume']) ?></td>
										<td class="actions" style="white-space: nowrap">
											<div class="hidden-sm hidden-xs">
												<a href="/functii/edit/<?= (int)$v['id'] ?>" class="btn btn-sm btn-primary">
													<i class="fa fa-edit bigger-110"></i> Edit
												</a>
												<a href="/functii/delete/<?= (int)$v['id'] ?>" class="btn btn-sm btn-danger btnDelete"
													data-name="funcţia <?= html_escape($v['nume']) ?>">
													<i class="fa fa-trash-o bigger-110"></i> Delete
												</a>
											</div>
											<div class="hidden-md hidden-lg">
												<a href="/functii/edit/<?= (int)$v['id'] ?>" class="btn btn-sm btn-primary" title="Edit">
													<i class="fa fa-edit bigger-110"></i>
												</a>
												<a href="/functii/delete/<?= (int)$v['id'] ?>" class="btn btn-sm btn-danger btnDelete"
													data-name="funcţia <?= html_escape($v['nume']) ?>" title="Delete">
													<i class="fa fa-trash-o bigger-110"></i>
												</a>
											</div>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						<?php else: ?>
							<tbody>
								<tr>
									<td colspan="50" class="nodata"><?= html_escape('No data found') ?></td>
								</tr>
							</tbody>
						<?php endif; ?>
					</table>

				</div>

			</div>
		</div>

	</div>
</div>