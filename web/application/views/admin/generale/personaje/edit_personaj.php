<!-- Main content -->
<?php

if ($this->session->flashdata('log')) { ?>
    <div class="alert alert-success alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <?php echo $this->session->flashdata('log') ?>
    </div>
<?php } ?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?php echo isset($autocomplete_page['subtitle']) ? $autocomplete_page['subtitle'] : '' ?></h2>

                <ul class="nav navbar-right panel_toolbox">
                    <li>
                        <button class="btn btn-primary" id="ajaxSubmit">
                            <i class="fa fa-save"></i> Save
                        </button>
                    </li>
                </ul>
                <br />
                <div class="clearfix"></div>
            </div>

            <div class="x_content">

                <input type="hidden" name="tokenID" id="token" value="<?php echo rand(1, 999) ?>" />

                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="<?php echo ($tab_activ == 'generale') ? 'active' : '' ?>">
                            <a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab"
                                aria-expanded="<?php echo ($tab_activ == 'generale') ? 'true' : 'false' ?>">Date generale</a>
                        </li>

                        <li role="presentation" class="<?php echo ($tab_activ == 'episoade_represive') ? 'active' : (!$edit ? 'disabled disabledTab' : '') ?>">
                            <a href="#tab_content5" role="tab" id="profile-tab5" <?= $edit ? 'data-toggle="tab"' : '' ?>
                                aria-expanded="<?php echo ($tab_activ == 'episoade_represive') ? 'true' : 'false' ?>">Episoade represive</a>
                        </li>

                        <li role="presentation" class="<?php echo ($tab_activ == 'itinerariu_detentie') ? 'active' : (!$edit ? 'disabled disabledTab' : '') ?>">
                            <a href="#tab_content6" role="tab" id="profile-tab6" <?= $edit ? 'data-toggle="tab"' : '' ?>
                                aria-expanded="<?php echo ($tab_activ == 'itinerariu_detentie') ? 'true' : 'false' ?>">Itinerariu detentie</a>
                        </li>

                        <li role="presentation" class="<?php echo ($tab_activ == 'biografie') ? 'active' : (!$edit ? 'disabled disabledTab' : '') ?>">
                            <a href="#tab_content2" role="tab" id="profile-tab" <?= $edit ? 'data-toggle="tab"' : '' ?>
                                aria-expanded="<?php echo ($tab_activ == 'biografie') ? 'true' : 'false' ?>">Biografie</a>
                        </li>
                        <li role="presentation" class="<?php echo ($tab_activ == 'semnalmente_corporale') ? 'active' : (!$edit ? 'disabled disabledTab' : '') ?>">
                            <a href="#tab_content3" role="tab" id="profile-tab3" <?= $edit ? 'data-toggle="tab"' : '' ?>
                                aria-expanded="<?php echo ($tab_activ == 'semnalmente_corporale') ? 'true' : 'false' ?>">Semnalmente corporale</a>
                        </li>
                        <li role="presentation" class="<?php echo ($tab_activ == 'functii') ? 'active' : (!$edit ? 'disabled disabledTab' : '') ?>">
                            <a href="#tab_content4" role="tab" id="profile-tab4" <?= $edit ? 'data-toggle="tab"' : '' ?>
                                aria-expanded="<?php echo ($tab_activ == 'realizarisocioprofesionale') ? 'true' : 'false' ?>">Realizari socio-profesionale</a>
                        </li>

                        <li role="presentation" class="<?php echo ($tab_activ == 'pedepse_disciplinare') ? 'active' : (!$edit ? 'disabled disabledTab' : '') ?>">
                            <a href="#tab_content7" role="tab" id="profile-tab7" <?= $edit ? 'data-toggle="tab"' : '' ?>
                                aria-expanded="<?php echo ($tab_activ == 'pedepse_disciplinare') ? 'true' : 'false' ?>">Pedepse disciplinare</a>
                        </li>

                        <li role="presentation" class="<?php echo ($tab_activ == 'carti') ? 'active' : (!$edit ? 'disabled disabledTab' : '') ?>">
                            <a href="#tab_content8" role="tab" id="profile-tab8" <?= $edit ? 'data-toggle="tab"' : '' ?>
                                aria-expanded="<?php echo ($tab_activ == 'carti') ? 'true' : 'false' ?>">Cărți</a>
                        </li>

                        <li role="presentation" class="<?php echo ($tab_activ == 'citate') ? 'active' : (!$edit ? 'disabled disabledTab' : '') ?>">
                            <a href="#tab_content9" role="tab" id="profile-tab8" <?= $edit ? 'data-toggle="tab"' : '' ?>
                                aria-expanded="<?php echo ($tab_activ == 'citate') ? 'true' : 'false' ?>">Citate</a>
                        </li>
                    </ul>

                    <!-- Tab Content -->
                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade <?php echo ($tab_activ == 'generale') ? 'active in' : '' ?>" id="tab_content1" aria-labelledby="home-tab">
                            <?php $this->load->view('/admin/generale/personaje/_partials/generale') ?>
                        </div>

                        <?php if ($edit) { ?>

                            <div role="tabpanel" class="tab-pane fade <?php echo ($tab_activ == 'episoade_represive') ? 'active in' : '' ?>" id="tab_content5" aria-labelledby="profile-tab">
                                <?php $this->load->view('/admin/generale/personaje/_partials/episoade_represive') ?>
                            </div>

                            <div role="tabpanel" class="tab-pane fade <?php echo ($tab_activ == 'itinerariu_detentie') ? 'active in' : '' ?>" id="tab_content6" aria-labelledby="profile-tab">
                                <?php $this->load->view('/admin/generale/personaje/_partials/itinerariu_detentie') ?>
                            </div>
                            <div role="tabpanel" class="tab-pane fade <?php echo ($tab_activ == 'biografie') ? 'active in' : '' ?>" id="tab_content2" aria-labelledby="profile-tab">
                                <?php $this->load->view('/admin/generale/personaje/_partials/biografie') ?>
                            </div>

                            <div role="tabpanel" class="tab-pane fade <?php echo ($tab_activ == 'semnalmente_corporale') ? 'active in' : '' ?>" id="tab_content3" aria-labelledby="profile-tab">
                                <?php $this->load->view('/admin/generale/personaje/_partials/semnalmente_corporale') ?>
                            </div>

                            <div role="tabpanel" class="tab-pane fade <?php echo ($tab_activ == 'realizarisocioprofesionale') ? 'active in' : '' ?>" id="tab_content4" aria-labelledby="profile-tab">
                                <?php $this->load->view('/admin/generale/personaje/_partials/realizari_socio_profesionale') ?>
                            </div>

                            <div role="tabpanel" class="tab-pane fade <?php echo ($tab_activ == 'pedepse_disciplinare') ? 'active in' : '' ?>" id="tab_content7" aria-labelledby="profile-tab">
                                <?php $this->load->view('/admin/generale/personaje/_partials/pedepse_disciplinare') ?>
                            </div>

                            <div role="tabpanel" class="tab-pane fade <?php echo ($tab_activ == 'carti') ? 'active in' : '' ?>" id="tab_content8" aria-labelledby="profile-tab">
                                <?php $this->load->view('/admin/generale/personaje/_partials/carti') ?>
                            </div>

                            <div role="tabpanel" class="tab-pane fade <?php echo ($tab_activ == 'citate') ? 'active in' : '' ?>" id="tab_content9" aria-labelledby="profile-tab">
                                <?php $this->load->view('/admin/generale/personaje/_partials/citate') ?>
                            </div>
                        <?php } ?>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {

        $('.table').on('click', '.delete_row', function() {
            $(this).parents('tr').remove();
        })

        $('.datepicker').datepicker({
            dateFormat: 'dd-mm-yy'
        });
    })
</script>