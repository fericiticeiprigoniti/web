<?php

/**
 * @var array $items
 * @var object $filters
 * @var object $order
 * @var string $pagination
 * @var array $pag
 * @var array $roluri
 * @var array $ocupatii
 */
?>

<!-- FILTRE -->
<?php $this->load->view('/admin/generale/personaje/_partials/filtre') ?> 

<style>
	.labelStatus {
		display: inline-block;
		width: 100%;
	}
</style>

<div class="row">
	<div class="col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2><i class="fa fa-table"></i> Lista personajelor</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				</ul>
				<div class="clear"></div>
			</div>

			<div class="x_content">

				<div class="table-responsive">

					<table class="table table-striped table-bordered table-hover table-highlight dataTable">
						<thead>
							<tr>
								<?= $this->load->view('/_partial/table/thSort', array('name' => 'id', 'label' => 'ID', 'order' => $order), true) ?>
								<?= $this->load->view('/_partial/table/thSort', array('name' => 'avatar_poza_id', 'label' => 'Foto', 'order' => $order), true) ?>
								<?= $this->load->view('/_partial/table/thSort', array('name' => 'nume_complet', 'label' => 'Nume complet', 'order' => $order), true) ?>
								<?= $this->load->view('/_partial/table/thSort', array('name' => 'ocupatii_socioprofesionale', 'label' => 'Ocupație / Ocupații', 'order' => $order, 'class' => 'hidden-xs'), true) ?>
								<th class="hidden-xs">Confesiune / <br />Nationalitate</th>
								<?= $this->load->view('/_partial/table/thSort', array('name' => 'data_nastere', 'label' => 'Loc. nasterii /<br />Data nasterii', 'order' => $order), true) ?>
								<?= $this->load->view('/_partial/table/thSort', array('name' => 'data_adormire', 'label' => 'Loc. înmormântare /<br />Data adormire', 'order' => $order), true) ?>
								<?= $this->load->view('/_partial/table/thSort', array('name' => 'nr_roluri', 'label' => 'Roluri', 'order' => $order, 'class' => 'hidden-xs'), true) ?>
								<th class="hidden-xs">Status</th>
								<th style="width: 1px">
									<div class="hidden-sm hidden-xs">
										<a href="/personaje/editeaza/" class="btn btn-info btn-sm" role="button" target="_blank">
											<i class="fa fa-plus"></i>
											<?= html_escape('Add') ?>
										</a>
									</div>
									<div class="hidden-md hidden-lg">
										<a href="/personaje/editeaza/" class="btn btn-info btn-sm" role="button" target="_blank">
											<i class="fa fa-plus"></i>
										</a>
									</div>
								</th>
							</tr>
						</thead>

						<?php if (count($items)): ?>
							<tbody>
								<?php foreach ($items as $k => $v): ?>
									<tr>
										<td class="center"><?= html_escape($v['id']) ?></td>
										<td class="center"></td>
										<td>
											<?= html_escape($v['nume_complet']) ?>
											<?= !empty($v['nume_extrainfo']) ? $v['nume_extrainfo']  : '' ?>
											<div class="gray font11">
												<small>Alias: <?= $v['alias'] ?></small>
											</div>
										</td>
										<td class="hidden-xs" style="max-width:200px"><?= html_escape($v['ocupatii_socioprofesionale']) ?></td>
										<td class="hidden-xs" style="max-width:100px">
											<?= html_escape($v['confesiune']) ?>
											<?= !empty($v['confesiune']) ? '<br />' : '' ?>
											<?= html_escape($v['nationalitate']) ?>
										</td>
										<td>
											<?= html_escape($v['loc_nastere']) ?>
											<?= !empty($v['loc_nastere']) ? '<br />' : '' ?>
											<?= $v['data_nastere'] ? Calendar::convertDateMysql2Ro($v['data_nastere']) : '' ?>
										</td>
										<td>
											<?= html_escape($v['loc_inmormantare']) ?>
											<?= !empty($v['loc_inmormantare']) ? '<br />' : '' ?>
											<?= $v['data_adormire'] ? Calendar::convertDateMysql2Ro($v['data_adormire']) : '' ?>
										</td>
										<td class="hidden-xs" style="max-width:200px"><?= html_escape($v['roluri']) ?></td>
										<td class="hidden-xs text-center">
											<?php if ($v['status'] == 'in_lucru'): ?>
												<span class="labelStatus label label-default">În lucru</span>
											<?php elseif ($v['status'] == 'activ'): ?>
												<span class="labelStatus label label-primary">Activ</span>
											<?php elseif ($v['status'] == 'sters'): ?>
												<span class="labelStatus label label-danger">Şters</span>
											<?php endif; ?>
										</td>
										<td style="white-space: nowrap">
											<div class="hidden-sm hidden-xs">
												<a class="btn btn-sm btn-primary" target="_blank"
													href="/personaje/editeaza/<?= (int)$v['id'] ?>">
													<i class="fa fa-edit bigger-110"></i> <?= html_escape('Edit') ?>
												</a>
											</div>
											<div class="hidden-md hidden-lg">
												<a href="/personaje/editeaza/<?= (int)$v['id'] ?>" target="_blank"
													class="btn btn-sm btn-primary" title="<?= html_escape('Edit') ?>">
													<i class="fa fa-edit bigger-110"></i>
												</a>
											</div>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						<?php else: ?>
							<tbody>
								<tr>
									<td colspan="50" class="nodata"><?= html_escape('No data found') ?></td>
								</tr>
							</tbody>
						<?php endif; ?>
						<tfoot>
							<tr>
								<th colspan="50" class="panel-footer">
									<div style="float: left">
										<?= $this->load->view('/_partial/table/paginator', $pag, true) ?>
									</div>
									<div style="float: right">
										<?= $pagination ?>
									</div>
								</th>
							</tr>
						</tfoot>
					</table>

				</div>

			</div>
		</div>

	</div>
</div>