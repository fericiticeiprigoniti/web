<!-- Tab - Semnalmente corporale -->
<form name="personaj_semnalmente" id="personaj_semnalmente" data-parsley-validate
  class="form-horizontal form-label-left formular formular_autor ajaxForm"
  method="post"
  action="<?php echo '/personaje/' . (($edit) ? 'editeaza/' . $edit : 'adauga') . '?tab=semnalmente_corporale'; ?>">

  <div class="col-lg-12 col-md-12 col-sm-12">
    <?php foreach ($list_semnalmente as $semn) { ?>
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><?php echo $semn['nume']; ?></label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text"
            name="semn_<?php echo $semn['id']; ?>"
            id="semn_<?php echo $semn['id']; ?>"
            class="form-control col-md-7 col-xs-12"
            value="<?php echo set_value('semn_' . $semn['id'], ($edit && !empty($info['semnalmente'][$semn['id']])) ? $info['semnalmente'][$semn['id']] : ''); ?>">
        </div>
      </div>
    <?php } ?>
  </div>
</form>
<!-- end Tab - Semnalmente corporale -->