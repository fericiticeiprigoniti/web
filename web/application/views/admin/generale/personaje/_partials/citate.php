<?php echo validation_errors(); ?>

<div class="col-lg-6">

    <form name="personaj_citate"
        data-parsley-validate
        class="form-horizontal form-label-left formular formular_autor ajaxForm"
        method="post"
        action="<?php echo '/personaje/' . (($edit) ? 'editeaza/' . $edit : 'adauga') . '?tab=citate' . (!empty($citatID) ? '&citatID=' . $citatID : '') ?>"
        data-redirect-url="/personaje/editeaza/<?= $edit ?>?tab=citate">

        <div class="form-group">
            <label class="control-label">Conținut</label>
            <textarea name="continut" class="tinymcesimple form-control" style="height:50px"><?php echo set_value('continut', (!empty($info_citat) ? $info_citat['continut'] : '')) ?></textarea>
            <?php echo form_error('continut'); ?>
        </div>

        <div class="form-group">
            <label class="control-label">Cartea</label>
            <select name="carte_id" class="form-control allowClear" data-placeholder="selecteaza">
                <option></option>
                <?php echo_select_options($list_carti, (!empty($info_citat) ? $info_citat['carte_id'] : false), array('_key', '_value')) ?>
            </select>
            <?php echo form_error('carte_id'); ?>
        </div>

        <div class="form-group">
            <label class="control-label">Sursa</label>
            <input type="text" name="sursa" class="form-control" value="<?php echo set_value('sursa', (!empty($info_citat) ? $info_citat['sursa'] : '')) ?>" />
            <?php echo form_error('sursa'); ?>
        </div>

        <div class="form-group">
            <label class="control-label">Paginație</label>
            <div class="">
                <input type="number" name="pag_start" class="col-lg-3" placeholder="pagina inceput" value="<?php echo set_value('pag_start', (!empty($info_citat) ? $info_citat['pag_start'] : '')) ?>" />
                <input type="number" name="pag_end" class="col-lg-3" placeholder="pagina sfarsit" value="<?php echo set_value('pag_end', (!empty($info_citat) ? $info_citat['pag_end'] : '')) ?>" />
                <?php echo form_error('pag_start'); ?>
                <?php echo form_error('pag_end'); ?>
            </div>
        </div>


        <div class="form-group">
            <label class="control-label">Observații</label>
            <textarea name="observatii" class="form-control" style="height:50px"><?php echo set_value('observatii', (!empty($info_citat) ? $info_citat['observatii'] : '')) ?></textarea>
            <?php echo form_error('observatii'); ?>
        </div>
    </form>
</div>

<?php if (!empty($info['pers_citate']) && $edit) { ?>
    <div class="col-lg-6">
        <table class="table table-hover table_functii">
            <thead>
                <tr>
                    <th width='50px'>Nr</th>
                    <th>Citat</th>
                    <th>Carte/Sursa</th>
                    <th>Pagini</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($info['pers_citate'] as $item) { ?>
                    <tr>
                        <td><?= $item['id'] ?></td>
                        <td>
                            <?= $item['continut'] ?>
                            <?php if ($item['observatii']) { ?>
                                <div class="gray font11">
                                    Observații: <?= $item['observatii'] ?>
                                </div>
                            <?php } ?>
                        </td>
                        <td>
                            <?= 'Carte ID:' . $item['carte_id'] . '/' . $item['sursa'] ?>
                        </td>
                        <td>
                            <?= ($item['pag_start'] ? $item['pag_start'] : ''); ?>
                            <?= ($item['pag_end'] ? '- ' . $item['pag_end'] : ''); ?>
                        </td>

                        <td>
                            <a href="/personaje/editeaza/<?= $info['personaj_id'] ?>?tab=citate&citatID=<?= $item['id'] ?>" class="btn btn-sm btn-primary">
                                <i class="fa fa-edit bigger-110"></i>
                            </a>

                            <a href="/personaje/stergeCitat/<?= $info['personaj_id'] . '/' . $item['id']; ?>" class="btn btn btn-danger btn-sm btnDelete"
                                data-name="citatul #<?= $item['id'] ?>"
                                title="Sterge definitiv citatul">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
<?php } ?>