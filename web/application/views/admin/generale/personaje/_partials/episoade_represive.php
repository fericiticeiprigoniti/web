<?php

/**
 * @var int $edit
 * @var int $condamnareID
 * @var array $list_condamnari_iesire_tipuri
 * @var array $list_represiune_tipuri
 * @var array $list_ocupatii
 * @var array $list_apart_politice
 * @var array $list_stare_civila
 * @var array $list_sentinte_detinuti
 * @var array $list_pedepse_tipuri
 * @var array $info
 * @var array $info_condamnare
 */

if (!isset($condamnareID)) {
    $condamnareID = null;
}

if (!isset($info_condamnare)) {
    $info_condamnare = [];
}

echo validation_errors();
?>

<form name="personaj_condamnari"
    data-parsley-validate class="form-horizontal form-label-left formular formular_autor ajaxForm"
    method="post"
    action="<?php echo '/personaje/' . ($edit ? 'editeaza/' . $edit : 'adauga') . '?tab=episoade_represive' . ($condamnareID ? '&condamnareID=' . $condamnareID : '') ?>"
    data-redirect-url="/personaje/editeaza/<?= $edit ?>?tab=episoade_represive">

    <div class="col-lg-4 col-md-6 col-sm-12 pl0">
        <div class="form-group">
            <label class="control-label col-md-4 col-sm-3 col-xs-12">Tip represiune<b class="required">*</b></label>
            <div class="col-md-8 col-sm-9 col-xs-12">
                <select name="represiune_tip_id" class="form-control allowClear is-required" data-placeholder="selecteaza">
                    <option></option>
                    <?php
                    echo_select_options(
                        $list_represiune_tipuri,
                        set_value('represiune_tip_id', $info_condamnare['represiune_tip_id'] ?? false),
                        ['_key', '_value']
                    )
                    ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-4 col-sm-3 col-xs-12">Data început represiune<b class="required">*</b></label>
            <div class="col-md-8 col-sm-9 col-xs-12">
                <input type="text" class="form-control " placeholder="zi/luna/an" data-inputmask="'mask': '99/99/9999'"
                    name="represiune_data_start" value="<?php echo set_value('represiune_data_start', Calendar::convertDateMysql2Ro($info_condamnare['represiune_data_start'] ?? null)) ?>">
                <?php echo form_error('represiune_data_start'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-4 col-sm-3 col-xs-12">Data ieșire represiune<b class="required">*</b></label>
            <div class="col-md-8 col-sm-9 col-xs-12">
                <input type="text" class="form-control " placeholder="zi/luna/an" data-inputmask="'mask': '99/99/9999'"
                    name="represiune_data_end" value="<?php echo set_value('represiune_data_end', Calendar::convertDateMysql2Ro($info_condamnare['represiune_data_end'] ?? null)) ?>">
                <?php echo form_error('represiune_data_end'); ?>
            </div>
        </div>


        <div class="form-group">
            <label class="control-label col-md-4 col-sm-3 col-xs-12">Condamnare iesire data</label>
            <div class="col-md-8 col-sm-9 col-xs-12">
                <input type="text" class="form-control datepicker" placeholder="zi/luna/an" data-inputmask="'mask': '99/99/9999'"
                    name="cond_iesire_data" value="<?php echo set_value('cond_iesire_data', Calendar::convertDateMysql2Ro($info_condamnare['cond_iesire_data'] ?? null)) ?>">
                <?php echo form_error('cond_iesire_data'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-4 col-sm-3 col-xs-12">Tip ieșire</label>
            <div class="col-md-8 col-sm-9 col-xs-12">
                <select name="cond_iesire_tip_id" class="form-control allowClear" data-placeholder="selecteaza">
                    <option></option>
                    <?php
                    echo_select_options(
                        $list_condamnari_iesire_tipuri,
                        set_value('cond_iesire_tip_id', $info_condamnare['cond_iesire_tip_id'] ?? false),
                        ['_key', '_value']
                    );
                    ?>
                </select>
                <?php echo form_error('cond_iesire_tip_id'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-4 col-sm-3 col-xs-12">Localitatea la arestare</label>
            <div class="col-md-8 col-sm-9 col-xs-12">
                <input type="hidden" id="arest_dom_loc_id" name="arest_dom_loc_id" value="<?= $info_condamnare['arest_dom_loc_id'] ?? '' ?>">
                <input type="text" class="form-control devbridge-autocomplete" placeholder="cauta localitatea"
                    data-id="arest_dom_loc_id" data-url="/personaje/cautaLocalitate"
                    value="<?= '' ?>" />

            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-4 col-sm-3 col-xs-12">Strada</label>
            <div class="col-md-8 col-sm-9 col-xs-12">
                <input type="text" class="form-control" name="arest_dom_strada"
                    value="<?php echo set_value('arest_dom_strada', $info_condamnare['arest_dom_strada'] ?? '') ?>">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-4 col-sm-3 col-xs-12">Ocupația la arestare</label>
            <div class="col-md-8 col-sm-9 col-xs-12">
                <select name="arest_ocupatie_id" class="form-control allowClear" data-placeholder="selecteaza">
                    <option></option>
                    <?php
                    echo_select_options(
                        $list_ocupatii,
                        set_value('arest_ocupatie_id', $info_condamnare['arest_ocupatie_id'] ?? false),
                        ['_key', '_value']
                    )
                    ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-4 col-sm-3 col-xs-12">Apartenența politică la arestare</label>
            <div class="col-md-8 col-sm-9 col-xs-12">
                <select name="arest_apart_politica_id" class="form-control allowClear" data-placeholder="selecteaza">
                    <option></option>
                    <?php
                    echo_select_options(
                        $list_apart_politice,
                        set_value('arest_apart_politica_id', $info_condamnare['arest_apart_politica_id'] ?? false),
                        ['_key', '_value']
                    )
                    ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-4 col-sm-3 col-xs-12">Apartenența politică înainte de arestare</label>
            <div class="col-md-8 col-sm-9 col-xs-12">
                <select name="pre_arest_apart_politica_id" class="form-control allowClear" data-placeholder="selecteaza">
                    <option></option>
                    <?php
                    echo_select_options(
                        $list_apart_politice,
                        set_value('pre_arest_apart_politica_id', $info_condamnare['pre_arest_apart_politica_id'] ?? false),
                        ['_key', '_value']
                    )
                    ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-4 col-sm-3 col-xs-12">Stare civilă la arestare</label>
            <div class="col-md-8 col-sm-9 col-xs-12">
                <select name="stare_civila_id" class="form-control allowClear" data-placeholder="selecteaza">
                    <option></option>
                    <?php
                    echo_select_options(
                        $list_stare_civila,
                        set_value('stare_civila_id', $info_condamnare['stare_civila_id'] ?? false),
                        ['_key', '_value']
                    )
                    ?>
                </select>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-6 col-sm-12 pl0">

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nr. sentință / inst. condamnare</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <select name="sentinta_id" class="form-control allowClear" data-placeholder="Selecteaza Nr. sentinta și instanța de condamnare">
                    <option></option>
                    <?php
                    echo_select_options(
                        $list_sentinte_detinuti,
                        set_value('sentinta_id', $info_condamnare['sentinta_id'] ?? false),
                        ['_key', '_value']
                    )
                    ?>
                </select>
                <?= form_error('sentinta_id') ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Fapta</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" class="form-control" name="cond_fapta"
                    value="<?php echo set_value('cond_fapta', $info_condamnare['cond_fapta'] ?? '') ?>">
                <?php echo form_error('cond_fapta'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Desc. pe scurt a faptei</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <textarea class="form-control" name="cond_fapta_short"><?php echo set_value('cond_fapta_short', $info_condamnare['cond_fapta_short'] ?? '') ?></textarea>
                <?php echo form_error('cond_fapta_short'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Încadrarea legală a faptei</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <textarea class="form-control" name="cond_fapta_lege"><?php echo set_value('cond_fapta_lege', $info_condamnare['cond_fapta_lege'] ?? '') ?></textarea>
                <?php echo form_error('cond_fapta_lege') ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Dată arestare</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" class="form-control datepicker" placeholder="zi/luna/an" data-inputmask="'mask': '99/99/9999'"
                    name="arest_data_arestare" value="<?php echo set_value('arest_data_arestare', !empty($info_condamnare['arest_data_arestare']) ? Calendar::convertDateMysql2Ro($info_condamnare['arest_data_arestare']) : '') ?>">
                <?php echo form_error('arest_data_arestare'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Instituție arestare</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" class="form-control" name="arest_institutie_arestare"
                    value="<?php echo set_value('arest_institutie_arestare', $info_condamnare['arest_institutie_arestare'] ?? '') ?>">
                <?php echo form_error('arest_institutie_arestare'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nr mandat arestare</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" class="form-control" name="arest_nr_mandat"
                    value="<?php echo set_value('arest_nr_mandat', $info_condamnare['arest_nr_mandat'] ?? '') ?>">
                <?php echo form_error('arest_nr_mandat'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Dată mandat arestare</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" class="form-control datepicker" placeholder="zi/luna/an" data-inputmask="'mask': '99/99/9999'"
                    name="arest_data_mandat" value="<?php echo set_value('arest_data_mandat', Calendar::convertDateMysql2Ro($info_condamnare['arest_data_mandat'] ?? null)) ?>">
                <?php echo form_error('arest_data_mandat'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Emitent mandat arestare</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" class="form-control" name="arest_emitent_mandat"
                    value="<?php echo set_value('arest_emitent_mandat', $info_condamnare['arest_emitent_mandat'] ?? '') ?>">
                <?php echo form_error('arest_emitent_mandat'); ?>
            </div>
        </div>

    </div>

    <div class="col-lg-4 col-md-6 col-sm-12 pl0">
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Durata pedepsei</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" class="form-control" name="cond_pedeapsa_durata"
                    value="<?php echo set_value('cond_pedeapsa_durata', $info_condamnare['cond_pedeapsa_durata'] ?? '') ?>">
                <?php echo form_error('cond_pedeapsa_durata'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Felul pedepsei</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <select name="cond_pedeapsa_felul_id" class="form-control allowClear" data-placeholder="selecteaza">
                    <option></option>
                    <?php
                    echo_select_options(
                        $list_pedepse_tipuri,
                        set_value('cond_pedeapsa_felul_id', $info_condamnare['cond_pedeapsa_felul_id'] ?? false),
                        ['_key', '_value']
                    )
                    ?>
                </select>
                <?php echo form_error('cond_pedeapsa_felul_id'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Dată începere pedeapsă</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" class="form-control datepicker" placeholder="zi/luna/an" data-inputmask="'mask': '99/99/9999'"
                    name="cond_pedeapsa_data_start" value="<?php echo set_value('cond_pedeapsa_data_start', !empty($info_condamnare['cond_pedeapsa_data_start']) ? Calendar::convertDateMysql2Ro($info_condamnare['cond_pedeapsa_data_start']) : ''); ?>">
                <?php echo form_error('cond_pedeapsa_data_start'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Dată sfârșit pedeapsă</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" class="form-control datepicker" placeholder="zi/luna/an" data-inputmask="'mask': '99/99/9999'"
                    name="cond_pedeapsa_data_end" value="<?php echo set_value('cond_pedeapsa_data_end', !empty($info_condamnare['cond_pedeapsa_data_end']) ? Calendar::convertDateMysql2Ro($info_condamnare['cond_pedeapsa_data_end']) : ''); ?>">
                <?php echo form_error('cond_pedeapsa_data_end'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Document ieșire</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" class="form-control" name="cond_iesire_doc"
                    value="<?php echo set_value('cond_iesire_doc', $info_condamnare['cond_iesire_doc'] ?? '') ?>" />
                <?php echo form_error('cond_iesire_doc'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Observații</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <textarea class="form-control" name="observatii"><?php echo set_value('observatii', $info_condamnare['observatii'] ?? '') ?></textarea>
                <?php echo form_error('observatii'); ?>
            </div>
        </div>
    </div>
</form>

<!-- SUMAR -->
<?php
if (!empty($list_condamnari) && $edit): ?>
    <table class="table table-hover table_functii">
        <thead>
            <tr>
                <th style="width: 25%">Episod represiv</th>
                <th>Perioadă represiune (dată început / dată ieșire)</th>
                <th>Durata ani</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($list_condamnari as $item): ?>
                <tr>
                    <td><?= $item['represiune_tip_id'] ? $list_represiune_tipuri[(int)$item['represiune_tip_id']] : '' ?></td>
                    <td>
                        <?= Calendar::convertDateMysql2Ro($item['represiune_data_start']) ?: '--' ?> /
                        <?= Calendar::convertDateMysql2Ro($item['represiune_data_end']) ?: '--' ?>
                    </td>

                    <td><?= $item['cond_pedeapsa_durata'] ?: '' ?></td>

                    <td>
                        <a href="/personaje/editeaza/<?= $info['personaj_id'] ?>?tab=episoade_represive&condamnareID=<?= $item['id'] ?>" class="btn btn-sm btn-primary">
                            <i class="fa fa-edit bigger-110"></i>
                        </a>

                        <a href="/personaje/stergeCondamnare/<?= $info['personaj_id'] . '/' . $item['id']; ?>" class="btn btn btn-danger btn-sm btnDelete"
                            data-name=""
                            title="Sterge definitiv condamnarea">
                            <i class="fa fa-trash-o"></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>

<!-- end SUMAR -->