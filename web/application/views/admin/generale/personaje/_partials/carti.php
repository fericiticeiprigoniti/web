<!-- Tab - Carti -->
<?php echo validation_errors() ?>

<form name="personaj_carti"
    data-parsley-validate
    class="form-horizontal form-label-left formular formular_carti ajaxForm"
    method="post"
    action="<?php echo '/personaje/' . (($edit) ? 'editeaza/' . $edit : 'adauga') . '?tab=carti'; ?>"
    data-redirect-url="/personaje/editeaza/<?= $edit ?>?tab=carti">

    <div class="col-lg-6 col-md-6 col-sm-12 p0">
        <div class="x_content">
            <table class="table_carti_personaj table jambo_table wide">
                <thead>
                    <tr>
                        <th width='50px'>ID</th>
                        <th width='100px'>Autor</th>
                        <th>Titlu carte</th>
                        <th>An</th>
                        <th>Tip</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    if (!empty($info['pers_carti'])) {
                        foreach ($info['pers_carti'] as $key => $item_p) { ?>
                            <tr data-id="<?php echo $item_p['id']; ?>">
                                <td><?= $item_p['carte_id'] ?></td>
                                <td><?= !empty($item_p['autori']) ? $item_p['autori'] : '' ?></td>
                                <td><?= !empty($item_p['carte_titlu']) ? $item_p['carte_titlu'] : ''; ?></td>
                                <td><?= !empty($item_p['anul_publicatiei']) ? $item_p['anul_publicatiei'] : ''; ?></td>
                                <td><?= !empty($item_p['carte_tip']) ? $list_personaje2carti_tip[$item_p['carte_tip']] : ''; ?></td>
                                <td>
                                    <a class="btn btn btn-danger btnDelete"
                                        href="/personaje/stergePers2Carte/<?= $info['personaj_id'] . '/' . $item_p['p2c_id']; ?>"
                                        data-name="Cartea <?= $item_p['carte_titlu'] ?>"
                                        title="Sterge definitiv legatura dintre carte si personaj">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </td>
                            </tr>
                    <?php }
                    } ?>
                </tbody>
            </table>
        </div>
    </div>

    <!-- tabelul cu toate cartile din biblioteca -->
    <div class="col-lg-6 col-md-6 col-sm-12 p0 vtop">
        <div class="x_content" style="float:none">
            <table id="biblioteca" class="table_carti_sursa table jambo_table wide">
                <thead>
                    <tr>
                        <th width='30px'>ID</th>
                        <th width='100px'>Autor</th>
                        <th style="max-width:200px">Titlu carte</th>
                        <th width="50px">An</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>

        </div>
    </div>
    <!-- end Biblioteca -->

</form>
<!-- end Tab - Functii - distinctii -->

<script type="text/javascript">
    $(document).ready(function() {
        $('.table_carti_personaj').dataTable({
            "sDom": '<"top"f>rt<"bottom"p><"clear">'
        })

        $('#biblioteca').dataTable({
            "sDom": '<"top"f>rt<"bottom"p><"clear">',
            "processing": true,
            "serverSide": true,
            "ajax": "/personaje/getListaCartiBiblioteca",
            "iDisplayLength": 15,
            "order": [
                [0, "desc"],
                [2, "asc"]
            ]
        });

        $('body').on('click', '.importrow', function() {
            //$('.table_carti_personaj > tbody > tr:first')
            var row = $(this).parents('tr'),
                bookID = $(row).find('.row_id').text();

            // mark the row and remove the arrow
            $(row).addClass('selected id_' + bookID);
            $(row).find('.importrow').hide();

            $("<tr data-bid='" + bookID + "'>\
            <td><input type='hidden' name='book[]' value='" + bookID + "'/>" + bookID + "</td>\
            <td>" + $(row).find('.row_autor').text() + "</td>\
            <td>" + $(row).find('.row_titlu').text() + "</td>\
            <td>" + $(row).find('.row_an').text() + "</td>\
            <td>\
                <select name='booktip_" + bookID + "' data-placeholder='selecteaza'>\
                    <option value='1'>autor</option>\
                    <option value='2'>despre personaj</option>\
                </select>\
            </td>\
            <td><a href='javascript:void(0)'><span class='btn btn-default fa fa-remove red delete_row'></span></a></td>\
        </tr>").prependTo('.table_carti_personaj > tbody');
        })

        /* delete temp rows */
        $('.table').on('click', '.delete_row', function() {
            var bookID = $(this).parents('tr').data('bid');

            $('.table_carti_sursa tr.id_' + bookID).removeClass('selected');
            $('.table_carti_sursa tr.id_' + bookID).find('.importrow').show();
            $(this).parents('tr').remove();
        })

    })
</script>