<?php

/**
 * @var object $filters
 * @var object $order
 * @var array $nationalitati
 * @var array $roluri
 * @var array $ocupatii
 * @var array $confesiuni
 * @var array $originiSociale
 * @var array $locuriPatimire
 * @var array $lunileAnului
 * @var bool $hasAnyFilter
 */

$extraSorting = array(
	'id' => 'ID',
	'avatar_poza_id' => 'Poza',
	'nume_complet' => 'Nume, Prenume',
	'data_nastere' => 'Data nasterii',
	'data_adormire' => 'Data adormirii',
	'nr_ocupatii' => 'Numar ocupatii',
	'nr_roluri' => 'Numar roluri',
	'zi_adormire' => 'Ziua adormirii'
);

$tipuriSex = array(
	'm' => 'Masculin',
	'f' => 'Feminin',
);

$tipuriStatus = array(
	'in_lucru' => 'In lucru (Unpublished)',
	'activ' => 'Activ (Published)',
	'sters' => 'Sters',
);

?>
<div class="row">
	<div class="col-xs-12">
		<div class="x_panel">

			<div class="x_title">
				<h2><i class="fa fa-filter"></i> Filtre</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li>
						<a class="collapse-link <?= !$hasAnyFilter ? 'colapsed' : '' ?>">
							<i class="fa <?= $hasAnyFilter ? 'fa-chevron-up' : 'fa-chevron-down' ?>"></i>
						</a>
					</li>
				</ul>
				<div class="clear"></div>
			</div>

			<div class="x_content" style="<?= !$hasAnyFilter ? 'display:none;' : '' ?>">
				<form class="filter-form form-horizontal form-label-left" action="<?= current_url() ?>" method="get">
					<input type="hidden" name="_page" value="1" />
					<input type="hidden" name="_per_page" value="<?= get('_per_page') ?>" />

					<div class="row">
						<div class="col-sm-12 space-10"></div>
					</div>
					<div class="row">

						<!-- Column 1 -->
						<div class="col-sm-4 col-xs-12">

							<div class="form-group has-feedback">
								<input type="hidden" id="nume_id" name="nume_id" value="<?= html_escape($filters->nume_id) ?>" />
								<input type="text" class="form-control devbridge-autocomplete" placeholder="nume complet"
									data-id="nume_id" data-url="/personaje/cautaNume"
									value="<?= html_escape($filters->nume_complet) ?>" />
								<span class="fa fa-search form-control-feedback right" aria-hidden="true"></span>
							</div>

							<div class="form-group has-feedback">
								<select class="form-control" name="rol[]" multiple data-placeholder="rolul personajului">
									<option></option>
									<?php foreach ($roluri as $oValue => $oName): ?>
										<?php $selected = in_array($oValue, $filters->rol) ? 'selected="selected"' : ''; ?>
										<option <?= $selected ?> value="<?= html_escape($oValue) ?>"><?= html_escape($oName) ?></option>
									<?php endforeach; ?>
								</select>
								<span class="fa fa-sort form-control-feedback right" aria-hidden="true"></span>
							</div>

							<div class="form-group has-feedback">
								<select class="form-control" name="confesiune[]" multiple data-placeholder="confesiune">
									<option></option>
									<?php foreach ($confesiuni as $oValue => $oName): ?>
										<?php $selected = in_array($oValue, $filters->confesiune) ? 'selected="selected"' : ''; ?>
										<option <?= $selected ?> value="<?= html_escape($oValue) ?>"><?= html_escape($oName) ?></option>
									<?php endforeach; ?>
								</select>
								<span class="fa fa-sort form-control-feedback right" aria-hidden="true"></span>
							</div>

							<div class="form-group has-feedback">
								<select class="form-control" name="nationalitate[]" multiple data-placeholder="nationalitatea">
									<option></option>
									<?php foreach ($nationalitati as $oValue => $oName): ?>
										<?php $selected = in_array($oValue, $filters->nationalitate) ? 'selected="selected"' : ''; ?>
										<option <?= $selected ?> value="<?= html_escape($oValue) ?>"><?= html_escape($oName) ?></option>
									<?php endforeach; ?>
								</select>
								<span class="fa fa-sort form-control-feedback right" aria-hidden="true"></span>
							</div>

							<div class="form-group">
								<select class="form-control allowClear" name="luna_adormire" data-placeholder="luna adormire">
									<option></option>
									<?php foreach ($lunileAnului as $oValue => $oName): ?>
										<?php $selected = ($oValue == $filters->luna_adormire) ? 'selected="selected"' : ''; ?>
										<option <?= $selected ?> value="<?= html_escape($oValue) ?>"><?= html_escape($oName) ?></option>
									<?php endforeach; ?>
								</select>
							</div>

						</div>


						<!-- Column 2 -->
						<div class="col-sm-4 col-xs-12">

							<div class="form-group has-feedback">
								<select class="form-control" name="ocupatie_consacrata[]" multiple data-placeholder="ocupatie consacrata">
									<option></option>
									<?php foreach ($ocupatii as $oValue => $oName): ?>
										<?php $selected = in_array($oValue, $filters->ocupatie_consacrata) ? 'selected="selected"' : ''; ?>
										<option <?= $selected ?> value="<?= html_escape($oValue) ?>"><?= html_escape($oName) ?></option>
									<?php endforeach; ?>
								</select>
								<span class="fa fa-sort form-control-feedback right" aria-hidden="true"></span>
							</div>

							<div class="form-group has-feedback">
								<select class="form-control" name="ocupatie_arestare[]" multiple data-placeholder="ocupatie la arestare">
									<option></option>
									<?php foreach ($ocupatii as $oValue => $oName): ?>
										<?php $selected = in_array($oValue, $filters->ocupatie_arestare) ? 'selected="selected"' : ''; ?>
										<option <?= $selected ?> value="<?= html_escape($oValue) ?>"><?= html_escape($oName) ?></option>
									<?php endforeach; ?>
								</select>
								<span class="fa fa-sort form-control-feedback right" aria-hidden="true"></span>
							</div>

							<div class="form-group has-feedback">
								<select class="form-control" name="loc_surghiun[]" multiple data-placeholder="loc de surghiun">
									<option></option>
									<?php foreach ($locuriPatimire['items'] as $oValue => $oName): ?>
										<?php $selected = in_array($oValue, $filters->loc_surghiun) ? 'selected="selected"' : ''; ?>
										<option <?= $selected ?> value="<?= html_escape($oValue) ?>"><?= html_escape($oName) ?></option>
									<?php endforeach; ?>
								</select>
								<span class="fa fa-sort form-control-feedback right" aria-hidden="true"></span>
							</div>

							<div class="form-group has-feedback">
								<select class="form-control" name="origine_sociala[]" multiple data-placeholder="origine sociala">
									<option></option>
									<?php foreach ($originiSociale as $oValue => $oName): ?>
										<?php $selected = in_array($oValue, $filters->origine_sociala) ? 'selected="selected"' : ''; ?>
										<option <?= $selected ?> value="<?= html_escape($oValue) ?>"><?= html_escape($oName) ?></option>
									<?php endforeach; ?>
								</select>
								<span class="fa fa-sort form-control-feedback right" aria-hidden="true"></span>
							</div>

						</div>


						<!-- Column 3 -->
						<div class="col-sm-4 col-xs-12">

							<div class="form-group">
								<select class="form-control allowClear" name="status" data-placeholder="status">
									<option></option>
									<?php foreach ($tipuriStatus as $oValue => $oName): ?>
										<?php $selected = ($oValue == $filters->status) ? 'selected="selected"' : ''; ?>
										<option <?= $selected ?> value="<?= html_escape($oValue) ?>"><?= html_escape($oName) ?></option>
									<?php endforeach; ?>
								</select>
							</div>

							<div class="form-group">
								<select class="form-control allowClear" name="sex" data-placeholder="sexul personajului">
									<option></option>
									<?php foreach ($tipuriSex as $oValue => $oName): ?>
										<?php $selected = ($oValue == $filters->sex) ? 'selected="selected"' : ''; ?>
										<option <?= $selected ?>value="<?= html_escape($oValue) ?>"><?= html_escape($oName) ?></option>
									<?php endforeach; ?>
								</select>
							</div>

							<div class="form-group has-feedback">
								<input type="text" class="form-control devbridge-autocomplete" placeholder="ID personaj"
									data-id="id" data-url="/personaje/cautaId"
									id="id" name="id" value="<?= html_escape($filters->id) ?>" />
								<span class="fa fa-search form-control-feedback right" aria-hidden="true"></span>
							</div>

							<div class="form-group has-feedback">
								<input type="hidden" id="loc_nastere" name="loc_nastere" value="<?= html_escape($filters->loc_nastere) ?>" />
								<input type="text" class="form-control devbridge-autocomplete" placeholder="locul nasterii"
									data-id="loc_nastere" data-url="/personaje/cautaLocalitate"
									value="<?= html_escape($filters->loc_nastere_nume_complet) ?>" />
								<span class="fa fa-search form-control-feedback right" aria-hidden="true"></span>
							</div>

						</div>

					</div>

					<div class="ln_solid" style="margin-top: 10px"></div>

					<div class="row">
						<div class="form-group col-md-8 col-sm-6 col-xs-12"></div>
						<div class="form-group col-md-3 col-sm-4 col-xs-8">
							<select class="form-control allowClear" name="_order_by" data-placeholder="sortare...">
								<option></option>
								<?php foreach ($extraSorting as $oValue => $oName): ?>
									<?php $selected = ($oValue == $order->by) ? 'selected="selected"' : ''; ?>
									<option <?= $selected ?>value="<?= html_escape($oValue) ?>"><?= html_escape($oName) ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="form-group col-md-1 col-sm-2 col-xs-4" style="padding-left: 0">
							<select class="form-control" name="_order_dir">
								<option <?= ($order->dir == 'asc') ? 'selected="selected"' : '' ?> value="asc">ASC</option>
								<option <?= ($order->dir == 'desc') ? 'selected="selected"' : '' ?> value="desc">DESC</option>
							</select>
						</div>
					</div>

					<div class="form-group panel-footer" style="text-align: right">
						<a href="<?= current_url() ?>" class="btn btn-warning">
							<i class="fa fa-refresh"></i> Resetare
						</a>
						<button type="submit" class="btn btn-success">
							<i class="fa fa-search"></i> Filtrare
						</button>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>