<!-- TAB - Generale (informatii)-->
<?php
/**
 * @var PersonajItem $objPersonaj
 */
?>

<?= validation_errors(); ?>
<form id="personaj_generale"
    name="personaj_generale"
    method="post"
    class="formular formular_autor form-horizontal form-label-left ajaxForm"
    action="<?= '/personaje/' . (($edit) ? 'editeaza/' . $edit : 'adauga') . '?tab=generale' ?>">

    <div class="col-lg-6 col-md-6 col-sm-12">

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nume</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" class="form-control" name="nume" value="<?= set_value('nume', $objPersonaj->getNume()) ?>">
                <?= form_error('nume'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Prenume</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" class="form-control" name="prenume" value="<?= set_value('prenume', $objPersonaj->getPrenume()) ?>">
                <?= form_error('prenume'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nume anterior casatorie</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" class="form-control" name="nume_anterior" value="<?= set_value('nume_anterior', $objPersonaj->getNumeAnterior()) ?>">
                <?= form_error('nume_anterior'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Prenume monahism</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" class="form-control" name="prenume_monahism" value="<?= set_value('prenume_monahism', $objPersonaj->getPrenumeMonahism()) ?>">
                <?= form_error('prenume_monahism'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Poreclă</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" class="form-control" name="nume_porecla" value="<?= set_value('nume_porecla', $objPersonaj->getNumePorecla()) ?>">
                <?= form_error('nume_porecla'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Pseudonim</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" class="form-control" name="nume_pseudonim" value="<?= set_value('nume_pseudonim', $objPersonaj->getNumePseudonim()) ?>">
                <?= form_error('nume_pseudonim'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Prefix profesional</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" class="form-control" name="prefix" value="<?= set_value('prefix', $objPersonaj->getPrefix()) ?>">
                <?= form_error('prefix'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Alias</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" class="form-control" name="alias" value="<?= set_value('alias', $objPersonaj->getAlias()) ?>">
                <?= form_error('alias'); ?>
            </div>
        </div>

        <hr />

        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <label class="control-label">Data nastere</label>
                <input type="text" class="form-control" name="data_nastere" placeholder="zi/luna/an" data-inputmask="'mask': '99/99/9999'" value="<?= set_value('data_nastere', Calendar::convertDateMysql2Ro($objPersonaj->getDataNastere())) ?>">
                <?= form_error('data_nastere') ?>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <label class="control-label">Locul nasterii</label>
                <input type="hidden" id="nastere_loc_id" name="nastere_loc_id" value="<?= $objPersonaj->getNastereLocId() ?>">
                <input type="text" class="form-control devbridge-autocomplete" placeholder="cauta localitatea"
                    data-id="nastere_loc_id" data-url="/personaje/cautaLocalitate"
                    value="<?= $objPersonaj->getLocNastere() ? html_escape($objPersonaj->getLocNastere()->getNumeComplet()) : '' ?>" />
                <?= form_error('nastere_loc_id'); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <label class="control-label">Dată adormire</label>
                <input type="text" id="data_adormire" class="form-control" placeholder="zi/luna/an"
                    data-inputmask="'mask': '99/99/9999'" name="data_adormire"
                    value="<?= set_value('data_adormire', (($edit && $objPersonaj->getDataAdormire()) ? date('d.m.Y', strtotime($objPersonaj->getDataAdormire())) : '')) ?>">
                <?= form_error('data_adormire'); ?>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12">
                <label class="control-label">Locul înmormântării</label>
                <input type="hidden" id="deces_loc_inmormantare_id" name="deces_loc_inmormantare_id" value="<?= $objPersonaj->getDecesLocInmormantareId() ?>">
                <input type="text" class="form-control devbridge-autocomplete" placeholder="cauta localitatea"
                    data-id="deces_loc_inmormantare_id" data-url="/personaje/cautaLocalitate"
                    value="<?= $objPersonaj->getLocInmormantare() ? html_escape($objPersonaj->getLocInmormantare()->getNumeComplet()) : '' ?>" />
                <?= form_error('deces_loc_inmormantare_id'); ?>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <label class="control-label">Locul morții</label>
                <input type="text" class="form-control" name="deces_locul_mortii"
                    value="<?= set_value('deces_locul_mortii', $objPersonaj->getDecesLoculMortii()) ?>">
                <?= form_error('deces_locul_mortii'); ?>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12">
                <label class="control-label">Deces nume cimitir</label>
                <input type="text" class="form-control" name="deces_nume_cimitir"
                    value="<?= set_value('deces_nume_cimitir', $objPersonaj->getDecesNumeCimitir()) ?>">
                <?= form_error('deces_nume_cimitir'); ?>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <label class="control-label">Coord. mormânt (Lat)</label>
                <input type="text" class="form-control" name="deces_mormant_coord_lat"
                    value="<?= set_value('deces_mormant_coord_lat', $objPersonaj->getDecesMormantCoordLat()) ?>">
                <?= form_error('deces_mormant_coord_lat'); ?>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12">
                <label class="control-label">Coord. mormânt (Long)</label>
                <input type="text" class="form-control" name="deces_mormant_coord_long"
                    value="<?= set_value('deces_mormant_coord_long', $objPersonaj->getDecesMormantCoordLong()) ?>">
                <?= form_error('deces_mormant_coord_long'); ?>
            </div>
        </div>

        <hr />

        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <label class="control-label">Total ani detenție</label>
                <input type="number" class="form-control" name="durata_detentie"
                    value="<?= set_value('durata_detentie', $objPersonaj->getDurataDetentie()) ?>">
                <?= form_error('durata_detentie'); ?>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12">
                <label class="control-label">Total ani prizonierat</label>
                <input type="number" class="form-control" name="durata_prizonierat"
                    value="<?= set_value('durata_prizonierat', $objPersonaj->getDurataPrizonierat()) ?>">
                <?= form_error('durata_prizonierat'); ?>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <label class="control-label">Total ani deportare</label>
                <input type="number" class="form-control" name="durata_deportare"
                    value="<?= set_value('durata_deportare', $objPersonaj->getDurataDeportare()) ?>">
                <?= form_error('durata_deportare'); ?>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12">
                <label class="control-label">Total ani domiciliu obligatoriu</label>
                <input type="number" class="form-control" name="durata_domiciliu_oblig"
                    value="<?= set_value('durata_domiciliu_oblig', $objPersonaj->getDurataDomiciliuOblig()) ?>">
                <?= form_error('durata_domiciliu_oblig'); ?>
            </div>
        </div>

        <hr />

        <div class="row">
            <div class="col-md-6">
                <label class="control-label">Nr. copii (băieți)</label>
                <input type="number" class="form-control" name="count_copii_b"
                    value="<?= set_value('count_copii_b', $objPersonaj->getCountCopiiB()) ?>">
                <?= form_error('count_copii_b'); ?>
            </div>

            <div class="col-md-6">
                <label class="control-label">Nr. copii (fete)</label>
                <input type="number" class="form-control" name="count_copii_f"
                    value="<?= set_value('count_copii_f', $objPersonaj->getCountCopiiF()) ?>">
                <?= form_error('count_copii_f'); ?>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-6">
                <label class="control-label">Prenume tată</label>
                <input type="text" class="form-control" name="prenume_tata"
                    value="<?= set_value('prenume_tata', $objPersonaj->getPrenumeTata()) ?>">
                <?= form_error('prenume_tata'); ?>
            </div>

            <div class="col-md-6">
                <label class="control-label">Prenume mamă</label>
                <input type="text" class="form-control" name="prenume_mama"
                    value="<?= set_value('prenume_mama', $objPersonaj->getPrenumeMama()) ?>">
                <?= form_error('prenume_mama'); ?>
            </div>
        </div>


        <div class="form-group">
            <label class="control-label col-3">Observații personă / critica la opera lui</label>
            <div class="col-md-12 col-sm-9 col-xs-12">
                <textarea name="observatii" class="tinymce form-control" style="height:100px"><?= set_value('observatii', $objPersonaj->getObservatii()) ?></textarea>
                <?= form_error('observatii'); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Autor observație</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <select name="observatie_autor" class="form-control select2 allowClear" data-placeholder="selecteaza">
                    <option></option>
                    <?php foreach ($list_aauth_users as $objUser) { ?>
                        <option value="<?= $objUser->getId() ?>" <?= set_select('observatie_autor', $objPersonaj->getObservatiiUserId(), $objUser->getId() == $objPersonaj->getObservatiiUserId()) ?>><?= $objUser->getFullname() . " (" . $objUser->getEmail() . ")" ?></option>
                    <?php } ?>
                </select>
                <?= form_error('observatie_autor'); ?>
            </div>
        </div>

    </div>
    <!-- end LEFT COLUMN -->

    <!-- right COLUMN -->
    <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="form-group alert alert-info2">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <select name="status_id" class="form-control select2" data-placeholder="selecteaza">
                    <option></option>
                    <option value="1" <?= set_select('', $objPersonaj->getStatusId(), $objPersonaj->getStatusId() == 1) ?>>finalizat/publicat</option>
                    <option value="0" <?= set_select('', $objPersonaj->getStatusId(), $objPersonaj->getStatusId() == 0) ?>>în lucru</option>
                </select>
                <?= form_error('status_id'); ?>
            </div>
        </div>

        <div class="alert alert-info2">
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Detinut politic</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <select name="is_detinut_politic" class="form-control allowClear" data-placeholder="selecteaza" data-placeholder="selecteaza">
                        <option></option>
                        <option value="1" <?= set_select('', $objPersonaj->getIsDetinutPolitic(), !is_null($objPersonaj->getIsDetinutPolitic()) && $objPersonaj->getIsDetinutPolitic() == 1) ?>>da</option>
                        <option value="0" <?= set_select('', $objPersonaj->getIsDetinutPolitic(), !is_null($objPersonaj->getIsDetinutPolitic()) && $objPersonaj->getIsDetinutPolitic() == 0) ?>>nu</option>
                    </select>
                    <?= form_error('is_detinut_politic'); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Sex</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <select name="sex" class="form-control allowClear" data-placeholder="selecteaza" data-placeholder="selecteaza">
                        <option></option>
                        <option value="1" <?= set_select('sex', $objPersonaj->getSex(), !is_null($objPersonaj->getSex()) && $objPersonaj->getSex() == 1) ?>>bărbat</option>
                        <option value="0" <?= set_select('sex', $objPersonaj->getSex(), !is_null($objPersonaj->getSex()) && $objPersonaj->getSex() == 0) ?>>femeie</option>
                    </select>
                    <?= form_error('sex'); ?>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Confesiune</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <select name="confesiune_id" class="form-control select2 allowClear" data-placeholder="selecteaza">
                        <option></option>
                        <?php echo_select_options($list_confesiuni, set_value('confesiune_id', !empty($objPersonaj->getConfesiuneId()) ? $objPersonaj->getConfesiuneId() : false), array('id', 'nume')) ?>
                    </select>
                    <?= form_error('confesiune_id'); ?>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Naționalitate</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <select name="nationalitate_id" class="form-control select2 allowClear" data-placeholder="selecteaza">
                        <option></option>
                        <?php echo_select_options($list_nationalitati, set_value('nationalitate_id', $objPersonaj->getNationalitateId()), array('id', 'nume')) ?>
                    </select>
                    <?= form_error('nationalitate_id'); ?>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Ocupatii socio-profesionale</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" class="form-control" name="ocupatii_socioprofesionale" value="<?= set_value('ocupatii_socioprofesionale', $objPersonaj->getOcupatiiSocioprofesionale()) ?>">
                    <?= form_error('ocupatii_socioprofesionale') ?>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Origine sociale</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <select name="orig_sociala_id" class="form-control select2 allowClear" data-placeholder="selecteaza">
                        <option></option>
                        <?php echo_select_options($list_origini_sociale, $objPersonaj->getOrigSocialaId(), array('id', 'nume')) ?>
                    </select>
                    <?= form_error('orig_sociala_id'); ?>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Roluri</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <p>
                        <?php foreach ($list_roluri as $key => $rol) { ?>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="roluri[]" class="flat" value="<?= $key ?>" <?= !empty($edit) && in_array($key, $objPersonaj->getRoluri(true)) ? 'checked="checked"' : '' ?>> <?= $rol ?>
                        </label>
                    </div>
                <?php } ?>
                </p>
                <?= form_error('roluri'); ?>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Observații interne</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <textarea name="observatii_interne" class="form-control" style="height:100px"><?= set_value('observatii_interne', (($edit) ? $info['observatii_interne'] : '')) ?></textarea>
                    <?= form_error('observatii_interne'); ?>
                </div>
            </div>
        </div>

        <div class="form-group alert alert-info2">
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">SEO Meta description</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <textarea name="seo_metadescription" class="form-control" style="height:100px"><?= set_value('obserseo_metadescriptionvatii', $objPersonaj->getSeoMetadescription()) ?></textarea>
                    <?= form_error('seo_metadescription'); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">SEO Meta Keywords</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <input name="seo_metakeywords" class="form-control" value="<?= set_value('seo_metakeywords', $objPersonaj->getSeoMetakeywords()) ?>" />
                    <?= form_error('seo_metakeywords') ?>
                </div>
            </div>
        </div>
    </div>
    <!-- end RIGHT COLUMN -->

</form>