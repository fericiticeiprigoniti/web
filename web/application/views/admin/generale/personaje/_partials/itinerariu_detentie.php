<!-- Tab - Itinerariu detentie -->
<form name="personaj_itinerariu_detentie"
    data-parsley-validate
    class="form-horizontal form-label-left formular formular_pedepse ajaxForm"
    method="post"
    data-redirect-url=""
    action="<?= '/personaje/' . (($edit) ? 'editeaza/' . $edit : 'adauga') . '?tab=itinerariu_detentie'; ?>">

    <div class="col-lg-12 col-md-12 col-sm-12 pr0">
        <div class="x_content">
            <table class="table table-hover table_itinerarii">
                <thead>
                    <tr>
                        <th class="col-md-2">Loc pătimire</th>
                        <th class="col-md-2">Data start (zi/luna/an)</th>
                        <th class="col-md-2">Data end (zi/luna/an)</th>
                        <th class="col-md-3">Episod represiv</th>
                        <th class="col-md-3">Observatii</th>
                        <th class="col-md-1"></th>
                    </tr>
                </thead>
                <tbody id="sortable">
                    <tr class="wizard_add_itinerarii">
                        <td>
                            <select name="loc_patimire_id" class="form-control allowClear loc_patimire_id" data-placeholder="selecteaza" required>
                                <option></option>
                                <?php echo_select_options($list_locuri_patimire, set_value('loc_patimire_id'), array('_key', '_value')); ?>
                            </select>
                        </td>
                        <td>
                            <div>
                                <input type="text" id="col11" name="data_start_zi" class="form-control data_start_zi" value="" data-inputmask="'mask': '9[9]'" />
                                <input type="text" id="col12" name="data_start_luna" class="form-control data_start_luna" value="" data-inputmask="'mask': '9[9]'" />
                                <input type="text" id="col13" name="data_start_an" class="form-control data_start_an" value="" data-inputmask="'mask': '9999'" />
                            </div>
                        </td>

                        <td>
                            <div>
                                <input type="text" id="col21" name="data_end_zi" class="form-control data_end_zi" value="" data-inputmask="'mask': '9[9]'" />
                                <input type="text" id="col22" name="data_end_luna" class="form-control data_end_luna" value="" data-inputmask="'mask': '9[9]'" />
                                <input type="text" id="col23" name="data_end_an" class="form-control data_end_an" value="" data-inputmask="'mask': '9999'" />
                            </div>
                        </td>

                        <td>
                            <select name="condamnare_id" class="form-control allowClear condamnare_id" data-placeholder="selecteaza" required>
                                <option></option>
                                <?php echo_select_options($pers_list_condamnari, set_value('condamnare_id'), array('_key', '_value')) ?>
                            </select>
                        </td>

                        <td>
                            <textarea name="observatii" class="form-control observatii"></textarea>
                        </td>

                        <td>
                            <a href="javascript:void(0)" id="addRow" class="btn btn-success"><span class="fa fa-plus"></span></a>
                        </td>
                    </tr>

                    <?php
                    // repopuleaza din POST
                    if (!empty($new_rows)) {
                        foreach ($new_rows as $key => $item) { ?>
                            <tr class="ui-state-default">
                                <td>
                                    <input type="text" name="new_loc_patimire_id<?= $key ?>" value="<?= $item['loc_patimire_id']; ?>" style="max-width:100px;" class="form-control loc_patimire_id" />
                                    <?= form_error('new_loc_patimire_id' . $key); ?>
                                </td>

                                <td>
                                    <div>
                                        <input type="text" name="new_data_start_zi_<?= $key ?>" class="form-control data_start_zi" value="<?= $item['data_start_zi']; ?>" data-inputmask="'mask': '9[9]'" />
                                        <input type="text" name="new_data_start_luna_<?= $key ?>" class="form-control data_start_luna" value="<?= $item['data_start_luna']; ?>" data-inputmask="'mask': '9[9]'" />
                                        <input type="text" name="new_data_start_an_<?= $key ?>" class="form-control data_start_an" value="<?= $item['data_start_an']; ?>" data-inputmask="'mask': '9999'" />
                                    </div>
                                    <?php foreach (array('zi', 'luna', 'an') as $time_interval) {
                                        echo form_error('new_data_start_' . $time_interval . '_' . $key);
                                    } ?>
                                </td>
                                <td>
                                    <div>
                                        <input type="text" name="new_data_end_zi_<?= $key ?>" class="form-control data_end_zi" value="<?= $item['data_end_zi']; ?>" data-inputmask="'mask': '9[9]'" />
                                        <input type="text" name="new_data_end_luna_<?= $key ?>" class="form-control data_end_luna" value="<?= $item['data_end_luna']; ?>" data-inputmask="'mask': '9[9]'" />
                                        <input type="text" name="new_data_end_an_<?= $key ?>" class="form-control data_end_an" value="<?= $item['data_end_an']; ?>" data-inputmask="'mask': '9999'" />
                                    </div>
                                    <?php foreach (array('zi', 'luna', 'an') as $time_interval) {
                                        echo form_error('new_data_end_' . $time_interval . '_' . $key);
                                    } ?>
                                </td>

                                <td>
                                    <input type="text" name="new_condamnare_id_<?= $key ?>" value="<?= $item['condamnare_id']; ?>" style="max-width:100px;" class="form-control condamnare_id" />
                                    <?= form_error('new_condamnare_id_' . $key); ?>
                                </td>

                                <td>
                                    <textarea class="form-control observatii" name="new_observatii_<?= $key ?>"><?= $item['observatii'] ?></textarea>
                                    <?= form_error('new_observatii_' . $key); ?>
                                </td>

                                <td>
                                    <a href="javascript:void(0)" class=""><span class="btn btn-default fa fa-remove red delete_row"></span></a>
                                </td>
                            </tr>
                    <?php }
                    } ?>

                    <?php


                    if (!empty($info['pers_itinerarii'])): ?>
                        <?php foreach ($info['pers_itinerarii'] as $key => $item_p): ?>
                            <tr data-id="<?= $item_p['id']; ?>">
                                <td><?= $list_locuri_patimire[$item_p['loc_patimire_id']] ?? null ?></td>
                                <td><?= !empty($item_p['data_start']) ? Calendar::convertIntDate2Ro($item_p['data_start']) : '' ?></td>
                                <td><?= !empty($item_p['data_end']) ? Calendar::convertIntDate2Ro($item_p['data_end']) : '' ?></td>
                                <td><?= $item_p['condamnare'] ?: '' ?></td>
                                <td><?= $item_p['observatii'] ?: '' ?></td>
                                <td>
                                    <a class="btn btn btn-danger btnDelete"
                                        href="/personaje/stergeItinerariu/<?= $info['personaj_id'] . '/' . $item_p['id']; ?>"
                                        data-name="itinerariu cu Loc patimire *<?= $list_locuri_patimire[$item_p['loc_patimire_id']] ?? null ?>*"
                                        title="Sterge definitiv itinerariu">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
            </table>

            <a href="javascript:void(0)" id="save_order_itDetentie" class="btn btn-success">Salvează ordinea</a>

        </div>
    </div>

</form>
<!-- end Tab - Functii - distinctii -->

<script type="text/javascript">
    $(document).ready(function() {
        var counter_d = 0;

        $('.wizard_add_itinerarii').on('click', '#addRow', function() {
            var row = $(this).parents('tr');
            var patimire = $(row).find('.loc_patimire_id').select2('data'),
                loc_patimireID = $(row).find('.loc_patimire_id').val(),
                loc_patimire = (loc_patimireID !== null) ? patimire['0']['text'] : '';

            var cond = $(row).find('.condamnare_id').select2('data'),
                condamnareID = $(row).find('.condamnare_id').val(),
                condamnare = (condamnareID !== null && cond && cond[0]) ? cond[0]['text'] : '';

            var data_start_zi = $(row).find('.data_start_zi').val(),
                data_start_luna = $(row).find('.data_start_luna').val(),
                data_start_an = $(row).find('.data_start_an').val(),
                data_end_zi = $(row).find('.data_end_zi').val(),
                data_end_luna = $(row).find('.data_end_luna').val(),
                data_end_an = $(row).find('.data_end_an').val(),
                observatii = $(row).find('.observatii').val();


            validateRequiredFields($(row));
            if (!loc_patimireID || !condamnareID) return;

            $(".table_itinerarii").find('tbody')
                .append($('<tr>')
                    .append($('<td>')
                        .append($('<input>')
                            .attr('type', 'hidden')
                            .attr('name', 'new_loc_patimire_id_' + counter_d)
                            .attr('value', loc_patimireID)
                        )
                        .append('<label>' + loc_patimire + '</label>')
                    )

                    .append($('<td>')
                        .append($('<input>')
                            .attr('type', 'text')
                            .attr('name', 'new_data_start_zi_' + counter_d)
                            .attr('value', data_start_zi)
                            .attr('class', $('#col11').attr('class'))
                        )
                        .append($('<input>')
                            .attr('type', 'text')
                            .attr('name', 'new_data_start_luna_' + counter_d)
                            .attr('value', data_start_luna)
                            .attr('class', $('#col12').attr('class'))
                        )
                        .append($('<input>')
                            .attr('type', 'text')
                            .attr('name', 'new_data_start_an_' + counter_d)
                            .attr('value', data_start_an)
                            .attr('class', $('#col13').attr('class'))
                        )
                    )

                    .append($('<td>')
                        .append($('<input>')
                            .attr('type', 'text')
                            .attr('name', 'new_data_end_zi_' + counter_d)
                            .attr('value', data_end_zi)
                            .attr('class', $('#col21').attr('class'))
                        )
                        .append($('<input>')
                            .attr('type', 'text')
                            .attr('name', 'new_data_end_luna_' + counter_d)
                            .attr('value', data_end_luna)
                            .attr('class', $('#col22').attr('class'))
                        )
                        .append($('<input>')
                            .attr('type', 'text')
                            .attr('name', 'new_data_end_an_' + counter_d)
                            .attr('value', data_end_an)
                            .attr('class', $('#col23').attr('class'))
                        )
                    )

                    .append($('<td>')
                        .append($('<input>')
                            .attr('type', 'hidden')
                            .attr('name', 'new_condamnare_id_' + counter_d)
                            .attr('value', condamnareID)
                        )
                        .append('<label>' + condamnare + '</label>')
                    )
                    .append($('<td>')
                        .append($('<input>')
                            .attr('type', 'text')
                            .attr('name', 'new_observatii_' + counter_d)
                            .attr('value', observatii)
                        )
                    )

                    .append($('<td>')
                        .append($('<a>')
                            .attr('href', 'javascript:void(0)')
                            .attr('class', 'btn btn-default fa fa-remove red delete_row')
                        )
                    )
                );

            counter_d++;
            resetForm($('.wizard_add_itinerarii'));
        })

        $('body').on('click', '#save_order_itDetentie', function() {
            var order = Array();
            $('#sortable tr').each(function(index, value) {
                if (index == 0) return;
                order[index] = $(value).data('id');
            })

            $.ajax({
                type: 'POST',
                url: '/personaje/orderItinerariiDetentie',
                dataType: 'json',
                data: {
                    'order': order
                },
                success: function(data) {
                    if (data.success) {
                        showNotification({
                            type: 'success',
                            title: NOTICE_TITLE_SUCCESS,
                            text: 'Ordinea a fost salvată!'
                        });
                    } else {
                        alert('Lista a fost salvata partial sau deloc. Incearca din nou!');
                    }
                }
            })

            return false;
        })

        $('.table').on('click', '.delete_row', function() {
            $(this).parents('tr').remove();
        })

        // order all elements
        $("#sortable").sortable({
            placeholder: "ui-state-highlight"
        });
        $("#sortable").disableSelection();
    })
</script>