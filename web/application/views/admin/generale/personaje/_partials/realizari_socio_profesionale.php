<!-- Tab - Functii - distinctii -->
<?php echo validation_errors(); ?>
<form name="personaj_functii_distinctii"
    data-parsley-validate class="form-horizontal form-label-left formular formular_functii ajaxForm"
    method="post" action="<?php echo '/personaje/' . (($edit) ? 'editeaza/' . $edit : 'adauga') . '?tab=functii'; ?>">

    <div class="col-lg-12 col-md-12 col-sm-12 pr0">
        <div class="x_content">
            <table class="table table-hover table_realizari">
                <thead>
                    <tr>
                        <th>Tip realizare (fcp_personaje_realizari_tipuri)</th>
                        <th>Descriere</th>
                        <th>Data inceput</th>
                        <th>Data sfarsit</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                    <tr class="wizard_add_realizare">
                        <td>
                            <input type="text" value="" name="realizare_tip" class="form-control realizare_tip" required />
                        </td>
                        <td>
                            <input type="text" value="" name="realizare_input" class="form-control realizare_input" required />
                        </td>
                        <td>
                            <input type="text" id="col11" name="distinctie_data_inceput" class="form-control realizare_data_inceput" value="" data-inputmask="" />
                        </td>
                        <td>
                            <input type="text" id="col11" name="distinctie_data_sfarsit" class="form-control realizare_data_sfarsit" value="" data-inputmask="" />
                        </td>
                        <td>
                            <a href="javascript:void(0)" id="addRealizare" class="btn btn-success"><span class="fa fa-plus"></span></a>
                        </td>
                    </tr>

                    <?php
                    // repopuleaza din POST
                    if (!empty($new_rows_realizari)) {
                        foreach ($new_rows_realizari as $key => $item) { ?>
                            <tr>
                                <td>
                                    <input type="text" name="new_realizare_input_<?php echo $key; ?>" value="<?php echo $item['distinctie']; ?>" />
                                    <?php echo !empty($item['distinctie']) ? $item['distinctie'] : ''; ?>
                                </td>
                                <td>
                                    <input type="text" name="new_realizare_data_inceput_<?php echo $key; ?>" class="form-control realizare_data_inceput" value="<?php echo $item['data_inceput']; ?>" data-inputmask="" />
                                </td>
                                <td>
                                    <input type="text" name="new_realizare_data_sfarsit_<?php echo $key; ?>" class="form-control realizare_data_sfarsit" value="<?php echo $item['data_sfarsit']; ?>" data-inputmask="" />
                                </td>
                                <td>
                                    <a href="javascript:void(0)" class=""><span class="btn btn-default fa fa-remove red delete_row"></span></a>
                                </td>
                            </tr>
                    <?php }
                    } ?>
                    <?php
                    if (!empty($info['pers_realizarisocioprofesionale'])) {
                        foreach ($info['pers_realizarisocioprofesionale'] as $key => $item_dist) { ?>
                            <tr>
                                <td>todo - realizare tip</td>
                                <td><?= $item_dist['titlu_primit']; ?></td>
                                <td><?= $item_dist['data_inceput'] ?? '' ?></td>
                                <td><?= $item_dist['data_sfarsit'] ?? '' ?></td>
                                <td>
                                    <a class="btn btn btn-danger btnDelete"
                                        href="/personaje/stergRealizare/<?= $info['personaj_id'] . '/' . $item_dist['id'] ?>"
                                        data-name="distinctia <?php echo $item_dist['titlu_primit']; ?>"
                                        title="Sterge definitiv realizarea">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </td>
                            </tr>
                    <?php }
                    } ?>
            </table>

        </div>
    </div>

</form>
<!-- end Tab - Realizari socio profesionale -->

<script type="text/javascript">
    $(document).ready(function() {
        var counter_f = 0,
            counter_d = 0;

        $('.wizard_add_realizare').on('click', '#addRealizare', function() {
            var row = $(this).parents('tr'),
                text_input = $(row).find('.realizare_input').val(),
                realizare_tio = $(row).find('.select_realizare_tip').val(),
                data_inceput = $(row).find('.data_inceput').val(),
                data_sfarsit = $(row).find('.data_sfarsit').val();

            validateRequiredFields($(row));
            if (!text_input) return;

            $(".table_realizari").find('tbody')
                .append($('<tr>')
                    .append($('<td>')
                        .append($('<input>')
                            .attr('type', 'hidden')
                            .attr('name', 'new_realizare_' + counter_f)
                            .attr('value', realizare_tip)
                        )
                        .append($('<span>')
                            .text($(row).find('.select_realizare_tip option:selected').text())
                        )
                    )
                    .append($('<td>')
                        .append($('<input>')
                            .attr('type', 'text')
                            .attr('name', 'new_realizare_input_' + counter_d)
                            .attr('value', text_input)
                            .attr('class', 'form-control')
                        )
                    )
                    .append($('<td>')
                        .append($('<input>')
                            .attr('type', 'text')
                            .attr('name', 'new_realizare_data_inceput_' + counter_d)
                            .attr('value', data_inceput)
                            .attr('class', $('#col11').attr('class'))
                        )
                    )
                    .append($('<td>')
                        .append($('<input>')
                            .attr('type', 'text')
                            .attr('name', 'new_realizare_data_sfarsit_' + counter_d)
                            .attr('value', data_sfarsit)
                            .attr('class', $('#col11').attr('class'))
                        )
                    )
                    .append($('<td>')
                        .append($('<a>')
                            .attr('href', 'javascript:void(0)')
                            .attr('class', 'btn btn-default fa fa-remove red delete_row')
                        )
                    )
                );

            counter_d++;
            resetForm($('.wizard_add_realizare'));
        })

    })
</script>