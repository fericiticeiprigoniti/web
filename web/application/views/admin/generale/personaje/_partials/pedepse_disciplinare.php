<!-- Tab - Pedepse disciplinare -->
<form name="personaj_pedepse_disciplinare"
    data-parsley-validate
    class="form-horizontal form-label-left formular formular_pedepse ajaxForm"
    method="post"
    data-redirect-url=""
    action="<?php echo '/personaje/' . (($edit) ? 'editeaza/' . $edit : 'adauga') . '?tab=pedepse_disciplinare'; ?>">

    <div class="col-lg-12 col-md-12 col-sm-12 p0">
        <div class="x_content">
            <table class="table table-hover table_pedepse jambo_table">
                <thead>
                    <tr>
                        <th>Data start (zi/luna/an)</th>
                        <th>Data end (zi/luna/an)</th>
                        <th>Nr zile</th>
                        <th>Descrierea faptei</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="sortable">
                    <tr class="wizard_add_pedepse">

                        <td>
                            <div>
                                <input type="text" id="col11" name="data_start_zi" class="form-control data_start_zi" value="" data-inputmask="'mask': '9[9]'" />
                                <input type="text" id="col12" name="data_start_luna" class="form-control data_start_luna" value="" data-inputmask="'mask': '9[9]'" />
                                <input type="text" id="col13" name="data_start_an" class="form-control data_start_an" value="" data-inputmask="'mask': '9999'" />
                            </div>
                        </td>
                        <td>
                            <div>
                                <input type="text" id="col21" name="data_end_zi" class="form-control data_end_zi" value="" data-inputmask="'mask': '9[9]'" />
                                <input type="text" id="col22" name="data_end_luna" class="form-control data_end_luna" value="" data-inputmask="'mask': '9[9]'" />
                                <input type="text" id="col23" name="data_end_an" class="form-control data_end_an" value="" data-inputmask="'mask': '9999'" />
                            </div>
                        </td>
                        <td>
                            <input type="text" id="col3" name="nr_zile" style="max-width:100px;" class="form-control nr_zile" value="" />
                        </td>
                        <td>
                            <input type="text" id="col4" value="" name="pedeapsa_input" class="form-control pedeapsa_input" required />
                        </td>
                        <td>
                            <a href="javascript:void(0)" id="addPedeapsa" class="btn btn-success"><span class="fa fa-plus"></span></a>
                        </td>
                    </tr>

                    <?php
                    // repopuleaza din POST
                    if (!empty($new_rows)) {
                        foreach ($new_rows as $key => $item) { ?>
                            <tr class="ui-state-default">
                                <td>
                                    <div>
                                        <input type="text" name="new_data_start_zi_<?php echo $key; ?>" class="form-control data_start_zi" value="<?php echo $item['data_start_zi']; ?>" data-inputmask="'mask': '9[9]'" />
                                        <input type="text" name="new_data_start_luna_<?php echo $key; ?>" class="form-control data_start_luna" value="<?php echo $item['data_start_luna']; ?>" data-inputmask="'mask': '9[9]'" />
                                        <input type="text" name="new_data_start_an_<?php echo $key; ?>" class="form-control data_start_an" value="<?php echo $item['data_start_an']; ?>" data-inputmask="'mask': '9999'" />
                                    </div>
                                    <?php foreach (array('zi', 'luna', 'an') as $time_interval) {
                                        echo form_error('new_data_start_' . $time_interval . '_' . $key);
                                    } ?>
                                </td>
                                <td>
                                    <div>
                                        <input type="text" name="new_data_end_zi_<?= $key; ?>" class="form-control data_end_zi" value="<?= $item['data_end_zi']; ?>" data-inputmask="'mask': '9[9]'" />
                                        <input type="text" name="new_data_end_luna_<?= $key; ?>" class="form-control data_end_luna" value="<?= $item['data_end_luna']; ?>" data-inputmask="'mask': '9[9]'" />
                                        <input type="text" name="new_data_end_an_<?= $key; ?>" class="form-control data_end_an" value="<?= $item['data_end_an']; ?>" data-inputmask="'mask': '9999'" />
                                    </div>
                                    <?php foreach (array('zi', 'luna', 'an') as $time_interval) {
                                        echo form_error('new_data_end_' . $time_interval . '_' . $key);
                                    } ?>
                                </td>
                                <td>
                                    <input type="text" name="new_nr_zile_<?= $key; ?>" value="<?php echo $item['nr_zile']; ?>" style="max-width:100px;" class="form-control nr_zile" />
                                    <?php echo form_error('new_nr_zile_' . $key); ?>
                                </td>
                                <td>
                                    <input type="text" name="new_pedeapsa_input_<?= $key; ?>" value="<?php echo !empty($item['pedeapsa_input']) ? $item['pedeapsa_input'] : ''; ?>" class="form-control pedeapsa_input" required />
                                    <?php echo form_error('new_pedeapsa_input_' . $key); ?>
                                </td>

                                <td>
                                    <a href="javascript:void(0)" class=""><span class="btn btn-default fa fa-remove red delete_row"></span></a>
                                </td>
                            </tr>
                    <?php }
                    } ?>
                    <?php
                    if (!empty($info['pers_pedepse'])) {
                        foreach ($info['pers_pedepse'] as $key => $item_p) { ?>
                            <tr data-id="<?php echo $item_p['id']; ?>">
                                <td><?php echo !empty($item_p['data_start']) ? Calendar::convertIntDate2Ro($item_p['data_start']) : ''; ?></td>
                                <td><?php echo !empty($item_p['data_end']) ? Calendar::convertIntDate2Ro($item_p['data_end']) : ''; ?></td>
                                <td><?php echo !empty($item_p['nr_zile']) ? $item_p['nr_zile'] : ''; ?></td>
                                <td><?php echo $item_p['content']; ?></td>

                                <td>
                                    <a class="btn btn btn-danger btnDelete"
                                        href="/personaje/stergePedeapsa/<?php echo $info['personaj_id'] . '/' . $item_p['id']; ?>"
                                        data-name="pedeapsa <?php echo $item_p['content']; ?>"
                                        title="Sterge definitiv pedeapsa">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </td>
                            </tr>
                    <?php }
                    } ?>
            </table>

            <a href="javascript:void(0)" id="save_order_pedepse" class="btn btn-success">Salvează ordinea</a>

        </div>
    </div>

</form>
<!-- end Tab - Functii - distinctii -->

<script type="text/javascript">
    $(document).ready(function() {
        var counter_d = 0;

        $('.wizard_add_pedepse').on('click', '#addPedeapsa', function() {
            var row = $(this).parents('tr'),
                text_input = $(row).find('.pedeapsa_input').val(),
                nr_zile = $(row).find('.nr_zile').val(),
                data_start_zi = $(row).find('.data_start_zi').val(),
                data_start_luna = $(row).find('.data_start_luna').val(),
                data_start_an = $(row).find('.data_start_an').val(),
                data_end_zi = $(row).find('.data_end_zi').val(),
                data_end_luna = $(row).find('.data_end_luna').val(),
                data_end_an = $(row).find('.data_end_an').val();

            validateRequiredFields($(row));
            if (!text_input) return;

            $(".table_pedepse").find('tbody')
                .append($('<tr>')
                    .append($('<td>')
                        .append($('<input>')
                            .attr('type', 'text')
                            .attr('name', 'new_data_start_zi_' + counter_d)
                            .attr('value', data_start_zi)
                            .attr('class', $('#col11').attr('class'))
                        )
                        .append($('<input>')
                            .attr('type', 'text')
                            .attr('name', 'new_data_start_luna_' + counter_d)
                            .attr('value', data_start_luna)
                            .attr('class', $('#col12').attr('class'))
                        )
                        .append($('<input>')
                            .attr('type', 'text')
                            .attr('name', 'new_data_start_an_' + counter_d)
                            .attr('value', data_start_an)
                            .attr('class', $('#col13').attr('class'))
                        )
                    )

                    .append($('<td>')
                        .append($('<input>')
                            .attr('type', 'text')
                            .attr('name', 'new_data_end_zi_' + counter_d)
                            .attr('value', data_end_zi)
                            .attr('class', $('#col21').attr('class'))
                        )
                        .append($('<input>')
                            .attr('type', 'text')
                            .attr('name', 'new_data_end_luna_' + counter_d)
                            .attr('value', data_end_luna)
                            .attr('class', $('#col22').attr('class'))
                        )
                        .append($('<input>')
                            .attr('type', 'text')
                            .attr('name', 'new_data_end_an_' + counter_d)
                            .attr('value', data_end_an)
                            .attr('class', $('#col23').attr('class'))
                        )
                    )

                    .append($('<td>')
                        .append($('<input>')
                            .attr('type', 'text')
                            .attr('name', 'new_nr_zile_' + counter_d)
                            .attr('value', nr_zile)
                            .attr('class', $('#col3').attr('class'))
                        )
                    )
                    .append($('<td>')
                        .append($('<input>')
                            .attr('type', 'text')
                            .attr('name', 'new_pedeapsa_input_' + counter_d)
                            .attr('value', text_input)
                            .attr('class', $('#col4').attr('class'))
                        )
                    )
                    .append($('<td>')
                        .append($('<a>')
                            .attr('href', 'javascript:void(0)')
                            .attr('class', 'btn btn-default fa fa-remove red delete_row')
                        )
                    )
                );

            counter_d++;
            resetForm($('.wizard_add_pedepse'));
        })

        $('body').on('click', '#save_', function() {
            var order = Array();
            $('#sortable tr').each(function(index, value) {
                if (index == 0) return;
                order[index] = $(value).data('id');
            })

            $.ajax({
                type: 'POST',
                url: '/personaje/orderPedepseDisciplinare',
                dataType: 'json',
                data: {
                    'order': order
                },
                success: function(data) {
                    console.log('asd');
                    console.log(data);
                    if (data.success) {
                        showNotification({
                            type: 'success',
                            title: NOTICE_TITLE_SUCCESS,
                            text: 'Ordinea a fost salvată!'
                        });
                    } else {
                        alert('Lista a fost salvata partial sau deloc. Incearca din nou!');
                    }
                }
            })

            return false;
        })

        $('.table').on('click', '.delete_row', function() {
            $(this).parents('tr').remove();
        })

        // order all elements
        $("#sortable").sortable({
            placeholder: "ui-state-highlight"
        });
        $("#sortable").disableSelection();

    })
</script>