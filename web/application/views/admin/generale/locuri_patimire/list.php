<?php

/**
 * @var array $items
 * @var string $pagination
 * @var array $pag
 */
?>

<!-- FILTRE -->
<?php $this->load->view('/admin/generale/locuri_patimire/_partials/filtre') ?>

<style>
	.labelStatus {
		display: inline-block;
		width: 80px;
	}
</style>

<div class="row">
	<div class="col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2><i class="fa fa-table"></i> Locuri pătimire</h2>
				<div class="clear"></div>
			</div>

			<div class="x_content">

				<div class="table-responsive">

					<table class="table table-striped table-bordered table-hover table-highlight dataTable">
						<thead>
							<tr>
								<th class="center" style="width:50px">Nr. crt.</th>
								<?= $this->load->view('/_partial/table/thSort', array('name' => 'id', 'label' => 'ID', 'order' => $order), true) ?>
								<?= $this->load->view('/_partial/table/thSort', array('name' => 'locatie_nume', 'label' => 'Nume loc pătimire', 'order' => $order), true) ?>
								<?= $this->load->view('/_partial/table/thSort', array('name' => 'tip_loc_surghiun', 'label' => 'Tip loc pătimire', 'order' => $order), true) ?>
								<?= $this->load->view('/_partial/table/thSort', array('name' => 'localitate_nume', 'label' => 'Nume Localitate', 'order' => $order), true) ?>
								<?= $this->load->view('/_partial/table/thSort', array('name' => 'judet_nume', 'label' => 'Județ', 'order' => $order), true) ?>
								<?= $this->load->view('/_partial/table/thSort', array('name' => 'destinatie', 'label' => 'Destinație', 'order' => $order), true) ?>
								<th class="actions">
									<a href="/locuri_patimire/edit/" class="btn btn-info btn-sm" role="button" title="Add">
										<i class="fa fa-plus"></i>
										<span class="hidden-sm hidden-xs"> Add</span>
									</a>
								</th>
							</tr>
						</thead>

						<?php if (count($items)): ?>
							<tbody>
								<?php $i = 0 ?>
								<?php foreach ($items as $k => $v): ?>
									<tr>
										<td class="center"><?= ++$i ?></td>
										<td class="center"><?= html_escape($v['id']) ?></td>
										<td><?= html_escape($v['locatie_nume']) ?>
											<?php if (!empty($v['locatie_cod_unitate'])): ?>
												<br><small>(<?= $v['locatie_cod_unitate'] ?>)</small>
											<?php endif; ?>
										</td>
										<td><?= html_escape($v['tip_loc_surghiun']) ?></td>
										<td><?= html_escape($v['localitate_nume']) ?></td>
										<td><?= html_escape($v['judet_nume']) ?></td>
										<td><?= html_escape($v['destinatie']) ?></td>
										<td class="actions" style="white-space: nowrap">
											<a href="/locuri_patimire/edit/<?= (int)$v['id'] ?>" class="btn btn-sm btn-primary">
												<i class="fa fa-edit bigger-110"></i> <span class="hidden-sm hidden-xs">Edit</span>
											</a>

											<?php if (!$v['is_deleted']) { ?>
												<a href="/locuri_patimire/delete/<?= (int)$v['id'] ?>" class="btn btn-sm btn-danger btnDelete"
													data-name="categoria <?= html_escape($v['tip_loc_surghiun']) ?>">
													<i class="fa fa-trash-o bigger-110"></i><span class="hidden-sm hidden-xs"> Delete</span>
												</a>
											<?php } ?>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						<?php else: ?>
							<tbody>
								<tr>
									<td colspan="50" class="nodata"><?= html_escape('No data found') ?></td>
								</tr>
							</tbody>
						<?php endif; ?>

						<tfoot>
							<tr>
								<th colspan="50" class="panel-footer">
									<div style="float: left">
										<?= $this->load->view('/_partial/table/paginator', $pag, true) ?>
									</div>
									<div style="float: right">
										<?= $pagination ?>
									</div>
								</th>
							</tr>
						</tfoot>
					</table>

				</div>

			</div>
		</div>

	</div>
</div>