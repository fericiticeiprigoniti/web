<?php

/**
 * @var ArticolItem $descriere
 * @var int|null $edit
 * @var array $list_carti
 * @var array $list_useri
 * @var array $list_useri
 */

?>

<form id="personaj_biografie"
    name="personaj_biografie"
    method="post"
    class="formular formular_autor form-horizontal form-label-left ajaxForm"
    action="<?php echo '/locuri_patimire/' . ($edit ? 'edit/' . $edit : 'add') . '?tab=descriere'; ?>">

    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="form-group">
            <label class="control-label">Titlu <span class="req">*</span></label>
            <input type="text" name="titlu" class="form-control" value="<?= set_value('titlu', $descriere ? $descriere->getTitlu() : '') ?>" placeholder="Titlul articolului" />
            <?= form_error('titlu'); ?>
        </div>

        <div class="form-group">
            <label class="control-label">Conținut <span class="req">*</span></label>
            <textarea name="continut" class="tinymce form-control" style="height:50px"><?php echo set_value('continut', ($descriere ? $descriere->getContinut() : '')) ?></textarea>
            <?php echo form_error('continut'); ?>
        </div>

        <div class="form-group">
            <label class="control-label">Cartea</label>
            <select name="carte_id" class="form-control allowClear" data-placeholder="selecteaza">
                <option></option>
                <?php echo_select_options($list_carti, $descriere ? $descriere->getSursaDocCarteId() : '', array('_key', '_value')) ?>
            </select>
            <?php echo form_error('carte_id'); ?>
        </div>

        <div class="form-group">
            <label class="control-label">Paginație</label>
            <div class="">
                <input type="number" name="pag_start" class="col-lg-6" placeholder="pagina inceput" value="<?php echo set_value('pag_start', ($descriere ? $descriere->getSursaDocStartPage() : '')) ?>" />
                <input type="number" name="pag_end" class="col-lg-6" placeholder="pagina sfarsit" value="<?php echo set_value('pag_end', ($descriere ? $descriere->getSursaDocEndPage() : '')) ?>" />
                <?php echo form_error('pag_start'); ?>
                <?php echo form_error('pag_end'); ?>
            </div>
            <br />
        </div>

        <div class="form-group">
            <label class="control-label">Sursa link</label>
            <input type="text" name="sursa_link" class="form-control" value="<?php echo set_value('sursa_link', $descriere ? $descriere->getSursaLink() : '') ?>" placeholder="https://example.com/source-link" />
            <?= form_error('sursa_link'); ?>
        </div>

        <div class="form-group">
            <label class="control-label">Sursa titlu</label>
            <input type="text" name="sursa_titlu" class="form-control" value="<?= set_value('sursa_titlu', $descriere ? $descriere->getSursaTitlu() : '') ?>" />
            <?php echo form_error('sursa_titlu'); ?>
        </div>

        <div class="form-group">
            <label class="control-label">Autor articol</label>

            <select name="user_id" class="form-control select2 allowClear" data-placeholder="selecteaza">
                <option></option>
                <?php foreach ($list_useri as $user) { ?>
                    <option value="<?= $user->id ?>" <?= set_select('user_id', $descriere ? $descriere->getUserId() : '', ($descriere && $user->id == $descriere->getUserId())) ?>><?= $user->email ?></option>
                <?php } ?>
            </select>
            <?= form_error('user_id'); ?>

        </div>

    </div>
    <!-- end LEFT COLUMN -->

    <!-- right COLUMN -->
    <div class="col-lg-3 col-md-3 col-sm-12">
        <div class="form-group alert alert-info2">
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <?php $statusId = $descriere && $descriere->isPublished() ?>
                    <select name="status_id" class="form-control select2" data-placeholder="selecteaza">
                        <option value="0" <?= set_select('', $statusId, $statusId == 0) ?>>nepublicat</option>
                        <option value="1" <?= set_select('', $statusId, $statusId == 1) ?>>publicat</option>
                    </select>
                    <?= form_error('status_id'); ?>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Data publicării</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" class="form-control datepicker" placeholder="zi/luna/an"
                        data-inputmask="'mask': '99/99/9999'" name="data_publicare"
                        value="<?= set_value('data_publicare', $descriere && $descriere->getDataPublicare() ? $descriere->getDataPublicare()->format('d/m/Y') : '') ?>" />
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Taguri</label>
                <div class="col-md-9 col-sm-9 col-xs-12">

                    // TODO
                    // multiselect pentru tags

                </div>
            </div>
        </div>

        <div class="clearfix"></div>



    </div>
    <!-- end RIGHT COLUMN -->

</form>