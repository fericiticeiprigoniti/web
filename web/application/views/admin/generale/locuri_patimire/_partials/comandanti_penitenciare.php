<?php

/**
 * @var array $list_carti
 */

?>

<!-- Tab - Functii - distinctii -->
<?php echo validation_errors(); ?>
<form name="personaj_functii_distinctii"
    data-parsley-validate class="form-horizontal form-label-left formular formular_comandanti ajaxForm"
    method="post" action="<?php echo '/locuri_patimire/' . (($edit) ? 'edit/' . $edit : 'add') . '?tab=comandanti_penitenciare'; ?>">

    <div class="col-lg-12 col-md-12 col-sm-12 pr0">
        <div class="x_content">
            <table class="table table-hover table_comandanti">
                <thead>
                    <tr>
                        <th>Comandant penitenciar <span class="req">*</span></th>
                        <th>Grad militar</th>
                        <th>Data început (zi/luna/an)</th>
                        <th>Data sfârșit (zi/luna/an)</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="wizard_add_comandant">
                        <td>

                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <select class="form-control select2 select_comandant allowClear" data-placeholder="Comandant penitenciar">
                                    <option></option>
                                    <?php echo_select_options($list_comandanti, null, ['_key', '_value']) ?>
                                </select>
                                <?php echo form_error('comandant_id'); ?>
                            </div>

                        </td>
                        <td>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <select class="form-control select2 select_grad allowClear" data-placeholder="Grad militar">
                                    <option></option>
                                    <?php echo_select_options($list_grade_militare, null, ['_key', '_value']) ?>
                                </select>
                                <?php echo form_error('grad_id'); ?>
                            </div>
                        </td>
                        <td>
                            <div>
                                <input type="text" id="col11" class="form-control data_start_zi" value="" data-inputmask="'mask': '9[9]'" />
                                <input type="text" id="col12" class="form-control data_start_luna" value="" data-inputmask="'mask': '9[9]'" />
                                <input type="text" id="col13" class="form-control data_start_an" value="" data-inputmask="'mask': '9999'" />
                            </div>
                        </td>
                        <td>
                            <div>
                                <input type="text" id="col21" class="form-control data_end_zi" value="" data-inputmask="'mask': '9[9]'" />
                                <input type="text" id="col22" class="form-control data_end_luna" value="" data-inputmask="'mask': '9[9]'" />
                                <input type="text" id="col23" class="form-control data_end_an" value="" data-inputmask="'mask': '9999'" />
                            </div>
                        </td>
                        <td>
                            <a href="javascript:void(0)" id="addComandant" class="btn btn-success"><span class="fa fa-plus"></span></a>
                        </td>
                    </tr>

                    <?php
                    // repopuleaza din POST
                    if (!empty($new_rows_comandanti)) {
                        foreach ($new_rows_comandanti as $key => $item) { ?>
                            <tr>
                                <td>
                                    <input type="text" name="new_comandant_input_<?php echo $key; ?>" value="<?php echo $item['comandant']; ?>" />
                                    <?php echo !empty($item['comandant']) ? $item['comandant'] : ''; ?>
                                </td>
                                <td>
                                    <div>
                                        <input type="text" name="new_comandant_data_zi_<?php echo $key; ?>" class="form-control data_start_zi" value="<?php echo $item['comandant_data_zi']; ?>" data-inputmask="'mask': '9[9]'" />
                                        <input type="text" name="new_comandant_data_luna_<?php echo $key; ?>" class="form-control data_start_luna" value="<?php echo $item['comandant_data_luna']; ?>" data-inputmask="'mask': '9[9]'" />
                                        <input type="text" name="new_comandant_data_an_<?php echo $key; ?>" class="form-control data_start_an" value="<?php echo $item['comandant_data_an']; ?>" data-inputmask="'mask': '9999'" />
                                    </div>
                                    <?php foreach (array('zi', 'luna', 'an') as $time_interval) {
                                        echo form_error('new_comandant_data_' . $time_interval . '_' . $key);
                                    } ?>
                                </td>
                                <td>
                                    <a href="javascript:void(0)" class=""><span class="btn btn-default fa fa-remove red delete_row"></span></a>
                                </td>
                            </tr>
                    <?php }
                    } ?>
                    <?php
                    if (!empty($info['locuri_patimire_comandanti'])) {
                        foreach ($info['locuri_patimire_comandanti'] as $key => $item_comandant) { ?>
                            <tr>
                                <td><?php echo $item_comandant['comandant_nume']; ?></td>
                                <td><?php echo $item_comandant['grad_militar_nume']; ?></td>
                                <td><?php echo !empty($item_comandant['data_start']) ? Calendar::convertDateMysql2Ro($item_comandant['data_start']) : ''; ?></td>
                                <td><?php echo !empty($item_comandant['data_end']) ? Calendar::convertDateMysql2Ro($item_comandant['data_end']) : ''; ?></td>
                                <td>
                                    <a class="btn btn btn-danger btnDelete"
                                        href="/locuri_patimire/stergeComandant/<?php echo $item_comandant['id']; ?>"
                                        data-name="comandantul <?php echo $item_comandant['comandant_nume']; ?>"
                                        title="Sterge definitiv comandantul">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </td>
                            </tr>
                    <?php }
                    } ?>
            </table>

        </div>
    </div>

</form>
<!-- end Tab - Functii - distinctii -->

<script type="text/javascript">
    $(document).ready(function() {
        var counter_c = 0;

        $('.wizard_add_comandant').on('click', '#addComandant', function() {
            var row = $(this).parents('tr');
            let comandant_nume = $(row).find('.select_comandant option:selected').text();
            let grad_militar_nume = $(row).find('.select_grad option:selected').text();
            let comandant_id = $(row).find('.select_comandant').val();
            let grad_militar_id = $(row).find('.select_grad').val();
            let data_start_zi = $(row).find('.data_start_zi').val();
            let data_start_luna = $(row).find('.data_start_luna').val();
            let data_start_an = $(row).find('.data_start_an').val();
            let data_end_zi = $(row).find('.data_end_zi').val();
            let data_end_luna = $(row).find('.data_end_luna').val();
            let data_end_an = $(row).find('.data_end_an').val();

            validateRequiredFields($(row));
            if (!comandant_nume) return;

            $(".table_comandanti").find('tbody')
                .append($('<tr>')

                    .append($('<td>')
                        .append($('<span>')
                            .text(comandant_nume)
                            .css('font-weight', 'bold')
                        )
                        .append($('<input>')
                            .attr('type', 'hidden')
                            .attr('name', 'new_comandant_comandant_id_' + counter_c)
                            .attr('value', comandant_id)
                        )
                    )

                    .append($('<td>')
                        .append($('<span>')
                            .text(grad_militar_nume)
                            .css('font-weight', 'bold')
                        )
                        .append($('<input>')
                            .attr('type', 'hidden')
                            .attr('name', 'new_comandant_grad_militar_id_' + counter_c)
                            .attr('value', grad_militar_id)
                        )
                    )

                    .append($('<td>')
                        .append($('<input>')
                            .attr('type', 'text')
                            .attr('name', 'new_comandant_data_start_zi_' + counter_c)
                            .attr('value', data_start_zi)
                            .attr('class', $('#col11').attr('class'))
                        )
                        .append($('<input>')
                            .attr('type', 'text')
                            .attr('name', 'new_comandant_data_start_luna_' + counter_c)
                            .attr('value', data_start_luna)
                            .attr('class', $('#col12').attr('class'))
                        )
                        .append($('<input>')
                            .attr('type', 'text')
                            .attr('name', 'new_comandant_data_start_an_' + counter_c)
                            .attr('value', data_start_an)
                            .attr('class', $('#col13').attr('class'))
                        )
                    )

                    .append($('<td>')
                        .append($('<input>')
                            .attr('type', 'text')
                            .attr('name', 'new_comandant_data_end_zi_' + counter_c)
                            .attr('value', data_end_zi)
                            .attr('class', $('#col11').attr('class'))
                        )
                        .append($('<input>')
                            .attr('type', 'text')
                            .attr('name', 'new_comandant_data_end_luna_' + counter_c)
                            .attr('value', data_end_luna)
                            .attr('class', $('#col12').attr('class'))
                        )
                        .append($('<input>')
                            .attr('type', 'text')
                            .attr('name', 'new_comandant_data_end_an_' + counter_c)
                            .attr('value', data_end_an)
                            .attr('class', $('#col13').attr('class'))
                        )
                    )

                    .append($('<td>')
                        .append($('<a>')
                            .attr('href', 'javascript:void(0)')
                            .attr('class', 'btn btn-default fa fa-remove red delete_row')
                        )
                    )
                );

            counter_c++;
            resetForm($('.wizard_add_comandant '));
        });


    })
</script>