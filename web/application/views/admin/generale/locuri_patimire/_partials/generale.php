<!-- TAB - Generale (informatii)-->
<?php
/**
 * @var array $tip_surghiun_list
 * @var array $descriere_articol_list
 */

$tip_surghiun_list = $tip_surghiun_list ?? [];
$descriere_articol_list = $descriere_articol_list ?? [];
?>

<div class="col-md-8 col-md-offset-1">
    <?= validation_errors(); ?>
    <form id="personaj_generale"
        name="personaj_generale"
        method="post"
        class="formular formular_autor form-horizontal form-label-left ajaxForm"
        action="<?= '/locuri_patimire/' . (($edit) ? 'edit/' . $edit : 'add') . '?tab=generale' ?>">


        <?php if ($edit): ?>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">ID</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" class="form-control" value="<?= $edit ?>" readonly />
                </div>
            </div>
        <?php endif; ?>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Tip surghiun <span class="req">*</span></label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <select name="tip_surghiun_id" class="form-control select2 allowClear" data-placeholder="selecteaza">
                    <option></option>
                    <?php echo_select_options($tip_surghiun_list, set_value('tip_surghiun_id', Strings::fill($data['tip_surghiun_id'])), ['_key', '_value']) ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nume locație <span class="req">*</span></label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" class="form-control" name="locatie_nume" value="<?= set_value('locatie_nume', $data['locatie_nume']) ?>">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Supranume locație</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" class="form-control" name="locatie_supranume" value="<?= set_value('locatie_supranume', $data['locatie_supranume']) ?>">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Cod unitate locație</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" class="form-control" name="locatie_cod_unitate" value="<?= set_value('locatie_cod_unitate', $data['locatie_cod_unitate']) ?>">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Coordonate locație (Lat)</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" class="form-control" name="locatie_coord_lat" value="<?= set_value('locatie_coord_lat', $data['locatie_coord_lat']) ?>" />
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Coordonate locație (Long)</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" class="form-control" name="locatie_coord_long" value="<?= set_value('locatie_coord_long', $data['locatie_coord_long']) ?>" />
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Destinație</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="text" class="form-control" name="destinatie" value="<?= set_value('destinatie', $data['destinatie']) ?>">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Localitate <span class="req">*</span></label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <input type="hidden" id="localitate_id" name="localitate_id" value="<?= set_value('localitate_id', $data['localitate_id']) ?>" />
                <input type="text" class="form-control devbridge-autocomplete" placeholder="localitate"
                    data-id="localitate_id" data-url="/locuri_patimire/cautaLocalitate"
                    value="<?= html_escape($data['localitate_nume']) ?>" />
                <span class="fa fa-search form-control-feedback right" aria-hidden="true"></span>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Titlu articol descriere (istoric)</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <select name="descriere_art_id" class="form-control select2 allowClear" data-placeholder="selecteaza">
                    <option></option>
                    <?php echo_select_options($descriere_articol_list, set_value('descriere_art_id', Strings::fill($data['descriere_art_id'])), ['_key', '_value']) ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Sters</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <select name="is_deleted" class="form-control select2" data-placeholder="selecteaza">
                    <option></option>
                    <?php echo_select_options(['0' => 'Nu', '1' => 'Da'], set_value('is_deleted', $data['is_deleted']), ['_key', '_value']) ?>
                </select>
            </div>
        </div>

    </form>
</div>