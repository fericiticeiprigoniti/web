<?php

/**
 * @var array $list_carti
 */

?>

<?php echo validation_errors(); ?>

<div class="col-lg-6">
    <form name="locuri_patimire_citate"
        data-parsley-validate
        class="form-horizontal form-label-left formular formular_autor ajaxForm"
        method="post"
        action="<?php echo '/locuri_patimire/' . (($edit) ? 'edit/' . $edit : 'adauga') . '?tab=citate' . (!empty($citatID) ? '&citatID=' . $citatID : '') ?>"
        data-redirect-url="/locuri_patimire/edit/<?= $edit ?>?tab=citate">

        <div class="form-group">
            <label class="control-label">Conținut <span class="req">*</span></label>
            <textarea name="content" class="tinymcesimple form-control" style="height:50px"><?php echo set_value('content', (!empty($info_citat) ? $info_citat['content'] : '')) ?></textarea>
            <?php echo form_error('content'); ?>
        </div>

        <div class="form-group col-lg-12">
            <label class="control-label">Cartea</label>
            <select name="carte_id" class="form-control allowClear" data-placeholder="selecteaza">
                <option></option>
                <?php echo_select_options($list_carti, (!empty($info_citat) ? $info_citat['carte_id'] : false), array('_key', '_value')) ?>
            </select>
            <?php echo form_error('carte_id'); ?>
        </div>

        <div class="form-group col-lg-12">
            <label class="control-label">Sursa</label>
            <input type="text" name="sursa" class="form-control" value="<?php echo set_value('sursa', (!empty($info_citat) ? $info_citat['sursa'] : '')) ?>" />
            <?php echo form_error('sursa'); ?>
        </div>

        <div class="form-group col-lg-8">
            <label class="control-label">Paginație</label>
            <div class="">
                <input type="number" name="pag_start" class="col-lg-4" placeholder="pagina inceput" value="<?= set_value('pag_start', (!empty($info_citat) ? $info_citat['pag_start'] : '')) ?>" />
                <input type="number" name="pag_end" class="col-lg-4" placeholder="pagina sfarsit" value="<?= set_value('pag_end', (!empty($info_citat) ? $info_citat['pag_end'] : '')) ?>" />
                <?= form_error('pag_start'); ?>
                <?= form_error('pag_end'); ?>
            </div>
        </div>
    </form>
</div>

<?php if (!empty($info['citate']) && $edit) { ?>
    <div class="col-lg-6">

        <table class="table table-hover table_functii">
            <thead>
                <tr>
                    <th width='50px'>Nr</th>
                    <th>Citat</th>
                    <th>Carte/Sursa</th>
                    <th>Pagini</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($info['citate'] as $item) { ?>
                    <tr>
                        <td><?= $item['id'] ?></td>
                        <td><?= $item['content'] ?></td>
                        <td> <?= $item['titlu_carte'] ? $item['titlu_carte'] : '-' ?> / <?= $item['sursa'] ? $item['sursa'] : '-' ?></td>
                        <td>
                            <?php echo ($item['pag_start'] ? $item['pag_start'] : ''); ?>
                            <?php echo ($item['pag_end'] ? '- ' . $item['pag_end'] : ''); ?>
                        </td>

                        <td>
                            <a href="/locuri_patimire/edit/<?= $item['loc_patimire_id'] ?>?tab=citate&citatID=<?= $item['id'] ?>" class="btn btn-sm btn-primary">
                                <i class="fa fa-edit bigger-110"> Edit</i>
                            </a>

                            <a href="/locuri_patimire/stergeCitat/<?= $item['loc_patimire_id'] . '/' . $item['id']; ?>" class="btn btn btn-danger btn-sm btnDelete"
                                data-name="citatul #<?= $item['id'] ?>"
                                title="Sterge definitiv citatul">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    <?php } ?>
    </div>