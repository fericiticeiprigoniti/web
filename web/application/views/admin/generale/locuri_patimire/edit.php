<!-- Main content -->

<?php
if($this->session->flashdata('log')){?>
    <div class="alert alert-success alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <?php echo $this->session->flashdata('log') ?>
    </div>
<?php }?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?php echo isset($autocomplete_page['subtitle'])? $autocomplete_page['subtitle'] : '' ?></h2>

                <ul class="nav navbar-right panel_toolbox">
                    <li>
                        <button class="btn btn-primary" id="ajaxSubmit">
                            <i class="fa fa-save"></i> Save
                        </button>
                    </li>
                </ul>
                <br/>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">

                <input type="hidden" name="tokenID" id="token" value="<?php echo rand(1,999) ?>" />

                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="<?php echo ($tab_activ == 'generale')? 'active' : '' ?>">
                            <a href="#tab_content1" id="generale-tab" role="tab" data-toggle="tab"
                               aria-expanded="<?php echo ($tab_activ == 'generale')? 'true' : 'false' ?>">Date generale</a>
                        </li>
                        <li role="presentation" class="<?php echo ($tab_activ == 'descriere')? 'active' : (!$edit? 'disabled disabledTab' : '') ?>">
                            <a href="#tab_content2" role="tab" id="descriere-tab" <?= $edit ? 'data-toggle="tab"' : '' ?>
                               aria-expanded="<?php echo ($tab_activ == 'descriere')? 'true' : 'false' ?>">Descriere</a>
                        </li>
                        <li role="presentation" class="<?php echo ($tab_activ == 'comandanti_penitenciare')? 'active' : (!$edit? 'disabled disabledTab' : '') ?>">
                            <a href="#tab_content3" role="tab" id="comandanti_penitenciare-tab" <?= $edit ? 'data-toggle="tab"' : '' ?>
                               aria-expanded="<?php echo ($tab_activ == 'comandanti_penitenciare')? 'true' : 'false' ?>">Comandanți penitenciar / lagăr</a>
                        </li>
                        <li role="presentation" class="<?php echo ($tab_activ == 'citate')? 'active' : (!$edit? 'disabled disabledTab' : '') ?>">
                            <a href="#tab_content4" role="tab" id="citate-tab" <?= $edit ? 'data-toggle="tab"' : '' ?>
                               aria-expanded="<?php echo ($tab_activ == 'citate')? 'true' : 'false' ?>">Citate</a>
                        </li>
                    </ul>

                    <!-- Tab Content -->
                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade <?php echo ($tab_activ == 'generale')? 'active in' : '' ?>" id="tab_content1" aria-labelledby="home-tab">
                            <?php $this->load->view('/admin/generale/locuri_patimire/_partials/generale') ?>
                        </div>

                        <?php if ($edit){?>
                            <div role="tabpanel" class="tab-pane fade <?php echo ($tab_activ == 'descriere')? 'active in' : '' ?>" id="tab_content2" aria-labelledby="descriere-tab">
                                <?php $this->load->view('/admin/generale/locuri_patimire/_partials/descriere') ?>
                            </div>

                            <div role="tabpanel" class="tab-pane fade <?php echo ($tab_activ == 'comandanti_penitenciare')? 'active in' : '' ?>" id="tab_content3" aria-labelledby="comandanti_penitenciar-tab">
                                <?php $this->load->view('/admin/generale/locuri_patimire/_partials/comandanti_penitenciare') ?>
                            </div>

                            <div role="tabpanel" class="tab-pane fade <?php echo ($tab_activ == 'citate')? 'active in' : '' ?>" id="tab_content4" aria-labelledby="citate-tab">
                                <?php $this->load->view('/admin/generale/locuri_patimire/_partials/citate') ?>
                            </div>

                            <br><br>
                            <div class="col-xs-12">
                                <div class="panel-footer">
                                    <span class="req">*</span> - <i>Câmpuri obligatorii</i>
                                    <div class="clear"></div>
                                </div>
                            </div>

                        <?php }?>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){

        $('.table').on('click','.delete_row',function(){
            $(this).parents('tr').remove();
        })

        $('.datepicker').datepicker({ dateFormat: 'dd-mm-yy' });
    })
</script>