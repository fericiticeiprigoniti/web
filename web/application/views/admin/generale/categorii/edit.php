<?php

/**
 * @var array $data
 * @var int $edit
 * @var string $page_title
 * @var array $parentItems
 */

?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-edit"></i> <?= $page_title ?></h2>
                <div class="clear"></div>
            </div>

            <div class="x_content">

                <form method="post" class="form-horizontal ajaxForm" action="<?= General::url() ?>" data-redirect-url="/categorii/">
                    <div class="col-md-8 col-md-offset-1">

                        <?php if ($edit): ?>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">ID</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" class="form-control" value="<?= $edit ?>" readonly />
                                </div>
                            </div>
                        <?php endif; ?>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nume <span class="req">*</span></label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control" name="nume" value="<?= set_value('nume', $data['nume']) ?>">
                                <?php echo form_error('nume'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Alias</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control" name="alias" value="<?= set_value('alias', $data['alias']) ?>">
                                <?php echo form_error('alias'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Order id</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="number" class="form-control" min="0" max="255" name="order_id" value="<?= set_value('order_id', $data['order_id']) ?>">
                                <?php echo form_error('alias'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Publicat</label>

                            <div class="col-md-9 col-sm-9 col-xs-12">

                                <select name="is_published" class="form-control select2">
                                    <option value="0" <?php echo empty($data['is_published']) ? 'selected' : '' ?>>nu</option>
                                    <option value="1" <?php echo !empty($data['is_published']) ? 'selected' : '' ?>>da</option>
                                </select>
                            </div>
                            <?php echo form_error('is_published') ?>

                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Părinte</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <select name="parent_id" class="form-control select2 allowClear" data-placeholder="selecteaza parinte...">
                                    <option></option>
                                    <?php echo_select_options($parentItems, $data['parent_id'], array('id', 'nume')) ?>
                                </select>
                                <?php echo form_error('parent_id'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="panel-footer">
                            <span class="req">*</span> - <i>Câmpuri obligatorii</i>
                            <button type="submit" class="btn btn-primary pull-right">
                                <i class="fa fa-save"></i> Salvare
                            </button>
                            <div class="clear"></div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>