<?php

/**
 * @var array $items
 */
?>

<style>
	.labelStatus {
		display: inline-block;
		width: 80px;
	}
</style>

<div class="row">
	<div class="col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2><i class="fa fa-table"></i>
					Categorii articole
					<span class="badge"><?= count($items) ?></span>
				</h2>
				<div class="clear"></div>
			</div>

			<div class="x_content">

				<div class="table-responsive">

					<table class="table table-striped table-bordered table-hover table-highlight dataTable">
						<thead>
							<tr>
								<th class="center">ID</th>
								<th>Titlu</th>
								<th>Parent ID / nume</th>
								<th>Nr. articole publicate</th>
								<th class="actions">
									<div class="hidden-sm hidden-xs">
										<a href="/categorii/add/" class="btn btn-info btn-sm" role="button" style="width:100%">
											<i class="fa fa-plus"></i> Add
										</a>
									</div>
									<div class="hidden-md hidden-lg">
										<a href="/categorii/edit/" class="btn btn-info btn-sm" role="button" title="Add">
											<i class="fa fa-plus"></i>
										</a>
									</div>
								</th>
							</tr>
						</thead>

						<?php if (count($items)): ?>
							<tbody>
								<?php $i = 0 ?>
								<?php foreach ($items as $k => $v): ?>
									<tr>
										<td class="center"><?= html_escape($v['id']) ?></td>
										<td>
											<?= html_escape($v['nume']) ?>
											<div style="float:right" class="float-right text-center">
												<?php if ($v['is_deleted'] == 1): ?>
													<span class="labelStatus label label-danger">Ștearsă</span>
												<?php elseif ($v['is_published'] == 1): ?>
													<span class="labelStatus label label-primary">Publicat</span>
												<?php elseif ($v['is_published'] == 0): ?>
													<span class="labelStatus label label-default">Nepublicat</span>
												<?php else: ?>
													<span class="labelStatus label label-default">Nepublicat</span>
												<?php endif; ?>
											</div>
										</td>
										<td>
											<?= $v['parent_id']
												? html_escape($v['parent_id']) . ' -> ' . html_escape($v['nume_parinte'])
												: ''
											?>
										</td>
										<td><?= html_escape($v['used_in_articles_count']) ?></td>
										<td class="actions" style="white-space: nowrap">
											<?php
											$editUrl = '/categorii/edit/' . (int)$v['id'];
											$deleteUrl = '/categorii/delete/' . (int)$v['id'];
											$restoreUrl = '/categorii/restore/' . (int)$v['id'];
											?>
											<?php if (!$v['is_deleted']): ?>
												<a href="<?= $editUrl ?>" class="btn btn-sm btn-primary" title="Edit">
													<i class="fa fa-edit bigger-110"></i>
													<span class="hidden-sm hidden-xs"> Edit</span>
												</a>
												<a href="<?= $deleteUrl ?>" class="btn btn-sm btn-danger btnDelete softDelete"
													data-name="categoria <?= html_escape($v['nume']) ?>" title="Delete">
													<i class="fa fa-trash-o bigger-110"></i>
													<span class="hidden-sm hidden-xs"> Delete</span>
												</a>
											<?php endif; ?>

											<?php if ($v['is_deleted']): ?>
												<div class="hidden-sm hidden-xs">
													<a href="<?= $restoreUrl ?>" class="btn btn-sm btn-warning ajaxLink withConfirmation" style="width: 100%"
														data-message="Esti sigur ca vrei sa restaurezi categoria <b><?= html_escape($v['nume']) ?></b>?">
														<i class="fa fa-undo bigger-110"></i> Restore
													</a>
												</div>
												<div class="hidden-md hidden-lg">
													<a href="<?= $restoreUrl ?>" class="btn btn-sm btn-warning ajaxLink withConfirmation"
														data-message="Esti sigur ca vrei sa restaurezi categoria <b><?= html_escape($v['nume']) ?></b>?" title="Restore">
														<i class="fa fa-undo bigger-110"></i>
													</a>
												</div>
											<?php endif; ?>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						<?php else: ?>
							<tbody>
								<tr>
									<td colspan="50" class="nodata"><?= html_escape('No data found') ?></td>
								</tr>
							</tbody>
						<?php endif; ?>
					</table>

				</div>

			</div>
		</div>

	</div>
</div>