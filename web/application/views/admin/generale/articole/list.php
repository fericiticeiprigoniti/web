<?php

/**
 * @var ArticolItem[] $items
 * @var object $filters
 * @var object $order
 * @var string $pagination
 * @var array $pag
 */
?>

<!-- FILTRE -->
<?php $this->load->view('admin/generale/articole/_partials/filters') ?>

<div class="row">
	<div class="col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2><i class="fa fa-table"></i>
					Lista articolelor
					<span class="badge"><?= $pag['count'] ?></span>
				</h2>
				<div class="clear"></div>
			</div>

			<div class="x_content">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover table-highlight ">
						<thead>
							<tr>
								<th width="15px">ID</th>
								<th style="width:10px;" title="Flag - in lucru"></th>
								<th>Titlu articol</th>
								<th>Autor</th>
								<th>Adaugat de</th>
								<th>Data publicare</th>
								<th>Hits</th>
								<th style="width: 1px">
									<div>
										<a href="/articole/add/" class="btn btn-info btn-sm" role="button">
											<i class="fa fa-plus"></i>
											<span class="hidden-sm hidden-xs"><?= html_escape('Add') ?></span>
										</a>
									</div>
								</th>
							</tr>
						</thead>

						<?php if (count($items)): ?>
							<tbody>
								<?php foreach ($items as $k => $v):
									// flag - in lucru
									$flag = !$v->isPublished()
										? '<span title="Articol in lucru - Nepublicat" class="glyphicon glyphicon-bookmark"></span>'
										: '';
								?>
									<tr>
										<td class="center"><?= html_escape($v->getId()) ?></td>
										<td class="center"><?= $flag ?></td>
										<td>
											<div><?= html_escape($v->getTitlu()) ?></div>

											<?php if ($v->getCatId()) { ?>
												<div class="gray font11">
													Categorie: <a href="/articole?todo-filterbyCatID=<?= $v->getCatId() ?>"><?= $v->getCategorie()->getNume() ?></a>
												</div>
											<?php } ?>

											<?php if ($v->getPersonaj()) { ?>
												<div class="gray font11">
													Personaj:
													<?= html_escape($v->getPersonaj()->getNumeComplet()) ?>
												</div>
											<?php } ?>
										</td>

										<td><?= $v->getAutor() ? html_escape($v->getAutor()->getFullname()) : '' ?></td>
										<td><?= $v->getUser() ? html_escape($v->getUser()->getUsername()) : '' ?></td>
										<td><?= $v->getDataInsert() ? $v->getDataInsert()->format('d.m.Y H:i') : '' ?></td>
										<td><?= $v->getHits() ?></td>

										<td style="white-space: nowrap">
											<div>
												<a href="/articole/edit/<?= (int)$v->getId() ?>"
													class="btn btn-sm btn-primary"
													title="<?= html_escape('Edit') ?>">
													<i class="fa fa-edit bigger-110"></i> <span class="hidden-sm hidden-xs">Edit</span>
												</a>
											</div>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						<?php else: ?>
							<tbody>
								<tr>
									<td colspan="50" class="nodata"><?= html_escape('No data found') ?></td>
								</tr>
							</tbody>
						<?php endif; ?>
						<tfoot>
							<tr>
								<th colspan="50">
									<div style="float: left">
										<?= $this->load->view('/_partial/table/paginator', $pag, true) ?>
									</div>
									<div style="float: right">
										<?= $pagination ?>
									</div>
								</th>
							</tr>
						</tfoot>
					</table>

				</div>

			</div>
		</div>

	</div>
</div>