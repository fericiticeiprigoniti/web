<!-- TAB - Generale (informatii)-->

<?php
/**
 * @var ArticolItem $objArticol
 */
?>
<form name=""
    method="post"
    action="<?php echo '/articole/' . (($edit) ? 'edit/' . $edit : 'add') . '?tab=generale' ?>"
    class="formular formular_poezie form-horizontal form-label-left ajaxForm"
    <?php if (!$edit) { ?>
    data-redirect-url="/articole"
    <?php } ?>>

    <div class="col-lg-9 col-md-9 col-sm-12">

        <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Titlu<span class="red">*</span></label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                <input type="text" class="form-control" name="titlu" value="<?= set_value('titlu', Strings::fill($objArticol->getTitlu())) ?>">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Categorie<span class="red">*</span></label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                <select name="cat_id" class="form-control select2 allowClear" data-placeholder="- alege o categorie -">
                    <?php foreach ($groups_categorii as $grp) { ?>
                        <optgroup label="<?= $grp['nume'] ?? 'undefined' ?>">
                            <?php echo_select_options($grp['items'], set_value('cat_id', Strings::fill($objArticol->getCatId())), ['_key', '_value']) ?>
                        </optgroup>
                    <?php } ?>
                    <option></option>
                </select>
            </div>
        </div>

        <!-- Autocomplete -->
        <!-- <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Autor</label>

            <div class="col-md-10 col-sm-10 col-xs-12">
                <input type="text" class="form-control devbridge-autocomplete" placeholder="Autor"
                        data-id="personajId" data-url="/poezii/cautaAutor"
                        value="<?= $objArticol->getAutor() ? html_escape($objArticol->getAutor()->getFullname()) : null ?>" />
                </div>
        </div> -->

        <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Autor</label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                <select name="autor_id" class="form-control select2 allowClear" data-placeholder="- alege un autor -">
                    <option></option>
                    <?php echo_select_options($list_autori, set_value('autor_id', Strings::fill($objArticol->getAutorId())), ['_key', '_value']) ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Personaj</label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                <select name="personaj_id" class="form-control select2 allowClear" data-placeholder="- alege un personaj -">
                    <option></option>
                    <?php echo_select_options($list_personaje, set_value('personaj_id', Strings::fill($objArticol->getPersonajId())), ['id', 'nume_complet']) ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Conținut<span class="red">*</span></label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                <textarea name="continut" class="tinymce form-control"><?= set_value('continut', $objArticol->getContinut()) ?></textarea>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Cuvinte cheie</label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                <select name="cuvinteCheie[]" class="tags2" multiple>
                    <?php
                    $tagsSaved = $objArticol->getArrTagKeys();
                    foreach ($list_taguri as $key => $item) { ?>
                        <option value="<?= $key ?>" <?= in_array($key, $tagsSaved) ? 'selected="selected"' : '' ?>><?= $item ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <div class="alert alert-info">
            Detalii sursa
            <hr />
            <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Carte</label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <select name="sursa_doc_carte_id" class="form-control select2 allowClear" data-placeholder="selecteaza">
                        <option></option>
                        <?php echo_select_options($list_carti, set_value('sursa_doc_carte_id', Strings::fill($objArticol->getSursaDocCarteId())), ['_key', '_value']) ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Publicatie (not implemented)</label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <select name="sursa_doc_publicatie_id" class="form-control select2 allowClear" data-placeholder="selecteaza">
                        <option></option>
                        <?php //echo_select_options($list_publicatii, set_value('sursa_doc_publicatie_id', Strings::fill($objArticol->getSursaDocPublicatieId())));
                        ?>1
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Paginare</label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <input type="number" name="sursa_doc_start_page" class="form-control inlineb"
                        style="width:75px"
                        placeholder="inceput"
                        value="<?= set_value('sursa_doc_start_page', Strings::fill($objArticol->getSursaDocStartPage())) ?>" />
                    -
                    <input type="number" name="sursa_doc_end_page"
                        class="form-control inlineb" style="width:75px"
                        placeholder="sfarsit"
                        value="<?= set_value('sursa_doc_end_page', Strings::fill($objArticol->getSursaDocEndPage())) ?>" />
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Titlu (sursă online)</label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <input type="text" class="form-control" name="sursa_titlu" value="<?= set_value('sursa_titlu', Strings::fill($objArticol->getSursaTitlu())) ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Link (sursă online)</label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <input type="text" class="form-control" name="sursa_link" value="<?= set_value('sursa_link', Strings::fill($objArticol->getSursaLink())) ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12">Culegător</label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <input type="text" class="form-control" name="sursa_culegator" value="<?= set_value('sursa_culegator', Strings::fill($objArticol->getSursaCulegator())) ?>">
                </div>
            </div>

        </div>

        <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Utilizator</label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                <input type="text" class="form-control" value="<?= $objArticol->getUser() ? $objArticol->getUser()->getFullname() : '' ?>" readonly />
            </div>
        </div>

    </div>

    <!-- right COLUMN -->
    <div class="col-lg-3 col-md-3 col-sm-12">
        <div class="alert alert-info2">

            <div class="form-group p0">
                <div class="col-lg-6">
                    <label>Publicat</label>

                    <select name="is_published" class="form-control select2">
                        <?php foreach ($filters::fetchPublishedList() as $oValue => $oName): ?>
                            <?php $selected = ($oValue == $objArticol->isPublished() ? 'selected="selected"' : '') ?>
                            <option <?= $selected ?> value="<?= html_escape($oValue) ?>"><?= html_escape($oName) ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="col-lg-6 pr0">
                    <label>Sters</label>
                    <select name="is_deleted" class="form-control select2">
                        <option value="1" <?= ($objArticol->isDeleted() == 1) ? 'selected' : '' ?>>da</option>
                        <option value="0" <?= ($objArticol->isDeleted() == 0) ? 'selected' : '' ?>>nu</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-6">
                    <label>Data publicare</label>
                    <input type="text" class="form-control datepicker"
                        placeholder="zi/luna/an"
                        data-inputmask="'mask': '99/99/9999'"
                        name="data_publicare"
                        value="<?= $objArticol->getDataPublicare() ? $objArticol->getDataPublicare()->format('d/m/Y') : '' ?>" />

                </div>

                <div class="col-lg-6 pr0">
                    <label>Order ID</label>
                    <input type="number" name="order_id" class="form-control" value="<?= set_value('order_id', Strings::fill($objArticol->getOrderId())) ?>" />
                </div>
            </div>

        </div>

        <div class="form-group alert alert-info2">
            <h4>SEO</h4>
            <hr />
            <div class="form-group">
                <label class="col-12">Meta description</label>
                <div class="col-12">
                    <textarea name="seo_metadescription" class="form-control" style="height:125px"><?= set_value('seo_metadescription', $objArticol->getSeoMetadescription()) ?></textarea>
                </div>
            </div>

            <div class="form-group">
                <label class="col-12">Meta Keywords</label>
                <div class=" col-12">
                    <textarea name="seo_metakeywords" class="form-control" style="height:120px"><?= set_value('seo_metakeywords', $objArticol->getSeoMetakeywords()) ?></textarea>
                </div>
            </div>
        </div>
    </div>
    <!-- end right COLUMN -->

</form>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {

        $('.tags2').select2({
            tags: true,
        });

    });
</script>