<?php
/**
 * @var object $filters
 * @var object $order */

$extraSorting = array(
	'id'
	, 'nume' => 'Titlu'
	, 'autor' => 'Autor'
);

/*
 * Am comentat aceasta bucata de cod, pentru ca depinde de o biblioteca mai veche (Kint),
 * care arunca urmatoarea eroare pe PHP 7.4:
A PHP Error was encountered
Severity: 8192
Message: Function ReflectionType::__toString() is deprecated
 */
//  d($filters);

?>
<div class="row">
	<div class="col-xs-12">
		<div class="x_panel">

			<div class="x_title">
                <h2><i class="fa fa-filter"></i> Filtre (TODO)</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link colapsed"><i class="fa fa-chevron-down"></i></a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <div class="x_content" style="display: none;">
                <form class="filter-form form-horizontal form-label-left" action="<?= current_url() ?>" method="get">
                    <input type="hidden" name="_page" value="1" />
                    <input type="hidden" name="_per_page" value="<?= (isset($_GET['_per_page']) ? (int)$_GET['_per_page'] : '') ?>" />


                    <div class="row"><div class="col-sm-12 space-10"></div></div>
                    <div class="row">

                        <!-- Column 1 -->
                        <div class="col-sm-4 col-xs-12">

                            

                        </div>


                    </div>



                    <div class="form-group panel-footer" style="text-align: right">
                        <a href="<?= current_url() ?>" class="btn btn-warning">
                            <i class="fa fa-refresh"></i> Resetare
                        </a>
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-search"></i> Filtrare
                        </button>
                    </div>


				</form>
			</div>
		</div>
	</div>
</div>


<script src="/www/admin/vendors/select2/dist/js/select2.full.min.js"></script>
<script src="/www/admin/vendors/datepicker/daterangepicker.js"></script>
<script src="/www/admin/vendors/jquery-datetimepicker/jquery.datetimepicker.full.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('#filterForm').submit(function () {
		$('#filterForm input, #filterForm select').each(function () {
			if ($(this).val() === "") {
				$(this).attr('name', '');
			}
			showLoadingImage();
		});
	});
});
</script>

<!-- tags multiple select -->
<script>
$(document).ready(function() {
    $(".select2_multiple").select2({
        placeholder: "Selecteaza un tag",
        allowClear: true
    });
});
</script>
<!-- /tags multiple select -->
