<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-edit"></i> <?= $page_title ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li>
                        <button class="btn btn-primary" id="save_form">
                            <i class="fa fa-save"></i> Save
                        </button>
                    </li>
                </ul>
                <div class="clear"></div>
            </div>

            <div class="x_content">

                <input type="hidden" name="tokenID" id="token" value="<?php echo rand(1, 999); ?>" />

                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs" role="tablist">

                        <li role="presentation" class="<?php echo (!empty($tab_activ) && $tab_activ == 'generale') ? 'active' : ''; ?>">
                            <a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab"
                                aria-expanded="<?php echo (!empty($tab_activ) && $tab_activ == 'generale') ? 'true' : 'false' ?>">Informații generale</a>
                        </li>

                        </li>
                    </ul>

                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade <?php echo (!empty($tab_activ) && $tab_activ == 'generale') ? 'active in' : ''; ?>" id="tab_content1" aria-labelledby="home-tab">
                            <?php $this->load->view('/admin/generale/articole/_partials/tab_generale') ?>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {

        $('#save_form').click(function() {
            $('.tab-pane.active').find('form').submit();
        })

        $('.preview').click(function(e) {
            e.preventDefault();
        })

        $('.allowClear').select2({
            placeholder: "selecteaza",
            allowClear: true
        });


        $("#cuvinteCheie").select2({
            tags: true,
            minimumInputLength: 2,
            allowClear: true,
            ajax: {
                url: '/admin/taguri/getTags',
                dataType: "json",
                data: function(term, page) {
                    return {
                        q: term
                    };
                },
                results: function(data, page) {
                    var ret = {
                        results: []
                    };
                    for (tag in data.tags) {
                        tag = data.tags[tag];
                        ret.results.push({
                            id: tag.id,
                            text: tag.text
                        });
                    }
                    return ret;
                }
            }
        });


        $("#tagsAjax").select2({
            placeholder: "Search for a movie",
            minimumInputLength: 1,
            ajax: {
                url: "/taguri/getTags",
                //url: "https://api.github.com/search/repositories",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function(data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            placeholder: 'Search for a repository',
            escapeMarkup: function(markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 1,
            //templateResult: formatRepo,
            //templateSelection: formatRepoSelection
        });

        //function formatRepo (repo) {
        //          if (repo.loading) {
        //            return repo.text;
        //          }

        //          var markup = "<div class='select2-result-repository clearfix'>" +
        //            "<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
        //            "<div class='select2-result-repository__meta'>" +
        //              "<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

        //          if (repo.description) {
        //            markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
        //          }

        //          markup += "<div class='select2-result-repository__statistics'>" +
        //            "<div class='select2-result-repository__forks'><i class='fa fa-flash'></i> " + repo.forks_count + " Forks</div>" +
        //            "<div class='select2-result-repository__stargazers'><i class='fa fa-star'></i> " + repo.stargazers_count + " Stars</div>" +
        //            "<div class='select2-result-repository__watchers'><i class='fa fa-eye'></i> " + repo.watchers_count + " Watchers</div>" +
        //          "</div>" +
        //          "</div></div>";

        //          return markup;
        //        }

        //        function formatRepoSelection (repo) {
        //          return repo.full_name || repo.text;
        //        }
    });
</script>