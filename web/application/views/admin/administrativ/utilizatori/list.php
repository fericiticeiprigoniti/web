<?php

/**
 * @var array $items
 * @var object $order
 * @var AauthUserItem $objUser
 */
?>

<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-table"></i> Lista utilizatori</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">

                <div class="table-responsive">

                    <table class="table table-striped table-bordered table-hover table-highlight dataTable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nume</th>
                                <th>Email</th>
                                <th>Created</th>
                                <th>Last login</th>
                                <th>IP</th>
                                <th style="width: 1px">
                                    <div>
                                        <a href="/utilizatori/cont" class="btn btn-info btn-sm" role="button">
                                            <i class="fa fa-plus"></i>
                                            <span class="hidden-sm hidden-xs"><?= html_escape('Add') ?></span>
                                        </a>
                                    </div>
                                </th>
                            </tr>
                        </thead>

                        <?php if (count($items)): ?>
                            <tbody>
                                <?php foreach ($items as $k => $objUser): ?>
                                    <tr>
                                        <td class="border-right">
                                            <?= $objUser->getId() ?>
                                        </td>
                                        <td class="border-right">
                                            <?= $objUser->getFullname() ?>
                                        </td>
                                        <td>
                                            <?= ($this->aauth->config_vars['login_with_name'] == FALSE ? '<span class="fa fa-fw fa-user"></span>' : '') . $objUser->getEmail() ?>
                                        </td>
                                        <td><?= Calendar::convertDateMysql2Ro($objUser->getDateCreated()) ?></td>
                                        <td><?= Calendar::convertDateMysql2Ro($objUser->getLastLogin()) ?></td>
                                        <td><?= $objUser->getIpAddress() ?></td>

                                        <td style="white-space: nowrap">
                                            <div>
                                                <a href="/utilizatori/cont/<?= (int)$objUser->getId() ?>"
                                                    class="btn btn-sm btn-primary"
                                                    title="<?= html_escape('Edit') ?>">
                                                    <i class="fa fa-edit bigger-110"></i> <span class="hidden-sm hidden-xs"><?= html_escape('Edit') ?></span>
                                                </a>

                                                <a href="javascript:void(0)"
                                                    class="delete btn btn-sm btn-danger"
                                                    title="Baneaza utilizatorul - nu mai are acces pe site"
                                                    data-url="/utilizatori/ban"
                                                    data-id="<?= $objUser->getId() ?>">ban</a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        <?php else: ?>
                            <tbody>
                                <tr>
                                    <td colspan="50" class="nodata"><?= html_escape('No data found') ?></td>
                                </tr>
                            </tbody>
                        <?php endif; ?>

                    </table>

                </div>

            </div>
        </div>

    </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {

        //  Ban utilizator
        $('body').on('click', '.delete', function(e) {
            e.preventDefault();

            var button = $(this),
                ajaxUrl = $(this).attr("data-url");
            id = $(this).attr("data-id");

            $.ajax({
                url: ajaxUrl,
                dataType: 'JSON',
                type: 'POST',
                data: {
                    'id': id,
                    'cr_csrf_token': $('meta[name=csrf-token]').attr("content")
                },
                success: function(data) {
                    if (data.success) {
                        $(button).parents('tr').remove();
                    }
                }
            })

            return false;
        })
    });
</script>