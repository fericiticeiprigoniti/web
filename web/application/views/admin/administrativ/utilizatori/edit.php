<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-edit"></i> <?= $page_title ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li>
                        <button class="btn btn-primary" id="ajaxSubmit">
                            <i class="fa fa-save"></i> Save
                        </button>
                    </li>
                </ul>
                <div class="clear"></div>
            </div>

            <div class="x_content tab-pane active">
                <form name="add-user"
                    method="post"
                    class="formular form-horizontal ajaxForm"
                    action="<?= General::url() ?>"
                    data-redirect-url="/utilizatori">

                    <div class="row">
                        <div class="form-group">
                            <label for="nume" class="control-label col-md-3 col-sm-3 col-xs-12">Nume</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" id="nume" name="nume" class="form-control" value="<?php echo set_value('nume', !empty($userInfo->variables['nume']) ? $userInfo->variables['nume'] : '') ?>" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="prenume" class="control-label col-md-3 col-sm-3 col-xs-12">Prenume</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" id="prenume" name="prenume" class="form-control" value="<?php echo set_value('prenume', !empty($userInfo->variables['prenume']) ? $userInfo->variables['prenume'] : '') ?>" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" name="email" class="form-control" value="<?php echo set_value('email', !empty($userInfo->email) ? $userInfo->email : '') ?>" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Parola nouă <span class="fa fa-info-circle" title="Introduceti parola noua"></span></label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="password" name="pass" class="form-control" value="<?php echo set_value('pass'); ?>" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="pass_conf" class="control-label col-md-3 col-sm-3 col-xs-12">Confirmare parola <span class="fa fa-info-circle" title="Reintrodu parola pentru verificare"></span></label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="password" id="pass_conf" name="pass_conf" class="form-control" value="<?php echo set_value('pass_conf'); ?>" />
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Tip utilizator</label>

                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php if (!empty($groups)) {
                                    foreach ($groups as $group) { ?>
                                        <div>
                                            <label>
                                                <input type="checkbox"
                                                    name="groups[]"
                                                    value="<?= $group->id ?>"
                                                    <?= set_checkbox('groups[]', $group->id, !empty($userInfo->groups) ? in_array($group->id, $userInfo->groups) : '') ?> />
                                                <b><?= $group->name ?></b> [<?= $group->definition ?>]
                                            </label>
                                        </div>
                                <?php }
                                }
                                ?>

                            </div>

                        </div>

                    </div>
                </form>

            </div>
        </div>
    </div>
</div>