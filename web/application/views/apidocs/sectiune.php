<?php
/**
 * @var string $apiName
 * @var Tools\ApiMethodDocumenter[] $apiMethods
 */
?>

<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-table"></i> <?= $apiName ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">

                <!-- Sumar -->
                <h3>Sumar</h3>
                <div class="table-responsive col-md-6">

                    <table class="table table-striped table-bordered table-hover table-highlight dataTable">
                        <thead>
                        <tr>
                            <th>Metoda</th>
                            <th>Endpoint</th>
                            <th>Sumar</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php foreach($apiMethods as $method): ?>
                            <tr>
                                <td><?= $method->requestType ?></td>
                                <td><a href="/api/<?= strtolower($apiName) ?>#<?= $method->methodName ?>">./<?= strtolower($apiName) ?>/<?= $method->methodName ?></a></td>
                                <td>
                                    <?= $method->title ?>

                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>

                    </table>

                </div>
                <!-- end Sumar -->

                <!-- describe each endpoint -->
                <?php foreach($apiMethods as $method): ?>
                    <?php $params = $method->getParametersAsString(); ?>
                    <div class="mt20 col-sm-12">
                        <h3>
                            <a id="<?= $method->methodName ?>" class="anchor"></a>
                            <b><?= $method->requestType ?> ./<?= strtolower($apiName) ?>/<?= $method->methodName . ($params ? "/{\$$params}" : '') ?></b>
                        </h3>

                        <?= $method->title ?>

                        <table class="table table-striped table-bordered table-hover table-highlight dataTable">
                            <tbody>

                            <tr>
                                <th>Filtre disponibile</th>
                                <td>
                                    <table class="table table-striped table-bordered table-hover table-highlight dataTable">
                                        <?php if($method->filtersProperties): ?>
                                            <thead>
                                                <th>Nume</th>
                                                <th>Tip</th>
                                                <th>Descriere</th>
                                            </thead>

                                        <tbody>
                                            <?php foreach($method->filtersProperties as $prop) :?>

                                            <tr>
                                                <td><?= $prop['prop']?></td>
                                                <td><?= $prop['type']?></td>
                                                <td><?= $prop['info']?></td>
                                            </tr>

                                            <?php endforeach; ?>
                                        </tbody>

                                        <?php endif; ?>

                                    </table>
                                </td>
                            </tr>
                            <tr class="hidden">
                                <th>Schema raspuns</th>
                                <td>
                                    <table class="table table-striped table-bordered table-hover table-highlight dataTable">
                                        <?php if($method->itemProperties): ?>
                                            <thead>
                                            <th>Nume</th>
                                            <th>Tip</th>
                                            <th>Descriere</th>
                                            </thead>

                                            <tbody>
                                            <?php foreach($method->itemProperties as $prop) :?>

                                                <tr>
                                                    <td><?= $prop['prop']?></td>
                                                    <td><?= $prop['type']?></td>
                                                    <td><?= $prop['info']?></td>
                                                </tr>

                                            <?php endforeach; ?>
                                            </tbody>

                                        <?php endif; ?>

                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <th>Request URL</th>
                                <td>
                                    <a href="<?= $method->getLink() ?>" target="_blank">
                                        <?= $method->getLink() ?>
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

    </div>
</div>
