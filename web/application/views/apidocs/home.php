
<div class="row">
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-table"></i> Documentatie FCP</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">

                <div class="table-responsive">

                    <table class="table table-striped table-bordered table-hover table-highlight dataTable">
                        <thead>
                        <tr>
                            <th>Sectiune</th>
                            <th>Descriere</th>
                        </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td><a href="/api/articole">Articole</a></td>
                                <td>listarea articolele si a categoriilor</td>
                            </tr>

                            <tr>
                                <td><a href="/api/biblioteca">Biblioteca</a></td>
                                <td>carti, edituri, categorii de carti, colectii, dimensiuni carti </td>
                            </tr>

                            <tr>
                                <td><a href="/api/personaje">Personaje</a></td>
                                <td>grupati pe diferite roluri ex: poeti, marturisitori, autori, tortionari etc</td>
                            </tr>

                            <tr>
                                <td><a href="/api/poezii">Poezii</a></td>
                                <td>rime, structura, compozitie, strofe, perioada compozitiei si alte detalii</td>
                            </tr>

                            <tr>
                                <td><a href="/api/citate">Citate</a></td>
                                <td>preia citate random pe baza unor filtre</td>
                            </tr>

                        </tbody>

                    </table>

                </div>

            </div>
        </div>

    </div>
</div>
