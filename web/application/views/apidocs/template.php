<?php
// Get view's variables.
$data = (object)$this->get_vars();
unset($data->content);
?>
<!DOCTYPE html>
<html lang="ro">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>API docs</title>

    <!-- Bootstrap -->
    <!--    <link rel="stylesheet" type="text/css" href="/www/css/bootstrap.min.css">-->
    <link rel="stylesheet" type="text/css" href="/www/admin/vendors/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link href="/www/admin/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="/www/admin/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="/www/admin/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- jVectorMap -->
    <link href="/www/admin/demo/css/maps/jquery-jvectormap-2.0.3.css" rel="stylesheet"/>
    <!-- DataTables -->
    <link href="/www/admin/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet"/>

    <link href="/www/admin/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- jQuery -->
    <script src="/www/admin/vendors/jquery/dist/jquery.min.js"></script>

    <script type="text/javascript" src="/www/admin/plugins/tinymce-4.6.5/tinymce.min.js"></script>

    <!-- Bootstrap -->
    <!--<script src="/www/admin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>-->

    <!-- scrolling-tabs-->
    <link rel="stylesheet" type="text/css" href="/www/admin/vendors/scrolling-tabs/jquery.scrolling-tabs.min.css">

    <!-- jQuery -->
    <script src="/www/admin/vendors/jquery/dist/jquery.min.js"></script>

    <!-- PNotify -->
    <link href="/www/admin/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="/www/admin/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="/www/admin/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="/www/admin/custom.css" rel="stylesheet">
    <link href="/www/admin/main.css" rel="stylesheet">
    <link href="/www/css/global.css" rel="stylesheet">

</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <div class="navbar nav_title" style="border: 0;">
                    <a href="/api" class="site_title"><i class="fa fa-rocket"></i> <span>API docs</span></a>
                    <div class="center">DB used: <?= $this->db->database ?></div>
                </div>

                <div class="clearfix"></div>

                <hr>
                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <ul class="nav side-menu">

                            <li><a href="/api/articole"><i class="fa fa-file-text"></i> Articole </a></li>
                            <li><a href="/api/biblioteca"><i class="fa fa-book"></i> Biblioteca </a></li>
                            <li><a href="/api/personaje"><i class="fa fa-address-book"></i> Personaje </a></li>
                            <li><a href="/api/poezii"><i class="fa fa-quote-right"></i> Poezii </a></li>
                            <li><a href="/api/tags"><i class="fa fa-quote-right"></i> Cuvinte cheie </a></li>
                            <li><a href="/api/citate"><i class="fa fa-quote-left"></i> Citate </a></li>
                        </ul>
                    </div>

                </div>
                <!-- /sidebar menu -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <?php if (isset($data->breadcrumb) && count($data->breadcrumb)): ?>
                        <ol class="breadcrumb hidden-sm hidden-xs">

                            <?php foreach ($data->breadcrumb as $b): ?>
                                <?php if (isset($b['href']) && !empty($b['href'])): ?>
                                    <li><a href="<?= html_escape($b['href']) ?>"><?= $b['name'] ?></a></li>
                                <?php else: ?>
                                    <li class="active"><?= $b['name'] ?></li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ol>
                    <?php endif; ?>

                </nav>
            </div>

        </div>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main" style="min-height: 480px;">
            <div id="messagesContainer"></div>

            <?php
                echo $content;
            ?>

        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">
                {elapsed_time} / {memory_usage}
                | <a href="<?= $this->config->config['base_url'] ?>">FericitiCeiPrigoniti.net</a>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>

    <!-- Don't remove. It's used for loading a GIF whenever an Ajax is called -->
    <div id="requestLoading"></div>
</div>

<!-- Bootstrap -->
<script src="/www/admin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/www/admin/vendors/bootbox/bootbox.min.js"></script>

<!-- Select2 -->
<script src="/www/admin/vendors/select2/dist/js/select2.full.min.js"></script>

<!-- Datatables -->
<script src="/www/admin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/www/admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="/www/admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="/www/admin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="/www/admin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="/www/admin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="/www/admin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="/www/admin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="/www/admin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="/www/admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="/www/admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<!--    <script src="/www/admin/vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>-->

<!-- FastClick -->
<script src="/www/admin/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="/www/admin/vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="/www/admin/vendors/Chart.js/dist/Chart.min.js"></script>
<!-- gauge.js -->
<script src="/www/admin/vendors/bernii/gauge.js/dist/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="/www/admin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="/www/admin/vendors/iCheck/icheck.min.js"></script>
<!-- Skycons -->
<script src="/www/admin/vendors/skycons/skycons.js"></script>
<!-- Flot -->
<script src="/www/admin/vendors/Flot/jquery.flot.js"></script>
<script src="/www/admin/vendors/Flot/jquery.flot.pie.js"></script>
<script src="/www/admin/vendors/Flot/jquery.flot.time.js"></script>
<script src="/www/admin/vendors/Flot/jquery.flot.stack.js"></script>
<script src="/www/admin/vendors/Flot/jquery.flot.resize.js"></script>
<!-- jquery.inputmask -->
<script src="/www/admin/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
<!-- Flot plugins -->
<script src="/www/admin/demo/js/flot/jquery.flot.orderBars.js"></script>
<script src="/www/admin/demo/js/flot/date.js"></script>
<script src="/www/admin/demo/js/flot/jquery.flot.spline.js"></script>
<script src="/www/admin/demo/js/flot/curvedLines.js"></script>
<!-- jVectorMap -->
<script src="/www/admin/demo/js/maps/jquery-jvectormap-2.0.3.min.js"></script>

<!-- jQuery autocomplete (2 different ones: jQuery UI and Devbridge's autocomplete) -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/www/admin/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>

<!-- scrolling-tabs-->
<script type="text/javascript" src="/www/admin/vendors/scrolling-tabs/jquery.scrolling-tabs.min.js"></script>

<!-- PNotify -->
<script src="/www/admin/vendors/pnotify/dist/pnotify.js"></script>
<script src="/www/admin/vendors/pnotify/dist/pnotify.buttons.js"></script>
<script src="/www/admin/vendors/pnotify/dist/pnotify.nonblock.js"></script>

<!-- Custom Theme Scripts -->
<script src="/www/admin/demo/js/custom.js"></script>
<script src="/www/admin/main.js"></script>
<script type="text/javascript" src="/www/admin/global.js"></script>
<script src="/www/admin/lang_ro.js"></script>

</body>
</html>