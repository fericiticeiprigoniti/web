<?php
/**
 * @var string $name
 * @var string $label
 * @var object $order
 * @var string $class
 * @var string $style
 * @var int $rowspan
 * @var int $colspan
 */

// Set params.
$fName = $name;
$fLabel = $label;
$orderby = $order->by;
$orderdir = $order->dir;

// Set extra params.
$customClass = !empty($class) ? html_escape($class) : '';

// ----------------------------------------

$aClass = 'sorting';
$linkOrderDir = 'asc';
if($fName == $orderby) {
    if('asc' == strtolower($orderdir)) {
	    $aClass = 'sorting_asc';
        $linkOrderDir = 'desc';
    } else {
	    $aClass = 'sorting_desc';
    }
}
$newUrlParams = array('_order_by'=>$fName, '_order_dir'=>$linkOrderDir, '_page'=>'1');
if (isset($_GET['_per_page'])) {
	$newUrlParams['_per_page'] = (int)$_GET['_per_page'];
}
$url = General::url($newUrlParams);
?>
<th class="<?= $aClass ?> <?= $customClass ?>" style="<?= !empty($style) ? html_escape($style) : '' ?>"
	rowspan="<?= !empty($rowspan) ? (int)$rowspan : '' ?>" colspan="<?= !empty($colspan) ? (int)$colspan : '' ?>" >
	<a href="<?= html_escape($url) ?>"><?= $fLabel ?></a>
</th>