<?php
/**
 * @var array $itemsPerPage
 * @var int $start
 * @var int $perPage
 * @var int $count
 */

// Set params.
$start++;
$itemsPerPage = !empty($itemsPerPage) ? $itemsPerPage : array(10, 20, 50, 100, 200, 500);

// Reset query params.
$newUrlParams = array('_page'=>'1', '_per_page'=>null);
$url = General::url($newUrlParams);
?>

<?php if(count($itemsPerPage)): ?>
	<div style="display: inline-block; width:70px">
		<select id="itemsPerPage">
		<?php foreach($itemsPerPage as $v): ?>
			<?php $selected = $v == $perPage ? 'selected' : '' ?>
			<option value="<?= $v ?>" <?= $selected ?>><?= $v ?></option>
		<?php endforeach; ?>
		</select>
	</div>
<?php endif; ?>

<?php $perPage = ($count < $start + $perPage - 1) ? $count : $start + $perPage - 1 ?>
<span>Afisare de la <?= $start ?> la <?= $perPage ?> din <?= $count ?> rezultate</span>

<script type="text/javascript">
$(document).ready(function(){
	$('#itemsPerPage').change(function(){
		window.location.href = <?= json_encode($url) ?> + '&_per_page=' + $(this).val();
	});
});
</script>