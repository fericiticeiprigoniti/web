<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Clean string and replace anything beside letters and numbers with an dash line
 *
 * @param string $_string
 * @return string
 */
function dash_it($_string)
{
    return trim(strtolower(preg_replace('/([^A-Za-z0-9]+)/i', '-', $_string)), '-');
}

function array_filter_keys($array, $allowed)
{
    return array_filter(
        $array,
        function ($key) use ($allowed) {
            return in_array($key, $allowed);
        },
        ARRAY_FILTER_USE_KEY
    );
}

/**
 * build a random string
 *
 * @param mixed $length
 * @param mixed $numbers
 * @param mixed $special
 * @param mixed $special2
 */
function random_token($length = 12, $numbers = true, $special = false, $special2 = false)
{
    $charPool = array();
    $token = "";
    for ($i = 65; $i <= 90; $i++) $charPool[] = chr($i);
    for ($i = 97; $i <= 122; $i++) $charPool[] = chr($i);

    if ($numbers) {
        for ($i = 48; $i <= 57; $i++) $charPool[] = chr($i);
    }

    if ($special) {
        for ($i = 33; $i <= 46; $i++) $charPool[] = chr($i);
    }

    if ($special2) {
        for ($i = 58; $i <= 63; $i++) $charPool[] = chr($i);
    }

    for ($i = 0; $i <= $length; $i++) {
        $token .= $charPool[rand(0, count($charPool) - 1)];
    }

    return $token;
}

/**
 * Cut string to n symbols and add delim but do not break words.
 * @param string string we are operating with
 * @param integer character count to cut to
 * @param string|NULL delimiter. Default: '...'
 * @return string processed string
 **/
function neat_trim($str, $n, $delim = ' ...', $break_words = false)
{
    $len = strlen($str);
    if ($len > $n) {
        if (!$break_words) {
            $str = strip_tags($str);
            $str = wordwrap($str, $n);
            $str = explode("\n", $str);
            $str = $str[0] . $delim;
            return $str;
        } else {
            return substr($str, 0, $n) . $delim;
        }
    } else {
        return $str;
    }
}

/**
 * afiseaza option-urile pentru un select
 *
 * @param mixed $source
 * @param mixed $selected
 * @param mixed $map
 */
function echo_select_options($source, $selected = false, $map = null)
{
    foreach ($source as $key => $item) {
        if ($map[0]) {
            switch ($map[0]) {
                case '_key':
                    $value = $key;
                    break;
                case '_value':
                    $value = $item;
                    break;
                default:
                    $value = (array_key_exists($map[0], $item)) ? $item[$map[0]] : '';
            }
        } else {
            $value = $item['value'];
        }

        if ($map[1]) {
            switch ($map[1]) {
                case '_key':
                    $label = $key;
                    break;
                case '_value':
                    $label = $item;
                    break;
                default:
                    $label = (array_key_exists($map[1], $item)) ? $item[$map[1]] : '';
            }
        } else {
            $label = $item['label'];
        }

        echo '<option value="' . $value . '" ' . (($value == $selected) ? 'selected="selected"' : '') . '>' . $label . '</option>';
    }
}

/**
 * returneaza un timestamp formatat pentru mysql datetime
 *
 * @param mixed $now
 */
function sqldatetime($now)
{
    return date('Y-m-d H:i:s', $now);
}

/**
 * generare extrainfo
 *
 * @param mixed $user
 * @param mixed $time
 */
function createExtrainfo($user, $date = false, $oldContent = false)
{
    return '[user: ' . $user .
        (($date) ? ', data: ' . date('d-m-Y H:s:i') : '') .
        (($oldContent) ? ', Continut modificat: ' . $oldContent : '')
        . ']' . "\n";
}

/**
 * display Default Image if the file is not exist
 *
 * @param mixed $file
 */
function getImage($path = false, $filename = false, $thumb = false)
{
    $defaultImage = '/www/uploads/products/default.jpg';
    if (!$filename || !$path) {
        return $defaultImage;
    }

    if ($thumb) {
        $file = $path . '/thumb/' . $filename;
    } else {
        $file = $path . $filename;
    }

    return (file_exists($file)) ? ('/' . $file) : $defaultImage;
}

/**
 * Pentru proiectul PoetiiInchisorilor
 * poza default pentru un personaj
 *
 * @param bool $path
 * @return string
 */
function getPiPersonajImage($path = false)
{
    $defaultImage = 'https://fcp.cpco.ro/www/uploads/pi-personaj-default.jpg';

    return ($path && file_exists('www' . $path)) ? "https://fcp.cpco.ro/www{$path}" : $defaultImage;
}

/**
 * divide lista in 2 array-uri
 *
 * @param mixed $list
 */
function arrayDivide($list)
{
    $list1 = $list2 = array();
    $max_list = ceil(sizeof($list) / 2);
    foreach ($list as $key => $item) {
        if ($key >= $max_list) {
            $list1[] = $item;
        } else {
            $list2[] = $item;
        }
    }

    return array($list1, $list2);
}

/**
 * retruneaza class="active" sau active daca meniul in cauza e activ
 *
 * @param mixed $buttonID
 * @param mixed $withClass
 */
function menu_active($buttonID, $withClass = true)
{
    $ci = &get_instance();

    $active =  (is_array($ci->activeMenu)) ? $ci->activeMenu : array($ci->activeMenu);
    if (in_array($buttonID, $active)) {
        return ($withClass) ? 'class="active"' : 'active';
    }

    return '';
}

/**
 * verifica daca un string este o data valida
 *
 * @param string $str
 * @return boolean
 */
function isValidData($str)
{
    $infoData = explode('-', $str);
    $anData = intval($infoData[0]);
    $lunaData = intval($infoData[1]);
    $ziData = intval($infoData[2]);

    $ok = true;

    if ($anData < 1900 || $anData > 2037) { //validare an
        $ok = false;
    } elseif ($lunaData < 1 || $lunaData > 12) { //validare luna
        $ok = false;
    } elseif ($ziData > date('t', mktime(0, 0, 0, $lunaData, 1, $anData)) || $ziData <= 0) { //validare zi
        $ok = false;
    }

    return $ok;
}

/**
 * converteste din data romaneasca (zi-luna-an ora-minute-secunde) in timestamp
 *
 * @param mixed $date - data romaneasca
 * @param mixed $delimiter
 */
function date2time($date, $delimiter = '/')
{
    if ($info = explode(' ', $date)) {
        $date  = explode($delimiter, $info[0]);

        $hours = explode(':', $info[1]);
    }
    $hours = array(
        'h' => ($hours[0]) ? $hours[0] : '00',
        'm' => ($hours[1]) ? $hours[1] : '00',
        's' => ($hours[2]) ? $hours[2] : '00'
    );

    return mktime(intval($hours['h']), intval($hours['m']), intval($hours['s']), $date[1], $date[0], $date[2]);
}

function date2dbdate($date, $delimiter = '/')
{
    return date('Y-m-d', date2time($date, $delimiter));
}
function sqldate($now = false)
{
    $now = ($now) ? $now : time();
    return date('Y-m-d', $now);
}


/**
 * validare CNP
 *
 * @param mixed $cnp_primit
 */
function validare_cnp($cnp_primit)
{
    $cnp['cnp_primit'] = $cnp_primit;

    // prima cifra din cnp reprezinta sexul si nu poate fi decat 1,2,5,6 (pentru cetatenii romani)
    // 1, 2 pentru cei nascuti intre anii 1900 si 1999
    // 5, 6 pentru cei nascuti dupa anul 2000
    $cnp['sex']     = (int) substr($cnp_primit, 0, 1);
    // cifrele 2 si 3 reprezinta anul nasterii
    $cnp['an']      = (int) substr($cnp_primit, 1, 2) + ((in_array($cnp['sex'], array(1, 2))) ? 1900 : 2000);
    // cifrele 4 si 5 reprezinta luna (nu poate fi decat intre 1 si 12)
    $cnp['luna']    = (int) substr($cnp_primit, 3, 2);
    // cifrele 6 si 7 reprezinta ziua (nu poate fi decat intre 1 si 31)
    $cnp['zi']      = (int) substr($cnp_primit, 5, 2);
    // cifrele 8 si 9 reprezinta codul judetului (nu poate fi decat intre 1 si 52)
    $cnp['judet']   = (int) substr($cnp_primit, 7, 2);

    // cifrele 10,11,12 reprezinta un nr. poate fi intre 001 si 999.
    // Numerele din acest interval se impart pe judete,
    // birourilor de evidenta a populatiei, astfel inct un anumit numar din acel
    // interval sa fie alocat unei singure persoane intr-o anumita zi.

    // cifra 13 reprezinta cifra de control aflata in relatie cu
    // toate celelate 12 cifre ale CNP-ului.
    // fiecare cifra din CNP este inmultita cu cifra de pe aceeasi pozitie
    // din numarul 279146358279; rezultatele sunt insumate,
    // iar rezultatul final este impartit cu rest la 11. Daca restul este 10,
    // atunci cifra de control este 1, altfel cifra de control este egala cu restul.

    /**
     * definim variabilele de control
     */
    $test_key = 279146358279;
    $cnp_tst = substr($cnp_primit, 0, 12);
    $cnp_ctrl = (int) substr($cnp_primit, 12, 1);

    $response = '';

    /**
     * inmultim (de la stanga spre dreapta) fiecare cifra a variabilei $test_key cu corespondentul ei in variabila $cnp_test
     */
    for ($x = 0; $x < 12; $x++) {
        $response = $response + ((int) substr($test_key, $x, 1) * (int) substr($cnp_tst, $x, 1));
    }

    // setarea variabilei de erori, in cazul in care nu exista erori
    // sa returneze un array gol (altfel ar da eroare)
    $erori = array();

    if (empty($cnp['cnp_primit'])) {
        $erori[] = 'Campul CNP este gol!';
        return FALSE;
    } else {
        if (! is_numeric($cnp['cnp_primit'])) return FALSE;
        //$erori[] = 'CNP-ul este format doar din cifre!<br>';
        if (strlen($cnp['cnp_primit']) != 13) return FALSE;
        if ($cnp['sex'] != 1 && $cnp['sex'] != 2 && $cnp['sex'] != 5 && $cnp['sex'] != 6) return FALSE;
        //$erori[] = 'Prima cifra din CNP - eronata!<br>';
        if ($cnp['luna'] > 12 || $cnp['luna'] == 0) return FALSE;
        //$erori[] = 'Luna este incorecta!<br>';
        if ($cnp['zi'] > 31 || $cnp['zi'] == 0) return FALSE;
        //$erori[] = 'Ziua este incorecta!<br>';
        if (is_numeric($cnp['luna']) && is_numeric($cnp['zi']) && is_numeric($cnp['an'])) {
            if (! checkdate($cnp['luna'], $cnp['zi'], $cnp['an']))
                //$erori[] = 'Data de nastere specificata este incorecta<br>';
                return FALSE;
        }

        $response = $response % 11;
        $response = ($response == 10) ? 1 : $response;
        return ((int) $response === $cnp_ctrl) ? TRUE : FALSE;
    }

    return ($erori) ? FALSE : TRUE;
}



/**
 *
 * @Utf8_decode
 *
 * @Replace accented chars with latin
 *
 * @param string $string The string to convert
 *
 * @return string The corrected string
 *
 */

// deal with whitespaces
function decode_utf8($string)
{
    $string = preg_replace('/\s+/u', ' ', $string);

    // RO     'Ă', 'ă', 'Â', 'â', 'Î', 'î', 'Ş', 'ş', 'Ţ', 'ţ',     'Ş', 'ş', 'Ţ', 'ţ'
    $ro_in = array("\xC4\x82", "\xC4\x83", "\xC3\x82", "\xC3\xA2", "\xC3\x8E", "\xC3\xAE", "\xC8\x98", "\xC8\x99", "\xC8\x9A", "\xC8\x9B", "\xC5\x9E", "\xC5\x9F", "\xC5\xA2", "\xC5\xA3");
    $ro_out = array('A', 'a', 'A', 'a', 'I', 'i', 'S', 's', 'T', 't', 'S', 's', 'T', 't');

    // FR     À     à     Â     â     Æ     æ     È     è     É     é     Ê     ê     Ë     ë     Î     î     Ï     ï     Ô     ô     Œ     œ     Ù     ù     Û     û     Ü     ü     Ÿ     ÿ     Ç     ç
    $fr_in = array("\xC3\x80", "\xC3\xA0", "\xC3\x82", "\xC3\xA2", "\xC3\x86", "\xC3\xA6", "\xC3\x88", "\xC3\xA8", "\xC3\x89", "\xC3\xA9", "\xC3\x8A", "\xC3\xAA", "\xC3\x8B", "\xC3\xAB", "\xC3\x8E", "\xC3\xAE", "\xC3\x8F", "\xC3\xAF", "\xC3\x94", "\xC3\xB4", "\xC5\x92", "\xC5\x93", "\xC3\x99", "\xC3\xB9", "\xC3\x9B", "\xC3\xBB", "\xC3\x9C", "\xC3\xBC", "\xC5\xB8", "\xC3\xBF", "\xC3\x87", "\xC3\xA7");
    $fr_out = array('A',     'a',     'A', 'a', 'Ae', 'ae',     'E',     'e',     'E',     'e',     'E',     'e',     'E',     'e',     'I',     'i',     'I',     'i',     'O',     'o',     'Oe',     'oe',     'U',     'u',     'U',     'u',     'U',     'u',     'Y',     'y',     'C',     'c');

    // HU     Á     á     É     é     Í     í     Ó     ó     Ö     ö     Ő     ő     Ú     ú     Ü     ü     Ű     ű
    $hu_in    = array("\xC3\x81", "\xC3\xA1", "\xC3\x89", "\xC3\xA9", "\xC3\x8D", "\xC3\xAD", "\xC3\x93", "\xC3\xB3", "\xC3\x96", "\xC3\xB6", "\xC5\x90", "\xC5\x91", "\xC3\x9A", "\xC3\xBA", "\xC3\x9C", "\xC3\xBC", "\xC5\xB0", "\xC5\xB1");
    $hu_out    = array('A',     'a',     'E',     'e',     'I',     'i',     'O',     'o',     'O',     'o',     'O',     'o',     'U',     'u',     'U',     'u',     'U',     'u');

    // DE     Ä     ä     Ö     ö     Ü     ü     ß
    $de_in = array("\xC3\x84", "\xC3\xA4", "\xC3\x96", "\xC3\xB6", "\xC3\x9C", "\xC3\xBC", "\xC3\x9F");
    $de_out = array('Ae',     'ae',     'Oe', 'oe',     'Ue',     'ue',     'ss');

    // ES Á á É é Í í Ó ó Ú ú Ñ ñ Ü ü
    $es_in = array("\xC3\x81", "\xC3\xA1", "\xC3\x89", "\xC3\xA9", "\xC3\x8D", "\xC3\xAD", "\xC3\x93", "\xC3\xB3", "\xC3\x9A", "\xC3\xBA", "\xC3\x91", "\xC3\xB1", "\xC3\x9C", "\xC3\xBC");
    $es_out = array('A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u', 'N', 'n', 'U', 'u');

    // Miscelaneous
    $misc_in    = array("@");
    $misc_out = array('a');

    $search     = array_merge($ro_in, $fr_in, $hu_in, $de_in, $misc_in, $es_in);
    $replace    = array_merge($ro_out, $fr_out, $hu_out, $de_out, $misc_out, $es_out);
    $string     = str_replace($search, $replace, $string);

    // remove any other chars that stand in our way
    //$string = preg_replace('@[^a-zA-Z0-9_-]@', '', $string);

    //return strtolower($string);
    return $string;
}

function createArrayList($dataset, $_key, $_value)
{
    if (!$dataset) {
        return false;
    }
    $list = [];
    foreach ($dataset as $item) {
        $list[$item[$_key]] = $item[$_value];
    }

    return $list;
}

/**
 * creeaza sursa completa
 *
 * @param mixed $author
 * @param mixed $book_name
 * @param mixed $edition
 * @param mixed $publishing
 * @param mixed $locality
 * @param mixed $year_publication
 * @param mixed $page_start
 * @param mixed $page_end
 * @param mixed $source
 * @param mixed $source_link
 */
function generateSource($author = false, $book_name = false, $edition = false, $publishing = false, $locality = false, $year_publication = false, $page_start = false, $page_end = false, $source = false, $source_link = false)
{
    $result = array();

    if ($author) {
        array_push($result, $author);
    }
    if ($book_name) {
        array_push($result, $book_name);
    }
    if ($edition) {
        array_push($result, 'Ediția ' . $edition);
    }
    if ($publishing) {
        array_push($result, 'Editura ' . $publishing);
    }
    if ($locality) {
        array_push($result, $locality);
    }
    if ($year_publication) {
        array_push($result, $year_publication);
    }
    if ($page_start) {
        if ($page_end) {
            array_push($result, 'pp. ' . $page_start . '-' . $page_end);
        } else {
            array_push($result, 'p. ' . $page_start);
        }
    }

    // extra
    if ($source) {
        array_push($result, $source);
    }

    return implode(', ', $result);
}

if (!function_exists('dd')) {
    function dd(...$vars)
    {
        d(...$vars);
        die();
    }
}
