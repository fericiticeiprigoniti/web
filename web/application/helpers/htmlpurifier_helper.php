<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
 * Codeigniter HTMLPurifier Helper
 *
 * Purify input using the HTMLPurifier standalone class.
 * Easily use multiple purifier configurations.
 *
 * @author     Tyler Brownell <tyler.brownell@mssociety.ca>
 * @copyright  Public Domain
 *
 * @access  public
 * @param   string or array  $dirty_html  A string (or array of strings) to be cleaned.
 * @param   string           $scope      The name of the configuration (switch case) to use.
 * @return  string or array               The cleaned string (or array of strings).
 */
if (!function_exists('html_purify')) {
    function html_purify($dirty_html, $scope = false)
    {
        if (is_array($dirty_html)) {
            foreach ($dirty_html as $key => $val) {
                $clean_html[$key] = html_purify($val, $scope);
            }
        } else {
            $ci = &get_instance();
            $config = \HTMLPurifier_Config::createDefault();
            $config->set('Core.Encoding', $ci->config->item('charset'));
            $config->set('HTML.Doctype', 'XHTML 1.0 Strict');

            // aici putem defini mai multe optiuni de sanitizare in functie de un scop
            // de ex; Article -> aici putem lasa mai multe taguri pe care le folosim
            // default este 'false' -> vom face remove la tot , practic in afara de bold/ul/li radem tot
            switch ($scope) {
                case 'article':
                    $options = [
                        'HTML' => [
                            'Allowed' => 'p,b,strong,i,ul,li,a[href],img[src|width|height|alt],span[style],em,br',
                        ],
                        'URI' => [
                            'AllowedSchemes' => ['http' => true, 'https' => true],
                            'DefaultScheme' => 'http',
                        ],
                        'CSS' => [
                           'AllowedProperties' => ['color','text-decoration'],
                        ],
                    ];

                    // $config->set('HTML.SafeInline', true);
                    $config->set('AutoFormat.AutoParagraph', true);
                    $config->set('AutoFormat.Linkify', true);
                    $config->set('AutoFormat.RemoveEmpty', true);

                    break;

                case false:
                    // remove all
                    $options = [
                        'HTML' => [
                            'Allowed' => 'p,b,strong,em,ul,li,br',
                        ],
                        'URI' => [
                            'AllowedSchemes' => ['http' => true, 'https' => true],
                            'DefaultScheme' => 'http',
                        ],
                    ];

                    $config->set('AutoFormat.Linkify', false);
                    $config->set('AutoFormat.RemoveEmpty', true);

                    break;

                default:
                    show_error('The HTMLPurifier configuration labeled "'.htmlspecialchars($scope, ENT_QUOTES, $ci->config->item('charset')).'" could not be found.');
            }

            // override config with Options
            foreach ($options as $section => $values) {
                foreach ($values as $key => $value) {
                    $config->set("$section.$key", $value);
                }
            }

            $purifier = new \HTMLPurifier($config);
            $clean_html = $purifier->purify($dirty_html);
        }

        return $clean_html;
    }
}


/* End of htmlpurifier_helper.php */
/* Location: ./application/helpers/htmlpurifier_helper.php */