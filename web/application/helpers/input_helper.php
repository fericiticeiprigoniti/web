<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

function get($field = NULL, $xss = false)
{
    $ci = &get_instance();
    return $ci->input->get($field, $xss);
}

function post($field = NULL, $xss = false)
{
    $ci = &get_instance();
    return $ci->input->post($field, $xss);
}

function get_post($field = NULL, $xss = false)
{
    $ci = &get_instance();
    return $ci->input->post($field, $xss);
}

/** end of file */