<?php

/**
 * General class
 *
 * handles general stuff
 */
class General
{


    /**
     * Check if email address is valid
     * @param string $email
     * @return mixed
     */
    public static function checkEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * Check if it's a int value
     * @param int $val
     * @param bool $positive default false
     * @return bool
     */
    public static function checkInt($val, $positive = false)
    {
        return ($positive) ? filter_var($val, FILTER_VALIDATE_INT, array('options' => array('min_range' => 0))) : filter_var($val, FILTER_VALIDATE_INT);
    }

    /**
     * Check if it's a float value
     * @param float $val
     * @param bool $positive default false
     * @return bool
     */
    public static function checkFloat($val, $positive = false)
    {
        return ($positive) ? filter_var($val, FILTER_VALIDATE_FLOAT, array('options' => array('min_range' => 0))) : filter_var($val, FILTER_VALIDATE_FLOAT);
    }

    // ---------------------------------------------------------------------------------------------

    public static function formatFileSize($fileSize)
    {
        $fileSize = (float)$fileSize;
        if (($fileSize > 0) && ($fileSize < 1024 * 1024)) {
            $fileSize = (string)round(($fileSize / 1024), 1) . ' KB';
        } else {
            $fileSize = (string)round(($fileSize / 1024 / 1024), 1) . ' MB';
        }
        return $fileSize;
    }

    public static function getFileType($file, $tmp = true)
    {
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        return ($tmp ? $finfo->file($file->tmp_name) : $finfo->file($file));
    }

    public static function isPdfFile($file)
    {
        $fType = self::getFileType($file, false);
        if (stripos($fType, 'pdf') === false) {
            return false;
        } else {
            return true;
        }
    }

    public static function isImageFile($file)
    {
        $fType = self::getFileType($file, false);
        if (stripos($fType, 'image') === false) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Output file with specific header.
     * @param array $options - file, name
     * @param bool $forceDownload default false
     * @throws Exception
     */
    public static function outputFile(array $options, $forceDownload = false)
    {
        // Check if all options are set.
        if (!isset($options['file'])) {
            throw new Exception("Set file");
        } elseif (!isset($options['name'])) {
            throw new Exception("Set file name");
        }

        // Set file path.
        $file = $options['file'];

        // Check if file exists on disk.
        if (!file_exists($file)) {
            throw new Exception("File not found");
        }

        // Set file name.
        $fileName = str_replace('"', '', $options['name']);
        $fileName = str_replace("'", "", $fileName);

        // Check content type.
        $contentType = self::getFileType($file, false);

        // Set content disposition.
        $contentDisposition = ($forceDownload ? 'attachment' : 'inline');

        // Set content type, disposition and length.
        header("Content-Type: $contentType");
        header('Content-Disposition: ' . $contentDisposition . '; filename="' . $fileName . '"');
        header('Content-Length: ' . filesize($file));
        header("Content-Transfer-Encoding: UTF-8");

        // Disable caching
        header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
        header("Pragma: no-cache"); // HTTP 1.0
        header("Expires: 0"); // Proxies

        readfile($file);
        exit;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Generate URL in function of current url. See the example below
     * Current URL: /personaje/editeaza/3?tab=3&alt_param=1
     * General::url(array('tab'=>2, 'alt_param'=>null, 'un_param_nou'=>'valoare') returns
     * /personaje/3?tab=2&un_param_nou=valoare
     * @param array $query
     * @param bool $resetQuery - defaults to false. usually we want to carry query params around when using it.
     * @return string
     */
    public static function url(array $query = array(), $resetQuery = false)
    {
        $query = $resetQuery ? $query : array_merge($_GET, $query);
        $result = current_url() . (count($query) ? '?' . http_build_query($query) : '');
        return $result;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Display autocomplete results in json format and exit.
     * @param $searchTerm
     * @param array $data
     */
    public static function displayAutocompleteResults($searchTerm, array $data)
    {
        header("Content-type: application/json");
        echo json_encode(array(
            'query' => $searchTerm,
            'suggestions' => $data,
        ));
        exit;
    }

    // ---------------------------------------------------------------------------------------------

}
