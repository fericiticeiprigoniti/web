<?php

trait ArrayOrJson
{
    /**
     * @return array
     */
    public function asArray()
    {
        return get_object_vars($this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->asArray();
    }

    /**
     * @return string
     */
    public function asJson()
    {
        return json_encode($this->asArray());
    }

    /**
     * @return string
     */
    public function toJson()
    {
        return $this->asJson();
    }

}
