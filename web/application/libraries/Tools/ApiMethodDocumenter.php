<?php

namespace Tools;

use Exception;
use ReflectionClass;
use ReflectionMethod;
use ReflectionParameter;

class ApiMethodDocumenter
{

    const KEY_TITLE     = '@title';
    const KEY_FILTERS   = '@filters';
    const KEY_ITEM      = '@response';
    const KEY_VAR       = '@var';
    const KEY_EXAMPLE   = '@example';
    const KEY_DOCS_IGNORE   = '@docs_ignore';


    /**
     * @var ReflectionMethod
     */
    public $method;

    /**
     * @var string
     */
    public $methodName;

    /**
     * @var string
     */
    public $requestType;

    /**
     * @var ReflectionParameter[]
     */
    public $methodParameters = [];

    /**
     * @var string
     */
    public $methodDescription;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $filtersClassName;

    /**
     * @var array
     */
    public $filtersProperties = [];

    /**
     * @var string
     */
    public $itemClassName;

    /**
     * @var array
     */
    public $itemProperties = [];

    /**
     * ApiDocumenter constructor.
     * @param ReflectionMethod $apiMethod
     */
    public function __construct(ReflectionMethod $apiMethod)
    {
        $this->method = $apiMethod;

        // Extract method name and request type
        $this->parseMethodName($apiMethod->getName());

        // Set method parameters
        $this->methodParameters = $apiMethod->getParameters();

        // Parse method description and extract all the necessary information
        $this->parseMethodDescription($apiMethod->getDocComment());
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Parse api method name and extract name and request type (GET, POST, PUT etc.)
     * @param string $methodName
     */
    public function parseMethodName($methodName)
    {
        $arr = explode('_', $methodName);
        $this->requestType = array_pop($arr);
        $this->requestType = $this->requestType ? strtoupper($this->requestType) : null;
        $this->methodName = implode('_', $arr);
    }

    /**
     * @param string $methodDescription
     */
    public function parseMethodDescription($methodDescription)
    {
        // Set method description as it was written
        $this->methodDescription = $methodDescription;

        // Break description into individual lines
        $lines = explode("\n", $methodDescription);

        // Clean description lines of trailing spaces
        $lines = array_map('trim', $lines);

        // Parse each line and extract specific information
        if (count($lines)) {
            foreach ($lines as $line) {
                switch(true) {
                    case stripos($line, $this::KEY_TITLE):
                        $this->title = trim(substr($line, stripos($line, $this::KEY_TITLE) + strlen($this::KEY_TITLE)));
                        break;
                    case stripos($line, $this::KEY_FILTERS):
                        $this->filtersClassName = trim(substr($line, stripos($line, $this::KEY_FILTERS) + strlen($this::KEY_FILTERS)));
                        break;
                    case stripos($line, $this::KEY_ITEM):
                        $this->itemClassName = trim(substr($line, stripos($line, $this::KEY_ITEM) + strlen($this::KEY_ITEM)));
                        $this->itemClassName = trim($this->itemClassName, "[]");
                        break;
                }
            }
        }

        // Extract filters properties
        $this->parseFiltersClass($this->filtersClassName);

        // Extract item properties
        $this->parseItemClass($this->itemClassName);
    }

    /**
     * @param string $className
     */
    public function parseFiltersClass($className)
    {
        if (!$className) {
            return;
        }

        try {
            $filtersRef = new ReflectionClass($className);
            $this->filtersProperties = [];
            if (count($filtersRef->getProperties())) {
                foreach($filtersRef->getProperties() as $property) {
                    $propName = $property->getName();

                    // Break property description into individual lines
                    $lines = explode("\n", $property->getDocComment());

                    // Clean description lines of trailing spaces
                    $lines = array_map('trim', $lines);

                    // Parse line by line and extract only variable type stuff
                    $propDoc = '';
                    $propDocExample = '';
                    foreach($lines as $line) {
                        if (stripos($line, $this::KEY_VAR)) {
                            $propDoc = substr($line, stripos($line, $this::KEY_VAR));
                            $propDoc = str_replace($this::KEY_VAR,'',$propDoc);
                        }

                        if (stripos($line, $this::KEY_EXAMPLE)) {
                            $propDocExample = substr($line, stripos($line, $this::KEY_EXAMPLE));
                            $propDocExample = str_replace($this::KEY_EXAMPLE,'',$propDocExample);
                        }

                        // if we use @* @docs_ignore in comment
                        // this parameter will be ignore in API Documentation
                        if (stripos($line, $this::KEY_DOCS_IGNORE)) {
                            continue 2;
                        }
                    }

                    //$this->filtersProperties[] = $propName . '- '.$propDoc . ($propDocExample? ' '. $propDocExample : '');
                    $this->filtersProperties[] = [
                        'prop' => $propName,
                        'type' => $propDoc,
                        'info' => $propDocExample? $propDocExample : ''
                    ];
                }
            }
        } catch (Exception $e) {
            //TODO
        }
    }

    /**
     * @param string $className
     */
    public function parseItemClass($className)
    {
        if (!$className) {
            return;
        }

        try {
            $itemRef = new ReflectionClass($className);
            $this->itemProperties = [];
            if (count($itemRef->getProperties())) {
                foreach($itemRef->getProperties() as $property) {
                    $propName = $property->getName();

                    // Break property description into individual lines
                    $lines = explode("\n", $property->getDocComment());

                    // Clean description lines of trailing spaces
                    $lines = array_map('trim', $lines);

                    // Parse line by line and extract only variable type stuff
                    $propDoc = '';
                    $propDocExample = '';
                    foreach($lines as $line) {
                        if (stripos($line, $this::KEY_VAR)) {
                            $propDoc = substr($line, stripos($line, $this::KEY_VAR));
                            $propDoc = str_replace($this::KEY_VAR,'',$propDoc);
                        }

                        if (stripos($line, $this::KEY_EXAMPLE)) {
                            $propDocExample = substr($line, stripos($line, $this::KEY_EXAMPLE));
                            $propDocExample = str_replace($this::KEY_EXAMPLE,'',$propDocExample);
                        }
                    }

                    $this->itemProperties[] = [
                        'prop' => $propName,
                        'type' => $propDoc,
                        'info' => $propDocExample? $propDocExample : ''
                    ];
                }
            }
        } catch (Exception $e) {
            //TODO
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return string
     */
    public function getParametersAsString()
    {
        $params = [];
        foreach($this->methodParameters as $parameter) {
            $params[] = $parameter->getName();
        }
        return count($params) ? implode(', ', $params) : '';
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return '/api/' . strtolower($this->method->getDeclaringClass()->getName()) . '/' . $this->methodName;
    }

}