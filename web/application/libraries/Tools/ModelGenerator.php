<?php

namespace Tools;

use Exception;

class ModelGenerator {

    /**
    * The CodeIgniter object variable
    * @access public
    * @var object
    */
    public $CI;

    /**
    * @var string
    */
    private $tableName;

    /**
     * @var string
     */
    private $modelName;

    /**
     * @var mixed
     */
    private $modelPath;

    /**
    * @var ColumnItem[]
    */
    private $columns;

    /**
    * @var ColumnItem[]
    */
    private $filtersItems;

    /**
    * @var OrderItem[]
    */
    private $orderItems;

    /**
     * @var string
     */
    private $primaryKey = 'id';

    /**
     * @var string
     */
    private $tabchar = '    ';

    /**
     * @var string
     */
    private $varDecType = 'private';

    // ---------------------------------------------------------------------------------------------

    /**
     * ModelGenerator constructor
     */
    public function __construct()
    {
        // get main CI object
        $this->CI = & get_instance();

        // Dependencies
        if(CI_VERSION >= 2.2){
            $this->CI->load->library('driver');
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param string $tableName
     * @param string $modelName
     * @throws Exception
     */
    public function init($tableName, $modelName=null)
    {
        // Set table and model name.
        $this->setTableName($tableName);
        $this->setModelName($modelName);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param string $table
     * @throws Exception
     */
    private function setTableName($table)
    {
        if (empty($table)) {
            throw new Exception('Empty table name');
        } elseif(!$this->CI->db->table_exists($table)) {
            throw new Exception('Table not found: ' . $table);
        }
        $this->tableName = $table;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param string $modelName
     */
    private function setModelName($modelName)
    {
        $modelName = strip_tags(trim($modelName));
        if (empty($modelName)) {
            $modelName = $this->tableName;
        }
        $this->modelName = ucfirst(MyStrings::_convertToCamelCase($modelName));
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Create object with all columns with all properties
     * @param string $customDbCols - delimiter by comma
     * @throws Exception
     */
    public function loadTableStructure($customDbCols = null)
    {
        $db_tablename = $this->tableName;
        if( !empty($db_tablename) )
        {
            if( $this->CI->db->table_exists($db_tablename) ) {
                if( $objCols = $this->CI->db->field_data($db_tablename) ) {
                    if (!isset($objCols[0]->key)) {
                        throw new Exception("Please check CI_DB_mysqli_driver::field_data(\$table). You need to set \$retval[\$i]->key = \$query[\$i]->Key");
                    }
                    foreach($objCols as $col) {
                        $col = new ColumnItem($col);
                        $this->columns[] = $col;
                    }
                }
            } else {
                throw new Exception("Can't get table columns");
            }

        } elseif( is_string($customDbCols) ) {

            /**
             * Parse custom string and create new objects
             */
            if ($cols = explode(",", $customDbCols)) {
                foreach($cols as $item) {
                    $col = new ColumnItem((object)array(
                        'name' => trim($item),
                    ));
                    $this->columns[] = $col;
                }
            }

        } else {
            throw new Exception('No data table set or custom elements for the requested model');
        }

    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return ColumnItem[]
     */
    public function getColumns()
    {
        return $this->columns;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param ColumnItem $column
     * @return string
     */
    private function getGetter(ColumnItem $column)
    {
        return 'get' . ucfirst($column->phpVariable);
    }

    /**
     * @param ColumnItem $column
     * @return string
     */
    private function getSetter(ColumnItem $column)
    {
        return 'set' . ucfirst($column->phpVariable);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Create necessary models (Item, Filters and Table)
     * @param string $modelPath
     * @throws Exception
     */
    public function createModels($modelPath=null)
    {
        // sanitize & prepare data
        $modelPath = strip_tags(trim($modelPath));

        //TODO - rethink the $dirPath thingy
        if (empty($modelPath)) {
            $modelPath = 'Generated';
        }

        // set Dir path
        $modelsPath = APPPATH . "models/";
        if(!empty($modelPath)) {
            // check if exist else create
            if(!file_exists($modelsPath . $modelPath)) {
                mkdir($modelsPath . $modelPath, null, true);
            }

            // override new path
            $this->modelPath = $modelsPath . $modelPath .'/';
        } else {
            $this->modelPath = $modelPath;
        }

        // Check if the columns are loaded.
        if (!$this->columns || !count($this->columns)) {
            $this->loadTableStructure();
        }

        // Generate Item model
        $this->generateItemModel();

        // Generate Filters model
        $this->generateFiltersModel();

        // Generate Table model
        $this->generateTableModel();
    }

    // ---------------------------------------------------------------------------------------------

    private function generateItemModel()
    {
        $className  = $this->modelName . 'Item';
        $myfile    = fopen($this->modelPath . $className . '.php', "w") or die("Unable to open item file!");
        $tab = $this->tabchar;

        // start declaring Class
        $injection = "<?php\n\nclass {$className}\n{\n";

        // Declare variables
        $injection .= $this->writePropertiesForItemModel();

        $injection .= "\n$tab// ---------------------------------------------------------------------------------------------\n\n";

        // create Constructor
        $injection .= $this->writeConstructorForItemModel();

        $injection .= "\n$tab// ---------------------------------------------------------------------------------------------\n\n";

        // create Setter & Getter
        foreach($this->columns as $col)
        {
            $injection .= $this->writeGetterForItemModel($col);
            $injection .= $this->writeSetterForItemModel($col);
        }

        // end Class
        $injection .= "\n}\n";

        // add all injection code in file
        fwrite($myfile, $injection);

        fclose($myfile);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * declarations
     *
     * @return string
     */
    private function writePropertiesForItemModel()
    {
        $str = '';

        if( $this->columns ) {
            foreach($this->columns as $col ) {
                $str .= <<<EOPHP
    /**
     * @var {$col->phpType}
     */
    {$this->varDecType} \${$col->phpVariable};\n\n
EOPHP;
            }
        }

        return $str;
    }

    // ---------------------------------------------------------------------------------------------

    private function writeConstructorForItemModel()
    {
        $tab = $this->tabchar;

        $injection = <<<EOPHP
    public function __construct(array \$dbRow = [])
    {
        if ( !count(\$dbRow) ) {
            return;
        }\n\n
EOPHP;

        // Parse columns and set each property
        foreach($this->columns as $col ) {

            // ex: $this->id = $dbRow['id'] ? (int)$dbRow['id'] : null;
            $injection .= "{$tab}{$tab}\$this->{$col->phpVariable} = \$dbRow['{$col->name}'] ? {$col->castAs}\$dbRow['{$col->name}'] : null;\n";

        }

        // End __construct method
        $injection .= "{$tab}}\n";

        return $injection;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param ColumnItem $col
     * @return string
     */
    private function writeGetterForItemModel($col)
    {
        $funcName = 'get'.ucfirst($col->phpVariable);

        return <<<EOPHP
    /**
     * @return {$col->phpType}
     */
    public function {$funcName}()
    {
        return \$this->{$col->phpVariable};
    }\n\n
EOPHP;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param ColumnItem $col
     * @return string
     */
    private function writeSetterForItemModel($col)
    {
        $funcName = $this->getSetter($col);

        return <<<EOPHP
    /**
     * @param {$col->phpType} \${$col->phpVariable}
     */
    public function {$funcName}(\${$col->phpVariable})
    {
        \$this->{$col->phpVariable} = \${$col->phpVariable} ? {$col->castAs}\${$col->phpVariable} : null;
    }\n\n
EOPHP;
    }

    // ---------------------------------------------------------------------------------------------

    private function generateFiltersModel()
    {
        $className = $this->modelName . 'Filters';
        $myfile = fopen($this->modelPath . $className . '.php', "w") or die("Unable to open filters file!");
        $tab = $this->tabchar;

        // Set filters' items ($filtersItems) in function of columns
        $this->setFiltersItems();

        // Set order items ($orderItems).
        $this->setOrderItems();

        // start declaring Class
        $injection = "<?php\n\nclass {$className}\n{\n\n";

        // Create order constants
        $injection .= $this->writeOrderConstantsForFiltersModel();

        // Set properties
        $injection .= $this->writePropertiesForFiltersModel();

        $injection .= "{$tab}// ---------------------------------------------------------------------------------------------\n\n";

        // Create constructor
        $injection .= $this->writeConstructorForFiltersModel();

        $injection .= "\n{$tab}// ---------------------------------------------------------------------------------------------\n\n";

        // create Setter & Getter
        foreach($this->filtersItems as $filter)
        {
            $injection .= $this->writeGetterForFiltersModel($filter);
            $injection .= $this->writeSetterForFiltersModel($filter);
        }

        $injection .= "{$tab}// ---------------------------------------------------------------------------------------------\n\n";

        // Create fetchOrderItems() method
        $injection .= $this->writeFetchOrderItems();

        // end Class
        $injection .= "\n}\n";

        // add all injection code in file
        fwrite($myfile, $injection);

        fclose($myfile);
    }

    // ---------------------------------------------------------------------------------------------

    private function setFiltersItems()
    {
        if (!$this->columns || !count($this->columns)) {
            return null;
        }

        // Init filter items
        $this->filtersItems = array();

        // Parse column items and set filters for the ones that have primary keys
        foreach($this->columns as $column) {
            if ($column->primary_key) {
                $this->filtersItems[] = $column;

                $excluded = clone $column;
                $excluded->phpVariable = MyStrings::_convertToCamelCase('excluded_' . $excluded->name);
                $excluded->primary_key = false;
                $excluded->sqlOperator = '<>';
                $this->filtersItems[] = $excluded;
            } elseif ($column->key) {
                $this->filtersItems[] = $column;
            }
        }


        // Insert here any other filter your heart desires.


        // Set $orderBy filter. It will be used for sorting queries.
        $obj = new ColumnItem(null);
        $obj->phpVariable = 'orderBy';
        $obj->phpType = 'array';
        $this->filtersItems[] = $obj;
    }

    // ---------------------------------------------------------------------------------------------

    private function setOrderItems()
    {
        if (!$this->columns || !count($this->columns)) {
            return null;
        }

        // Init filter items
        $this->orderItems = array();

        // Parse column items and set filters for the ones that have primary keys
        foreach($this->columns as $column) {
            if ($column->primary_key) {
                $this->orderItems[] = new OrderItem($column->name, 'ASC');
                $this->orderItems[] = new OrderItem($column->name, 'DESC');
            }
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return string
     */
    private function writeOrderConstantsForFiltersModel()
    {
        $result = '';
        if (!count($this->orderItems)) {
            return $result;
        }
        foreach($this->orderItems as $orderItem) {
            $result .= "{$this->tabchar}const {$orderItem->key} = '{$orderItem->value}';\n";
        }
        return $result . "\n";
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return string
     */
    private function writePropertiesForFiltersModel()
    {
        $str = '';

        if( $this->filtersItems ) {
            foreach($this->filtersItems as $col ) {
                $defaultValue = ($col->phpVariable == 'orderBy') ? ' = []' : '';

                $str .= <<<EOPHP
    /**
     * @var {$col->phpType}
     */
    {$this->varDecType} \${$col->phpVariable}{$defaultValue};\n\n
EOPHP;
            }
        }

        return $str;
    }

    // ---------------------------------------------------------------------------------------------

    private function writeConstructorForFiltersModel()
    {
        $tab = $this->tabchar;

        $injection = <<<EOPHP
    public function __construct(array \$getData = [])
    {\n
EOPHP;

        // Parse filters' items and set a value for each one.
        foreach($this->filtersItems as $filter ) {
            /* Ex:
             * if (isset($getData['id'])) {
             *   $this->setId($getData['id']);
             * }
             */
            $key = $filter->phpVariable == 'orderBy' ? '_orderBy' : $filter->phpVariable;
            $setter = $this->getSetter($filter);
            $injection .= <<< EOPHP
        if (isset(\$getData['{$key}'])) {
            \$this->{$setter}(\$getData['{$key}']);
        }\n
EOPHP;
        }

        // End __construct method
        $injection .= "{$tab}}\n";

        return $injection;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param ColumnItem $filter
     * @return string
     */
    private function writeGetterForFiltersModel($filter)
    {
        $methodName = 'get' . ucfirst($filter->phpVariable);

        return <<<EOPHP
    /**
     * @return {$filter->phpType}
     */
    public function {$methodName}()
    {
        return \$this->{$filter->phpVariable};
    }\n\n
EOPHP;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @param ColumnItem $filter
     * @return string
     */
    private function writeSetterForFiltersModel($filter)
    {
        $methodName = $this->getSetter($filter);

        if ($filter->phpVariable == 'orderBy') {

            $injection = $injection = <<<EOPHP
    /**
     * @param array \$orderBy
     */
    public function setOrderBy(array \$orderBy)
    {
        \$orderBy = (is_array(\$orderBy) && count(\$orderBy) ? \$orderBy : array());
        if (count(\$orderBy)) {
            \$orderItems = array_keys(self::fetchOrderItems());
            foreach (\$orderBy as \$k=>\$v) {
                if (!in_array(\$v, \$orderItems)) {
                    unset(\$orderBy[\$k]);
                }
            }
        }
        \$this->orderBy = \$orderBy;
    }\n\n
EOPHP;

        } else {

            $injection = <<<EOPHP
    /**
     * @param {$filter->phpType} \${$filter->phpVariable}
     */
    public function {$methodName}(\${$filter->phpVariable})
    {
        \$this->{$filter->phpVariable} = \${$filter->phpVariable} ? {$filter->castAs}\${$filter->phpVariable} : null;
    }\n\n
EOPHP;

        }

        return $injection;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return string
     */
    private function writeFetchOrderItems()
    {
        if (!count($this->orderItems)) {
            return '';
        }
        $tab = $this->tabchar;

        $orderItems = "\n";
        foreach($this->orderItems as $orderItem) {
            $orderItems .= "{$tab}{$tab}{$tab}self::{$orderItem->key} => '{$orderItem->value}',\n";
        }
        $orderItems .= "{$tab}{$tab}";

        return <<<EOPHP
    /**
     * @return array
     */
    public static function fetchOrderItems()
    {
        return array($orderItems);
    }\n\n
EOPHP;
    }

    // ---------------------------------------------------------------------------------------------

    private function generateTableModel()
    {
        $className = $this->modelName . 'Table';
        $myfile = fopen($this->modelPath . $className . '.php', "w") or die("Unable to open item file!");
        $tab = $this->tabchar;

        // start declaring Class
        $injection = <<<EOPHP
<?php

/**
 * Class {$className}
 * @table {$this->tableName}
 */
class {$className} extends CI_Model
{

    /**
     * Used for caching items
     * @var {$this->modelName}Item[]
     */
    public static \$collection = [];

    /**
     * @var {$className}
     */
    private static \$instance;


    /**
     * Singleton
     * {$className} constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * We can later dependency inject and thus unit test this
     * @return {$className}
     */
    public static function getInstance() {
        if (is_null(self::\$instance)) {
            self::\$instance = new self();
        }
        return self::\$instance;
    }

    /**
     * @param int \${$this->primaryKey}
     */
    public static function resetCollectionId(\${$this->primaryKey})
    {
        if (isset(self::\$collection[\${$this->primaryKey}])) {
            unset(self::\$collection[\${$this->primaryKey}]);
        }
    }\n\n
EOPHP;

        $injection .= "{$tab}// ---------------------------------------------------------------------------------------------\n\n";

        // Write 'load' method
        $injection .= $this->writeLoadMethodForTableModel();

        // Write 'reload' method
        $injection .= $this->writeReloadMethodForTableModel();

        $injection .= "{$tab}// ---------------------------------------------------------------------------------------------\n\n";

        // Write 'fetchAll' method
        $injection .= $this->writefetchAllMethodForTableModel();

        $injection .= "{$tab}// ---------------------------------------------------------------------------------------------\n\n";

        // Write 'save' method
        $injection .= $this->writeSaveMethodForTableModel();

        $injection .= "{$tab}// ---------------------------------------------------------------------------------------------\n\n";

        // Write 'create' method
        $injection .= $this->writeDeleteMethodForTableModel();

        $injection .= "{$tab}// ---------------------------------------------------------------------------------------------\n\n";

        // end Class
        $injection .= "\n}\n";

        // add all injection code in file
        fwrite($myfile, $injection);

        fclose($myfile);
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return string
     */
    private function writeLoadMethodForTableModel()
    {
        return <<<EOPHP
    /**
     * @param mixed \${$this->primaryKey}
     * @return {$this->modelName}Item|null
     * @throws Exception
     */
    public function load(\${$this->primaryKey})
    {
        if (!\${$this->primaryKey}) {
            throw new Exception("Invalid {$this->modelName} {$this->primaryKey}");
        }

        // If data is cached return it
        if (MODEL_CACHING_ENABLED && isset(self::\$collection[\${$this->primaryKey}])) {
            return self::\$collection[\${$this->primaryKey}];
        }

        // Fetch data
        \$filters = new {$this->modelName}Filters(array('{$this->primaryKey}'=>\${$this->primaryKey}));
        \$data = \$this->fetchAll(\$filters);

        // Cache result
        self::\$collection[\${$this->primaryKey}] = isset(\$data['items'][0]) ? \$data['items'][0] : null;

        // Return cached result
        return self::\$collection[\${$this->primaryKey}];
    }\n\n
EOPHP;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return string
     */
    private function writeReloadMethodForTableModel()
    {
        return <<<EOPHP
    /**
     * Reset item from static collection and load a new one
     * @param mixed \${$this->primaryKey}
     * @return {$this->modelName}Item|null
     * @throws Exception
     */
    public function reload(\${$this->primaryKey})
    {
        self::resetCollectionId(\${$this->primaryKey});
        return \$this->load(\${$this->primaryKey});
    }\n\n
EOPHP;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return string
     */
    private function writefetchAllMethodForTableModel()
    {
        $tab = $this->tabchar;

        $filterItems = "\n";
        foreach($this->filtersItems as $filter) {
            if ($filter->phpVariable == 'orderBy') {
                continue;
            }
            $getter = $this->getGetter($filter);
            $filterItems .= <<<EOSQL
            if (\$filters->$getter()) {
                \$sqlWhere[] = "{$filter->name} {$filter->sqlOperator} {\$filters->$getter()}";
            }\n
EOSQL;
        }
        $filterItems .= "{$tab}{$tab}";

        $orderItems = "\n";
        foreach($this->orderItems as $orderItem) {
            $orderItems .= <<<EOSQL
                    case \$filters::{$orderItem->key}:
                        \$sqlOrder[] = "{$orderItem->query}";
                        break;\n
EOSQL;
        }
        $orderItems .= "{$tab}{$tab}";

        $injection = /** @lang text */
            <<<EOPHP
    /**
     * @param {$this->modelName}Filters \$filters
     * @param int|null \$limit
     * @param int|null \$offset
     * @return array
     */
    public function fetchAll({$this->modelName}Filters \$filters = null, \$limit = null, \$offset = null)
    {
        // Set where and having conditions.
        \$sqlWhere = array();
        \$sqlHaving = array();
        if (\$filters) {{$filterItems}}

        \$sqlWhere = implode("\\n\\tAND ", \$sqlWhere);
        \$sqlWhere = !empty(\$sqlWhere) ? "AND " . \$sqlWhere : '';

        \$sqlHaving = implode("\\n\\tAND ", \$sqlHaving);
        \$sqlHaving = !empty(\$sqlHaving) ? "HAVING " . \$sqlHaving : '';

        // ------------------------------------

        \$sqlOrder = array();
        if (\$filters && count(\$filters->getOrderBy())) {
            foreach(\$filters->getOrderBy() as \$ord) {
                switch(\$ord) {{$orderItems}}
            }
        }
        if (!count(\$sqlOrder)) {
            \$sqlOrder[] = "{$this->primaryKey} DESC";
        }
        \$sqlOrder = implode(", ", \$sqlOrder);

        // ------------------------------------

        // Set limit.
        \$sqlLimit = '';
        if (!is_null(\$offset) && !is_null(\$limit)) {
            \$sqlLimit = 'LIMIT ' . (int)\$offset . ', ' . (int)\$limit;
        } elseif (!is_null(\$limit)) {
            \$sqlLimit = 'LIMIT ' . (int)\$limit;
        }

        // ------------------------------------

        \$sql = <<<EOSQL
SELECT SQL_CALC_FOUND_ROWS
    *
FROM
    {$this->tableName}
WHERE
    1
    \$sqlWhere
GROUP BY {$this->primaryKey}
\$sqlHaving
ORDER BY \$sqlOrder
\$sqlLimit
EOSQL;

        /** @var CI_DB_result \$result */
        \$result = \$this->db->query(\$sql);
        \$count = \$this->db->query("SELECT FOUND_ROWS() AS cnt")->row()->cnt;
        \$items = array();
        foreach(\$result->result('array') as \$row){
            \$items[] = new {$this->modelName}Item(\$row);
        }

        return array(
            'items' => \$items,
            'count' => \$count,
        );
    }\n\n
EOPHP;
        return $injection;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return string
     */
    private function writeSaveMethodForTableModel()
    {
        $tab = $this->tabchar;

        $elements = "\n";
        foreach($this->columns as $column) {
            if ($column->primary_key) {
                continue;
            }
            $getter = $this->getGetter($column);
            $elements .= "{$tab}{$tab}{$tab}'{$column->name}' => \$item->{$getter}() ? \$item->{$getter}() : null,\n";
        }
        $elements .= "{$tab}{$tab}";

        $getterPrimaryKey = 'get' . ucfirst($this->primaryKey);

        return <<<EOPHP
    /**
     * @param {$this->modelName}Item \$item
     * @return bool|int
     */
    public function save({$this->modelName}Item \$item)
    {
        // Set data.
        \$eData = array({$elements});

        // Insert/Update.
        if (!\$item->{$getterPrimaryKey}()) {
            \$res = \$this->db->insert('{$this->tableName}', \$eData);
        } else {
            \$res = \$this->db->update('{$this->tableName}', \$eData, '{$this->primaryKey} = ' . \$item->$getterPrimaryKey());
        }

        // Remove old item from cache
        self::resetCollectionId(\$item->$getterPrimaryKey());

        return \$res;
    }\n\n
EOPHP;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * @return string
     */
    private function writeDeleteMethodForTableModel()
    {
        $getterPrimaryKey = 'get' . ucfirst($this->primaryKey);

        return <<<EOPHP
    /**
     * @param {$this->modelName}Item \$item
     * @return mixed
     */
    public function delete({$this->modelName}Item \$item)
    {
        // Remove old item from cache
        self::resetCollectionId(\$item->$getterPrimaryKey());

        return \$this->db->delete('{$this->tableName}', '{$this->primaryKey} = ' . \$item->$getterPrimaryKey());
    }\n\n
EOPHP;
    }


}


class ColumnItem
{

    /**
     * Column name
     * @var string
     */
    public $name;

    /**
     * Column type (MySQL)
     * @var string
     */
    public $type;

    /**
     * @var int|null
     */
    public $max_length;

    /**
     * @var mixed
     */
    public $default;

    /**
     * @var bool
     */
    public $primary_key;

    /**
     * @var string
     */
    public $key;

    /**
     * @var string
     */
    public $phpVariable;

    /**
     * @var string
     */
    public $phpType;

    /**
     * @var string
     */
    public $castAs;

    /**
     * Default sql operator for filters
     * @var string
     */
    public $sqlOperator = '=';


    /**
     * ColumnItem constructor.
     * @param \stdClass|null $col
     */
    public function __construct($col)
    {
        if (!$col) {
            return;
        }

        $this->name = $col->name;
        $this->type = $col->type ? $col->type : 'mixed';
        $this->max_length = $col->max_length ? (int)$col->max_length : null;
        $this->default = $col->default ? $col->default : null;
        $this->primary_key = !is_null($col->primary_key) ? (bool)$col->primary_key : null;
        $this->key = $col->key ? $col->key : null;

        // Set PHP variable, in function MySQL field name
        $this->setPhpVariable();

        // Set PHP type, in function of MySQL field type
        $this->setPhpType();

        // Set cast type
        $this->setCastType();

        // Set SQL operator
        $this->setSqlOperator();
    }

    // ---------------------------------------------------------------------------------------------

    private function setPhpVariable()
    {
        $this->phpVariable = MyStrings::_convertToCamelCase(trim($this->name));
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Set PHP type, in function of MySQL field type
     * Inspired by http://propelorm.org/Propel/reference/schema.html#column-types
     * return null;
     */
    private function setPhpType()
    {
        $mysqlType = strtoupper($this->type);

        $typeAssociation = array(
            'CHAR' => 'string',
            'VARCHAR' => 'string',
            'TEXT' => 'string',
            'LONGTEXT' => 'string',
            'DECIMAL' => 'string',      // PHP int is limited
            'BIGINT' => 'string',       // PHP int is limited

            'INTEGER' => 'int',
            'TINYINT' => 'int',
            'SMALLINT' => 'int',
            'INT' => 'int',

            'REAL' => 'float',
            'FLOAT' => 'float',
            'DOUBLE' => 'float',

            'BLOB' => 'double',
            'MEDIUMBLOB' => 'double',
            'LONGBLOB' => 'double',

            'BOOLEAN' => 'bool',
            'BOOL' => 'bool',

            // The data below should be DateTime objects, but I'm not ready for this yet. It's a nightmare when you reach the __construct
            'DATETIME' => 'string',
            'TIMESTAMP' => 'string',
            'DATE' => 'string',
            'TIME' => 'string',
        );

        foreach($typeAssociation as $key=>$value) {
            if (strpos($mysqlType, $key) === 0) {
                $this->phpType = $value;
                return null;
            }
        }

        $this->phpType = 'mixed';
    }

    // ---------------------------------------------------------------------------------------------

    private function setCastType()
    {
        $phpType = strtolower($this->phpType);
        switch($phpType) {
            case 'string':
                $castAs = '(string)';
                break;
            case 'int':
                $castAs = '(int)';
                break;
            case 'float':
            case 'double':
                $castAs = '(float)';
                break;
            default:
                $castAs = '';
                break;
        }
        $this->castAs = $castAs;
    }

    // ---------------------------------------------------------------------------------------------

    private function setSqlOperator()
    {
        $phpType = strtolower($this->phpType);
        switch($phpType) {
            case 'string':
                $op = 'LIKE';
                break;
            case 'int':
            case 'float':
            case 'double':
            default:
                $op = '=';
                break;
        }
        $this->sqlOperator = $op;
    }

}


class OrderItem
{

    /**
     * Constant name
     * @var string
     */
    public $key;

    /**
     * Constant value
     * @var string
     */
    public $value;

    /**
     * Will be showned in the filters panel (in the view)
     * @var string
     */
    public $label;

    /**
     * This is inserted into the query, in the ORDER BY clause
     * @var string
     */
    public $query;


    /**
     * OrderItem constructor.
     * @param string $columnName - column name from db
     * @param string $sortOrder (ASC/DESC)
     */
    public function __construct($columnName, $sortOrder)
    {
        if (empty($columnName)) {
            return;
        }

        $sortOrder = strtoupper($sortOrder);
        $sortOrder = $sortOrder == 'DESC' ? $sortOrder : 'ASC';

        $this->key = 'ORDER_BY_' . strtoupper($columnName) . '_' . $sortOrder;
        $this->value = $columnName . '_' . strtolower($sortOrder);
        $this->label = $columnName . ' - ' . $sortOrder;
        $this->query = $columnName . ' ' . $sortOrder;
    }

}

/**
* string helpers
*/
class MyStrings
{

    /**
    * Convert string to Camel Case
    *
    * @param mixed $_str
    * @return string
    */
    public static function _convertToCamelCase($_str)
    {
        $_str    = trim($_str);
        $pieces  = explode(' ', trim(preg_replace('/([^A-Za-z0-9]+)|(?=[A-Z])/', ' ', $_str)));

        $variable = '';
        if( sizeof($pieces) > 1 )
        {
            array_walk($pieces, function($item, $key) use (&$variable){
                $item = strtolower($item);
                $variable .= $key ? ucfirst($item) : lcfirst($item);
            });
        }else{
            $variable = reset($pieces);
        }

        return $variable;
    }

}