<?php

namespace Tools;

use Exception;
use ReflectionClass;
use ReflectionMethod;

class ApiDocumenter
{

    /**
     * @var string
     */
    public $apiName;

    /**
     * @var string
     */
    public $apiDescription;

    /**
     * @var ApiMethodDocumenter[]
     */
    public $apiMethods = [];

    /**
     * ApiDocumenter constructor.
     * @param string $className
     */
    public function __construct($className)
    {
        try {
            $serviceDef = new ReflectionClass($className);
        } catch (Exception $e) {
            die($e->getMessage());
        }

        // Set api name
        $this->apiName = $className;

        // Set api name
        $this->apiDescription = $serviceDef->getDocComment();

        // Fetch list of methods
        $methods = $serviceDef->getMethods(ReflectionMethod::IS_PUBLIC);
        foreach($methods as $methodDef) {
            if ($methodDef->class != $className) {
                continue;   // ignore methods that are inherited
            }
            if (substr($methodDef->getName(), 0, 2) == '__') {
                continue;   // ignore magic methods
            }
            if ($methodDef->isStatic()) {
                continue;   // ignore static methods
            }
            if ($methodDef->getName() == 'index_get') {
                continue;   // ignore method that lists methods :P
            }

            // Append parsed api method
            $this->apiMethods[] = new ApiMethodDocumenter($methodDef);
        }
    }

}