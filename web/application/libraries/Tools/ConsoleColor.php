<?php

namespace Tools;

/**
 * Class ConsoleColor
 * Got my information from: https://misc.flogisoft.com/bash/tip_colors_and_formatting
 */
class ConsoleColor
{

    // Foreground colors
    public $default     = "\033[39m";
    public $black       = "\033[30m";
    public $blue        = "\033[34m";
    public $green       = "\033[32m";
    public $cyan        = "\033[36m";
    public $red         = "\033[31m";
    public $purple      = "\033[35m";
    public $brown       = "\033[33m";
    public $lightGray   = "\033[37m";

    public $darkGray    = "\033[90m";
    public $lightRed    = "\033[91m";
    public $lightGreen  = "\033[92m";
    public $lightYellow = "\033[93m";
    public $lightBlue   = "\033[94m";
    public $lightMagenta= "\033[95m";
    public $lightCyan   = "\033[96m";
    public $white       = "\033[97m";

    // Background colors
    public $bgBlack     = "\033[40m";
    public $bgRed       = "\033[41m";
    public $bgGreen     = "\033[42m";
    public $bgYellow    = "\033[43m";
    public $bgBlue      = "\033[44m";
    public $bgMagenta   = "\033[45m";
    public $bgCyan      = "\033[46m";
    public $bgLightGray = "\033[47m";

    // Formatting
    public $bold        = "\033[1m";
    public $dim         = "\033[2m";
    public $underlined  = "\033[4m";
    public $blink       = "\033[5m";
    public $reverse     = "\033[7m";
    public $hidden      = "\033[8m";

    // Reset formatting
    public $reset       = "\033[0m";
    public $resetAll    = "\033[0m";
    public $resetBold   = "\033[21m";
    public $resetDim    = "\033[22m";
    public $resetUnderlined = "\033[24m";
    public $resetBlink  = "\033[25m";
    public $resetReverse= "\033[27m";
    public $resetHidden = "\033[28m";

}