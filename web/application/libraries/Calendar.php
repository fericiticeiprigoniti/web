<?php

/**
 * Calendar class
 *
 * handles date/time functionalities
 */
class Calendar
{

    /**
     * Check if date is valid - Romanian style (dd.mm.YYYY)
     * @param string $f_date
     * @param string $sep - Date separator
     * @return bool
     */
    public static function checkDate($f_date, $sep = '.')
    {
        // Check length.
        if (strlen($f_date) != 10) {
            return false;
        }

        // Check the position of separator variable.
        if ((strpos($f_date, $sep) != 2) || (strpos($f_date, $sep, 3) != 5)) {
            return false;
        }

        // Convert day, month and year values to integer.
        $day = intval($f_date[0] . $f_date[1]);
        $month = intval($f_date[3] . $f_date[4]);
        $year = intval($f_date[6] . $f_date[7] . $f_date[8] . $f_date[9]);

        // Return result.
        return checkdate($month, $day, $year);
    }
    /**
     * Check if partial date is valid - Romanian style (dd.mm.YYYY)
     * @param string $f_date
     * @param string $sep - Date separator
     * @return bool
     */
    public static function checkPartialDate($f_date, $sep = '.')
    {
        // Check length.
        if (strlen($f_date) != 10) {
            return false;
        }

        // Check the position of separator variable.
        if ((strpos($f_date, $sep) != 2) || (strpos($f_date, $sep, 3) != 5)) {
            return false;
        }

        // Convert day, month and year values to integer.
        $day = intval($f_date[0] . $f_date[1]);
        $month = intval($f_date[3] . $f_date[4]);
        $year = intval($f_date[6] . $f_date[7] . $f_date[8] . $f_date[9]);

        // Check if year is valid.
        if ($year < 1000 || $year > 9999) {
            return false;
        }

        // Check if month is valid.
        if ($month > 12) {
            return false;
        }

        // Check if day is valid.
        if ($day > 31) {
            return false;
        }

        return true;
    }

    /**
     * Check if it's a datetime valid value - Romanian style (dd.mm.YYYY HH:ii)
     * @param string $f_date
     * @param string $dateSep - Date separator
     * @param string $timeSep - Time separator
     * @return bool|int
     */
    public static function checkDatetime($f_date, $dateSep = '.', $timeSep = ':')
    {
        // Check length.
        if (strlen($f_date) != 16) {
            return false;
        }

        // Check the position of separator variables.
        if ((strpos($f_date, $dateSep) != 2) || (strpos($f_date, $dateSep, 3) != 5) || (strpos($f_date, $timeSep, 6) != 13)) {
            return false;
        }

        // Convert day, month, year, hours, minutes values to integer.
        $day = intval($f_date[0] . $f_date[1]);
        $month = intval($f_date[3] . $f_date[4]);
        $year = intval($f_date[6] . $f_date[7] . $f_date[8] . $f_date[9]);
        $hours = intval($f_date[11] . $f_date[12]);
        $minutes = intval($f_date[14] . $f_date[15]);

        // Return result - if it's not correct it returns false.
        return mktime($hours, $minutes, 0, $month, $day, $year);
    }

    /**
     * Convert Romanian date to Mysql date.
     * @param string $f_date - date in Romanian format (d.m.Y)
     * @return bool|string - date in Mysql format (Y-m-d)
     */
    public static function convertDate2Mysql($f_date)
    {
        if (is_null($f_date)) return;

        $day = $f_date[0] . $f_date[1];
        $month = $f_date[3] . $f_date[4];
        $year = $f_date[6] . $f_date[7] . $f_date[8] . $f_date[9];

        return checkdate($month, $day, $year) ? "$year-$month-$day" : false;
    }

    /**
     * Convert a string like 00/00/1999 to 1999-00-00 or 01/00/1990 to 1990-00-01
     * @param string $f_date - date in Romanian format (dd/mm/YYYY)
     * @param string $sep - Date separator
     * @return bool|string - date in Mysql format (Y-mm-dd)
     */
    public static function convertPartialDate2Mysql($f_date, $sep = '/')
    {
        if (is_null($f_date)) return false;

        // Check the position of separator variable.
        if ((strpos($f_date, $sep) != 2) || (strpos($f_date, $sep, 3) != 5)) {
            return false;
        }

        // Convert day, month and year values to integer.
        $day = intval($f_date[0] . $f_date[1]);
        $month = intval($f_date[3] . $f_date[4]);
        $year = intval($f_date[6] . $f_date[7] . $f_date[8] . $f_date[9]);

        if ($year < 1000 || $year > 9999) {
            return false;
        }

        if ($month < 0 || $month > 12) {
            return false;
        }

        if ($day < 0 || $day > 31) {
            return false;
        }

        return sprintf('%04d-%02d-%02d', $year, $month, $day);
    }
    /**
     * Convert date from Mysql format to Romanian (YYYY-mm-dd -> dd.mm.YYYY)
     * @param string $sqlDate - date in Mysql format
     * @return string|void - date in dd.mm.YYYY format
     */
    public static function convertDateMysql2Ro($sqlDate)
    {
        if (!$sqlDate) {
            return;
        }

        $arr = explode(" ", $sqlDate);
        $d = explode("-", $arr[0]);

        return "{$d['2']}.{$d['1']}.{$d['0']}";
    }

    /**
     * Convert time from seconds to Hours Minutes Seconds format.
     * @param int $sec
     * @return string
     */
    public static function seconds2Hms($sec)
    {
        $sec = (int)$sec;
        if (!$sec) {
            return false;
        }
        $hours = floor($sec / 3600);
        $minutes = floor(($sec / 60) % 60);
        $seconds = $sec % 60;
        return $hours . 'h ' . $minutes . 'm ' . $seconds . 's';
    }

    /**
     * validate a partial date
     *
     * @param mixed $date - array cu Day/Month/Year
     * ex: array(false,'12','2016'), array(false,false,'2016')
     * @param mixed $format
     * @return int
     */
    public static function validateDate($date, $format = 'd-m-Y')
    {
        $f_date = $f_format = array();

        foreach (explode('-', $format) as $key => $f) {
            if (!empty($date[$key]) && (int)$date[$key]) {
                $f_format[] = $f;
                $f_date[] = sprintf('%02d', $date[$key]);
            }
        }

        $date_filtered = implode('-', array_filter($f_date));
        $format        = implode('-', $f_format);

        $dateCreated = DateTime::createFromFormat($format, $date_filtered);
        if ($dateCreated && $dateCreated->format($format) == $date_filtered) {
            list($day, $month, $year) = $date;
            return (int)sprintf('%04d', $year) . sprintf('%02d', $month) . sprintf('%02d', $day);
        } else {
            return false;
        }
    }

    /**
     * decode an string date from Mysql
     * ex: 19201012 - into 12/10/1920
     * ex: 00001012 - into 12/10
     *
     * @param mixed $date
     * @param mixed $delimiter
     * @return string|null
     */
    public static function convertIntDate2Ro($date, $delimiter = '/')
    {
        if (empty($date)) {
            return null;
        }

        $year   = substr($date, 0, 4);
        $month  = substr($date, 4, 2);
        $day    = substr($date, 6, 2);

        return implode($delimiter, array($day, $month, $year));
    }

    /**
     * @param int|null $now
     * @return false|string
     */
    public static function sqlDateTime($now = null)
    {
        return date('Y-m-d H:i:s', $now ?: time());
    }

    public static function fetchLunileAnului()
    {
        return array(
            1 => 'Ianuarie',
            'Februarie',
            'Martie',
            'Aprilie',
            'Mai',
            'Iunie',
            'Iulie',
            'August',
            'Septembrie',
            'Octombrie',
            'Noiembrie',
            'Decembrie'
        );
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Convert date from d.m.Y [H:i[:s]] format to timestamp
     * @param string $str
     * @param bool $lastSecond
     * @param bool $throwException
     * @return false|int|null
     * @throws Exception
     */
    public static function strToTimestamp($str, $lastSecond = false, $throwException = true)
    {
        $str = trim($str);
        $str = preg_split('/[ :.\/]/', $str);
        switch (count($str)) {
            case 3:
                return mktime($lastSecond ? 23 : 0, $lastSecond ? 59 : 0, $lastSecond ? 59 : 0, $str[1], $str[0], $str[2]);
            case 5:
                return mktime($str[3], $str[4], 0, $str[1], $str[0], $str[2]);
            case 6:
                return mktime($str[3], $str[4], $str[5], $str[1], $str[0], $str[2]);
            default:
                if ($throwException) {
                    throw new Exception('Date format is invalid');
                }
        }
        return null;
    }

    /**
     * Convert date from d.m.Y [H:i[:s]] format to DateTime object
     * @param string $str
     * @param bool $lastSecond
     * @param bool $throwException
     * @return DateTime|null
     * @throws Exception
     */
    public static function strToDateTime($str, $lastSecond = false, $throwException = true)
    {
        $timestamp = self::strToTimestamp($str, $lastSecond, $throwException);
        if (!$timestamp) {
            return null;
        }

        $result = new DateTime();
        $result->setTimestamp($timestamp);

        return $result;
    }

    /**
     * @param string $mysqlDate - Mysql date format (Y-m-d)
     * @param bool $lastSecond
     * @return DateTime|null
     */
    public static function mysqlDateToDateTime($mysqlDate, $lastSecond = false)
    {
        $arr = explode('-', $mysqlDate);

        if (count($arr) != 3) {
            return null;
        }

        $year = (int)$arr[0];
        $month = (int)$arr[1];
        $day = (int)$arr[2];

        if (!$year || !$month || !$day) {
            return null;
        }

        if (!checkdate($month, $day, $year)) {
            return null;
        }

        $timestamp = $lastSecond ? mktime(23, 59, 59, $month, $day, $year) : mktime(0, 0, 0, $month, $day, $year);

        $return = new DateTime();
        $return->setTimestamp($timestamp);
        return $return;
    }

    /**
     * @param string $mysqlDate - Mysql date format (Y-m-d H:i:s)
     * @return DateTime|null
     */
    public static function mysqlDateTimeToDateTimeObject($mysqlDate)
    {
        return $mysqlDate ? new DateTime($mysqlDate) : null;
    }
}
