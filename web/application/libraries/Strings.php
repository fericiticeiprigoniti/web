<?php

/**
 * String class
 *
 * handles strings
 */
class Strings
{

    public static function convertText2Html($text)
    {
        // Convert new lines '\n' to brakes '<br />'
        $text = str_replace("\n", '<br />', $text);

        // Generate links.
        $text = self::makeClickableLinks($text);

        return $text;
    }

    /**
     * @param string $s
     * @return string|string[]|null
     */
    public static function makeClickableLinks($s)
    {
        return preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a href="$1" target="_blank">$1</a>', $s);
    }

    // ---------------------------------------------------------------------------------------------

    /**
    * Convert string to Camel Case
    *
    * @param string $_str
    * @return string
    */
    public static function convertToCamelCase($_str)
    {
        $_string = trim($_str);
        $pieces  = explode(' ', trim(preg_replace('/([^A-Za-z0-9]+)|(?=[A-Z])/', ' ', $_string)));

        $variable = '';
        if( sizeof($pieces) > 1 )
        {
            array_walk($pieces, function($item, $key) use (&$variable){
                $item = strtolower($item);
                $variable .= $key ? ucfirst($item) : lcfirst($item);
            });
        }else{
            $variable = reset($pieces);
        }

        return $variable;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Sanitize string for database usage.
     * @param string $searchTerm
     * @param array $excludedCharacters
     * @return string
     */
    public static function sanitizeForDatabaseUsage($searchTerm, array $excludedCharacters = array())
    {
        $searchTerm = trim(strip_tags($searchTerm));
        if (!count($excludedCharacters)) {
            $excludedCharacters = array("'", '"', '_', '/', '%', ',', '^', '*', '+', '=', '|');
        }

        $searchTerm = str_replace($excludedCharacters, '', $searchTerm);
        return $searchTerm;
    }

    /**
     * Split a string into multiple words (array) and keep only the ones that meet the criterias
     * @param string $searchTerm
     * @param int $maxNrWords - maximum number of words accepted
     * @param int $minWordLength - minimum word length
     * @return array
     */
    public static function splitIntoWords($searchTerm, $maxNrWords = 3, $minWordLength = 2)
    {
        $maxNrWords = (int)$maxNrWords;
        $minWordLength = (int)$minWordLength;

        // Split into words.
        $words = preg_split('/\s+/', $searchTerm);

        // Parse words and keep only valid ones.
        $i = 0;
        $data = array();
        foreach ($words as $word) {
            if (strlen($word) < $minWordLength) {    // Accept words that have a minimum length
                continue;
            }
            $i++;
            $data[] = $word;
            if ($i == $maxNrWords) {    // If reached the maximum number of words, break loop.
                break;
            }
        }

        return $data;
    }

    // ---------------------------------------------------------------------------------------------

    /**
    * @param string $_str
    * @return string
    */
    public static function fill($_str)
    {
        return !empty($_str) ? html_escape($_str) : '';
    }

}
