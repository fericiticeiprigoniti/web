<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

class MY_Form_validation extends CI_Form_validation
{

    public function __construct()
    {
        parent::__construct($config = array());

        $this->_error_prefix = '<div class="error">';
        $this->_error_suffix = '</div>';
    }

    /**
     * Set Error
     *
     * @access  public
     * @param   string
     * @return  bool
    */
    function add_error($field, $message = '')
    {
        // Save the error message
        $this->_field_data[$field]['error'] = $message;

        if ( ! isset($this->_error_array[$field]))
        {
            $this->_error_array[$field] = $message;
        }
    }

    function data_valida($str, $format)
    {
        $date = date_parse_from_format($format, $str);
        return checkdate($date['month'], $date['day'], $date['year']);
    }

    function ora_valida($ora)
    {
        $valid = false;
        if (preg_match("/(2[0-3]|[01][0-9]):[0-5][0-9]/", $ora))
        {
            $valid = true;
        }
        return $valid;
    }
    
    function valid_phone($phone)
    {        
        $this->set_message('valid_phone', lang('text_campul').' \'%s\' '. lang('text_nu_este_valid'));        
        return ( ! preg_match("/^([+]?[0-9]+){9,}$/ix", $phone)) ? FALSE : TRUE;        
    }

    function values($input, $values)
    {
        $this->set_message('values', lang('text_campul').' \'%s\' '. lang('text_nu_este_valid'));        
        return in_array($input, explode(',', $values));
    }

    function clear($noUnsetPost = false)
    {        
        $this->_field_data = array();        
        if (!$noUnsetPost) unset($_POST);
    }

    function rfc_date($date)
    {
        if (!preg_match('/^([12]\d{3})-(?!00)(\d{2})-(?!00)(\d{2})$/', $date)) return false;
        if (strtotime($date) === false) return false;

        return true;
    }
    
    /**
     * Error Array
     *
     * Returns the error messages as an array
     *
     * @return  array
     */
    function error_array()
    {
        if (count($this->_error_array) === 0)
        {
                return FALSE;
        }
        else
            return $this->_error_array;
 
    }

}

/** end of file */