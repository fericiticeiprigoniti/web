<?php

/**
 * Messenger class
 * Handles all messaging (alerts, errors, success messages)
 */
class Mess
{

    private $errors = array();
    private $infos = array();
    private $warnings = array();

    /**
     * Add error
     * @param mixed $msg
     */
    public function addError($msg)
    {
        if (is_array($msg)) {
            foreach($msg as $k=>$v) {
                $this->errors[$k] = $v;
            }
        } else {
            $this->errors[] = $msg;
        }
    }

    /**
     * Get all errors
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Check if we have any errors saved
     * @return bool
     */
    public function hasErrors()
    {
        return count($this->errors) > 0;
    }

    /**
     * Print errors in json format
     * @param string|array|null $err
     */
    public function displayErrors($err=null)
    {
        if (is_null($err)) {
            $res = json_encode(array('error' => $this->errors));
        } elseif(is_array($err)) {
            $res = json_encode(array('error' => $err));
        } else {
            $res = json_encode(array('error' => array($err)));
        }
        echo $res;
        exit;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Add info
     * @param string $msg
     */
    public function addInfo($msg)
    {
        $this->infos[] = $msg;
    }

    /**
     * Get all infos
     * @return array
     */
    public function getInfos()
    {
        return $this->infos;
    }

    /**
     * Check if we have any infos saved
     * @return bool
     */
    public function hasInfos()
    {
        return count($this->infos) > 0;
    }

    /**
     * Print infos in json format
     * @param null|string $msg
     */
    public function displayInfos($msg=null)
    {
        echo is_null($msg) ? json_encode(array('info' => $this->infos)) : json_encode(array('info' => array($msg)));
        exit;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Add warning
     * @param string $msg
     */
    public function addWarning($msg)
    {
        $this->warnings[] = $msg;
    }

    /**
     * Get all warnings
     * @return array
     */
    public function getWarnings()
    {
        return $this->warnings;
    }

    /**
     * Check if we have any warnings saved
     * @return bool
     */
    public function hasWarnings()
    {
        return count($this->warnings) > 0;
    }

    /**
     * Print warnings in json format
     * @param null|string $msg
     */
    public function displayWarnings($msg=null)
    {
        echo is_null($msg) ? json_encode(array('warning' => $this->warnings)) : json_encode(array('warning' => array($msg)));
        exit;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Print success message in json format
     * @param string $msg
     * @param array $append
     */
    public function displaySuccess($msg, array $append = array())
    {
        die(json_encode(array_merge(array('success' => $msg), $append)));
    }

}
