<?php
$lang['surname']        = "Surname";
$lang['name']           = "Name";
$lang['namesurname']    = "Name and surname";
$lang['username']       = "Username";
$lang['email']          = "E-mail";
$lang['phone']          = "Phone";
$lang['password']       = "Password";
$lang['message']        = "Message";
$lang['new_password']           = "New password";
$lang['confirm_password']       = "Confirm password";
$lang['address']                = "Address";
$lang['adrs_correspondence']    = "Mailing address";
$lang['subject']                = "Subject";

// other
$lang['ora']              = 'Hour';
$lang['data']             = 'Date';
$lang['traveldate']       = 'Travel date';
$lang['tourname']         = 'Tour name';
$lang['no_of_persons']    = 'Number of persons';
$lang['google_captcha']   = "Pleace check 'I`m not robot' to send mail!";
$lang['message_send_success']   = 'The message was succesfully sent! Thank you!';
$lang['message_send_fail']      = 'The message has no been sent. Try again!';
$lang['insert_required_fields'] = 'Insert required fields';