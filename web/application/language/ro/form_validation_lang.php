<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['required']           = "Câmpul '%s' este obligatoriu.";
$lang['isset']              = "Câmpul '%s' este obligatoriu.";
$lang['valid_email']        = "Câmpul '%s' trebuie să conţină o adresă de e-mail validă.";
$lang['valid_emails']       = "Câmpul '%s' trebuie să conțină adrese de e-mail valide.";
$lang['valid_url']          = "Câmpul '%s' trebuie să conţinâ un URL valid.";
$lang['valid_ip']           = "Câmpul '%s' trebuie să conţină o adresă de IP validă.";
$lang['min_length']         = "Câmpul '%s' trebuie să aibă minimum %s caractere.";
$lang['max_length']         = "Câmpul '%s' trebuie să aibă maximum %s caractere.";
$lang['exact_length']       = "Câmpul '%s' trebuie să aibă exact %s caractere.";
$lang['alpha']              = "Câmpul '%s' trebuie să aibă conţină numai litere.";
$lang['alpha_numeric']      = "Câmpul '%s' trebuie să aibă conţină numai caractere alfa-numerice.";
$lang['alpha_dash']         = "Câmpul '%s' trebuie să aibă conţină numai caractere alfa-numerice, _ (underscore) şi - (minus).";
$lang['numeric']            = "Câmpul '%s' trebuie să conţină numai caractere numerice.";
$lang['is_numeric']         = "Câmpul '%s' trebuie să conțină numai caractere numerice.";
$lang['integer']            = "Câmpul '%s' trebuie să conțină un număr întreg.";
$lang['regex_match']        = "Câmpul '%s' nu este formatat corespunzător.";
$lang['matches']            = "Câmpul '%s' nu are aceeaşi valoare cu '%s'.";
$lang['is_unique']          = "Câmpul '%s' trebuie să conțină o valoare unică.";
$lang['is_natural']         = "Câmpul '%s' trebuie să conțină un număr natural.";
$lang['is_natural_no_zero'] = "Câmpul '%s' trebuie să conțină un număr mai mare decât 0.";
$lang['decimal']            = "Câmpul '%s' trebuie să conțină un număr zecimal.";
$lang['less_than']          = "Câmpul '%s' trebuie să conțină un număr mai mic decât %s.";
$lang['greater_than']       = "Câmpul '%s' trebuie să conțină un număr mai mare decât %s.";

//addons

/** End of file */