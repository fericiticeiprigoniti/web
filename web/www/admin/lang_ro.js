// titluri pentru stiky notes
var NOTICE_TITLE_ERROR_VALIDATION   = 'Erori validare';
var NOTICE_CONTENT_FORM_ERRORS      = 'Datele nu pot fi salvate. Formularul conţine erori';
var NOTICE_TITLE_SUCCESS            = 'Succes';
var NOTICE_CONTENT_SUCCESS_EDIT     = 'Modificările au fost salvate cu succes';
var NOTICE_FORM_REQUIRED_FIELDS     = 'Completați câmpurile obligatorii';