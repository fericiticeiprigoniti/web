<?php
/*
  RoxyFileman - web based file manager. Ready to use with CKEditor, TinyMCE. 
  Can be easily integrated with any other WYSIWYG editor or CMS.

  Copyright (C) 2013, RoxyFileman.com - Lyubomir Arsov. All rights reserved.
  For licensing, see LICENSE.txt or http://RoxyFileman.com/license

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

  Contact: Lyubomir Arsov, liubo (at) web-lobby.com
*/
function checkAccess($action){
    
    $ip_address     = (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']); // remote addr IP
    
    // decrypt data
    $key = "0d7xPM3CRzaz5JMmDE9jQ44LnJSfuRst"; // The key size does not matter
    $iv = "WEAREGOO"; // The IV MUST be equal to the block size of the encryption method
    $ciphertext_dec = base64_decode($_COOKIE['fm_token']);

    $decryped_text = trim(mcrypt_decrypt(MCRYPT_BLOWFISH, $key, $ciphertext_dec, MCRYPT_MODE_CBC, $iv)); // remove white spaces after decryption
    
    if( $ip_address != $decryped_text )
    {
        var_dump('knocking on another door');
        die(); 
    }
}
?>