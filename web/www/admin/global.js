$(document).ready(function(){

    tinymce.init({
      selector: '.tinymce',
      height: 500,
      theme: 'modern',
      plugins: [
        'advlist autolink lists link image charmap hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime nonbreaking save table contextmenu directionality',
        'template paste textcolor colorpicker textpattern imagetools'
      ],
      toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor',
      file_browser_callback: RoxyFileBrowser ,
      image_advtab: true,
      templates: [
        { title: 'Test template 1', content: 'Test 1' },
        { title: 'Test template 2', content: 'Test 2' }
      ],
      content_css: [
        '//www.tiny.cloud/css/codepen.min.css'
      ],
      valid_elements : '+*[*]',
      cleanup: false
     });

    function RoxyFileBrowser(field_name, url, type, win) {
      var roxyFileman = '/www/admin/plugins/fileman/index.html';
      if (roxyFileman.indexOf("?") < 0) {
        roxyFileman += "?type=" + type;
      }
      else {
        roxyFileman += "&type=" + type;
      }
      roxyFileman += '&input=' + field_name + '&value=' + win.document.getElementById(field_name).value;
      if(tinyMCE.activeEditor.settings.language){
        roxyFileman += '&langCode=' + tinyMCE.activeEditor.settings.language;
      }
      tinyMCE.activeEditor.windowManager.open({
         file: roxyFileman,
         title: 'Roxy Fileman',
         width: 850,
         height: 650,
         resizable: "yes",
         plugins: "media",
         inline: "yes",
         close_previous: "no"
      }, {     window: win,     input: field_name    });
      return false;
    }

    tinymce.init({
      selector: ".tinymcesimple",
      height: 200,
      menubar: false,
      plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks fullscreen",
        "insertdatetime media table contextmenu paste",
      ],
      toolbar:
        "insertfile | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
    });

    
    tinymce.init({
      selector: ".tinymcesimple-sm",
      height: 80,
      menubar: false,
      plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks fullscreen",
        "insertdatetime media table contextmenu paste",
      ],
      toolbar:
        "insertfile | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
    });

    $('.alert .close').slideUp();
})

