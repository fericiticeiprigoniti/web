/**
 * Show alert (error, warning, success, info)
 * @param opts object {type, container, title, data, scroll}
 */
function showAlert(opts) {
  // Set default options.
  let defaults = {
    type: "info", // Alert type (error, warning, success, info)
    container: "#messagesContainer", // Alert container (parent)
    title: "", // Alert title
    data: "", // Alert data (html code)
    scroll: true, // Scroll to error
  };

  // Merge default options with the ones submited
  let options = $.extend({}, defaults, opts);

  // Set alert class.
  let alertClass = "";
  let iconClass = "";
  switch (options.type) {
    case "error":
      alertClass = "alert-danger";
      iconClass = "fa-times";
      break;
    case "warning":
      alertClass = "alert-warning";
      iconClass = "fa-exclamation-triangle";
      break;
    case "success":
      alertClass = "alert-success";
      iconClass = "fa-check";
      break;
    case "info":
      alertClass = "alert-info";
      iconClass = "fa-info-circle";
      break;
    default:
      alertClass = "alert-info";
      iconClass = "fa-info-circle";
      break;
  }

  // Set alert container.
  let container =
    options.container != "" ? options.container : defaults.container;

  // Set title.
  let title = options.title != "" ? "<b>" + options.title + "</b> - " : "";

  // Set final content and append it.
  let mess =
    '<div class="alert ' +
    alertClass +
    ' alert-dismissible fade in" role="alert">' +
    '<button class="close" aria-label="Close" data-dismiss="alert" type="button">' +
    '<span aria-hidden="true">×</span>' +
    "</button>" +
    '<i class="fa ' +
    iconClass +
    '"> </i> ' +
    "<strong>" +
    title +
    "</strong> " +
    options.data +
    "</div>";
  $(container).append(mess);

  // Go to error.
  if (options.scroll) {
    $("html, body").animate(
      {
        scrollTop: $(container).offset().top - 15,
      },
      300
    );
  }
}

// -------------------------------------------------------------------------------------------------

$(document).ready(function () {
  $(document).delegate("button.close", "click", function (event) {
    event.preventDefault();
    $(this).closest("div.alert").remove();
  });

  $(".datepicker").datepicker({
    dateFormat: "dd-mm-yy",
  });

  $("select").select2({
    dropdownAutoWidth: true,
    width: "100%",
  });

  $(".allowClear").select2({
    allowClear: true,
    width: "100%",
  });

  $(":input").inputmask();
  $(".nav-tabs").scrollingTabs();

  // Fix for sorting fields - we need to keep the order of the selected items. It's very important!
  $("select.selectOrderBy").on("select2:select", function (evt) {
    let element = evt.params.data.element;
    let $element = $(element);

    $element.detach();
    $(this).append($element);
    $(this).trigger("change");
  });
});

// -------------------------------------------------------------------------------------------------

// Copies a string having the begining and end chars/strings.
function special_substr(data_string, from_string, to_string) {
  let from_index = data_string.indexOf(from_string) + from_string.length;
  let to_index = null;
  if (to_string == null) {
    to_index = data_string.length;
  } else {
    to_index = data_string.indexOf(to_string);
  }
  return data_string.substring(from_index, to_index);
}

// -------------------------------------------------------------------------------------------------

/**
 * Create snippet from string of max length.
 * @param str
 * @param maxLength
 */
function createSnippet(str, maxLength) {
  if (str == null) {
    return null;
  }
  if (maxLength == null) {
    return str;
  } else {
    maxLength = parseInt(maxLength);
  }
  if (str.length > maxLength) {
    str = str.substring(0, maxLength);
    str = str.substring(0, str.lastIndexOf(" ")) + " ...";
  }
  return str;
}

// -------------------------------------------------------------------------------------------------

/**
 * confirmation dialog, the bootstrap way
 */
$(document).on("click", "a.delete-confirm", function (event) {
  event.preventDefault();
  let href = this.href;
  let message = this.getAttribute("confirm-message");
  if (!message) {
    message = "Are you sure?";
  }
  bootbox.confirm(message, function (result) {
    if (result) {
      document.location.href = href;
    }
  });
});

// -------------------------------------------------------------------------------------------------

function showLoadingImage() {
  let obj = $("#requestLoading");
  if (obj.length) {
    obj[0].style.visibility = "visible";
  }
}

function hideLoadingImage() {
  let obj = $("#requestLoading");
  if (obj.length) {
    obj[0].style.visibility = "hidden";
  }
}

// -------------------------------------------------------------------------------------------------

/**
 * Show loading image when sorting.
 */
$(document).ready(function () {
  let th = "table.dataTable > thead > tr > th.sorting";
  th += ", table.dataTable > thead > tr > th.sorting_asc";
  th += ", table.dataTable > thead > tr > th.sorting_desc";
  $(th).click(function (event) {
    event.preventDefault();
    showLoadingImage();
    let link = $(this).children("a")[0];
    window.location.href = $(link).attr("href");
  });
});

// -------------------------------------------------------------------------------------------------

/**
 * validate required input - add a red border on it & cell background
 *
 */
function validateRequiredFields($form) {
  $("td").removeClass("cell_error");
  $("body").find("input,textarea,select").removeClass("valid_error");

  $form
    .find("input,textarea,select")
    .filter("[required]:visible")
    .each(function (i, requiredField) {
      console.log(i, $(requiredField).val());

      if ($(requiredField).val() == "" || $(requiredField).val() == null) {
        $(requiredField).addClass("valid_error");
        $(requiredField).parent("td").addClass("cell_error");

        showNotification({ text: NOTICE_FORM_REQUIRED_FIELDS });
      }
    });
}

// -------------------------------------------------------------------------------------------------

/**
 * add CI form error bellow each input/textarea/select
 *
 *
 */
function showFormErrors(element) {
  // Destroy old errors.
  let all_errors = document.getElementsByClassName("error");
  $(all_errors).remove();

  // Show errors on form.
  let field = "";
  $.each(element, function (k, v) {
    field = document.getElementsByName(k)[0];
    if (field !== undefined) {
      $(field)
        .parent()
        .append('<div class="error">' + v + "</div>");
    }
  });
}

// -------------------------------------------------------------------------------------------------

/**
 * display Stiky Notes - notification
 */
function showNotification(opts) {
  // Set default options.
  let defaults = {
    title: NOTICE_TITLE_ERROR_VALIDATION,
    text: NOTICE_CONTENT_FORM_ERRORS,
    type: "error",
  };

  // Merge default options with the ones submited
  let options = opts === "undefined" ? defaults : $.extend({}, defaults, opts);

  new PNotify({
    title: options.title,
    text: options.text,
    type: options.type,
    styling: "bootstrap3",
    delay: 1000,
  });
}

/**
 *
 * @param itemOrList string|object
 * @param notificationType string
 */
function showNotifications(itemOrList, notificationType = "error") {
  if (notificationType === "warning") {
    notificationType = "notice";
  }

  if (typeof itemOrList === "object") {
    $.each(itemOrList, function (index, item) {
      showNotification({ text: item, type: notificationType });
    });
  } else {
    showNotification({ text: itemOrList, type: notificationType });
  }
}

// -------------------------------------------------------------------------------------------------

/**
 * reset input/textarea/select fields
 */
function resetForm($form) {
  $form
    .find("input:text, input:password, input:file, select, textarea")
    .val("");
  $form
    .find("input:radio, input:checkbox")
    .removeAttr("checked")
    .removeAttr("selected");
  $form.find("select").select2("val", "");
}

// -------------------------------------------------------------------------------------------------

// Auto-submit Ajax forms.
$(document).ready(function () {
  $("body").on("click", "#ajaxSubmit", function (event) {
    $(".tab-pane.active").find(".ajaxForm").submit();
  });

  // Save element.
  $(".ajaxForm").submit(function (event) {
    event.preventDefault();

    // save from tinymce editor
    tinyMCE.triggerSave();

    let form = $(this);
    let formAction = form.attr("action");
    let formData = form.serialize();
    let redirect_url = form.data("redirect-url");
    let saveButton = $("button[type='submit']", this);

    $.ajax({
      type: "POST",
      url: formAction,
      data: formData,
      dataType: "json",
      beforeSend: function () {
        showLoadingImage();
      },
      success: function (result) {
        hideLoadingImage();

        $.each(result, function (index, element) {
          if (index == "error") {
            // Destroy old errors.
            let all_errors = document.getElementsByClassName("error");
            $(all_errors).remove();

            showFormErrors(element);

            showNotification();

            if (element) {
              $.each(element, function (index, item) {
                showNotification({ text: item });
              });
            }

            // enable Submit button
            $.wait(function () {
              $(saveButton).text("Salvare").prop("disabled", false);
            }, 3);
          } else if (index == "success") {
            // push pNotify
            showNotification({
              type: "success",
              title: "Succes",
              text: result.success,
            });

            // Redirect
            setTimeout(function () {
              window.location.href = redirect_url
                ? redirect_url
                : result.redirect_url
                ? result.redirect_url
                : formAction;
            }, 500); //will call the function after 1 secs.
          }
        });
      },
    });
  });

  // Delete element.
  $("body").on("click", ".btnDelete", function (event) {
    event.preventDefault();

    let softDelete = $(this).hasClass("softDelete");

    let htmlMessage = '<div id="messConfContainer"></div>';
    if (softDelete) {
      htmlMessage += "Ești sigur că vrei să ștergi";
    } else {
      htmlMessage +=
        "Ștergerea este definitivă!! Esti sigur?<br/> Se va șterge fără să mai dai save la formular";
    }
    htmlMessage += " <b>" + $(this).data("name") + "</b>";

    let tr = $(this).parents("tr");
    let url = $(this).attr("href");

    bootbox.dialog({
      message: htmlMessage,
      title: "Confirmare ștergere definitivă",
      buttons: {
        btnDelete: {
          label: "<i class='ace-icon fa fa-trash-o'></i> Șterge",
          className: "btn-sm btn-primary",
          callback: function (event) {
            event.preventDefault();
            $("#messConfContainer").html(""); // Empty/reset alert container.

            $.ajax({
              type: "POST",
              url: url,
              data: { submited: true },
              dataType: "json",
              beforeSend: function () {
                showLoadingImage();
              },
              success: function (result) {
                hideLoadingImage();
                $.each(result, function (index, element) {
                  if (index == "error") {
                    $.each(element, function (k, v) {
                      showAlert({
                        type: "error",
                        data: v,
                        container: "#messConfContainer",
                        scroll: false,
                      });
                    });
                  } else if (index == "success") {
                    if (softDelete) {
                      location.reload();
                    } else {
                      tr.remove();
                      bootbox.hideAll();
                    }
                  }
                });
              },
            });
            return false;
          },
        },
        btnCancel: {
          label: "<i class='ace-icon fa fa-ban'></i> Anulează",
          className: "btn-sm btn-default",
          callback: function () {
            //Nothing to do. It closes automatically.
          },
        },
      },
    });
  });
});

// -------------------------------------------------------------------------------------------------

// DevBridge Autocomplete
$(document).ready(function () {
  let devbridgeAutocomplete = $(".devbridge-autocomplete");

  devbridgeAutocomplete.each(function () {
    let field = this;
    $(field).devbridgeAutocomplete({
      serviceUrl: $(field).data("url"),
      deferRequestBy: 300, // wait 300ms before making the ajax call.
      minChars: 2,
      noCache: true,
      groupBy: "category",
      onSelect: function (suggestion) {
        let fieldIdObj = $("#" + $(field).data("id"));

        fieldIdObj.val(suggestion.id);
        fieldIdObj.trigger("devbridgeValueChanged");
      },
    });
  });

  devbridgeAutocomplete.change(function () {
    if ($(this).val() == "") {
      let fieldId = $(this).data("id");
      $("#" + fieldId).val("");
    }
  });
});

// -------------------------------------------------------------------------------------------------

// Filter form submit
$(document).ready(function () {
  $(".filter-form").submit(function () {
    // Remove empty form elements before submiting.
    $(".filter-form input, .filter-form select").each(function () {
      if ($(this).val() === "") {
        $(this).attr("name", "");
      }
    });
    showLoadingImage();
  });
});

// -------------------------------------------------------------------------------------------------

$(document).ready(function () {
  // Simulate Ajax.
  $("body").on("click", ".ajaxLink", function (event) {
    event.preventDefault();

    let formAction = $(this).attr("href");
    if (formAction === undefined) {
      console.error("Invalid url");
      return false;
    }

    if ($(this).hasClass("withConfirmation")) {
      let confirmationMessage = $(this).data("message")
        ? $(this).data("message")
        : "Are you sure?";
      bootbox.confirm(confirmationMessage, function (result) {
        if (result) {
          ajaxLink(formAction);
        }
      });
    } else {
      ajaxLink(formAction);
    }
  });
});

// -------------------------------------------------------------------------------------------------

// Wait X seconds
$.wait = function (callback, seconds) {
  return window.setTimeout(callback, seconds * 1000);
};

// -------------------------------------------------------------------------------------------------

function openPopup(href, params) {
  let width = params && params.width ? params.width : 800;
  let height = params && params.height ? params.height : 600;
  let w = window.open(
    href,
    "_blank",
    "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=yes, width=" +
      width +
      ", height=" +
      height
  );
  w.focus();
  return w;
}

// -------------------------------------------------------------------------------------------------

/**
 *
 * @returns {*}
 * @param href
 * @param opts
 */
function ajaxLink(href, opts) {
  // Set default data.
  let defaults = {
    submited: true,
  };

  // Merge default data with the ones submited
  let data = $.extend({}, defaults, opts);

  return $.ajax({
    type: "POST",
    url: href,
    data: data,
    beforeSend: function () {
      showLoadingImage();
    },
    success: function (result) {
      hideLoadingImage();

      let objResult = JSON.parse(result);

      // If it's not a valid json, display one error message with raw content
      if (typeof objResult !== "object") {
        showNotification({ text: result });
        return;
      }

      let isValid = false;

      $.each(objResult, function (index, element) {
        showNotifications(element, index);
        if (index === "success") {
          isValid = true;
        }
      });

      // Reload page
      if (isValid) {
        setTimeout(function () {
          window.location.reload();
        }, 500);
      }
    },
  });
}
