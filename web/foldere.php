<?php
function getFoldersRecursive($path) {
    $folders = [];

    $iterator = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::SKIP_DOTS),
        RecursiveIteratorIterator::SELF_FIRST
    );

    foreach ($iterator as $item) {
        if ($item->isDir()) {
            $folders[] = $item->getPathname();
        }
    }

    return $folders;
}

// Setează calea folderului dorit (ex. application)
$path = __DIR__ . '/application';
$folders = getFoldersRecursive($path);

echo "<pre>";
print_r($folders);
echo "</pre>";
