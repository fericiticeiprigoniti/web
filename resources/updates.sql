﻿
-- Acest fisier este folosit pentru salva toate modificarile de structura care au loc asupra bazei de date

-- 2016-05-24 - Daniel
-- (1151 de randuri si toate cu valoare NULL)
ALTER TABLE fcp_autori DROP COLUMN prenume;

-- -------------------------------------------------------------------------------------------------

-- 2016-07-14 - Nicanor
RENAME TABLE `fcp_poezii_momentulcreatiei` TO `fcp_poezii_momentecreatie`;
RENAME TABLE `fcp_poezii_structurastrofe` TO `fcp_poezii_structuristrofa`;
RENAME TABLE `fcp_poezii_piciormetric` TO `fcp_poezii_picioaremetrice`;
ALTER TABLE `fcp_poezii` CHANGE `data_insert` `data_insert` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `fcp_poezii` MODIFY `is_deleted` TINYINT(3) NOT NULL DEFAULT 2;

-- -------------------------------------------------------------------------------------------------

-- 2016-07-18 - Alex (+ inca cateva tabele care le-am modificat fara sa mai salvez query-ul :) )
ALTER TABLE `fcp_personaje2semnalmente`
	CHANGE COLUMN `p2sID` `id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT FIRST ,
	CHANGE COLUMN `p2sPersonajID` `personaj_id`  int(10) UNSIGNED NOT NULL AFTER `id`,
	CHANGE COLUMN `p2sSemnID` `semn_id`  int(10) UNSIGNED NOT NULL COMMENT 'semnalmente ID' AFTER `personaj_id`,
	CHANGE COLUMN `p2sDetalii` `detalii`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `semn_id`,
	DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci;

ALTER TABLE `fcp_personaje_functii`
	CHANGE COLUMN `funcID` `id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT FIRST ,
	CHANGE COLUMN `funcNume` `nume`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `id`,
	DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci;

-- ocupatia principala pe care a avut-o personajul
ALTER TABLE `fcp_personaje2ocupatii`
	ADD COLUMN `is_primary` TINYINT UNSIGNED NOT NULL DEFAULT 0 AFTER `ocupatie_id`;

ALTER TABLE `fcp_personaje_pedepse_detentie`
ADD COLUMN `order_id`  int UNSIGNED NOT NULL DEFAULT 0 AFTER `content`;


ALTER TABLE `fcp_personaje_distinctii`
MODIFY COLUMN `data`  int(8) UNSIGNED NULL DEFAULT NULL COMMENT 'data primirii' AFTER `titlu_primit`;

ALTER TABLE `fcp_personaje2functii`
MODIFY COLUMN `data_start`  int(8) UNSIGNED NULL DEFAULT NULL AFTER `functie_id`,
MODIFY COLUMN `data_end`  int(8) UNSIGNED NULL DEFAULT NULL AFTER `data_start`;

ALTER TABLE `fcp_personaje_pedepse_detentie`
MODIFY COLUMN `data_start`  int(8) UNSIGNED NULL DEFAULT NULL AFTER `pedeapsa_id`,
MODIFY COLUMN `data_end`  int(8) UNSIGNED NULL DEFAULT NULL AFTER `data_start`;

-- -------------------------------------------------------------------------------------------------

-- 2017-01-11 - Daniel
ALTER TABLE fcp_personaje CHANGE COLUMN status_id status_id TINYINT UNSIGNED DEFAULT 0 COMMENT '0 - in lucru / unpublished; 1 - activ / published';

-- -------------------------------------------------------------------------------------------------

-- 2017-01-14 - Daniel
ALTER TABLE fcp_personaje ADD COLUMN is_deleted TINYINT NOT NULL DEFAULT 0 COMMENT '1-true; 0-false';

-- -------------------------------------------------------------------------------------------------

-- 2017-01-16 - Daniel
CREATE TABLE `nom_tari` (
	`cod` char(2) NOT NULL,
	`nume` varchar(255) NOT NULL,
	`nume_anterior` varchar(255) DEFAULT NULL,
	`lang` enum('ro','en') NOT NULL DEFAULT 'en',
	`ord` int(10) unsigned DEFAULT NULL,
	PRIMARY KEY (`cod`),
	UNIQUE KEY `nume` (`nume`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Import data from nom_tari.sql

CREATE TABLE `nom_judete` (
	`id` int(11) NOT NULL,
	`cod_tara` char(2) NOT NULL,
	`cod` varchar(32) NOT NULL,
	`nume` varchar(255) NOT NULL,
	`nume_anterior` varchar(255) DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `idx_cod_tara_nume` (`cod_tara`,`nume`),
	CONSTRAINT `nom_judete_ibfk_1` FOREIGN KEY (`cod_tara`) REFERENCES `nom_tari` (`cod`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Import data from nom_judete.sql

CREATE TABLE `nom_localitati_tip` (
	`cod` char(4) NOT NULL,
	`nume` varchar(50) NOT NULL,
	`ord` tinyint(4) DEFAULT NULL,
	PRIMARY KEY (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO fcp.nom_localitati_tip (cod, nume, ord) VALUES
	('loc.', 'localitate', 1),
	('mun.', 'municipiu', 2),
	('oras', 'oraş', 3),
	('com.', 'comună', 4),
	('sat', 'sat', 5),
	('sec.', 'sector', 6);

CREATE TABLE `nom_localitati` (
	`id` int(11) NOT NULL,
	`id_judet` int(11) NOT NULL,
	`nume` varchar(255) NOT NULL,
	`nume_anterior` varchar(255) DEFAULT NULL,
	`tip_localitate` char(4) NOT NULL DEFAULT 'loc.' COMMENT 'nom_localitati_tip.cod',
	`parent_id` int(10) unsigned DEFAULT NULL COMMENT 'nom_localitati.id',
	PRIMARY KEY (`id`),
	KEY `fk_nom_localitati_1` (`id_judet`),
	KEY `fk_tip_localitate` (`tip_localitate`),
	KEY `idx_parent_id` (`parent_id`),
	CONSTRAINT `nom_localitati_ibfk_1` FOREIGN KEY (`id_judet`) REFERENCES `nom_judete` (`id`) ON UPDATE CASCADE,
	CONSTRAINT `nom_localitati_ibfk_2` FOREIGN KEY (`tip_localitate`) REFERENCES `nom_localitati_tip` (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Import data from nom_localitati.sql

-- -------------------------------------------------------------------------------------------------

-- 2017-01-19 - Alex
# este un array in config/constants PEDEPSE_TIPURI
ALTER TABLE `fcp_personaje_condamnari`
CHANGE COLUMN `cond_pedeapsa_felul` `cond_pedeapsa_felul_id`  tinyint UNSIGNED NULL DEFAULT NULL AFTER `cond_pedeapsa_durata`;

-- 2017-01-20 - Alex
#remove tables
DROP TABLE fcp_condamnari_iesire;
DROP TABLE fcp_condamnari_tipuri;
#se gasesc datele in config/constants

-- 2017-01-21 - Alex
ALTER TABLE `fcp_personaje2locuripatimire`
MODIFY COLUMN `data_start`  int(8) UNSIGNED NULL DEFAULT NULL AFTER `loc_patimire_id`,
MODIFY COLUMN `data_end`  int(8) UNSIGNED NULL DEFAULT NULL AFTER `data_start`,

-- 2017-01-22 - Alex
ADD INDEX `indx_persID` (`personaj_id`),
CHANGE COLUMN `order` `order_id`  int(10) UNSIGNED NULL DEFAULT NULL AFTER `condamnare_id`;

-- -------------------------------------------------------------------------------------------------

-- 2017-08-10 - Daniel
ALTER TABLE fcp_biblioteca_carti_dimensiuni DROP COLUMN dimensiune_w;
ALTER TABLE fcp_biblioteca_carti_dimensiuni DROP COLUMN dimensiune_h;
ALTER TABLE fcp_biblioteca_carti_dimensiuni ADD COLUMN latime INT NOT NULL COMMENT 'in milimetri';
ALTER TABLE fcp_biblioteca_carti_dimensiuni ADD COLUMN inaltime INT NOT NULL COMMENT 'in milimetri';

-- -------------------------------------------------------------------------------------------------

-- 2020-04-08 - Daniel
DROP TABLE IF EXISTS fcp_personaje_extrainfo;

-- -------------------------------------------------------------------------------------------------

-- 2020-10-02 - Silviu
ALTER TABLE fcp_articole_categorii add status_id TINYINT UNSIGNED DEFAULT 0 COMMENT '0 - in lucru / unpublished; 1 - activ / published';

-- -------------------------------------------------------------------------------------------------

-- 2022-03-18 - Alex
RENAME TABLE fcp_personaje_condamnari TO fcp_personaje_episoade_represive;

-- 2023-11-11 - Alex
ALTER TABLE fcp_biblioteca_carti ADD COLUMN oras VARCHAR(255) AFTER localitate_id;
