# README #
---
### Cum instalez proiectul? ###

* Cloneaza proiectul folosind comanda:\
  _**git clone git@bitbucket.org:fericiticeiprigoniti/web.git**_
* creeaza fisierul `application/config/config.php` dupa modelul `config.sample.php`


### Docker ###
1. Instalare Docker -> https://www.docker.com/
2. Editeaza fisierul `hosts` adăugând o linie nouă cu `127.0.0.1	dev.fcp`

  Vezi:
    * MAC -> https://help.nexcess.net/how-to-find-the-hosts-file-on-my-mac
    * Windows & Linux -> https://www.howtogeek.com/howto/27350/beginner-geek-how-to-edit-your-hosts-file/
  

3. Acceseaza directorul `dev-env` din terminal și executa comenzile:
    - `cp .env.sample .env` - copiem si modificam fisierul care seteaza variabilele de sistem 
    - `docker-compose -p fcp-web up --build --force-recreate` - fortam sa se faca build-ul (e pentru cand pornim initial proiectul)
    - `docker-compose up -d` pentru a face build la imagini și pentru a crea containerele pentru php-fpm & nginx
    - `docker-compose down` - daca dorești să oprești proiectul
    - `docker ps` - pentru a vedea containerele
    - `docker exec -it {container_id} bash` - daca dorești acces ssh la unul dintre containere

# Website #
Pentru a accesa site-ul intrati pe:
* `http://dev.fcp:8080`
* `http://dev.fcp:8080/api`
  - user: `admin@example.com`
  - password: `12345`

# Mod de lucru #

### Crearea modelelor ###

Pentru generarea automata a modelelor, se folsoeste o librarie (ORM) Tools.
Este o librarie custom, realizată de noi (soo, nu cautați documentație pe net :)).

*Observatie: pentru a rula comenzile de mai jos trebuie sa fiti conectati la container-ul PHP.*

*Utilizare*:

* `$ cd /path/to/project`
* `$ php index.php tools createModel {table_name} {model_name}`

Ex: `php index.php tools createModel fcp_articole Articol`

Se vor genera in /application/models/Generated trei fisiere:

* `ArticolItem.php` - contine metodele set/get pentru fiecare coloana din tabelul fcp_articole
* `ArticolFilters.php` - se genereaza cu un minim de filtre necesare. Este clasa care se ocupa de filtrare/ordonare articole. Se vor defini metode noi in cazul in care se doreste o cautare mai avansata.
* `ArticolTables.php` - este clasa care se ocupa de preluarea datelor din baza de date, folosindu-se de obiectul ArticolFilters, si returnand o lista cu obiecte ArticolItem.php


# Pentru admini conectare ssh

- docker-compose up -d
- docker exec -ti {container_php} bash
- apt update
- apt install ssh
- ssh -L 127.0.0.1:3306:127.0.0.1:3306 aalex@server01.cpco.ro